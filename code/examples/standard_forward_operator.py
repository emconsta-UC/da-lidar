
# Create a forward operator with standard settings, that can be used for multiple experiments

import numpy as np
from scipy.io import netcdf as Dataset
import matplotlib.pyplot as plt
import pickle
import datetime as datetime
import os
import os.path as pathfile
import urllib
import sys
import re

python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0): 
    pass
else:
    range = xrange

# Prepare paths, and sub-packages:
parent_path = os.path.abspath('../')
if parent_path not in sys.path:
    sys.path.append(parent_path)

try:
    from forward_model import ForwardOperator
except ImportError:
    import dl_setup
    dl_setup.main()
    from forward_model import ForwardOperator


def dynamical_model_configs():
    """
    Standard configuratiions of the dynamical model (HyPar)
    """
    model_configs = {'size': [101, 101, 41],
                     'iproc': [2, 2, 2],
                     'dt': 0.15,
                     'n_iter': 10,
                     'cleanup_dir':False,
                     'boundary':dict(num_faces=6,  # X-low, X-high, Y-low, Y-high, Z-low, Z-high
                                   faces_types=['periodic',
                                                'periodic',
                                                'periodic',
                                                'periodic',
                                                'thermal-noslip-wall',
                                                'slip-wall'],
                                   faces_settings=[None,
                                                   None,
                                                   None,
                                                   None,
                                                   (0.0, 0.0, 0.0, 'temperature_data.dat'),
                                                   (0.0, 0.0, 0.0)]
                                 ), #
                    }
    return model_configs

def observation_configs(prognostic_variable='wind-velocity'):
    """
    Standard lidar observation configurations
    Remarks:
        Consider making it more flexible by adding options!
    """
    # Observations Settings (passed to DL_obs)
    obs_configs = {'site_facility': 'sgpdlC1',
                   't': 0,
                   'dl_coordinates': None,
                   'prog_var': prognostic_variable,
                   'num_gates': 400,
                   'range_gate_length': 30,
                   'elevations': [45, 60, 90],
                   'azimuthes': [0, 45, 90, 135, 180, 225, 270, 315]
                   }
    return obs_configs


def create_forward_operator(prognostic_variable="radial_velocity"):
    """
    """
    model_configs = dynamical_model_configs()
    obs_configs = observation_configs(prognostic_variable=prognostic_variable)
    # create forward operator (dynamical model + observations):
    model = ForwardOperator(model_configs, obs_configs)

    return model


if __name__ == '__main__':

    model = create_forward_operator()

