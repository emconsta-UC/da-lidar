#! /usr/bin/env python

""" Gather DL-data within a specific interval, and analysis data availability, and data collection frequency
    This will be added to DL_data_handler module after testing, and debugging
"""

import sys
import os
import shutil

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()
import DL_utility as utility
#

import numpy as np
import datetime
import pickle

# if False:
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import rc

python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange
 

def plots_enhancer():
    # set fonts, and colors:
    font_size = 12
    font = {'family' : 'serif',
            'weight' : 'bold',
            'size'   : font_size}
    #
    rc('font', **font)
    rc('text', usetex=True)

def create_plots_dir(dir_name='DopplerLIDAR_DataAvailabilityResults', overwrite=True):
    """
    """
    if dir_name is None:
        dir_name = '__PLOTS'
    elif isinstance(dir_name, str):
        if len(dir_name.strip()) == 0:
            dir_name = '__PLOTS'
    else:
        print("dir_name must be a valid string, or pass None for a default name!")
        raise TypeError
    #
    if os.path.isabs(dir_name):
        plots_path = dir_name
    else:
        head, _ = os.path.split(__file__)
        plots_path = os.path.join(head, dir_name)
    #print(dir_name, plots_path)

    if (os.path.isdir(plots_path) and (not overwrite)):
        print("Existing results directory: %s ; Skipping as requested... " % plots_path)
        # plots_path = None
    else:
        if os.path.isdir(dir_name):
            shutil.rmtree(dir_name)
        os.makedirs(plots_path)
    return plots_path
    #



# Import DL_DATA_HANDLER and generate an object (instances)
from DL_data_handler import DL_DATA_HANDLER


def create_availability_plots(num_obs, times, durations, valid_coords, seq_valid_coords, plots_dir, plot_fileprefix=None, show_plots=False):
    """
    """
    #
    plots_enhancer()
    #
    sepp = "\n" + ("*"*80) + "\n"
    print("%sCreating Doppler Lidar data availability plots over timespan:\n\t[%s to %s]%s" % (sepp, times[0], times[-1], sepp))
    # Plot general data availability overtime (regardless of observational coordinates)
    fig = plt.figure(facecolor='white')
    title = "Lidar Data Availability over time regardless the coordinates"
    title += "\nStart Time = %s" % times[0]
    title += "\tEnd Time = %s" % times[-1]
    fig.suptitle(title)
    ax1 = fig.add_subplot(111)
    ax1.plot(range(num_obs-1), durations, 'b-d', linewidth=0.5, markersize=4, markerfacecolor='red')
    ax1.set_ylabel(r"$Time \delta$ (seconds)")
    ax1.set_xlabel("Time")

    ticks = []
    for i in ax1.get_xticks():
        if int(i)<len(times):
           ticks.append(int(i))
    ax1.set_xticks(ticks)
    tick_labels = [utility.str_to_timestamp(times[i], timeonly=True) for i in ax1.get_xticks()]

    ax1.set_xticklabels(tick_labels)
    ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45)
    
    if plot_fileprefix is None:
        file_name = "Lidar_Data_Availability"
    else:
        file_name = plot_fileprefix
    file_name = utility.try_file_name(plots_dir, file_prefix=file_name, extension='pdf', return_abspath=True)
    plt.savefig(file_name, dpi=500, facecolor='w', format='PDF', transparent=True, bbox_inches='tight')
    print("File created: %s " % os.path.abspath(file_name))

    # 3D-Plot of data availablity over time (elevation/azimuth/time plot)
    elev_list = [c[0] for c in valid_coords]
    azim_list = [c[1] for c in valid_coords]

    # The next plot will take significantly long time to build and render; order 
    fig = plt.figure(facecolor='white')
    title = "Lidar Data Availability over time:"
    title += "\nStart Time = %s" % times[0]
    title += "\tEnd Time = %s" % times[-1]
    fig.suptitle(title)
    ax2 = Axes3D(fig)
    for time_ind in range(num_obs):
        elev_list = [c[0] for c in seq_valid_coords[time_ind]]
        azim_list = [c[1] for c in seq_valid_coords[time_ind]]
        ax2.scatter(azim_list, elev_list, time_ind, c='b', marker='^')
    ax2.set_xlabel('Azimuth')
    ax2.set_ylabel('Elevation')
    ax2.set_zlabel('Time')

    # Set time labels
    ticks = []
    for i in ax2.get_zticks():
        if int(i)<len(times):
            ticks.append(int(i))
    ax2.set_zticks(ticks)
    tick_labels = [utility.str_to_timestamp(times[i], timeonly=True) for i in ax2.get_zticks()]
    ax2.set_zticklabels(tick_labels)

    # print("Creating a 3D plot for Doppler Lidar data availability of the selected timespan; this may take few seconds...")
    if plot_fileprefix is None:
        file_name = "Lidar_Data_Availability_3D"
    else:
        file_name = "%s_3D" % plot_fileprefix
    file_name = utility.try_file_name(plots_dir, file_prefix=file_name, extension='pdf', return_abspath=True)
    plt.savefig(file_name, dpi=500, facecolor='w', format='PDF', transparent=True, bbox_inches='tight')
    print("File created: %s " % os.path.abspath(file_name))

    if show_plots:
        plt.show()

    plt.close(fig)
    return

def collect_observations(start_time, end_time, site_facility, field, collect_remote_data, results_dir, screen_output=False):
    """
    """
    sep = "*" * 80
    print("%s\n  Checking doppler Lidar data availability on the time interval : " % sep)
    print("Start Time: %s \nEnd Time: %s \n%s" % (start_time, end_time, sep))

    # create lidar data object:
    dl_obj = DL_DATA_HANDLER()
    times, observations = dl_obj.get_DL_site_observations(field=field, site_facility=site_facility,
                                                          timespan=[start_time, end_time],
                                                          aggregate_data=False,  # <-- notice this flag
                                                          snr_threshold=False,
                                                          collect_remote_data=collect_remote_data
                                                         )

    # number of timepoints where observations are avialble
    num_obs = len(times)
    if num_obs == 0:
        print("Didn't find any matching observations...")
        if not collect_remote_data:
            print("Consider setting 'collect_remote_data' to True!")
            return tuple([None]*6)

    if screen_output:  # this branch, if enabled, will likely show lots of nans/missing-values
        # print data: Plotting should be considered next:
        num_obs = len(times)
        for i, t in enumerate(times):
            print("%s\n Observation [%4d / %4d] \n%s" % ('='*50, i, num_obs,'-'*25))
            print("Time = %s " % repr(t))
            print("DL-Data:")
            print(observations[i])  # <- we can show table of coordinates/values, but plotting is better!
    
    if num_obs == 0:
        print("No observation found in the specified interval")
        return tuple([None]*6)
    print("%d observation instances found" % num_obs)
    seq_valid_coords = []
    # print only available (not-nan) entries:
    for i, t in enumerate(times):
        valid_coords, valid_obs = observations[i].extract_valid_entries()
        seq_valid_coords.append(valid_coords)
        if screen_output:
            print("%s\n Observation [%4d / %4d] \n%s" % ('='*50, i, num_obs,'-'*25))
            print("Time = %s " % repr(t))
            print("DL-Data:")
            for c, o in zip(valid_coords, valid_obs):
                print("Coordinates: %s\t Observation [%s: %s]" % (c, field, o))

    # Get some statistics, and plot data availability; other ideas!
    min_time_delta = times[1]-times[0]
    time_deltas = []
    for i in range(1, num_obs):
        time_diff = times[i] - times[i-1]
        time_deltas.append(time_diff)
        if  time_diff < min_time_delta:
            min_time_delta = time_diff
    durations = [d.total_seconds() for d in time_deltas]

    data_avail_results = dict(start_time=start_time,
                              end_time=end_time,
                              site_facility=site_facility,
                              field=field,
                              times=times,
                              num_obs=num_obs,
                              seq_valid_coords=seq_valid_coords,
                              valid_coords=valid_coords,
                              valid_obs=valid_obs,
                              durations=durations
                             )
    # write data availability resuls to file
    file_name = utility.try_file_name(directory=results_dir, file_prefix="Lidar_Data_Availability_Results", extension='pickle', return_abspath=True)
    # print("Pickling to %s" % file_name)
    with open(file_name, 'wb') as f_id:
        pickle.dump(data_avail_results, f_id)

    return times, durations, num_obs, seq_valid_coords, valid_coords, valid_obs


if __name__ == "__main__":

    # general settings
    plot_existing       = True   # loop over results in the results directory, and plot only; no data collection
    create_plots        = True  # helpfu for remote runs
    screen_output       = False  # print some stuff to screen if you need!
    show_plots          = False  # turn if to save figures only
    collect_remote_data = True   # Synchronize data with main data repo on rlogin

    if not plot_existing:
        # Observation time setting (Multiple-days results, splitted into hourly files/groups):
        # Specify lists with corresponding start_times [t00, t01, ...], and end_times [t10, t11, ...]
        start_times   = ['2016:06:04:%02d:00:00'%i for i in range(0, 24) ]
        end_times     = ['2016:06:04:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:06:05:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:06:05:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:06:06:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:06:06:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:06:07:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:06:07:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:06:08:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:06:08:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:06:09:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:06:09:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:06:10:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:06:10:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:06:11:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:06:11:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:06:12:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:06:12:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:10:15:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:10:15:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:10:19:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:10:19:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2016:10:25:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2016:10:25:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:01:01:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:01:01:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:01:02:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:01:02:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:01:03:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:01:03:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:03:01:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:03:01:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:05:01:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:05:01:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:01:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:01:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:02:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:02:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:03:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:03:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:04:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:04:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:05:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:05:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:06:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:06:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:07:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:07:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:08:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:08:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:09:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:09:%02d:59:59'%i for i in range(0, 24) ]
        start_times  += ['2017:07:10:%02d:00:00'%i for i in range(0, 24) ]
        end_times    += ['2017:07:10:%02d:59:59'%i for i in range(0, 24) ]

        site_facility = 'sgpdlE32'
        field         = 'radial-velocity'  # any filed will work here

        # Create proper directory for ersults:
        results_dir = os.path.abspath(create_plots_dir(overwrite=True))

        for start_time, end_time in zip(start_times, end_times):
            # Collect observations, and save availability results
            times, durations, num_obs, seq_valid_coords, \
                    valid_coords, valid_obs = collect_observations(start_time=start_time,
                                                                   end_time=end_time,
                                                                   site_facility=site_facility,
                                                                   field=field,
                                                                   results_dir=results_dir,
                                                                   collect_remote_data=collect_remote_data
                                                                  )

            if num_obs is times is durations is valid_coords is seq_valid_coords is None:
                continue
            else:
                # Create plots:
                if create_plots:
                    create_availability_plots(num_obs, times, durations, valid_coords, seq_valid_coords, plots_dir=results_dir, show_plots=show_plots)

    else:
        # get the directory where the results are saved:
        results_dir = os.path.abspath(create_plots_dir(overwrite=False))
        list_of_files = utility.get_list_of_files(results_dir, recursive=False)  # TODO: correct this

        for fpath in list_of_files:
            _, fname = os.path.split(fpath)
            fname, fext = os.path.splitext(fname)
            fext = fext.lower().strip('. ')

            # read results
            if fext == 'pickle':
                # cont = pickle.load(open(fpath, 'rb'))
                cont = utility.read_pickled_data(fpath)
                num_obs          = cont['num_obs']
                times            = cont['times']
                durations        = cont['durations']
                valid_coords     = cont['valid_coords']
                seq_valid_coords = cont['seq_valid_coords']

                # Plot results
                plot_file_name = fname
                create_availability_plots(num_obs, times, durations, valid_coords, seq_valid_coords, plot_fileprefix=plot_file_name, plots_dir=results_dir, show_plots=show_plots)


    print("...Done...\n")

