#! /usr/bin/env python

""" Gather Doppler Lidar data/observations over a given timespan with/without aggregation, and plot results as suitble

"""

import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange
import os

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()
#

import DL_utility as utility

import numpy as np
import datetime

import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from matplotlib import rc



def plots_enhancer():
    # set fonts, and colors:
    font_size = 12
    font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : font_size}
    #
    rc('font', **font)
    rc('text', usetex=True)


# Import DL_DATA_HANDLER and generate an object (instances)
from DL_data_handler import DL_DATA_HANDLER

if __name__ == "__main__":
    
    # Sync with server to get all observations vs. only local availability on this machine
    collect_remote_data = False # Set to True once

    # general settings
    screen_output = False  # print some stuff to screen if you need!
    show_plots = True  # turn if to save figures only

    # Observation time setting:
    start_time = '2017:05:28:00:00:00'
    end_time   = '2017:05:28:04:00:00'
    site_facility='sgpdlE32'
    field = 'doppler'

    # create lidar data object:
    dl_obj = DL_DATA_HANDLER()
    times, observations = dl_obj.get_DL_site_observations(field=field, site_facility=site_facility,
                                                          timespan=[start_time, end_time],
                                                          aggregate_data=False,  # <-- notice this flag
                                                          snr_threshold=False,
                                                          collect_remote_data=collect_remote_data
                                                         )

    # number of timepoints where observations are avialble
    num_obs = len(times)
    sepp = "\n%s\n" % ("*"*80)
    if num_obs == 0:
        print("%sNo observations found in the specified timespan:\n\t [ %s to %s ]" % (sepp, start_time, end_time))
        if not collect_remote_data:
            print("Consider turning 'collect_remote_data' on, i.e. set to 'True'\n")
        print(sepp)
        sys.exit()
    else:
        pass
    print("%sNumber of observaitons found is [ %d ]%s" % (sepp, num_obs, sepp))
    if screen_output:  # this branch, if enabled, will likely show lots of nans/missing-values
        # print data: Plotting should be considered next:
        num_obs = len(times)
        for i, t in enumerate(times):
            print("%s\n Observation [%4d / %4d] \n%s" % ('='*50, i, num_obs,'-'*25))
            print("Time = %s " % repr(t))
            print("DL-Data:")
            print(observations[i])  # <- we can show table of coordinates/values, but plotting is better!

    seq_valid_coords = []
    # print only available (not-nan) entries:
    for i, t in enumerate(times):
        valid_coords, valid_obs = observations[i].extract_valid_entries()
        seq_valid_coords.append(valid_coords)
        if screen_output:
            print("%s\n Observation [%4d / %4d] \n%s" % ('='*50, i, num_obs,'-'*25))
            print("Time = %s " % repr(t))
            print("DL-Data:")
            for c, o in zip(valid_coords, valid_obs):
                print("Coordinates: %s\t Observation [%s: %s]" % (c, field, o))


    # Get some statistics, and plot data availability; other ideas!
    min_time_delta = times[1]-times[0]
    time_deltas = []
    for i in range(1, num_obs):
        time_diff = times[i] - times[i-1]
        time_deltas.append(time_diff)
        if  time_diff < min_time_delta:
            min_time_delta = time_diff
    durations = [d.total_seconds() for d in time_deltas]

    #
    plots_enhancer()
    print("Creating data availability plots...")
    # Plot general data availability overtime (regardless of observational coordinates)
    fig = plt.figure(facecolor='white')
    title = "Lidar Data Availability over time regardless the coordinates\nInitial time %s" %str(times[0])
    fig.suptitle(title)
    ax1 = fig.add_subplot(111)
    ax1.plot(times[:-1], durations, 'b-d', linewidth=2, markersize=4)
    ax1.set_ylabel(r"$Time\, \delta$ (seconds)")
    ax1.set_xlabel("Time")

    # adjust xticklabels
    if False:
        ticks = []
        for i in ax1.get_xticks():
            if int(i)<len(times):
               ticks.append(int(i))
        ax1.set_xticks(ticks)
        tick_labels = [times[i] for i in ax1.get_xticks()]

        ax1.set_xticklabels(tick_labels)
        ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45)
    else:
        curr_ticks = ax1.get_xticks()
        l = min(len(curr_ticks), 10)
        xlims = ax1.get_xlim()
        tick_vals = np.linspace(xlims[0], xlims[-1], l, endpoint=True)
        tick_vals = [mdates.num2date(v) for v in tick_vals]
        tick_labels = [v.time().strftime('%H:%M:%S') for v in tick_vals]
        ax1.set_xticks(tick_vals)
        ax1.set_xticklabels(tick_labels, rotation=60)

    file_name = "Lidar_Data_Availability.pdf"
    plt.savefig(file_name, dpi=500, facecolor='w', format='PDF', transparent=True, bbox_inches='tight')
    print("File created: %s " % file_name)

    # 3D-Plot of data availablity over time (elevation/azimuth/time plot)
    azim_list = [c[0] for c in valid_coords]
    elev_list = [c[1] for c in valid_coords]

    # The next plot will take significantly long time to build and render; order
    fig = plt.figure(facecolor='white')
    ax2 = Axes3D(fig)
    for time_ind in range(num_obs):
        azim_list = [c[0] for c in seq_valid_coords[time_ind]]
        elev_list = [c[1] for c in seq_valid_coords[time_ind]]
        ax2.scatter(azim_list, elev_list, time_ind, c='b', marker='^')
    ax2.set_xlabel('Azimuth')
    ax2.set_ylabel('Elevation')
    ax2.set_zlabel('Time')

    title = "Lidar Data Availability over time\nInitial time %s" %str(times[0])
    fig.suptitle(title)

    # Set time labels
    if False:
        ticks = []
        for i in ax2.get_zticks():
            if int(i)<len(times):
                ticks.append(int(i))
        ax2.set_zticks(ticks)
        tick_labels = [times[i] for i in ax2.get_zticks()]
        ax2.set_zticklabels(tick_labels)
    else:
        curr_ticks = ax2.get_zticks()
        l = min(len(curr_ticks), 10)
        zlims = ax2.get_zlim()
        tick_range = np.linspace(0, len(times)-1, l, endpoint=True, dtype=int)
        tick_vals = utility.linear_translate(tick_range, tick_range[0], tick_range[-1], zlims[0], zlims[-1])
        spaced_times = np.linspace(mdates.date2num(times[0]), mdates.date2num(times[-1]), l, endpoint=True)
        tick_labels = [mdates.num2date(v).time().strftime('%H:%M:%S') for v in spaced_times]
        ax2.set_zticks(tick_vals)
        ax2.set_zticklabels(tick_labels)

    file_name = "Lidar_Data_Availability_3D.pdf"
    plt.savefig(file_name, dpi=500, facecolor='w', format='PDF', transparent=True, bbox_inches='tight')
    print("File created: %s " % file_name)

    if show_plots:
        plt.show()

    print("...Done...")
