#! /usr/bin/env python

"""
    This is a very Simple scrip to test the Filtering_Process class that iterates over a filter, e.g. EnKF with HyPar model.
    If MPI is initiated, model, fitler, and process are all initiated on all nodes in the communicator
"""

import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

from time import sleep, clock
import pickle
import shutil

import numpy as np

try:
    import mpi4py
    mpi4py.rc.recv_mprobe = False
    from mpi4py import MPI
    use_MPI = True
except(ImportError):
    use_MPI = False

# Initialize DATeS with default settings
if use_MPI:
    comm = MPI.COMM_WORLD
    my_rank = comm.rank
    comm_size = comm.size
else:
    comm = None
    my_rank = 0
    comm_size = 1

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

if False and use_MPI:
    comm.Barrier()


__LINER = "="*60
__FILE_OUTPUT_DIR = '../FilteringProcessLooper_Results/'


import DL_utility as utility

# import forward operator class:
from forward_model import ForwardOperator
from EnKF import DEnKF as KalmanFilter

from filtering_process import FilteringProcess


def seconds_to_str(t):
    """
    convert time (t) in seconds into a string "Hours: Minutes: Seconds.Microseconds"
    """
    time_str = "%d:%02d:%02d.%03d" % reduce(lambda ll,b : divmod(ll[0],b) + ll[1:], [(t*1000,),1000,60,60])
    return time_str


def _match_model_configs(config1, config2):
    """
    Match essential model configs, and return a binary flag
    The configuratios to compare are:
        1- 
        2- 
        3- 

    """
    compare_keys = ['size',
                    'nvars',
                    'ndims',
                    'domain_lower_bounds',
                    'domain_upper_bounds',
                    'boundary',
                    'thermal_source',
                    'physics']
    match = True
    for key in compare_keys:
        if config1[key] != config2[key]:
            # print("\n\n Configuration mismatch in key : %s\n\n" % key)
            # print(config1[key])
            # print(config2[key])
            match = False
            break
        else:
            pass
            # print("Key %s match yeeeeeeey!!!" % key)

    # print("****** Final match : %s " % match)
    return match


def generate_process_IC(model, load_from_file=True, file_path=None, repository=None, repo_config_basename='IC_config', timespan=None):
    """
    Generate an initial condition (true/reference state) for the assimilation process.
    This IC will be generated once, and written to file; after that, it will be re-used for future runs.
    The initial condition depends on the model configurations, so we need to match the model configurations with those of the initial condition before loading. Either we store the
    configurations withing the state file, or in a separate file; I'll follow the latter, i.e. I'll save the configuration dictionary with the IC, and upon loading the IC, I'll
    test for match. This will be helpful for testing multiple model setttings

    Args:
        model: model object, used to create state_vector object
        load_from_file: if False, an IC will be regenerated given the passed timespan, with IC generated at timespan[-1]
        file_path: The path to file from/to which the IC will be loaded/written;
            if None, the default is ./filteringProcessIC.dlvec
        repository: path to the directory that contains the initial conditions, along with corresponding configurations
        repo_config_basename: used only if repository is not None; this is the base name of configurations files
        timespan: used if the initial condition fails to be loaded from file.
            The standard HyPar initial solution is loaded, and propagated over timespan,
            the IC is the result at the upper bound of timespan. If None, the default is [0, 300], i.e. 5 minutes

    Returns:
        IC: the model state, to be used as IC

    """
    ic_base_filename = 'filteringProcessIC'
    model_configs = model.get_model_configs().copy()
    config_file_path = None
    #
    if file_path is None:
        if load_from_file and repository is not None:
            if not os.path.isdir(repository):
                os.makedirs(repository)
            # Look for the initial condition inside the given repository
            # Match configurations to get the initial condition filename
            trgt_filename = None
            config_files = utility.get_list_of_files(repository, recursive=False)
            proper_config_files = []
            for fname in config_files:
                _, fname = os.path.split(fname)
                if fname.startswith(repo_config_basename):
                    proper_config_files.append(fname)

            # print(config_files, proper_config_files)
            # Loop over proper configuration files
            for fname in proper_config_files:
                configs = pickle.load(open(os.path.join(repository, fname), 'rb'))
                if _match_model_configs(configs, model_configs):
                    # print("Match configs; found %s" % fname)
                    try:
                        trgt_filename = configs['IC_filename']
                    except KeyError:
                        pass
                    if trgt_filename is None:
                        continue
                    else:
                        break
            if trgt_filename is not None:
                file_path = os.path.join(repository, trgt_filename)
                # print("found exisiting matching configs file %s " % file_path)
            else:
                # print("NO matching configs in %s. Will create a new one " % repository)
                # Now, generate a filepath to save IC to:
                load_from_file = False  # to force file-saving
                filename = utility.try_file_name(repository, file_prefix=ic_base_filename, extension='dlvec', return_abspath=False)
                file_path = os.path.join(repository, filename)
                model_configs.update({'IC_filename':filename})
                config_file_path = utility.try_file_name(repository, file_prefix=repo_config_basename, return_abspath=True)
                #
            # print(">>>>> configurations file: %s " % config_file_path)
            # print(">>>>> ic filepath: %s " % file_path)

    else:
        # a specific filepath is given:
        this_dir = os.path.dirname(os.path.abspath(__file__))
        ic_filename = "%s.dlvec" % ic_base_filename
        file_path = os.path.join(this_dir, ic_filename)
        if not load_from_file:
            model_configs.update({'IC_filename':ic_filename})
            config_file_path = utility.try_file_name(this_dir, file_prefix=repo_config_basename, return_abspath=True)

    if load_from_file:
        if not os.path.isfile(file_path):
            IC = None
            location, ic_filename = os.path.split(file_path)
            model_configs.update({'IC_filename':ic_filename})
            config_file_path = utility.try_file_name(location, file_prefix=repo_config_basename, return_abspath=True)
            save_to_file = True
            # print("IC file '%s' doesn't exist" % file_path)
            # print("The IC will be created and saved to file for later use")
        else:
            try:
                # try to load from file, check size, etc., and then assign to state_vector entries
                print("Loading Filtering Process IC from file..."),
                sys.stdout.flush()
                IC = model.state_vector(data_file=file_path)
                save_to_file = False
                print("done...")
            except (ValueError, IOError):
                print("\nFailed to load the IC from file; Will recreate, and save for later use")
                IC = None
                location, ic_filename = os.path.split(file_path)
                model_configs.update({'IC_filename':ic_filename})
                config_file_path = utility.try_file_name(location, file_prefix=repo_config_basename, return_abspath=True)
                save_to_file = True
    else:
        IC = None
        save_to_file = True

    if IC is None:
        # Create IC
        print("Creating the Process IC for the first time")
        if timespan is None:
            timespan = [0, 300]
        else:
            assert len(timespan) >= 2, "The timespan must be an iterable of length >= 2"
            timespan = [timespan[0], timespan[-1]]
        IC = model.current_model_state()  # Reference initial solution,
        _, traject = model.integrate_state(IC, checkpoints=timespan)
        IC = traject[-1].copy()

    if save_to_file:
        print("Writitng Process IC to file %s ..." % file_path),
        sys.stdout.flush()
        # write the configurations first:
        if config_file_path is not None:
            print("Saving configurations file into %s " % config_file_path)
            with open(config_file_path, 'wb') as f_id:
                pickle.dump(model_configs, f_id)
        print("Saving initial condition in %s " % file_path)
        IC.write_to_file(file_path)
        print("done...")
    return IC


def generate_process_initial_ensemble(model, load_from_file=True, repo_dir=None, filename_prefix='initial_ensemble_', timespan=None):
    """
    Generate an initial ensemble for the assimilation process.
    This IC will be generated once, and written to file; after that, it will be re-used for future runs.

    Args:
        model: model object, used to create state_vector object
        load_from_file: if False, an IC will be regenerated given the passed timespan, with IC generated at timespan[-1]
        repo_dir: The path under/to which the initial_ensemble files will be loaded/written;
            if None, the default is ./filteringProcessIC.dlvec
        timespan: used if the initial ensemble fails to be loaded from file.
            The standard HyPar initial solution is loaded, and propagated over timespan, to generate a trajectory,
            the IC is the result at the upper bound of timespan. If None, the default is [0:10:300], i.e. 5 minutes

    Returns:
        ensemble: 

    """
    raise NotImplementedError("TODO")
    # return ensemble


def start_filtering_process(apply_filtering=True,
                            filter_ensemble_size=None,
                            filter_localization_function=None,
                            filter_localization_radius=None,
                            filter_forecast_inflation_factor=None,
                            filter_analysis_inflation_factor=None,
                            #
                            model_domain_upper_bounds=None,
                            model_size=None,
                            model_iproc=None,
                            model_thermal_source_center=None,
                            model_rcent=None,
                            model_T_diff=None,
                            obs_doppler_lidar_num_gates=None,
                            obs_doppler_lidar_elevations=None,
                            obs_doppler_lidar_azimuthes=None,
                            #
                            create_plots=False,
                            overwrite_results=False,
                            #
                            results_dir=None
                           ):
    """
    """
    if not (apply_filtering or create_plots):
        # Nothing to do
        print("You don't need to apply the filtering process, nor to create plots, e.g. for existing results directory!\nNothing to be done!")
        return

    # Validate results directory
    if results_dir is None:
        results_dir = os.path.join(__FILE_OUTPUT_DIR, "Filtering_Results")
    else:
        if __FILE_OUTPUT_DIR in results_dir:
            pass
        else:
            results_dir = os.path.join(__FILE_OUTPUT_DIR, results_dir)
    results_dir = os.path.abspath(results_dir)
    #

    # print("%s\n%s\n%s" % (__LINER, __LINER, __LINER))
    print("\n%s" % __LINER)
    print(">>> Starting a New Experiment")
    print("Target results to be saved in:\n  >> %s" % results_dir)
    print("%s" % (__LINER))

    if os.path.isdir(results_dir):
        if not overwrite_results:
            print("Experiment results exist, and overwrite is set to False. Passing. ")
            return
        else:
            # cleanup
            print("[[[***WARNING***]]]: I'll REMOVE THIS DIRECTORY! Press CTRL+Z if you ran this by mistake, and want to stop me")
            seconds = 9
            rem_sec = 10
            for i in range(seconds):
                print("\r%03d seconds to resume" % rem_sec),
                sys.stdout.flush()
                rem_sec -= 1
                sleep(1)
                #
            shutil.rmtree(results_dir)
            #
    else:
        print("\n\n\n <<< Results dir not Found. Will be created...")
        pass

    # ======================================================================================== #
    #                                         Settings                                         #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #  This is where you can play with experiment and plotting settins                         #
    #  You only need to play with settings here.                                               #
    # ======================================================================================== #

    # ---------------------------------------------------------------------------------------- #
    # 1- Hypar Model Settings:
    # ---------------------------------------------------------------------------------------- #

    # i   ) domain size in the x, y, z direction (i.e. Nx, Ny, Nz)
    _size = model_size if model_size is not None else [51, 51, 31]

    # ii  ) grid limits (lower and upper limits the grid in each direction)
    _domain_lower_bounds = [0, 0, 0]
    _domain_upper_bounds = model_domain_upper_bounds if model_domain_upper_bounds is not None else [25000, 25000, 5000]

    # iii ) Number of process in each direction
    _iproc = model_iproc if model_iproc is not None else [4, 4, 2]
    # iv  ) number of ghost points:
    _ghost = 3
    # v   ) (Maximum) time step of the time-integration scheme
    _dt = 0.2
    # vi  ) timestepping method and PETSc Runtime options
    _ts            = 'rk'
    _ts_type       = 'ssprk3'
    _hyp_scheme    = 'weno5'  # TODO: I haven't involved this yet; will be ignored for now
    _PETSc_options = '-ts_type rk -ts_rk_type 4'
    # vii ) Other hypar-specific settings
    _hyp_flux_split = 'no'
    _hyp_int_type   = 'components'
    _par_type       = 'none'
    _par_scheme     = 'none'
    # viii) Physics Settings:
    _gamma     = 1.4
    _upwinding = "rusanov"
    _gravity   = [0.0, 0.0, 9.8]
    _rho_ref   = 1.1612055171196529
    _p_ref     = 100000.0
    _R         = 287.058
    _HB        = 2

    # ix  ) Boundary Settings
    # 6 sides of the boundary x-lower, x-upper, y-lower, y-upper, z-lower, z-upper; see example below
    _faces_types = ['periodic',             # x-lower
                    'periodic',             # x-upper
                    'periodic',             # y-lower
                    'periodic',             # y-upper
                    'thermal-noslip-wall',     # z-lower
                    'extrapolate'              # z-upper
                   ]
    # boundary settings for each of the 6 sides. Put None if neither slip nor thermal in the side type
    _faces_settings=[None,
                     None,
                     None,
                     None,
                     (0.0, 0.0, 0.0, 'temperature_data.dat'),
                     None
                    ]
    # Example of boundary settings:
    # faces_types=['periodic', 'periodic', 'periodic', 'periodic', 'thermal-slip-wall', 'slip-wall'],
    # faces_settings=[None, None, None, None, (0.0, 0.0, 0.0, 'temperature_data.dat'), (0.0, 0.0, 0.0)]

    # x   ) Thermal source settings; used for any wall having 'thermal' in its face_type value
    #       Now, you can create multiple sources, by passing iterables instead of scalars for x,y,z,r
    _thermal_shape = 'multiple-discs'  # this is more general than disc; Py_HyPar.pyx will be refactored accordingly
    if model_thermal_source_center is not None:
        _thermal_source_center = model_thermal_source_center
    else:
        _thermal_source_center = [[2500,  2500, 10000, 12500, 22500],
                                  [17500, 4000, 10000, 12500, 20000],
                                  [500]]  # x-y-z coordinates of the the thermal source; scalars and 1d iterables  are replicated
    _rcent = model_rcent if model_rcent is not None else [1500, 1500, 500, 1500, 1000]  # radius/radii (for multiple-discs shape) of the thermal spot
    _T_diff = model_T_diff if model_T_diff is not None else  [10, 10, 15, 10, 20]  # Temperature (K) differnece used to create thermal spot

    # xi  )

    # ---------------------------------------------------------------------------------------- #
    # 2- Doppler Lidar Settings:
    # ---------------------------------------------------------------------------------------- #
    _doppler_lidar_num_gates = obs_doppler_lidar_num_gates if obs_doppler_lidar_num_gates is not None else 100
    _doppler_lidar_range_gate_length = 30
    _doppler_lidar_elevations = obs_doppler_lidar_elevations if obs_doppler_lidar_elevations is not None else [45, 90]
    _doppler_lidar_azimuthes = obs_doppler_lidar_azimuthes if obs_doppler_lidar_azimuthes is not None else [0, 45, 90, 135, 180, 225, 270, 315]

    # ---------------------------------------------------------------------------------------- #
    # 3- Experiment Settings:
    # ---------------------------------------------------------------------------------------- #
    start_time = '2017:08:13:00:00:00'
    end_time   = '2017:08:13:00:05:10'
    delta_time = '0000:00:00:00:00:05'

    prtrb_initial_ensemble = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                                   END OF Settings                                        #
    # ======================================================================================== #

    # Prepre the model and plot things
    # model configureations
    HyPar_model_configs = {'size':                _size,
                         'domain_upper_bounds': _domain_upper_bounds,
                         'domain_lower_bounds': _domain_lower_bounds,
                         'iproc':               _iproc,
                         'dt':                  _dt,
                         'PETSc_options':       _PETSc_options,
                         'init_time':           0.0,  # (optional) time of the initial state (default is zero)
                         'ghost':               _ghost,
                         'rest_iter':           0,
                         'ts':                  _ts,
                         'ts_type':             _ts_type,
                         'hyp_scheme':          _hyp_scheme,
                         'hyp_flux_split':      _hyp_flux_split,
                         'hyp_int_type':        _hyp_int_type,
                         'par_type':            _par_type,
                         'par_scheme':          _par_scheme,
                         'cleanup_dir':False}
    # Physics
    ph_dict = dict(gamma     = _gamma,
                   upwinding = _upwinding,
                   gravity   = _gravity,
                   rho_ref   = _rho_ref,
                   p_ref     = _p_ref,
                   R         = _R,
                   HB        = _HB)
    HyPar_model_configs.update({'physics':ph_dict})
    # Boundary
    b_dict = dict(boundary=dict(num_faces=len(_faces_types),
                                faces_types=_faces_types,
                                faces_settings=_faces_settings),
                  thermal_source=dict(shape=_thermal_shape,
                                      xcent= _thermal_source_center[0],
                                      ycent= _thermal_source_center[1],
                                      zcent= _thermal_source_center[2],
                                      rcent=_rcent,
                                      T_diff=_T_diff))
    HyPar_model_configs.update(b_dict)

    # ====================================================================================== #
    #                                    GENERAL SETTINGS                                    #
    # ====================================================================================== #
    model_dist_unit = 'M'  # model grid distance usit 'M' for meters, 'KM' for Kilometers
    use_real_observations = False  # True/False  --> real/synthetic Observations
    if filter_ensemble_size is None:
        filter_ensemble_size = 50
    if filter_localization_function is None:
        filter_localization_function = 'Gaspari-Cohn'
    if filter_localization_radius is None:
        filter_localization_radius = 500  # loc radius im dist_unit
    if filter_forecast_inflation_factor is None:
        filter_forecast_inflation_factor = 1.0
    if filter_analysis_inflation_factor is None:
        filter_analysis_inflation_factor = 1.05
    verbose = False
    random_seed = 2345  # passed to the random number generator used; numpy here
    #
    # ====================================================================================== #


    # ====================================================================================== #
    #                                 Model and Observation                                  #
    # ====================================================================================== #



    # Create Checkpoints for observation and assimilation:
    experiment_timespan = utility.create_timespan(start_time=start_time,
                                                  end_time=end_time,
                                                  delta_time=delta_time,
                                                  return_string=False)

    scalar_experiment_timespan = utility.timespan_to_scalars(experiment_timespan)

    t0 = scalar_experiment_timespan[0]

    site_facility = 'sgpdlC1'                     # sgpdlE32, sgpdlC1, etc.
    field         = 'wind-velocity'               # 'radial_velocity', 'doppler', 'wind-velocity', etc

    DLiDA_obs_configs = {'site_facility':site_facility,
                         't':t0,
                         'dl_coordinates': '0,0,0',
                         'prog_var': field,
                         'num_gates': _doppler_lidar_num_gates,
                         'range_gate_length': _doppler_lidar_range_gate_length,
                         'elevations': _doppler_lidar_elevations,
                         'azimuthes': _doppler_lidar_azimuthes}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Forward Operator)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #
    # Forward Operator: Dynamics+Observations; using HyPar, and DL-Data settings
    model_configs = HyPar_model_configs
    obs_configs = DLiDA_obs_configs
    model = ForwardOperator(model_configs, obs_configs, dist_unit=model_dist_unit)  # model distance M/KM

    # Propagate the initial state, and use it as a reference initial condition
    if my_rank == 0:  # it will be broadcasted in the filter initiation
        # model.integrate_state(checkpoints=[0, 5])
        # xtrue = model.current_model_state()  # Reference initial solution,
        xtrue = generate_process_IC(model, load_from_file=True, repository='FilteringProcess_ICRepo')
        # xtrue = generate_process_IC(model, load_from_file=True, file_path=None, timespan=None)
        xtrue.time = t0
    else:
        xtrue = None
    if comm is not None:
        xtrue = comm.bcast(xtrue, root=0)
        if my_rank == 0:
            print("Done broadcasting reference solution")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~(Synthetic or Real Observations)~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


    if apply_filtering:

        #
        # Genrate real or synthetic observations
        if use_real_observations:
            # collect real observations...
            if my_rank == 0:  # it will be broadcasted in the filter initiation
                _, observations = model.get_real_observations(experiment_timespan)

        else:
            if True:  # testing...
                observations = None
            else:
                # No need to create synthetic observations here; filtering process will do it on the fly
                obs_err_model = model._obs_err_model
                if my_rank == 0:
                    observations = [obs_err_model.add_noise(model.evaluate_theoretical_observation(xtrue))]
                    traject = [xtrue.copy()]  # no need to copy!
                    #
                    for t0, t1 in zip(experiment_timespan[:-1], experiment_timespan[1:]):
                        _, traject = model.integrate_state(traject[-1], checkpoints=[t0, t1])
                        ytrue = model.evaluate_theoretical_observation(traject[-1])
                        observations.append(obs_err_model.add_noise(ytrue, in_place=False))
                    del traject

        #
        # ====================================================================================== #


        # ====================================================================================== #
        #                                     Assimilation                                       #
        # ====================================================================================== #

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Filter Object/Cycle)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
        #
        # Generate perturbed initial forecast state, and initial ensemble (real method needed for real observations!)
        ensemble_size = filter_ensemble_size
        # if my_rank == 0:
        # else:
        #     init_ensemble = None

        if my_rank == 0:
            if prtrb_initial_ensemble:
                xf = model._frcst_err_model.add_noise(xtrue, in_place=False)
                xf.time = t0
                init_ensemble = model.create_state_ensemble(0, fill_ensemble=False, t=t0)
                for i in range(ensemble_size):
                    member = model._frcst_err_model.add_noise(xf, in_place=False)  # The noise is not normalized
                    member.time = t0  # TODO: Check; do we need it?:
                    init_ensemble.append(member)
                del xf
                del member
            else:
                init_ensemble = model.create_state_ensemble(ensemble_size, fill_ensemble=True, t=t0)  # get empty ensemble
        else:
            init_ensemble = None

        # Create the filter object:
        filter_configs={'model': model,
                        'MPI_COMM':comm,
                        'ensemble_size':filter_ensemble_size,
                        'localize_covariances':True,
                        'localization_radius':filter_localization_radius,
                        'localization_function':filter_localization_function,
                        'forecast_inflation_factor':filter_forecast_inflation_factor,
                        'analysis_inflation_factor':filter_analysis_inflation_factor,
                       }
        filter_output_configs = {'file_output_moment_only':False}
        filter_obj = KalmanFilter(filter_configs=filter_configs, output_configs=filter_output_configs)


        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Assimiltion Process)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
        #
        # Observation time setting: (Create proper timespan)
        obs_checkpoints = da_checkpoints = experiment_timespan[1: ]
        assimilation_configs = {'filter':filter_obj,
                                'MPI_COMM':comm,
                                'obs_checkpoints':obs_checkpoints,
                                'da_checkpoints':da_checkpoints,
                                'ref_initial_condition':xtrue,
                                'ref_initial_time':experiment_timespan[0],
                                'initial_ensemble':init_ensemble,
                                'forecast_first':True,
                                'random_seed':random_seed}
        assim_output_configs = {'file_output_dir':results_dir,
                                'scr_output':True,
                                'scr_output_iter':1,
                                'file_output':True,
                                'file_output_iter':1,
                                'verbose':False,}
        assim_experiment = FilteringProcess(assimilation_configs=assimilation_configs,
                                            output_configs=assim_output_configs)

        # Iterate over the experiment_timespan, and apply filtering at each cycle
        assim_experiment.start_assimilation_process()

    if create_plots:
        slice_indexes = [_size[0]/2, _size[1]/2, min(5, _size[2])]
        # Plot the results in the corresponding directory:
        print("\n%s\nPlotting Results\n%s\n" % (__LINER, __LINER))
        output_dir_structure_file = os.path.join('../', results_dir, 'output_dir_structure.txt')
        cmd = "python filtering_results_reader.py %s '%s'" % (output_dir_structure_file, slice_indexes)
        print("Running cmd: \n%s" % cmd)
        os.system(cmd)

        print("%s\n\t...DONE...%s\n%s\n" % (__LINER, __LINER, __LINER))
    #
    # ====================================================================================== #
    #                                     <<<<DONE>>>>                                       #
    # ====================================================================================== #
    #


def resolution_looper():
    """
        # Loop over multiple settings, and save the results
        # Need to test different values of:
        # 1- _domain_upper_bounds 
        #    >> _size
        #    >> _iproc
        #    >> _thermal_source_center
        #    >> _rcent
        #    >> _T_diff

        # 2- _doppler_lidar_num_gates         = 100
        # 3- _doppler_lidar_elevations        = [45, 90]
        # 4- _doppler_lidar_azimuthes         = [0, 90, 180, 270]

        # 
        # Create list of stackabale settings, to try together; corresponding entries are used for each experiment
        # A) Experiment 1: Base experiment 25KM x 25KM x 5KM with observations at 45, 90 elevations, and 4 azimuthes, and 100 gates of range 30 meters
        # B) Experiment 2: Increase observation resolution vs. base experiment
        # C) Experiment 3: Decrease model grid resolution vs. base experiment (and compare the effect to experiment 2)
        # D) Experiment 4: Increase number of range gates only vs. base experiment, same space resolution
        # E) Experiment 5: Decrease observation resolution vs. base experiment
        # F) Experiment 6: Increase model grid resolution vs. base experiment (and compare the effect to experiment 2)
    """
    _domain_upper_boundsZ = [[25000, 25000, 5000],
                             [25000, 25000, 5000],
                             [25000, 25000, 5000],
                             [25000, 25000, 5000],
                             [25000, 25000, 5000],
                             [25000, 25000, 5000],
                            ]
    _sizeZ  = [[51, 51, 31],
               [51, 51, 31],
               [26, 26, 16],
               [51, 51, 31],
               [51, 51, 31],
               [101, 101, 61],
              ]
    _iprocZ = [[4, 4, 2],
               [4, 4, 2],
               [2, 2, 1],
               [4, 4, 2],
               [4, 4, 2],
               [4, 4, 2],
              ]
    _thermal_source_centerZ = [[[2500,  2500, 10000, 12500, 22500], [17500, 4000, 10000, 12500, 20000],[500]],
                               [[2500,  2500, 10000, 12500, 22500], [17500, 4000, 10000, 12500, 20000],[500]],
                               [[2500,  2500, 10000, 12500, 22500], [17500, 4000, 10000, 12500, 20000],[500]],
                               [[2500,  2500, 10000, 12500, 22500], [17500, 4000, 10000, 12500, 20000],[500]],
                               [[2500,  2500, 10000, 12500, 22500], [17500, 4000, 10000, 12500, 20000],[500]],
                               [[2500,  2500, 10000, 12500, 22500], [17500, 4000, 10000, 12500, 20000],[500]],
                              ]
    _rcentZ = [[1500, 1500, 500, 1500, 1000],
               [1500, 1500, 500, 1500, 1000],
               [1500, 1500, 500, 1500, 1000],
               [1500, 1500, 500, 1500, 1000],
               [1500, 1500, 500, 1500, 1000],
               [1500, 1500, 500, 1500, 1000],
              ]
    _T_diffZ = [[10, 10, 15, 10, 20],
                [10, 10, 15, 10, 20],
                [10, 10, 15, 10, 20],
                [10, 10, 15, 10, 20],
                [10, 10, 15, 10, 20],
                [10, 10, 15, 10, 20],
               ]

    _doppler_lidar_num_gatesZ = [100,
                                 100,
                                 100,
                                 150,
                                 100,
                                 100,
                                ]
    _doppler_lidar_elevationsZ = [[45, 90],
                                  [30, 60, 90],
                                  [45, 90],
                                  [45, 90],
                                  [90],
                                  [45, 90],
                                 ]
    _doppler_lidar_azimuthesZ = [[0, 90, 180, 270],
                                 [0, 45, 90, 135, 180, 225, 270, 315],
                                 [0, 90, 180, 270],
                                 [0, 90, 180, 270],
                                 [90],
                                 [0, 90, 180, 270],
                                ]

    for exp_ind, _domain_upper_bounds, _size, _iproc, _thermal_source_center, _rcent, \
            _T_diff, _doppler_lidar_num_gates, _doppler_lidar_elevations, \
            _doppler_lidar_azimuthes in zip(range(len(_sizeZ)),
                                            _domain_upper_boundsZ,
                                            _sizeZ,
                                            _iprocZ,
                                            _thermal_source_centerZ,
                                            _rcentZ,
                                            _T_diffZ,
                                            _doppler_lidar_num_gatesZ,
                                            _doppler_lidar_elevationsZ,
                                            _doppler_lidar_azimuthesZ):

        # Start the filtering process given these settings:
        start_filtering_process(apply_filtering=True,
                                model_domain_upper_bounds=_domain_upper_bounds,
                                model_size=_size,
                                model_iproc=_iproc,
                                model_thermal_source_center=_thermal_source_center,
                                model_rcent=_rcent,
                                model_T_diff=_T_diff,
                                obs_doppler_lidar_num_gates=_doppler_lidar_num_gates,
                                obs_doppler_lidar_elevations=_doppler_lidar_elevations,
                                obs_doppler_lidar_azimuthes=_doppler_lidar_azimuthes,
                                create_plots=True)


def inflation_localization_looper(localization_radius_pool=[50, 100, 250, 500, 1000, 2000],
                                  localization_function_pool=['Gauss', 'Gaspari-Cohn'],
                                  inflation_factor_pool=np.arange(1, 1.5, 0.1),
                                  ensemble_size_pool=[50, 25, 75, 100],
                                  overwrite_results=False  # discard existing experiment results
                                 ):
    """
    ensemble_size is either a scalar, or an iterable of multiple ensemble sizes to try
    """
    if np.isscalar(ensemble_size_pool):
        ensemble_size_pool = [ensemble_size_pool]
    else:
        ensemble_size_pool = [n for n in ensemble_size_pool]

    num_of_experiments = len(ensemble_size_pool)*len(localization_function_pool)*len(localization_radius_pool)*len(inflation_factor_pool)*2
    #
    sep = "*"*80
    exp_ind = 1

    total_time_elapsed = 0
    avg_time_elapsed = 0
    #
    for ensemble_size in ensemble_size_pool:
        for localization_function in localization_function_pool:
            for localization_radius in localization_radius_pool:
                for inflation_factor in inflation_factor_pool:
                    for inflation_flag in [0, 1]:  # inflation to forecast or analysis ensemble
                        #
                        print("\n%s\n\t\tRunning Experiment[%03d/%03d]\n%s\n " % (sep , exp_ind, num_of_experiments, '-'*80))
                        print("Total Time Elapsed: %s " % seconds_to_str(total_time_elapsed))
                        print("Expected Total Time Remaining: %s " % seconds_to_str(avg_time_elapsed*(num_of_experiments-exp_ind)) )
                        print("Average time per experiment: %s " % seconds_to_str(avg_time_elapsed) )
                        print("\n%s\n " % sep)
                        #
                        #
                        if inflation_flag:
                            forecast_inflation_factor = 1.0
                            analysis_inflation_factor = inflation_factor
                        else:
                            forecast_inflation_factor = inflation_factor
                            analysis_inflation_factor = 1.0
                        results_dir = "Varying_Inflation_Localization_EnsembleSize/EnsSize_%d_FrcstInfl_%f_AnlInfl_%f_LocFunc_%s_LocRad_%d" %(ensemble_size,
                                                                                                                                              forecast_inflation_factor,
                                                                                                                                              analysis_inflation_factor,
                                                                                                                                              localization_function,
                                                                                                                                              localization_radius)
                        #
                        # Start the assimilation process, and keep track of timing
                        start = clock()  # get wall time
                        start_filtering_process(apply_filtering=True,
                                                create_plots=True,
                                                filter_ensemble_size=ensemble_size,
                                                filter_localization_function=localization_function,
                                                filter_localization_radius=localization_radius,
                                                filter_forecast_inflation_factor=forecast_inflation_factor,
                                                filter_analysis_inflation_factor=analysis_inflation_factor,
                                                results_dir=results_dir,
                                                overwrite_results=overwrite_results
                                               )
                        #
                        # Update Time Estimate, and update experiment index:
                        end = clock()  # get wall time
                        time_elapsed = end - start
                        total_time_elapsed += time_elapsed
                        # avg_time_elapsed = (avg_time_elapsed*(exp_ind-1) + time_elapsed) / exp_ind
                        avg_time_elapsed = total_time_elapsed / exp_ind
                        exp_ind += 1
                        #

if __name__ == '__main__':

    # =====================================================================
    # Settings:
    # =====================================================================
    test_resolution = False
    test_inflation_localization = True
    overwrite_results = False
    # =====================================================================


    # ---------------------------------------------------------------------
    # 1- Looper for resolution
    if test_resolution:
        resolution_looper(ensemble_size=50)
        #

    # 2- Looper for inflation/localization (benchmarking)
    if test_inflation_localization:
        # Test inflation/localization/ensemble_size
        inflation_localization_looper(overwrite_results=overwrite_results)
        #
    # ---------------------------------------------------------------------
