#! /usr/bin/env python

"""
This script loops over 'doppler_lidar_vertical_ovservation_plotter' functionality,
and create plots for multiple site facilities, and time intervals, etc.
"""

from doppler_lidar_vertical_ovservation_plotter import *


#
if __name__ == '__main__':

    # ====================================================================================== #
    #                              Settings & DATA Collection                                #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # --------------------------------------(SETTINGS)-------------------------------------- #
    #

    # Sync data with remote date repo? this gets all matching files from mcs.anl.gov repo if not available on this machine
    collect_remote_data = True

    # Observation time setting: (Create proper timespan)
    start_time = '2017:08:13:00:00:00'
    end_time   = '2017:08:13:23:59:59'
    delta_time_pool = ['0000:00:00:01:00:00', '0000:00:00:00:45:00', '0000:00:00:00:30:00',
                       '0000:00:00:00:15:00', '0000:00:00:00:10:00', '0000:00:00:00:05:00',
                       '0000:00:00:00:01:00', '0000:00:00:00:00:30', '0000:00:00:00:00:15'
                       ]

    # Specify setings/filters for obsrvations to collect
    site_facility_pool = ['sgpdlC1', 'sgpdlE32']
    field_pool = ['doppler', 'radial_velocity', 'wind-speed']
    scan_type     = 'stare'                         # 'stare'/'vad'; if vad, must provide vad_scan_degree(default 60); also the plotting down should be updated for multiple vad degrees

    # --------------------------(Data collection and Plotting)------------------------------ #

    # Loop over all experiments
    for site_facility in site_facility_pool:
        for field in field_pool:
            for delta_time in delta_time_pool:
                for filter_observations in [False, True]:  # statistics for raw vs. filtered data
                    #
                    out_dir_name = "__PLOTS_filtered_%s__%s_%s__%s" %(filter_observations, site_facility, field, '_'.join(delta_time.split(':')))
                    print(" >> Experiment: %s\n" % out_dir_name)
                    read_and_plot(start_time=start_time,
                                  end_time=end_time,
                                  delta_time=delta_time,
                                  site_facility=site_facility,
                                  field=field,
                                  scan_type=scan_type,
                                  filter_observations=filter_observations,
                                  out_dir_name=out_dir_name,
                                  collect_data_only=False,
                                  collect_remote_data=collect_remote_data
                                  )  # Collect only remotely; will plot later on my machine


    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                              Results Plotting complete                                 #
    # ====================================================================================== #
