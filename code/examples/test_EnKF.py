#! /usr/bin/env python3

"""
    This is a very Simple scrip to test EnKF with  Hypar model, and synthetic DL-observations.
"""

import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import numpy as np

try:
    import mpi4py
    mpi4py.rc.recv_mprobe = False
    from mpi4py import MPI
    use_MPI = True
except(ImportError):
    use_MPI = False

# Initialize DATeS with default settings
if use_MPI:
    COMM = MPI.COMM_WORLD
    my_rank = COMM.rank
    comm_size = COMM.size
else:
    my_rank = 0
    comm_size = 1

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

if False and use_MPI:
    COMM.Barrier()

# import forward operator class:
from forward_model import ForwardOperator
from EnKF import DEnKF as KalmanFilter


if __name__ == '__main__':

    # ======================================================================================== #
    #                                         Settings                                         #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #  This is where you can play with experiment and plotting settins                         #
    #  You only need to play with settings here.                                               #
    # ======================================================================================== #

    # ---------------------------------------------------------------------------------------- #
    # 1- Hypar Model Settings:
    # ---------------------------------------------------------------------------------------- #

    # i   ) domain size in the x, y, z direction (i.e. Nx, Ny, Nz)
    _size = [101, 101, 41]

    # ii  ) grid limits (lower and upper limits the grid in each direction)
    _domain_upper_bounds = [50000, 50000, 4000]
    _domain_lower_bounds = [0, 0, 0]

    # iii ) Number of process in each direction
    _iproc = [4, 4, 1]
    # iv  ) number of ghost points:
    _ghost = 3
    # v   ) (Maximum) time step of the time-integration scheme
    _dt = 0.10
    # vi  ) timestepping method and PETSc Runtime options
    _ts            = 'rk'
    _ts_type       = 'ssprk3'
    _hyp_scheme    = 'weno5'  # TODO: I haven't involved this yet; will be ignored for now
    _PETSc_options = '-ts_type rk -ts_rk_type 4'
    # vii ) Other hypar-specific settings
    _hyp_flux_split = 'no'
    _hyp_int_type   = 'components'
    _par_type       = 'none'
    _par_scheme     = 'none'
    # viii) Physics Settings:
    _gamma     = 1.4
    _upwinding = "rusanov"
    _gravity   = [0.0, 0.0, 9.8]
    _rho_ref   = 1.1612055171196529
    _p_ref     = 100000.0
    _R         = 287.058
    _HB        = 2

    # ix  ) Boundary Settings
    # 6 sides of the boundary x-lower, x-upper, y-lower, y-upper, z-lower, z-upper; see example below
    _faces_types = ['slip-wall',             # x-lower
                    'slip-wall',             # x-upper
                    'slip-wall',             # y-lower
                    'slip-wall',             # y-upper
                    'thermal-slip-wall',     # z-lower
                    'slip-wall'              # z-upper
                   ]
    # boundary settings for each of the 6 sides. Put None if neither slip nor thermal in the side type
    _faces_settings=[(0.0, 0.0, 0.0),
                     (0.0, 0.0, 0.0),
                     (0.0, 0.0, 0.0),
                     (0.0, 0.0, 0.0),
                     (0.0, 0.0, 0.0, 'temperature_data.dat'),
                     (0.0, 0.0, 0.0)
                    ]
    # Example of boundary settings:
    # faces_types=['periodic', 'periodic', 'periodic', 'periodic', 'thermal-slip-wall', 'slip-wall'],
    # faces_settings=[None, None, None, None, (0.0, 0.0, 0.0, 'temperature_data.dat'), (0.0, 0.0, 0.0)]

    # x   ) Thermal source settings; used for any wall having 'thermal' in its face_type value
    _thermal_shape = 'disc'
    _thermal_source_center = [25000, 25000, 2000]  # x-y-z coordinates of the the thermal source
    _rcent = 3000  # radius of the (for disc shape) of the thermal spot
    _T_diff = 10  # time differnece used to create thermal spot
    #

    # ---------------------------------------------------------------------------------------- #
    # 2- Observation settings
    # ---------------------------------------------------------------------------------------- #
    _site_facility = 'sgpdlE32'
    _prog_var = 'wind-velocity'
    _num_gates = 30
    _range_gate_length = 30
    _elevations = [90]
    _azimuthes = [90]

    # ---------------------------------------------------------------------------------------- #
    # 3- Experiment Settings:
    # ---------------------------------------------------------------------------------------- #
    # time points (seconds) at which solution is checkpointed/saved and plotted
    _start_time = 0.0     # initial time
    _final_time = 60.0    # final time
    _delta_time = 0.5     # time between consecutive checkpoints/assimilation times

    # EnKF ensemble size
    ensemble_size = 50

    # ---------------------------------------------------------------------------------------- #
    # 4- Pltting Settings:
    # ---------------------------------------------------------------------------------------- #
    slices_index = [50, 50, 1]  # integr (0<=index<N), or list of integers, giving  index(s) of the slicing grid poins in each direction

    quiver_mask_size = None # skip every (mask_size-1) grid points in etach direction for 3D quiver; Make None if you don't want quiver plot
    overwrite_existing_plots = True
    plots_directory = '__RESULTS_and_PLOTS'     # Name of the directory to checkpoint and plot

    save_model_states = False # write model states to files

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                                   END OF Settings                                        #
    # ======================================================================================== #

    # Prepre the model and plot things
    # model configureations
    model_configs = {'size':                _size,
                     'domain_upper_bounds': _domain_upper_bounds,
                     'domain_lower_bounds': _domain_lower_bounds,
                     'iproc':               _iproc,
                     'dt':                  _dt,
                     'PETSc_options':       _PETSc_options,
                     'init_time':           0.0,  # (optional) time of the initial state (default is zero)
                     'ghost':               _ghost,
                     'rest_iter':           0,
                     'ts':                  _ts,
                     'ts_type':             _ts_type,
                     'hyp_scheme':          _hyp_scheme,
                     'hyp_flux_split':      _hyp_flux_split,
                     'hyp_int_type':        _hyp_int_type,
                     'par_type':            _par_type,
                     'par_scheme':          _par_scheme,
                     'cleanup_dir':False}
    # Physics
    ph_dict = dict(gamma     = _gamma,
                   upwinding = _upwinding,
                   gravity   = _gravity,
                   rho_ref   = _rho_ref,
                   p_ref     = _p_ref,
                   R         = _R,
                   HB        = _HB)
    # Boundary
    b_dict = dict(boundary=dict(num_faces=len(_faces_types),
                                faces_types=_faces_types,
                                faces_settings=_faces_settings),
                  thermal_source=dict(shape=_thermal_shape,
                                      xcent= _thermal_source_center[0],
                                      ycent= _thermal_source_center[1],
                                      zcent= _thermal_source_center[2],
                                      rcent=_rcent,
                                      T_diff=_T_diff))
    # create the model:

    # Observations Settings (passed to DL_obs):  # Create very sparse one!  # observation distances in meters
    obs_configs = {'site_facility':     _site_facility,
                   't':                 0,
                   'dl_coordinates':    '0,0,0',
                   'prog_var':          _prog_var,
                   'num_gates':         _num_gates,
                   'range_gate_length': _range_gate_length,
                   'elevations':        _elevations,
                   'azimuthes':         _azimuthes
                   }


    # create forward operator (dynamical model + observations):
    model = ForwardOperator(model_configs, obs_configs, dist_unit='M')
    # print('model_grid', model.model_grid())
    # print('obs_grid', model.observation_grid())
    obs_err_model = model._obs_err_model


    # timespan
    tspan = [0, 0.1]

    if my_rank == 0:
        # Create Reference state, and observation:
        x0 = model.current_model_state()
        _, traject = model.integrate_state(x0, checkpoints=[0, 360])  # we can run it longer to get more usefule IC
        xtrue = traject[-1]
        ytrue = model.evaluate_theoretical_observation(xtrue)
        y = model._obs_err_model.add_noise(ytrue, in_place=False)
    else:
        y = None

    try:
        if use_MPI:
            COMM.bcast(y, root=0)
    except(TypeError):
        print("Working on Communicating Python-based objects (without pickling)")
        pass

    # Generate forecast state, and synthetic observation
    if my_rank == 0:
        xf = model._frcst_err_model.add_noise(x0, in_place=False)
        _, traject = model.integrate_state(xf, checkpoints=[0, 0.5])
        xf = traject[-1]

        init_ensemble = model.create_state_ensemble(0)
        for i in range(ensemble_size):
            member = model._frcst_err_model.add_noise(xf, in_place=False)
            init_ensemble.append(member)
        xf = init_ensemble.mean()
    else:
        init_ensemble = None

    # Create Filter
    filter_configs={'model': model,
                    'MPI_COMM':COMM,
                    'observation_error_model': obs_err_model,
                    'tspan': tspan[1: ],
                    'observation': y,
                    'ensemble_size': ensemble_size,
                    'forecast_ensemble':init_ensemble,
                    'analysis_ensemble':None,
                    'forecast_first':False,
                    'localize_covariances':True,
                    'localization_radius':5000,  # loc radius im dist_unit
                    'localization_function':'Gaspari-Cohn'
                    }
    output_configs={'verbose':False}
    da_filter = KalmanFilter(filter_configs=filter_configs, output_configs=output_configs)

    # apply a filtering cycle (analysis+forecast); forecast should be ignored here since tspan[0] == tspan[-1]
    da_filter.filtering_cycle()

    # get analysis state
    xa = da_filter.get_analysis_state()

    if my_rank == 0:
        # print("xtrue: ", xtrue)
        # print("xa: ", xa)
        # print("xf: ", xf)
        # print("ytrue: ", ytrue)
        # print("y: ", y)

        # calculate analysis RMEs:
        analysis_err = xa.axpy(-1.0, xtrue, in_place=False)
        analysis_rmse = analysis_err.norm2() / np.sqrt(xtrue.size)

        # calculate forecast RMEs:
        forecast_err = xf.axpy(-1.0, xtrue, in_place=False)
        forecast_rmse = forecast_err.norm2() / np.sqrt(xtrue.size)

        print("EnKf carried out. Forecast RMSE=%f; Analsysi RMSE=%f " % (forecast_rmse, analysis_rmse))

    print("This is node %d; I've done my part...Terminating the main script " % my_rank)
