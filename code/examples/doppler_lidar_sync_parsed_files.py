#! /usr/bin/env python

""" Just sync/copy parsed data files from the server side to the data local directory on this machine

"""

import sys
import os

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()
#

import DL_utility as utility


# Import DL_DATA_HANDLER and generate an object (instances)
from DL_data_handler import DL_DATA_HANDLER

if __name__ == "__main__":

    # Observation time setting:
    start_times = ['2016:06:01:00:00:00', '2016:07:08:00:00:00']
    end_times   = ['2016:06:08:00:00:00', '2016:07:15:00:00:00']

    site_facility='sgpdlE32'

    # create lidar data object:
    dl_obj = DL_DATA_HANDLER()

    # Start collecting data:
    for start_time, end_time in zip(start_times, end_times):
        dl_obj.get_remote_matching_parsed_files(start_time, end_time, site_facility, scan_type='stare')

    print("...Done...")
