#! /usr/bin/env python

""" This is an example that shows how to collect DL observations at a given time instance
    or whithin a time interval
"""

# TODO: Fix xticklabels, and show either + seconds, or nicely formatted date/time strings;
# hint: use dates.DateFormatte from matplotlib.dates; check https://stackoverflow.com/questions/28385184/how-to-set-the-xticklabels-for-date-in-matplotlib

import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import os
import re

try:
    import cPickle as pickle
except ImportError:
    import pickle

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

#
import numpy as np
import scipy.io as sio
import datetime

import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib import rc

# set fonts, and colors:
font_size = 12
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : font_size}
#
rc('font', **font)
rc('text', usetex=True)


import h5py
import re
import shutil

import DL_utility as utility

# Import DL_DATA_HANDLER and generate an object (instances)
from DL_data_handler import DL_DATA_HANDLER
dl_obj = DL_DATA_HANDLER()


def create_plots_dir(dir_name='__PLOTS', overwrite=True):
    """
    """
    if dir_name is None:
        dir_name = '__PLOTS'
    elif isinstance(dir_name, str):
        if len(dir_name.strip()) == 0:
            dir_name = '__PLOTS'
    else:
        print("dir_name must be a valid string, or pass None for a default name!")
        raise TypeError
    #
    if os.path.isabs(dir_name):
        plots_path = dir_name
    else:
        head, _ = os.path.split(__file__)
        plots_path = os.path.join(head, dir_name)
    print(dir_name, plots_path)

    if (os.path.isdir(plots_path) and (not overwrite)):
        print("Existing results directory: %s ; Skipping as requested... " % plots_path)
        plots_path = None
    else:
        if os.path.isdir(dir_name):
            shutil.rmtree(dir_name)
        os.makedirs(plots_path)
    return plots_path
    #

def altitude_to_gate_index(altitude, range_gate_length=30):
    """ """
    index = int( altitude / range_gate_length - 0.5 )
    return index

def gate_index_to_altitude(index, range_gate_length=30):
    altitude = (index + 0.5) * range_gate_length
    return altitude

def read_and_plot(start_time,
                  end_time,
                  delta_time,
                  site_facility,
                  field,
                  scan_type,
                  add_intensity_to_lineplots=False,
                  aggregate_data=True,
                  filter_observations=False,
                  collect_data_only=False,
                  all_gates=True,
                  overwrite=False,
                  collect_remote_data=False,
                  out_dir_name='__PLOTS',
                  out_file_format='eps',
                  data_output_format='matlab'
                  ):
    """
    Read, aggregate, and filter Doppler Lidar data

    Args:
        start_time
        end_time
        delta_time
        site_facility
        field
        scan_type
        add_intensity_to_lineplots
        aggregate_data
        filter_observations
        collect_data_only
        all_gates
        overwrite
        collect_remote_data
        out_dir_name
        out_file_format
        data_output_format : format of the saved/aggreaged data files ['txt', 'matlab', 'pickle']

    Returns:
        results_dict: a dictionary containing collected data

    """
    plots_path = create_plots_dir(dir_name=out_dir_name, overwrite=overwrite)
    if plots_path is None:
        print("plots_path exists, and you don't want to overwrite things! Please set overwrite to True, or choose a different output path!")
        raise ValueError
        # return None

    # All range gates or just selective one
    if all_gates:
        range_gates = None
    else:
        # Specify a list of range gate indexes (starting at zero), Make it None if you want all gates
        range_gates   = range(0, 20) + range(80, 100) + range(175, 200)

    range_gate_length = 30  # KM

    # ----------------------------------(DATA COLLECTION)----------------------------------- #

    # Now, create a timespan to aggregate DL data over:
    timespan = utility.create_timespan(start_time=start_time, end_time=end_time, delta_time=delta_time, return_string=False)


    # THIS IS A NEW METHOD; Get all stare observations at gates 2, 3, 4, 7:
    obs_timespan, coordinates, observations = dl_obj.filter_DL_site_observations(field=field,
                                                                                 site_facility=site_facility,
                                                                                 timespan=timespan,
                                                                                 scan_type=scan_type,
                                                                                 range_gates=range_gates,
                                                                                 snr_threshold=False,
                                                                                 aggregate_data=aggregate_data,
                                                                                 collect_remote_data=collect_remote_data)
    if len(obs_timespan) == 0:
        print("Didn't find any obbservations! Terminating...")
        return {}  # an empty dictionary1
    else:
        # print("Found number of observation points: %d " %len(obs_timespan))
        pass

    # Get intensity:
    _, _, intensity = dl_obj.filter_DL_site_observations(field='intensity',
                                                         site_facility=site_facility,
                                                         timespan=timespan,
                                                         scan_type=scan_type,
                                                         range_gates=range_gates,
                                                         snr_threshold=False,
                                                         aggregate_data=aggregate_data,
                                                         collect_remote_data=False)

    # Thresholded observations:
    _, _, filtered_observations = dl_obj.filter_DL_site_observations(field=field,
                                                                     site_facility=site_facility,
                                                                     timespan=timespan,
                                                                     scan_type=scan_type,
                                                                     range_gates=range_gates,
                                                                     snr_threshold=True,
                                                                     aggregate_data=aggregate_data,
                                                                     collect_remote_data=False)



    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                                Data collection complete                                #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # Extract some configs from returned results:
    range_gates = [altitude_to_gate_index(c[-1]) for c in coordinates[0]]
    num_gates = len(range_gates)
    obs_dimension = len(observations[0])
    num_obs = len(obs_timespan)
    num_vars = len(observations[0][0])
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                                   Displaying Results                                   #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # -----------------------------------(Screen  Output)----------------------------------- #

    # # Showing results:
    # # -----------------
    # num_obs = len(obs_timespan)
    # sep = "\n" + "*"*80 + "\n"
    # print(" %s\t\tObservations collected for variable: %s: %s" %(sep, field, sep))
    # for i, t, c, o in zip(range(num_obs), obs_timespan, coordinates, observations):
    #     print("%04d] time=%s | coordinates:%s | observation: %s " % (i, t, repr(c), repr(o)))

    # save/pickle configs in a text file:
    # ----------------------------
    experiment_configs = dict(all_gates=all_gates,
                              collect_remote_data=collect_remote_data,
                              site_facility=site_facility,
                              field=field,
                              scan_type=scan_type,
                              range_gates=range_gates,
                              start_time=start_time,
                              end_time=end_time,
                              delta_time=delta_time,
                              timespan=timespan,
                              out_file_format=out_file_format
                              )

    configs_file = os.path.join(out_dir_name, 'experiment_configs.txt')
    with open(configs_file, 'w') as f_id:
        f_id.write("\t\tExperiment Configs\n%s\n" % ("="*50))
        for i, key in enumerate(experiment_configs):
            f_id.write("\n%02d - %s : %s \n" %(i, key, experiment_configs[key]))

    results_dict = dict(coordinates=coordinates,
                        observations=observations,
                        filtered_observations=filtered_observations,
                        intensity=intensity,
                        filter_observations=filter_observations)
    results_dict.update(experiment_configs)
    #
    base_results_file = os.path.join(out_dir_name, "DL_observations")
    # 
    if re.match(r'\A(txt|text|ascii)\Z', data_output_format, re.IGNORECASE):
        print("Text format will be implemented soon if really needed. Matlab format seems more portable to R")
        raise NotImplementedError("...TODO...")
        # results_file = os.path.abspath("%s.txt" % base_results_file)
        #print("Results saved to Text file: %s\n...Proceeding to plotting..." % os.path.abspath(results_file))
        #
    elif re.match(r'\Amat(lab)*\Z', data_output_format, re.IGNORECASE):
        out_dict = results_dict.copy() 
        # Matlab doesn't like DateTime; either use CSV or strings
        str_tspan = utility.create_timespan(start_time=start_time, end_time=end_time, delta_time=delta_time, return_string=True)
        out_dict['timespan'] = str_tspan
        results_file = os.path.abspath("%s.mat" % base_results_file)
        sio.savemat(results_file, out_dict)
        print("Results saved to Matlab (m) file:  %s\n...Proceeding to plotting..." % results_file)
        #
    elif ree.match(r'\Apickle\Z', data_output_format, re.IGNORECASE):
        results_file = os.path.abspath("%s.p" % base_results_file)
        with open(results_file, 'wb') as f_id:
            pickle.dump(results_dict, f_id)
        print("Results saved as a pickled Python object into: %s\n...Proceeding to plotting..." % results_file)
        #
    else:
        print("Unsupported format %s" % data_output_format)
        print("Only text, pickle, matlab formats are supported")
        raise NotImplementedError
    # 
    if collect_data_only:
        return results_dict
    # ------------------------------------(Create  Plots)------------------------------------ #

    # x-limits:
    min_time = mdates.date2num(obs_timespan[0])
    max_time = mdates.date2num(obs_timespan[-1])

    # y-limits:
    margin = 1e-3
    min_obs = np.nanmin(np.asarray(observations)) - margin
    max_obs = np.nanmax(np.asarray(observations)) + margin
    if add_intensity_to_lineplots:
        min_obs = np.nanmin([min_obs, np.nanmin(np.asarray(intensity))])
        max_obs = np.nanmax([max_obs, np.nanmin([max_obs+np.abs(max_obs), np.nanmax(np.asarray(intensity))])])

    # Create Numpy-based data-views:
    np_intensity = np.empty((num_obs, obs_dimension, 1))
    for t_ind, intens in enumerate(intensity):
        try:
            np_intensity[t_ind, :, 0] = [i[0] for i in intens]
        except:
            print intens
            raise
    #
    np_observations = np.empty((num_obs, obs_dimension, num_vars))
    np_filtered_observations = np.empty((num_obs, obs_dimension, num_vars))
    # if filter_observations:
    #     for t_ind, obs in enumerate(filtered_observations):
    #         np_observations[t_ind, :, :] = obs
    # else:
    #     pass
    for t_ind, obs in enumerate(observations):
        np_observations[t_ind, :, :] = obs

    for t_ind, obs in enumerate(filtered_observations):
        np_filtered_observations[t_ind, :, :] = obs

    #
    print("\n\nPlot Imsec plot of stare data")
    #
    # Plot Imsec plot of stare data:
    try:
        _field = field.replace('-', ' ').replace('_', ' ')
        # plt.ylabel("Range Gates")

        y_ticks = np.linspace(1, num_gates, 10, endpoint=False, dtype=int)
        y_ticks = [range_gates[g] for g in y_ticks]
        y_ticklabels = [gate_index_to_altitude(g) for g in y_ticks]
        #

        # max_gates = max(400, num_gates)  # maximum number of gates is 400 in all DL settings
        max_gates = np.asarray(range_gates).max() + 1

        #
        if re.match(r'\Awind(-|_| )*(velocity|speed)(_|-| )*(field)*\Z', _field, re.IGNORECASE):

            vmin, vmax = min_obs, max_obs

            fig4, axes = plt.subplots(3, 2, sharex='col', sharey='row', figsize=(18,10), dpi=85, facecolor='white')
            fig4.suptitle("Vertical Stare Profile; Variable:%s\nStarting Date/Time: %s" % (_field, obs_timespan[0]))

            # ax3 = fig4.add_subplot(313)
            ax3 = axes[2, 0]
            im3 = ax3.imshow(np_observations[:,:,2].T, origin='lower', aspect='auto', vmin=vmin, vmax=vmax)
            ax3.autoscale(True)
            ax3.set_ylabel("Altitude (m)")
            ax3.set_title(r'$w$')
            ax3.set_xlabel('Time')
            #

            ax2 = axes[1, 0]
            im2 = ax2.imshow(np_observations[:,:,1].T, origin='lower', aspect='auto', vmin=vmin, vmax=vmax)
            ax2.autoscale(True)
            ax2.set_ylabel("Altitude (m)")
            ax2.set_title(r'$v$')
            ax2.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

            ax1 = axes[0, 0]
            im1 = ax1.imshow(np_observations[:,:,0].T, origin='lower', aspect='auto', vmin=vmin, vmax=vmax)
            ax1.autoscale(True)
            # ax1.set_ylabel("Altitude (m)")
            ax1.set_title(r'$u$')
            ax1.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
            #

            # add filtered results
            ax3 = axes[2, 1]
            im3 = ax3.imshow(np_filtered_observations[:,:,2].T, origin='lower', aspect='auto', vmin=vmin, vmax=vmax)
            ax3.autoscale(True)
            # plt.colorbar(im3, ax=ax3)
            # ax3.set_ylabel("Altitude (m)")
            ax3.set_title(r'filtered $w$')
            ax3.set_xlabel('Time')
            #

            ax2 = axes[1, 1]
            im2 = ax2.imshow(np_filtered_observations[:,:,1].T, origin='lower', aspect='auto', vmin=vmin, vmax=vmax)
            ax2.autoscale(True)
            # ax2.set_ylabel("Altitude (m)")
            ax2.set_title(r'filtered $v$')
            ax2.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

            ax1 = axes[0, 1]
            im1 = ax1.imshow(np_filtered_observations[:,:,0].T, origin='lower', aspect='auto', vmin=vmin, vmax=vmax)
            ax1.autoscale(True)
            # ax1.set_ylabel("Altitude (m)")
            ax1.set_title(r'filtered $u$')
            ax1.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')


            # Adjust wind-velocity subplots:
            plt.subplots_adjust(hspace=0.25, left=0.1, bottom=0.30, right=0.63, top=0.85)

            for axis in fig4.axes:
                curr_ticks = axis.get_xticks()
                l = min(len(curr_ticks-2), 10)
                x_ticks = np.arange(0, num_obs, num_obs/l)
                x_tickvals = np.linspace(mdates.date2num(obs_timespan[0]), mdates.date2num(obs_timespan[-1]), l, endpoint=True)
                x_tickvals = [mdates.num2date(v) for v in x_tickvals]
                x_ticklabels = [v.time().strftime('%H:%M:%S') for v in x_tickvals]
                axis.set_xticks(x_ticks)
                axis.set_xticklabels(x_ticklabels, rotation=60)
                try:
                    axis.label_outer()
                except:
                    pass

            # Add intensity:
            ax = fig4.add_axes([0.70, 0.35, 0.27, 0.25])
            im = ax.imshow(np_intensity[:,:,0].T, origin='lower', aspect='auto')
            ax.autoscale(True)
            ax.set_title("Signal Intensity (SNR+1)")
            ax.set_xlabel("Time")
            ax.set_ylabel("Altitude (m)")
            curr_ticks = ax.get_xticks()
            l = min(len(curr_ticks-2), 10)
            x_ticks = np.arange(0, num_obs, num_obs/l)
            x_tickvals = np.linspace(mdates.date2num(obs_timespan[0]), mdates.date2num(obs_timespan[-1]), l, endpoint=True)
            x_tickvals = [mdates.num2date(v) for v in x_tickvals]
            x_ticklabels = [v.time().strftime('%H:%M:%S') for v in x_tickvals]
            ax.set_xticks(x_ticks)
            ax.set_xticklabels(x_ticklabels, rotation=60)

            # Adding a unified colorbar for wind-speed:
            # Adding a unified colorbar
            cbar_ax = fig4.add_axes([0.13, 0.15, 0.45, 0.03])
            fig4.colorbar(im1, cax=cbar_ax, orientation='horizontal')

            # Adding a colorbar
            cbar_ax = fig4.add_axes([0.73, 0.20, 0.25, 0.03])
            fig4.colorbar(im, cax=cbar_ax, orientation='horizontal')

        else:
            # Now, we will add filtered/thresholded observations (filtered_observations) as well
            fig4 = plt.figure(figsize=(18,10), dpi=85, facecolor='white')
            fig4.suptitle("Vertical Stare Profile; Variable:%s\nStarting Date/Time: %s" % (_field, obs_timespan[0]))

            vmin, vmax = min_obs, max_obs

            ax1 = fig4.add_subplot(121)
            im1 = ax1.imshow(np_observations[:,:,0].T, origin='lower', aspect='auto', vmin=vmin, vmax=vmax)
            ax1.autoscale(True)
            ax1.set_ylabel("Altitude (m)")
            ax1.set_xlabel('Time')
            ax1.set_title(_field)
            #

            ax2 = fig4.add_subplot(122, sharex=ax1, sharey=ax1)
            im2 = ax2.imshow(np.asarray(np_filtered_observations[:,:,0].T), origin='lower', aspect='auto', vmin=vmin, vmax=vmax)
            ax2.autoscale(True)
            plt.setp([ax2.get_yticklabels()], visible=False)
            ax2.set_xlabel('Time')
            ax2.set_title("Filtered %s" % _field)

            plt.subplots_adjust(hspace=.025, left=0.1, bottom=0.30, right=0.65, top=0.85)

            for axis in [ax1, ax2]:
                curr_ticks = axis.get_xticks()
                l = min(len(curr_ticks-2), 10)
                x_ticks = np.arange(0, num_obs, num_obs/l)
                x_tickvals = np.linspace(mdates.date2num(obs_timespan[0]), mdates.date2num(obs_timespan[-1]), l, endpoint=True)
                x_tickvals = [mdates.num2date(v) for v in x_tickvals]
                x_ticklabels = [v.time().strftime('%H:%M:%S') for v in x_tickvals]
                axis.set_xticks(x_ticks)
                axis.set_xticklabels(x_ticklabels, rotation=60)
                try:
                    axis.label_outer()
                except:
                    pass

            # TODO: Something odd is happenning with the xticklabels; Trye to figure it out!
            # Add intensity:
            ax = fig4.add_axes([0.72, 0.30, 0.25, 0.55])
            im = ax.imshow(np_intensity[:,:,0].T, origin='lower', aspect='auto')
            ax.autoscale(True)
            ax.set_title("Signal Intensity (SNR+1)")
            ax.set_xlabel("Time")
            ax.set_ylabel("Altitude (m)")
            curr_ticks = ax.get_xticks()
            l = min(len(curr_ticks-2), 10)
            x_ticks = np.arange(0, num_obs, num_obs/l)
            x_tickvals = np.linspace(mdates.date2num(obs_timespan[0]), mdates.date2num(obs_timespan[-1]), l, endpoint=True)
            x_tickvals = [mdates.num2date(v) for v in x_tickvals]
            x_ticklabels = [v.time().strftime('%H:%M:%S') for v in x_tickvals]
            ax.set_xticks(x_ticks)
            ax.set_xticklabels(x_ticklabels, rotation=60)

            # Adding a unified colorbar
            cbar_ax = fig4.add_axes([0.15, 0.12, 0.45, 0.03])
            fig4.colorbar(im1, cax=cbar_ax, orientation='horizontal')

            # Adding a colorbar
            cbar_ax = fig4.add_axes([0.73, 0.20, 0.22, 0.03])
            fig4.colorbar(im, cax=cbar_ax, orientation='horizontal')

        # save plot:
        file_name = "Obs_VerticalStare_Profile_%s.%s" %(field, out_file_format.strip(" ."))
        file_name = os.path.join(plots_path, file_name)
        plt.savefig(file_name, dpi=500, facecolor='w', format=out_file_format, transparent=True, bbox_inches='tight')
        print("\rSaved: %s    " % file_name),
        sys.stdout.flush()
        plt.close(fig4)

    except ValueError:  # happens of all values are nan; e.g. if sync is interrupted
        print("Failed to plot Imsec plot of stare data; Mostly all values are NaNs")
        plt.close('all')
        raise
        #



    print("\nPlotting BpxPlots for all gates, at each time point...")
    # BoxPlots, and collect means, standard deviations of values over time;
    # ------------------------------
    if filter_observations:
        target_obs = np_filtered_observations
    else:
        target_obs = np_observations
    try:
        fig2 = plt.figure(facecolor='white')
        _field = field.replace('-', ' ').replace('_', ' ')

        fig2.suptitle("Variable:%s\nStarting Date/Time: %s" % (_field, obs_timespan[0]))
        bps = []
        if re.match(r'\Awind(-|_| )*(velocity|speed)(_|-| )*(field)*\Z', _field, re.IGNORECASE):
            vals = []
            for t_ind in range(num_obs):
                data = target_obs[t_ind, :, 0].flatten()
                data = data[~np.isnan(data)]
                vals.append(data)
            # for v in vals:
            #     v.shape = (-1, 1)
            ax1 = fig2.add_subplot(311)
            bp = ax1.boxplot(vals, notch=True, patch_artist=True, sym='+', vert=1, whis=1.5)
            bps.append(bp)
            ax1.set_ylabel(r'$u$')

            vals = []
            for t_ind in range(num_obs):
                data = target_obs[t_ind, :, 1].flatten()
                data = data[~np.isnan(data)]
                vals.append(data)
            for v in vals:
                v.shape = (-1, 1)
            ax2 = fig2.add_subplot(312, sharex=ax1)
            bp = ax2.boxplot(vals, notch=True, patch_artist=True, sym='+', vert=1, whis=1.5)
            bps.append(bp)
            ax2.set_ylabel(r'$v$')

            vals = []
            for t_ind in range(num_obs):
                data = target_obs[t_ind, :, 2].flatten()
                data = data[~np.isnan(data)]
                vals.append(data)
            # for v in vals:
            #     v.shape = (-1, 1)
            ax3 = fig2.add_subplot(313, sharex=ax1)
            bp = ax3.boxplot(vals, notch=True, patch_artist=True, sym='+', vert=1, whis=1.5)
            bps.append(bp)
            ax3.set_ylabel(r'$w$')
            ax3.set_xlabel('Time')

        else:
            vals = []
            for t_ind in range(num_obs):
                data = target_obs[t_ind, :, 0].flatten()
                data = data[~np.isnan(data)]
                vals.append(data)
            # for v in vals:
            #     v.shape = (-1, 1)
            ax = fig2.gca()
            bp = ax.boxplot(vals, notch=True, patch_artist=True, sym='+', vert=1, whis=1.5)
            bps.append(bp)
            ax.set_xlabel("Time")
            ax.set_ylabel(_field)

        for i, ax in enumerate(fig2.axes):
            bp = bps[i]
            plt.setp(bp['boxes'], color='black')
            plt.setp(bp['whiskers'], color='black')
            plt.setp(bp['fliers'], color='red', marker='+')
            ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
            ax.set_axisbelow(True)

        # Adjust spacing and ticking
        plt.subplots_adjust(hspace=.1, left=0.15, bottom=0.05, right=0.95, top=0.85)
        #
        for ax in fig2.axes:
            l = min(num_obs, 10)
            tick_vals = []
            tick_labels = []
            for i, t in enumerate(obs_timespan):
                if i%(num_obs/l) == 0:
                    tick_vals.append(i+1)
                    tick_labels.append(obs_timespan[i])

            tick_labels = [v.time().strftime('%H:%M:%S') for v in tick_labels]
            ax.set_xticks(tick_vals)
            ax.set_xticklabels(tick_labels, rotation=60)
            try:
                ax.label_outer()
            except:
                pass


        # save plot:
        file_name = "Obs_BoxPlot_overTime_%s.%s" %(field, out_file_format.strip(" ."))
        file_name = os.path.join(plots_path, file_name)
        plt.savefig(file_name, dpi=500, facecolor='w', format=out_file_format, transparent=True, bbox_inches='tight')
        print("\rSaved: %s    " % file_name),
        sys.stdout.flush()
        plt.close(fig2)

    except ValueError:  # happens of all values are nan; e.g. if sync is interrupted
        print("Failed to plot BoxPlots of Stare Observations: Mostly all values are NaNs" )
        plt.close('all')
        raise

    # ------------------------------
    print("\nPlotting BpxPlots for all times, at each gate...")
    if filter_observations:
        target_obs = np_filtered_observations
    else:
        target_obs = np_observations
    try:
        fig2 = plt.figure(facecolor='white')
        _field = field.replace('-', ' ').replace('_', ' ')

        fig2.suptitle("Variable:%s\nDate/Time Interval: %s to %s" % (_field, obs_timespan[0], obs_timespan[-1]))
        bps = []
        if re.match(r'\Awind(-|_| )*(velocity|speed)(_|-| )*(field)*\Z', _field, re.IGNORECASE):
            vals = []
            for g_ind in range(num_gates):
                data = target_obs[:, g_ind, 0].flatten()
                data = data[~np.isnan(data)]
                vals.append(data)
            # for v in vals:
            #     v.shape = (-1, 1)
            ax1 = fig2.add_subplot(131)
            bp = ax1.boxplot(vals, vert=False, notch=True, patch_artist=True, sym='+', whis=1.5)
            bps.append(bp)
            ax1.set_ylabel('Altitude (m)')
            ax1.set_xlabel(r'$u$')

            vals = []
            for g_ind in range(num_gates):
                data = target_obs[:, g_ind, 1].flatten()
                data = data[~np.isnan(data)]
                vals.append(data)
            for v in vals:
                v.shape = (-1, 1)
            ax2 = fig2.add_subplot(132, sharey=ax1)
            bp = ax2.boxplot(vals, vert=False, notch=True, patch_artist=True, sym='+', whis=1.5)
            bps.append(bp)
            ax2.set_xlabel(r'$v$')

            vals = []
            for g_ind in range(num_gates):
                data = target_obs[:, g_ind, 2].flatten()
                data = data[~np.isnan(data)]
                vals.append(data)
            # for v in vals:
            #     v.shape = (-1, 1)
            ax3 = fig2.add_subplot(133, sharey=ax1)
            bp = ax3.boxplot(vals, vert=False, notch=True, patch_artist=True, sym='+', whis=1.5)
            bps.append(bp)
            ax3.set_ylabel(r'$w$')

        else:
            vals = []
            for g_ind in range(num_gates):
                data = target_obs[:, g_ind, 0].flatten()
                data = data[~np.isnan(data)]
                vals.append(data)
            # for v in vals:
            #     v.shape = (-1, 1)
            ax = fig2.gca()
            bp = ax.boxplot(vals, vert=False, notch=True, patch_artist=True, sym='+', whis=1.5)
            bps.append(bp)
            ax.set_ylabel("Altitude (m)")
            ax.set_xlabel(_field)

        for i, ax in enumerate(fig2.axes):
            bp = bps[i]
            plt.setp(bp['boxes'], color='black')
            plt.setp(bp['whiskers'], color='black')
            plt.setp(bp['fliers'], color='red', marker='+')
            ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
            ax.set_axisbelow(True)

        # Adjust spacing and ticking
        plt.subplots_adjust(hspace=.1, left=0.15, bottom=0.05, right=0.95, top=0.85)
        #
        for ax in fig2.axes:
            l = min(num_gates, 10)
            tick_vals = []
            tick_labels = []
            for i, t in enumerate(range_gates):
                if i%(num_gates/l) == 0:
                    tick_vals.append(i+1)
                    tick_labels.append(range_gates[i])
            tick_labels = [gate_index_to_altitude(g) for g in tick_labels]
            ax.set_yticks(tick_vals)
            ax.set_yticklabels(tick_labels)

            try:
                ax.label_outer()
            except:
                pass

        # save plot:
        file_name = "Obs_BoxPlot_overGates_%s.%s" %(field, out_file_format.strip(" ."))
        file_name = os.path.join(plots_path, file_name)
        plt.savefig(file_name, dpi=500, facecolor='w', format=out_file_format, transparent=True, bbox_inches='tight')
        print("\rSaved: %s    " % file_name),
        sys.stdout.flush()
        plt.close(fig2)

    except ValueError:  # happens of all values are nan; e.g. if sync is interrupted
        print("Failed to plot BoxPlots of Stare Observations: Mostly all values are NaNs" )
        plt.close('all')


    #
    print("\n\nPlot Statistics, e.g. means and standard deviations (over all selected gates) over time")
    #
    if filter_observations:
        target_obs = np_filtered_observations
    else:
        target_obs = np_observations
    means = []
    stdevs = []
    for t_ind in range(num_obs):
        avg = [np.nanmean(target_obs[t_ind, :, i]) for i in range(num_vars)]
        std = [np.nanstd(target_obs[t_ind, :, i]) for i in range(num_vars)]
        means.append(avg)
        stdevs.append(std)

    # Plot Statistics, e.g. means and standard deviations (for all selected gates-together-) over time
    try:
        fig3 = plt.figure(facecolor='white')
        _field = field.replace('-', ' ').replace('_', ' ')

        fig3.suptitle("Mean \& STDEV; Variable:%s\nStarting Date/Time: %s" % (_field, obs_timespan[0]))

        if re.match(r'\Awind(-|_| )*(velocity|speed)(_|-| )*(field)*\Z', _field, re.IGNORECASE):
            ax1 = fig3.add_subplot(311)
            ax1.plot(obs_timespan, [m[0] for m in means], 'r-d', linewidth=2, markersize=4, label=r'$\mu(u)$')
            ax1.plot(obs_timespan, [s[0] for s in stdevs], 'g-d', linewidth=2, markersize=4, label=r'$\sigma(u)$')
            ax1.set_ylabel(_field)
            ax1.legend(loc='best')
            plt.setp(ax1.get_xticklabels(), visible=False)

            ax2 = fig3.add_subplot(312, sharex=ax1, sharey=ax1)
            ax2.plot(obs_timespan, [m[1] for m in means], 'r-d', linewidth=2, markersize=4, label=r'$\mu(v)$')
            ax2.plot(obs_timespan, [s[1] for s in stdevs], 'g-d', linewidth=2, markersize=4, label=r'$\sigma(v)$')
            ax2.set_ylabel(_field)
            ax2.legend(loc='best')
            plt.setp(ax2.get_xticklabels(), visible=False)

            ax3 = fig3.add_subplot(313, sharex=ax1, sharey=ax1)
            ax3.plot(obs_timespan, [m[2] for m in means], 'r-d', linewidth=2, markersize=4, label=r'$\mu(w)$')
            ax3.plot(obs_timespan, [s[2] for s in stdevs], 'g-d', linewidth=2, markersize=4, label=r'$\sigma(w)$')
            ax3.set_ylabel(_field)
            ax3.legend(loc='best')
            ax3.set_xlabel('Time')

            # Set axes limits
            ax1.set_xlim([min_time, max_time])

            ax1_ylims = ax1.get_ylim()
            ax2_ylims = ax2.get_ylim()
            ax3_ylims = ax3.get_ylim()
            min_y = np.nanmin([ax1_ylims[0], ax2_ylims[0], ax3_ylims[0]])
            max_y = np.nanmax([ax1_ylims[-1], ax2_ylims[-1], ax3_ylims[-1]])

            if not (np.isnan(min_y) or np.isnan(max_y)):
                ax1.set_ylim([min_y, max_y])

        else:
            plt.plot(obs_timespan, means, 'r-d', linewidth=2, markersize=4, label=r"$\mu$(%s)"%_field)
            plt.plot(obs_timespan, stdevs, 'g-d', linewidth=2, markersize=4, label=r"$\sigma$(%s)"%_field)
            plt.xlabel("Time")
            plt.ylabel(_field)

            plt.legend(loc='best')

            # Set axes limits
            ax = plt.gca()
            ax.set_xlim([min_time, max_time])


        # Adjust subplots spacing and , and tick-labels
        fig3.subplots_adjust(wspace=0.04, hspace=0.04)
        #
        for ax in fig3.axes:
            curr_ticks = ax.get_xticks()
            l = min(len(curr_ticks), 10)
            xlims = ax.get_xlim()
            tick_vals = np.linspace(xlims[0], xlims[-1], l, endpoint=True)
            tick_vals = [mdates.num2date(v) for v in tick_vals]
            tick_labels = [v.time().strftime('%H:%M:%S') for v in tick_vals]
            ax.set_xticks(tick_vals)
            ax.set_xticklabels(tick_labels, rotation=60)
            try:
                ax.label_outer()
            except:
                pass

        # save plot:
        file_name = "Obs_Statistics_%s.%s" %(field, out_file_format.strip(" ."))
        file_name = os.path.join(plots_path, file_name)
        plt.savefig(file_name, dpi=500, facecolor='w', format=out_file_format, transparent=True, bbox_inches='tight')
        print("\rSaved: %s    " % file_name),
        sys.stdout.flush()
        plt.close(fig3)

    except ValueError:  # happens of all values are nan; e.g. if sync is interrupted
        print("Failed to plot Statistics of stare data; Mostly all values are NaNs")
        plt.close('all')


    # Simple plot of the timeseries
    print("Plotting Observations at selected gates over time...")
    # ------------------------------
    if filter_observations:
        target_obs = np_filtered_observations
    else:
        target_obs = np_observations

    for gate_ind, gate in enumerate(range_gates):
        try:
            fig = plt.figure(facecolor='white')
            _field = field.replace('-', ' ').replace('_', ' ')

            fig.suptitle("Gate %d at Altitude %6.2f(m); \ Variable: %s\nStarting Date/Time: %s" % (gate, gate_index_to_altitude(gate), _field, obs_timespan[0]))

            if re.match(r'\Awind(-|_| )*(velocity|speed)(_|-| )*(field)*\Z', _field, re.IGNORECASE):
                ax1 = fig.add_subplot(311)
                ax1.plot(obs_timespan, target_obs[:, gate_ind, 0], 'r-d', linewidth=2, markersize=4, label=r'$u$')
                if add_intensity_to_lineplots:
                    ax1.plot(obs_timespan, intensity[:, gate_ind, 0], 'g-.o', linewidth=2, markersize=4, label='intensity (SNR+1)')
                ax1.set_ylabel(_field)
                ax1.legend(loc='best')

                ax2 = fig.add_subplot(312, sharex=ax1, sharey=ax1)
                ax2.plot(obs_timespan, target_obs[:, gate_ind, 1], 'g-d', linewidth=2, markersize=4, label=r'$v$')
                if add_intensity_to_lineplots:
                    ax2.plot(obs_timespan, intensity[:, gate_ind, 0], 'g-.o', linewidth=2, markersize=4, label='intensity (SNR+1)')
                ax2.set_ylabel(_field)
                ax2.legend(loc='best')

                ax3 = fig.add_subplot(313, sharex=ax1, sharey=ax1)
                ax3.plot(obs_timespan, target_obs[:, gate_ind, 2], 'b-d', linewidth=2, markersize=4, label=r'$w$')
                if add_intensity_to_lineplots:
                    ax3.plot(obs_timespan, intensity[:, gate_ind, 0], 'g-.o', linewidth=2, markersize=4, label='intensity (SNR+1)')
                ax3.set_ylabel(_field)
                ax3.legend(loc='best')
                ax3.set_xlabel('Time')

                # Set axes limits
                ax1.set_xlim([min_time, max_time])

                if not (np.isnan(min_obs) or np.isnan(max_obs)):
                    y_lims = [min_obs, max_obs]
                    ax1.set_ylim(y_lims)

            else:
                plt.plot(obs_timespan, target_obs[:, gate_ind, 0], 'r-d', linewidth=2, markersize=4, label=_field)
                if add_intensity_to_lineplots:
                    plt.plot(obs_timespan, intensity[:, gate_ind, 0], 'g-.o', linewidth=2, markersize=4, label='intensity (SNR+1)')
                plt.xlabel("Time")
                plt.ylabel(_field)

                # get axis control:
                ax = plt.gca()

                # Set axes limits
                ax.set_xlim([min_time, max_time])

                if not (np.isnan(min_obs) or np.isnan(max_obs)):
                    ax.set_ylim([min_obs, max_obs])

                plt.legend(loc='best')

            # Adjust subplots spacing and , and tick-labels
            fig.subplots_adjust(wspace=0.04, hspace=0.04)
            #
            for ax in fig.axes:
                curr_ticks = ax.get_xticks()
                l = min(len(curr_ticks), 10)
                xlims = ax.get_xlim()
                tick_vals = np.linspace(xlims[0], xlims[-1], l, endpoint=True)
                tick_vals = [mdates.num2date(v) for v in tick_vals]
                tick_labels = [v.time().strftime('%H:%M:%S') for v in tick_vals]
                ax.set_xticks(tick_vals)
                ax.set_xticklabels(tick_labels, rotation=60)
                try:
                    ax.label_outer()
                except:
                    pass


            # save plot:
            file_name = "Obs_overTime_%s_Gate_%03d.%s" %(field, gate, out_file_format.strip(" ."))
            file_name = os.path.join(plots_path, file_name)
            sys.stdout.flush()
            plt.savefig(file_name, dpi=500, facecolor='w', format=out_file_format, transparent=True, bbox_inches='tight')
            print("\rSaved: %s    " % file_name),

            plt.close(fig)

        except ValueError:  # happens of all values are nan; e.g. if sync is interrupted
            print("Failed to plot Stare Observations at gate %d ; Mostly all values are NaNs" % gate)
            plt.close('all')
            #
    print("...DONE...")
    #
    return results_dict
    #



#
if __name__ == '__main__':

    # ====================================================================================== #
    #                              Settings & DATA Collection                                #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # --------------------------------------(SETTINGS)-------------------------------------- #
    #
    out_dir_name  = '_DL_OBSERVATIONS_RESULTS'  # folder/path to save plots and collected observation to
    data_output_format = 'matlab'  # both matlab/mat and pickle are currently supported. 

    # Sync data with remote date repo? this gets all matching files from mcs.anl.gov repo if not available on this machine
    collect_remote_data = True

    # Observation time setting: (Create proper timespan)
    start_time = '2016:06:07:23:00:00'
    end_time   = '2016:06:07:23:59:59'
    # start_time = '2017:06:04:07:00:00'
    # end_time   = '2017:06:04:07:10:00'
    delta_time = '0000:00:00:00:00:01'

    # Specify setings/filters for obsrvations to collect
    site_facility = 'sgpdlE32'                       # sgpdlE32, sgpdlC1, etc.
    field         = 'radial_velocity'                # 'radial_velocity', 'doppler', 'wind-speed', etc
    scan_type     = 'stare'                         # 'stare'/'vad'; if vad, must provide vad_scan_degree(default 60); also the plotting down should be updated for multiple vad degrees

    aggregate_data = True  # if False, average observations over each subinterval (delta_time is ignored)

    overwrite_existing_plots = True
    # --------------------------(Data collection and Plotting)------------------------------ #


    collected_results = read_and_plot(start_time=start_time,
                                      end_time=end_time,
                                      delta_time=delta_time,
                                      site_facility=site_facility,
                                      field=field,
                                      scan_type=scan_type,
                                      collect_data_only=True,
                                      all_gates=True,
                                      filter_observations=True,
                                      aggregate_data=aggregate_data,
                                      collect_remote_data=collect_remote_data,
                                      overwrite=overwrite_existing_plots,
                                      out_dir_name=out_dir_name,
                                      data_output_format=data_output_format,
                                     )  # Collect only remotely; will plot later on my machine

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                              Results Plotting complete                                 #
    # ====================================================================================== #
