#!/bin/bash
#SBATCH --job-name='Test-FP-DA'
#SBATCH -N 10
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --output=FP_Bebop_log_%A_%a.out
#SBATCH --output=FP_Bebop_log_%A_%a.err
#SBATCH -p bdwall
#SBATCH -A UNCERTAINTY-CLIMATE
#SBATCH --time=00:30:00

module load anaconda

mpirun python ./test_Filtering_Process.py
