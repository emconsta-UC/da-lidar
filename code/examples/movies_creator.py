
import numpy as np
from PIL import Image as PIL_Image

import matplotlib.pyplot as plt
import matplotlib.image as mgimg
from matplotlib import animation

import os
import sys

sys.path.append('../src/utility')
import DL_utility as utility

import shutil

# Given the results directory, look for reference/forecast/free/analysis state slices, and create combined images (vertical stacking)
# Once these are created, a movie is created out of them

# setup manually
__VIDEO_FILE = "movie.mp4"
__PLOTS_FORMAT = "png"


def merge_images(images, stack_vertical=True, filename=None):
    """
    """
    imgs = []
    for img in images:
        if img is not None:
            imgs.append(PIL_Image.open(img))
        else:
            pass
    # pick the image which is the smallest, and resize the others to match it (can be arbitrary image shape here)
    min_shape = sorted( [(np.sum(i.size), i.size ) for i in imgs])[0][1]
    imgs_comb = np.hstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )

    if stack_vertical:
        # Horizontal stacking:
        imgs_comb = np.vstack( (np.asarray( i.resize(min_shape) ) for i in imgs ) )
        imgs_comb = PIL_Image.fromarray( imgs_comb)
    else:
        # Horizontal stacking:
        imgs_comb = PIL_Image.fromarray( imgs_comb)

    if filename is not None:
        imgs_comb.save(filename)

    return imgs_comb


def create_states_movies(reference_states_list,
                         free_run_states_list,
                         forecast_states_list,
                         analysis_states_list,
                         plots_dir,
                         stack_vertical=True,
                         fileprefix=None,
                         cleanup=False,
                         archive=False,
                         init_ind=None,
                         final_ind=None,
                         dpi=1200,
                         fps=2,
                         repeat_delay=1000,
                         interval=1000):
    """
    """
    # Create Videos:
    unified_length = max(len(forecast_states_list), len(analysis_states_list), len(reference_states_list), len(free_run_states_list))

    forecast_states_list.sort()
    analysis_states_list.sort()
    reference_states_list.sort()
    free_run_states_list.sort()
    #
    forecast_states_list  = ( [None] * (unified_length - len(forecast_states_list)) )  + forecast_states_list
    analysis_states_list  = ( [None] * (unified_length - len(analysis_states_list)) )  + analysis_states_list
    reference_states_list = ( [None] * (unified_length - len(reference_states_list)) ) + reference_states_list
    free_run_states_list  = ( [None] * (unified_length - len(free_run_states_list)) )  + free_run_states_list
    
    if init_ind is not None:
        if init_ind > unified_length-1:
            print("Initial index exceeds maximum allowed length")
            print("IGNORING init_ind")
            init_ind = 0
    else:
        init_ind = 0
    if final_ind is not None:
        if final_ind > unified_length-1:
            print("Finaal index exceeds maximum allowed length")
            print("IGNORING final_ind")
            final_ind = unified_length - 1
    else:
        final_ind = unified_length - 1
    if init_ind >= final_ind:
        print("Initial index exceeds final ind!")
        print("Nothing to plot. Returning...")
        return [], [], [], []  # just for consistency
    
    forecast_states_list = forecast_states_list[init_ind: final_ind+1]
    analysis_states_list = analysis_states_list[init_ind: final_ind+1]
    reference_states_list = reference_states_list[init_ind: final_ind+1]
    free_run_states_list = free_run_states_list[init_ind: final_ind+1]
    
    created_files = []
    for i, ref, free, frcs, anls in zip(range(unified_length), reference_states_list, free_run_states_list, forecast_states_list, analysis_states_list):
        print("Merging: ", frcs, anls, ref, free)
        filename = "img_%04d.%s" % (i, __PLOTS_FORMAT)
        if fileprefix is not None:
            filename = "%s_%s" % (fileprefix, filename)
        filename = os.path.join(plots_dir, filename)
        created_files.append(filename)
        merge_images([ref, free, frcs, anls], stack_vertical=stack_vertical, filename=filename)
        # if i == 10: break  # TODO: remove after debugging...
    
    # Use created files now
    filename = "%s" % __VIDEO_FILE
    if fileprefix is not None:
        filename = "%s_%s" % (fileprefix, filename)
    else:
        filename = "Ref_Free_Frcst_Anls_%s" % filename

    # Now pass to movie creator
    create_movie_from_images(created_files,
                             plots_dir,
                             dpi=dpi,
                             fps=fps,
                             movie_filename=filename,
                             repeat_delay=repeat_delay,
                             interval=interval
                            )

    if cleanup:
        print("Cleaning up created files")
        if archive:
            pass
        for f in created_files:
            os.remove(f)

    return reference_states_list, free_run_states_list, forecast_states_list, analysis_states_list


def create_movie_from_images(list_of_files,
                             output_dir,
                             dpi=1200,
                             fps=2,
                             movie_filename=None,
                             repeat_delay=1000,
                             interval=1000
                            ):
    """
    """
    if len(list_of_files) == 0:
        print("An empty list is passed to the function 'create_movie_from_images', nothing to be done.")
        my_anim = None
    else:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        fig.tight_layout()
        ax.axis('off')
        myimages = []
        print("Loading Images:")
        # Loop  over list of files, and read, then plot
        for i, fname in enumerate(list_of_files):
            sys.stdout.write("\rReading: %d/%d " % (i+1, len(list_of_files)))
            sys.stdout.flush()
            try:
                img = mgimg.imread(fname)
                imgplot = ax.imshow(img)
                myimages.append([imgplot])
            except(IOError):
                pass
        print("...\n")
        if len(myimages) == 0:
            print("Failed to load Images")
            my_anim = None
        else:
            ## create and save an instance of animation
            if movie_filename is None:
                movie_filename = "UnNamed_%s"  % __VIDEO_FILE
            m_d, movie_filename = os.path.split(movie_filename)
            if len(m_d) == 0:
                if not os.path.isdir(output_dir):
                    os.makedirs(output_dir)
                    movie_filename = os.path.join(output_dir, movie_filename)
                movie_file = os.path.join(output_dir, movie_filename)
            else:
                if not os.path.isdir(m_d):
                    os.makedirs(m_d)
                movie_file = os.path.join(m_d, movie_filename)
                print("Passed filename contains a path; ignoring output_dir")

            my_anim = animation.ArtistAnimation(fig, myimages, interval=interval, blit=True, repeat_delay=repeat_delay)
            my_anim.save(movie_file,
                         fps=fps,
                         dpi=dpi,
                         savefig_kwargs=dict(pad_inches=0)
                        )

    return my_anim


def create_observation_movies(list_of_files,
                              plots_dir,
                              patterns_list=None,
                              fileprefix=None,
                              init_ind=None,
                              final_ind=None,
                              dpi=1200,
                              fps=2,
                              repeat_delay=1000,
                              interval=1000):
    """
    """

    skip = False
    if patterns_list is not None:
        _list_of_files = []
        for f in list_of_files:
            _, fn = os.path.split(f)

            skip = False
            for p in patterns_list:
                if p.lower() not in fn.lower():
                    skip = True
                    break
            if not skip:
                _list_of_files.append(f)
        list_of_files = _list_of_files
    else:
        pass

    list_of_files.sort()

    if init_ind is None:
        init_ind = 0
    if final_ind is None:
        final_ind = len(list_of_files) - 1
    list_of_files = list_of_files[init_ind: final_ind+1]
    
    if fileprefix is None:
        filename = "Observations_Profile_%s" % __VIDEO_FILE
    else:
        filename = "%s_%s" % (fileprefix, __VIDEO_FILE)

    create_movie_from_images(list_of_files,
                             plots_dir,
                             dpi=dpi,
                             fps=fps,
                             movie_filename=filename,
                             repeat_delay=repeat_delay,
                             interval=interval
                            )

def movies_creator(list_of_files,
                   patterns_list=None,
                   stack_vertical=True,
                   plot_forecast=True,
                   plot_analysis=True,
                   plot_reference=True,
                   plot_free_run=True,
                   fileprefix=None,
                   init_ind=0,
                   cleanup=False):
    """
    """
    # Slices
    forecast_states_list  = []
    analysis_states_list  = []
    reference_states_list = []
    free_run_states_list  = []

    for f in list_of_files:
        _, fn = os.path.split(f)

        skip = False
        if patterns_list is not None:
            for p in patterns_list:
                if p.lower() not in fn.lower():
                    skip = True
                    break
        if skip:
            continue
        #
        if 'forecast' in fn.lower():
            if plot_forecast:
                forecast_states_list.append(f)
        elif 'analysis' in fn.lower():
            if plot_analysis:
                analysis_states_list.append(f)
        elif 'reference' in fn.lower():
            if plot_reference:
                reference_states_list.append(f)
        elif 'free' in fn.lower():
            if plot_free_run:
                free_run_states_list.append(f)
        else:
            pass
    
    if fileprefix is None:
        fileprefix = ""
    if plot_reference:
        fileprefix += "Reference"
    if plot_free_run:
        fileprefix += "Free"
    if plot_forecast:
        fileprefix += "Forecast"
    if plot_analysis:
        fileprefix += "Analysis"

    if False:
        print("forecast_states_list", forecast_states_list)
        print("analysis_states_list", analysis_states_list)
        print("reference_states_list", reference_states_list)
        print("free_run_states_list", free_run_states_list)
    # 
    create_states_movies(reference_states_list,
                         free_run_states_list,
                         forecast_states_list,
                         analysis_states_list,
                         plots_dir,
                         stack_vertical=stack_vertical,
                         fileprefix=fileprefix,
                         init_ind=init_ind,
                         cleanup=cleanup
                        )


if __name__ == '__main__':

    # What to plot, and what to not!
    plot_states          = False
    plot_observations    = True
    plot_rank_histograms = False
    plot_quivers         = False

    cleanup = True  # Remove any newly created files (e.g., merged files, etc.)

    try:
        plots_dir = sys.argv[1]
    except:
        print("Usage: python plots_merger.py <plots_dir>")
        sys.exit(1)

    list_of_files = utility.get_list_of_files(plots_dir, recursive=False, return_abs=True, extension=__PLOTS_FORMAT)
    if len(list_of_files) == 0:
        print("No files found under [%s] with format [%s]" % (plots_dir, __PLOTS_FORMAT))
        sys.exit(1)

    init_ind = 0

    if plot_states:
        for plot_forecast in [True, False]:
            movies_creator(list_of_files, patterns_list=['state_wind_slice', 'png'],
                           stack_vertical=True,
                           plot_reference=False,
                           plot_forecast=plot_forecast,
                           plot_free_run=True,
                           init_ind=init_ind,
                           fileprefix='States',
                           cleanup=cleanup)

    if plot_quivers:
        movies_creator(list_of_files,
                       patterns_list=['Quiver', 'png'],
                       stack_vertical=False,
                       fileprefix='StatesQuiver',
                       cleanup=cleanup)

    if plot_rank_histograms:
        movies_creator(list_of_files,
                       patterns_list=['Observation_Rhist', 'png'],
                       stack_vertical=False,
                       fileprefix='ObservationRhist',
                       cleanup=cleanup)

        movies_creator(list_of_files,
                       patterns_list=['Observation_Splitted_Rhist', 'png'],
                       stack_vertical=False,
                       fileprefix='ObservationSplittedRhist',
                       cleanup=cleanup)

    if plot_observations:
        create_observation_movies(list_of_files,
                                  plots_dir,
                                  patterns_list=['Observation_WithConfidence_YScaled_XScaled', '.png'],
                                  fileprefix='Observation_WithConfidence_YScaled_XScaled',
                                 )

        create_observation_movies(list_of_files,
                                  plots_dir,
                                  patterns_list=['Observation_WithConfidence_YScaled_cycle', '.png'],
                                  fileprefix='Observation_WithConfidence_YScaled_cycle',
                                 )
        create_observation_movies(list_of_files,
                                  plots_dir,
                                  patterns_list=['Observation_YScaled_XScaled_cycle', '.png'],
                                  fileprefix='Observation_YScaled_XScaled',
                                 )
        create_observation_movies(list_of_files,
                                  plots_dir,
                                  patterns_list=['Observation_cycle', '.png'],
                                  fileprefix='Observation',
                                 )

