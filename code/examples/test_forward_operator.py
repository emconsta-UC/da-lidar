
# This is a very Simple scrip to run Euler3D Hypar model.

import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

# Prepare paths, and sub-packages:
parent_path = os.path.abspath('../')
if parent_path not in sys.path:
    sys.path.append(parent_path)

try:
    from forward_model import ForwardOperator
except ImportError:
    import dl_setup
    dl_setup.main()
    from forward_model import ForwardOperator

# import forward operator class:


if __name__ == '__main__':
    # Forward Model settings (passed to Euler_hypar)
    model_configs = {'size': [101, 101, 41],
                     'iproc': [2, 2, 2],
                     'dt': 0.15,
                     'n_iter': 10,
                     'cleanup_dir':False,
                     'boundary':dict(num_faces=6,  # X-low, X-high, Y-low, Y-high, Z-low, Z-high
                                   faces_types=['periodic', 'periodic', 'periodic', 'periodic', 'thermal-noslip-wall', 'slip-wall'],
                                   faces_settings=[None, None, None, None, (0.0, 0.0, 0.0, 'temperature_data.dat'), (0.0, 0.0, 0.0)]
                                 ), #
                    }
    # Observations Settings (passed to DL_obs)
    obs_configs = {'site_facility': 'sgpdlE32',
                   't': 0,
                   'dl_coordinates': '0,0,0',
                   'prog_var': 'wind-velocity',
                   'num_gates': 400,
                   'range_gate_length': 30,
                   'elevations': [45, 60, 90],
                   'azimuthes': [0, 45, 90, 135, 180, 225, 270, 315]
                   }

    # create forward operator (dynamical model + observations):
    model = ForwardOperator(model_configs, obs_configs)

    # Play with states and observations:
    x = model.state_vector()  # get an empty model state
    print("State: ", x)

    y = model.observation_vector()  # get an empty observation
    print("Observation: ", y)

    model_grid = model.model_grid()  # get model grid
    print("model_grid", model_grid)

    obs_grid = model.observation_grid()  # get observational grid
    print("obs_grid", obs_grid)

    # Integrate state forward in time
    tspan, traject = model._dynamical_model.integrate_state(checkpoints=[0, 3])

    # get the propagated state at time tspan[-1] = 3.0:
    x = traject[-1]
    y = model.evaluate_theoretical_observation(x)  # corresponding observations:

    # Test observation operator Jacobian (vec prod)
    Hx = model.observation_operator_Jacobian_VecProd(x, x)
    HTx = model.observation_operator_Jacobian_T_VecProd(x, y)

    # print stuff if needed
    print("x:, ", x)
    print("y:, ", y)
    for i in range(y.size):
        print("wind-velocity components (u,v,w) at index [%d] are: %s" %(i, repr(y[i*3: i*3+3])))
