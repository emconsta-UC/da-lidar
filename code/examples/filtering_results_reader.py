#!/usr/bin/env python


# standard libraries calculations and plotting
import sys
import itertools
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange
    zip = itertools.izip

import os
import getopt

import numpy as np
import scipy.io as sio

try:
    import h5py
    use_h5py = True
except(ImportError):
    use_h5py = False
import pickle

import re
try:
    import ConfigParser
except:
    import configparser as ConfigParser

import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter  # useful for `logit` scale
from matplotlib import rc

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

import DL_utility as utility
import DLidarVec

# import forward operator class:
import forward_model
from forward_model import ForwardOperator

from hypar_model_plotter import plot_wind_slices, plot_contourf_slices, create_output_dir, rank_histogram


__FREE_RUN_STATE_FILENAME = 'free_run_state.dlvec'
__COLLECTIVE_RES_FILENAME = 'Collective_Results'

__DEF_PLOT_GATES = [10, 15, 20, 25, 50, 55, 60, 100, 105, 110, 115]

__def_out_dir_tree_structure_file = "../Results/Filtering_Results/output_dir_structure.txt.txt"
_FIG_FORMAT = 'png'
_TIME_REL_TOL = 1e-15

# set nan value as it will be needed for config-parsers
nan = np.NaN
inf = np.infty
none = None

def plots_enhancer(fontsize=10, usetex=True):
    # set fonts, and colors:
    font = {'family' : 'serif',
            'weight' : 'bold',
            'size'   : fontsize}
    #
    rc('font', **font)
    rc('text', usetex=usetex)

def read_cycle_results(model, results_path, states_dir, observations_dir):
    """
    Look inside the passed assimilation directory/path, and collect results of this cycle given the prespecified directories

    Args:
        model             : the forward operator (instance of ForwardOperator, that gives access to dynamical model, and observations
        results_path      : relative/absolute path of the results directory
        states_dir        : relative directory (wrt results_path) where model states (forecast/analysis/reference) are saved
        observations_dir  : relative directort (wrt results_path) where observations are saved

    Returns:

        cycle_tspan       : list [t0, t1] of the time bounds of the assimilation cycle
        cycle_configs     : cycle filter and output configurations of this cycle
        forecast_time     : the time at which the forcast is produced; should always be t1
        forecast_state    : forecast state (mean of the forecast ensemble) of this assimilation cycle
        forecast_ensemble : forecast ensemble of this assimilation cycle. If the only the forecast state is saved; this will be None
        forecast_rmse     : RMSE of the forecast (at the forecast time)
        analysis_time     : the time at which the analysis is produced; t0 if analysis first, t1 if forecast first
        analysis_state    : analysis state (mean of the analysis ensemble) of this assimilation cycle
        analysis_ensemble : analysis ensemble of this assimilation cycle. If the only the forecast state is saved; this will be None
        analysis_rmse     : RMSE of the analysis (at the analysis time)
        observation_rmse  : RMSE of the analysis (at the analysis time), projected to the observation space, compared to real observations
        reference_time    : time of the reference state (None if no reference state)
        reference_state   : the true/reference state of this assimilation cycle (if saved)
        observation_time  : observation/analysis time
        observation       : observation vector of this assimilation cycle
        initial_rmse      : RMSE at the initial time of the assimilation cycle (best RMSE) this is the RMSE at the beginning of the window before doing anything
        final_rmse        : RMSE at the final time of the assimilation cycle (best RMSE) this is the RMSE at the end of the window after finishing the cycle

    """
    print("Reading cycle results; states and observations")
    # Read states:
    dir_name = os.path.join(results_path, states_dir)
    cycle_configs, \
            cycle_statistics, \
            reference_state, \
            forecast_state, \
            forecast_ensemble, \
            analysis_state, \
            analysis_ensemble = read_cycle_model_states(dir_name, model)

    if cycle_configs is None:
        print("States directory doesn't contain any configurations file!")
        # raise ValueError
        cycle_tspan = [np.NaN, np.NaN]
        if forecast_state is forecast_ensemble is None:
            forecast_time = np.NaN
        elif forecast_state is not None:
            forecast_time = forecast_state.time
        else:
            forecast_time = forecast_ensemble[0].time
        forecast_rmse = analysis_rmse = observation_rmse = final_rmse = initial_rmse = np.NaN
    else:
        # good to go
        cycle_filter_configs = cycle_configs['filter_configs']
        cycle_output_configs = cycle_configs['output_configs']

        # extract details from configs
        forecast_firts = cycle_filter_configs['forecast_first']
        cycle_tspan    = cycle_filter_configs['timespan']
        forecast_time  = cycle_filter_configs['forecast_time']
        analysis_time  = cycle_filter_configs['analysis_time']

        filter_statistics = cycle_output_configs['filter_statistics']
        forecast_rmse     = filter_statistics['forecast_rmse']
        analysis_rmse     = filter_statistics['analysis_rmse']
        observation_rmse  = filter_statistics['observation_rmse']
        initial_rmse      = filter_statistics['initial_rmse']
        final_rmse        = filter_statistics['final_rmse']

    if reference_state is not None:  # TODO: validate this
        reference_time = reference_state.time
        if not np.isclose(reference_time, np.asarray(cycle_tspan), rtol=_TIME_REL_TOL).all():
            # print("The reference time doesn't match any of assimilation cycle tspan bounds")
            reference_time = cycle_tspan[0]
            reference_state.time = reference_time
    else:
        reference_time = np.NaN

    # Read observations:
    dir_name = os.path.join(results_path, observations_dir)
    obs_configs, observation, external_observations = read_cycle_observations(dir_name, model)
    # extract details from configs
    if obs_configs is not None and observation is not None:
        observation_time = obs_configs['observation_time']
        if not np.isclose(observation_time, observation.time, rtol=_TIME_REL_TOL):
            observation.time = observation_time
            #
    elif observation is not None:
        observation_time = observation.time
        #
    else:
        observation_time = np.NaN

    return cycle_tspan,  cycle_configs, \
            forecast_time, forecast_state, forecast_ensemble, forecast_rmse, \
            analysis_time, analysis_state, analysis_ensemble, analysis_rmse, observation_rmse, \
            reference_time, reference_state, \
            observation_time, observation, external_observations, \
            initial_rmse, final_rmse

def read_cycle_observations(dir_name, model, verbose=False):
    """
    """
    cycle_config_file = os.path.join(dir_name, 'setup.dat')
    # print("cycle_config_file", cycle_config_file)
    filter_configparser = ConfigParser.ConfigParser()
    #
    if os.path.isfile(cycle_config_file):
        filter_configparser.read(cycle_config_file)
        section_header = 'Filter Configs'
        if not filter_configparser.has_section(section_header):
            print("Couldn't find the section header '%s' in the file!" % section_header)
            raise ValueError
        else:
            if not filter_configparser.has_option(section_header, 'observation_time'):
                print("The configurations file doesnt have the option %s under the section header %s")
                raise ValueError
            else:
                observation_time = filter_configparser.getfloat(section_header, 'observation_time')
                #
            filter_config_dict = {'observation_time':observation_time}
    else:
        filter_config_dict = None
    #
    cycle_configs = filter_config_dict

    # get list of observation files
    DLiDA_Vec_ext = 'dlvec'
    list_of_files = utility.get_list_of_files(dir_name, recursive=False, return_abs=False, extension=DLiDA_Vec_ext)

    observation = None  # this will be default (DLiDA) observation
    external_observations = {}  # External observations key'd based on the naming...
    for fle in list_of_files:
        _, fle = os.path.split(fle)
        if re.match(r'\Aobservation(.%s)\Z'%DLiDA_Vec_ext, fle, re.IGNORECASE):
            obs_filename = os.path.join(dir_name, fle)
            observation = model.observation_vector(data_file=obs_filename)
        else:
            # Look for external observations
            source = fle.lower()
            source = source.replace('dlvec', '').replace('.', '').replace('observation', '').replace('_', '').upper()
            source = source.upper()
            #
            obs_filename = os.path.join(dir_name, fle)
            ext_observation = model.observation_vector(data_file=obs_filename)
            external_observations.update({source:ext_observation})

    if len(external_observations) == 0:
        external_observations = None

    # check for reference state
    obs_filename = os.path.join(dir_name, 'observation')
    if not os.path.isfile(obs_filename):
        obs_filename += '.%s' % DLiDA_Vec_ext
    if os.path.isfile(obs_filename):
        observation = model.observation_vector(data_file=obs_filename)
    else:
        observation = None


    return cycle_configs, observation, external_observations

def read_cycle_model_states(dir_name, model, verbose=False):
    """
    """
    # check for reference state
    ref_filename = os.path.join(dir_name, 'reference_state')
    if not os.path.isfile(ref_filename):
        ref_filename += '.dlvec'
    if os.path.isfile(ref_filename):
        reference_state = model.state_vector(data_file=ref_filename)
    else:
        reference_state = None

    # Get filter configs:
    cycle_config_file = os.path.join(dir_name, 'setup.dat')
    filter_configparser = ConfigParser.ConfigParser()
    #
    if os.path.isfile(cycle_config_file):
        filter_configparser.read(cycle_config_file)
        section_header = 'Filter Configs'
        if not filter_configparser.has_section(section_header):
            print("Couldn't find the section header '%s' in the file!" % section_header)
            raise ValueError
        else:
            for option in ['ensemble_size', 'timespan', 'observation_time', 'forecast_time', 'analysis_time']:
                if not filter_configparser.has_option(section_header, option):
                    print("The configurations file doesnt have the option %s under the section header %s")
                    raise ValueError
                    #
            filter_configs = filter_configparser.options(section_header)
            filter_config_dict = {}
            for option in filter_configs:
                option_val = filter_configparser.get(section_header, option)
                if re.match(r'\A(timespan|observation_time|forecast_time|ensemble_size|forecast_first|analysis_time)\Z', option, re.IGNORECASE):
                    option_val = eval(option_val)
                elif re.match(r'\Afilter_name\Z', option, re.IGNORECASE):
                    pass
                else:
                    print("Unknown option %s " % option)
                    raise ValueError
                #
                filter_config_dict.update({option:option_val})

        section_header = 'Output Configs'
        if not filter_configparser.has_section(section_header):
            print("Couldn't find the section header '%s' in the file!" % section_header)
            raise ValueError
        else:
            for option in ['file_output_moment_only', 'file_output_moment_name', 'filter_statistics']:
                if not filter_configparser.has_option(section_header, option):
                    print("The configurations file doesnt have the option %s under the section header %s")
                    raise ValueError
                    #
            output_configs = filter_configparser.options(section_header)
            output_config_dict = {}
            for option in output_configs:
                if re.match(r'\Afilter_statistics\Z', option, re.IGNORECASE):
                    option_val = filter_configparser.get(section_header, option)
                    option_val = eval(option_val)
                elif re.match(r'\Afile_output_moment_only\Z', option, re.IGNORECASE):
                    option_val = filter_configparser.getboolean(section_header, option)
                else:
                    option_val = filter_configparser.get(section_header, option)
                    # print("Unknown option %s " % option)
                    # raise ValueError
                #
                output_config_dict.update({option:option_val})

        # Read ensemble results
        try:
            ensemble_size = max(filter_config_dict['ensemble_size'])
        except(TypeError):
            ensemble_size = filter_config_dict['ensemble_size']

        file_output_moment_only = output_config_dict['file_output_moment_only']

        forecast_time = filter_config_dict['forecast_time']
        analysis_time = filter_config_dict['analysis_time']
        #
        if file_output_moment_only:
            #
            # check for forecast state
            forecast_ensemble = None
            frcst_filename = os.path.join(dir_name, 'forecast_state')
            if not os.path.isfile(frcst_filename):
                frcst_filename += '.dlvec'
            if os.path.isfile(frcst_filename):
                forecast_state = model.state_vector(data_file=frcst_filename)
                forecast_state.time = forecast_time
            else:
                forecast_state = None

            # check for analysis state
            analysis_ensemble = None
            anls_filename = os.path.join(dir_name, 'anls_state')
            if not os.path.isfile(anls_filename):
                anls_filename += '.dlvec'
            if os.path.isfile(anls_filename):
                analysis_state = model.state_vector(data_file=anls_filename)
                analysis_state.time = anslysis_time
            else:
                analysis_state = None
                #
        else:
            forecast_ensemble = model.create_state_ensemble(0, fill_ensemble=False, t=forecast_time)
            for ens_ind in range(ensemble_size):
                ens_member_file = os.path.join(dir_name, 'forecast_ensemble_'+str(ens_ind))
                if not os.path.isfile(ens_member_file):
                    ens_member_file += '.dlvec'
                    #
                if os.path.isfile(ens_member_file):
                    state = model.state_vector(data_file=ens_member_file)
                    state.time = forecast_time
                    forecast_ensemble.append(state)
                else:
                    print("Failed to find/read the %dth forecast ensemble member %s" % (ens_ind, ens_member_file))
                    # raise IOError
                    #
            if forecast_ensemble.size == 0:
                forecast_ensemble = None
                forecast_state = None
            else:
                forecast_state = forecast_ensemble.mean()
                forecast_state.time = forecast_time

            analysis_ensemble = model.create_state_ensemble(0, fill_ensemble=False, t=analysis_time)
            for ens_ind in range(ensemble_size):
                ens_member_file = os.path.join(dir_name, 'analysis_ensemble_'+str(ens_ind))
                if not os.path.isfile(ens_member_file):
                    ens_member_file += '.dlvec'

                if os.path.isfile(ens_member_file):
                    state = model.state_vector(data_file=ens_member_file)
                    state.time = analysis_time
                    analysis_ensemble.append(state)
                else:
                    print("Failed to find/read the %dth analysis ensemble member %s" % (ens_ind, ens_member_file))
                    # raise IOError
                    #
            if analysis_ensemble.size == 0:
                analysis_ensemble = None
                analysis_state = None
            else:
                analysis_state = analysis_ensemble.mean()
                analysis_state.time = analysis_time

        if verbose:
            if forecast_state is forecast_ensemble is None:
                print("Failed to retrieve forecast state and forecast ensemble at this cycle")
            if analysis_state is analysis_ensemble is None:
                print("Failed to retrieve analysis state and analysis ensemble at this cycle")

        cycle_configs = {'filter_configs':filter_config_dict, 'output_configs':output_config_dict}
        cycle_statistics = output_config_dict['filter_statistics']
        #
    else:
        # this means nothing is saved to this folder
        cycle_configs = cycle_statistics = reference_state = forecast_state = forecast_ensemble = analysis_state = analysis_ensemble = None

    return cycle_configs, cycle_statistics, reference_state, forecast_state, forecast_ensemble, analysis_state, analysis_ensemble

def get_output_dir_structure(output_dir_structure_file, section_header='out_dir_tree_structure', full_paths=True):
    """
    Given the location of the of the output_dir_structure_file, infer the current paths of filter output

    Args:
        full_path: if False, only file_output_dir is abolution, and the rest are relative to it

    Returns:
        output_dir_strucutre: a dictionary with full paths, and configurations of the output directory

    """
    output_dir_structure_file = os.path.abspath(output_dir_structure_file)
    if not os.path.isfile(output_dir_structure_file):
        print("The output_dir_structure_file passed [%s] doesn't point to an existing file!" % output_dir_structure_file)
        raise IOError
    else:
        # The current results directory; this may be different from what's saved in file if the results directory is moved somewhere
        target_out_dir = os.path.dirname(output_dir_structure_file)

    dir_configparser = ConfigParser.ConfigParser()
    dir_configparser.read(output_dir_structure_file)
    if not dir_configparser.has_section(section_header):
        print("Couldn't find the section header '%s' in the file!" % section_header)
        raise ValueError
    else:
        dir_configs = dir_configparser.options(section_header)
        for option in dir_configs:
            option_val = dir_configparser.get(section_header, option)
            if re.match(r'\Afile(-|_)*output(-|_)*dir', option, re.IGNORECASE):
                file_output_dir = option_val
            elif re.match(r'\A(model)*(-|_)*state(s)*(-|_)*dir', option, re.IGNORECASE):
                model_states_dir = option_val
            elif re.match(r'\Aobservation(s)*(-|_)*dir', option, re.IGNORECASE):
                observations_dir = option_val
            elif re.match(r'\A(filter)*(-|_)*statistics(-|_)*dir', option, re.IGNORECASE):
                statistics_dir = option_val
            elif re.match(r'\Acycle(-|_)*prefix', option, re.IGNORECASE):
                cycle_prefix = option_val
            else:
                print("Unknown option [%s] found in the output_dir_structur file: %s" % (option, output_dir_structure_file))
                raise ValueError
        # Correct path, wrt the current path
        if file_output_dir in model_states_dir:
            model_states_dir = model_states_dir.replace(file_output_dir, target_out_dir)
            if not full_paths:
                model_states_dir = os.path.relpath(model_states_dir, start=target_out_dir)
        else:
            print("The configuration in output_dir_structure_file are inconsistent!")
            raise ValueError
        if file_output_dir in observations_dir:
            observations_dir = observations_dir.replace(file_output_dir, target_out_dir)
            if not full_paths:
                observations_dir = os.path.relpath(observations_dir, start=target_out_dir)
        else:
            print("The configuration in output_dir_structure_file are inconsistent!")
            raise ValueError
        if file_output_dir in statistics_dir:
            statistics_dir = statistics_dir.replace(file_output_dir, target_out_dir)
            if not full_paths:
                statistics_dir = os.path.relpath(statistics_dir , start=target_out_dir)
        else:
            print("The configuration in output_dir_structure_file are inconsistent!")
            raise ValueError

    output_dir_structure = dict(file_output_dir=target_out_dir,
                                model_states_dir=model_states_dir,
                                observations_dir=observations_dir,
                                statistics_dir=statistics_dir,
                                cycle_prefix=cycle_prefix
                               )
    return output_dir_structure

def get_list_of_dirs(dirpath, dirname_only=True):
    """
    Return a list of relative directories under dirpath

    Args:
        dir_name: where to look
        cycle_prefix: initial part of the name of directories to match
        direname_only: no path

    """
    dirs =  utility.get_list_of_subdirectories(dirpath, ignore_root=True, return_abs=False, ignore_special=True, recursive=False)
    if dirname_only:
        for d_ind, d_name in enumerate(dirs):
            _, dirs[d_ind] = os.path.split(d_name)
    return dirs

def get_cycles_dirs(dir_name, cycle_prefix, dirname_only=True):
    """
    Given the dire_name, look for all folders starting with cycle_prefix, and check how many cycles are saved

    Args:
        dir_name: where to look
        cycle_prefix: initial part of the name of directories to match

    Returns:
        num_cycles: number of cycles saved (maximum postfix number attached to the passed prefix), i.e.
            if cycle_prefix + 12 is found to be the maximum number, then 11 cycles are saved
        missing_cycle: a list of missing cycle numbers is returned; if all numbers from 0 to num_cycles-1 are saved, an empty list is returned.

    """
    found_cycles   = []
    index = 0
    # list all dirs
    list_of_dirs = get_list_of_dirs(dir_name, dirname_only=True)
    num_dirs = len(list_of_dirs)
    for dirname in list_of_dirs:
        if cycle_prefix in dirname:
            postfix = dirname.replace(cycle_prefix, '')
            found_cycles.append(int(postfix))
        else:
            num_dirs -= 1
    found_cycles = list(set(found_cycles))
    found_cycles.sort()
    found_cycles_len = len(found_cycles)
    if found_cycles_len > 0:
        num_cycles = found_cycles[-1] + 1  # postfix index starts at 0
    else:
        num_cycles = 0
    #
    missing_cycles = list(set.difference(set(range(num_cycles)), set(found_cycles)))
    missing_cycles.sort()

    found_cycle_dirs = []
    for cycle_ind in found_cycles:
        cycle_path = cycle_prefix+str(cycle_ind)
        if not dirname_only:
            cycle_path = os.path.join(dir_name, cycle_prefix+str(cycle_ind))
        found_cycle_dirs.append(cycle_path)

    if len(missing_cycles)>0:
        print("Some results cycles are missing in the output directory %s!" % dir_name)
        print("Missing:")
        for mc in missing_cycles:
            print("  %s" % mc)
    #
    return num_cycles, found_cycle_dirs, missing_cycles


def _plot_wind_slices(x, y, z, u, v, w, uvlim=None, vvlim=None, wvlim=None, index=None, filename=None, threeD=True, fontsize=10):
    """
    """
    if threeD:
        if uvlim is not None:
            uvmin = uvlim[0]
            uvmax = uvlim[1]
        else:
            uvmin = uvmax = None
        if vvlim is not None:
            vvmin = vvlim[0]
            vvmax = vvlim[1]
        else:
            vvmin = vvmax = None
        if wvlim is not None:
            wvmin = wvlim[0]
            wvmax = wvlim[1]
        else:
            wvmin = wvmax = None

        # create figure with three subplots:
        fig = plt.figure(figsize=plt.figaspect(0.33), facecolor='white')  # figure with width 3 times the default
        fs = fontsize
        # Subplot with U:
        ax0 = fig.add_subplot(1, 3, 1, projection='3d')
        plot_contourf_slices(x, y, z, u, index, single_axis=True, target_axis=ax0, filename=None, add_color_bars=True, cmap='jet', vmin=uvmin, vmax=uvmax, honor_data_limits=True,
                             dpi=500, alpha=0.6, font_size=5)
        title = ax0.get_title().replace('\\n', ' ')
        title = '$u$ ' + title
        title = r"%s" % title
        ax0.set_title(title, fontsize=fs)
        # Subplot with V:
        ax1 = fig.add_subplot(1, 3, 2, projection='3d')
        plot_contourf_slices(x, y, z, v, index, single_axis=True, target_axis=ax1, filename=None, add_color_bars=True, cmap='jet', vmin=vvmin, vmax=vvmax, honor_data_limits=True,
                             dpi=500, alpha=0.6, font_size=5)
        title = ax1.get_title().replace('\\n', ' ')
        title = '$v$ ' + title
        title = r"%s" % title
        ax1.set_title(title, fontsize=fs)
        # Subplot with W:
        ax2 = fig.add_subplot(1, 3, 3, projection='3d')
        plot_contourf_slices(x, y, z, w, index, single_axis=True, target_axis=ax2, filename=None, add_color_bars=True, cmap='jet', vmin=wvmin, vmax=wvmax, honor_data_limits=True,
                             dpi=500, alpha=0.6, font_size=5)
        title = ax2.get_title().replace('\\n', ' ')
        title = '$w$ ' + title
        title = r"%s" % title
        ax2.set_title(title, fontsize=fs)

    else:
        fig = plot_wind_slices(x, y, z, u, v, w, index=index, filename=filename)
    #
    fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
    return fig

def single_state_plot(model, state, index=None, filename=None, threeD=False, fontsize=10):
    """
    """
    x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0 = model._dynamical_model.get_premitive_atmos_flow_variable(state=state)
    #
    if threeD:
        # create figure with three subplots:
        fig = plt.figure(figsize=plt.figaspect(0.33), facecolor='white')  # figure with width 3 times the default
        fs = fontsize
        # Subplot with U:
        ax0 = fig.add_subplot(1, 3, 1, projection='3d')
        plot_contourf_slices(x, y, z, u, index, single_axis=True, target_axis=ax0, filename=None, add_color_bars=True, cmap='jet', vmin=-1.5, vmax=20, honor_data_limits=False,
                             dpi=500, alpha=0.6, font_size=5)
        title = ax0.get_title().replace('\\n', ' ')
        title = '$u$ ' + title
        title = r"%s" % title
        ax0.set_title(title, fontsize=fs)
        # Subplot with V:
        ax1 = fig.add_subplot(1, 3, 2, projection='3d')
        plot_contourf_slices(x, y, z, v, index, single_axis=True, target_axis=ax1, filename=None, add_color_bars=True, cmap='jet', vmin=-1.5, vmax=1.5, honor_data_limits=False,
                             dpi=500, alpha=0.6, font_size=5)
        title = ax1.get_title().replace('\\n', ' ')
        title = '$v$ ' + title
        title = r"%s" % title
        ax1.set_title(title, fontsize=fs)
        # Subplot with W:
        ax2 = fig.add_subplot(1, 3, 3, projection='3d')
        plot_contourf_slices(x, y, z, w, index, single_axis=True, target_axis=ax2, filename=None, add_color_bars=True, cmap='jet', vmin=-1.5, vmax=1.5, honor_data_limits=False,
                             dpi=500, alpha=0.6, font_size=5)
        title = ax2.get_title().replace('\\n', ' ')
        title = '$w$ ' + title
        title = r"%s" % title
        ax2.set_title(title, fontsize=fs)

    else:
        fig = plot_wind_slices(x, y, z, u, v, w, index=slices_index, filename=None)

    fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
    return fig

def create_states_plots(model, reference_state, forecast_state, forecast_ensemble, analysis_state, analysis_ensemble, free_run_state, slices_index, plots_dir, file_base_name, threeD=True,
                        force_limits=True):
    """
    Given reference_state, forecast_state, forecast_ensemble, analysis_state, analysis_ensemble
    create and save the following plots:
        slices of  each state, i.e. reference, forecast, analysis (if not None)
    The plots are saved to the directory plots_dir

    """
    if force_limits:
        umin = np.infty
        umax = -np.infty
        vmin = np.infty
        vmax = -np.infty
        wmin = np.infty
        wmax = -np.infty
        if reference_state is not None :
            _, _, _, _, ref_u, ref_v, ref_w, _, _, _, _, _, _ = model._dynamical_model.get_premitive_atmos_flow_variable(state=reference_state)
            umin = np.min([umin, np.min(ref_u)])
            umax = np.max([umax, np.max(ref_u)])
            vmin = np.min([vmin, np.min(ref_v)])
            vmax = np.max([vmax, np.max(ref_v)])
            wmin = np.min([wmin, np.min(ref_w)])
            wmax = np.max([wmax, np.max(ref_w)])
        if forecast_state is not None :
            _, _, _, _, frc_u, frc_v, frc_w, _, _, _, _, _, _ = model._dynamical_model.get_premitive_atmos_flow_variable(state=forecast_state)
            umin = np.min([umin, np.min(frc_u)])
            umax = np.max([umax, np.max(frc_u)])
            vmin = np.min([vmin, np.min(frc_v)])
            vmax = np.max([vmax, np.max(frc_v)])
            wmin = np.min([wmin, np.min(frc_w)])
            wmax = np.max([wmax, np.max(frc_w)])
        if analysis_state is not None :
            _, _, _, _, anl_u, anl_v, anl_w, _, _, _, _, _, _ = model._dynamical_model.get_premitive_atmos_flow_variable(state=analysis_state)
            umin = np.min([umin, np.min(anl_u)])
            umax = np.max([umax, np.max(anl_u)])
            vmin = np.min([vmin, np.min(anl_v)])
            vmax = np.max([vmax, np.max(anl_v)])
            wmin = np.min([wmin, np.min(anl_w)])
            wmax = np.max([wmax, np.max(anl_w)])
        if free_run_state is not None :
            _, _, _, _, fre_u, fre_v, fre_w, _, _, _, _, _, _ = model._dynamical_model.get_premitive_atmos_flow_variable(state=free_run_state)
            umin = np.min([umin, np.min(fre_u)])
            umax = np.max([umax, np.max(fre_u)])
            vmin = np.min([vmin, np.min(fre_v)])
            vmax = np.max([vmax, np.max(fre_v)])
            wmin = np.min([wmin, np.min(fre_w)])
            wmax = np.max([wmax, np.max(fre_w)])
    else:
        umin = umax = None
        vmin = vmax = None
        wmin = wmax = None

    if reference_state is not None:
        # Get the gridded velocity field; Note: passing x, y, z, u, v, w avoides reallocating memory for them
        x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0 = model._dynamical_model.get_premitive_atmos_flow_variable(state=reference_state)
        # Plotting Velocity Field:
        # 1- Plot slices of the wind velocity components of the solution at the given slices indexes
        fname = os.path.join(plots_dir, '%s_reference_state_wind'%file_base_name)
        fig = _plot_wind_slices(x, y, z, u, v, w, uvlim=(umin, umax), vvlim=(vmin, vmax), wvlim=(wmin, wmax), index=slices_index, filename="%s_slices.%s" % (fname, _FIG_FORMAT), threeD=threeD)

    if forecast_state is not None:
         # Get the gridded velocity field; Note: passing x, y, z, u, v, w avoides reallocating memory for them
        x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0 = model._dynamical_model.get_premitive_atmos_flow_variable(state=forecast_state)
        # Plotting Velocity Field:
        # 1- Plot slices of the wind velocity components of the solution at the given slices indexes
        fname = os.path.join(plots_dir, '%s_forecast_state_wind'%file_base_name)
        fig = _plot_wind_slices(x, y, z, u, v, w, uvlim=(umin, umax), vvlim=(vmin, vmax), wvlim=(wmin, wmax), index=slices_index, filename="%s_slices.%s" % (fname, _FIG_FORMAT), threeD=threeD)

    if analysis_state is not None:
         # Get the gridded velocity field; Note: passing x, y, z, u, v, w avoides reallocating memory for them
        x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0 = model._dynamical_model.get_premitive_atmos_flow_variable(state=analysis_state)
        # Plotting Velocity Field:
        # 1- Plot slices of the wind velocity components of the solution at the given slices indexes
        fname = os.path.join(plots_dir, '%s_analysis_state_wind'%file_base_name)
        fig = _plot_wind_slices(x, y, z, u, v, w, uvlim=(umin, umax), vvlim=(vmin, vmax), wvlim=(wmin, wmax), index=slices_index, filename="%s_slices.%s" % (fname, _FIG_FORMAT), threeD=threeD)

    if free_run_state is not None:
         # Get the gridded velocity field; Note: passing x, y, z, u, v, w avoides reallocating memory for them
        x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0 = model._dynamical_model.get_premitive_atmos_flow_variable(state=free_run_state)
        # Plotting Velocity Field:
        # 1- Plot slices of the wind velocity components of the solution at the given slices indexes
        fname = os.path.join(plots_dir, '%s_free_run_state_wind'%file_base_name)
        fig = _plot_wind_slices(x, y, z, u, v, w, uvlim=(umin, umax), vvlim=(vmin, vmax), wvlim=(wmin, wmax), index=slices_index, filename="%s_slices.%s" % (fname, _FIG_FORMAT), threeD=threeD)
    plt.close(fig)

def split_observation_indexes(observation_grid):
    """
    Given the observation grid, return stare, and VAD indexes splitted into a tuple of indexes, and associated grid locations
    """
    stare_x = np.where(observation_grid[:, 1] == 90)
    stare_y = np.where(observation_grid[:, 2] == 90)
    stare_indexes = np.intersect1d(stare_x, stare_y)
    vad_indexes = np.setdiff1d(np.arange(observation_grid.shape[0]), stare_indexes)
    return (stare_indexes, vad_indexes)

def create_observations_plot(observations, timespan=None, observation_grid=None, filename=None, v_limits=None):
    """
    observations: a list of observation vectors
    """
    ntimes = len(observations)
    obs_size = None
    i = 0
    while i<ntimes and obs_size is None:
        if observations[i] is not None:
            obs_size = observations[i].size
            break
        i += 1
    if obs_size is None:
        print("All observatios are None; Nothing to plot")
        raise ValueError

    for i, obs in enumerate(observations):
        if obs is None:
            observations[i] = np.empty(obs_size)
            observations[i][:] = np.NaN
    observations_np = np.asarray(observations)

    fig = plt.figure(facecolor='white', figsize=plt.figaspect(0.33))
    ax = fig.add_subplot(111)
    if v_limits is None:
        im = ax.imshow(observations_np.T, aspect='auto', origin='lower')  # Time on xaxis
    else:
        im = ax.imshow(observations_np.T, aspect='auto', origin='lower', vmin=v_limits[0], vmax=v_limits[1])  # Time on xaxis

    xticks = np.arange(0, ntimes, max(1, ntimes//10))
    yticks = np.arange(0, obs_size, max(1, obs_size//20))
    ax.set_xticks(xticks)
    ax.set_yticks(yticks)

    if timespan is not None:
        # updated orientation of the x axis
        timespan = np.asarray(timespan)
        xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in timespan[xticks]]
        ax.set_xticklabels(xticklabels, rotation=60)

    xlims = ax.get_xlim()
    yliners = np.where(observation_grid[:, 0] == observation_grid[0,0])[0]
    for yval in yliners:
        ax.plot([xlims[0], xlims[-1]], [yval, yval], '--k', linewidth=0.5, alpha=0.5)
    if observation_grid is not None:
        yticklabels = [str(observation_grid[tic]) for tic in yticks]
        ax.set_yticklabels(yticklabels)

    ax.set_ylabel("Observation Grid (radial distance, inclination, azimuth)")
    ax.set_xlabel("Time")

    fig.colorbar(im, ax=ax)

    if filename is not None:
        filename = os.path.abspath(filename)
        print("Saving: %s " % filename)
        fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')

    return fig

def create_rmse_plot(forecast_times, forecast_rmses, analysis_times, analysis_rmses, observation_rmses, free_run_times, free_run_rmses, plots_dir, file_name, linewidth=2, logscale=True):
    """
    If you want to skip any entries, pass None, i.e. if free_run results are unaviable, pass free_run_rmse and free_run-times as None
    """
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.grid(True)

    # add forecast rmse
    if forecast_times is not None and forecast_rmses is not None:
        if logscale:
            ax.semilogy(forecast_times, forecast_rmses, color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label='Forecast')
        else:
            ax.plot(forecast_times, forecast_rmses, color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label='Forecast')

    # add analysis rmse
    if analysis_times is not None and analysis_rmses is not None:
        if logscale:
            ax.semilogy(analysis_times, analysis_rmses, color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label='Analysis')
        else:
            ax.plot(analysis_times, analysis_rmses, color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label='Analysis')

    # add analysis-observation rmse
    if analysis_times is not None and observation_rmses is not None:
        if logscale:
            ax.semilogy(analysis_times, observation_rmses, color='r', linestyle='--', marker='*', linewidth=linewidth, label='Observation')
        else:
            ax.plot(analysis_times, observation_rmses, color='r', linestyle='--', marker='*', linewidth=linewidth, label='Observation')

    # add free-run rmse
    if free_run_times is not None and free_run_rmses is not None:
        if logscale:
            ax.semilogy(free_run_times, free_run_rmses, color='#EEC900', linestyle=(0, (3,1,1,1)), marker='s', linewidth=linewidth, label='Free')
        else:
            ax.plot(free_run_times, free_run_rmses, color='#EEC900', linestyle=(0, (3,1,1,1)), marker='s', linewidth=linewidth, label='Free')

    # Add a legend
    ax.legend(loc='best')

    # updated labels
    ax.set_xlabel("Time")
    if logscale:
        ax.set_ylabel("log-RMSE")
    else:
        ax.set_ylabel("RMSE")

    # updated orientation of the x axis
    xticks = ax.get_xticks()
    xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in xticks]
    ax.set_xticklabels(xticklabels, rotation=60)
    filename = os.path.join(plots_dir, file_name)
    fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')

    return fig

def create_error_plots(model=None,
                       forecast_times=None,
                       forecast_states=None,
                       forecast_observations=None,
                       analysis_times=None,
                       analysis_states=None,
                       analysis_observations=None,
                       observation_times=None,
                       observations=None,
                       free_run_times=None,
                       free_run_states=None,
                       free_run_observations=None,
                       reference_times=None,
                       reference_states=None,
                       plots_dir=None,
                       filename=None,
                       plot_initial_time=None,
                       linewidth=2,
                       logscale=True):
    """
    Create error plots:
        - RMSE
        - RSE (Best)
        - MAE
        - RAE

    """
    criteria = ['rmse', 'rse', 'mae', 'rae']
    # each entry of the dictionary will be a list of 4 entries each is a numpy array; [rmse, rse, mae, rae]
    reference_times = None
    time_spans = dict(forecast=forecast_times,
                      analysis=analysis_times,
                      observation=analysis_times,
                      free_run=free_run_times
                     )

    reference_errors = dict(free=None,
                            forecast=None,
                            analysis=None)
    if observation_times is None:
        observation_times = analysis_times
    observation_errors = dict(free=None,
                              forecast=None,
                              analysis=None)

    if reference_states is not None:
        if len(reference_states) == 0:
            reference_states = None
    if observations is not None:
        if len(observations) == 0:
            observations = None

    # Generate Results w.r.t Reference state
    no_reference = True
    if reference_states is not None:
        no_reference = False
        #
        # Forecast errors
        forecast_errors = [None, None, None, None]
        if forecast_states is not None:
            # TODO: Add a check on times, and make sure they match
            if len(forecast_states) != len(reference_states):
                print("length of forecast_states does NOT match length of reference_states")
                raise AssertionError
            for c_ind, criterion in enumerate(criteria):
                errors = np.zeros(len(forecast_states))
                for t_ind, xf, xref in zip(range(len(forecast_states)), forecast_states, reference_states):
                    errors[t_ind] = utility.calculate_errors(xf, xref, criterion=criterion, ignore_nan=True, second_is_truth=True)
                forecast_errors[c_ind] = errors.copy()
        # Analysis errors
        analysis_errors = [None, None, None, None]
        if analysis_states is not None:
            # TODO: Add a check on times, and make sure they match
            if len(analysis_states) != len(reference_states):
                print("length of analysis_states does NOT match length of reference_states")
                raise AssertionError
            for c_ind, criterion in enumerate(criteria):
                errors = np.zeros(len(analysis_states))
                for t_ind, xf, xref in zip(range(len(analysis_states)), analysis_states, reference_states):
                    errors[t_ind] = utility.calculate_errors(xf, xref, criterion=criterion, ignore_nan=True, second_is_truth=True)
                analysis_errors[c_ind] = errors.copy()
        free_run_errors = [None, None, None, None]
        if free_run_states is not None:
            # TODO: Add a check on times, and make sure they match
            if len(free_run_states) != len(reference_states):
                print("length of free_run_states does NOT match length of reference_states")
                raise AssertionError
            for c_ind, criterion in enumerate(criteria):
                errors = np.zeros(len(free_run_states))
                for t_ind, xf, xref in zip(range(len(free_run_states)), free_run_states, reference_states):
                    errors[t_ind] = utility.calculate_errors(xf, xref, criterion=criterion, ignore_nan=True, second_is_truth=True)
                free_run_errors[c_ind] = errors.copy()
                print("Observation Errors with criterion %s " % criteria[c_ind])
        #
        reference_errors.update({'free':free_run_errors, 'forecast':forecast_errors, 'analysis':analysis_errors})

    no_observation = True
    if observations is not None:
        no_observation = False
        # Forecast errors
        forecast_errors = [None, None, None, None]
        if forecast_observations is not None:
            pass
        elif forecast_states is not None:
            if model is not None:
                forecast_observations = []
                for x in forecast_states:
                    if x is not None:
                        y = model.evaluate_theoretical_observation(x)
                    else:
                        y = None
                    forecast_observations.append(y)
            else:
                print("The model is needed to generate observations from forecast states")
                raise ValueError
        else:
            print("Given the observations, either forecast states or forecasted observations are needed; both are None")
            raise ValueError

        # TODO: Add a check on times, and make sure they match
        if len(forecast_observations) != len(observations):
            print("length of forecast_observations does NOT match length of observations")
            raise AssertionError
        for c_ind, criterion in enumerate(criteria):
            errors = np.zeros(len(forecast_observations))
            for t_ind, xf, xref in zip(range(len(forecast_observations)), forecast_observations, observations):
                errors[t_ind] = utility.calculate_errors(xf, xref, criterion=criterion, ignore_nan=True, second_is_truth=True)
            forecast_errors[c_ind] = errors.copy()

        analysis_errors = [None, None, None, None]
        if analysis_observations is not None:
            pass
        elif analysis_states is not None:
            if model is not None:
                analysis_observations = []
                for x in analysis_states:
                    y = model.evaluate_theoretical_observation(x)
                    analysis_observations.append(y)
            else:
                print("The model is needed to generate observations from analysis states")
                raise ValueError
        else:
            print("Given the observations, either analysis states or analysed observations are needed; both are None")
            raise ValueError

        # TODO: Add a check on times, and make sure they match
        if len(analysis_observations) != len(observations):
            print("length of analysis_observations does NOT match length of observations")
            raise AssertionError
        for c_ind, criterion in enumerate(criteria):
            errors = np.zeros(len(analysis_observations))
            for t_ind, xf, xref in zip(range(len(analysis_observations)), analysis_observations, observations):
                errors[t_ind] = utility.calculate_errors(xf, xref, criterion=criterion, ignore_nan=True, second_is_truth=True)
            analysis_errors[c_ind] = errors.copy()

        free_run_errors = [None, None, None, None]
        if free_run_observations is not None:
            pass
        elif free_run_states is not None:
            if model is not None:
                free_run_observations = []
                for x in free_run_states:
                    if x is not None:
                        y = model.evaluate_theoretical_observation(x)
                    else:
                        y = None
                    free_run_observations.append(y)
            else:
                print("The model is needed to generate observations from free run states")
                # Just ignore free run
                # raise ValueError
        else:
            print("Given the observations, either free run states or analysed observations are needed; both are None")
            # Just ignore free run
            # raise ValueError

        for c_ind, criterion in enumerate(criteria):
            if free_run_times is None:
                errors = None
                free_run_errors[c_ind] = errors
            else:
                errors = np.empty(len(free_run_times))
                errors[...] = np.nan
                for t_ind, t in enumerate(free_run_times):
                    try:
                        obs_ind = np.where(observation_times==t)[0][0]
                        xf = free_run_observations[t_ind]
                        xref = observations[obs_ind]
                        errors[t_ind] = utility.calculate_errors(xf, xref, criterion=criterion, ignore_nan=True, second_is_truth=True)
                    except(IndexError):
                        continue
                free_run_errors[c_ind] = errors.copy()

        observation_errors.update({'free':free_run_errors, 'forecast':forecast_errors, 'analysis':analysis_errors})

    #
    # Start Creating plots:
    # ---------------------
    if not(reference_errors['forecast'] is reference_errors['analysis'] is None is reference_errors['free'] is None):
        valid_ref_errors = True
    else:
        valid_ref_errors = False

    if not(observation_errors['forecast'] is observation_errors['analysis'] is None is observation_errors['free'] is None):
        valid_obs_errors = True
    else:
        valid_obs_errors = False

    #
    if valid_ref_errors and valid_obs_errors:
        # Two rows; reference and Observations
        num_rows = 2
    elif valid_ref_errors:
        num_rows = 1
        target_errors = reference_errors
        err_name = 'Reference Errors'
        if logscale:
            err_name += ' (Log Scale)'
    elif valid_obs_errors:
        num_rows = 1
        target_errors = observation_errors
        err_name = 'Observation Errors'
        if logscale:
            err_name += ' (Log Scale)'
    else:
        num_rows = 0
        print("No Plots Could be generated; Couldn't calculate errors w.r.t. Reference Not Observaitons!")
        #

    #
    if plot_initial_time is not None:
        try:
            f_ref_ind = np.where(forecast_times>=plot_initial_time)[0][0]
        except(TypeError,ValueError):
            f_ref_ind = None
        except(IndexError):
            f_ref_ind = forecast_times.size
        try:
            a_ref_ind = np.where(analysis_times>=plot_initial_time)[0][0]
        except(TypeError,ValueError):
            a_ref_ind = None
        except(IndexError):
            a_ref_ind = forecast_times.size
        try:
            fr_ref_ind = np.where(free_run_times>=plot_initial_time)[0][0]
        except(TypeError,ValueError):
            fr_ref_ind = None
        except(IndexError):
            fr_ref_ind = forecast_times.size
    else:
        f_ref_ind = a_ref_ind = fr_ref_ind = 0

    #
    if num_rows == 0:
        fig = None
    elif num_rows == 1:

        lbl_1 = 'Forecast'
        lbl_2 = 'Analysis'
        lbl_3 = 'Free'
        #
        f_ref_errors = target_errors['forecast']
        a_ref_errors = target_errors['analysis']
        fr_ref_errors = target_errors['free']
        #

        #
        # RMSE-Only plot
        ind = 0
        single_fig = plt.figure(figsize=plt.figaspect(0.33))
        ax = single_fig.add_subplot(111)
        ax.minorticks_on()
        ax.grid(True, which='major', linestyle='-')
        ax.grid(True, which='minor', linestyle='-.')
        ax.set_ylabel("%s (%s)" % (err_name, criteria[ind].upper()))
        leg_labels = []
        leg_lines = []
        trgt_f_times = forecast_times[f_ref_ind+2: ]
        trgt_f_err = f_ref_errors[ind][f_ref_ind+2: ]
        trgt_a_times = analysis_times[a_ref_ind+1: -1]
        trgt_a_err = a_ref_errors[ind][a_ref_ind+1: -1]
        if logscale:
            # Forecast
            if f_ref_ind is not None:

                for i, [t0, t1, e0, e1] in enumerate(zip(trgt_a_times, trgt_f_times, trgt_a_err, trgt_f_err)):
                    if i == 0:
                        ln_1, = ax.semilogy([t0, t1], [e0, e1], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label=lbl_1)
                    else:
                        _ = ax.semilogy([t0, t1], [e0, e1], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth)
                leg_lines.append(ln_1)
                leg_labels.append(lbl_1)
            else:
                ln_1 = None

            # Analysis
            if a_ref_ind is not None:
                ln_2, = ax.semilogy(analysis_times[a_ref_ind: ], a_ref_errors[ind][a_ref_ind: ], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label=lbl_2)
                leg_lines.append(ln_2)
                leg_labels.append(lbl_2)
            else:
                ln_2 = None

            # Free Run
            if fr_ref_ind is not None:
                if fr_ref_errors[ind] is not None:
                    ln_3, = ax.semilogy(free_run_times[fr_ref_ind: ], fr_ref_errors[ind][fr_ref_ind: ], color='#EEC900', linestyle=(0,(3,1,1,1)), marker='s', linewidth=linewidth, label=lbl_3)
                    leg_lines.append(ln_3)
                    leg_labels.append(lbl_3)
                else:
                    ln_3 = None
            else:
                ln_3 = None
        else:
            # Forecast
            if f_ref_ind is not None:

                for i, [t0, t1, e0, e1] in enumerate(zip(trgt_a_times, trgt_f_times, trgt_a_err, trgt_f_err)):
                    if i == 0:
                        ln_1, = ax.plot([t0, t1], [e0, e1], color='#d62728', linestyle='-.', marker='d',  linewidth=linewidth, label=lbl_1)
                    else:
                        _ = ax.plot([t0, t1], [e0, e1], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth)
                leg_lines.append(ln_1)
                leg_labels.append(lbl_1)
            else:
                ln_1 = None

            # Analysis
            if a_ref_ind is not None:
                ln_2, = ax.plot(analysis_times[a_ref_ind: ], a_ref_errors[ind][a_ref_ind: ], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label=lbl_2)
                leg_lines.append(ln_2)
                leg_labels.append(lbl_2)
            else:
                ln_2 = None

            # Free Run
            if fr_ref_ind is not None:
                if fr_ref_errors[ind] is not None:
                    ln_3, = ax.plot(free_run_times[fr_ref_ind: ], fr_ref_errors[ind][fr_ref_ind: ], color='#EEC900', linestyle=(0,(3,1,1,1)), marker='s', linewidth=linewidth, label=lbl_3)
                    leg_lines.append(ln_3)
                    leg_labels.append(lbl_3)
                else:
                    ln_3 = None
            else:
                ln_3 = None

        #
        xticks = ax.get_xticks()
        xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in xticks]
        ax.set_xticklabels(xticklabels, rotation=60)

        # add legend to the whole figure
        single_fig.legend(tuple(leg_lines), tuple(leg_labels), 'upper center', ncol=3, framealpha=0.75, fancybox=True)
        # save figure
        if filename is not None:
            if plots_dir is not None:
                file_name = os.path.join(plots_dir, "%s_%s" % (criteria[ind].upper(), filename))
            else:
                file_name = "%s_%s" % (criteria[ind].upper(), filename)
            single_fig.savefig(file_name, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')

        #
        # ==========================
        # All-Measures error plots
        # ==========================
        fig, ax_arr = plt.subplots(1, 4, figsize=plt.figaspect(0.33), sharex=True)
        for ind, ax in enumerate(ax_arr):
            ax.minorticks_on()
            ax.grid(True, which='major', linestyle='-')
            ax.grid(True, which='minor', linestyle='-.')

            if ind == 0:
                ax.set_ylabel(err_name)

            #
            leg_labels = []
            leg_lines = []
            if logscale:
                if f_ref_ind is not None:
                    ln_1, = ax.semilogy(forecast_times[f_ref_ind: ] , f_ref_errors[ind][f_ref_ind: ], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label=lbl_1)
                    leg_lines.append(ln_1)
                    leg_labels.append(lbl_1)
                else:
                    ln_1 = None
                if a_ref_ind is not None:
                    ln_2, = ax.semilogy(analysis_times[a_ref_ind: ], a_ref_errors[ind][a_ref_ind: ], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label=lbl_2)
                    leg_lines.append(ln_2)
                    leg_labels.append(lbl_2)
                else:
                    ln_2 = None
                if fr_ref_ind is not None:
                    if fr_ref_errors[ind] is not None:
                        ln_3, = ax.semilogy(free_run_times[fr_ref_ind: ], fr_ref_errors[ind][fr_ref_ind: ], color='#EEC900', linestyle=(0,(3,1,1,1)), marker='s', linewidth=linewidth, label=lbl_3)
                        leg_lines.append(ln_3)
                        leg_labels.append(lbl_3)
                    else:
                        ln_3 = None
                else:
                    ln_3 = None
            else:
                if f_ref_ind is not None:
                    ln_1, = ax.plot(forecast_times[f_ref_ind: ] , f_ref_errors[ind][f_ref_ind: ], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label=lbl_1)
                    leg_lines.append(ln_1)
                    leg_labels.append(lbl_1)
                else:
                    ln_1 = None
                if a_ref_ind is not None:
                    ln_2, = ax.plot(analysis_times[a_ref_ind: ], a_ref_errors[ind][a_ref_ind: ], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label=lbl_2)
                    leg_lines.append(ln_2)
                    leg_labels.append(lbl_2)
                else:
                    ln_2 = None
                if fr_ref_ind is not None:
                    if fr_ref_errors[ind] is not None:
                        ln_3, = ax.plot(free_run_times[fr_ref_ind: ], fr_ref_errors[ind][fr_ref_ind: ], color='#EEC900', linestyle=(0,(3,1,1,1)), marker='s', linewidth=linewidth, label=lbl_3)
                        leg_lines.append(ln_3)
                        leg_labels.append(lbl_3)
                    else:
                        ln_3 = None
                else:
                    ln_3 = None
            ax.set_title(criteria[ind].upper())

            #
            xticks = ax.get_xticks()
            xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in xticks]
            ax.set_xticklabels(xticklabels, rotation=60)

        # add legend to the whole figure
        fig.legend(tuple(leg_lines), tuple(leg_labels), 'upper center', ncol=3, framealpha=0.75, fancybox=True)

    elif num_rows == 2:
        fig, ax_arr = plt.subplots(2, 4, sharex=True)

        lbl_1 = 'Forecast'
        lbl_2 = 'Analysis'
        lbl_3 = 'Free'

        for row_ind in range(2):
            if row_ind == 0:
                f_ref_errors = reference_errors['forecast']
                a_ref_errors = reference_errors['analysis']
                fr_ref_errors = reference_errors['free']
                err_name = 'Reference Errors'
                if logscale:
                    err_name += ' (Log Scale)'
            else:
                f_ref_errors = observation_errors['forecast']
                a_ref_errors = observation_errors['analysis']
                fr_ref_errors = observation_errors['free']
                err_name = 'Observation Errors'
                if logscale:
                    err_name += ' (Log Scale)'

            for ind, ax in enumerate(ax_arr[row_ind]):

                ax.minorticks_on()
                ax.grid(True, which='major', linestyle='-')
                ax.grid(True, which='minor', linestyle='-.')

                if ind == 0:
                    ax.set_ylabel(err_name)

                if logscale:
                    ln_1, = ax.semilogy(forecast_times, f_ref_errors[ind], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label=lbl_1)
                    ln_2, = ax.semilogy(analysis_times, a_ref_errors[ind], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label=lbl_2)
                    ln_3, = ax.semilogy(free_run_times, fr_ref_errors[ind], color='#EEC900', linestyle=(0,(3,1,1,1)), marker='s', linewidth=linewidth, label=lbl_3)
                else:
                    ln_1, = ax.plot(forecast_times, f_ref_errors[ind], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label=lbl_1)
                    ln_2, = ax.plot(analysis_times, a_ref_errors[ind], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label=lbl_2)
                    ln_3, = ax.plot(free_run_times, fr_ref_errors[ind], color='#EEC900', linestyle=(0,(3,1,1,1)), marker='s', linewidth=linewidth, label=lbl_3)
                ax.set_title(criteria[ind].upper())

                #
                if row_ind == num_rows-1:
                    xticks = ax.get_xticks()
                    xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in xticks]
                    ax.set_xticklabels(xticklabels, rotation=60)

        # add legend to the whole figure
        fig.legend((ln_1, ln_2, ln_3), (lbl_1, lbl_2, lbl_3), 'upper center', ncol=3, framealpha=0.75, fancybox=True)

    else:
        print("Impossible!")
        raise ValueError

    # save figure
    if fig is not None and filename is not None:
        if plots_dir is not None:
            file_name = os.path.join(plots_dir, filename)
        else:
            file_name = filename
        fig.savefig(file_name, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')

    return fig, single_fig, time_spans, reference_errors, observation_errors

def create_variance_trace_plot(times,
                               forecast_covariance_trace_repo=None,
                               analysis_covariance_trace_repo=None,
                               plots_dir=None,
                               filename=None,
                               plot_initial_time=None,
                               linewidth=2
                              ):
    """
    """
    forecast_covariance_trace_repo = np.asarray(forecast_covariance_trace_repo)   # each row corresponds to one time instance; columns are for prognostic variables
    analysis_covariance_trace_repo = np.asarray(analysis_covariance_trace_repo)
    if forecast_covariance_trace_repo.ndim==analysis_covariance_trace_repo.ndim==2:
        # print(forecast_covariance_trace_repo.shape, analysis_covariance_trace_repo.shape)
        assert np.size(forecast_covariance_trace_repo, 1) == np.size(analysis_covariance_trace_repo, 1) == 5, "trace repos are of wrong shape(s)"  # number of prognostic variables
        #
        overall_frcst_var_trace = np.sum(forecast_covariance_trace_repo, 1)
        overall_anals_var_trace = np.sum(analysis_covariance_trace_repo, 1)
        #
        if plot_initial_time is not None:
            try:
                init_ind = np.where(times>=plot_initial_time)[0][0]
            except(TypeError,ValueError):
                init_ind = None
            except(IndexError):
                init_ind = times.size
        else:
            init_ind = 0

        #
        # plot each of these things
        fig, axarr = plt.subplots(2, 3, figsize=plt.figaspect(0.33), sharex=True)
        # overall variance trace
        ax = axarr[0, 0]
        ax.plot(times[init_ind: ], overall_frcst_var_trace[init_ind: ], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label='Forecast')
        ax.plot(times[init_ind: ], overall_anals_var_trace[init_ind: ], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label='Analysis')
        ax.legend(loc='best')
        ax.set_title('Overall')
        # rho E
        ax = axarr[0, 2]
        ax.plot(times[init_ind: ], forecast_covariance_trace_repo[init_ind:, 4], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label='Forecast')
        ax.plot(times[init_ind: ], analysis_covariance_trace_repo[init_ind:, 4], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label='Analysis')
        ax.legend(loc='best')
        ax.set_title(r'$E$')
        # rho
        ax = axarr[0, 1]
        ax.plot(times[init_ind: ], forecast_covariance_trace_repo[init_ind:, 0], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label='Forecast')
        ax.plot(times[init_ind: ], analysis_covariance_trace_repo[init_ind:, 0], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label='Analysis')
        ax.legend(loc='best')
        ax.set_title(r'$\rho$')
        # rho u, rho v, rho w
        titles = [r'$\rho\, u$', r'$\rho\, v$', r'$\rho\, w$']
        for i in range(3):
            ax = axarr[1, i]
            ax.plot(times[init_ind: ], forecast_covariance_trace_repo[init_ind:, i+1], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label='Forecast')
            ax.plot(times[init_ind: ], analysis_covariance_trace_repo[init_ind:, i+1], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label='Analysis')
            ax.legend(loc='best')
            ax.set_title(titles[i])

        # times are scalars; so adjust xticks
        for row_ind, row in enumerate(axarr):
            for col_ind, ax in enumerate(row):
                if col_ind == 0:
                    ax.set_ylabel("Variance trace")
                if row_ind == 1:
                    xticks = ax.get_xticks()
                    xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in xticks]
                    ax.set_xticklabels(xticklabels, rotation=60)
                else:
                    ax.set_xticklabels([])
                    #

    elif forecast_covariance_trace_repo.ndim==analysis_covariance_trace_repo.ndim==1:
        # Single variable to consider plotting variance of (e.g., aggregated obsesrvation vector
        assert forecast_covariance_trace_repo.size == analysis_covariance_trace_repo.size, "trace repos are of wrong shape(s)"
        #
        #
        if plot_initial_time is not None:
            try:
                init_ind = np.where(times>=plot_initial_time)[0][0]
            except(TypeError,ValueError):
                init_ind = None
            except(IndexError):
                init_ind = times.size
        else:
            init_ind = 0

        #
        # plot each of these things
        fig = plt.figure(figsize=plt.figaspect(0.33))
        ax = fig.add_subplot(111)
        # overall variance trace
        ax.plot(times[init_ind: ], forecast_covariance_trace_repo[init_ind: ], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label='Forecast')
        ax.plot(times[init_ind: ], analysis_covariance_trace_repo[init_ind: ], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label='Analysis')
        xticks = ax.get_xticks().astype(int)
        ax.set_xticks(xticks)
        xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in xticks]
        ax.set_xticklabels(xticklabels, rotation=60)
        ax.set_ylabel("Variance trace")
        ax.legend(loc='best')
        ax.set_title('Overall')

    else:
        print("forecast_covariance_trace_repo.ndim: ", forecast_covariance_trace_repo.ndim)
        print("analysis_covariance_trace_repo.ndim: ", analysis_covariance_trace_repo.ndim)
        print("No figure is created for variance trace...")
        fig = None

    # save figure
    if fig is not None and filename is not None:
        if plots_dir is not None:
            file_name = os.path.join(plots_dir, filename)
        else:
            file_name = filename
        fig.savefig(file_name, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')

    return fig

def create_rank_histogram(ensemble, reference,
                          split_indexes=None,
                          split_labels=None,
                          first_var=0,
                          last_var=None,
                          var_skp=5,
                          draw_hist=True,
                          hist_type='relfreq',
                          hist_title=None,
                          hist_max_height=None,
                          add_fitted_beta=False,
                          ignore_nan=True,
                          add_uniform=True,
                          filename=None):
    """
    """
    if split_indexes is not None:
        # split_indexes is a list of iterables; in each entry is a list of indexes to plot on a subplot
        num_subplots = len(split_indexes)
        assert num_subplots > 0, "The length of split_indexes must be > 0"

        num_x_subplots = int(np.sqrt(num_subplots))
        num_y_subplots = int(np.ceil(num_subplots/num_x_subplots)) + num_subplots % num_x_subplots
        rem_axes = num_x_subplots*num_y_subplots - num_subplots

        if split_labels is None:
            split_labels = ['Var %d'%i for i in range(num_subplots)]
        else:
            if len(split_labels) != num_subplots:
                print("The passed labels must be of length %d" % num_subplots)
                raise ValueError

        # creage figure with subplots
        fig_hist, axarr = plt.subplots(num_x_subplots, num_y_subplots, sharex=True)
        if axarr.ndim == 1:
            axarr = axarr.reshape(1, max(num_x_subplots, num_y_subplots))
        subplot_index = 0
        for i in range(num_x_subplots):
            for j in range(num_y_subplots):
                ax = axarr[i, j]
                if subplot_index >= num_subplots:
                    # fig_hist.delaxes(ax)
                    ax.set_visible(False)
                    continue
                else:
                    indexes = np.asarray(split_indexes[subplot_index]).flatten()
                    split_ref = reference[indexes]
                    split_ens = [x[indexes] for x in ensemble]
                    #
                    ranks_freq, ranks_rel_freq, bins_bounds , fig_hist = rank_histogram(split_ens, split_ref,
                                                                                        first_var=first_var,
                                                                                        last_var=last_var,
                                                                                        var_skp=var_skp,
                                                                                        draw_hist=draw_hist,
                                                                                        hist_type=hist_type,
                                                                                        hist_title=hist_title,
                                                                                        hist_max_height=hist_max_height,
                                                                                        add_fitted_beta=add_fitted_beta,
                                                                                        target_fig=None,
                                                                                        target_ax=ax,
                                                                                        add_uniform=add_uniform
                                                                                        )
                    ax.text(0.5, 0.8, split_labels[subplot_index], transform=ax.transAxes)
                    # ylabel = ax.get_ylabel()
                    # ylabel = "%s - %s" % (ylabel, split_labels[subplot_index])
                    # ax.set_ylabel(ylabel)
                    #
                    subplot_index += 1

                if not (i==(num_x_subplots-1) or j==0):
                    ax.xaxis.set_visible(False)
                    ax.yaxis.set_visible(False)
                else:
                    if i != (num_x_subplots-1):  # last row
                        ax.xaxis.set_visible(False)
                    if j != 0:  # first column
                        ax.yaxis.set_visible(False)
                        #

        # Save figure
        if draw_hist and fig_hist is None:
            print("rank_histogrm didnt return a proper histogram figure while draw_hist is True!")
            raise ValueError
        elif fig_hist is not None:
            fig_hist.subplots_adjust(hspace=0.1, wspace=0.1)
        else:
            pass

    else:
        ranks_freq, ranks_rel_freq, bins_bounds , fig_hist = rank_histogram(ensemble, reference,
                                                                            first_var=first_var,
                                                                            last_var=last_var,
                                                                            var_skp=var_skp,
                                                                            draw_hist=draw_hist,
                                                                            hist_type=hist_type,
                                                                            hist_title=hist_title,
                                                                            hist_max_height=hist_max_height,
                                                                            add_fitted_beta=add_fitted_beta,
                                                                            target_fig=None,
                                                                            target_ax=None,
                                                                            add_uniform=add_uniform
                                                                            )
    # Save figure
    if draw_hist and fig_hist is None:
        print("rank_histogrm didnt return a proper histogram figure while draw_hist is True!")
        raise ValueError
    elif fig_hist is not None:
        fig_hist.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
    else:
        pass

    return ranks_freq, ranks_rel_freq, bins_bounds , fig_hist

def read_rmse_file(filename):
    """
    given the filename (full/relative path) of the rmse file; load times and RMSE values
    """
    print("Creating RMSE Plot")

    file_cont = np.loadtxt(filename, skiprows=3)
    observation_time    = file_cont[:, 0]
    initial_time        = file_cont[:, 1]
    final_time          = file_cont[:, 2]
    forecast_time       = file_cont[:, 3]
    analysis_time       = file_cont[:, 4]
    initial_rmse        = file_cont[:, 5]
    final_rmse          = file_cont[:, 6]
    forecast_rmse       = file_cont[:, 7]
    analysis_rmse       = file_cont[:, 8]
    observation_rmse    = file_cont[:, 9]

    if initial_time[0] < forecast_time[0]:
        forecast_time = np.concatenate(([initial_time[0]], forecast_time))
        forecast_rmse = np.concatenate(([initial_rmse[0]], forecast_rmse))

    return observation_time, initial_time, initial_rmse, final_time, final_rmse, forecast_time, forecast_rmse, analysis_time, analysis_rmse, observation_rmse

def recreate_model(output_dir_structure_file):
    """
    Lookup results directory, and recreate the forward operator
    """
    output_dir_strucutre = utility.get_output_dir_structure(output_dir_structure_file, full_paths=True)
    file_output_dir  = output_dir_strucutre['file_output_dir']
    model_states_dir = output_dir_strucutre['model_states_dir']
    observations_dir = output_dir_strucutre['observations_dir']
    statistics_dir   = output_dir_strucutre['statistics_dir']
    cycle_prefix     = output_dir_strucutre['cycle_prefix']

    # get model and observation configs, and create a forward model instance:
    model_configs_file = os.path.join(file_output_dir, 'setup.dat')
    model_configs, obs_configs = forward_model.read_model_configs(model_configs_file)

    # Lookup observation error model data
    obs_err_file = os.path.join(file_output_dir, "Observation_Error_Model.hpy")
    if os.path.isfile(obs_err_file):
        model = ForwardOperator(model_configs, obs_configs, dist_unit='M', obs_err_model_file=obs_err_file)
        print("Model created from file")
    else:
        model = ForwardOperator(model_configs, obs_configs, dist_unit='M', space_dependent_obs_err=False)
    return model

def quick_rank_histogram(frequency_repo=None, relative_frequency_repo=None, hist_title=None, init_ind=0):
    """
    """
    if isinstance(frequency_repo, (list, tuple, type(None))):
        pass
    elif isinstance(frequency_repo, np.ndarray):
        frequency_repo = [frequency_repo]
    else:
        print("frequency_repo is of unrecognized type; it must be either None, a numpy array or a list/tuple of np arrays, received %s " % type(frequency_repo))
        raise TypeError
    #
    if isinstance(relative_frequency_repo, (list, tuple, type(None))):
        pass
    elif isinstance(relative_frequency_repo, np.ndarray):
        relative_frequency_repo = [relative_frequency_repo]
    else:
        print("relative_frequency_repo is of unrecognized type; it must be either None, a numpy array or a list/tuple of np arrays, received %s " % type(relative_frequency_repo))
        raise TypeError

    if frequency_repo is relative_frequency_repo is None:
        aggreg_ranks = aggreg_rel_ranks = None
    elif frequency_repo is not None and relative_frequency_repo is not None:
        print("You are passing both frequencies and relative frequencies. Will check consistency")
        raise NotImplementedError("TODO:")
    elif frequency_repo is not None:
        # Total relative frequencies from individual frequencies
        aggreg_ranks = None
        for rnk_ind, rnk in enumerate(frequency_repo):
            if rnk_ind < init_ind:
                continue
            if rnk is None:
                pass
            elif aggreg_ranks is None:
                if np.isnan(np.asarray(rnk)).any():
                    pass
                else:
                    aggreg_ranks = np.asarray(rnk).copy().flatten()
            else:
                rnk = np.asarray(rnk).flatten()
                if np.isnan(rnk).any():
                    pass
                else:
                    aggreg_ranks += rnk
        if aggreg_ranks is not None:
            aggreg_rel_ranks = aggreg_ranks.astype(np.float) / aggreg_ranks.sum()
            # plot these relative frequencies
        else:
            aggreg_rel_ranks = None
    else:
        # Total relative frequencies from individual relative frequencies
        aggreg_rel_ranks = None
        iters = 0
        for rnk_ind, rnk in enumerate(relative_frequency_repo):
            if rnk_ind < init_ind:
                continue
            if rnk is None:
                pass
            elif aggreg_rel_ranks is None:
                if np.isnan(np.asarray(rnk)).any():
                    pass
                else:
                    aggreg_rel_ranks = np.asarray(rnk).copy().flatten()
                    iters += 1
            else:
                rnk = np.asarray(rnk).flatten()
                if np.isnan(rnk).any():
                    pass
                else:
                    aggreg_rel_ranks += rnk
                    iters += 1
        if aggreg_rel_ranks is not None:
            aggreg_rel_ranks /= iters
            # plot these relative frequencies

    if aggreg_rel_ranks is not None:
        # Based on hist_type decide on the height of histogram bars
        bins_bounds = np.arange(aggreg_rel_ranks.size)
        bins_heights = aggreg_rel_ranks
        ylabel = 'Relative Frequency'
        # Start plotting:
        fig, ax = plt.subplots(facecolor='white')
        ax.bar(bins_bounds , bins_heights, width=1, edgecolor='black')

        # Adjust limits of the plot as necessary:
        ax.set_xlim(bins_bounds[0]-0.5, bins_bounds[-1]+0.5)
        ax.set_ylim(0, bins_heights.max()*1.25)
        #
        if hist_title is not None and isinstance(hist_title, str):
            fig.suptitle(hist_title)
        ax.set_xlabel("Rank")
        ax.set_ylabel("Relative Frequency")
    else:
        fig = ax = None
    #
    return fig

def start_reading_results(output_dir_structure_file,
                          generate_free_run=True,
                          load_from_saved=True,
                          error_criteria=['rmse', 'rse', 'mae', 'rae']
                         ):
    """
    """
    if use_h5py:
        # h5py is imported; use it
        output = start_reading_h5py_results(output_dir_structure_file=output_dir_structure_file,
                                            generate_free_run=generate_free_run,
                                            load_from_saved=load_from_saved,
                                            error_criteria=error_criteria)
    else:
        # use pickle.
        output = start_reading_pickle_results(output_dir_structure_file=output_dir_structure_file,
                                              generate_free_run=generate_free_run,
                                              load_from_saved=load_from_saved,
                                              error_criteria=error_criteria)

    return output

def start_reading_h5py_results(output_dir_structure_file,
                               generate_free_run=True,
                               load_from_saved=True,
                               error_criteria=['rmse', 'rse', 'mae', 'rae'],
                               initialize_to_nan=True
                              ):
    """
    Check if h5py is available, imported, and ready to use.
        - If available, check the saved file;
        - If not found, look for pickled data.
        - If neither exist, recollect data, and save as h5py if available, otherwise, pickle.

    """
    # Get the results directories
    output_dir_strucutre = utility.get_output_dir_structure(output_dir_structure_file, full_paths=True)
    file_output_dir  = output_dir_strucutre['file_output_dir']
    model_states_dir = output_dir_strucutre['model_states_dir']
    observations_dir = output_dir_strucutre['observations_dir']
    statistics_dir   = output_dir_strucutre['statistics_dir']
    cycle_prefix     = output_dir_strucutre['cycle_prefix']

    if use_h5py:
        # h5py is imported; use it
        results_file = os.path.join(file_output_dir, "%s.hpy"%__COLLECTIVE_RES_FILENAME)
    else:
        # use pickle.
        print("Can't use this function without h5py library setup")
        print("the variable use_h5py is set to False!")
        raise ValueError()

    #
    if load_from_saved and not os.path.isfile(results_file):
            print("Can't find the results file: %s " % results_file)
            print("Will attempt to recollect data from cycles results")
            load_from_saved = False
    else:
        # either reconstruct, even if exists, or file is available and ready to load
        pass

    # get model and observation configs, and create a forward model instance:
    print("Recreating Forward Operator instance...")
    model = recreate_model(output_dir_structure_file)

    if load_from_saved:
        # File exists, load results from the collective results file
        print("Loading results from HDF5 (.hpy) file...")
        # Here, we return an HDF5 file, rather than loading everything in memory
        output_dict = {'results_file':results_file}
        #
    else:
        # Read RMSE results from statistics file:  TODO: This should be debracated!
        rmse_file = os.path.join(file_output_dir, statistics_dir, 'rmse.dat')
        observation_times, initial_times, initial_rmses, \
                final_times, final_rmses, forecast_times, forecast_rmses, \
                analysis_times, analysis_rmses, observation_rmse= read_rmse_file(rmse_file)

        # recollect results for each cycle, and create  results file
        states_num_cycles, states_found_cycle_dirs, states_missing_cycles = get_cycles_dirs(model_states_dir, cycle_prefix)
        observations_num_cycles, observations_found_cycle_dirs, observations_missing_cycles = get_cycles_dirs(observations_dir, cycle_prefix)
        #
        if states_num_cycles != observations_num_cycles:
            print("The number of observations is inconsistent with the number of cycles")
            raise ValueError
        else:
            print("Found %04d Assimilation cycles; starting to read saved results for each cycle..." % states_num_cycles)
            # print(states_found_cycle_dirs)
            # print(observations_found_cycle_dirs)

        # ************************************************************
        # Use HDF5 Format, and save things incrementally at
        # every cycle; don't keep things in memory
        # NOTE: Everything will be converted to Numpy arrays here.
        # It is your responsibility  to convert things to the desired
        # format, e.g. StateVector, StateEnsemble, etc.
        # ************************************************************
        state_size = model.state_size()
        observation_size = model.observation_size()
        ensemble_size = None
        num_err_criteria = len(error_criteria)
        #
        # Check if file is available, and append things, otherwise, create new
        if not h5py.is_hdf5(results_file):
            with h5py.File(results_file, 'w') as file_contents:
                # A flag that is set to True (on File) only after full proper update"
                file_contents.attrs['valid_contents'] = False

                # Create the dataset, and initialize the file; leave it empty
                experiment_group   = file_contents.create_group('Experiment')     # Timespans, etc
                states_group       = file_contents.create_group('States')        # States (forecast, analysis etc)
                observations_group = file_contents.create_group('Observations')  # Observations
                external_observations_group = observations_group.create_group("External")  # Subgroup in which an observation
                statistics_group   = file_contents.create_group('Statistics')    # RMSE, Innovations, RankHists, etc.
                rank_histograms_group = statistics_group.create_group("Rank_Histograms")
                #

                # Fill Experimental setup group
                experiment_group.create_dataset("experiment_tspan", (0, ), maxshape=(None,))
                experiment_group.create_dataset("forecast_times", (0, ), maxshape=(None,))
                experiment_group.create_dataset("analysis_times", (0, ), maxshape=(None,))
                experiment_group.create_dataset("reference_times", (0, ), maxshape=(None,))
                experiment_group.create_dataset("free_run_times", (0, ), maxshape=(None,))

                # 1- Statistics Group: Errors are (RMSE, RSE, MAE, RAE), respectively for each cycle
                statistics_group.create_dataset("state_forecast_errors", ((num_err_criteria, 0)), maxshape=(num_err_criteria, None))
                statistics_group.create_dataset("state_analysis_errors", ((num_err_criteria, 0)), maxshape=(num_err_criteria, None))
                statistics_group.create_dataset("state_free_run_errors", ((num_err_criteria, 0)), maxshape=(num_err_criteria, None))
                statistics_group.create_dataset("observation_forecast_errors", ((num_err_criteria, 0)), maxshape=(num_err_criteria, None))
                statistics_group.create_dataset("observation_analysis_errors", ((num_err_criteria, 0)), maxshape=(num_err_criteria, None))
                statistics_group.create_dataset("observation_free_run_errors", ((num_err_criteria, 0)), maxshape=(num_err_criteria, None))
                statistics_group.create_dataset("forecast_states_stdev", (state_size, 0), maxshape=(state_size, None))
                statistics_group.create_dataset("analysis_states_stdev", (state_size, 0), maxshape=(state_size, None))
                statistics_group.create_dataset("forecast_observations_stdev", (observation_size, 0), maxshape=(observation_size, None))
                statistics_group.create_dataset("analysis_observations_stdev", (observation_size, 0), maxshape=(observation_size, None))
                #

                # 2- States:
                states_group.create_dataset("forecast_states", (state_size, 0), maxshape=(state_size, None))
                states_group.create_dataset("analysis_states", (state_size, 0), maxshape=(state_size, None))
                states_group.create_dataset("reference_states", (state_size, 0), maxshape=(state_size, None))
                states_group.create_dataset("free_run_states", (state_size, 0), maxshape=(state_size, None))
                #

                # 3- Observations
                observations_group.create_dataset("observations", (observation_size, 0), maxshape=(state_size, None))
                observations_group.create_dataset("forecast_observations", (observation_size, 0), maxshape=(state_size, None))
                observations_group.create_dataset("analysis_observations", (observation_size, 0), maxshape=(state_size, None))
                observations_group.create_dataset("reference_observations", (observation_size, 0), maxshape=(state_size, None))
                observations_group.create_dataset("free_run_observations", (observation_size, 0), maxshape=(state_size, None))
                #

                # Set the attributes
                file_contents.attrs['last_cycle'] = -1  # so that when we load first time, initial cycle is zero
                file_contents.attrs['states_num_cycles'] = 0
                file_contents.attrs['observations_num_cycles'] = 0
                file_contents.attrs['state_size'] = state_size
                file_contents.attrs['observation_size'] = observation_size
                file_contents.attrs['error_criteria'] = error_criteria

                # Update validity flag
                file_contents.attrs['valid_contents'] = True

        print("HDF5 File initialized...")
        num_cycles = states_num_cycles
        if states_num_cycles == observations_num_cycles:
            pass
        elif observations_num_cycles == states_num_cycles-1:
            pass
        else:
            print("*** WARNING: States Observations are incompatible with states observations ***")
            # raise ValueError

        #
        with h5py.File(results_file, 'a') as file_contents:
            # Update validity flag
            file_contents.attrs['valid_contents'] = False

            initial_cycle = file_contents.attrs['last_cycle'] + 1

            # Resize existing datasets  NOTE: Everything will be saved w.r.t experiment timespan to avoid confusion
            file_contents["Experiment/experiment_tspan"].resize((num_cycles, ))
            # file_contents["Experiment/forecast_times"].resize((num_cycles, ))
            # file_contents["Experiment/analysis_times"].resize((num_cycles, ))
            # file_contents["Experiment/reference_times"].resize((num_cycles, ))
            # file_contents["Experiment/free_run_times"].resize((num_cycles, ))

            file_contents["States/forecast_states"].resize((state_size, num_cycles))
            file_contents["States/analysis_states"].resize((state_size, num_cycles))
            file_contents["States/reference_states"].resize((state_size, num_cycles))
            file_contents["States/free_run_states"].resize((state_size, num_cycles))

            file_contents["Observations/observations"].resize((observation_size, num_cycles))
            file_contents["Observations/forecast_observations"].resize((observation_size, num_cycles))
            file_contents["Observations/analysis_observations"].resize((observation_size, num_cycles))
            file_contents["Observations/reference_observations"].resize((observation_size, num_cycles))
            file_contents["Observations/free_run_observations"].resize((observation_size, num_cycles))
            for external_dataset in file_contents["Observations/External"].keys():
                file_contents["Observations/External"][external_dataset].resize((observation_size, num_cycles))

            file_contents["Statistics/state_forecast_errors"].resize((num_err_criteria, num_cycles))
            file_contents["Statistics/state_analysis_errors"].resize((num_err_criteria, num_cycles))
            file_contents["Statistics/state_free_run_errors"].resize((num_err_criteria, num_cycles))
            file_contents["Statistics/observation_forecast_errors"].resize((num_err_criteria, num_cycles))
            file_contents["Statistics/observation_analysis_errors"].resize((num_err_criteria, num_cycles))
            file_contents["Statistics/observation_free_run_errors"].resize((num_err_criteria, num_cycles))
            file_contents["Statistics/forecast_states_stdev"].resize((state_size, num_cycles))
            file_contents["Statistics/analysis_states_stdev"].resize((state_size, num_cycles))
            file_contents["Statistics/forecast_observations_stdev"].resize((observation_size, num_cycles))
            file_contents["Statistics/analysis_observations_stdev"].resize((observation_size, num_cycles))

            # Initialize everthing to np.NaN to avoid using zero-value instead of missing data; TODO: Maybe, we want to be selective here!
            if initialize_to_nan:
                file_contents["Experiment/experiment_tspan"][initial_cycle: ] = np.NaN
                file_contents["States/forecast_states"][:, initial_cycle: ] = np.NaN
                file_contents["States/analysis_states"][:, initial_cycle: ] = np.NaN
                file_contents["States/reference_states"][:, initial_cycle: ] = np.NaN
                file_contents["States/free_run_states"][:, initial_cycle: ] = np.NaN
                file_contents["Observations/observations"][:, initial_cycle: ] = np.NaN
                file_contents["Observations/forecast_observations"][:, initial_cycle: ] = np.NaN
                file_contents["Observations/analysis_observations"][:, initial_cycle: ] = np.NaN
                file_contents["Observations/reference_observations"][:, initial_cycle: ] = np.NaN
                file_contents["Observations/free_run_observations"][:, initial_cycle: ] = np.NaN
                for external_dataset in file_contents["Observations/External"].keys():
                    file_contents["Observations/External"][external_dataset][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/state_forecast_errors"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/state_analysis_errors"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/state_free_run_errors"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/observation_forecast_errors"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/observation_analysis_errors"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/observation_free_run_errors"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/forecast_states_stdev"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/analysis_states_stdev"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/forecast_observations_stdev"][:, initial_cycle: ] = np.NaN
                file_contents["Statistics/analysis_observations_stdev"][:, initial_cycle: ] = np.NaN

            # Loop over cycles, and append stuff to the created file
            for c_ind in range(num_cycles):
                # Skip previously saved cycles; maybe add some sanity checks if needed!
                if c_ind < initial_cycle:
                    continue

                print("Reading results for Cycle: %d / %d " % (c_ind+1, num_cycles))
                file_base_name = 'cycle_%04d'%c_ind  # prefix of files saved furing this cycle

                cycle_states_dir = os.path.join(model_states_dir, states_found_cycle_dirs[c_ind])
                cycle_observations_dir = os.path.join(observations_dir, observations_found_cycle_dirs[c_ind])

                cycle_tspan,  cycle_configs, \
                    forecast_time, forecast_state, forecast_ensemble, forecast_rmse, \
                    analysis_time, analysis_state, analysis_ensemble, analysis_rmse, observation_rmse, \
                    reference_time, reference_state, \
                    observation_time, observation, external_observations,\
                    initial_rmse, final_rmse = read_cycle_results(model, file_output_dir, cycle_states_dir, cycle_observations_dir)

                # Check ensemble size
                if analysis_ensemble is not None:
                    if ensemble_size is None:
                        ensemble_size = analysis_ensemble.size
                    elif analysis_ensemble.size != ensemble_size:
                        print("Analsysis Ensemble Size (%d) Found doesn't match ensemble size(%d) !" % (analysis_ensemble.size, ensemble_size))
                        raise ValueError
                if forecast_ensemble is not None:
                    if ensemble_size is None:
                        ensemble_size = forecast_ensemble.size
                    elif forecast_ensemble.size != ensemble_size:
                        print("Forecast Ensemble Size (%d) Found doesn't match ensemble size(%d) !" % (forecast_ensemble.size, ensemble_size))
                        raise ValueError

                # Lookup/Create Free Run State
                if generate_free_run:  # for all cycles
                    print("Generating a free-run state/observation")
                    #
                    free_run_state_file = os.path.join(cycle_states_dir, __FREE_RUN_STATE_FILENAME)
                    if c_ind == 0:
                        try:
                            free_run_state = DLidarVec.state_vector_from_file(free_run_state_file)
                            print("Successfully loaded free state from file")
                        except:
                            if forecast_state is not None:
                                free_run_state = forecast_state.copy()
                                print("Got free IC from forecast state of the first cycle")
                            elif forecast_ensemble is not None:
                                free_run_state = forecast_ensemble.mean()
                                print("Got free IC from averaging forecast ensemble of the first cycle")
                            elif analysis_state is not None:
                                print("Got free IC from analysis state of the first cycle")
                                free_run_state = analysis_state.copy()
                            elif analysis_ensemble is not None:
                                print("Got free IC from averaging analysis ensemble of the first cycle")
                                free_run_state = analysis_ensemble.mean()
                            else:
                                print("The initial cycle doesn't contain any analysis/forecast information that can be used to infer initial state for a free run!")
                                raise ValueError
                            # Save free-run-state to file
                            free_run_state.write_to_file(free_run_state_file)
                    else:
                        try:
                            loaded_state = DLidarVec.state_vector_from_file(free_run_state_file)
                            free_run_state = loaded_state
                            print("Successfully loaded free state from file")
                        except:
                            print("Failed to load: %s " % free_run_state_file)
                            print("Integrating from the previously loaded free state")
                            # Need to propagate previous one!
                            _, _traject = model.integrate_state(free_run_state, cycle_tspan)
                            free_run_state = _traject[-1]
                            free_run_state.write_to_file(state_file)
                    # Create a free run Observation from the free-run-observation
                    free_run_observation = model.evaluate_theoretical_observation(free_run_state)
                else:
                    # No free run state/observation
                    free_run_state = free_run_observation = None

                # Update model observation operator (if necessary)
                model.update_observation_operator(observation=observation)

                # Update experiment_tspan;
                if c_ind == 0:
                    if cycle_tspan[0] != cycle_tspan[-1]:
                        print("** WARNINIG: The times in the initial cycle do not match. \nThis means the initial cycle is not properly saved. Initial cycle is ignored.***")
                file_contents["Experiment/experiment_tspan"][c_ind] = cycle_tspan[-1]

                # Update States and Observations
                if forecast_state is not None:
                    file_contents["States/forecast_states"][:, c_ind] = forecast_state[:]
                    Hx = model.evaluate_theoretical_observation(forecast_state)
                    file_contents["Observations/forecast_observations"][:, c_ind] = Hx[:]
                else:
                    file_contents["States/forecast_states"][:, c_ind] = np.NaN
                    file_contents["Observations/forecast_observations"][:, c_ind] = np.NaN

                if analysis_state is not None:
                    file_contents["States/analysis_states"][:, c_ind] = analysis_state[:]
                    Hx = model.evaluate_theoretical_observation(analysis_state)
                    file_contents["Observations/analysis_observations"][:, c_ind] = Hx[:]
                else:
                    file_contents["States/analysis_states"][:, c_ind] = np.NaN
                    file_contents["Observations/analysis_observations"][:, c_ind] = np.NaN

                if reference_state is not None:
                    file_contents["States/reference_states"][:, c_ind] = reference_state[:]
                    Hx = model.evaluate_theoretical_observation(reference_state)
                    file_contents["Observations/reference_observations"][:, c_ind] = Hx[:]
                else:
                    file_contents["States/reference_states"][:, c_ind] = np.NaN
                    file_contents["Observations/reference_observations"][:, c_ind] = np.NaN

                if free_run_state is not None:
                    file_contents["States/free_run_states"][:, c_ind] = free_run_state[:]
                    Hx = model.evaluate_theoretical_observation(free_run_state)
                    file_contents["Observations/free_run_observations"][:, c_ind] = Hx[:]
                else:
                    file_contents["States/free_run_states"][:, c_ind] = np.NaN
                    file_contents["Observations/free_run_observations"][:, c_ind] = np.NaN

                if observation is not None:
                    file_contents["Observations/observations"][:, c_ind] = observation[:]
                else:
                    file_contents["Observations/observations"][:, c_ind] = np.NaN

                if external_observations is None:
                    for external_dataset in file_contents["Observations/External"].keys():
                        file_contents["Observations/External"][external_dataset][:, c_ind] = np.NaN
                elif len(external_observations) == 0:
                    for external_dataset in file_contents["Observations/External"].keys():
                        file_contents["Observations/External"][external_dataset][:, c_ind] = np.NaN
                else:
                    # Look for keys inside passed external observations, and add them to the dictionary
                    avail_keys = file_contents["Observations/External"].keys()
                    for source in external_observations.keys():
                        ext_obs = external_observations[source]
                        if source in avail_keys:
                            file_contents["Observations/External"][source][:, c_ind] = ext_obs[:]
                        else:
                            # prepend with Non's if previous points were missing
                            file_contents["Observations/External"].create_dataset(source, (observation_size, num_cycles), maxshape=(observation_size, None))
                            file_contents["Observations/External"][source][:, c_ind] = ext_obs[:]
                            #
                    # Now update other keys not in external_observations
                    rem_keys = list(set.difference(set([k for k in avail_keys]), set([k for k in external_observations.keys()])))
                    for external_dataset in rem_keys:
                        file_contents["Observations/External"][external_dataset][:, c_ind] = np.NaN
                        #

                # Update Statistics
                # i   Calculate state-errors
                if reference_state is not None:
                    if forecast_state is not None:
                        state_forecast_errors = [utility.calculate_errors(forecast_state, reference_state, criterion) for criterion in error_criteria]
                    else:
                        state_forecast_errors = [np.NaN] * len(error_criteria)
                    if analysis_state is not None:
                        state_analysis_errors = [utility.calculate_errors(analysis_state, reference_state, criterion) for criterion in error_criteria]
                    else:
                        state_analysis_errors = [np.NaN] * len(error_criteria)
                    if free_run_state is not None:
                        state_free_run_errors = [utility.calculate_errors(free_run_state, reference_state, criterion) for criterion in error_criteria]
                    else:
                        state_free_run_errors = [np.NaN] * len(error_criteria)
                else:
                    state_forecast_errors = state_analysis_errors = state_free_run_errors = [np.NaN] * len(error_criteria)
                    #
                # update data file
                file_contents["Statistics/state_forecast_errors"][:, c_ind] = state_forecast_errors
                file_contents["Statistics/state_analysis_errors"][:, c_ind] = state_analysis_errors
                file_contents["Statistics/state_free_run_errors"][:, c_ind] = state_free_run_errors

                # ii  Calculate observation-errors
                if observation is not None:
                    Hx = file_contents["Observations/forecast_observations"][:, c_ind]
                    observation_forecast_errors = [utility.calculate_errors(Hx, observation, criterion) for criterion in error_criteria]
                    Hx = file_contents["Observations/analysis_observations"][:, c_ind]
                    observation_analysis_errors = [utility.calculate_errors(Hx, observation, criterion) for criterion in error_criteria]
                    Hx = file_contents["Observations/free_run_observations"][:, c_ind]
                    observation_free_run_errors = [utility.calculate_errors(Hx, observation, criterion) for criterion in error_criteria]
                else:
                    observation_forecast_errors = observation_analysis_errors = observation_free_run_errors = [np.NaN] * len(error_criteria)
                # update data file
                file_contents["Statistics/observation_forecast_errors"][:, c_ind] = observation_forecast_errors
                file_contents["Statistics/observation_analysis_errors"][:, c_ind] = observation_analysis_errors
                file_contents["Statistics/observation_free_run_errors"][:, c_ind] = observation_free_run_errors

                # iii Calculate second-order (and order) statistics
                nan_state = np.empty(state_size)
                nan_state[:] = np.NaN
                nan_observation = np.empty(observation_size)
                nan_observation[:] = np.NaN
                #
                if forecast_ensemble is not None:
                    forecast_states_stdev = forecast_ensemble.stdev()
                    forecast_observation_ensemble = utility.state_to_observation_ensemble(forecast_ensemble, model)
                    forecast_observations_stdev = forecast_observation_ensemble.stdev()
                    if reference_state is not None:
                        forecast_ranks_freq, forecast_ranks_rel_freq, forecast_bins_bounds, \
                                _ = create_rank_histogram(forecast_ensemble, reference_state, draw_hist=False)
                    else:
                        forecast_ranks_freq = forecast_ranks_rel_freq = forecast_bins_bounds = None
                    if observation is not None:
                        obs_forecast_ranks_freq, obs_forecast_ranks_rel_freq, obs_forecast_bins_bounds, \
                                _ = create_rank_histogram(forecast_observation_ensemble, observation, draw_hist=False)
                    else:
                        obs_forecast_ranks_freq = obs_forecast_ranks_rel_freq = obs_forecast_bins_bounds = None
                else:
                    forecast_states_stdev = nan_state
                    forecast_observations_stdev = nan_observation
                    forecast_ranks_freq = forecast_ranks_rel_freq = forecast_bins_bounds = None
                    obs_forecast_ranks_freq = obs_forecast_ranks_rel_freq = obs_forecast_bins_bounds = None
                #
                if analysis_ensemble is not None:
                    analysis_states_stdev = analysis_ensemble.stdev()
                    analysis_observation_ensemble = utility.state_to_observation_ensemble(analysis_ensemble, model)
                    analysis_observations_stdev = analysis_observation_ensemble.stdev()
                    if reference_state is not None:
                        analysis_ranks_freq, analysis_ranks_rel_freq, analysis_bins_bounds, \
                                _ = create_rank_histogram(analysis_ensemble, reference_state, draw_hist=False)
                    else:
                        analysis_ranks_freq = analysis_ranks_rel_freq = analysis_bins_bounds = None
                    if observation is not None:
                        obs_analysis_ranks_freq, obs_analysis_ranks_rel_freq, obs_analysis_bins_bounds, \
                                _ = create_rank_histogram(analysis_observation_ensemble, observation, draw_hist=False)
                    else:
                        obs_analysis_ranks_freq = obs_analysis_ranks_rel_freq = obs_analysis_bins_bounds = None
                else:
                    analysis_states_stdev = nan_state
                    analysis_observations_stdev = nan_observation
                    analysis_ranks_freq = analysis_ranks_rel_freq = analysis_bins_bounds = None
                    obs_analysis_ranks_freq = obs_analysis_ranks_rel_freq = obs_analysis_bins_bounds = None
                # update data file
                file_contents["Statistics/forecast_states_stdev"][:, c_ind] = forecast_states_stdev
                file_contents["Statistics/analysis_states_stdev"][:, c_ind] = analysis_states_stdev
                file_contents["Statistics/forecast_observations_stdev"][:, c_ind] = forecast_observations_stdev
                file_contents["Statistics/analysis_observations_stdev"][:, c_ind] = analysis_observations_stdev
                # Check second order statistics
                if forecast_ranks_freq is not None:
                    try:
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_frequency"][:, c_ind] = forecast_ranks_freq
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_relative_frequency"][:, c_ind] = forecast_ranks_rel_freq
                        file_contents["Statistics/Rank_Histograms/forecast_bins_bounds"][:, c_ind] = forecast_bins_bounds
                    except(ValueError):
                        # TODO: Maybe add a check to make sure the sizes (old/new) are right!
                        # h5py raises a ValueError for accessing wrong index
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_frequency"].resize((len(forecast_ranks_freq), num_cycles))
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_frequency"][:, c_ind] = forecast_ranks_freq
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_relative_frequency"].resize((len(forecast_ranks_rel_freq), num_cycles))
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_relative_frequency"][:, c_ind] = forecast_ranks_rel_freq
                        file_contents["Statistics/Rank_Histograms/forecast_bins_bounds"].resize((len(forecast_bins_bounds), num_cycles))
                        file_contents["Statistics/Rank_Histograms/forecast_bins_bounds"][:, c_ind] = forecast_bins_bounds
                    except(KeyError):
                        file_contents["Statistics/Rank_Histograms"].create_dataset('forecast_ranks_frequency',
                                                                                   (len(forecast_ranks_freq), num_cycles),
                                                                                   maxshape=(len(forecast_ranks_freq), None))
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_frequency"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_frequency"][:, c_ind] = forecast_ranks_freq
                        #
                        file_contents["Statistics/Rank_Histograms"].create_dataset('forecast_ranks_relative_frequency',
                                                                                   (len(forecast_ranks_rel_freq), num_cycles),
                                                                                   maxshape=(len(forecast_ranks_rel_freq), None))
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_relative_frequency"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/forecast_ranks_relative_frequency"][:, c_ind] = forecast_ranks_rel_freq
                        #
                        file_contents["Statistics/Rank_Histograms"].create_dataset('forecast_bins_bounds',
                                                                                   (len(forecast_bins_bounds), num_cycles),
                                                                                   maxshape=(len(forecast_bins_bounds), None))
                        file_contents["Statistics/Rank_Histograms/forecast_bins_bounds"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/forecast_bins_bounds"][:, c_ind] = forecast_bins_bounds
                    print("<"*100)
                        #
                if analysis_ranks_freq is not None:
                    try:
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_frequency"][:, c_ind] = analysis_ranks_freq
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_relative_frequency"][:, c_ind] = analysis_ranks_rel_freq
                        file_contents["Statistics/Rank_Histograms/analysis_bins_bounds"][:, c_ind] = analysis_bins_bounds
                    except(ValueError):
                        # TODO: Maybe add a check to make sure the sizes (old/new) are right!
                        # h5py raises a ValueError for accessing wrong index
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_frequency"].resize((len(analysis_ranks_freq), num_cycles))
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_frequency"][:, c_ind] = analysis_ranks_freq
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_relative_frequency"].resize((len(analysis_ranks_rel_freq), num_cycles))
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_relative_frequency"][:, c_ind] = analysis_ranks_rel_freq
                        file_contents["Statistics/Rank_Histograms/analysis_bins_bounds"].resize((len(analysis_bins_bounds), num_cycles))
                        file_contents["Statistics/Rank_Histograms/analysis_bins_bounds"][:, c_ind] = analysis_bins_bounds
                    except(KeyError):
                        file_contents["Statistics/Rank_Histograms"].create_dataset('analysis_ranks_frequency',
                                                                                   (len(analysis_ranks_freq), num_cycles),
                                                                                   maxshape=(len(analysis_ranks_freq), None))
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_frequency"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_frequency"][:, c_ind] = analysis_ranks_freq
                        #
                        file_contents["Statistics/Rank_Histograms"].create_dataset('analysis_ranks_relative_frequency',
                                                                                   (len(analysis_ranks_rel_freq), num_cycles),
                                                                                   maxshape=(len(analysis_ranks_rel_freq), None))
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_relative_frequency"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/analysis_ranks_relative_frequency"][:, c_ind] = analysis_ranks_rel_freq
                        #
                        file_contents["Statistics/Rank_Histograms"].create_dataset('analysis_bins_bounds',
                                                                                   (len(analysis_bins_bounds), num_cycles),
                                                                                   maxshape=(len(analysis_bins_bounds), None))
                        file_contents["Statistics/Rank_Histograms/analysis_bins_bounds"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/analysis_bins_bounds"][:, c_ind] = analysis_bins_bounds
                        #
                # Repeat the same with observations
                if obs_forecast_ranks_freq is not None:
                    try:
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_frequency"][:, c_ind] = obs_forecast_ranks_freq
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_relative_frequency"][:, c_ind] = obs_forecast_ranks_rel_freq
                        file_contents["Statistics/Rank_Histograms/observation_forecast_bins_bounds"][:, c_ind] = obs_forecast_bins_bounds
                    except(ValueError):
                        # TODO: Maybe add a check to make sure the sizes (old/new) are right!
                        # h5py raises a ValueError for accessing wrong index
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_frequency"].resize((len(obs_forecast_ranks_freq), num_cycles))
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_frequency"][:, c_ind] = obs_forecast_ranks_freq
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_relative_frequency"].resize((len(obs_forecast_ranks_rel_freq), num_cycles))
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_relative_frequency"][:, c_ind] = obs_forecast_ranks_rel_freq
                        file_contents["Statistics/Rank_Histograms/observation_forecast_bins_bounds"].resize((len(obs_forecast_bins_bounds), num_cycles))
                        file_contents["Statistics/Rank_Histograms/observation_forecast_bins_bounds"][:, c_ind] = obs_forecast_bins_bounds
                    except(KeyError):
                        file_contents["Statistics/Rank_Histograms"].create_dataset('observation_forecast_ranks_frequency',
                                                                                   (len(obs_forecast_ranks_freq), num_cycles),
                                                                                   maxshape=(len(obs_forecast_ranks_freq), None))
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_frequency"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_frequency"][:, c_ind] = obs_forecast_ranks_freq
                        #
                        file_contents["Statistics/Rank_Histograms"].create_dataset('observation_forecast_ranks_relative_frequency',
                                                                                   (len(obs_forecast_ranks_rel_freq), num_cycles),
                                                                                   maxshape=(len(obs_forecast_ranks_rel_freq), None))
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_relative_frequency"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/observation_forecast_ranks_relative_frequency"][:, c_ind] = obs_forecast_ranks_rel_freq
                        #
                        file_contents["Statistics/Rank_Histograms"].create_dataset('observation_forecast_bins_bounds',
                                                                                   (len(obs_forecast_bins_bounds), num_cycles),
                                                                                   maxshape=(len(obs_forecast_bins_bounds), None))
                        file_contents["Statistics/Rank_Histograms/observation_forecast_bins_bounds"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/observation_forecast_bins_bounds"][:, c_ind] = obs_forecast_bins_bounds
                        #
                if obs_analysis_ranks_freq is not None:
                    try:
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_frequency"][:, c_ind] = obs_analysis_ranks_freq
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_relative_frequency"][:, c_ind] = obs_analysis_ranks_rel_freq
                        file_contents["Statistics/Rank_Histograms/observation_analysis_bins_bounds"][:, c_ind] = obs_analysis_bins_bounds
                    except(ValueError):
                        # TODO: Maybe add a check to make sure the sizes (old/new) are right!
                        # h5py raises a ValueError for accessing wrong index
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_frequency"].resize((len(obs_analysis_ranks_freq), num_cycles))
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_frequency"][:, c_ind] = obs_analysis_ranks_freq
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_relative_frequency"].resize((len(obs_analysis_ranks_rel_freq), num_cycles))
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_relative_frequency"][:, c_ind] = obs_analysis_ranks_rel_freq
                        file_contents["Statistics/Rank_Histograms/observation_analysis_bins_bounds"].resize((len(obs_analysis_bins_bounds), num_cycles))
                        file_contents["Statistics/Rank_Histograms/observation_analysis_bins_bounds"][:, c_ind] = obs_analysis_bins_bounds
                    except(KeyError):
                        file_contents["Statistics/Rank_Histograms"].create_dataset('observation_analysis_ranks_frequency',
                                                                                   (len(obs_analysis_ranks_freq), num_cycles),
                                                                                   maxshape=(len(obs_analysis_ranks_freq), None))
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_frequency"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_frequency"][:, c_ind] = obs_analysis_ranks_freq
                        #
                        file_contents["Statistics/Rank_Histograms"].create_dataset('observation_analysis_ranks_relative_frequency',
                                                                                   (len(obs_analysis_ranks_rel_freq), num_cycles),
                                                                                   maxshape=(len(obs_analysis_ranks_rel_freq), None))
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_relative_frequency"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/observation_analysis_ranks_relative_frequency"][:, c_ind] = obs_analysis_ranks_rel_freq
                        #
                        file_contents["Statistics/Rank_Histograms"].create_dataset('observation_analysis_bins_bounds',
                                                                                   (len(obs_analysis_bins_bounds), num_cycles),
                                                                                   maxshape=(len(obs_analysis_bins_bounds), None))
                        file_contents["Statistics/Rank_Histograms/observation_analysis_bins_bounds"][:, :c_ind] = np.NaN
                        file_contents["Statistics/Rank_Histograms/observation_analysis_bins_bounds"][:, c_ind] = obs_analysis_bins_bounds
                        #


                # Update Attributes
                file_contents.attrs['last_cycle'] = c_ind
                file_contents.flush()
            #
            # Close file, and return file pointer, or filename to be loaded  # TODO: Decide to avoid not closing hte file!
            file_contents.attrs['states_num_cycles'] = num_cycles
            file_contents.attrs['observations_num_cycles'] = num_cycles
            file_contents.attrs['ensemble_size'] = ensemble_size

            # Update validity flag
            file_contents.attrs['valid_contents'] = True
            #


    #
    # Only a filename is returned; nothing else for h5py
    output_dict = dict(results_file=results_file, model=model, file_output_dir=file_output_dir)

    return output_dict

def start_reading_pickle_results(output_dir_structure_file,
                          generate_free_run=True,
                          load_from_saved=True,
                         ):
    """
    Args:
        generate_free_run: use the initial analysis_ensemble/state to generate a free run over all timepoints
    """
    # Get the results directories
    output_dir_strucutre = utility.get_output_dir_structure(output_dir_structure_file, full_paths=True)
    file_output_dir  = output_dir_strucutre['file_output_dir']
    model_states_dir = output_dir_strucutre['model_states_dir']
    observations_dir = output_dir_strucutre['observations_dir']
    statistics_dir   = output_dir_strucutre['statistics_dir']
    cycle_prefix     = output_dir_strucutre['cycle_prefix']

    # get model and observation configs, and create a forward model instance:
    print("Recreating Forward Operator instance...")
    model = recreate_model(output_dir_structure_file)

    #
    results_file = os.path.join(file_output_dir, "%s.pickle"%__COLLECTIVE_RES_FILENAME)
    if load_from_saved:
        if not os.path.isfile(results_file):
            print("Can't find the pickle results file: %s " % results_file)
            print("Will attempt to recollect data from cycles results")
            load_from_saved = False
        else:
            pass

    if not load_from_saved:
        # read data from sources, and save to collective results file
        pass

        # Start reading results:
        # 1- get states results
        states_num_cycles, states_found_cycle_dirs, states_missing_cycles = get_cycles_dirs(model_states_dir, cycle_prefix)
        observations_num_cycles, observations_found_cycle_dirs, observations_missing_cycles = get_cycles_dirs(observations_dir, cycle_prefix)
        if states_num_cycles != observations_num_cycles:
            print("The number of observations is inconsistent with the number of cycles")
            raise ValueError
        else:
            print("Found %04d Assimilation cycles; starting to read saved results for each cycle..." % states_num_cycles)
            # print(states_found_cycle_dirs)
            # print(observations_found_cycle_dirs)

        # Creating an output  directory for checkpoints and plots:
        plots_dir = os.path.join(file_output_dir, plots_dir_name)
        plots_dir = create_output_dir(plots_dir, remove_existing=overwrite)
        #

        # Read RMSE results from statistics file:
        rmse_file = os.path.join(file_output_dir, statistics_dir, 'rmse.dat')
        observation_times, initial_times, initial_rmses, \
                final_times, final_rmses, forecast_times, forecast_rmses, \
                analysis_times, analysis_rmses, observation_rmse= read_rmse_file(rmse_file)

        # placeholders
        experiment_tspan    = np.empty(states_num_cycles+1)
        experiment_tspan[:] = np.NaN
        forecast_times      = np.empty(states_num_cycles)
        forecast_times[:]   = np.NaN
        forecast_rmses      = np.empty(states_num_cycles)
        forecast_rmses[:]   = np.NaN
        analysis_times      = forecast_times.copy()
        reference_times     = []
        analysis_rmses      = forecast_rmses.copy()
        observation_rmses   = forecast_rmses.copy()
        free_run_times      = []
        free_run_rmses      = []
        free_IC = None

        # observed states and observations
        observations     = []
        forecast_states  = []
        analysis_states  = []
        reference_states = []
        free_run_states  = []
        forecast_observations = []
        analysis_observations = []
        free_run_observations = []
        forecast_observed_errors = []
        analysis_observed_errors = []

        # Observations such as WRF obs
        external_observations_dict = {}

        # containers for Covariance trace (forecast/analysis) corresponding to each of the prognostic variables (total trace is the sum)
        forecast_covariance_trace_repo = []
        analysis_covariance_trace_repo = []
        #
        obs_forecast_stdev_repo          = []
        obs_analysis_stdev_repo          = []

        # containers for rank histogram results for all cycles
        forecast_ranks_freq_repo         = []
        analysis_ranks_freq_repo         = []
        obs_forecast_ranks_freq_repo     = []
        obs_analysis_ranks_freq_repo     = []
        forecast_ranks_rel_freq_repo     = []
        analysis_ranks_rel_freq_repo     = []
        obs_forecast_ranks_rel_freq_repo = []
        obs_analysis_ranks_rel_freq_repo = []
        forecast_bins_bounds_repo        = []
        analysis_bins_bounds_repo        = []
        obs_forecast_bins_bounds_repo    = []
        obs_analysis_bins_bounds_repo    = []

        # Read states, observations, and statistics for each cycle:
        for c_ind in range(states_num_cycles):
            print("Reading results for Cycle: %d / %d " % (c_ind+1, states_num_cycles))
            file_base_name = 'cycle_%04d'%c_ind  # prefix of files saved furing this cycle

            cycle_states_dir = os.path.join(model_states_dir, states_found_cycle_dirs[c_ind])
            cycle_observations_dir = os.path.join(observations_dir, observations_found_cycle_dirs[c_ind])

            cycle_tspan,  cycle_configs, \
                forecast_time, forecast_state, forecast_ensemble, forecast_rmse, \
                analysis_time, analysis_state, analysis_ensemble, analysis_rmse, observation_rmse, \
                reference_time, reference_state, \
                observation_time, observation, external_observations,\
                initial_rmse, final_rmse = read_cycle_results(model, file_output_dir, cycle_states_dir, cycle_observations_dir)

            #
            model.update_observation_operator(observation=observation)

            if reference_state is not None:
                reference_states.append(reference_state)
                reference_times.append(reference_time)
            else:
                reference_states.append(None)
                reference_times.append(None)


            if analysis_state is not None:
                analysis_states.append(analysis_state.get_np_array())
                Hxa = model.evaluate_theoretical_observation(analysis_state)
                analysis_observations.append(Hxa.get_np_array())
            else:
                analysis_states.append(None)
                Hxa = None
                analysis_observations.append(Hxa)

            if forecast_state is not None:
                forecast_states.append(forecast_state.get_np_array())
                Hxf = model.evaluate_theoretical_observation(forecast_state)
                forecast_observations.append(Hxf.get_np_array())
            else:
                forecast_states.append(None)
                Hxf = None
                forecast_observations.append(Hxf)

            if observation is not None:
                observations.append(observation.get_np_array())
                # If an observation is given, calculate the mismatch to forecast and analysis state, projected into the observation space
                if Hxf is not None:
                    frcst_obs_err = observation.axpy(-1, Hxf, in_place=False).get_np_array()
                    forecast_observed_errors.append(frcst_obs_err)
                else:
                    frcst_obs_err = None
                if Hxa is not None:
                    anl_obs_err = observation.axpy(-1, Hxa, in_place=False).get_np_array()
                    analysis_observed_errors.append(anl_obs_err)
                else:
                    anl_obs_err = None

                # print("frcst_obs_err, anl_obs_err: ", frcst_obs_err, anl_obs_err)
            else:
                observations.append(observation)

            if external_observations is None:
                for key in external_observations_dict:
                    external_observations_dict[key].append(None)
            elif len(external_observations) == 0:
                for key in external_observations_dict:
                    external_observations_dict[key].append(None)
            else:
                # Look for keys inside passed external observations, and add them to the dictionary
                avail_keys = external_observations_dict.keys()
                for source in external_observations.keys():
                    ext_obs = external_observations[source]
                    if source in avail_keys:
                        # print("Found %s in the loaded external_observations_dict" % (source))
                        # print("Appending: ", ext_obs)
                        external_observations_dict[source].append(ext_obs)
                    else:
                        # prepend with Non's if previous points were missing
                        # print("Source %s is New to external_observations_dict" % (source))
                        init_obs_list = ([None]*c_ind) + [ext_obs]
                        # print("Creating new Entry with initial data: ", init_obs_list)
                        external_observations_dict.update({source:init_obs_list})

                # Now update other keys not in external_observations
                rem_keys = list(set.difference(set([k for k in avail_keys]), set([k for k in external_observations.keys()])))
                for source in rem_keys:
                   # print("Missing info about source %s Adding None" % source)
                   external_observations_dict[source].append(None)

            # updated times:  TODO: DEBUG/REMOVE!
            if c_ind == 0:
                experiment_tspan[0: 2] = cycle_tspan[:]
            else:
                if not np.isclose(cycle_tspan[0], experiment_tspan[c_ind], rtol=_TIME_REL_TOL):
                    if c_ind == 1:
                        experiment_tspan[c_ind+1] = cycle_tspan[-1]
                        pass
                    else:
                        print("The initial time of this timespan doesn't coincide with the final time of the previous assimilation cycle!")
                        print("cycle_tspan", cycle_tspan)
                        print("experiment_tspan[%d]: %f " % ( c_ind, experiment_tspan[c_ind]))
                        # raise ValueError
                else:
                    experiment_tspan[c_ind+1] = cycle_tspan[-1]
                    #
            analysis_times[c_ind] = analysis_time
            forecast_times[c_ind] = forecast_time

            # update RMSEs:
            forecast_rmses[c_ind]    = forecast_rmse
            analysis_rmses[c_ind]    = analysis_rmse
            observation_rmses[c_ind] = observation_rmse

            # Check if a free run is needed, and grap IC for it
            # Initialize and integrate free-run state
            if c_ind == 0 and generate_free_run:
                print("Generating a free-run initial condition")

            if generate_free_run and reference_state is None:
                print("Will create free-run for observations only.")
                # generate_free_run = False
            else:
                pass

            if generate_free_run:  # for all cycles
                print("Generating a free-run state+RMSE")

                if c_ind == 0:
                    #
                    try:
                        state_file = os.path.join(cycle_states_dir, 'free_run_state.dlvec')
                        free_IC = DLidarVec.state_vector_from_file(state_file)
                        print("Successfully loaded free state from file")
                    except:
                        #
                        if forecast_state is not None:
                            free_IC = forecast_state.copy()
                            print("Got free IC from forecast state of the first cycle")
                        elif forecast_ensemble is not None:
                            free_IC = forecast_ensemble.mean()
                            print("Got free IC from averaging forecast ensemble of the first cycle")
                        elif analysis_state is not None:
                            print("Got free IC from analysis state of the first cycle")
                            free_IC = analysis_state.copy()
                        elif analysis_ensemble is not None:
                            print("Got free IC from averaging analysis ensemble of the first cycle")
                            free_IC = analysis_ensemble.mean()
                        else:
                            print("The initial cycle doesn't contain any analysis/forecast information that can be used to infer initial state for a free run!")
                            raise ValueError
                        #
                        state_file = os.path.join(cycle_states_dir, 'free_run_state.dlvec')
                        free_IC.write_to_file(state_file)

                    #  free run time
                    # free_IC_time = cycle_tspan[0]
                    free_IC_time = free_IC.time
                    # print("***\nFREE IC TIME: %s \n***\n" % str(free_IC.time))
                    # free_IC.time = free_IC_time
                    free_rmse = np.NaN
                    #
                    if cycle_tspan[0] <= free_IC.time < cycle_tspan[-1]:
                        free_run_states.append(free_IC.get_np_array())
                        free_run_times.append(free_IC.time)
                        free_run_rmses.append(free_rmse)
                        #
                        # get the observation
                        free_obs = model.evaluate_theoretical_observation(free_IC)
                        free_run_observations.append(free_obs)

                else:
                    try:
                        state_file = os.path.join(cycle_states_dir, 'free_run_state.dlvec')
                        loaded_free_IC = DLidarVec.state_vector_from_file(state_file)
                        free_IC = loaded_free_IC
                        print("Successfully loaded free state from file")
                    except:
                        print("Failed to load: %s " % state_file)
                        pass

                    if free_IC is None:
                        print("Failed to find the free_IC after the first cycle!")
                        raise ValueError

                # print("Cycle %d; free_IC_time, tspan" %c_ind, free_IC_time, cycle_tspan)

                # Double check free_IC and reference_state
                if free_IC is None:
                    print("Tried to generate a free run trajectory, but didn't find free_IC!")
                    raise ValueError
                if reference_state is None:
                    print("Couldn't find reference state at this cycle %d to calculate free-run RMSE" % c_ind)
                    # raise ValueError
                else:
                    reference_time = reference_state.time

                #
                if free_IC.time < cycle_tspan[-1]:
                    free_tspan, trajectory = model.integrate_state(free_IC, checkpoints=[free_IC.time, cycle_tspan[-1]])
                    free_IC = trajectory[-1]
                    free_IC.time = cycle_tspan[-1]
                    free_IC_time = free_IC.time

                    state_file = os.path.join(cycle_states_dir, 'free_run_state.dlvec')
                    free_IC.write_to_file(state_file)
                    print("free run state written to file")

                    # if c_ind > 1:
                    if reference_state is None:
                        free_rmse = np.NaN
                    elif np.isclose(free_IC_time, reference_time, rtol=_TIME_REL_TOL):
                        free_rmse = utility.calculate_rmse(free_IC, reference_state)
                    else:
                        free_rmse = np.NaN
                        print("Reference time doesn't match free-run time. Passing...")

                    # update free-run rmse and time
                    free_run_rmses.append(free_rmse)

                if free_IC.time not in free_run_times:
                    free_run_states.append(free_IC.copy())
                    free_run_times.append(free_IC.time)

                    free_obs = model.evaluate_theoretical_observation(free_IC)
                    free_run_observations.append(free_obs)

                # save the free run to file
                state_file = os.path.join(cycle_states_dir, 'free_run_state')
                free_IC.write_to_file(state_file)
                #
            else:
                # no free run is needed; del this branch after debugging
                free_IC = None
                free_run_states.append(None)
                free_run_times.append(None)
                #
                free_run_observations.append(None)

            # Get rank histogram stuff
            if forecast_ensemble is not None:
                if reference_state is not None:
                    f_out = create_rank_histogram(forecast_ensemble, reference_state, draw_hist=False)
                    forecast_ranks_freq, forecast_ranks_rel_freq, forecast_bins_bounds, _ = f_out
                    #
                    #
                else:
                    forecast_ranks_freq = forecast_ranks_rel_freq = forecast_bins_bounds = None

                # Repeat with observations
                if observation is not None:
                    forecast_obs_ensemble = utility.state_to_observation_ensemble(forecast_ensemble, model)
                    f_out = create_rank_histogram(forecast_obs_ensemble, observation, draw_hist=False)
                    obs_forecast_ranks_freq, obs_forecast_ranks_rel_freq, obs_forecast_bins_bounds, _ = f_out
                    #
                    forecast_obs_ensemble_stdev = forecast_obs_ensemble.stdev()
                else:
                    obs_forecast_ranks_freq = obs_forecast_ranks_rel_freq = obs_forecast_bins_bounds = None
                    forecast_obs_ensemble_stdev = None
                #
            else:
                forecast_ranks_freq = forecast_ranks_rel_freq = forecast_bins_bounds = None
                obs_forecast_ranks_freq = obs_forecast_ranks_rel_freq = obs_forecast_bins_bounds = None
                forecast_obs_ensemble_stdev = None

            # update Forecast-reference repos
            forecast_ranks_freq_repo.append(forecast_ranks_freq)
            forecast_ranks_rel_freq_repo.append(forecast_ranks_rel_freq)
            forecast_bins_bounds_repo.append(forecast_bins_bounds)
            # update Forecast-observation repos
            obs_forecast_ranks_freq_repo.append(obs_forecast_ranks_freq)
            obs_forecast_ranks_rel_freq_repo.append(obs_forecast_ranks_rel_freq)
            obs_forecast_bins_bounds_repo.append(obs_forecast_bins_bounds)
            obs_forecast_stdev_repo.append(forecast_obs_ensemble_stdev)

            if analysis_ensemble is not None:
                if reference_state is not None:
                    f_out = create_rank_histogram(analysis_ensemble, reference_state, draw_hist=False)
                    analysis_ranks_freq, analysis_ranks_rel_freq, analysis_bins_bounds, _ = f_out
                    #
                    #
                else:
                    analysis_ranks_freq = analysis_ranks_rel_freq = analysis_bins_bounds = None

                # Repeat with observations
                if observation is not None:
                    analysis_obs_ensemble = utility.state_to_observation_ensemble(analysis_ensemble, model)
                    f_out = create_rank_histogram(analysis_obs_ensemble, observation, draw_hist=False)
                    obs_analysis_ranks_freq, obs_analysis_ranks_rel_freq, obs_analysis_bins_bounds, _ = f_out
                    #
                    analysis_obs_ensemble_stdev =  analysis_obs_ensemble.stdev()
                else:
                    obs_analysis_ranks_freq = obs_analysis_ranks_rel_freq = obs_analysis_bins_bounds = None
                    analysis_obs_ensemble_stdev = None
                #
            else:
                analysis_ranks_freq = analysis_ranks_rel_freq = analysis_bins_bounds = None
                obs_analysis_ranks_freq = obs_analysis_ranks_rel_freq = obs_analysis_bins_bounds = None
                analysis_obs_ensemble_stdev = None

            # update Forecast-reference repos
            analysis_ranks_freq_repo.append(analysis_ranks_freq)
            analysis_ranks_rel_freq_repo.append(analysis_ranks_rel_freq)
            analysis_bins_bounds_repo.append(analysis_bins_bounds)
            # update Forecast-observation repos
            obs_analysis_ranks_freq_repo.append(obs_analysis_ranks_freq)
            obs_analysis_ranks_rel_freq_repo.append(obs_analysis_ranks_rel_freq)
            obs_analysis_bins_bounds_repo.append(obs_analysis_bins_bounds)
            obs_analysis_stdev_repo.append(analysis_obs_ensemble_stdev)

            #
            # Covariance Trace
            if not (forecast_ensemble is analysis_ensemble is None):
                var_indexes = model.get_model_prognostic_variables_indexes()
                frcst_trace = [np.NaN] * len(var_indexes)
                anals_trace = [np.NaN] * len(var_indexes)
                #
                if forecast_ensemble is not None:
                    var = forecast_ensemble.variance()
                    frcst_trace = [var[ind].sum() for ind in var_indexes]
                if analysis_ensemble is not None:
                    var = analysis_ensemble.variance()
                    anals_trace = [var[ind].sum() for ind in var_indexes]
                #
            else:
                frcst_trace = anals_trace = [np.NaN] * len(var_indexes)
            #
            forecast_covariance_trace_repo.append(frcst_trace)
            analysis_covariance_trace_repo.append(anals_trace)
            #

        #
        free_run_times = np.asarray(free_run_times)
        forecast_covariance_trace_repo = np.array(forecast_covariance_trace_repo)
        analysis_covariance_trace_repo = np.array(analysis_covariance_trace_repo)
        experiment_tspan = np.unique(experiment_tspan)
        output_dict = dict(experiment_tspan=experiment_tspan,
                           forecast_times=forecast_times,
                           analysis_times=analysis_times,
                           reference_times=reference_times,
                           free_run_times=free_run_times,
                           forecast_rmses=forecast_rmses,
                           analysis_rmses=analysis_rmses,
                           observation_rmses=observation_rmses,
                           free_run_rmses=free_run_rmses,
                           forecast_states=forecast_states,
                           reference_states=reference_states,
                           analysis_states=analysis_states,
                           free_run_states=free_run_states,
                           observations=observations,
                           external_observations=external_observations_dict,
                           forecast_observations=forecast_observations,
                           analysis_observations=analysis_observations,
                           free_run_observations=free_run_observations,
                           forecast_observed_errors=forecast_observed_errors,
                           analysis_observed_errors=analysis_observed_errors,
                           forecast_ranks_freq_repo=forecast_ranks_freq_repo,
                           analysis_ranks_freq_repo=analysis_ranks_freq_repo,
                           obs_forecast_ranks_freq_repo=obs_forecast_ranks_freq_repo,
                           obs_analysis_ranks_freq_repo=obs_analysis_ranks_freq_repo,
                           forecast_ranks_rel_freq_repo=forecast_ranks_rel_freq_repo,
                           analysis_ranks_rel_freq_repo=analysis_ranks_rel_freq_repo,
                           obs_forecast_ranks_rel_freq_repo=obs_forecast_ranks_rel_freq_repo,
                           obs_analysis_ranks_rel_freq_repo=obs_analysis_ranks_rel_freq_repo,
                           obs_forecast_stdev_repo=obs_forecast_stdev_repo,
                           obs_analysis_stdev_repo=obs_analysis_stdev_repo,
                           forecast_bins_bounds_repo=forecast_bins_bounds_repo,
                           analysis_bins_bounds_repo=analysis_bins_bounds_repo,
                           obs_analysis_bins_bounds_repo=obs_analysis_bins_bounds_repo,
                           forecast_covariance_trace_repo=forecast_covariance_trace_repo,
                           analysis_covariance_trace_repo=analysis_covariance_trace_repo
                          )
        try:
            pickle.dump(output_dict, open(os.path.join(file_output_dir, "%s.pickle"%__COLLECTIVE_RES_FILENAME), 'wb'))
        except:
            print("Failed to write results dictionary")
        #
    else:
        # Loading from pickled file
        print("Loading results from pickled file...")
        output_dict = pickle.load(open(results_file, 'rb'))
    #
    # update dictionary with model
    output_dict.update({'model':model})
    #
    return output_dict

def start_reading_and_plotting(output_dir_structure_file,
                               skip_plotting_spinup=False,
                               generate_free_run=False,
                               rank_histograms_per_cycle=True,
                               state_plot_per_cycle=True,
                               observation_plot_per_cycle=True,
                               quiver_mask_size=None,
                               slices_index=None,
                               threeD=True,
                               plot_gates=None,
                               overwrite=True,
                               load_from_saved=False,
                               unify_scales=False,
                               fontsize=10,
                               linewidth=2,
                               plots_dir_name='PLOTS',
                               observation_profile_x_axis_lims=None
                              ):
    """
    """
    if use_h5py:
        if plot_gates is None:
            plot_gates = __DEF_PLOT_GATES  # TODO: this should be moved to __main__
        output = start_reading_h5py_and_plotting(output_dir_structure_file=output_dir_structure_file,
                                                 skip_plotting_spinup=skip_plotting_spinup,
                                                 generate_free_run=generate_free_run,
                                                 rank_histograms_per_cycle=rank_histograms_per_cycle,
                                                 state_plot_per_cycle=state_plot_per_cycle,
                                                 observation_plot_per_cycle=observation_plot_per_cycle,
                                                 quiver_mask_size=quiver_mask_size,
                                                 slices_index=slices_index,
                                                 threeD=threeD,
                                                 plot_gates=plot_gates,
                                                 overwrite=overwrite,
                                                 load_from_saved=load_from_saved,
                                                 unify_scales=unify_scales,
                                                 fontsize=fontsize,
                                                 linewidth=linewidth,
                                                 plots_dir_name=plots_dir_name,
                                                 observation_profile_x_axis_lims=observation_profile_x_axis_lims
                                                )
    else:
        output = start_reading_pickle_and_plotting(output_dir_structure_file=output_dir_structure_file,
                                                   skip_plotting_spinup=skip_plotting_spinup,
                                                   generate_free_run=generate_free_run,
                                                   rank_histograms_per_cycle=rank_histograms_per_cycle,
                                                   state_plot_per_cycle=state_plot_per_cycle,
                                                   observation_plot_per_cycle=observation_plot_per_cycle,
                                                   quiver_mask_size=quiver_mask_size,
                                                   slices_index=slices_index,
                                                   threeD=threeD,
                                                   overwrite=overwrite,
                                                   load_from_saved=load_from_saved,
                                                   unify_scales=unify_scales,
                                                   fontsize=fontsize,
                                                   plots_dir_name=plots_dir_name,
                                                   observation_profile_x_axis_lims=observation_profile_x_axis_lims
                                                  )
    return output

def start_reading_h5py_and_plotting(output_dir_structure_file,
                                    skip_plotting_spinup=False,
                                    generate_free_run=False,
                                    rank_histograms_per_cycle=True,
                                    state_plot_per_cycle=True,
                                    observation_plot_per_cycle=True,
                                    quiver_mask_size=None,
                                    slices_index=None,
                                    plot_gates=None,
                                    add_cone=True,
                                    threeD=True,
                                    overwrite=True,
                                    load_from_saved=False,
                                    unify_scales=False,
                                    fontsize=10,
                                    linewidth=1.5,
                                    plots_dir_name='PLOTS',
                                    observation_profile_x_axis_lims=None
                                   ):
    """
    Given the results file that contains results dictionary generated by 'start_reading_and_plotting', load and plot results

    Args:
        *
        plot_gates: gate numbers to plot; or None to avoid plotting
        *

    """
    results_dict = start_reading_results(output_dir_structure_file=output_dir_structure_file,
                                         generate_free_run=generate_free_run,
                                         load_from_saved=load_from_saved)

    try:
        results_file = results_dict['results_file']
        if not h5py.is_hdf5(results_file):
            raise TypeError
        # Check results  validity flag
        with h5py.File(results_file, 'r') as file_contents:
            valid_contents = file_contents.attrs['valid_contents']
        if not valid_contents:
            print("The contents of the results file is inorrect; mostly reading a cycle was interrupted. Please rebuild results file")
            raise ValueError

    except(KeyError):
        print("This function requires h5py reading mode")
        print("The results dict must contain 'results_file' entry!")
        raise
    except(TypeError):
        print("The results file found in the returned results_dict is not a valid hdf5 file!")
        raise

    model = results_dict['model']
    # state_size = model.state_size()
    # observation_size = model.observation_size()

    # Creating an output  directory for checkpoints and plots:
    try:
        file_output_dir = results_dict['file_output_dir']
    except(KeyError):
        output_dir_strucutre = utility.get_output_dir_structure(output_dir_structure_file, full_paths=True)
        file_output_dir  = output_dir_strucutre['file_output_dir']
    plots_dir = os.path.join(file_output_dir, plots_dir_name)
    plots_dir = create_output_dir(plots_dir, remove_existing=overwrite)

    # Enhance plotter
    plots_enhancer(fontsize=fontsize)

    # TODO: Consider Closing-and-Reopening the file to reduce memory consumption (test if this is actually needed!)
    # Start reading and plotting
    with h5py.File(results_file) as file_contents:
        # Access the needed data from file (don't load everything to the memory
        # 1) Read attributess
        state_size       = file_contents.attrs['state_size']
        observation_size = file_contents.attrs['observation_size']
        ensemble_size    = file_contents.attrs['ensemble_size']
        if state_size != model.state_size():
            print("Stored state size on file (%d) doesn't match model state size %d" % (state_size, model.state_size()))
            raise ValueError
        if observation_size != model.observation_size():
            print("Stored observation size on file (%d) doesn't match model observatioin size %d" % (observation_size, model.observation_size()))
            raise ValueError

        # 2) Read experiment settings...
        experiment_tspan = file_contents["Experiment/experiment_tspan"][...]
        num_cycles = experiment_tspan.size

        if skip_plotting_spinup:
            skip_initial_time = experiment_tspan[experiment_tspan.size//3]
            plot_initial_time = skip_initial_time
        else:
            skip_initial_time = plot_initial_time = None

        # 3) Read/Plot Statistics
        state_forecast_errors       = file_contents["Statistics/state_forecast_errors"][...]
        state_analysis_errors       = file_contents["Statistics/state_analysis_errors"][...]
        state_free_run_errors       = file_contents["Statistics/state_free_run_errors"][...]
        observation_forecast_errors = file_contents["Statistics/observation_forecast_errors"][...]
        observation_analysis_errors = file_contents["Statistics/observation_analysis_errors"][...]
        observation_free_run_errors = file_contents["Statistics/observation_free_run_errors"][...]

        valid_state_errors = np.any(~np.isnan(state_forecast_errors)) or \
                np.any(~np.isnan(state_analysis_errors)) or np.any(~np.isnan(state_free_run_errors))

        valid_observation_errors = np.any(~np.isnan(observation_forecast_errors)) or \
                np.any(~np.isnan(observation_analysis_errors)) or np.any(~np.isnan(observation_free_run_errors))

        # CREATE ERROR PLOTS
        print("Creating Statistics Plots...")
        lbl_1, lbl_2, lbl_3 = ['Forecast', 'Analysis', 'Free']
        error_criteria = file_contents.attrs['error_criteria']
        num_error_criteria = len(error_criteria)
        #
        for target in ['state', 'observation']:
            if target=='state' and valid_state_errors:
                forecast_errors = state_forecast_errors
                analysis_errors = state_analysis_errors
                free_run_errors = state_free_run_errors
                base_filename = "State_Errors"
            elif target=='observation' and valid_observation_errors:
                forecast_errors = observation_forecast_errors
                analysis_errors = observation_analysis_errors
                free_run_errors = observation_free_run_errors
                base_filename = "Observation_Errors"
            else:
                # Nothing to do; skip this target
                continue

            # With/Without Log-Scale
            for logscale in [False, True]:
                for initial_time in list({None, skip_initial_time}):
                    for first_only in [False, True]:
                        if first_only:
                            err_name = error_criteria[0].upper()
                            filename = "%s_%s" % (base_filename, err_name)
                        else:
                            err_name = 'Errors'
                            filename = "%s" % base_filename
                        filename += "_InitTime_%s" % str(initial_time)
                        if logscale:
                            filename += "_LogScale"
                        fig_filename = filename + ".%s" % (_FIG_FORMAT)
                        if logscale:
                            err_name += ' (Log Scale)'

                        if plots_dir is not None:
                            fig_filename = os.path.join(plots_dir, fig_filename)

                        # Plot without free-run
                        if first_only:
                            fig = plt.figure(figsize=plt.figaspect(0.33))
                            ax = fig.add_subplot(111)
                            ax_arr = [ax]
                        else:
                            fig, ax_arr = plt.subplots(nrows=1, figsize=plt.figaspect(0.33), ncols=num_error_criteria, sharex=True)
                        for ax_ind in range(num_error_criteria):
                            if first_only and ax_ind > 0:
                                break
                            ax = ax_arr[ax_ind]
                            # add plots
                            plot = ax.semilogy if logscale else ax.plot
                            # The forecast should be plotted as segments not a continueous line
                            for i, [t0, t1, e0, e1] in enumerate(zip(experiment_tspan[: -1], experiment_tspan[1: ], analysis_errors[ax_ind, :-1], forecast_errors[ax_ind, 1:])):
                                if i == 0:
                                    ln_1, = plot([t0, t1], [e0, e1], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth, label=lbl_1)
                                else:
                                    _ = plot([t0, t1], [e0, e1], color='#d62728', linestyle='-.', marker='d', linewidth=linewidth)

                            ln_2, = plot(experiment_tspan, analysis_errors[ax_ind, :], color='#1f77b4', linestyle='--', marker='o', linewidth=linewidth, label=lbl_2)
                            ln_3, = plot(experiment_tspan, free_run_errors[ax_ind, :], color='#EEC900', linestyle=(0,(3,1,1,1)), marker='s', linewidth=linewidth, label=lbl_3)

                            # axis grid, ticks, and labels
                            ax.minorticks_on()
                            ax.grid(True, which='major', linestyle='-')
                            ax.grid(True, which='minor', linestyle='-.')
                            ax.set_title(error_criteria[ax_ind].upper())
                            if ax_ind == 0:
                                ax.set_ylabel(err_name)
                            xticks = ax.get_xticks()
                            xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in xticks]
                            ax.set_xticklabels(xticklabels, rotation=60)

                        # add legend and save
                        fig.legend((ln_1, ln_2, ln_3), (lbl_1, lbl_2, lbl_3), 'upper center', ncol=3, framealpha=0.75, fancybox=True)
                        fig.savefig(fig_filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')

                    # OnlyRMSE plot


        # Create Variance/Trace Plots
        # -- states --
        # Todo: this will only generate overall; we need to convert to reshape standard deviations before summing
        if False:
            raise NotImplementedError("TODO; Comment this out untill you impelemente it; or just take the overall variance")
        else:
            forecast_covariance_trace = np.nansum(file_contents["Statistics/forecast_states_stdev"][...]**2, axis=0)
            analysis_covariance_trace = np.nansum(file_contents["Statistics/analysis_states_stdev"][...]**2, axis=0)
            filename = os.path.join(plots_dir, "State_Variance_Trace.%s" % _FIG_FORMAT)
            fig = create_variance_trace_plot(experiment_tspan, forecast_covariance_trace, analysis_covariance_trace, filename=filename, plots_dir=plots_dir)
            plt.close(fig)

        # -- observations --
        forecast_covariance_trace = np.nansum(file_contents["Statistics/forecast_observations_stdev"][...]**2, axis=0)
        analysis_covariance_trace = np.nansum(file_contents["Statistics/analysis_observations_stdev"][...]**2, axis=0)
        filename = os.path.join(plots_dir, "Observation_Variance_Trace.%s" % _FIG_FORMAT)
        fig = create_variance_trace_plot(experiment_tspan, forecast_covariance_trace, analysis_covariance_trace, filename=filename, plots_dir=plots_dir)
        plt.close(fig)


        # All-gates observations (Stare only)
        print("Creating Observations Profile (for Stare data only)...")
        observation_grid = model.observation_grid(coordinate_system='spherical')
        stare_indexes, _ = split_observation_indexes(observation_grid)
        obs_heights = model.observation_grid()[:, -1]
        obs_dx =  model.get_observation_configs()['range_gate_length']
        ylims = [0, obs_heights.max()+obs_dx]
        model_grid = model.get_model_grid()
        model_zlims = [model_grid[:,  -1].min(), model_grid[:,  -1].max()]
        obs_progvar  = model.get_observation_configs()['prog_var']

        observations = file_contents["Observations/observations"]
        forecast_observations = file_contents["Observations/forecast_observations"]
        analysis_observations = file_contents["Observations/analysis_observations"]
        reference_observations = file_contents["Observations/reference_observations"]
        free_run_observations = file_contents["Observations/free_run_observations"]
        try:
            external_observations = file_contents["Observations/External"]
        except(KeyError, ValueError):
            external_observations = None
        analysis_observations_stdev = file_contents["Statistics/analysis_observations_stdev"]
        for c_ind in range(num_cycles):
            predictioin_stdev = analysis_observations_stdev[stare_indexes, c_ind]
            y = observations[stare_indexes, c_ind]
            yf = forecast_observations[stare_indexes, c_ind]
            ya = analysis_observations[stare_indexes, c_ind]
            yfr = free_run_observations[stare_indexes, c_ind]

            #  Create  observation plots (with/without uncertainty Cones and with/witout trimming x/y axes)
            if np.all(np.isnan(predictioin_stdev)):
                with_cones = [False]
            else:
                with_cones = [False, True]

            file_base_name = "cycle_%04d" % c_ind  # prefix of files saved furing this cycle
            for add_cone in with_cones:
                for scale_y in [False, True]:
                    # create figure with proper axis
                    fig = plt.figure(figsize=(7.1, 9.2), facecolor='white')  # figure with width 3 times the default
                    ax = fig.add_subplot(111)
                    ax.minorticks_on()
                    ax.grid(True, which='major', linestyle='-')
                    ax.grid(True, which='minor', linestyle='-.')

                    if np.any(~np.isnan(y)):
                        ax.plot(y, obs_heights, color='#d62728', linestyle='solid', linewidth=linewidth, label=r"$y$")
                    if np.any(~np.isnan(yfr)):
                        ax.plot(yfr, obs_heights, color='#EEC900', linestyle=(0, (3, 1, 1, 1)), linewidth=linewidth, label=r"$H(x^{\rm free})$")
                    if np.any(~np.isnan(yf)):
                        ax.plot(yf, obs_heights, color='#1f77b4', linestyle='dashed', linewidth=linewidth, label=r"$H(x^{\rm f})$")
                    if np.any(~np.isnan(ya)):
                        ax.plot(ya, obs_heights, color='#9467bd', linestyle='dashdot', linewidth=linewidth, label=r"$H(x^{\rm a})$")
                        #
                        if add_cone:
                            lower_bounds = ya[:] - 2 * predictioin_stdev[:]
                            upper_bounds = ya[:] + 2 * predictioin_stdev[:]
                            ax.fill_betweenx(obs_heights,
                                             lower_bounds, upper_bounds,
                                             alpha=0.4,
                                             edgecolor='#1B2ACC',
                                             facecolor='#089FFF',
                                             linewidth=0,
                                             antialiased=True,
                                             label=r'$H(x^{\rm a}) \mp 2 \sigma $')

                    # Add external data (if any)
                    if external_observations is not None:
                        external_sources = external_observations.keys()
                        linecolors = utility.unique_colors(len(external_sources))
                        for s_i, source in enumerate(external_sources):
                            linestyle = (0, (5, 10))  # 'densly dashed'
                            linecolor = linecolors[s_i]
                            ext_y = external_observations[source][stare_indexes, c_ind]
                            if np.any(~np.isnan(ext_y)):
                                ax.plot(ext_y, obs_heights, color=linecolor, linestyle=linestyle, linewidth=linewidth, label=r"$%s$"%source.upper())

                    # Start saving with/without axis limits
                    # Plot model bounds
                    xlims = ax.get_xlim()
                    plt.plot(xlims, [model_zlims[0]]*len(xlims), '-g', linewidth=2)
                    plt.plot(xlims, [model_zlims[-1]]*len(xlims), '-g', linewidth=2)

                    # Adjust x/y limits and ticks/tick_labels
                    t = utility.timestamp_from_scalar(experiment_tspan[c_ind], return_string=True)
                    ax.set_xlabel("%s  %s" % (obs_progvar, t))
                    ax.set_ylim(ylims[0], ylims[-1])
                    ax.set_xlim(xlims[0], xlims[-1])
                    ax.set_ylabel("Height (M)")

                    if scale_y:
                        ax.set_ylim(model_zlims[0], model_zlims[-1])

                    # Add legend
                    ax.legend(loc='upper right', framealpha=0.6)

                    # Proper Naming
                    filename  = "Observation"
                    if add_cone:
                        filename = "%s_WithConfidence" % filename
                    if scale_y:
                        filename = "%s_YScaled" % filename

                    # Now save before and after scaling x axis
                    # 1: Before Scaling X
                    filepath = os.path.join(plots_dir, "%s_%s.%s" %(filename, file_base_name, _FIG_FORMAT))
                    fig.savefig(filepath, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                    # 2: After scaling x if limits are given
                    if False:  # TODO: need to define x_axis_lims first
                        if x_axis_lims is not None:
                            ax.set_xlim(x_axis_lims[0], x_axis_lims[-1])
                            if x_axis_lims[0] < xlims[0] or x_axis_lims[1] > xlims[1]:
                                # replot(model bounds)
                                xlims = ax.get_xlim()
                                plt.plot(xlims, [model_zlims[0]]*len(xlims), '-g', linewidth=2)
                                plt.plot(xlims, [model_zlims[-1]]*len(xlims), '-g', linewidth=2)
                            #
                            filepath = os.path.join(plots_dir, "%s_XScaled_%s.%s" %(filename, file_base_name, _FIG_FORMAT))
                            fig.savefig(filepath, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')

                    plt.close(fig)

        # Observations over time, with prediction uncertainty cones for certain gates
        print("Creating Timeseries Plots...")
        if plot_gates is not None:
            plot_gates = np.unique(np.asarray(plot_gates))
            if plot_gates[0] <= 0:
                print("The gate index must be at least 1 for the first gate")
                raise ValueError
            #
            observation_grid = model.observation_grid(coordinate_system='spherical')
            obs_prog_var_indexes = model.get_observation_prognostic_variables_indexes()
            max_gate_index = np.max([inds[-1] for inds in obs_prog_var_indexes])
            if plot_gates[-1] > max_gate_index+1:
                print("The gate index must be at most %d for the last gate" % num_range_gates)
                raise ValueError
            #
            plot_gates -= 1  # put gate numer into Python index 0+

            # extract only stare-observations; we can extend to VAD similrly
            unique_grid = observation_grid[obs_prog_var_indexes[0], :]
            target_locs = np.intersect1d(np.where(unique_grid[:, 0]==90) , np.where(unique_grid[:, 1]==90))
            # target_locs contains the indexes to be accessed in obs_prog_var_indexes[*] representing stare data
            # We plot obs_prog_var_indexes[plot_gates] for each plot_gate
            # create plot with subplots equal to observation number of indexes
            num_prog_vars = len(obs_prog_var_indexes)

            for gate in plot_gates:
                # create figure with subplots equal to num of prognostic variables
                observations = file_contents["Observations/observations"]
                forecast_observations = file_contents["Observations/forecast_observations"]
                analysis_observations = file_contents["Observations/analysis_observations"]
                reference_observations = file_contents["Observations/reference_observations"]
                free_run_observations = file_contents["Observations/free_run_observations"]
                try:
                    external_observations = file_contents["Observations/External"]
                except(KeyError, ValueError):
                    external_observations = None
                #
                fig, ax_arr = plt.subplots(1, num_prog_vars, figsize=plt.figaspect(0.33), sharex=True)
                if isinstance(ax_arr, plt.Axes):
                    ax_arr = [ax_arr]
                for ax_ind, ax in enumerate(ax_arr):
                    target_index = obs_prog_var_indexes[ax_ind][gate]
                    #
                    y = observations[target_index, :]
                    if np.any(~np.isnan(y)):
                        ax.plot(experiment_tspan, y, color='#d62728', linestyle='solid', linewidth=linewidth, label=r"$y$")
                    yfr = free_run_observations[target_index, :]
                    if np.any(~np.isnan(yfr)):
                        ax.plot(experiment_tspan, yfr, color='#EEC900', linestyle=(0, (3, 1, 1, 1)), linewidth=linewidth, label=r"$H(x^{\rm free})$")
                    yf = forecast_observations[target_index, :]
                    if np.any(~np.isnan(yf)):
                        ax.plot(experiment_tspan, yf, color='#1f77b4', linestyle='dashed', linewidth=linewidth, label=r"$H(x^{\rm f})$")
                    ya = analysis_observations[target_index, :]  # with uncertainty
                    if np.any(~np.isnan(ya)):
                        ax.plot(experiment_tspan, ya, color='#9467bd', linestyle='dashdot', linewidth=linewidth, label=r"$H(x^{\rm a})$")
                        #
                        if add_cone:
                            predictioin_stdev = file_contents["Statistics/analysis_observations_stdev"][target_index, :]
                            lower_bounds = ya[:] - 2 * predictioin_stdev[:]
                            upper_bounds = ya[:] + 2 * predictioin_stdev[:]
                            ax.fill_between(experiment_tspan,
                                             lower_bounds, upper_bounds,
                                             alpha=0.4,
                                             edgecolor='#1B2ACC',
                                             facecolor='#089FFF',
                                             linewidth=0,
                                             antialiased=True,
                                             label=r'$H(x^{\rm a}) \mp 2 \sigma $')

                    # Add external data (if any)
                    if external_observations is not None:
                        external_sources = external_observations.keys()
                        linecolors = utility.unique_colors(len(external_sources))
                        for s_i, source in enumerate(external_sources):
                            linestyle = (0, (5, 10))  # 'densly dashed'
                            linecolor = linecolors[s_i]
                            ext_y = external_observations[source][target_index, :]
                            if np.any(~np.isnan(ext_y)):
                                ax.plot(experiment_tspan, ext_y, color=linecolor, linestyle=linestyle, linewidth=linewidth, label=r"$%s$"%source.upper())

                    # Adjust x/y limits and ticks/tick_labels
                    xticks = ax.get_xticks()
                    xticklabels = [utility.timestamp_from_scalar(t, return_string=True) for t in xticks]
                    ax.set_xticklabels(xticklabels, rotation=60)
                    ax.set_xlabel("Time")

                    # Add legend
                    fig.legend(loc='upper center', ncol=3, framealpha=0.65, fancybox=True)

                    # Proper Naming
                    filename  = "Observation_OverTime_Gate_%d" % (target_index+1)
                    if add_cone:
                        filename = "%s_WithConfidence" % filename

                # Now save before and after scaling x axis
                filepath = os.path.join(plots_dir, "%s.%s" %(filename, _FIG_FORMAT))
                fig.savefig(filepath, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                plt.close(fig)

        # Create Rank Histogram Plots
        for target in ['Forecast', 'Analysis']:
            for vector in ['State', 'Observation']:
                if vector == 'State':
                    vector_prefix = ''
                else:
                    vector_prefix = 'observation_'

                # extract relative frequency info (state/observation)
                dataset_name = "%s%s_ranks_relative_frequency" % (vector_prefix, target.lower())
                try:
                    target_ranks_relative_frequency = file_contents["Statistics/Rank_Histograms"][dataset_name][...]
                except(KeyError):
                    # print("Tried to access: file_contents['Statistics/Rank_Histograms'].[%s] " % dataset_name)
                    # print("file_contents['Statistics/Rank_Histograms'].keys(): ", file_contents["Statistics/Rank_Histograms"].keys())
                    # print("Failed to load rank histogram data for: ", target, vector)
                    # this means the entry was not created; e.g. there was no reference/observatiioin state; Just skip it
                    continue

                # Start Plotting
                # Start with Aggregated Rank Histogram
                ranks_relative_frequency = [target_ranks_relative_frequency[:, i].copy() for i in range(num_cycles)]
                fig = quick_rank_histogram(relative_frequency_repo=ranks_relative_frequency)
                if fig is not None:
                    filename = os.path.join(plots_dir, "Aggregated_%s_%s_RankHistogram.%s" % (target, vector, _FIG_FORMAT))
                    fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                    print("Figure saved to: %s " % filename)
                    plt.close(fig)

                # Rank Histogram Per cycle
                if rank_histograms_per_cycle:
                    for c_ind in range(num_cycles):
                        ranks_relative_frequency = target_ranks_relative_frequency[:, c_ind]
                        fig = quick_rank_histogram(relative_frequency_repo=ranks_relative_frequency)
                        if fig is not None:
                            filename = os.path.join(plots_dir, "cycle_%04d_%s_%s_Rhist.%s" % (c_ind, target, vector, _FIG_FORMAT))
                            fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                            plt.close(fig)

        # 4) Read/Plot Observations
        # --- standard observations (forecast, analysis, free_run, and possibly reference!)
        for target in ['Observations', 'Forecast_Observations', 'Analysis_Observations', 'Free_Run_Observations']:
            dataset = target.lower()
            observations = file_contents["Observations"][dataset][...]

            # Get limits of all observations to force imshow limits
            v_limits = None
            if dataset=='observations':
                if unify_scales:
                    obs_min, obs_max = np.inf, -np.inf
                    for o in observations:
                        if o is not None:
                            try:
                                obs_min = min(obs_min, np.nanmin(o))
                            except(ValueError):
                                pass
                            try:
                                obs_max = max(obs_max, np.nanmax(o))
                            except(ValueError):
                                pass
                    if not np.isinf([obs_min, obs_max]).any():
                        margin = (obs_max - obs_min) * 0.05
                        v_limits = [obs_min-margin, obs_max+margin]
            #
            filename = os.path.join(plots_dir, "%s.%s" % (target, _FIG_FORMAT))
            fig = create_observations_plot(observations.T, experiment_tspan, observation_grid, filename, v_limits=v_limits)
            plt.close(fig)

        # --- external observations (if any)
        for external_dataset in file_contents["Observations/External"].keys():
            observations = file_contents["Observations/External"][external_dataset][...]
            filename = os.path.join(plots_dir, "%s.%s" % (external_dataset, _FIG_FORMAT))
            fig = create_observations_plot(observations.T, experiment_tspan, observation_grid, filename, v_limits=v_limits)
            plt.close(fig)

        # 5) Read/Plots States
        if state_plot_per_cycle:
            print("Ploting model states slices")
            for cycle_ind in range(num_cycles):
                print("Plotting States at Cycle: %d / %d " % (cycle_ind+1, num_cycles))
                file_base_name = "cycle_%04d" % cycle_ind  # prefix of files saved furing this cycle

                # Get cycle results
                forecast_state   = file_contents["States/forecast_states"][:, cycle_ind]
                analysis_state   = file_contents["States/analysis_states"][:, cycle_ind]
                reference_state  = file_contents["States/reference_states"][:, cycle_ind]
                free_run_state   = file_contents["States/free_run_states"][:, cycle_ind]
                create_states_plots(model=model,
                                    reference_state=reference_state,
                                    forecast_state=forecast_state,
                                    forecast_ensemble=None,
                                    analysis_state=analysis_state,
                                    analysis_ensemble=None,
                                    free_run_state=free_run_state,
                                    slices_index=None,
                                    plots_dir=plots_dir,
                                    file_base_name=file_base_name,
                                    force_limits=not unify_scales)

def start_reading_pickle_and_plotting(output_dir_structure_file,
                               skip_plotting_spinup=False,
                               generate_free_run=False,
                               rank_histograms_per_cycle=True,
                               state_plot_per_cycle=True,
                               observation_plot_per_cycle=True,
                               quiver_mask_size=None,
                               slices_index=None,
                               threeD=True,
                               overwrite=True,
                               load_from_saved=False,
                               unify_scales=False,
                               fontsize=10,
                               linewidth=2,
                               plots_dir_name='PLOTS',
                               observation_profile_x_axis_lims=None
                              ):
    """
    Given the results file that contains results dictionary generated by 'start_reading_and_plotting', load and plot results
    """
    results_dict = start_reading_results(output_dir_structure_file, generate_free_run=generate_free_run, load_from_saved=load_from_saved)  # attempt to load;  if failed, will reread
    model = results_dict['model']

    experiment_tspan=results_dict['experiment_tspan']
    forecast_times=results_dict['forecast_times']
    analysis_times=results_dict['analysis_times']
    reference_times=results_dict['reference_times']
    free_run_times=results_dict['free_run_times']
    forecast_rmses=results_dict['forecast_rmses']
    analysis_rmses=results_dict['analysis_rmses']
    observation_rmses=results_dict['observation_rmses']
    free_run_rmses=results_dict['free_run_rmses']
    forecast_states=results_dict['forecast_states']
    analysis_states=results_dict['analysis_states']
    reference_states=results_dict['reference_states']
    free_run_states=results_dict['free_run_states']
    observations=results_dict['observations']
    forecast_observations=results_dict['forecast_observations']
    analysis_observations=results_dict['analysis_observations']
    free_run_observations=results_dict['free_run_observations']
    forecast_observed_errors=results_dict['forecast_observed_errors']
    analysis_observed_errors=results_dict['analysis_observed_errors']
    forecast_ranks_freq_repo=results_dict['forecast_ranks_freq_repo']
    analysis_ranks_freq_repo=results_dict['analysis_ranks_freq_repo']
    obs_forecast_ranks_freq_repo=results_dict['obs_forecast_ranks_freq_repo']
    obs_analysis_ranks_freq_repo=results_dict['obs_analysis_ranks_freq_repo']
    forecast_ranks_rel_freq_repo=results_dict['forecast_ranks_rel_freq_repo']
    analysis_ranks_rel_freq_repo=results_dict['analysis_ranks_rel_freq_repo']
    obs_forecast_ranks_rel_freq_repo=results_dict['obs_forecast_ranks_rel_freq_repo']
    obs_analysis_ranks_rel_freq_repo=results_dict['obs_analysis_ranks_rel_freq_repo']
    forecast_bins_bounds_repo=results_dict['forecast_bins_bounds_repo']
    analysis_bins_bounds_repo=results_dict['analysis_bins_bounds_repo']
    obs_analysis_bins_bounds_repo=results_dict['obs_analysis_bins_bounds_repo']

    try:
        # this is supposed to be a dictionary of external observations for validation with sources as keys; e.g. wrf
        external_observations = results_dict['external_observations']
        assert isinstance(external_observations, dict), "external_observations is expected to be a dictionary not %s" % type(external_observations)
    except(KeyError):
        external_observations = None

    try:
        forecast_covariance_trace_repo=results_dict['forecast_covariance_trace_repo']
        analysis_covariance_trace_repo=results_dict['analysis_covariance_trace_repo']
    except(KeyError):
        forecast_covariance_trace_repo = analysis_covariance_trace_repo = None

    try:
        obs_forecast_stdev_repo=results_dict['obs_forecast_stdev_repo']
        obs_analysis_stdev_repo=results_dict['obs_analysis_stdev_repo']
    except(KeyError):
        obs_forecast_stdev_repo =  obs_analysis_stdev_repo = None


    print("Results Loaded. Started Plotting...")

    #
    # Start Plotting
    output_dir_strucutre = utility.get_output_dir_structure(output_dir_structure_file, full_paths=True)
    file_output_dir  = output_dir_strucutre['file_output_dir']
    plots_dir = os.path.join(file_output_dir, plots_dir_name)
    plots_dir = create_output_dir(plots_dir, remove_existing=overwrite)

    plots_enhancer(fontsize=fontsize)

    # Create Extended error plots; This will be made standard soon
    if skip_plotting_spinup:
        plot_initial_time = analysis_times[analysis_times.size//3]
    else:
        plot_initial_time = None


    base_filename = "Errors_"
    for logscale in [False, True]:
        for plot_initial_time in [None, analysis_times[analysis_times.size//3]]:
            filename = "%s" % base_filename
            filename += "InitTime_%s" % str(plot_initial_time)
            if logscale:
                filename += "_LogScale"

            fig_filename = filename + ".%s" % (_FIG_FORMAT)
            fig1, fig2, time_spans, reference_errors, \
                observation_errors = create_error_plots(model=model,
                                                        forecast_times=forecast_times,
                                                        forecast_states=forecast_states,
                                                        analysis_times=analysis_times,
                                                        analysis_states=analysis_states,
                                                        observations=observations,
                                                        free_run_times=None,
                                                        free_run_states=None,
                                                        plots_dir=plots_dir,
                                                        plot_initial_time=plot_initial_time,
                                                        filename=fig_filename,
                                                        linewidth=linewidth,
                                                        logscale=logscale
                                                        )
            plt.close(fig1)
            plt.close(fig2)

            fig_filename = filename + "_withFree.%s" % (_FIG_FORMAT)
            fig1, fig2, time_spans, reference_errors, \
                observation_errors = create_error_plots(model=model,
                                                        forecast_times=forecast_times,
                                                        forecast_states=forecast_states,
                                                        analysis_times=analysis_times,
                                                        analysis_states=analysis_states,
                                                        observations=observations,
                                                        free_run_times=free_run_times,
                                                        free_run_states=free_run_states,
                                                        plots_dir=plots_dir,
                                                        plot_initial_time=plot_initial_time,
                                                        filename=fig_filename,
                                                        linewidth=linewidth,
                                                        logscale=logscale
                                                        )
            plt.close(fig1)
            plt.close(fig2)
    results_dict.update(dict(time_spans=time_spans, reference_errors=reference_errors, observation_errors=observation_errors))

    # Create plots for covariance traces
    filename = os.path.join(plots_dir, "Variance_Trace.%s" % _FIG_FORMAT)
    fig = create_variance_trace_plot(forecast_times, forecast_covariance_trace_repo, analysis_covariance_trace_repo, filename=filename, plots_dir=plots_dir)
    plt.close(fig)


    if unify_scales:
        # Get limits of all observations to force imshow limits
        pass
        obs_min = np.inf
        obs_max = -np.inf
        for o in observations:
            if o is not None:
                try:
                    obs_min = min(obs_min, np.nanmin(o))
                except(ValueError):
                    pass
                try:
                    obs_max = max(obs_max, np.nanmax(o))
                except(ValueError):
                    pass
        if np.isinf([obs_min, obs_max]).any():
            v_limits = None
            print("WARNING: Failed to calcualate X-Axis limits from data...")
        else:
            margin = (obs_max - obs_min) * 0.05
            v_limits = [obs_min-margin, obs_max+margin]
    else:
        v_limits = None

    # Create other plots; states, observations, etc.
    filename = os.path.join(plots_dir, "Observations.%s" % _FIG_FORMAT)
    observation_grid = model.observation_grid(coordinate_system='spherical')
    fig = create_observations_plot(observations, analysis_times, observation_grid, filename, v_limits=v_limits)
    plt.close(fig)

    # Add observation profiles from external sources
    if external_observations is not None:
        observation_grid = model.observation_grid(coordinate_system='spherical')
        for ext_source in external_observations:
            source_obs = external_observations[ext_source]
            filename = os.path.join(plots_dir, "%s_Observations.%s" % (ext_source, _FIG_FORMAT))
            fig = create_observations_plot(source_obs, analysis_times, observation_grid, filename, v_limits=v_limits)
            plt.close(fig)

    # Create other plots; states, observations, etc.
    filename = os.path.join(plots_dir, "Forecast_Observations.%s" % _FIG_FORMAT)
    observation_grid = model.observation_grid(coordinate_system='spherical')
    fig = create_observations_plot(forecast_observations, analysis_times, observation_grid, filename, v_limits=v_limits)
    plt.close(fig)

    # Create other plots; states, observations, etc.
    filename = os.path.join(plots_dir, "Analysis_Observations.%s" % _FIG_FORMAT)
    observation_grid = model.observation_grid(coordinate_system='spherical')
    fig = create_observations_plot(analysis_observations, analysis_times, observation_grid, filename, v_limits=v_limits)
    plt.close(fig)

    # Create other plots; states, observations, etc.
    filename = os.path.join(plots_dir, "FreeRun_Observations.%s" % _FIG_FORMAT)
    observation_grid = model.observation_grid(coordinate_system='spherical')
    fig = create_observations_plot(free_run_observations, analysis_times, observation_grid, filename, v_limits=v_limits)
    plt.close(fig)

    # Plot Collective Rank histogram plots (state/observations)
    fig = quick_rank_histogram(relative_frequency_repo=forecast_ranks_rel_freq_repo, hist_title="Forecast", init_ind=2)  # Skip two cycles (spin-up)
    if fig is not None:
        filename = os.path.join(plots_dir, "Aggregated_Forecast_RankHistogram.%s"%_FIG_FORMAT)
        fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
        plt.close(fig)
    #
    fig = quick_rank_histogram(relative_frequency_repo=analysis_ranks_rel_freq_repo, hist_title="Analysis", init_ind=2)  # Skip two cycles (spin-up)
    if fig is not None:
        filename = os.path.join(plots_dir, "Aggregated_Analysis_RankHistogram.%s"%_FIG_FORMAT)
        fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
        plt.close(fig)

    fig = quick_rank_histogram(relative_frequency_repo=obs_forecast_ranks_rel_freq_repo, hist_title="Forecasted Observations", init_ind=2)
    if fig is not None:
        filename = os.path.join(plots_dir, "Aggregated_Forecast_Obs_RankHistogram.%s"%_FIG_FORMAT)
        fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
        plt.close(fig)
    #
    fig = quick_rank_histogram(relative_frequency_repo=obs_analysis_ranks_rel_freq_repo, hist_title="Analysis Observations", init_ind=2)
    if fig is not None:
        filename = os.path.join(plots_dir, "Aggregated_Analysi_Obs_RankHistogram.%s"%_FIG_FORMAT)
        fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
        plt.close(fig)


    # print(forecast_ranks_rel_freq_repo, analysis_ranks_rel_freq_repo, obs_forecast_ranks_rel_freq_repo, obs_analysis_ranks_rel_freq_repo)
    # Loop over cycles
    if rank_histograms_per_cycle or state_plot_per_cycle or observation_plot_per_cycle:

        if  observation_plot_per_cycle:
            # This  works for one prognostic variable only, optimally with stare observations; Correct for more TODO
            # Add a flag for plottiing observations per cycle
            observation_grid = model.observation_grid()
            observation_size = np.size(observation_grid, 0)
            obs_num_cycles = len(observations)
            obs_progvar  = model.get_observation_configs()['prog_var']
            obs_dx =  model.get_observation_configs()['range_gate_length']
            model_grid = model.get_model_grid()
            obs_heights = observation_grid[:, -1]
            ylims = [0, obs_heights.max()+obs_dx]
            model_zlims = [model_grid[:,  -1].min(), model_grid[:,  -1].max()]

            # Find Minimum/Maximum of real/forecast/analysis observations
            if observation_profile_x_axis_lims is not None:
                assert len(observation_profile_x_axis_lims) == 2, "observation_profile_x_axis_lims must be iterable of size 2"
                assert observation_profile_x_axis_lims[1] > observation_profile_x_axis_lims[0], "observation_profile_x_axis_lims entries must ascending in value!"
                x_axis_lims = observation_profile_x_axis_lims
            else:
                obs_min = np.inf
                obs_max = -np.inf
                initind = len(forecast_observations)//10  # To avoid initial values
                for obs in [observations[initind:], forecast_observations[initind:], analysis_observations[initind:]]:
                    for o in obs:
                        if o is not None:
                            try:
                                obs_min = min(obs_min, np.nanmin(o))
                            except(ValueError):
                                pass
                            try:
                                obs_max = max(obs_max, np.nanmax(o))
                            except(ValueError):
                                pass

                if np.isinf([obs_min, obs_max]).any():
                    x_axis_lims = None
                    print("WARNING: Failed to calcualate X-Axis limits from data...")
                else:
                    margin = (obs_max - obs_min) * 0.05
                    x_axis_lims = [obs_min-margin, obs_max+margin]

            #
            for cycle_ind in range(obs_num_cycles):
                print("Plotting Observations for Cycles: %d / %d " % (cycle_ind+1, obs_num_cycles))
                file_base_name = "cycle_%04d" % cycle_ind  # prefix of files saved furing this cycle
                y = observations[cycle_ind]

                if len(forecast_observations) == obs_num_cycles:
                    cf_ind = cycle_ind
                    yf = forecast_observations[cf_ind]
                elif len(forecast_observations) == obs_num_cycles + 1:
                    cf_ind = cycle_ind + 1
                    yf = forecast_observations[cf_ind]
                else:
                    print("Length mismatch in observatioin results...")
                    yf = None

                predictioin_stdev = None
                if len(analysis_observations) == obs_num_cycles:
                    ca_ind = cycle_ind
                    ya = analysis_observations[ca_ind]
                    if obs_analysis_stdev_repo is not None:
                        predictioin_stdev = obs_analysis_stdev_repo[cycle_ind]
                elif len(analysis_observations) == obs_num_cycles + 1:
                    ca_ind = cycle_ind + 1
                    ya = analysis_observations[ca_ind]
                    if obs_analysis_stdev_repo is not None:
                        predictioin_stdev = obs_analysis_stdev_repo[cycle_ind]
                else:
                    print("Length mismatch in observatioin results...")
                    ya = None

                if len(free_run_observations) == obs_num_cycles:
                    cfr_ind = cycle_ind
                    yfr = free_run_observations[ca_ind]
                elif len(free_run_observations) == obs_num_cycles + 1:
                    cfr_ind = cycle_ind + 1
                    yfr = free_run_observations[ca_ind]
                else:
                    print("Length mismatch in observatioin results...")
                    print("len(free_run_observations):", len(free_run_observations))
                    print("obs_num_cycles: ", obs_num_cycles)
                    yfr = None


                #  Create  observation plots (with/without uncertainty Cones and with/witout trimming x/y axes)
                if predictioin_stdev is None:
                    with_cones = [False]
                else:
                    with_cones = [False, True]
                    #
                for add_cone in with_cones:
                    for scale_y in [False, True]:
                        # create figure with proper axis
                        fig = plt.figure(figsize=(7.1, 9.2), facecolor='white')  # figure with width 3 times the default
                        ax = fig.add_subplot(111)
                        ax.minorticks_on()
                        ax.grid(True, which='major', linestyle='-')
                        ax.grid(True, which='minor', linestyle='-.')

                        if y is not None:
                            ax.plot(y, obs_heights, color='#d62728', linestyle='solid', linewidth=linewidth, label=r"$y$")
                        if yfr is not None:
                            ax.plot(yfr, obs_heights, color='#EEC900', linestyle=(0, (3, 1, 1, 1)), linewidth=linewidth, label=r"$H(x^{\rm free})$")
                        if yf is not None:
                            ax.plot(yf, obs_heights, color='#1f77b4', linestyle='dashed', linewidth=linewidth, label=r"$H(x^{\rm f})$")
                        if ya is not None:
                            ax.plot(ya, obs_heights, color='#9467bd', linestyle='dashdot', linewidth=linewidth, label=r"$H(x^{\rm a})$")
                            #
                            if add_cone:
                                lower_bounds = ya[:] - 2 * predictioin_stdev[:]
                                upper_bounds = ya[:] + 2 * predictioin_stdev[:]
                                ax.fill_betweenx(obs_heights,
                                                 lower_bounds, upper_bounds,
                                                 alpha=0.4,
                                                 edgecolor='#1B2ACC',
                                                 facecolor='#089FFF',
                                                 linewidth=0,
                                                 antialiased=True,
                                                 label=r'$H(x^{\rm a}) \mp 2 \sigma $')

                        # Add external data (if any)
                        if external_observations is not None:
                            external_sources = external_observations.keys()
                            linecolors = utility.unique_colors(len(external_sources))
                            for s_i, source in enumerate(external_sources):
                                linestyle = (0, (5, 10))  # 'densly dashed'
                                linecolor = linecolors[s_i]
                                ext_y = external_observations[source][cycle_ind]
                                if ext_y is not None:
                                    ax.plot(ext_y, obs_heights, color=linecolor, linestyle=linestyle, linewidth=linewidth, label=r"$%s$"%source.upper())

                        # Start saving with/without axis limits
                        # Plot model bounds
                        xlims = ax.get_xlim()
                        plt.plot(xlims, [model_zlims[0]]*len(xlims), '-g', linewidth=2)
                        plt.plot(xlims, [model_zlims[-1]]*len(xlims), '-g', linewidth=2)

                        # Adjust x/y limits and ticks/tick_labels
                        t = utility.timestamp_from_scalar(analysis_times[cycle_ind], return_string=True)
                        ax.set_xlabel("%s  %s" % (obs_progvar, t))
                        ax.set_ylim(ylims[0], ylims[-1])
                        ax.set_xlim(xlims[0], xlims[-1])
                        ax.set_ylabel("Height (M)")

                        if scale_y:
                            ax.set_ylim(model_zlims[0], model_zlims[-1])

                        # Add legend
                        ax.legend(loc='upper right', framealpha=0.6)

                        # Proper Naming
                        filename  = "Observation"
                        if add_cone:
                            filename = "%s_WithConfidence" % filename
                        if scale_y:
                            filename = "%s_YScaled" % filename

                        # Now save before and after scaling x axis
                        # 1: Before Scaling X
                        filepath = os.path.join(plots_dir, "%s_%s.%s" %(filename, file_base_name, _FIG_FORMAT))
                        fig.savefig(filepath, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                        # 2: After scaling x if limits are given
                        if x_axis_lims is not None:
                            ax.set_xlim(x_axis_lims[0], x_axis_lims[-1])
                            if x_axis_lims[0] < xlims[0] or x_axis_lims[1] > xlims[1]:
                                # replot(model bounds)
                                xlims = ax.get_xlim()
                                plt.plot(xlims, [model_zlims[0]]*len(xlims), '-g', linewidth=2)
                                plt.plot(xlims, [model_zlims[-1]]*len(xlims), '-g', linewidth=2)
                            #
                            filepath = os.path.join(plots_dir, "%s_XScaled_%s.%s" %(filename, file_base_name, _FIG_FORMAT))
                            fig.savefig(filepath, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')

                        # print(fig.get_figwidth() , fig.get_figheight() )  # 7.1 9.19 is the right size for ffmpeg!  TODO: Inforce by fig.set_size_inches(w, h)

                        plt.close(fig)

        #
        states_num_cycles = len(forecast_times)
        for cycle_ind in range(states_num_cycles):

            # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            print("Results for Assimilation Cycle: %d / %d " % (cycle_ind+1, states_num_cycles))
            file_base_name = "cycle_%04d" % cycle_ind  # prefix of files saved furing this cycle

            if state_plot_per_cycle:
                # Get cycle results
                forecast_state   = forecast_states[cycle_ind]
                analysis_state   = analysis_states[cycle_ind]
                reference_state  = reference_states[cycle_ind]
                free_run_state   = free_run_states[cycle_ind]
                observation      = observations[cycle_ind]
                print("Ploting model states slices")
                # a) create slices for forecast/analysis/reference states
                create_states_plots(model=model,
                                    reference_state=reference_state,
                                    forecast_state=forecast_state,
                                    forecast_ensemble=None,
                                    analysis_state=analysis_state,
                                    analysis_ensemble=None,
                                    free_run_state=free_run_state,
                                    slices_index=None,
                                    plots_dir=plots_dir,
                                    file_base_name=file_base_name,
                                    force_limits=not unify_scales)
                #

            if rank_histograms_per_cycle:
                forecast_ranks_rel_freq     = forecast_ranks_rel_freq_repo[cycle_ind]
                analysis_ranks_rel_freq     = analysis_ranks_rel_freq_repo[cycle_ind]
                obs_forecast_ranks_rel_freq = obs_forecast_ranks_rel_freq_repo[cycle_ind]
                obs_analysis_ranks_rel_freq = obs_analysis_ranks_rel_freq_repo[cycle_ind]

                fig = quick_rank_histogram(relative_frequency_repo=forecast_ranks_rel_freq, hist_title="Forecast")
                if fig is not None:
                    filename = os.path.join(plots_dir, "cycle_%04d_ForecastStates_Rhist.%s" % (cycle_ind, _FIG_FORMAT))
                    fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                    plt.close(fig)

                fig = quick_rank_histogram(relative_frequency_repo=analysis_ranks_rel_freq, hist_title="Analysis")
                if fig is not None:
                    filename = os.path.join(plots_dir, "cycle_%04d_AnalysisStates_Rhist.%s" % (cycle_ind, _FIG_FORMAT))
                    fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                    plt.close(fig)

                fig = quick_rank_histogram(relative_frequency_repo=obs_forecast_ranks_rel_freq, hist_title="Forecast Observations")
                if fig is not None:
                    filename = os.path.join(plots_dir, "cycle_%04d_ForecastObservation_Rhist.%s" % (cycle_ind, _FIG_FORMAT))
                    fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                    plt.close(fig)

                fig = quick_rank_histogram(relative_frequency_repo=obs_analysis_ranks_rel_freq, hist_title="Analysis Observations")
                if fig is not None:
                    filename = os.path.join(plots_dir, "cycle_%04d_AnalysisObservation_Rhist.%s" % (cycle_ind, _FIG_FORMAT))
                    fig.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
                    plt.close(fig)

            plt.close('all')  # TODO: Try to close individual plts instead.
            print("done")
            # Cycle done

            # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    else:
        print("Skipping cycles plots")
    #
    # Overwrite collective results
    try:
        output_dict = output_dict.copy()
        output_dict.update({'model': None})
        pickle.dump(output_dict, open(os.path.join(file_output_dir, "%s.pickle"%__COLLECTIVE_RES_FILENAME), 'wb'))
    except:
        print("Failed to write results dictionary")

    return results_dict

def str2bool(v):
    """
    Convert a string to a boolean (if the string makes sense!, otherwise ValueError is raised)
    """
    if v.lower().strip() in ('yes', 'true', 't', 'y', '1'):
        val = True
    elif v.lower().strip() in ('no', 'false', 'f', 'n', '0'):
        val = False
    else:
        print("Boolean Value is Expected Yes/No, Y/N, True/False, T/F, 1/0")
        raise ValueError
    return val

def get_args(input_args,
             output_repository_dir=None,
             out_dir_tree_structure_file=None,
             overwrite=None,
             recoellect_data=False,
             generate_free_run=True,
             slices_index=None,
             rank_hist_per_cycle=False,
             state_plot_per_cycle=False
            ):
    """
    Get command line arguments; default values passed are used, if not entered in the initiating command
    """

    try:
        opts, args = getopt.getopt(input_args,
                                   "hf:d:o:s:r:e:k:t:",
                                   ["help","structurefile=","outputdir=","overwrite=","slices-index=","recollect-data=","free-run=","rank-hist-per-cycle"]
                                   )
    except getopt.GetoptError:
        msg = 'python/run filtering_results_reader.py '
        msg += '-f <out_dir_tree_structure_file> '
        msg += '-d <output_repository_dir> '
        msg += '-o <overwrite> '
        msg += '-s <slices_index> '
        msg += '-r <recollect-data-flag> '
        msg += '-e <free-run> '
        msg += '-k <rank-hist-per-cycle> '
        msg += '-t <state-plot-per-cycle> '
        print(msg)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            msg = 'python/run filtering_results_reader.py '
            msg += '-f <out_dir_tree_structure_file> '
            msg += '-d <output_repository_dir> '
            msg += '-o <overwrite> '
            msg += '-s <slices_index> '
            msg += '-r <recollect-data-flag> '
            msg += '-e <free-run> '
            msg += '-k <rank-hist-per-cycle> '
            msg += '-t <state-plot-per-cycle> '
            print(msg)
            sys.exit()
        elif opt in ("-o", "--overwrite"):
            overwrite = str2bool(arg)
        elif opt in ("-f", "--structurefile"):
            out_dir_tree_structure_file = arg
        elif opt in ("-s", "--slices-index"):
            slices_index = map(int, arg.strip('[]').split(','))
        elif opt in ("-d", "--outputdir"):
            output_repository_dir = arg
        elif opt in ("-e", "--free-run"):
            generate_free_run = str2bool(arg)
        elif opt in ("-r", "--recollect-data"):
            recoellect_data = str2bool(arg)
        elif opt in ("-k", "--rank-hist-per-cycle"):
            rank_hist_per_cycle = str2bool(arg)
        elif opt in ("-t", "--state-plot-per-cycle"):
            state_plot_per_cycle = str2bool(arg)

        if None not in [output_repository_dir, out_dir_tree_structure_file]:
            print("You passed both output respository, and out_dir_tree_structure_file; which is confusing; only one of them should be passed")
            raise ValueError

    return output_repository_dir, out_dir_tree_structure_file, overwrite, slices_index, recoellect_data, generate_free_run, rank_hist_per_cycle, state_plot_per_cycle

#
if __name__ == '__main__':

    # ---------------------------------------------------------------------------------------- #
    # Default Plot Settings; overwrite is overriddable via command-line with option -o
    # ---------------------------------------------------------------------------------------- #
    quiver_mask_size = None  # skip every (mask_size-1) grid points in etach direction for 3D quiver; Make None if you don't want quiver plot
    overwrite = False
    unify_scales = True
    observation_profile_x_axis_lims = [-1.75, 1.75]  # if Nonem, will be adjusted automatically
    fontsize = 12
    plots_dir_name = 'PLOTS'     # Name of the directory to checkpoint and plot
    # ---------------------------------------------------------------------------------------- #

    # ---------------------------------------------------------------------------------------- #
    # Read command-line arguments and start reading & plotting
    # ---------------------------------------------------------------------------------------- #
    #
    # Check passed arguments (This will be useful if we want to call this script from another):
    output_repository_dir, out_dir_tree_structure_file, \
        overwrite, slices_index, recoellect_data, generate_free_run, \
            rank_histograms_per_cycle, \
                state_plot_per_cycle  = get_args(sys.argv[1: ],
                                                 slices_index=[25, 25, 5],
                                                 overwrite=overwrite,
                                                )
    load_from_saved = not recoellect_data
    # check the arguments
    if output_repository_dir is not None:
        output_dirs_list = utility.get_list_of_subdirectories(output_repository_dir,
                                                              ignore_root=True,
                                                              return_abs=False,
                                                              ignore_special=True,
                                                              recursive=False)
        #
    elif out_dir_tree_structure_file is not None:
        output_dirs_list = [os.path.dirname(out_dir_tree_structure_file)]
    else:
        out_dir_tree_structure_file = __def_out_dir_tree_structure_file
        output_dirs_list = [os.path.dirname(out_dir_tree_structure_file)]

    # Loop over all directories in the output repository
    for out_dir in output_dirs_list:
        out_dir_tree_structure_file = os.path.join(out_dir, 'output_dir_structure.txt')
        if not os.path.isfile(out_dir_tree_structure_file):
            print("Nothing to be done here...\nThe file %s is not valid" % out_dir_tree_structure_file)
            #
        else:
            start_reading_and_plotting(out_dir_tree_structure_file,
                                       generate_free_run=generate_free_run,
                                       rank_histograms_per_cycle=rank_histograms_per_cycle,
                                       state_plot_per_cycle=state_plot_per_cycle,
                                       quiver_mask_size=quiver_mask_size,
                                       slices_index=slices_index,
                                       overwrite=overwrite,
                                       plots_dir_name=plots_dir_name,
                                       load_from_saved=load_from_saved,
                                       unify_scales=unify_scales,
                                       observation_profile_x_axis_lims=observation_profile_x_axis_lims,
                                       fontsize=fontsize
                                      )
            #
    # ---------------------------------------------------------------------------------------- #
