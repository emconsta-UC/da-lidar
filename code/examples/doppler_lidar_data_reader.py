#! /usr/bin/env python

""" Gather Doppler Lidar data/observations over a given timespan with/without any sort of preprocessing
"""

import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange
import os

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()
#


# Import DL_DATA_HANDLER and generate an object (instances)
from DL_data_handler import DL_DATA_HANDLER

if __name__ == "__main__":

    # **************************************
    # Settings
    # **************************************
    # Observation time setting:
    start_time = '2017:05:28:00:00:00'
    end_time   = '2017:05:28:04:00:00'

    # Sync with server to get all observations vs. only local availability on this machine
    collect_remote_data = False  # Set to True once to get data from MCS server side

    site_facility='sgpdlE32'
    scan_type = 'all'  # e.g., 'stare', 'vad', 'all'
    # **************************************

    #
    # **************************************
    # Collect/Read data
    # **************************************
    # create lidar data object:
    dl_obs_obj = DL_DATA_HANDLER()
    times, observations = dl_obs_obj.gather_DL_data_file_contents(start_time=start_time,
                                                                  end_time=end_time,
                                                                  site_facility=site_facility,
                                                                  scan_type=scan_type,
                                                                  collect_remote_data=collect_remote_data
                                                                  )

    print("Number of valid data files: %d " % len(times))
    # 
    # **************************************

