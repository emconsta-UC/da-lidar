
import matplotlib.pyplot as plt
from matplotlib import rc


def plots_enhancer(font_size=12, usetex=True):
    # set fonts, and colors:
    font = {'family' : 'serif',
            'weight' : 'bold',
            'size'   : font_size}
    #
    rc('font', **font)
    rc('text', usetex=usetex)

