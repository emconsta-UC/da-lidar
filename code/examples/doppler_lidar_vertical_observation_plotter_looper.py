#! /usr/bin/env python

"""
Loop over the functionality of doppler_lidar_vertical_observation_plotter,
to create vertical profiles over multiple timespnas
"""

from doppler_lidar_vertical_observation_plotter import *


#
if __name__ == '__main__':

    # ====================================================================================== #
    #                              Settings & DATA Collection                                #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # --------------------------------------(SETTINGS)-------------------------------------- #
    #
    base_out_dir_name  = '_DL_OBSERVATIONS_HOURLY_RESULTS'  # folder/path to save plots and collected observation to
    data_output_format = 'matlab'  # both matlab/mat and pickle are currently supported. 

    # Sync data with remote date repo? this gets all matching files from mcs.anl.gov repo if not available on this machine
    collect_remote_data = False  # try running doppler_lidar_sync_parsed_files.py once if False

    # Specify setings/filters for obsrvations to collect
    site_facility = 'sgpdlE32'                       # sgpdlE32, sgpdlC1, etc.
    field         = 'radial_velocity'                # 'radial_velocity', 'doppler', 'wind-speed', etc
    scan_type     = 'stare'                         # 'stare'/'vad'; if vad, must provide vad_scan_degree(default 60); also the plotting down should be updated for multiple vad degrees

    aggregate_data = True  # if False, average observations over each subinterval (delta_time is ignored)

    overwrite_existing_plots = True
    # --------------------------(Data collection and Plotting)------------------------------ #


    # Observation time setting: (Create proper timespan)
    # start_time = '2016:06:04:13:00:00'
    # end_time   = '2016:06:04:14:00:00'
    delta_time = '0000:00:00:00:00:10'  # aggregate every 10 seconds

    for day in range(1, 8):  # first week of the month
        for hour in range(11, 18):  # from 11am to 5 pm

            start_time = '2016:06:%02d:%02d:00:00'  % (day, hour)
            end_time = '2016:06:%02d:%02d:59:00'  % (day, hour)  # One hour timespan

            out_dir_name = "%s_%s" % (base_out_dir_name, str.replace(start_time, ':', '_'))

            collected_results = read_and_plot(start_time=start_time,
                                              end_time=end_time,
                                              delta_time=delta_time,
                                              site_facility=site_facility,
                                              field=field,
                                              scan_type=scan_type,
                                              collect_data_only=False,
                                              all_gates=True,
                                              filter_observations=True,
                                              aggregate_data=aggregate_data,
                                              plot_vertical_profile=True,
                                              plot_boxplots_all_gates=False,
                                              plot_boxplots_all_times=False,
                                              plot_statistics=False,
                                              plot_gates_series=False,
                                              collect_remote_data=collect_remote_data,
                                              overwrite=overwrite_existing_plots,
                                              out_dir_name=out_dir_name,
                                              data_output_format=data_output_format,
                                             )  # Collect only remotely; will plot later on my machine

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                              Results Plotting complete                                 #
    # ====================================================================================== #
