
# This is a very Simple scrip to run NavierStokes model implemented in Hypar, and get lots of plots







# ======================================================================================== #
#                                       Script Starts                                      #
# ======================================================================================== #

import os
import sys 
import shutil

import numpy as np
from mpl_toolkits.mplot3d import axes3d, Axes3D
import matplotlib.pyplot as plt
try:
    from pyevtk.hl import gridToVTK
    use_evtk = True
except ImportError:
    use_evtk = False
    print("Consider installing Python-based evtk package to enable output for ParaView")
    print(" e.g. --> pip install vtk pyevtk")

from hypar_model_plotter import quiver3D_plotter, \
        create_output_dir, plot_contourf_slices, plot_temperature, plot_wind_slices

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()
from Py_HyPar import HyPar_Model

def create_model(model_configs, ph_dict, b_dict):
    """
    Create Py-HyPar model instance

    Args:
        model_configs
        physics_configs
        boundary_configs

    Returns:
        model: HyPar_Model instance

    """
    # Aggregate configurations
    model_configs.update(b_dict)
    model_configs.update({'physics':ph_dict})
    #
    # Create model instances with configurations set above
    model = HyPar_Model(model_configs=model_configs)
    return model
    #


if __name__ == '__main__':

    # ======================================================================================== #
    #                                         Settings                                         # 
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #  This is where you can play with experiment and plotting settins                         #
    #  You only need to play with settings here.                                               # 
    # ======================================================================================== #

    # ---------------------------------------------------------------------------------------- #
    # 1- Hypar Model Settings:
    # ---------------------------------------------------------------------------------------- #

    # i   ) domain size in the x, y, z direction (i.e. Nx, Ny, Nz)
    _size = [101, 101, 41]

    # ii  ) grid limits (lower and upper limits the grid in each direction)
    _domain_upper_bounds = [50000, 50000, 4000]
    _domain_lower_bounds = [0, 0, 0]

    # iii ) Number of process in each direction
    _iproc = [4, 4, 1]
    # iv  ) number of ghost points:
    _ghost = 3
    # v   ) (Maximum) time step of the time-integration scheme
    _dt = 0.10
    # vi  ) timestepping method and PETSc Runtime options
    _ts            = 'rk'
    _ts_type       = 'ssprk3'
    _hyp_scheme    = 'weno5'  # TODO: I haven't involved this yet; will be ignored for now
    _PETSc_options = '-ts_type rk -ts_rk_type 4'
    # vii ) Other hypar-specific settings
    _hyp_flux_split = 'no'
    _hyp_int_type   = 'components'
    _par_type       = 'none'
    _par_scheme     = 'none'
    # viii) Physics Settings:
    _gamma     = 1.4
    _upwinding = "rusanov"
    _gravity   = [0.0, 0.0, 9.8]
    _rho_ref   = 1.1612055171196529
    _p_ref     = 100000.0
    _R         = 287.058
    _HB        = 2

    # ix  ) Boundary Settings
    # 6 sides of the boundary x-lower, x-upper, y-lower, y-upper, z-lower, z-upper; see example below
    _faces_types = ['slip-wall',             # x-lower
                    'slip-wall',             # x-upper
                    'slip-wall',             # y-lower
                    'slip-wall',             # y-upper
                    'thermal-slip-wall',     # z-lower
                    'slip-wall'              # z-upper
                   ]
    # boundary settings for each of the 6 sides. Put None if neither slip nor thermal in the side type
    _faces_settings=[(0.0, 0.0, 0.0),
                     (0.0, 0.0, 0.0),
                     (0.0, 0.0, 0.0),
                     (0.0, 0.0, 0.0),
                     (0.0, 0.0, 0.0, 'temperature_data.dat'),
                     (0.0, 0.0, 0.0)
                    ]
    # Example of boundary settings:
    # faces_types=['periodic', 'periodic', 'periodic', 'periodic', 'thermal-slip-wall', 'slip-wall'],
    # faces_settings=[None, None, None, None, (0.0, 0.0, 0.0, 'temperature_data.dat'), (0.0, 0.0, 0.0)]

    # x   ) Thermal source settings; used for any wall having 'thermal' in its face_type value
    _thermal_shape = 'disc'
    _thermal_source_center = [25000, 25000, 2000]  # x-y-z coordinates of the the thermal source
    _rcent = 3000  # radius of the (for disc shape) of the thermal spot
    _T_diff = 10  # time differnece used to create thermal spot

    # xi  ) 


    # ---------------------------------------------------------------------------------------- #
    # 2- Experiment Settings:
    # ---------------------------------------------------------------------------------------- #
    # time points (seconds) at which solution is checkpointed/saved and plotted
    _start_time = 0.0     # initial time
    _final_time = 60.0    # final time
    _delta_time = 0.5     # time between consecutive checkpoints


    # ---------------------------------------------------------------------------------------- #
    # 3- Pltting Settings:
    # ---------------------------------------------------------------------------------------- #
    slices_index = [50, 50, 1]  # integr (0<=index<N), or list of integers, giving  index(s) of the slicing grid poins in each direction 

    quiver_mask_size = None # skip every (mask_size-1) grid points in etach direction for 3D quiver; Make None if you don't want quiver plot 
    overwrite_existing_plots = True
    plots_directory = os.path.join(os.path.abspath(os.path.dirname(__file__)), '__RESULTS_and_PLOTS')     # Name of the directory to checkpoint and plot

    save_model_states = False # write model states to files

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                                   END OF Settings                                        # 
    # ======================================================================================== #

    # Prepre the model and plot things
    # model configureations
    model_configs = {'size':                _size,
                     'domain_upper_bounds': _domain_upper_bounds,
                     'domain_lower_bounds': _domain_lower_bounds,
                     'iproc':               _iproc,
                     'dt':                  _dt,
                     'PETSc_options':       _PETSc_options,
                     'init_time':           0.0,  # (optional) time of the initial state (default is zero)
                     'ghost':               _ghost,
                     'rest_iter':           0,
                     'ts':                  _ts,
                     'ts_type':             _ts_type,
                     'hyp_scheme':          _hyp_scheme,
                     'hyp_flux_split':      _hyp_flux_split,
                     'hyp_int_type':        _hyp_int_type,
                     'par_type':            _par_type,
                     'par_scheme':          _par_scheme,
                     'cleanup_dir':False}
    # Physics
    ph_dict = dict(gamma     = _gamma,
                   upwinding = _upwinding,
                   gravity   = _gravity,
                   rho_ref   = _rho_ref,
                   p_ref     = _p_ref,
                   R         = _R,
                   HB        = _HB)
    # Boundary 
    b_dict = dict(boundary=dict(num_faces=len(_faces_types),
                                faces_types=_faces_types,
                                faces_settings=_faces_settings),
                  thermal_source=dict(shape=_thermal_shape,
                                      xcent= _thermal_source_center[0],
                                      ycent= _thermal_source_center[1],
                                      zcent= _thermal_source_center[2],
                                      rcent=_rcent,
                                      T_diff=_T_diff))
    # create the model:
    model = create_model(model_configs, ph_dict, b_dict)

    # Integration timespan:
    tspan = np.arange(_start_time, _final_time+1e-7, _delta_time, dtype=np.float)

    # Creating an outpu  directory for checkpoints and plots:
    dir_name = create_output_dir(plots_directory, remove_existing=overwrite_existing_plots)
    fname_base = 'NS_Thermal'

    # Loop over all check points, and generate plots as requested
    # Get velocity field from trajectory, and plot at each time instance:
    n_checkpoints = len(tspan)
    for i, checkpoints in enumerate(zip(tspan[0: -1], tspan[1:])):

        # Won't plot initial state
        # Inegrate the model state to the next checkpoint:
        if i < (n_checkpoints-1):
            print("Forward propagation:")
            model.integrate_state(checkpoints=checkpoints, reset_state=False)

        print("\n%s\n Plotting at time %f \n%s\n" % ("*"*60, checkpoints[-1], "*"*60))
        # Retrieve state (and checkpoint for future reference) and and get premitive atmospheric variales
        state = model.get_current_state()
        if save_model_states:
            state.write_to_file(fname)
        #
        # Get the gridded velocity field; Note: passing x, y, z, u, v, w avoides reallocating memory for them
        x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0 = model.get_premitive_atmos_flow_variable(state=state)

        # Plotting Velocity Field:
        # 1- Plot slices of the wind velocity components of the solution at the center
        fname = os.path.join(dir_name, "%s_%s_%05d"%(fname_base, 'Wind' , i))
        fig = plot_wind_slices(x, y, z, u, v, w, index=slices_index, filename="%s_slices.eps"%fname)

        if False:  # TODO: still polishing it.
            # 2- Plot Countourf projection at some offsets
            fname = os.path.join(dir_name, "%s_%s_%05d"%(fname_base, 'WindContourf' , i))
            cf_axes = plot_contourf_slices(x, y, z, w, [1, 10], filename=fname, add_color_bars=False)

        # 3- plot updated velocity field using MatPlotLib
        if quiver_mask_size is not None:
            _x, _y, _z, _u, _v, _w = model.velocity_field_gridded_to_numpy(state=state, mask_size=mask_size)
            fname = os.path.join(dir_name, "%s_%s_%05d"%(fname_base, 'wind3DQuiver' , i))
            quiver3D_plotter(_x, _y, _z, _u, _v, _w, "%s.eps"%fname)

        # Plot Theta; slices of theta, theta0, theta-theta0
        fname = os.path.join(dir_name, "%s_%s_%05d"%(fname_base, 'Theta' , i))
        fig, z_grid, theta_tuple = plot_temperature(x, y, z, theta, theta0, index=slices_index, filename="%s_slices.eps"%fname)
        # We can do similar stuff for P, P0, etc.

        # Close all open figures
        plt.close('all')

        #

# ======================================================================================== #
#                                       Script Ends                                        #
# ======================================================================================== #
