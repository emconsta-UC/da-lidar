#! /usr/bin/env python3

"""
    This is a temporary script to update states written on file with proper time
"""

import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import numpy as np

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

import DLidarVec
# from DLidarVec import StateVector, state_vector_from_file
import DL_utility as utility

def time_from_file(config_file, verbose=False):
    forecast_time = None
    analysis_time = None
    observation_time = None
    with open(config_file, 'r') as f_id:
        cont = f_id.readlines()
        for ln in cont:
            ln = ln.strip()
            if 'forecast_time' in ln:
                forecast_time = float(ln.split(" ")[-1])
            if 'observation_time' in ln:
                observation_time = float(ln.split(" ")[-1])
            if 'analysis_time' in ln:
                analysis_time = float(ln.split(" ")[-1])
            if not (analysis_time is None or forecast_time is None or observation_time is None):
                break
    if verbose:
        if forecast_time is None:
            print("Failed to load forecast time")
        else:
            print("Forecast_time: %f" % forecast_time)
        if analysis_time is None:
            print("Failed to load analysis time")
        else:
            print("Aalysis_time: %f" % analysis_time)
        if observation_time is None:
            print("Failed to load observation time")
        else:
            print("Observation_time: %f" % observation_time)
    return forecast_time, analysis_time, observation_time

def update_state_file(file_path, new_time, err_file=None):
    """
    """
    try:
        state = DLidarVec.state_vector_from_file(file_path)
        if state.time == new_time:
            pass
            print("File: %s has the proper time %f" % (file_path, new_time))
        else:
            print("***\nUpdating state:\n\t%s\nWith Time:\n\t%f" % (file_path, new_time))
            state.time = new_time
            os.remove(file_path)  # shouldn't be needed!
            state.write_to_file(file_path)
            print("...TIME UPDATED ON FILE ...\n***\n")
    except:
        print("Failed to update file: %s" % file_path)
        if err_file is not None:
            with open(err_file, 'a') as f_id:
                f_id.write("%s\n" % file_path)
        else:
            raise


def update_files_times(results_dir, config_file_name, def_tf=0.0, def_ta=0.0, def_to=0.0, err_file='update_state_failed.dat'):
    """
    """
    # look for all files and a valid setup file
    try:
        tf, ta, to = time_from_file(os.path.join(results_dir, config_file_name))
    except(FileNotFoundError):
        print("Couldn't find proper config file: %s\nUSING DEFAULT TIMES FOR FILE UPDATE..." % os.path.join(results_dir, config_file_name))
        return
    if tf is None:
        tf = def_tf
    if ta is None:
        ta = def_ta
    if to is None:
        to = def_to

    list_of_files = utility.get_list_of_files(root_dir=results_dir, extension='dlvec')
    forecast_list = [f for f in list_of_files if os.path.split(f)[-1].lower().startswith('forecast_')]
    analysis_list = [f for f in list_of_files if os.path.split(f)[-1].lower().startswith('analysis_')]
    observations_list = [f for f in list_of_files if os.path.split(f)[-1].lower().startswith('observation')]
    forecast_list.sort()
    analysis_list.sort()
    observations_list.sort()

    if err_file is not None:
        _err_file = os.path.join(results_dir, err_file)
    else:
        _err_file = None

    for f in forecast_list:
        update_state_file(f, tf, _err_file)

    for f in analysis_list:
        update_state_file(f, ta, _err_file)

    for f in observations_list:
        update_state_file(f, to, _err_file)

if __name__ == '__main__':
    config_file_name = 'setup.dat'
    recursive = True

    root_results_dir = os.path.abspath(sys.argv[1])
    if recursive:
        dirs_list = utility.get_list_of_subdirectories(root_results_dir, return_abs=True, ignore_root=False)
    else:
        dirs_list = [root_results_dir]

    for dir_name in dirs_list:
        # print("Inspecting: %s" % dir_name)
        update_files_times(dir_name, config_file_name)
