#! /usr/bin/env python

"""
    This is a very SIMPLE scrip to run  Hypar model, e.g. Navier Stokes, and create a 3D movie of velocity field
"""

import os
import sys
import shutil

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()
import Py_HyPar

import numpy as np

from mpl_toolkits.mplot3d import axes3d, Axes3D
import matplotlib.pyplot as plot


def quiver3D_plotter(x, y, z, u, v, w, fname=None):

    if fname is None:
        _fname = 'velocity_field_quiver_3D_500_025.eps'
    else:
        _fname = fname

    # Plot velocity field
    fig = plot.figure()
    ax = Axes3D(fig)

    ax.quiver(x, y, z, u, v, w, length=70, arrow_length_ratio=0.20)
    ax.set_xlabel(r'$X$')
    ax.set_ylabel(r'$Y$')
    ax.set_zlabel(r'$Z$')
    ax.text2D(0.05, 0.95, r"Velocity Field $(u,\, v,\, w)$", transform=ax.transAxes)
    _, ext = os.path.splitext(fname)
    if ext:
        ext = ext.strip(' .')

    plot.draw()
    plot.savefig(_fname, dpi=250,
                        facecolor='w',
                        edgecolor='w',
                        orientation='portrait',
                        format=ext,
                        transparent=True,
                        bbox_inches='tight')
    plot.close(fig)
    return None

def create_dir(path=None, remove_existing=True):
    """
    Create a directory for plotting;
    """
    if path is None:
        dir_name = dir_name = '__Plots'
    else:
        dir_name = path
    if os.path.isdir(dir_name):
        if remove_existing:
            # Remove existing one (with all contents)
            shutil.rmtree(dir_name)
            # Now create after removeing it
            os.makedirs(dir_name)
        else:
            pass  # Don't remove the directory if exists
    else:
        os.makedirs(dir_name)
    return dir_name


if __name__ == '__main__':

    #
    # Create a model object with DEFAULT (+ PASSED) configurations; initial solution is automatically generated and loaded in the model.
    # model configureations; these are most of the options you may want to play with; defaul values will be used if removed from here
    model_configs = {'size': [101, 101, 41],  # grid settings; Nx, Ny, Nz
                     'domain_bounds': [50000, 50000, 4000],  # grid settings; upper limits of the doman; lower limits are set to 0
                     'iproc': [2, 2, 2],  # number of process in each direction
                     'dt': 0.25,  # time stepsize (in seconds) that is preserved by the Hypar/PETSc time integrator
                     'PETSc_options':'-ts_type rk -ts_rk_type 4',  # you don't have to put -use-petscts in the beginning
                     'init_time':0.0,  # (optional) time of the initial state (default is zero)
                     'ghost':3,
                     'rest_iter':0,
                     'ts':'rk',
                     'ts_type':'ssprk3',
                     'hyp_scheme':'weno5',
                     'hyp_flux_split':'no',
                     'hyp_int_type':'components',
                     'par_type':'none',
                     'par_scheme':'none'
                     }
    # Create model instances with configurations set above
    model = Py_HyPar.HyPar_Model(model_configs=model_configs)

    # Get model current state (initial condition)
    x0 = model.get_current_state()

    # Add smal perturbations (if you want)
    # x0 += 0.05  # additive constant noise is an option, or add normal erros as follows
    prtrb = np.random.randn(model.state_size()) * 0.05  # random perturbations (make sure perturbations are tiny for HyPar to converge!)
    x0[:] += prtrb  # __add__ supports two instances of StateVector or a scalar, but slicing [:] works with any conformable iterable

    # Propagae the state x0 over some give checkpoints (n_iter is changed internally, and dt is preserved for stability)
    checkpoints = np.arange(0, 20, 2)  # in seconds (dt is preserved, and n_iter is changed internally to get state at passed times)
    tspan, trajectory = model.integrate_state(state=x0, checkpoints=checkpoints)

    #
    # Plot settings:
    mask_size = 10  # Optionally skip every (mask_size-1) grid points in etach direction for a clear 3D plot
    fname_base = 'NS_3D'

    # Creating a directory for plots:
    dir_name = create_dir()

    # Get velocity field from trajectory, and plot at each time instance:
    for i, state in enumerate(trajectory):

        # Get the gridded velocity field; Note: passing x, y, z, u, v, w avoides reallocating memory for them
        try:
            x, y, z, u, v, w = model.velocity_field_gridded_to_numpy(state=state, x=x, y=y, z=z, u=u, v=v, w=w, mask_size=mask_size)
        except NameError:
            x, y, z, u, v, w = model.velocity_field_gridded_to_numpy(state=state, mask_size=mask_size)

        # plot updated velocity field
        fname = os.path.join(dir_name, "%s_%05d.eps"%(fname_base, i))
        quiver3D_plotter(x, y, z, u, v, w, fname)
        #

    #
    # Attempt to create a movie from saved eps files:
    vid_file = os.path.join(dir_name, "%s.mpeg"%fname_base)
    cmd = "convert -quality 10000 -delay 30 %s.eps %s" % (os.path.join(dir_name, "*"), vid_file)
    out = os.system(cmd)
    sep = "*" * 80
    if not out:
        print("\n%s\nA movie has been created inside: %s \n%s\n" % (sep, dir_name, sep))
        try:
            os.system('open %s' % vid_file)
        except:
            pass
    else:
        print("\n%s\nSORRY: Failed to Create a movie !\n\tCheck inside '%s' for some eps files \n%s\n" % (sep, dir_name, sep))
