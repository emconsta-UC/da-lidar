
# This is a very Simple module that contains plotting functionality for HyPar model
# TODO: Sync with new functionality added into filtering_results_reader module


import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import shutil

import numpy as np
from scipy import stats as scipy_stats

import matplotlib
from mpl_toolkits.mplot3d import axes3d, Axes3D
import matplotlib.pyplot as plt


_FIG_FORMAT = 'PNG'  # format of the saved plots (PNG easy to make movies)


def quiver3D_plotter(x, y, z, u, v, w, fname=None):

    if fname is None:
        _fname = 'velocity_field_quiver_3D_500_025.%s' % _FIG_FORMAT
    else:
        _fname = fname

    # Plot velocity field
    fig = plt.figure()
    ax = Axes3D(fig)

    ax.quiver(x, y, z, u, v, w, length=70, arrow_length_ratio=0.20)
    ax.set_xlabel(r'$X$')
    ax.set_ylabel(r'$Y$')
    ax.set_zlabel(r'$Z$')
    ax.text2D(0.05, 0.95, r"Velocity Field $(u,\, v,\, w)$", transform=ax.transAxes)
    _, ext = os.path.splitext(fname)
    if ext:
        ext = ext.strip(' .')

    plt.draw()
    plt.savefig(_fname, dpi=250,
                        facecolor='w',
                        edgecolor='w',
                        orientation='portrait',
                        format=ext,
                        transparent=True,
                        bbox_inches='tight')
    plt.close(fig)
    return None

def create_output_dir(path=None, remove_existing=True):
    """
    Create a directory for plotting;
    """
    if path is None:
        dir_name = dir_name = '__Plots'
    else:
        dir_name = path
    if os.path.isdir(dir_name):
        if remove_existing:
            # Remove existing one (with all contents)
            shutil.rmtree(dir_name)
            # Now create after removeing it
            os.makedirs(dir_name)
        else:
            pass  # Don't remove the directory if exists
    else:
        os.makedirs(dir_name)
    return dir_name


def _validate_plotting_indexes(x, y, z, index=None):
    """
    Internal function to validate the passed plotting index(es) againest coordinates

    Args:
        x, y, z: 3 one-D numpy arrays with x[i], y[i], z[i] referring to the ith coordinate
        index: None, integer, or an interable of len==3, specifying to slice the data vector along the 3D
            index refers to the value in the ordered set of uniqe grid points in each dimension

    Returns:
        xvals: 1D np.ndarray of unique gridpoints in the X-direction
        yvals: 1D np.ndarray of unique gridpoints in the X-direction
        zvals: 1D np.ndarray ofunique gridpoints in the X-direction
        index: validated list of three axis instances to add the plots to
        x_center: gridpoint in the X-direction at which slicing (YZ-slice) is created
        y_center: gridpoint in the Y-direction at which slicing (XZ-slice) is created
        z_center: gridpoint in the Z-direction at which slicing (YY-slice) is created

    """
    # Get unique grid points:
    xvals = np.asarray(list(set(x)))
    yvals = np.asarray(list(set(y)))
    zvals = np.asarray(list(set(z)))
    xvals.sort()
    yvals.sort()
    zvals.sort()

    # Default center indexes
    try:
        x_ce = np.where(xvals>=xvals.max()/2.0)[0][0]
        y_ce = np.where(yvals>=yvals.max()/2.0)[0][0]
    except:
        x_ce = xvals.size // 2
        y_ce = yvals.size // 2
    z_ce = 0

    # Validate slices indexes:
    if index is None:
        index = [x_ce, y_ce, z_ce]
    elif isinstance(index, int):
        index = [index] * 3
    else:
        try:
            index = [i for i in index]
        except TypeError:
            print("index must be either None, integer index, or an iterable of lenght <= 3")
            print("Received index of type %s; index is: " % (type(index), index))
            raise
        if len(index) > 3:
            print("index lenght can't be more than 3?!")
            raise ValueError
        else:
            index = index + [None]*(3-len(index))

    # Assert length and proper values
    assert len(index) == 3, "This should never happen! length of index must be 3 at this point; %s" % str(index)

    # Override None values
    if index[0] is None:
        index[0] = x_ce
    if index[1] is None:
        index[1] = y_ce
    if index[2] is None:
        index[2] = z_ce

    # Centers
    x_center = xvals[index[0]]
    y_center = yvals[index[1]]
    z_center = zvals[index[2]]
    #
    if x_center>=xvals[-1]:
        print("X slicing index is out of range; Nothing to plot in X direction; maximum grid point is %f" % xvals[-1])
        index[0] = None
        x_center = None
    if y_center>=yvals[-1]:
        print("Y slicing index is out of range!; Nothing to plot in Y direction; maximum grid point is %f" % yvals[-1])
        index[1] = None
        y_center = None
    if z_center>=zvals[-1]:
        print("Z slicing index is out of range!; Nothing to plot in Z direction; maximum grid point is %f" % zvals[-1])
        index[2] = None
        z_center = None
    #
    return xvals, yvals, zvals, index, x_center, y_center, z_center
    #

def plot_contourf_slices(x, y, z, data, index,
                         single_axis=True,
                         target_axis=None,
                         filename=None,
                         add_color_bars=True,
                         cmap='jet',
                         vmin=None,
                         vmax=None,
                         honor_data_limits=False,
                         force_passed_levels=False,
                         dpi=500,
                         alpha=0.6,
                         font_size=8
                        ):
    """
    Plot 3D projected slices of the passed data, given the slicing index

    Args:
        x, y, z: oneD arrays containing grid coordinates (xi, yi, zi)
        data: onD data corresponding the xyz coordinates
        index: None, integer, or an interable of len==3, specifying to slice the data vector along the 3D
            index refers to the value in the ordered set of uniqe grid points in each dimension
        single_axis: either plot al slices on the same axis, or subplots; used only if target_axds is None
        target_axis: if not None; must be a matplotlib axis in which case single_axis is set to True and all slices are plotted to this axis.
            This is where the data is plotted

    Returns:
        fig: the plotted figure

    """
    # TODO: Now, this works nicely; and should be extended to plot u, v, and w
    #       Follow 'plot_wind_slices'
    xvals, yvals, zvals, index, x_center, y_center, z_center  = _validate_plotting_indexes(x, y, z, index)

    nons = []
    for i, ind in enumerate(index):
        if ind is None:
            nons.append(i)
    if len(nons) == 3:
        print("All indixes are None!")
        raise ValueError

    # Get grid sizes and limits
    nx = xvals.size
    ny = yvals.size
    nz = zvals.size
    # Grid limits:
    x_lims = [xvals[0], xvals[-1]]
    y_lims = [yvals[0], yvals[-1]]
    z_lims = [zvals[0], zvals[-1]]

    if target_axis is not None:
        if isinstance(target_axis, matplotlib.axes._axes.Axes):
            # single axes
            single_axis = True
        else:
            single_axis = False
    else:
        pass

    # Start plotting
    # fig, axes = plt.subplots(3, 3, sharex='col', sharey='row', figsize=(18,10), dpi=85, facecolor='white')
    ax_height = 4.5
    axes = []
    if single_axis:
        if target_axis is not None:
            ax = target_axis
            fig = ax.get_figure()
        else:
            figsize = (ax_height*1.5, ax_height)
            fig = plt.figure(figsize=figsize, facecolor='white')
            ax = fig.add_subplot(111, projection='3d')
        axes = [ax, ax, ax]
    else:
        added_index = 1
        valid_indexes = 3-len(nons)
        figsize = (ax_height*(valid_indexes+0.5), ax_height)
        fig = plt.figure(figsize=figsize, facecolor='white')
        for i in range(1, 4):
            if i-1 in nons:
                ax = None
            else:
                ax = fig.add_subplot(1, valid_indexes, added_index, projection='3d')
                added_index += 1
            axes.append(ax)

    centers = [x_center, y_center, z_center]
    data_lims = [data.min(), data.max()]
    ims = []
    for slice_ind, slice_name  in enumerate(['X', 'Y', 'Z']):

        if slice_ind in nons:
            continue

        # Plot Theta Vals:
        ax = axes[slice_ind]
        if ax is None:
            print("Axes is None!")
            raise ValueError

        # Slice in each direction
        # ========

        # location along x axis
        if centers[slice_ind] is None:
            continue

        # get the target grdipoints
        if slice_ind == 0:
            locs = np.where(x==x_center)
            X = np.reshape(data[locs], (ny, nz))
            Y = y[locs].reshape((ny, nz))
            Z = z[locs].reshape((ny, nz))
            offset = x_center
            zdir = 'x'
        elif slice_ind == 1:
            locs = np.where(y==y_center)
            X = x[locs].reshape((nx, nz))
            Y = np.reshape(data[locs], (nx, nz))
            Z = z[locs].reshape((nx, nz))
            offset = y_center
            zdir = 'y'
        else:
            locs = np.where(z==z_center)
            X = x[locs].reshape((nx, ny))
            Y = y[locs].reshape((nx, ny))
            Z = np.reshape(data[locs], (nx, ny))
            offset = z_center
            zdir = 'z'

        # extract values along selected locations:
        target_data = data[locs].copy()  # no need to copy!
        # print("Maximum data value: ", target_data.max())
        # print("Minimum data value: ", target_data.min())

        if honor_data_limits:
            pass
        else:
            vmin = min(vmin, data_lims[0])
            vmax = max(vmax, data_lims[1])

        if None not in [vmin, vmax]:
            levels = np.linspace(vmin, vmax, 20)  # Number of color levels
        else:
            levels = None
        #
        if force_passed_levels:
            im = ax.contourf(X, Y, Z,
                             zdir=zdir,
                             offset=offset,
                             alpha=alpha,
                             levels=levels,
                             cmap=cmap
                            )
        else:
            im = ax.contourf(X, Y, Z,
                             zdir=zdir,
                             offset=offset,
                             alpha=alpha,
                             levels=levels,
                             cmap=cmap
                            )

        ims.append(im)
        # ax.autoscale(True)


    # Set titles and labels
    for slice_ind, ax in enumerate(axes):
        if not single_axis:
            title = r'$%s=%f$ Slice'% (slice_name, centers[slice_ind])
        else:
            # print("*** CENTERS *** ", centers)
            title = r"Slices: $X=%s$; $Y=%s$; $Z=%s$\n" % tuple([np.round(c, 2) for c in centers])
        ax.set_title(r'%s'% title, fontsize=font_size)
        ax.set_xlabel(r'$X$')
        ax.set_ylabel(r'$Y$')
        ax.set_zlabel(r'$Z$')
        #
        ax.set_aspect('auto')
        ax.set_xlim(x_lims[0], x_lims[1])
        ax.set_ylim(y_lims[0], y_lims[1])
        ax.set_zlim(z_lims[0], z_lims[1])
        #
        if add_color_bars:
            fig = ax.get_figure()
            cbar = fig.colorbar(ims[slice_ind], ax=ax)
            cbar.ax.tick_params(labelsize=font_size)
        #
        if single_axis:
            break

    if not single_axis:
        fig.subplots_adjust(wspace=0.05)

    if filename is not None:
        # Save figure given the passed file name:
        plt.savefig(filename, dpi=dpi, facecolor='w', transparent=True, bbox_inches='tight')
        print("\rSaved: %s   \n" % filename),
    #
    return fig
    #

def plot_temperature(x, y, z, theta, theta0, index=None, filename=None, cmap='jet', interpolation='bilinear', font_size=8):
    """
    Plot 2D section, of the temperature, theta, theta0, theta-theta0,
    at a specific X coordinate, Y coordinate, and Z coordinate (across the center of the domain)

    Args:
        x, y, z: oneD arrays containing grid coordinates (xi, yi, zi)
        theta:
        theta0:
        index: None, integer, or an interable of len==3, specifying to slice the data vector along the 3D
            index refers to the value in the ordered set of uniqe grid points in each dimension

    Returns:
        fig
        (x_vals, y_vals, z_vals)
        (theta_vals, theta0_vals, theta_diff)

    """
    xvals, yvals, zvals, index, x_center, y_center, z_center  = _validate_plotting_indexes(x, y, z, index)

    # Get grid sizes and limits
    nx = xvals.size
    ny = yvals.size
    nz = zvals.size
    # Grid limits:
    x_lims = [xvals[0], xvals[-1]]
    y_lims = [yvals[0], yvals[-1]]
    z_lims = [zvals[0], zvals[-1]]

    reshape_order = 'C'

    # fig, axes = plt.subplots(3, 3, sharex='col', sharey='row', figsize=(18,10), dpi=85, facecolor='white')
    fig, axes = plt.subplots(3, 3, figsize=(18,10), dpi=85, facecolor='white')

    # X Slice
    # ========
    if x_center is not None:
        # location along x axis
        locs = np.where(x==x_center)
        x_vals = x[locs]
        y_vals, z_vals = y[locs], z[locs]
        # extract values along selected locations:
        theta_vals = theta[locs]
        theta0_vals = theta0[locs]
        theta_diff = theta_vals - theta0_vals
        # limits:
        theta_lims = [theta_vals.min(), theta_vals.max()]
        theta0_lims = [theta0_vals.min(), theta0_vals.max()]
        theta_diff_lims = [theta_diff.min(), theta_diff.max()]

        # Plot Theta Vals:
        ax0 = axes[0, 0]
        im0 = ax0.imshow(theta_vals.reshape((ny, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta_lims[0], vmax=theta_lims[1], interpolation=interpolation, cmap=cmap)
        ax0.autoscale(True)
        ax0.set_ylabel(r'$\theta$')
        ax0.set_title(r'$X$-Slice (X=%f)'%x_center, fontsize=font_size)
        # ax0.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        #
        # Plot Theta0 Vals
        ax1 = axes[1, 0]
        im1 = ax1.imshow(theta0_vals.reshape((ny, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta0_lims[0], vmax=theta0_lims[1], interpolation=interpolation, cmap=cmap)
        ax1.autoscale(True)
        ax1.set_ylabel(r'$\theta_0$')
        # ax1.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        #
        # Plot Theta-Theta0  Vals
        ax2 = axes[2, 0]
        im2 = ax2.imshow(theta_diff.reshape((ny, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta_diff_lims[0], vmax=theta_diff_lims[1], interpolation=interpolation, cmap=cmap)
        ax2.autoscale(True)
        ax2.set_ylabel(r'$\theta-\theta_0$')
        # ax2.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

        # set tick labels
        for ax in [ax0, ax1, ax2]:
            ticks = ax.get_xticks()
            ticklabels = ax.get_xticklabels()
            ticklabels[1: -1] = yvals[ticks[1: -1].astype(np.int)]
            ax.set_xticklabels(ticklabels)
            ticks = ax.get_yticks()
            ticklabels = ax.get_yticklabels()
            ticklabels[1: -1] = np.round(zvals[ticks[1: -1].astype(np.int)], 2)
            ax.set_yticklabels(ticklabels)
            ax.tick_params(axis='both', which='major', labelsize=font_size)

    # Y Slice
    # ========
    if y_center is not None:
        # location along y axis
        locs = np.where(y==y_center)
        y_vals = y[locs]
        x_vals, z_vals = x[locs], z[locs]
        # extract values along selected locations:
        theta_vals = theta[locs]
        theta0_vals = theta0[locs]
        theta_diff = theta_vals - theta0_vals
        # limits:
        theta_lims = [theta_vals.min(), theta_vals.max()]
        theta0_lims = [theta0_vals.min(), theta0_vals.max()]
        theta_diff_lims = [theta_diff.min(), theta_diff.max()]

        # Plot Theta Vals:
        ax3 = axes[0, 1]
        im3 = ax3.imshow(theta_vals.reshape((nx, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta_lims[0], vmax=theta_lims[1], interpolation=interpolation, cmap=cmap)
        ax3.autoscale(True)
        # ax3.set_ylabel(r'$\theta$')
        ax3.set_title(r'$Y$-Slice (Y=%f)'%y_center, fontsize=font_size)
        # ax3.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        #
        # Plot Theta0 Vals
        ax4 = axes[1, 1]
        im4 = ax4.imshow(theta0_vals.reshape((nx, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta0_lims[0], vmax=theta0_lims[1], interpolation=interpolation, cmap=cmap)
        ax4.autoscale(True)
        # ax4.set_title(r'$\theta_0$')
        # ax4.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        #
        # Plot Theta-Theta0  Vals
        ax5 = axes[2, 1]
        im5 = ax5.imshow(theta_diff.reshape((nx, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta_diff_lims[0], vmax=theta_diff_lims[1], interpolation=interpolation, cmap=cmap)
        ax5.autoscale(True)
        # ax5.set_ylabel(r'$\theta-\theta_0$')

        # set tick labels
        for ax in [ax3, ax4, ax5]:
            ticks = ax.get_xticks()
            ticklabels = ax.get_xticklabels()
            ticklabels[1: -1] = xvals[ticks[1: -1].astype(np.int)]
            ax.set_xticklabels(ticklabels)
            ticks = ax.get_yticks()
            ticklabels = ax.get_yticklabels()
            ticklabels[1: -1] = np.round(zvals[ticks[1: -1].astype(np.int)], 2)
            ax.set_yticklabels(ticklabels)
            ax.tick_params(axis='both', which='major', labelsize=font_size)

       # ax5.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

    # Z Slice
    # ========
    if z_center is not None:
        # location along z axis
        locs = np.where(z==z_center)
        z_vals = z[locs]
        x_vals, y_vals = x[locs], y[locs]
        # extract values along selected locations:
        theta_vals = theta[locs]
        theta0_vals = theta0[locs]
        theta_diff = theta_vals - theta0_vals
        # limits:
        theta_lims = [theta_vals.min(), theta_vals.max()]
        theta0_lims = [theta0_vals.min(), theta0_vals.max()]
        theta_diff_lims = [theta_diff.min(), theta_diff.max()]

        # Plot Theta Vals:
        ax6 = axes[0, 2]
        im6 = ax6.imshow(theta_vals.reshape((nx, ny), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta_lims[0], vmax=theta_lims[1], interpolation=interpolation, cmap=cmap)
        ax6.autoscale(True)
        # ax6.set_ylabel(r'$\theta$')
        ax6.set_title(r'$Z$-Slice (Z=%f)'%z_center, fontsize=font_size)
        # ax6.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        #
        # Plot Theta0 Vals
        ax7 = axes[1, 2]
        im7 = ax7.imshow(theta0_vals.reshape((nx, ny), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta0_lims[0], vmax=theta0_lims[1], interpolation=interpolation, cmap=cmap)
        ax7.autoscale(True)
        # ax7.set_ylabel(r'$\theta_0$')
        # ax7.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        #
        # Plot Theta-Theta0  Vals
        ax8 = axes[2, 2]
        im8 = ax8.imshow(theta_diff.reshape((nx, ny), order=reshape_order).T, origin='lower', aspect='auto', vmin=theta_diff_lims[0], vmax=theta_diff_lims[1], interpolation=interpolation, cmap=cmap)
        ax8.autoscale(True)
        # ax8.set_ylabel(r'$\theta-\theta_0$')
        # ax8.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

        # set tick labels
        for ax in [ax6, ax7, ax8]:
            ticks = ax.get_xticks()
            ticklabels = ax.get_xticklabels()
            ticklabels[1: -1] = yvals[ticks[1: -1].astype(np.int)]
            ax.set_xticklabels(ticklabels)
            ticks = ax.get_yticks()
            ticklabels = ax.get_yticklabels()
            ticklabels[1: -1] = np.round(yvals[ticks[1: -1].astype(np.int)], 2)
            ax.set_yticklabels(ticklabels)
            ax.tick_params(axis='both', which='major', labelsize=font_size)

    # Adjust wind-velocity subplots:
    plt.subplots_adjust(hspace=0.25, left=0.1, bottom=0.20, right=0.73, top=0.85)

    # Adjust Ticks

    # Add Colorbars
    if x_center is not None:
        cbar = fig.colorbar(im0, ax=ax0)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im1, ax=ax1)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im2, ax=ax2)
        cbar.ax.tick_params(labelsize=font_size)
    if y_center is not None:
        cbar = fig.colorbar(im3, ax=ax3)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im4, ax=ax4)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im5, ax=ax5)
        cbar.ax.tick_params(labelsize=font_size)
    if z_center is not None:
        cbar = fig.colorbar(im6, ax=ax6)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im7, ax=ax7)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im8, ax=ax8)
        cbar.ax.tick_params(labelsize=font_size)

    if filename is not None:
        # Save figure given the passed file name:
        plt.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
        print("\rSaved: %s   \n" % filename),
    #
    return fig, (x_vals, y_vals, z_vals), (theta_vals, theta0_vals, theta_diff)


def plot_wind_slices(x, y, z, u, v, w, index=None, filename=None,  cmap='jet', interpolation='bilinear', font_size=8):
    """
    Plot 2D section, of the temperature, theta, theta0, theta-theta0,
    at a specific X coordinate, Y coordinate, and Z coordinate (across the center of the domain)

    Args:
        x, y, z: oneD arrays containing grid coordinates (xi, yi, zi)
        theta:
        theta0:
        index: None, integer, or an interable of len==3, specifying to slice the data vector along the 3D
            index refers to the value in the ordered set of uniqe grid points in each dimension

    Returns:
        fig

    """
    xvals, yvals, zvals, index, x_center, y_center, z_center  = _validate_plotting_indexes(x, y, z, index)

    # Get grid sizes and limits
    nx = xvals.size
    ny = yvals.size
    nz = zvals.size
    # Grid limits:
    x_lims = [xvals[0], xvals[-1]]
    y_lims = [yvals[0], yvals[-1]]
    z_lims = [zvals[0], zvals[-1]]

    reshape_order = 'C'

    # fig, axes = plt.subplots(3, 3, sharex='col', sharey='row', figsize=(18,10), dpi=85, facecolor='white')
    fig, axes = plt.subplots(3, 3, figsize=(18,10), dpi=85, facecolor='white')

    # X Slice
    # ========
    if x_center is not None:
        # location along x axis
        locs = np.where(x==x_center)
        y_vals, z_vals = y[locs], z[locs]
        # extract values along selected locations:
        u_vals = u[locs]
        v_vals = v[locs]
        w_vals = w[locs]
        # limits:
        u_lims = [u_vals.min(), u_vals.max()]
        v_lims = [v_vals.min(), v_vals.max()]
        w_lims = [w_vals.min(), w_vals.max()]

        # U Vals:
        ax0 = axes[0, 0]
        im0 = ax0.imshow(u_vals.reshape((ny, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=u_lims[0], vmax=u_lims[1], interpolation=interpolation, cmap=cmap)
        ax0.autoscale(True)
        ax0.set_ylabel(r'$u$')
        ax0.set_title(r'$X$-Slice (X=%f)'%x_center, fontsize=font_size)
        # ax0.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        # V Vals
        ax1 = axes[1, 0]
        im1 = ax1.imshow(v_vals.reshape((ny, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=v_lims[0], vmax=v_lims[1], interpolation=interpolation, cmap=cmap)
        ax1.autoscale(True)
        ax1.set_ylabel(r'$v$')
        # ax1.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        # W Vals
        ax2 = axes[2, 0]
        im2 = ax2.imshow(w_vals.reshape((ny, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=w_lims[0], vmax=w_lims[1], interpolation=interpolation, cmap=cmap)
        ax2.autoscale(True)
        ax2.set_ylabel(r'$w$')
        # ax2.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

        # set tick labels
        for ax in [ax0, ax1, ax2]:
            ticks = ax.get_xticks()
            ticklabels = ax.get_xticklabels()
            ticklabels[1: -1] = yvals[ticks[1: -1].astype(np.int)]
            ax.set_xticklabels(ticklabels)
            ticks = ax.get_yticks()
            ticklabels = ax.get_yticklabels()
            ticklabels[1: -1] = np.round(zvals[ticks[1: -1].astype(np.int)], 2)
            ax.set_yticklabels(ticklabels)
            ax.tick_params(axis='both', which='major', labelsize=font_size)


    # Y Slice
    # ========
    if y_center is not None:
        # location along x axis
        locs = np.where(y==y_center)
        x_vals, z_vals = x[locs], z[locs]
        # extract values along selected locations:
        u_vals = u[locs]
        v_vals = v[locs]
        w_vals = w[locs]
        # limits:
        u_lims = [u_vals.min(), u_vals.max()]
        v_lims = [v_vals.min(), v_vals.max()]
        w_lims = [w_vals.min(), w_vals.max()]

        # U Vals:
        ax3 = axes[0, 1]
        im3 = ax3.imshow(u_vals.reshape((nx, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=u_lims[0], vmax=u_lims[1], interpolation=interpolation, cmap=cmap)
        ax3.autoscale(True)
        # ax3.set_ylabel(r'$u$')
        ax3.set_title(r'$Y$-Slice (Y=%f)'%y_center, fontsize=font_size)
        # ax3.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        # V Vals
        ax4 = axes[1, 1]
        im4 = ax4.imshow(v_vals.reshape((nx, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=v_lims[0], vmax=v_lims[1], interpolation=interpolation, cmap=cmap)
        ax4.autoscale(True)
        # ax4.set_title(r'$v$')
        # ax4.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        # W Vals
        ax5 = axes[2, 1]
        im5 = ax5.imshow(w_vals.reshape((nx, nz), order=reshape_order).T, origin='lower', aspect='auto', vmin=w_lims[0], vmax=w_lims[1], interpolation=interpolation, cmap=cmap)
        ax5.autoscale(True)
        # ax5.set_ylabel(r'$w$')
        # ax5.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

        # set tick labels
        for ax in [ax3, ax4, ax5]:
            ticks = ax.get_xticks()
            ticklabels = ax.get_xticklabels()
            ticklabels[1: -1] = xvals[ticks[1: -1].astype(np.int)]
            ax.set_xticklabels(ticklabels)
            ticks = ax.get_yticks()
            ticklabels = ax.get_yticklabels()
            ticklabels[1: -1] = np.round(zvals[ticks[1: -1].astype(np.int)])
            ax.set_yticklabels(ticklabels)
            ax.tick_params(axis='both', which='major', labelsize=font_size)

    # Z Slice
    # ========
    if z_center is not None:
        # location along x axis
        locs = np.where(z==z_center)
        x_vals, y_vals = x[locs], y[locs]
        # extract values along selected locations:
        u_vals = u[locs]
        v_vals = v[locs]
        w_vals = w[locs]
        # limits:
        u_lims = [u_vals.min(), u_vals.max()]
        v_lims = [v_vals.min(), v_vals.max()]
        w_lims = [w_vals.min(), w_vals.max()]

        # U Vals:
        ax6 = axes[0, 2]
        im6 = ax6.imshow(u_vals.reshape((nx, ny), order=reshape_order).T, origin='lower', aspect='auto', vmin=u_lims[0], vmax=u_lims[1], interpolation=interpolation, cmap=cmap)
        ax6.autoscale(True)
        # ax6.set_ylabel(r'$u$')
        ax6.set_title(r'$Z$-Slice (Z=%f)'%z_center, fontsize=font_size)
        # ax6.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        # V Vals
        ax7 = axes[1, 2]
        im7 = ax7.imshow(v_vals.reshape((nx, ny), order=reshape_order).T, origin='lower', aspect='auto', vmin=v_lims[0], vmax=v_lims[1], interpolation=interpolation, cmap=cmap)
        ax7.autoscale(True)
        # ax7.set_ylabel(r'$v$')
        # ax7.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        # W Vals
        ax8 = axes[2, 2]
        im8 = ax8.imshow(w_vals.reshape((nx, ny), order=reshape_order).T, origin='lower', aspect='auto', vmin=w_lims[0], vmax=w_lims[1], interpolation=interpolation, cmap=cmap)
        ax8.autoscale(True)
        # ax8.set_ylabel(r'$w$')
        # ax8.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

        # set tick labels
        for ax in [ax6, ax7, ax8]:
            ticks = ax.get_xticks()
            ticklabels = ax.get_xticklabels()
            ticklabels[1: -1] = yvals[ticks[1: -1].astype(np.int)]
            ax.set_xticklabels(ticklabels)
            ticks = ax.get_yticks()
            ticklabels = ax.get_yticklabels()
            ticklabels[1: -1] = np.round(yvals[ticks[1: -1].astype(np.int)], 2)
            ax.set_yticklabels(ticklabels)
            ax.tick_params(axis='both', which='major', labelsize=font_size)

    # Adjust wind-velocity subplots:
    plt.subplots_adjust(hspace=0.25, left=0.1, bottom=0.20, right=0.73, top=0.85)

    # Adjust Ticks

    # Add Colorbars
    if x_center is not None:
        cbar = fig.colorbar(im0, ax=ax0)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im1, ax=ax1)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im2, ax=ax2)
        cbar.ax.tick_params(labelsize=font_size)
    if y_center is not None:
        cbar = fig.colorbar(im3, ax=ax3)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im4, ax=ax4)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im5, ax=ax5)
        cbar.ax.tick_params(labelsize=font_size)
    if z_center is not None:
        cbar = fig.colorbar(im6, ax=ax6)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im7, ax=ax7)
        cbar.ax.tick_params(labelsize=font_size)
        cbar = fig.colorbar(im8, ax=ax8)
        cbar.ax.tick_params(labelsize=font_size)

    if filename is not None:
        # Save figure given the passed file name:
        plt.savefig(filename, dpi=500, facecolor='w', transparent=True, bbox_inches='tight')
        print("\rSaved: %s   \n" % filename),
    #
    return fig


def rank_histogram(ensembles_repo, reference_state,
                   first_var=0,
                   last_var=None,
                   var_skp=1,
                   draw_hist=False,
                   target_fig=None,
                   target_ax=None,
                   hist_type='relfreq',
                   hist_title=None,
                   hist_max_height=None,
                   font_size=None,
                   ignore_indexes=None,
                   add_fitted_beta=False,
                   add_uniform=False,
                   zorder=0,
                   verbose=False
                  ):
    """
    Calculate the rank statistics of the true solution/observations w.r.t
    an ensemble of states/observations

    Args:
        ensembles_repo: an ensemble of model states (or model observations).
        reference_state: truth
        first_var: initial index in the reference states to evaluate ranks at
        last_var: last index in the reference states to evaluate ranks at
        var_skp: number of skipped variables to reduce correlation effect
        draw_hist: If True, a rank histogram is plotted, and a figure handle is returned,
                   None is returned otherwise
        target_fig, target axes are used if draw_hist is True. If target_ax is not None, histogram is added to it,
        hist_type: 'freq' vs 'relfreq': Frequency vs Relative frequencies for plotting.
                   Used only when 'draw_hist' is True.
        hist_title: histogram plot title (if given), and 'draw_hist' is True.
        hist_max_height: ,
        font_size: ,
        ignore_indexes: 1d iterable stating indexes of the state vector to ignore while calculating frequencies/relative frequencies
        add_fitted_beta: fit a beta disgtribution, and add to plot (only if draw_hist is True)
        add_uniform: add a perfect uniform distribution, and add to plot (only if draw_hist is True)
        zorder: order of the bars on the figure

    Returns:
        ranks_freq: frequencies of the rank of truth among ensemble members
        ranks_rel_freq: relative frequencies of the rank of truth among ensemble members
        bins_bounds: bounds of the bar plot
        fig_hist: a matlab.pyplot figure handle of the rank histogram plot

    """
    # Assertions:
    assert isinstance(first_var, int), "'first_var' has to be an integer!"
    if last_var is not None:
        assert isinstance(last_var, int), "'last_var' has to be either None, or an integer!"
    if var_skp is not None:
        assert isinstance(var_skp, int), "'var_skp' has to be either None, or an integer!"

    if verbose:
        print("Constructing Rank Histogram")

    if ignore_indexes is not None:
        local_ignore_inds = np.asarray(ignore_indexes).squeeze()
    else:
        local_ignore_inds = None

    # Check dimensionality of inputs:
    # 1- get a squeezed view of 'ensembles_repo'
    if isinstance(ensembles_repo, list):
        ens_size = len(ensembles_repo)
        st_size = len(ensembles_repo[0])
        loc_ensembles_repo = np.empty((st_size, ens_size))
        for i in range(ens_size):
            loc_ensembles_repo[:, i] = ensembles_repo[i][:]
    elif isinstance(ensembles_repo, np.ndarray):
        pass
    else:
        try:
            loc_ensembles_repo = ensembles_repo.get_numpy_array()
        except:
            print("The ensemble repository must be a list of iterables, an istnace of DL_Vec.Ensemble, or a two dimensional numpy array")
    ens_ndim = loc_ensembles_repo.ndim
    loc_ensembles_repo_shape = loc_ensembles_repo.shape

    state_size = loc_ensembles_repo_shape[0]
    ensemble_size = loc_ensembles_repo_shape[1]
    #
    # 2- get a squeezed view of 'reference_repo'
    try:
        loc_reference_repo = reference_state.get_numpy_array()
    except(AttributeError):
        loc_reference_repo = reference_state[:]
    ref_dim = loc_reference_repo.ndim
    ref_state_size = loc_reference_repo.size

    if state_size != ref_state_size:
        print("Mismatch in state/observation size!")
        raise AssertionError

    #
    if not (0 <= first_var <= state_size-1):
        first_var = 0
    #
    if last_var is None:
        last_var = state_size-1
    else:
        if last_var > state_size-1:
            last_var = state_size-1
        elif last_var < 0:
            last_var = 1
    #
    if not (1 <= var_skp):
        first_var = 1

    if not isinstance(hist_type, str):
        hist_type = 'freq'
    #
    # Done with assertion, and validation...
    #

    # Initialize results placeholders:
    ranksmat_length = ensemble_size + 1
    ranks_freq = np.zeros(ranksmat_length, dtype=int)

    # Start calculating ranks (of truth) w.r.t ensembles:
    if verbose:
        print("Calculating Ranks")
        #
    for var_ind in range (first_var, last_var+1, var_skp):
        # TODO: refactor/vectorize!
        if local_ignore_inds is not None:
            if var_ind in local_ignore_inds:
                continue
        else:
            pass

        ref_sol = loc_reference_repo[var_ind]
        # Check for NaNs:
        if np.isnan(ref_sol) or np.isinf(ref_sol):
            continue

        augmented_vec = loc_ensembles_repo[var_ind, :]
        augmented_vec = np.append(ref_sol, augmented_vec).flatten()

        # remove NaN and Inf (Can happen for observation operator resulting in NaNs
        if np.isinf(augmented_vec).any() or np.isnan(augmented_vec).any():
            # print("The augmented Vector still have Nans!")
            continue
        # rnk = np.where(np.sort(augmented_vec) == ref_sol)[0]
        # rnk = np.argsort(augmented_vec)[0]  # get rank of true/ref state/observation
        rnk = np.where(np.isclose(np.sort(augmented_vec), ref_sol))[0]
        if verbose:
            print("*"*50)
            print("ref_sol: ", ref_sol)
            print("augmented_vec: ", augmented_vec)
            print("Ranks: ", rnk)
        #
        if rnk.size > 1:
            rnk = np.random.choice(rnk, size=1)[0]
        elif rnk.size == 1:
            rnk = rnk[0]
        else:
            print("Impossible situation in rank histogram; truth is lost after augmentation!")
            raise ValueError
        #
        if verbose:
            print("Final rank of truth >>> ", rnk)
        #
        # msg = "rnk=%d; prior-freq=%f;" % (rnk, ranks_freq[rnk])
        ranks_freq[rnk] += 1
        # msg += "post-freq=%f" % ranks_freq[rnk]
        # print(msg)
        #
    # calculate ranks relative frequences
    ranks_rel_freq = ranks_freq / float(np.nansum(ranks_freq))
    # bounds of rank histogram plot:
    bins_bounds = np.arange(ensemble_size+1)

    # Draw the rank histogram
    if draw_hist:
        if verbose:
            print("Plotting the rank histogram...")
        beta_label = None
        u_label = None

        # Based on hist_type decide on the height of histogram bars
        if hist_type.lower() == 'freq':
            bins_heights = ranks_freq
            ylabel = 'Frequency'
        elif hist_type.lower() == 'relfreq':
            bins_heights = ranks_rel_freq
            ylabel = 'Relative Frequency'
        else:
            print("Unrecognized histogram plot type %s" % hist_type)
            raise ValueError

        # Start plotting:
        if target_fig is None and target_ax is None:
            fig_hist, ax = plt.subplots(facecolor='white')
        elif target_fig is None:
            ax = target_ax
            fig_hist = ax.get_figure()
        elif target_ax is None:
            fig_hist = target_fig
            ax = fig_hist.gca()
        ax.bar(bins_bounds , bins_heights, width=1, color='green', edgecolor='black', zorder=zorder)

        # Adjust limits of the plot as necessary:
        ax.set_xlim(-0.5, ensemble_size+0.5)
        if hist_max_height is not None and np.isscalar(hist_max_height):
            ax.set_ylim(0, max(hist_max_height, bins_heights.max()+1e-2))
        #
        if hist_title is not None and isinstance(hist_title, str):
            if font_size is not None:
                fig_hist.suptitle(hist_title, fontsize=font_size)
                for tickx, ticky in zip(ax.xaxis.get_major_ticks(), ax.yaxis.get_major_ticks()):
                    tickx.label.set_fontsize(font_size)
                    ticky.label.set_fontsize(font_size)
            else:
                fig_hist.suptitle(hist_title)

        if font_size is not None:
            ax.set_xlabel("Rank", fontsize=font_size)
            ax.set_ylabel(ylabel, fontsize=font_size)
        else:
            ax.set_xlabel("Rank")
            ax.set_ylabel(ylabel)
        #
        if add_fitted_beta:
            # Fit a beta distribution:
            # create data from frequencies:
            data = []
            for fr, bn in zip(ranks_freq, bins_bounds):
                data += [float(bn)] * fr
            data = np.asarray(data)
            # fit beta dist to generated data:
            dist = scipy_stats.beta
            try:
                params = dist.fit(data)  # beta distribution parameters
                beta_failed = False
                # print("Beta fitted params: ", params)
            except(ValueError):
                beta_failed = True

            if not beta_failed:
                # generate a pdf curve for fitted beta:
                pdf_x = np.linspace(dist.ppf(0.01, params[0], params[1]), dist.ppf(0.99, params[0], params[1]), 100)
                pdf_y = dist.pdf(pdf_x, params[0], params[1])

                # avoid very large values
                pdf_y[np.where(pdf_y>1e+3)[0]] = np.nan


                # shift X values to 0 to ensemble_size
                a, b = np.nanmin(pdf_x), np.nanmax(pdf_x)
                c, d = 0, ensemble_size
                pdf_x = c + ((d-c)/(b-a)) * (pdf_x-a)

                # scale Y's
                if True:
                    _ylims = ax.get_ylim()
                    y_scale = _ylims[1] - _ylims[0]
                    ul = y_scale*0.95 + _ylims[0]
                    fac = ul / np.nanmax(pdf_y)
                    pdf_y = pdf_y*fac + _ylims[0]
                else:
                    a, b = np.nanmin(pdf_y), np.nanmax(pdf_y)
                    _ylims = ax.get_ylim()
                    y_scale = _ylims[1] - _ylims[0]
                    c = y_scale*0.05 + _ylims[0]
                    d = y_scale*0.95 + _ylims[0]
                    pdf_y = c + ((d-c)/(b-a)) * (pdf_y-a)


                # Adjust pdf_y to the bins:
                if hist_type.lower() == 'freq':
                    if np.nanmin(pdf_y) < ranks_freq.min():
                        pdf_y += abs(np.nanmin(pdf_y)-ranks_freq.min())
                    else:
                        pdf_y -= abs(np.nanmin(pdf_y)-ranks_freq.min())
                elif hist_type.lower() == 'relfreq':
                    if np.nanmin(pdf_y) < ranks_rel_freq.min():
                        pdf_y += abs(np.nanmin(pdf_y)-ranks_rel_freq.min())
                    else:
                        pdf_y -= abs(np.nanmin(pdf_y)-ranks_rel_freq.min())
                else:
                    print("Unrecognized histogram plot type %s" % hist_type)
                    raise ValueError

                zorder += 1
                try:
                    if add_uniform:
                        beta_label = r'$\beta$(%3.2f, %3.2f)'%(params[0], params[1])
                    else:
                        beta_label = None
                    ax.plot(pdf_x, pdf_y, 'r-', linewidth=3, label=beta_label, zorder=zorder)
                except(RuntimeError):
                    if add_fitted_uniform:
                        beta_label = 'Beta(%3.2f, %3.2f)'%(params[0], params[1])
                    else:
                        beta_label = None
                    ax.plot(pdf_x, pdf_y, 'r-', linewidth=3, label=beta_label, zorder=zorder)
                # Update y limits; just in-case!
                ylim = ax.get_ylim()
                if hist_type.lower() == 'freq':
                    ax.set_ylim([ylim[0], max(ylim[-1], np.nanmax(pdf_y), ranks_freq.max())])
                elif hist_type.lower() == 'relfreq':
                    ax.set_ylim([ylim[0], max(ylim[-1], np.nanmax(pdf_y), ranks_rel_freq.max())])
                else:
                    print("Unrecognized histogram plot type %s" % hist_type)
                    raise ValueError

        # Add perfect uniform distribution
        if add_uniform:
            # get average height:
            if hist_type.lower() == 'freq':
                avg_height = np.mean(ranks_freq)
            elif hist_type.lower() == 'relfreq':
                avg_height = np.mean(ranks_rel_freq)
            else:
                print("Unrecognized histogram plot type %s" % hist_type)
                raise ValueError
            xlim = ax.get_xlim()
            zorder += 1

            try:
                if add_fitted_beta:
                    u_label = r'$\mathcal{U}$'
                else:
                    u_label = None
                ax.plot(xlim, [avg_height, avg_height], 'b--', linewidth=3, label=u_label, zorder=zorder)
            except(RuntimeError):
                if add_fitted_beta:
                    u_label = 'Uniform'
                else:
                    u_label = None
                ax.plot(xlim, [avg_height, avg_height], 'b--', linewidth=3, label=u_label, zorder=zorder)

        # Add legend
        if add_fitted_beta or add_uniform:
            if beta_label is u_label is None:
                pass
            else:
                ax.legend(loc='upper center', ncol=2, bbox_to_anchor=(0.5, 1.075), fancybox=True, shadow=True)

        # Draw everthing
        plt.draw()
        #
    else:
        fig_hist = None
    #
    if verbose:
        print("...done...")
        #
    return ranks_freq, ranks_rel_freq, bins_bounds , fig_hist
    #

