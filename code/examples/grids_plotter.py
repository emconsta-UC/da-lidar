#! /usr/bin/env python

"""
    Plot HyPar model grid and Doppler Lidar observational grid.
"""

SHOW_PLOTS = True


import os
import sys
import shutil

from beautify_plots import plots_enhancer


# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

import numpy as np

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

# import forward operator class:
from forward_model import ForwardOperator

plots_enhancer()


def plot_dl(ax, dl_cntr=None, base_rad_ratio=0.07, height_ratio=0.1, color='red', alpha=0.8):
    """
    given an axis (ax) plot a
    """
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    zmin, zmax = ax.get_zlim()
    
    if dl_cntr is None:
        dl_cntr = ( (xmin+xmax)/2.0 , (ymin+ymax)/2.0 , zmin)

    # radius and height
    rad = min( ymax-ymin, xmax-xmin ) * base_rad_ratio
    height = (zmax-zmin) * height_ratio

    # plot DL:
    R = np.array([[dl_cntr[0]-rad, dl_cntr[0]+rad], [dl_cntr[0]-rad, dl_cntr[0]+rad]])
    Q = np.array([[dl_cntr[1]-rad, dl_cntr[1]+rad], [dl_cntr[1]-rad, dl_cntr[1]+rad]])
    H = np.array([[dl_cntr[2], dl_cntr[2]+height], [dl_cntr[2], dl_cntr[2]+height]])

    # construct meshgrids and plot the 6 faces, and the 8 corners:
    ax.plot_surface(R, dl_cntr[1]-rad, H.T, alpha=alpha, color=color)
    ax.plot_surface(R, dl_cntr[1]+rad, H.T, alpha=alpha, color=color)
    #
    ax.plot_surface(dl_cntr[0]-rad, Q, H.T, alpha=alpha, color=color)
    ax.plot_surface(dl_cntr[0]+rad, Q, H.T, alpha=alpha, color=color)
    #
    try:
        zvals = np.empty_like(R)
        zvals[...] = dl_cntr[2]
        ax.plot_surface(R, Q.T, zvals, alpha=alpha, color=color)
        zvals[...] = dl_cntr[2]+height
        ax.plot_surface(R, Q.T, zvals, alpha=alpha, color=color)
    except:
        print("Failed to plot lower and upper faces of the Dl")
        print(dl_cntr[2])
        raise

    return ax


if __name__ == '__main__':
    # Forward Model settings (passed to HyPar)  # model distance in KM
    model_configs = {'size': [51, 51, 31],  # grid settings; Nx, Ny, Nz
                     'domain_upper_bounds': [25000, 25000, 5000],  # grid settings; upper limits of the doman; lower limits are set to 0
                     'iproc': [2, 2, 2],  # number of process in each direction
                     'dt': 0.2,  # time stepsize (in seconds) that is preserved by the Hypar/PETSc time integrator
                     'PETSc_options':'-ts_type rk -ts_rk_type 4',  # you don't have to put -use-petscts in the beginning
                     'init_time':0.0,  # (optional) time of the initial state (default is zero)
                     'ghost':3,
                     'rest_iter':0,
                     'ts':'rk',
                     'ts_type':'ssprk3',
                     'hyp_scheme':'weno5',
                     'hyp_flux_split':'no',
                     'hyp_int_type':'components',
                     'par_type':'none',
                     'par_scheme':'none'
                     }
    # Observations Settings (passed to DL_obs):  # Create very sparse one!  # observation distances in meters
    obs_configs = {'site_facility': 'sgpdlE32',
                   't': 0,
                   'dl_coordinates': '0,0,0',
                   'prog_var': 'wind-velocity',
                   'num_gates': 150,
                   'range_gate_length': 30,  # 30 meters
                   'elevations': [60, 90],
                   'azimuthes': [0, 90, 180, 270]
                   }

    dist_unit = 'M'
    # create forward operator (dynamical model + observations):
    model = ForwardOperator(model_configs, obs_configs, dist_unit=dist_unit)

    # Create Reference state, and observation:
    state = model.current_model_state()
    obs = model.evaluate_theoretical_observation(state)
    obs_resh = obs.get_numpy_array()
    obs_resh = obs_resh.reshape(obs_resh.size//3, 3)
    u_obs = obs_resh[:, 0]
    v_obs = obs_resh[:, 1]
    w_obs = obs_resh[:, 2]

    # get (sparsified) velocity field
    mask_size = 9
    x, y, z, u, v, w = model._dynamical_model.velocity_field_gridded_to_numpy(state=state, mask_size=mask_size)


    # some color properties:
    color = 'red'
    alpha = 0.8
    # Plot velocity field, and doppler lidar instrument:
    fig = plt.figure(figsize=(10,10), facecolor='white')
    ax = fig.gca(projection='3d')

    # plot velocity field
    ax.quiver(x,y,z,u,v,w,length=80, arrow_length_ratio=0.35)
    
    xmin, xmax = 0, model_configs['domain_upper_bounds'][0]
    ymin, ymax = 0, model_configs['domain_upper_bounds'][1]
    zmin, zmax = 0, model_configs['domain_upper_bounds'][2]
    
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.set_zlim(zmin, zmax)
    print("XLim", ax.get_xlim())

    # add DL instrument:
    plot_dl(ax, dl_cntr=(xmax/2, ymax/2, zmin), color='m')

    # Plot Observational Gridpoints:
    obs_grid = model.observation_grid()
    ax.scatter3D(obs_grid[:, 0], obs_grid[:, 1], obs_grid[:, 2], s=2, c=color, alpha=alpha)

    # set limits and labels:
    ax.set_xlabel(r'$X$ (%s)' % dist_unit)
    ax.set_ylabel(r'$Y$ (%s)' % dist_unit)
    ax.set_zlabel(r'$Z$ (%s)' % dist_unit)
    ax.text2D(0.05, 0.95, r"Velocity Field $(u,\, v,\, w)$", transform=ax.transAxes)

    xlim = ax.get_xlim()
    ylim = ax.get_xlim()
    zlim = ax.get_xlim()

    results_dir = "_GRID_PLOTS"
    if not os.path.isdir(results_dir):
        os.makedirs(results_dir)
    print("Results will be saved to: %s" % results_dir)

    file_name = "model_with_obs.pdf"
    file_name = os.path.join(results_dir, file_name)
    plt.savefig(file_name, dpi=500, facecolor='w', format='pdf', transparent=True, bbox_inches='tight')

    unilims = [np.asarray([xmin, ymin, zmin]).min(), np.asarray([xmax, ymax, zmax]).max()]
    ax.set_xlim(unilims)
    ax.set_ylim(unilims)
    ax.set_zlim(unilims)

    file_name = "model_with_obs_unif_units.pdf"
    file_name = os.path.join(results_dir, file_name)
    plt.savefig(file_name, dpi=500, facecolor='w', format='pdf', transparent=True, bbox_inches='tight')


    # Plot model gridpoints only:
    fig = plt.figure(figsize=(10,10), facecolor='white')
    ax = fig.gca(projection='3d')
    ax.scatter3D(x, y, z, s=2, c='blue', alpha=alpha)
    # set limits and labels:
    ax.set_xlabel(r'$X$ (%s)' % dist_unit)
    ax.set_ylabel(r'$Y$ (%s)' % dist_unit)
    ax.set_zlabel(r'$Z$ (%s)' % dist_unit)

    file_name = "model_grid.pdf"
    file_name = os.path.join(results_dir, file_name)
    plt.savefig(file_name, dpi=500, facecolor='w', format='pdf', transparent=True, bbox_inches='tight')

    ax.quiver(x,y,z,u,v,w,length=80, arrow_length_ratio=0.15)
    ax.text2D(0.05, 0.95, r"Velocity Field $(u,\, v,\, w)$", transform=ax.transAxes)

    file_name = "model_grid_with_velocity.pdf"
    file_name = os.path.join(results_dir, file_name)
    plt.savefig(file_name, dpi=500, facecolor='w', format='pdf', transparent=True, bbox_inches='tight')

    ax.set_xlim(unilims)
    ax.set_ylim(unilims)
    ax.set_zlim(unilims)

    file_name = "model_grid_with_velocity_unif_units.pdf"
    file_name = os.path.join(results_dir, file_name)
    plt.savefig(file_name, dpi=500, facecolor='w', format='pdf', transparent=True, bbox_inches='tight')


    # observation grid only:
    # ========================
    fig = plt.figure(figsize=(10,10), facecolor='white')
    ax = fig.gca(projection='3d')
    ax.scatter3D(obs_grid[:, 0], obs_grid[:, 1], obs_grid[:, 2], s=2, c=color, alpha=alpha)
    plot_dl(ax, dl_cntr=(xmax/2, ymax/2, zmin), base_rad_ratio=0.06, height_ratio=0.08, color='m')
    ax.set_xlabel(r'$X$ (%s)' % dist_unit)
    ax.set_ylabel(r'$Y$ (%s)' % dist_unit)
    ax.set_zlabel(r'$Z$ (%s)' % dist_unit)
    
    file_name = "obs_grid.pdf"
    file_name = os.path.join(results_dir, file_name)
    plt.savefig(file_name, dpi=500, facecolor='w', format='pdf', transparent=True, bbox_inches='tight')
    plt.show()


    ax.text2D(0.05, 0.95, r"Velocity Field $(u,\, v,\, w)$", transform=ax.transAxes)
    ax.quiver(obs_grid[:, 0], obs_grid[:, 1], obs_grid[:, 2], u_obs, v_obs, w_obs, length=35, arrow_length_ratio=0.15)
    file_name = "obs_grid_with_velocity.pdf"
    file_name = os.path.join(results_dir, file_name)
    plt.savefig(file_name, dpi=500, facecolor='w', format='pdf', transparent=True, bbox_inches='tight')

    if SHOW_PLOTS:
        plt.show()

    plt.close('all')
