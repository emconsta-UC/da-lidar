import os
import sys 

import numpy as np

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

# import forward operator class:
import DLidarVec

import mpi4py
mpi4py.rc.recv_mprobe = False  # Turn off matched probe (on Bebop, matched probe seems to not work properly)
from mpi4py import MPI 

import time


comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size
name = MPI.Get_processor_name()

if rank == 0:
    shared = np.random.rand(10)
else:
    shared = None

if rank == 0:
    sepp = "\n" + ("*"*60) + "\n"
    print("%sTest MPI Send/Receive:%s\n\t Number of processes: %d\n %s" % (sepp, sepp, size, sepp))

comm.Barrier()
if rank == 0:
    data = shared
    for tag in xrange(1, size):
        comm.send(data, dest=tag, tag=tag)
    print( 'From rank',name,'we sent',data)
else:
    data = comm.recv(source=0, tag=rank)
    print( 'on node',name, 'we received:',data)
sys.stdout.flush()
comm.Barrier()

if rank == 0:
    print("%sAll processes terminated successfully; \n ...DONE...%s" % (sepp, sepp) )

