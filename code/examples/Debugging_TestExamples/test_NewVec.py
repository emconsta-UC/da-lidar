#! /usr/bin/env python


import os
import sys

import numpy as np

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

import DL_utility as utility

from DLidarVec import StateVector


if __name__ == '__main__':

    x = StateVector(size=3)
    print("x.size: ", x.size)
    for i in xrange(x.size):
        x[i] = i
        print("Setting x[%d]: %d" % (i, x[i]))
        print("Updated x:", x)

    print("x: ", x)
    x.write_to_file('MMM.x')
    x += 2

    print("x: ", x)

    y = StateVector(data_file='MMM.x')
    print("y:", y)
