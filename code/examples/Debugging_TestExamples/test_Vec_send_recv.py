import os
import sys 

import numpy as np

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

# import forward operator class:
import DLidarVec

import mpi4py
# print("mpi4py.rc.recv_mprobe", mpi4py.rc.recv_mprobe)
mpi4py.rc.recv_mprobe = False  # Turn off matched probe (on Bebop, matched probe seems to not work properly)
from mpi4py import MPI 


comm = MPI.COMM_WORLD
comm_size = comm.size
my_rank = comm.rank

print("Testing DLiDAR-Vec communication. Node [%d/%d]" % (my_rank, comm_size))

if my_rank == 0:
    print("1- Testing Broadcasting")

if my_rank == 0:
    state = DLidarVec.StateVector(10)
    state[:] = 3
else:
    state = None
state = comm.bcast(state, root=0)

print("Node [%d/%d], received state: %s " % (my_rank, comm_size, str(state)))


if my_rank == 0:
    print("1- Testing Send/Recv")

if my_rank == 0:
    rec_state = DLidarVec.StateVector(15)
    rec_state[:] = 0
else:
    rec_state = None

comm.Barrier()

if my_rank == 0:
    for node_rank in xrange(1, comm_size):
        data = rec_state.copy()
        data[:] = 10*node_rank
        print(">> ROOT >> Sending state to node %d with tag %d" % (node_rank, node_rank))
        comm.send(data, dest=node_rank, tag=node_rank)
        print(">sent>")
else:
    print("<< NODE [%d] << Receiving state with tag %d" % (my_rank, my_rank))
    data = comm.recv(source=0, tag=my_rank)
    print("<received<")

comm.Barrier()
    
if my_rank == 0:
    print("DONE Testing. Bcasting, and Send/Recv work just fine. Terminating...")

