#! /usr/bin/env python

"""
    Parse DL data from the incoming data directory
"""

import sys
import os

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()
#

# Import DL_DATA_HANDLER and generate an object (instances)
from DL_data_handler import DL_DATA_HANDLER
dl_obj = DL_DATA_HANDLER()


# This tries parsing everything in 'Incoming' directory (recursively)
dist_list, parsed_list = dl_obj.process_incoming_raw_data(incoming_path=None,
                                                          recursive=True,
                                                          parse=True,
                                                          keep_incoming=True,
                                                          keep_extracted_raw=True,
                                                          overwrite_raw=False,
                                                          raw_format='hpl',
                                                          parse_format='hdf',
                                                          overwrite_parsed=False,
                                                          extract_dir_warn=True)

with open('handled_files.txt', 'w') as f:
    f.write("  Distributed List of files:\n %s \n" %('='*30))
    for i, fname in enumerate(dist_list):
        f.write("%04d - %s \n" % (i, fname))

    f.write("%s  Parsed List of files:\n %s \n" %('\n'*3, '='*30))
    for i, fname in enumerate(parsed_list):
        f.write("%04d - %s \n" % (i, fname))
