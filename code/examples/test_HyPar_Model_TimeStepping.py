#! /usr/bin/env python

"""
    Test HyPar forward operator with differet step sizes, and monitor the CFL.
    For the settings below, the best step-size is 0.25 (s).
"""

import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

import Py_HyPar
import DL_utility as utility

import numpy as np

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt


# if __name__ == '__main__':

model_screen_output = False  # if False; log is saved to __HyPar_log.dat file in CWD

# Create a model object with DEFAULT (+ PASSED) configurations; initial solution is automatically generated and loaded in the model.
model_configs = {'size': [101, 101, 41],
                 'domain_bounds': [50000.0, 50000.0, 4000.0],  # upper limits of the doman in x,y,z directions; lower limits are set to 0
                 'PETSc_options':'-ts_type rk -ts_rk_type 4',  # you don't have to put -use-petscts in the beginning
                 'iproc': [2, 2, 2],
                 'dt': 0.025,
                 'n_iter': 10
                 }
model = Py_HyPar.HyPar_Model(model_configs=model_configs)

# time interval to propagate the model over:
start_time = '2017:06:04:07:00:00'
end_time   = '2017:06:04:07:01:00'
delta_time = '0000:00:00:00:01:00'
timespan = utility.create_timespan(start_time=start_time, end_time=end_time, delta_time=delta_time, return_string=False)
checkpoints = utility.timespan_to_scalars(timespan, time_diff_unit='s')

# Create list of different step sizes, and propagate the mnodel forward over the defined checkpoints/timespan
step_sizes = np.linspace(0.25, 5, 50).round(3)

# Propagae the attached model state; support for external states can be added later
sepp = "\n%s\n" % ("="*100)
time_interval_str = "[ %s to %s ]" % (start_time, end_time)
if model_screen_output:
    out_line = "\n%s\n\tTesting HyPar forward operator with different step sizes\n\tTime Integration over interval %s %s \n" % (sepp, time_interval_str, sepp)
else:
    out_line = "\n%s\n\tTesting HyPar forward operator with different step sizes\n\tTime Integration over interval %s \n  \
    \tScreen Output is turned off. Model output is saved in a log file%s \n" % (sepp, time_interval_str, sepp)
print(out_line)
sys.stdout.flush()

IC = model.get_current_state().copy()
final_states = []
rmses = []
l2_norms = []
for i, step_size in enumerate(step_sizes):
    print("\nTrying step size: %f " % step_size)
    sys.stdout.flush()
    # 
    model.dt = step_size
    _ , trajectory = model.integrate_state(IC, checkpoints=checkpoints, screen_output=model_screen_output)  # <-- log is saved to __HyPar_log.dat file in CWD
    # 
    if i == 0:
        rmse = 0
        most_accurate_state = trajectory[-1].copy()  # best state is assumed to be the one integrated with smallest step size (dt)
    else:
        rmse = utility.calculate_rmse(trajectory[-1], most_accurate_state)
    norm2 = trajectory[-1].norm2()
    rmses.append(rmse)
    l2_norms.append(norm2)

# print RMSE for propagated states w.r.t the final state propagated with smallest step size:
for i, dt, rmse, norm in zip(range(len(step_sizes)), step_sizes, rmses, l2_norms):
    out_line = "PyHpar: [ID: %d] [dt: %5.3f] [RMSE: %12.8g] [L2-Norm: %12.8g]" % (i, dt, rmse, norm)
    print(out_line)

if not model_screen_output:
    print("%s >> Now Check the model log file: %s << %s" % (sepp, '_Hypar_output.log' ,sepp))



