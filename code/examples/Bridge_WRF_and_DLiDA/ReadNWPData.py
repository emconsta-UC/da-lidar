
import numpy as np
from scipy.io import netcdf as Dataset
import matplotlib.pyplot as plt
import pickle
import datetime as datetime
import os
import urllib
import sys
import re


python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../../'))
try:
    import DL_utility as utility
except(ImportError):
    import dl_setup
    dl_setup.main()
    import DL_utility as utility


_DATA_FILE="arm_d03_raw.pcl"
__FIG_FORMAT = 'pdf'


def get_WRF_data(datafile=None, file_url=None, verbose=True):
    """
    Read (retrieve from URL if necessary) the WRF data file;
        the strategy is to be discussed

    Args:
        datafile:
        file_url:
        verbose:

    Returns:
        Times
        timespan
        DLat
        DLon
        HeightLevels
        NDims
        TerrainHeight
        U
        V
        W
        T
        RHO
        TSkin

    Remarks:
        Main information in the file include:
            - Times: string representation of time instances
            - TerrainHeight (Ny, Nx): the topography of the domain
            - TSkin (Nt, Ny, Nx): Tempreature at the ground level (zero z), over time
            - T (Nt, Nz, Ny, Nx): Temperature above (z>0-level) ground, over time
            - HeightLevels (Nt, Nz, Ny, Nx): Z levels (above ground), they change over time (slightly)
            - DLat (Ny, Nx): Latitude coordiantes (of extracted informatioin
            - DLon (Ny, Nx): Longitude coordiantes (of extracted informatioin
            -

    """
    if datafile is None:
        datafile = _DATA_FILE
    # Get the file from MCS
    if not os.path.isfile(datafile):
        print("Failed to load WRF data from file: %s " % datafile)
        if file_url is None:
            file_link = "https://www.mcs.anl.gov/~emconsta/NWP_revolving/" + datafile
        else:
            file_link = file_url + datafile
        print('Retriving file from: %s ' % file_link),
        try:
            if python_version >= (3, 0, 0):
                urllib.request.urlretrieve(file_link, datafile)
            else:
                urllib.urlretrieve(file_link, datafile)

        except:
            print("\nFailed to retrieve the data file from the given URL\nSee full message below...\n\n")
            raise
        else:
            print("**Done**")

    else:
        print('Data file found; Loading WRF data')

    with open(datafile, 'rb') as fp:
        # Get Coordinates:
        NT  = pickle.load(fp)
        NX  = pickle.load(fp)
        NY  = pickle.load(fp)
        NZ  = pickle.load(fp)
        Times         = pickle.load(fp)
        _ = pickle.load(fp)
        _ = pickle.load(fp)
        HGT = pickle.load(fp)
        DLat          = pickle.load(fp)
        DLon          = pickle.load(fp)
        TerrainHeight = pickle.load(fp)
        HeightLevels  = pickle.load(fp)
        NDims         = list([HeightLevels.shape])
        NDims.reverse()
        NDims         = tuple(NDims)  # Nx, Ny, Nz, Nt

        # Get wind field:
        U =pickle.load(fp)
        V =pickle.load(fp)
        W = pickle.load(fp)

        # Ambient
        T     =pickle.load(fp)
        TSkin = pickle.load(fp)
        RHO   =pickle.load(fp)
        PRESS = pickle.load(fp)

        Energy = 2.5*PRESS
        Energy += 0.5*RHO*((U**2)+(V**2)+(W**2))

        # Time settings
        # TODO: Use to create a timespan (iterable) object using DL_utility stuff
        start_time = ""
        for zi in Times[0]:
            start_time += zi.decode()

        time_increment = ""
        for zi in Times[1]:
            time_increment += zi.decode()

        end_time = ""
        for zi in Times[-1]:
            end_time += zi.decode()

        timespan = []
        for t in Times:
            v = ""
            for ti in t:
                v += ti.decode()
            v = v.replace('-', ':').replace('_', ':').replace(' ', ':')
            timespan.append(v)

        if verbose:
            # print file information
            print('Shape of Lat, Lon {:}'.format(DLat.shape))
            print(' - distance for each grid point (horizontal): 1Km x 1Km')
            print('Shape of 4D fields U,V,T {:} organized as t,z,y,x'.format(U.shape))
            print('Height above ground (t,y,x): {:}; max: {:} - min {:}'.format(HeightLevels.shape,np.max(HeightLevels),np.min(HeightLevels)))
            #
            print('Start time    : {:} UTC'.format(start_time))
            print('Time increpent: {:} UTC'.format(time_increment))
            print('End time      : {:} UTC'.format(end_time))

    return Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, U, V, W, T, RHO, TSkin, Energy

def shift_WRF_coordinates(WRF_Lat, WRF_Lon, WRF_Alt, WRF_TerrainHeights, target_lat, target_lon, target_alt, add_terrain_heights=True, projection='ellipse', apply_rotation=False,
                          verbose=False):
    """
    Given WRF grid coordinates (WRF_Lat, WRF_Lon, WRF_Alt), and the TerrainHeights,
    the WRF grid is transformed into cartesian coordinates centered around the target
    coordiinates;
    the WRF_altitudes are above ground levels, and the target altitude is above mean sea level

    Remarks:
        - WRF_Lat is of dimension Ny x Nx
        - WRF_Lon is of dimension Ny x Nx
        - WRF_Alt is of dimension:
            either (Ny x Nx), or (Nz x Ny x Nx);
            the former if one vertical level is considered, and the latter for full 3d grid
            here WRF_Alt is the altitude above ground
            (This is an assumption which needs to be verified);
            this is turned off by 'add_terrain_heights'
        * The entries in the returned tuple are all of the same shape,
          with replicated coordinates if necessary  (e.g., if WRF_Alt is 3D)

    """
    if verbose:
        print("Adjusting WRF Globbal Coordinates to DLiDA cartesian coordinates...")

    # Convert Spherical/Geodetic to Cartesian coordinates
    # Here we use fixed altitude for all latitude altitude points (reference ellipsoide) of the earth, then we add elevation over MSL
    target_x, target_y, _ = utility.geodetic_to_cartesian(target_lat, target_lon, 0, projection=projection)
    target_z = target_alt
    WRF_X_grid, WRF_Y_grid, _ = utility.geodetic_to_cartesian(WRF_Lat, WRF_Lon, np.zeros_like(WRF_Alt), projection=projection)
    WRF_Z_grid = WRF_Alt

    # update altitudes (We need heights above ground levels (altitude above MSL + Terrain Heights)
    if WRF_TerrainHeights is not None and  add_terrain_heights:
        #
        if WRF_TerrainHeights.shape == WRF_Z_grid.shape:
            WRF_Z_grid += WRF_TerrainHeights
        elif np.ndim(WRF_TerrainHeights) == np.ndim(WRF_Z_grid) -1:
            assert WRF_Z_grid.shape[1: ] == WRF_TerrainHeights.shape, "Terrain heights must be one dimensions less than WRF_Alt"
            for k in range(np.size(WRF_Z_grid, 0)):
                WRF_Z_grid[k, ...] += WRF_TerrainHeights
        else:
            print("Terrain heights must be equal to or one dimensions less than WRF_Alt")
            raise ValueError
    else:
        # Don't add terrain heights
        pass
        # MSL_WRF_Alt = WRF_Alt

    # Replicate X, Y if needed
    if WRF_Z_grid.ndim == WRF_X_grid.ndim == WRF_Y_grid.ndim:
        pass
    elif 3 == WRF_Z_grid.ndim == WRF_X_grid.ndim+1 and WRF_X_grid.ndim == WRF_Y_grid.ndim:
        # replicate X, Y
        WRF_X_grid = np.repeat(WRF_X_grid[np.newaxis, ...], WRF_Z_grid.shape[0], axis=0)
        WRF_Y_grid = np.repeat(WRF_Y_grid[np.newaxis, ...], WRF_Z_grid.shape[0], axis=0)
        pass
    else:
        print("Unexpected shapes")
        print("WRF_X_grid.shape: %s" %WRF_X_grid.shape)
        print("WRF_Y_grid.shape: %s" %WRF_Y_grid.shape)
        print("WRF_Z_grid.shape: %s" %WRF_Z_grid.shape)
        raise ValueError

    # Shift coordinates to the new origin (Everything in cartesian coordinates;
    # Shift WRF Cartesian grid to be centered around the DL coordiantes
    WRF_X_grid -= target_x
    WRF_Y_grid -= target_y
    WRF_Z_grid -= target_z

    #
    #
    if False:  # TODO: remove after debugging
        print("WRF_X_grid", WRF_X_grid)
        print("WRF_Y_grid", WRF_Y_grid)
        print("WRF_Z_grid", WRF_Z_grid)
        print("target coordinates: ", target_x, target_y, target_z)

    return WRF_X_grid, WRF_Y_grid, WRF_Z_grid


def test_data_reader():
    """
    1- Load the WRF data file contents
    2- Locate the two DL machines (SGP-E32, and SGP-C1) location on the WRF grid
    3- Plot the ground Temperature (TSKin) in Celsius
        at the second time instance; this is fixed here just for testing
    4- Save the plot, and terminate
    """
    # Load data from file
    Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, \
        U, V, W, T, RHO, TSkin, Energy = get_WRF_data()

    # Plot ARM Sites (RedDots)
    site = 'SGP'
    facilities = ('E32', 'C1')
    SlrStat = [utility.get_ARM_DL_coordinates(site, facility, format='degrees')[0] for facility in facilities]

    # Plot devices (red dots), give longitude on x-axis, and latitude on y-axis
    for i, facility in enumerate(facilities):
        latitude, longitude = SlrStat[i]
        plt.plot(longitude, latitude, 'ro')
        plt.text(longitude+0.01, latitude+0.01, '-'.join([site, facility]), color="red")

    # NWP domain: Skin Temperature (at zero z-leve, i.e. ground)
    # Boundary
    plt.plot(DLon[0, : ], DLat[0, : ], '--', color='b')
    plt.plot(DLon[-1, :], DLat[-1, :], '--', color='b')
    plt.plot(DLon[:, -1], DLat[:, -1], '--', color='b')
    plt.plot(DLon[:, 0 ], DLat[:, 0 ], '--', color='b')
    # Contour of Skin temperature (ground-temp)
    plt.contourf(DLon, DLat, np.squeeze(TSkin[1, :, :]-273.15))  # Kelvin to Celsius
    plt.colorbar()
    plt.title('Skin temperature')
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    filename = "WRF_TSkin_time_%s.%s" % (timespan[0], __FIG_FORMAT)
    filename = os.path.abspath(filename)
    plt.savefig(filename, dpi=500, format=__FIG_FORMAT, facecolor='w', transparent=True, bbox_inches='tight')
    print("Saved TSkin to: %s" % filename)
    plt.close('all')
    #
    # You can add more testing stuff here...
    #


if __name__ == '__main__':
    test_data_reader()

