
"""
    This is a very Simple scrip to test the Filtering_Process class that iterates over a filter, e.g. EnKF with HyPar model.
    If MPI is initiated, model, fitler, and process are all initiated on all nodes in the communicator

    This will replace test_Filtering_Process.py

"""

import os
import sys
import getopt

python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    input = raw_input
    range = xrange

import pickle

import numpy as np

try:
    import ConfigParser
except:
    import configparser as ConfigParser

try:
    import mpi4py
    mpi4py.rc.recv_mprobe = False
    from mpi4py import MPI
    use_MPI = True
except(ImportError):
    use_MPI = False

# Initialize DATeS with default settings
if use_MPI:
    comm = MPI.COMM_WORLD
    my_rank = comm.rank
    comm_size = comm.size
else:
    comm = None
    my_rank = 0
    comm_size = 1

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../../'))
import dl_setup
dl_setup.main()


import DL_utility as utility
import DLidarVec

# import forward operator class:
import forward_model
from forward_model import ForwardOperator
from EnKF import DEnKF as KalmanFilter

from iterative_filtering_process import IterativeFilteringProcess

# Get functionalities for reading WRF Data (From Emil's code)
import ReadNWPData  # a module that creates a forward operator with standard settings


__STANDARD_ENSEMBLE_SIZE =  50
__STANDARD_RESULTS_DIR = "Results/Filtering_Results"
__INITIL_ENSEMBLE_REPO = "Initial_Ensemble_Repo"
__DEFAULT_INFLATION = (1.025,  1.)

__VERBOSE = False


def create_forward_operator(output_dir_structure_file, dl_stare_only=True):
    """
    Attempt to create model from existing configurations; if not, recreate, and save error information
    """
    if os.path.isfile(output_dir_structure_file):
        model = recreate_model(output_dir_structure_file)
    else:
        print("Not valid file: %s " % output_dir_structure_file)
        model =  None
    #
    if model is None:
        model_configs = forward_model.standard_model_configs()
        model_configs.update({'domain_upper_bounds':[50000, 50000, 4000],
                              'size':[101, 101, 41],
                              'iproc':[5, 5, 2],
                              'dt':0.02
                             })
        if not use_wrf_data:
            # In this case, we should  turn-off the update of ground temperature (from TSKin)
            boundary = model_configs['boundary']
            faces_types = boundary['faces_types']
            faces_settings = boundary['faces_settings']
            faces_types[-2] = 'noslip-wall'
            faces_settings[-2] = (0.0, 0.0, 0.0)
            boundary.update({'faces_types':faces_types,
                             'faces_settings':faces_settings})
            model_configs.update({'boundary':boundary})

        obs_configs = forward_model.standard_observation_configs(site='sgp',
                                                                  facility='C1',
                                                                  prognostic_variable='radial-velocity',
                                                                  num_gates=120,
                                                                  stare_only=dl_stare_only)

        # Forward Operator: Dynamics+Observations; using HyPar, and DL-Data settings
        # print("My Rank: %d; Creating Model" % my_rank)
        try:
            # TODO: this is in DEV mode. Once finalized, this should be updated
            model = ForwardOperator(model_configs, obs_configs, verbose=verbose, space_dependent_obs_err=False)
        except:
            model = ForwardOperator(model_configs, obs_configs, verbose=verbose)

    return model

def recreate_model(output_dir_structure_file):
    """
    Lookup results directory, and recreate the forward operator
    """
    output_dir_strucutre = utility.get_output_dir_structure(output_dir_structure_file, full_paths=True)
    file_output_dir  = output_dir_strucutre['file_output_dir']
    model_states_dir = output_dir_strucutre['model_states_dir']
    observations_dir = output_dir_strucutre['observations_dir']
    statistics_dir   = output_dir_strucutre['statistics_dir']
    cycle_prefix     = output_dir_strucutre['cycle_prefix']

    # Lookup observation error model data
    obs_err_file = os.path.join(file_output_dir, "Observation_Error_Model.hpy")
    if os.path.isfile(obs_err_file):
        # get model and observation configs, and create a forward model instance:
        model_configs_file = os.path.join(file_output_dir, 'setup.dat')
        model_configs, obs_configs = forward_model.read_model_configs(model_configs_file)
        model = ForwardOperator(model_configs, obs_configs, dist_unit='M', obs_err_model_file=obs_err_file)
    else:
        print("Couldn't find Observation Error Model file: %s " % obs_err_file)
        model = None
    return model


def inspect_initial_ensemble(model, directory, ensemble_size):
    """
    Look for proper states under the passed directory, and return True if enough proper states exist, otherwise return False
    """
    if not os.path.isdir(directory):
        proper_exists = False
    else:
        proper_exists = False

        checked = 0
        files = utility.get_list_of_files(directory, recursive=False, extension='dlvec')
        for fle in files:
            try:
                st = DLidarVec.state_vector_from_file(fle)
            except:
                continue
            if st.size == model.state_size():
                checked += 1
            #
            if checked == ensemble_size:
                break
        if checked >= ensemble_size:
            proper_exists = True
        else:
            proper_exists = False
    return proper_exists

def generate_ensemble(model, output_dir, ensemble_size, initial_state=None, inflate=False, inflation_factor=None, time=0, files_prefix="HyPar_Model_State"):
    """
    Given the model, create a long trajectory of model states, optionally inflate it, and save it to output_dir
    """
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if initial_state is None:
        initial_state = model.current_model_state()
        initial_state += 1e-5

    # Generate trajectory of model states; each time, calculate the mean, on the fly, and generate a noise vector (if requested) and calculate it's average
    x0 = initial_state
    t0 = x0.time
    dt = 10.0  # 10 seconds trajectory
    tspan = [t0 + (i)*dt for i in range(ensemble_size+1)]

    ens_mean = None
    gen = 0
    written = []
    print("Generating initial ensemble...")
    for t1, t2 in zip(tspan[: -1], tspan[1: ]):
        checkpoints = [t1, t2]
        _, traject = model.integrate_state(x0, checkpoints, model_screen_output=False)
        x0 = traject[-1]
        x0.time = time
        if inflate:
            gen += 1
        if ens_mean is None:
            ens_mean = x0.copy()
        elif inflate:
            ens_mean.add(x0, in_place=True)
        #
        filename = utility.try_file_name(directory=output_dir, file_prefix=files_prefix, extension='dlvec', return_abspath=True)
        msg = "\rSaving state [ %04d / %04d ] to: %s " % (gen, ensemble_size, filename)
        sys.stdout.write(msg)
        sys.stdout.flush()
        #
        x0.write_to_file(filename)
        if inflate:
            written.append(filename)
    print("\n")
    #
    if inflate:
        if inflation_factor is not None:
            infl_vec = model.create_inflation_vector(inflation_factor)
        else:
            infl_vec = model.create_inflation_vector()

        print("\nInflating Generated states")
        mark = ['|', '/', '--', '\\']
        ens_mean *= (1.0 / gen)
        for gen, filename in enumerate(written):
            x0 = DLidarVec.state_vector_from_file(filename)
            diff = x0 - ens_mean
            diff.multiply(infl_vec, in_place=True)
            x0 += diff
            x0.write_to_file(filename)
            msg = "\r --> %04d / %04d  %s" % (gen+1, ensemble_size, mark[gen%len(mark)])
            sys.stdout.write(msg)
            sys.stdout.flush()
        print("\ndone...")


def get_args(input_args, output_dir=None,
             init_ens_repo=None,
             dl_stare_only=True,
             use_wrf_data=True,
             wrf_data_file=None,
             ensemble_size=None,
             inflation_factors=None):
    """
    Get command line arguments; default values passed are used, if not entered in the initiating command
    """
    try:
        opts, args = getopt.getopt(input_args,
                                   "hd:s:i:u:w:e:l:",
                                   ["help",
                                    "output-dir=",
                                    "stare-only=",
                                    "init-ensemble=",
                                    "use-wrf-data=",
                                    "wrf-data=",
                                    "ensemble-size=",
                                    "inflation-factors="
                                   ]
                                  )
    except getopt.GetoptError:
        print('python/run filtering_with_WRF_TSkin.py -d <results output dir> -s <stare-only flag> -i <initial ensemble folder> -u <use-wrf-data flag> -w <wrf data file> -e <ensemble size> -l <inflation factors>')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print('python/run filtering_with_WRF_TSkin.py -d <results output dir> -s <stare-only flag> -i <initial ensemble folder> -u <use-wrf-data flag> -w <wrf data file> -e <ensemble size> -l <inflation-factors>')
            sys.exit()
        elif opt in ("-d", "--output-dir"):
            output_dir = arg
        elif opt in ("-s", "--stare-only"):
            dl_stare_only = utility.collection.str2bool(arg)
        elif opt in ("-i", "--init-ensemble"):
            init_ens_repo = arg
        elif opt in ("-u", "--use-wrf-data"):
            use_wrf_data = utility.collection.str2bool(arg)
        elif opt in ("-w", "--wrf-data"):
            wrf_data_file = arg
        elif opt in ("-e", "--ensemble-size"):
            try:
                ensemble_size = int(arg)
            except(ValueError):
                print("Invalid ensemble size %s\n" % arg)
                raise
            if ensemble_size < 1:
                print("Invalid ensemble size (< 1)")
                sys.exit(2)
            elif ensemble_size == 1:
                print("WARNING: ensemble size of size 1 won't be useful for filtering!")
            else:
                pass
        elif opt in ("-l", "--inflation-factors"):
            inflation_factors = eval(arg)

    # overwrite None values
    if ensemble_size is None:
        ensemble_size = __STANDARD_ENSEMBLE_SIZE

    if inflation_factors is None:
        inflation_factors = __DEFAULT_INFLATION

    if output_dir is None:
        output_dir = __STANDARD_RESULTS_DIR
        if use_wrf_data:
            output_dir + "_WRF_TSKin"
        if dl_stare_only:
            output_dir  += "_Stare"
        else:
            output_dir  += "_StareVad"
        output_dir = "%s_INFL_%f_%f" % (output_dir, inflation_factors[0], inflation_factors[1])
        output_dir = "%s_Ens_%d" % (output_dir, ensemble_size)

    if init_ens_repo is None:
        init_ens_repo = __INITIL_ENSEMBLE_REPO

    # All paths to abs form...
    output_dir = os.path.abspath(output_dir)
    init_ens_repo = os.path.abspath(init_ens_repo)
    if wrf_data_file is not None:
        wrf_data_file = os.path.abspath(wrf_data_file)
    #
    return output_dir, dl_stare_only, init_ens_repo, use_wrf_data, wrf_data_file, ensemble_size, inflation_factors


if __name__ == '__main__':

    verbose = __VERBOSE

    # get results directory (if passed as argument)
    results_dir, dl_stare_only, init_ensemble_repo, use_wrf_data, \
            wrf_data_file, filter_ensemble_size, filter_inflation_factors = get_args(sys.argv[1: ])

    # ====================================================================================== #
    #        Create Forward Operget_list_of_diroget_list_of_dirsator: Dynamical model + Observation handler/operator         #
    # ====================================================================================== #
    results_dir = os.path.abspath(results_dir)
    print("***\nResults will be saved to: \n %s \n***" % results_dir)
    #
    output_dir_structure_file = os.path.join(results_dir, "output_dir_structure.txt")
    model = create_forward_operator(output_dir_structure_file, dl_stare_only=dl_stare_only)
    dynamical_model = model.dynamical_model
    # print("Model Initialized")
    # sys.stdout.flush()
    #
    # ====================================================================================== #

    # Inpect Initial Ensemble and generate if no proper exist...
    if my_rank == 0:
        generate_initial_ensemble = not inspect_initial_ensemble(model, init_ensemble_repo, filter_ensemble_size)
    else:
        generate_initial_ensemble = None
    if comm_size > 1:
        generate_initial_ensemble = comm.bcast(generate_initial_ensemble, root=0)

    if my_rank == 0 and generate_initial_ensemble:
        print("Generating initial ensemble to: %s" % init_ensemble_repo)
        generate_ensemble(model, init_ensemble_repo, filter_ensemble_size, inflate=True)

    if comm is not None and comm_size > 1:
        comm.Barrier()

    # ====================================================================================== #
    #      Read WRF data; and get information needed for calculating ground temperature      #
    # ====================================================================================== #
    #
    if my_rank == 0:
        if use_wrf_data:
            _, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, U, V, W, T, RHO, TSkin, Energy = ReadNWPData.get_WRF_data(datafile=wrf_data_file)
            print("*** TSKIN SHAPE: ", TSkin.shape)
            wrf_data_dict = dict(timespan=timespan,
                                 DLat=DLat,
                                 DLon=DLon,
                                 HeightLevels=HeightLevels,
                                 TerrainHeight=TerrainHeight,
                                 U=U,
                                 V=V,
                                 W=W,
                                 RHO=RHO,
                                 TSkin=TSkin,
                                 Energy=Energy
                                 )

            # Model and Doppler Lidar Grid information
            dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']  # DL instrument global coordinates
            model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical model grid
            model_grid = model.model_grid()
            ground_inds = np.where(model_grid[:, -1]==0)
            target_x = model_grid[ground_inds, 0].flatten()
            target_y = model_grid[ground_inds, 1].flatten()
            target_grid = (target_x, target_y)  # Only the X-Y directioins
        else:
            timespan = ReadNWPData.get_WRF_data(datafile=wrf_data_file)[1]  # we just need the timespan
    else:
        #
        timespan = None

    #
    if comm is not None:
        # broadcast timespan to all nodes. Only root does the reading, and interpolation
        timespan = comm.bcast(timespan, root=0)
    #
    # ====================================================================================== #


    # ====================================================================================== #
    #                                     Assimilation                                       #
    # ====================================================================================== #
    # GENERAL SETTINGS:
    # =================
    filter_localization_function = 'Gaspari-Cohn'
    filter_localization_radius   = 600  # loc radius im dist_unit
    #
    collect_remote_data   = False # Set only once for a give timespan,, otherwise it will take time everytimt you run the script
    use_real_observations = True # True/False  --> real/synthetic Observations
    random_seed           = 2345  # passed to the random number generator used; numpy here
    #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Filter Object/Cycle)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # use model to produce more appropriate inflation factor
    if True:
        forecast_inflation = filter_inflation_factors[0]
        analysis_inflation = filter_inflation_factors[1]
    else:
        try:
            forecast_inflation = model.create_inflation_vector(filter_inflation_factors[0])
            analysis_inflation = model.create_inflation_vector(filter_inflation_factors[1])
        except:
            forecast_inflation = filter_inflation_factors[0]
            analysis_inflation = filter_inflation_factors[1]

    # Create the filter object:
    # TODO: update inflation factors to be space dependent
    filter_configs={'model': model,
                    'MPI_COMM':comm,
                    'ensemble_size':filter_ensemble_size,
                    'localize_covariances':True,
                    'localization_radius':filter_localization_radius,
                    'localization_function':filter_localization_function,
                    'forecast_inflation_factor':forecast_inflation,
                    'analysis_inflation_factor':analysis_inflation}
    filter_output_configs = {'file_output_moment_only':False, 'verbose':verbose}
    # print("Creating KalmanFilter")
    filter_obj = KalmanFilter(filter_configs=filter_configs, output_configs=filter_output_configs)
    # print("DONE...")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Assimiltion Process)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #
    # Observation time setting: (Create proper timespan)
    assimilation_configs = {'filter':filter_obj,
                            'MPI_COMM':comm,
                            'random_seed':random_seed}
    assim_output_configs = {'scr_output':True,
                            'scr_output_iter':1,
                            'file_output':True,
                            'file_output_iter':1,
                            'file_output_dir':results_dir,
                            'verbose':verbose}

    # print("Creating IterativeFilteringProcess")
    # sys.stdout.flush()
    assim_experiment = IterativeFilteringProcess(assimilation_configs=assimilation_configs,
                                        output_configs=assim_output_configs)
    print("CREATED; proceeding with new assimilation process...")
    sys.stdout.flush()

    # Look for previous results to see where to start assimilation
    try:
        file_exists, bad_file, _, _, _, model_states_dir, _ = assim_experiment.inspect_status_file(correct_contents=True)  # this is collective...
    except:
        file_exists, bad_file, _, _, _, model_states_dir, _ = assim_experiment.inspect_status_file()  # this is collective...

    if my_rank == 0:
        #
        if file_exists and not bad_file:
            # read the configuration file 'setup.dat' inside model_states_dir, and get 'timespan' option under 'Filter Configs'
            # Get filter configs:
            filter_configparser = ConfigParser.ConfigParser()
            filter_configparser.read(os.path.join(model_states_dir, 'setup.dat'))
            section_header = 'Filter Configs'
            if not filter_configparser.has_section(section_header):
                print("Couldn't find the section header '%s' in the file!\nExperiment will restart" % section_header)
                init_index = 0
            else:
                try:
                    cycle_tspan = filter_configparser.get(section_header, 'timespan')
                    cycle_tspan = eval(cycle_tspan)
                except:
                    print("Failed to read timespan from cycle configuratioins. Experiment will reset;")
                    cycle_tspan = None
                #
                if cycle_tspan is None:
                    init_index = 0
                else:
                    # The cycle_tspan is not None; previous results exist
                    last_time = cycle_tspan[-1]
                    wrf_tspan = np.array(utility.timespan_to_scalars(timespan))
                    init_index = np.where(wrf_tspan > last_time)[0]
                    if init_index.size >= 1:
                        init_index = init_index[0]
                        if init_index == 0:
                            pass
                        elif init_index < 0:
                            init_index = np.infty
                        else:
                            init_index -= 1
                    else:
                        print("last_time: ", last_time)
                        print("wrf_tspan: ", wrf_tspan)
                        init_index = np.infty
        else:
            init_index = 0
    else:
        init_index = None
    #
    if comm is not None:
        # sync init_index
        init_index = comm.bcast(init_index, root=0)

    if np.isinf(init_index):
        print("NODE %d; Terminating; Found previous results, with no matching time!" % my_rank)
        raise ValueError
    elif init_index == (len(timespan)-1):
        print("The results for the last cycle are found already; nothing to do further over this timespan!")
        print("Terminating on NODE %d " % my_rank)
        sys.exit()
    elif init_index >= len(timespan):
        print("This is not supposed to happen; init_index exceeds wrf timespan!")
        raise ValueError
    else:
        # good to go...
        pass

    #
    # Iterate over the experiment_timespan, and apply filtering at each cycle
    for t_ind, t0 in enumerate(timespan[: -1]):
        # Time settings
        t1 = timespan[t_ind+1]
        checkpoints = [t0, t1]
        checkpoints = utility.timespan_to_scalars(checkpoints)
        window_size = checkpoints[1] - checkpoints[0]
        if my_rank == 0:
            print("Time Winds: %s --> %s" % (t0, t1))

        # skipping cycles with preexisting results
        if t_ind < init_index:
            if my_rank == 0:
                print("***Previous results found. Skipping...***")
            continue

        if my_rank == 0 and use_wrf_data:
            # retrieve WRF grid coordinates at the current time (New origin is at the DL instrument)
            WRF_X, WRF_Y, _ = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                                HeightLevels[t_ind, :, :, :],
                                                                TerrainHeight,
                                                                dl_lat, dl_lon, dl_alt,
                                                                verbose=True)

            # update WRF grid to match dynamical model grid, since instrument is centered in the XY domain
            WRF_X += model_dl_coordinates[0]
            WRF_Y += model_dl_coordinates[1]

            # Source Grid (WRF), and Source Values (WRF ground temperature
            source_grid = (WRF_X[0, ...].T, WRF_Y[0, ...].T)  # we need to do interpolation in XY domain
            source_values = TSkin[t_ind, ...].T  # trasposing because WRF data is organized as Y X

            # Carry out the interpolation (from WRF cartesian grid to HyPar grid)
            # interpolated_TSkin is the groudn temperature at XY points of HyPar grid (Z is ommitted)
            interpolated_TSkin = utility.interpolate_field(source_grid=source_grid,
                                                           target_grid=target_grid,
                                                           source_field=source_values,
                                                           method='linear')

        else:
            interpolated_TSkin = None

        # Synchronize on all nodes if needed
        if comm is not None:
            interpolated_TSkin = comm.bcast(interpolated_TSkin, root=0)

        if use_wrf_data:
            # print("Rank: %d ; interpolated_TSkin: " % my_rank, interpolated_TSkin)
            # Update model (HyPar) temperature field on file
            dynamical_model.update_ground_temperature(interpolated_TSkin)

        # if my_rank == 0:
        #     print("WRF DATA loaded on root node, interpolatiion carried out, and interpolated TSKin is broadcasted")
        # Run the assimilation process
        assim_experiment.start_assimilation_process(initial_time=t0,  # to be read from initial ensemble
                                                    window_size=window_size,  # 10 seconds
                                                    number_of_windows=1,
                                                    load_ensemble_size=filter_ensemble_size,
                                                    read_initial_ensemble_from=init_ensemble_repo,
                                                    load_ensemble_prefix='analysis_ensemble'
                                                    )

        # If wrf TSKin is used, save it to file
        if use_wrf_data:
            _, _, _, _, _, model_states_dir, _ = assim_experiment.inspect_status_file()
            if my_rank == 0:
                # Save Integrated TSKin
                filename = os.path.join(model_states_dir, "WRF_TSKin.npy")
                print("Writing WRF-based TSKin to: %s " % filename)
                # Save Interpolated TSkin
                np.save(filename, interpolated_TSkin)

    #
    # ====================================================================================== #
    #                                     <<<<DONE>>>>                                       #
    # ====================================================================================== #
    #
