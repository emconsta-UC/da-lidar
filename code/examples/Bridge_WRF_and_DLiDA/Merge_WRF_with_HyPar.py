
# This script contains all functionalities needed to integrate WRF data into HyPar model simulations
#

import sys, os
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version < (3, 0, 0): range = xrange

# Prepare paths, and DLiDA (sub)-packages:
sys.path.append(os.path.abspath('../../'))
try:
    import DL_utility as utility
except(ImportError):
    import dl_setup
    dl_setup.main()
    import DL_utility as utility

import forward_model
from forward_model import ForwardOperator
import ReadNWPData
import hypar_model_plotter
import filtering_results_reader

import re
import numpy as np
import matplotlib.pyplot as plt

import pickle
import datetime as datetime
import os.path as pathfile


# GLOBAL CONSTANTS
# WRF-HyPar
__MINIMUM_DT = 1e-5  # Minimum allowd model timestep
__VERBOSE = False
__RESULTS_and_PLOTS = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'WRF_HyPar_RESULTS')
__FIG_FORMAT = 'pdf'

# WRF-DL
__RESULTS_FILENAME = "WRF_DL_Data_results.pcl"
__PLOT_FILENAME = "WRF_DL_Data_results.png"
__SELECTIVE_GATES = range(8, 15)  # selective gates for vertical profiling

# ************ #
#    COMMON    #
# ************ #
def create_model(obs_variable='radial-velocity'):
    """
    """
    # create forward operator (dynamical model + observations):
    model = forward_model.create_standard_forward_operator(obs_variable)
    return model


# *************** #
# WRF-HyPar Stuff #
# *************** #

def WRF_data_interpolation(wrf_data, model, field='wind', t=None, interpolation_method='linear', verbose=False):
    """
    Interpolate WRF data into a hypar-dynamical model grid at a given time instance (t)
        If t is None, all time instances found in wrf_data contents are assumed
    field is either 'tskin', 'wind', or 'all'
        'tskin': ground temperature is interpolated
        'wind': interpolate U, V, W
        'all': interpolate U, V, W, Rho, E

    Returns:
        a list with each entry containing the interpolated field
        (one entry for tskin, or a tuple with multiple entries otherwise)
    """
    assert isinstance(model, ForwardOperator), "model must be an instance of forward_model.ForwardOperator"

    if isinstance(wrf_data, str):
        # Load data from file
        _, timespan, DLat, DLon, HeightLevels, _, TerrainHeight, \
            U, V, W, _, RHO, TSkin, Energy = ReadNWPData.get_WRF_data()
        scalar_times = [utility.time_to_scalar(t) for t in timespan]

    elif isinstance(wrf_data, dict):
        # Extract data from dictionary  # No need to load everything if TSkin only is needed!
        timespan      = wrf_data['timespan']
        DLat          = wrf_data['DLat']
        DLon          = wrf_data['DLon']
        HeightLevels  = wrf_data['HeightLevels']
        TerrainHeight = wrf_data['TerrainHeight']
        U             = wrf_data['U']
        V             = wrf_data['V']
        W             = wrf_data['W']
        RHO           = wrf_data['RHO']
        TSkin         = wrf_data['TSkin']
        Energy        = wrf_data['Energy']

    else:
        print("wrf_data is either a file path to load, or a dictionary with proer entries; found %s" % type(wrf_data))
        raise TypeError

    # Extract model information
    # Get DL instrument coordinates
    model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical mdel grid
    model_grid = model.model_grid()

    dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']


    # Time instances to inspect in wrf tspan
    if t is None:
        lookup_times = scalar_times
    else:
        if isinstance(t, str):
            try:
                t = t.replace('-', ':').replace('_', ':').replace(' ', ':')
            except:
                v = ""
                for ti in t: v += ti.decode()
                v = t
        lookup_times = [utility.time_to_scalar(t)]

    # For each time instance in lookup_times, find the corresponding entry in timespan,
    # and do the interpolation
    interpolated_fields = []
    for t0 in lookup_times:
        if verbose:
            print("Looking up time: %s" % t0),
        try:
            found_ind = scalar_times.index(t0)
            if verbose:
                print("Found match; loading  and interpolating")
        except(ValueError):
            # no found index; put None
            found_ind = None
            if verbose:
                print("No mach found")

        # Start interpolating data if time is found
        if found_ind is None:
            interp_value = None
        else:
            # Retrieve WRF grid coordinates at the current time
            Heights = HeightLevels[found_ind, :, :, :]
            WRF_X, WRF_Y, WRF_Z = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                                    Heights, TerrainHeight,
                                                                    dl_lat, dl_lon, dl_alt)

            # update grid to match dynamical model grid, instrument is centered in XY domain
            WRF_X += model_dl_coordinates[0]
            WRF_Y += model_dl_coordinates[1]
            WRF_Z += model_dl_coordinates[2]

            if re.match(r'\Atskin\Z', field, re.IGNORECASE):
                # Target Grid (HyPar)
                ground_inds = np.where(model_grid[:, -1]==0)
                target_grid = (model_grid[ground_inds, 0].flatten(),
                               model_grid[ground_inds, 1].flatten())  # Only the X-Y directioins

                # Source Grid (WRF), and Source Values (WRF ground temperature
                source_grid = (WRF_X[0, ...].T, WRF_Y[0, ...].T)  # interpolation in XY domain

                # NOTE: trasposing srouce filed because WRF data is organized as Z Y X

                # Carry out the interpolation (from WRF cartesian grid to HyPar grid)
                # interpolated_TSkin is the groudn temperature at XY points of HyPar grid (Z is ommitted)
                interp_value = utility.interpolate_field(source_grid=source_grid,
                                                         target_grid=target_grid,
                                                         source_field=TSkin[found_ind, ...].T,
                                                         method=interpolation_method,
                                                         verbose=verbose)

            elif re.match(r'\A(wind|all)\Z', field, re.IGNORECASE):
                # Target Grid (HyPar)
                target_grid = (model_grid[:, 0].flatten(),
                               model_grid[:, 1].flatten(),
                               model_grid[:, 2].flatten())


                # Source Grid (WRF), and Source Values (WRF ground temperature
                source_grid = (WRF_X.T, WRF_Y.T, WRF_Z.T)  # interpolation in XYZ domain

                if verbose:
                    print("WRF grid ranges:")
                    print("X: ", source_grid[0].min(), source_grid[0].max())
                    print("Y: ", source_grid[1].min(), source_grid[1].max())
                    print("Z: ", source_grid[2].min(), source_grid[2].max())

                    print("Model grid ranges:")
                    print("X: ", target_grid[0].min(), target_grid[0].max())
                    print("Y: ", target_grid[1].min(), target_grid[1].max())
                    print("Z: ", target_grid[2].min(), target_grid[2].max())

                # source filed(s); u, v, w
                interpolated_u = utility.interpolate_field(source_grid=source_grid,
                                                           target_grid=target_grid,
                                                           source_field=U[found_ind, ...].T,
                                                           method=interpolation_method,
                                                           verbose=verbose)
                interpolated_v = utility.interpolate_field(source_grid=source_grid,
                                                           target_grid=target_grid,
                                                           source_field=V[found_ind, ...].T,
                                                           method=interpolation_method,
                                                           verbose=verbose)
                interpolated_w = utility.interpolate_field(source_grid=source_grid,
                                                           target_grid=target_grid,
                                                           source_field=W[found_ind, ...].T,
                                                           method=interpolation_method,
                                                           verbose=verbose)

                if re.match(r'\Awind\Z', field, re.IGNORECASE):
                    interp_value = (interpolated_u, interpolated_v, interpolated_w)
                else:
                    interpolated_rho = utility.interpolate_field(source_grid=source_grid,
                                                                 target_grid=target_grid,
                                                                 source_field=RHO[found_ind, ...].T,
                                                                 method=interpolation_method,
                                                                 verbose=verbose)
                    interpolated_energy = utility.interpolate_field(source_grid=source_grid,
                                                                    target_grid=target_grid,
                                                                    source_field=Energy[found_ind, ...].T,
                                                                    method=interpolation_method,
                                                                    verbose=verbose)

                    interp_value = (interpolated_rho, interpolated_u, interpolated_v, interpolated_w, interpolated_energy)

            else:
                print("Unsupported field %s" % field)
                raise ValueError

        # Validate data for nan;

        # Append
        interpolated_fields.append(interp_value)

    lookup_times = utility.timespan_from_scalars(lookup_times)
    return lookup_times, interpolated_fields

def update_model_ground_temperature(dynamical_model, interpolated_TSkin):
    """
    Given the ground temperature, interpolated from WRF grid into HyPar grid,
        update the temperature file for HyPar

    Args:
        dynamical_model: an instance of HyPar dynamical model
        interpolated_TSkin: np.ndarray 2d with x-y values of the ground temperature
            (in Celsius) at the hypar model grid

    """
    R = dynamical_model.model_configs()['physics']['R']
    # Convert WRF temperature (Kalvin) into HyPar-based units (K)
    dynamical_model.update_ground_temperature(interpolated_TSkin*R)

def save_and_plot_state(state, model, slices_index=None, output_dir=None, filename_base="TSKin_Integration"):
    """
    """
    if output_dir is None:
        output_dir = __RESULTS_and_PLOTS
    dir_name = hypar_model_plotter.create_output_dir(output_dir, remove_existing=False)

    fname = os.path.join(dir_name, "%s_State"%(filename_base))
    state.write_to_file(fname)

    dynamical_model = model.dynamical_model
    if slices_index is None:
        Nx, Ny, Nz = dynamical_model.get_grid_sizes()
        slices_index = [Nx//2, Ny//2, 1]

    # Get the gridded velocity field; Note: passing x, y, z, u, v, w avoides reallocating memory for them
    x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0 = dynamical_model.get_premitive_atmos_flow_variable(state=state)

    # Plotting Velocity Field:
    # 1- Plot slices of the wind velocity components of the solution at the center
    fname = os.path.join(dir_name, "%s_%s"%(filename_base, 'Wind'))
    fig1 = hypar_model_plotter.plot_wind_slices(x, y, z, u, v, w, index=slices_index, filename="%s_slices.%s" % (fname, __FIG_FORMAT))
    plt.close(fig1)
    #
    # 2- 3D plots
    if model is not None:
        fname = os.path.join(dir_name, "%s_%s"%(filename_base, 'Wind3D'))
        filename = "%s_slices.%s" % (fname, __FIG_FORMAT)
        fig1 = filtering_results_reader.single_state_plot(model, state, index=slices_index, filename=filename, threeD=False)
        plt.close(fig1)

    if False:  # not needed!
        # Plot Theta; slices of theta, theta0, theta-theta0
        fname = os.path.join(dir_name, "%s_%s"%(filename_base, 'Theta'))
        fig2, z_grid, theta_tuple = hypar_model_plotter.plot_temperature(x, y, z, theta, theta0, index=slices_index, filename="%s_slices.%s" % (fname, __FIG_FORMAT))
        plt.close(fig2)

def plot_ground_temperature(xgrid, ygrid, temperature, output_dir=None, convert_to_celsius=True, filename_base="TSKin_Integration", x_horizontal=True):
    """
    Plot the 2D colormap of the interpolated ground temperature
    """
    x_vals = np.unique(xgrid)
    y_vals = np.unique(ygrid)
    nx, ny = np.size(x_vals), np.size(y_vals)

    if convert_to_celsius:
        values = temperature - 273.15
    else:
        values = temperature

    if x_horizontal:
        values = np.reshape(values, (ny, nx), order='C')  # show x axis on the horizontal, and y on the vertical
    else:
        values = np.reshape(values, (nx, ny), order='F')  # show x axis on the horizontal, and y on the vertical

    fig = plt.figure(facecolor='white')
    ax = ax = fig.add_subplot(111)
    cf = ax.contourf(values)
    fig.colorbar(cf)

    # Standard ticks and labels
    xticks = ax.get_xticks().astype(np.int)
    yticks = ax.get_yticks().astype(np.int)
    xtick_labels = x_vals[xticks]
    ytick_labels = y_vals[xticks]
    xlabel, ylabel = r"$X$", r"$Y$"
    # exchange if needed
    if not x_horizontal:
        xlabel, ylabel = ylabel, xlabel
        xticks, yticks = yticks, xticks
        xtick_labels, ytick_labels = ytick_labels, xtick_labels
    # update
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xticks(xticks)
    ax.set_yticks(yticks)
    ax.set_xticklabels(xtick_labels)
    ax.set_yticklabels(ytick_labels)

    if output_dir is None:
        output_dir = __RESULTS_and_PLOTS
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    filename = os.path.join(output_dir, "%s_TSkin" % filename_base)

    fig_format = __FIG_FORMAT
    if not os.path.splitext(filename)[-1].endswith(fig_format):
        filename = "%s.%s" % (filename, fig_format)
    plt.savefig(filename, dpi=500, format=__FIG_FORMAT, facecolor='w', transparent=True, bbox_inches='tight')
    plt.close(fig)
    #
    print("\rSaved: %s   \n" % filename)

def simulate_model_with_WRF_TSkin(model, wrf_data=None, output_dir=None):
    """
    Given a forward model (instance of ForwardOperator), and access to wrf_data (str/dictionary),
    simulate a the model (HyPar: model.dynamical_model) over the timspan found in wrf_data,
    and save the TSkin (ground temperator) at each time instance, and save the simulations as well.

    """
    dynamical_model = model.dynamical_model

    model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical mdel grid
    model_grid = model.model_grid()
    ground_inds = np.where(model_grid[:, -1]==0)
    target_grid = (model_grid[ground_inds, 0].flatten(), model_grid[ground_inds, 1].flatten())  # Only the X-Y directioins

    # Load data from file, and create proper dictionary
    Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, \
        U, V, W, T, RHO, TSkin, Energy = ReadNWPData.get_WRF_data()
    wrf_data = dict(Times=Times,
                    timespan=timespan,
                    DLat=DLat,
                    DLon=DLon,
                    HeightLevels=HeightLevels,
                    NDims=NDims,
                    TerrainHeight=TerrainHeight,
                    U=U,
                    V=V,
                    W=W,
                    T=T,
                    RHO=RHO,
                    TSkin=TSkin,
                    Energy=Energy)

    # initialize model state, and results repositories
    state = model.current_model_state()
    initial_state = state.copy()  # for debugging TODO: remove, after debugging
    filename_base = "TSKin_Integration_%05d" % 0
    save_and_plot_state(state, model, output_dir=output_dir, filename_base=filename_base)

    dynamical_model.update_configs({'dt':0.05})
    dt = def_dt = dynamical_model.model_configs()['dt']
    #
    # Loop over all timepoints, interpolate temperature at the ground to HyPar, and integrate model state
    for t_ind, t0 in enumerate(timespan[: -1]):

        # Carry out the interpolation (from WRF cartesian grid to HyPar grid)
        interpolated_TSkin = Merge_WRF_with_HyPar.WRF_data_interpolation(wrf_data,
                                                                         model,
                                                                         field='tskin',
                                                                         t=t0)

        #
        filename_base = "TSKin_Integration_%05d" % (t_ind+1)
        plot_ground_temperature(target_grid[0], target_grid[1], interpolated_TSkin, output_dir=output_dir, filename_base=filename_base)


        # update mdoel temperature field on file
        update_model_ground_temperature(dynamical_model, interpolated_TSkin)

        # Integrate state with updated
        t1 = timespan[t_ind+1]
        checkpoints = [t0, t1]
        checkpoints = utility.timespan_to_scalars(checkpoints)
        succeeded = False
        while not succeeded:
            # Initialize:
            _traject = None
            rintegrate_state = False

            if dt < def_dt:
                print("%s\n\tRetrying with a step size: %f \n%s" % (("*x"*30), dt, ("*x"*30)))

            # Try forward propagation with current timestep
            try:
                _, _traject = model.integrate_state(state.copy(), checkpoints=checkpoints)
            except(ValueError):
                print("Model failed to integrate state with timestep: %f; retrying with smaller timestep" % dt)
                _traject = None

            if _traject is not None:
                if _traject[-1].where_nan().size == _traject[-1].where_inf().size == 0:
                    # Good to go
                    state = _traject[-1].copy()
                    succeeded = True
                else:
                    print("Model failed to integrate state with timestep: %f; \nFound Nan or Inf in model output\nretrying with smaller timestep" % dt)
                    _traject = None

            if succeeded:
                # update state, and modpel configurations
                state = _traject[-1].copy()
                dt = def_dt
                dynamical_model.update_configs({'dt':def_dt})  # reset steps size
                #
                # Save and plot the new state
                save_and_plot_state(state, model, output_dir=output_dir, filename_base=filename_base)
                #
            elif dt >= __MINIMUM_DT/2.0:
                dt /= 2.0
                dynamical_model.update_configs({'dt':dt})
            else:
                print("Minimum timestep reached. Check HyPar's Max CFL, and choose proper time integration scheme")
                raise ValueError

def hypar_model_states_from_WRF(model, wrf_data=None, output_dir=None, return_results=False, interpolation_method='linear', verbose=False):
    """
    Create HyPar model states from WRF data, and save to output directory

    Args:
    model: isntance of forward_operator.ForwardOperator
    wrf_data: path to wrf_data file, or dictionary containing WRF proper fields
    output_dir: location to save model states;
    return results; either return times, states, or return None, None

    """
    assert isinstance(model, ForwardOperator), "model must be an instance of forward_model.ForwardOperator"
    dynamical_model = model.dynamical_model

    if return_results:
        out_times = []
        out_states = []
    else:
        out_times = out_states = None

    # Load WRF data, and extract times
    if isinstance(wrf_data, str):
        # Load data from file
        _, timespan, DLat, DLon, HeightLevels, _, TerrainHeight, \
            U, V, W, _, RHO, TSkin, Energy = ReadNWPData.get_WRF_data()
        scalar_times = [utility.time_to_scalar(t) for t in timespan]
        wrf_data_dict = dict(timespan=timespan,
                             DLat=DLat,
                             DLon=DLon,
                             HeightLevels=HeightLevels,
                             TerrainHeight=TerrainHeight,
                             U=U,
                             V=V,
                             W=W,
                             RHO=RHO,
                             TSkin=TSkin,
                             Energy=Energy
                             )

    elif isinstance(wrf_data, dict):
        wrf_data_dict = wrf_data
        timespan = wrf_data['timespan']

    else:
        print("wrf_data is either a file path to load, or a dictionary with proer entries; found %s" % type(wrf_data))
        raise TypeError

    scalar_times = [utility.time_to_scalar(t) for t in timespan]

    if output_dir is None: output_dir = os.path.join(__RESULTS_and_PLOTS, "HyPar_Model_States_From_WRF_Data")
    dir_name = hypar_model_plotter.create_output_dir(output_dir, remove_existing=False)
    filename_base = "HyPar_Model_State_From_WRF_Data"

    # Get model grid
    model_grid = model.model_grid()
    x = model_grid[:, 0]
    y = model_grid[:, 1]
    z = model_grid[:, 2]

    n_times = len(scalar_times)
    for i in range(n_times):
        t = scalar_times[i]
        t_str = timespan[i]
        print("Interpolation [%04d/%04d]: Time: %s" % (i+1, n_times, t_str))

        # Interpolation
        rho, u, v, w, E = WRF_data_interpolation(wrf_data, model, field='all', t=t, interpolation_method=interpolation_method, verbose=verbose)[-1][0]

        # create state from retrieved filed
        state = dynamical_model.state_from_numpy(x, y, z, rho, u, v, w, E, t=t)

        # write state to file
        fname = os.path.join(dir_name, "%s_Time_%d"%(filename_base, t))
        state.write_to_file(fname)
        print("HyPar Model State saved to: %s" % fname)

        if return_results:
            out_times.append(t)
            out_states.append(state.copy())

    return out_times, out_states


# ************ #
# WRF-DL Stuff #
# ************ #

def get_DL_target_grid(model, gates=None):
    """
    """
    # Check gates
    if gates is None:
        gates = __SELECTIVE_GATES  # default gates to use
    if utility.isscalar(gates):
        gates = np.asarray([gates])
    elif utility.isiterable(gates):
        for g in gates:
            if not isinstance(g, int):
                print("Entries of gates must all be integers; some entries are of type %s" % type(g))
                raise TypeError
        gates = np.asarray(gates)
    else:
        print("gates must be None, scalar, or iterable of gates indexes; received type %s " % type(gates))
        raise TypeError

    # Doppler lidar instrument Geo location
    dl_geo_coordinates = model.get_observation_configs()['dl_coordinates']  # Latitude, longitude, elevation

    # DL (from Hypar/DLiDA) coordiante system
    obs_cartesian_grid = model.observation_grid('cartesian')
    obs_spherical_grid = model.observation_grid('spherical')

    # gates coordinates are fixed over time; for each gate store
    gates_indexes = gates
    gates_obs_indexes = []
    cartesian_coordinates = []
    spherical_coordinates = []
    geo_coordinates = []
    site_obs = model._site_observation
    for g in gates:
        elevation = azimuth = 90
        obs_index = site_obs.index_from_local_coordinates(elevation=90, azimuth=90, gate=g)  # index in observation vector
        gates_obs_indexes.append(obs_index)

        c_coord = tuple(obs_cartesian_grid[obs_index, :])
        cartesian_coordinates.append(c_coord)

        s_coord = tuple(obs_spherical_grid[obs_index, :])
        spherical_coordinates.append(s_coord)

        g_coord = (dl_geo_coordinates[0], dl_geo_coordinates[1], dl_geo_coordinates[2]+s_coord[0])  # Just add altitude of gate from DL
        geo_coordinates.append(g_coord)

    return gates_indexes, gates_obs_indexes, cartesian_coordinates, spherical_coordinates, geo_coordinates

def DL_observations_time_series_stats(model=None, gates=None):
    """
    """
    if model is None:
        model = create_model()

    # TODO:
    # We need to compare vertical velocity (w) at each of the gates
    # Save Gates; and coordiantes of these gates, along with Lidar data and the interpolated WRF data

    # Rad WRF and Lidar data
    # Load WRF data from file
    Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, \
            U, V, W, T, RHO, TSkin, Energy = ReadNWPData.get_WRF_data()

    # DL data from file
    cont, dl_observations, dl_unfiltered_observations = get_DL_observations(__RESULTS_FILENAME)

    # New origin based on the dl instrument coordinates
    dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']
    model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical model grid

    # get the cartesian coordinates of the model grid
    model_cartesian_grid = model_grid = model.get_model_grid()
    model_spherical_grid = utility.cartesian_to_spherical_grid(model_cartesian_grid)
    observation_cartesian_grid = observation_grid = model.get_observation_grid()
    observation_spherical_grid = utility.cartesian_to_spherical_grid(observation_cartesian_grid)
    elevations = observation_spherical_grid[:, 1]
    azimuths = observation_spherical_grid[:, 2]

    # Get target grid points in the observation space
    target_gates_coordinates = get_DL_target_grid(model, gates)
    gates_indexes, gates_obs_indexes, cartesian_coordinates, spherical_coordinates, geo_coordinates = target_gates_coordinates

    gates_grid_Xs = np.asarray([c[0] for c in cartesian_coordinates])
    gates_grid_Ys = np.asarray([c[1] for c in cartesian_coordinates])
    gates_grid_Zs = np.asarray([c[2] for c in cartesian_coordinates])

    # Placeholders
    errors_list = []
    observations_list = []
    unfiltered_observations_list = []
    unfiltered_errors_list = []
    WRF_observations_list = []
    WRF_grids = []
    dl_grids = []
    #

    # Loop over all time points;
    for ti in range(1, np.size(HeightLevels, 0)-1):
        try:
            print("Iteration : [ %d / %d ] " % (ti, np.size(HeightLevels, 0) - 2))
            # get WRF grid at this time point
            WRF_X, WRF_Y, WRF_Z = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                                    HeightLevels[ti, :, :, :],
                                                                    TerrainHeight,
                                                                    dl_lat, dl_lon, dl_alt)
            # update grid to match dynamical model grid
            WRF_X += model_dl_coordinates[0]
            WRF_Y += model_dl_coordinates[1]
            WRF_Z += model_dl_coordinates[2]
            source_grid = (WRF_X, WRF_Y, WRF_Z)

            # Doppler lidar observations and grid
            observations = dl_observations[gates_obs_indexes, ti]
            unfiltered_observations = dl_unfiltered_observations[gates_obs_indexes, ti]
            target_grid = (gates_grid_Xs, gates_grid_Ys, gates_grid_Zs)

            # MAP WRF data to model grid (Linear Interpolation)
            interp_method = 'linear'
            WRF_radial_velocity = WRF_W = utility.interpolate_field(source_grid=source_grid,
                                                                    source_field=W[ti, ...],
                                                                    target_grid=target_grid,
                                                                    method=interp_method)
            #
            # Compare WRF data to Lidar data
            errors = observations - WRF_radial_velocity
            unfiltered_errors = unfiltered_observations - WRF_radial_velocity

            # Append things
            WRF_grids.append((WRF_X, WRF_Y, WRF_Z))
            WRF_observations_list.append(WRF_radial_velocity)
            #
            dl_grids.append(target_grid)
            observations_list.append(observations)
            unfiltered_observations_list.append(unfiltered_observations)
            unfiltered_errors_list.append(unfiltered_errors)
            errors_list.append(errors)

            print("Time: %s " % timespan[ti])
            print("Un-Filtered Error stats: ")
            print("Min: %f " % np.nanmin(unfiltered_errors))
            print("Max: %f " % np.nanmax(unfiltered_errors))
            print("Mean: %f " % np.nanmean(unfiltered_errors))
            print("STD: %f " % np.nanstd(unfiltered_errors))
            #
            print("Filtered Error stats: ")
            print("Min: %f " % np.nanmin(errors))
            print("Max: %f " % np.nanmax(errors))
            print("Mean: %f " % np.nanmean(errors))
            print("STD: %f " % np.nanstd(errors))

        except(IndexError):
            print("Loop Ended Prematurely; this is unexpected!")
            break

    # Wrap up results
    Errors = np.asarray(errors_list).T
    Unfiltered_Errors = np.asarray(unfiltered_errors_list).T
    WRF_observations = np.asarray(WRF_observations_list).T
    DL_observations = np.asarray(observations_list).T
    DL_unfiltered_observations = np.asarray(unfiltered_observations_list).T
    # Save plots & statistics
    # Update errors, and written file
    results_dict = {'WRF_observations':WRF_observations,
                    'Errors':Errors,
                    'Unfiltered_Errors':Unfiltered_Errors,
                    'WRF_grids':WRF_grids,
                    'DL_grids':dl_grids,
                    'DL_observations':DL_observations,
                    'DL_unfiltered_observations':DL_unfiltered_observations,
                    }
    pickle.dump(results_dict, open("Time_Series_%s"%__RESULTS_FILENAME, 'wb'))


    # Now; plot collective results, and calculate statistics
    fig = plt.figure(facecolor='white', figsize=(12, 6))
    ax_0 = fig.add_subplot(221)
    min_val = np.nanmin([DL_observations, WRF_observations]) - 0.05
    max_val = np.nanmax([DL_observations, WRF_observations]) + 0.05
    lines = []
    labels = []
    for i, g_ind in enumerate(grid_indexes):
        label = 'Gate %d'%g_ind
        labels.append(label)
        l = ax_0.plot(DL_observations[i, :], label=label)
        lines.append(l)
        # ax_0.legend(labels, framealpha=0.5)
        ax_0.set_title("DL Stare Observations")
        ax_0.set_ylim([min_val, max_val])
    #
    ax_1 = fig.add_subplot(222)
    for i, g_ind in enumerate(grid_indexes):
        label = 'Gate %d'%g_ind
        l = ax_1.plot(WRF_observations[i, :], label=label)
        # ax_1.legend(labels, framealpha=0.5)
        ax_1.set_title(r"Interpolated WRF $w$")
        ax_1.set_ylim([min_val, max_val])
    #
    ax_2 = fig.add_subplot(223)
    for i, g_ind in enumerate(grid_indexes):
        label = 'Gate %d'%g_ind
        l = ax_2.plot(Errors[i, :], label=label)
        # ax_2.legend(labels, framealpha=0.5)
        ax_2.set_title(r"WRF vs. DL Errors $H(x)-y$")
    #
    # Histogram of a selected Gate (over time)
    g_ind = 2
    g = grid_indexes[g_ind]
    err = Errors[g_ind]
    ax_3 = fig.add_subplot(224)
    n, bins, patches = ax_3.hist(err, 60, normed=1, facecolor='green', alpha=0.75)
    ax_3.set_title("Error Histogram gate %d" % g)
    #
    fig.legend(["Gate %d"%d for d in grid_indexes], ncol=8, fancybox=True, framealpha=0.5)
    filename = "Time_Series%s"%__PLOT_FILENAME
    plt.savefig(filename, dpi=dpi, facecolor='w', transparent=True, bbox_inches='tight')

    return results_dict

def  get_DL_observations(filename):
    """
    """
    if os.path.isfile(filename):
        print("Found previous runs; Loading observations")
        cont = pickle.load(open(filename, 'rb'))
        dl_observations = cont['DL_observations']
        dl_unfiltered_observations = cont['DL_unfiltered_observations']
    else:
        # Doppler Lidar real observations:
        print("No previous runs of this script were initiated properly; Collecting DL real observations at %d time points..." % len(timespan))
        yno = utility.query_yes_no("Do you want to Syncrhonize DL data with with ANL data host?")
        print("Collecting DL observations: Filtered")
        _, dl_observations = model.get_real_observations(timespan, collect_remote_data=yno)
        print("Collecting DL observations: Unfiltered")
        _, dl_unfiltered_observations = model.get_real_observations(timespan, snr_threshold=None)
        dl_observations = np.asarray(dl_observations).T
        unfiltered_dl_observations = np.asarray(dl_unfiltered_observations).T
        cont = dict(DL_observations=dl_observations,
                    DL_unfiltered_observations=unfiltered_dl_observations,
                    WRF_observations=WRF_observations_list,
                    Errors=errors_list,
                    Unfiltered_Errors=errors_list)
        pickle.dump(cont, open(filename, 'wb'))
    #
    return cont, dl_observations, dl_unfiltered_observations

def calculate_DL_interpolation_errors(model):
    """
    """
    if model is None:
        model = create_model()

    # Load WRF data from file
    Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, \
            U, V, W, \
            T, RHO, TSkin = ReadNWPData.get_WRF_data()

    # DL data from file
    cont, dl_observations, dl_unfiltered_observations = get_DL_observations(__RESULTS_FILENAME)

    # New origin based on the dl instrument coordinates
    dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']
    model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical model grid

    # get the cartesian coordinates of the model grid
    model_cartesian_grid = model_grid = model.get_model_grid()
    model_spherical_grid = utility.cartesian_to_spherical_grid(model_cartesian_grid)
    observation_cartesian_grid = observation_grid = model.get_observation_grid()
    observation_spherical_grid = utility.cartesian_to_spherical_grid(observation_cartesian_grid)
    elevations = observation_spherical_grid[:, 1]
    azimuths = observation_spherical_grid[:, 2]

    # Placeholders
    errors_list = []
    unfiltered_errors_list = []
    WRF_observations_list = []
    WRF_grids = []
    dl_grids = []

    # We can resume from last point if file exists
    for ti in range(1, np.size(HeightLevels, 0)-1):
        try:
            print("Iteration : [ %d / %d ] " % (ti, np.size(HeightLevels, 0) - 2))
            WRF_X, WRF_Y, WRF_Z = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                                    HeightLevels[ti, :, :, :],
                                                                    TerrainHeight,
                                                                    dl_lat, dl_lon, dl_alt)
            # update grid to match dynamical model grid
            WRF_X += model_dl_coordinates[0]
            WRF_Y += model_dl_coordinates[1]
            WRF_Z += model_dl_coordinates[2]
            source_grid = (WRF_X, WRF_Y, WRF_Z)

            # MAP WRF data to model grid (Linear Interpolation)
            interp_method = 'linear'
            WRF_U = utility.interpolate_field(source_grid=source_grid, source_field=U[ti, ...], target_grid=observation_grid, method=interp_method)
            WRF_V = utility.interpolate_field(source_grid=source_grid, source_field=V[ti, ...], target_grid=observation_grid, method=interp_method)
            WRF_W = utility.interpolate_field(source_grid=source_grid, source_field=W[ti, ...], target_grid=observation_grid, method=interp_method)

            # Calculate radial velocity [apply observation operator]
            WRF_radial_velocity = utility.velocity_vector_to_radial_velocity((WRF_U, WRF_V, WRF_W), elevation=elevations, azimuth=azimuths)

            # Get Lidar real data
            observations = dl_observations[:, ti]
            unfiltered_observations = dl_unfiltered_observations[:, ti]

            errors = observations - WRF_radial_velocity
            unfiltered_errors = unfiltered_observations - WRF_radial_velocity

            # Append things
            WRF_grids.append((WRF_X, WRF_Y, WRF_Z))
            WRF_observations_list.append(WRF_radial_velocity)
            #
            dl_grids.append(target_grid)
            observations_list.append(observations)
            unfiltered_observations_list.append(unfiltered_observations)
            unfiltered_errors_list.append(unfiltered_errors)
            errors_list.append(errors)

            # Compare WRF data to Lidar data
            print("Time: %s " % timespan[ti])
            print("Un-Filtered Error stats: ")
            print("Min: %f " % np.nanmin(unfiltered_errors))
            print("Max: %f " % np.nanmax(unfiltered_errors))
            print("Mean: %f " % np.nanmean(unfiltered_errors))
            print("STD: %f " % np.nanstd(unfiltered_errors))
            #
            print("Filtered Error stats: ")
            print("Min: %f " % np.nanmin(errors))
            print("Max: %f " % np.nanmax(errors))
            print("Mean: %f " % np.nanmean(errors))
            print("STD: %f " % np.nanstd(errors))

        except(IndexError):
            print("Loop Ended Prematurely; this is unexpected!")
            break

    # Save plots & statistics
    # Wrap up results
    Errors = np.asarray(errors_list).T
    Unfiltered_Errors = np.asarray(unfiltered_errors_list).T
    WRF_observations = np.asarray(WRF_observations_list).T
    DL_observations = np.asarray(observations_list).T
    DL_unfiltered_observations = np.asarray(unfiltered_observations_list).T
    # Save plots & statistics
    # Update errors, and written file
    results_dict = {'WRF_observations':WRF_observations,
                    'Errors':Errors,
                    'Unfiltered_Errors':Unfiltered_Errors,
                    'WRF_grids':WRF_grids,
                    'DL_grids':dl_grids,
                    'DL_observations':DL_observations,
                    'DL_unfiltered_observations':DL_unfiltered_observations,
                    }
    pickle.dump(results_dict, open(__RESULTS_FILENAME, 'wb'))

    # Now; plot collective results, For every gate plot DL data, and WRF interpolation, and errors
    # TODO...
    pass

    return cont



if __name__ == '__main__':

    # What to do?!
    simulate_HyPar_with_WRF_TSkin = False
    calculate_WRF_Hypar_TSkin_interpolation_errors = False


    # Create forward operator (dynamical model + observations):
    model = forward_model.create_standard_forward_operator()

    if simulate_HyPar_with_WRF_TSkin:
        # Simulate the model with WRF-based TSKIN
        print("Simulating HyPar with WRF-based ground temperaturs")
        simulate_model_with_WRF_TSkin(model, output_dir='WRF_HyPar_RESULTS/TSkin_Interpolation')

    if calculate_WRF_Hypar_TSkin_interpolation_errors:
        # Time series of vertical radial velocity at selective gate(s) from lidar, and WRF
        time_series_results = time_series_stats(model)

        # Calculate and save interpolation errors (WRF vs. Lidar radial velocity)
        interpolation_results = calculate_interpolation_errors(model)

