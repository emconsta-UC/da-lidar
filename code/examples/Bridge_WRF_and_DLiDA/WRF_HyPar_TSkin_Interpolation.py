
# This script explains how to create a model, and interpolate WRF TSkin to the Hypar model grid, at z-lovel of zero.
#

import numpy as np
from scipy.io import netcdf as Dataset
import matplotlib.pyplot as plt
import pickle
import datetime as datetime
import os
import os.path as pathfile
import urllib
import sys
import re

python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

# Prepare paths, and sub-packages:
parent_path = os.path.abspath('../../')
if parent_path not in sys.path:
    sys.path.append(parent_path)

try:
    import forward_model
    from forward_model import ForwardOperator
except ImportError:
    import dl_setup
    dl_setup.main()
    import forward_model
    from forward_model import ForwardOperator

from hypar_model_plotter import create_output_dir
import DL_utility as utility
import ReadNWPData


__VERBOSE = False
__RESULTS_and_PLOTS = os.path.join(os.path.abspath(os.path.dirname(__file__)), '__RESULTS_and_PLOTS/TSkin_Interpolation')
__FIG_FORMAT = 'pdf'


from MergeWRF_TSkin_with_HyPar import plot_ground_temperature



if __name__ == '__main__':

    plots_dir = __RESULTS_and_PLOTS

    # create forward operator (dynamical model + observations):
    model = forward_model.create_standard_forward_operator()
    dynamical_model = model.dynamical_model

    model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical mdel grid
    model_grid = model.model_grid()
    ground_inds = np.where(model_grid[:, -1]==0)
    target_grid = (model_grid[ground_inds, 0].flatten(), model_grid[ground_inds, 1].flatten())  # Only the X-Y directioins

    # Load data from file
    Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, \
            U, V, W, T, RHO, TSkin, Energy = ReadNWPData.get_WRF_data()

    # Get DL instrument coordinates
    dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']


    # Loop over all timepoints, interpolate temperature at the ground to HyPar
    for t_ind, t0 in enumerate(timespan[: -1]):

        # retrieve WRF grid coordinates at the current time
        WRF_X, WRF_Y, _ = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                            HeightLevels[t_ind, :, :, :],
                                                            TerrainHeight,
                                                            dl_lat, dl_lon, dl_alt
                                                           )

        # update grid to match dynamical model grid, since instrument is centered in the XY domain
        WRF_X += model_dl_coordinates[0]
        WRF_Y += model_dl_coordinates[1]

        # Source Grid (WRF), and Source Values (WRF ground temperature
        source_grid = (WRF_X, WRF_Y)  # we need to do interpolation in XY domain
        source_values = TSkin[t_ind, ...].T  # trasposing because WRF data is organized as Y X


        # Carry out the interpolation (from WRF cartesian grid to HyPar grid)
        # interpolated_TSkin is the groudn temperature at XY points of HyPar grid (Z is ommitted)
        interpolated_TSkin = utility.interpolate_field(source_grid=source_grid,
                                                       target_grid=target_grid,
                                                       source_field=source_values,
                                                       method='linear'
                                                      )
        #
        filename_base = "HyPar_Interpolated_TSkin_time_%s.%s" % (timespan[t_ind].replace(':','_'), __FIG_FORMAT)
        plot_ground_temperature(target_grid[0], target_grid[1], interpolated_TSkin, output_dir=plots_dir, filename_base=filename_base)

        # plot original temperature
        # Plot ARM Sites (RedDots)
        site = 'SGP'
        facilities = ('E32', 'C1')
        SlrStat = [utility.get_ARM_DL_coordinates(site, facility, format='degrees')[0] for facility in facilities]

        # Plot devices (red dots), give longitude on x-axis, and latitude on y-axis
        for i, facility in enumerate(facilities):
            latitude, longitude = SlrStat[i]
            plt.plot(longitude, latitude, 'ro')
            plt.text(longitude+0.01, latitude+0.01, '-'.join([site, facility]), color="red")

        # NWP domain: Skin Temperature (at zero z-leve, i.e. ground)
        # Boundary
        plt.plot(DLon[0, : ], DLat[0, : ], '--', color='b')
        plt.plot(DLon[-1, :], DLat[-1, :], '--', color='b')
        plt.plot(DLon[:, -1], DLat[:, -1], '--', color='b')
        plt.plot(DLon[:, 0 ], DLat[:, 0 ], '--', color='b')
        # Contour of Skin temperature (ground-temp)
        plt.contourf(DLon, DLat, np.squeeze(TSkin[1, :, :]-273.15))  # Kelvin to Celsius
        plt.colorbar()
        plt.title('Skin temperature')
        plt.xlabel('Longitude')
        plt.ylabel('Latitude')
        filename = "WRF_TSkin_time_%s.%s" % (timespan[t_ind].replace(':','_'), __FIG_FORMAT)
        filename = os.path.join(plots_dir, filename)
        plt.savefig(filename, dpi=500, format=__FIG_FORMAT, facecolor='w', transparent=True, bbox_inches='tight')
        print("Saved TSkin to: %s" % filename)

        plt.close('all')

