#!/usr/bin/env python


# This module aims to test the forward integration procedure (PyHypar forward integration)
# when the ground temperature is update from external source (e.g. WRF data)

# Import DLiDA core, and essential elements
import os, sys
DLiDA_path = os.path.abspath('../../../')
if DLiDA_path not in sys.path:
    sys.path.insert(0, DLiDA_path)
import DLiDA
import forward_model
from forward_model import ForwardOperator
import DL_utility as utility

# Get functionalities for reading WRF Data (From Emil's code)
import ReadNWPData  # a module that creates a forward operator with standard settings

# Other libraries
import numpy as np
import matplotlib.pyplot as plt

# Define global constants
__MINIMUM_DT = 1e-5  # Minimum allowd model timestep
__VERBOSE = False
__RESULTS_and_PLOTS = os.path.abspath("__RESULTS_and_PLOTS")
__FIG_FORMAT = 'png'


def save_and_plot_state(model, state, slices_index=None, output_dir="__RESULTS_and_PLOTS", filename_base="TSKin_Integration"):
    """
    Given a model state, and the model instance, save and plot the state
    """
    # Prepare output directory
    output_dir = os.path.abspath(output_dir)
    dir_name = utility.collection.create_output_dir(output_dir, remove_existing=False)

    # Save and plot the passed model state:
    fname = os.path.join(dir_name, "%s_State.dlvec"%(filename_base))
    state.write_to_file(fname)
    # Plot twice; multiple slices
    fname = os.path.join(dir_name, "%s_%s.%s"%(filename_base, 'Wind_Slices', __FIG_FORMAT))
    _ = utility.plotters.plot_model_state(model, state, index=slices_index, filename=fname, return_fig=False)
    fname = os.path.join(dir_name, "%s_%s_2.%s"%(filename_base, 'Wind_Slices', __FIG_FORMAT))
    _ = utility.plotters.plot_model_state(model, state, index=None, filename=fname, return_fig=False)


def plot_ground_temperature(xgrid, ygrid, temperature, output_dir="__RESULTS_and_PLOTS", convert_to_celsius=True, filename_base="TSKin_Integration", x_horizontal=True):
    """
    Plot the 2D colormap of the interpolated ground temperature
    """
    x_vals = np.unique(xgrid)
    y_vals = np.unique(ygrid)
    nx, ny = np.size(x_vals), np.size(y_vals)

    if convert_to_celsius:
        values = temperature - 273.15
    else:
        values = temperature

    if x_horizontal:
        values = np.reshape(values, (ny, nx), order='C')  # show x axis on the horizontal, and y on the vertical
    else:
        values = np.reshape(values, (nx, ny), order='F')  # show x axis on the horizontal, and y on the vertical

    fig = plt.figure(facecolor='white')
    ax = ax = fig.add_subplot(111)
    cf = ax.contourf(values)
    fig.colorbar(cf)

    # Standard ticks and labels
    xticks = ax.get_xticks().astype(np.int)
    yticks = ax.get_yticks().astype(np.int)
    xtick_labels = x_vals[xticks]
    ytick_labels = y_vals[xticks]
    xlabel, ylabel = r"$X$", r"$Y$"
    # exchange if needed
    if not x_horizontal:
        xlabel, ylabel = ylabel, xlabel
        xticks, yticks = yticks, xticks
        xtick_labels, ytick_labels = ytick_labels, xtick_labels
    # update
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xticks(xticks)
    ax.set_yticks(yticks)
    ax.set_xticklabels(xtick_labels)
    ax.set_yticklabels(ytick_labels)

    # Save results
    output_dir = os.path.abspath(output_dir)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    filename = os.path.join(output_dir, "%s_TSkin" % filename_base)
    if not os.path.splitext(filename)[-1].endswith(__FIG_FORMAT):
        filename = "%s.%s" % (filename, __FIG_FORMAT)
    plt.savefig(filename, dpi=500, format=__FIG_FORMAT, facecolor='w', transparent=True, bbox_inches='tight')
    plt.close(fig)
    #
    print("\rSaved: %s   \n" % filename)

if __name__ == "__main__":

    use_WRF_data = False  # On/Off using  wrf ground temperature

    # create forward operator (dynamical model + observations), and get a reference to HyPar/dynamical-model
    model = forward_model.create_standard_forward_operator("Radial-Velocity")
    dynamical_model = model.dynamical_model

    # Play with HyPar timestep:
    # dynamical_model.update_configs({'dt':0.05})
    dt = def_dt = dynamical_model.model_configs()['dt']

    # Get indexes at which slices are plotted
    Nx, Ny, Nz = dynamical_model.get_grid_sizes()
    slices_index = [Nx//2, Ny//2, 1]

    # Initialize model state, and results repositories
    state = model.current_model_state()
    initial_state = state.copy()  # for debugging TODO: remove, after debugging
    filename_base = "TSKin_Integration_%05d" % 0
    save_and_plot_state(model, state, filename_base=filename_base, slices_index=slices_index)

    if use_WRF_data:
        # Model and Doppler Lidar Grid information
        dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']  # DL instrument global coordinates
        model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical model grid
        model_grid = model.model_grid()
        ground_inds = np.where(model_grid[:, -1]==0)
        target_x = model_grid[ground_inds, 0].flatten()
        target_y = model_grid[ground_inds, 1].flatten()
        target_z = np.ones_like(target_x) * dl_alt
        target_grid = (target_x, target_y)  # Only the X-Y directioins

        # Load WRF data from file
        Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, U, V, W, T, RHO, TSkin = ReadNWPData.get_WRF_data()
    else:
        timespan = ReadNWPData.get_WRF_data()[1]

    # Loop over all timepoints, interpolate temperature at the ground to HyPar, and integrate model state
    for t_ind, t0 in enumerate(timespan[: -1]):
        # Time settings
        t1 = timespan[t_ind+1]
        checkpoints = [t0, t1]
        checkpoints = utility.timespan_to_scalars(checkpoints)
        print("Time Winds: %s --> %s" % (t0, t1))

        if use_WRF_data:
            # retrieve WRF grid coordinates at the current time (New origin is at the DL instrument)
            WRF_X, WRF_Y, _ = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                                HeightLevels[t_ind, :, :, :],
                                                                TerrainHeight,
                                                                dl_lat, dl_lon, dl_alt,
                                                                verbose=True)

            # update WRF grid to match dynamical model grid, since instrument is centered in the XY domain
            WRF_X += model_dl_coordinates[0]
            WRF_Y += model_dl_coordinates[1]

            # Source Grid (WRF), and Source Values (WRF ground temperature
            source_grid = (WRF_X, WRF_Y)  # we need to do interpolation in XY domain
            source_values = TSkin[t_ind, ...].T  # trasposing because WRF data is organized as Y X

            # Carry out the interpolation (from WRF cartesian grid to HyPar grid)
            # interpolated_TSkin is the groudn temperature at XY points of HyPar grid (Z is ommitted)
            interpolated_TSkin = utility.interpolate_field(source_grid=source_grid,
                                                           target_grid=target_grid,
                                                           source_field=source_values,
                                                           method='linear')

            # Save Interpolated TSkin
            filename = "TSKin_Integration_%05d.npy" % (t_ind)
            filename = os.path.join(__RESULTS_and_PLOTS, filename)
            np.save(filename, interpolated_TSkin)

            filename_base = "TSKin_Integration_%05d" % (t_ind)
            plot_ground_temperature(target_grid[0], target_grid[1], interpolated_TSkin, filename_base=filename_base)

            # update model (HyPar) temperature field on file
            dynamical_model.update_ground_temperature(interpolated_TSkin)

        # Integrate state with updated
        # Initialize:

        try:
            _, _traject = model.integrate_state(state.copy(), checkpoints=checkpoints)
            if _traject[-1].where_nan().size == _traject[-1].where_inf().size == 0:
                # Save and plot the new state
                print("Model state updated; saving...")
                state = _traject[-1].copy()
                filename_base = "TSKin_Integration_%05d" % (t_ind+1)
                save_and_plot_state(model, state, filename_base=filename_base, slices_index=slices_index)
            else:
                print("Failed to integrate state (NANS FOUND). Will proceed to next profile with latest proper state")
                continue
        except(ValueError):
            print("Failed to integrate state (ValueError exception raised). Will proceed to next profile with latest proper state")
            continue

