#! ~/usr/bin/env python3

"""
    This is a very Simple scrip to test the Filtering_Process class that iterates over a filter, e.g. EnKF with HyPar model.
    If MPI is initiated, model, fitler, and process are all initiated on all nodes in the communicator

    This will replace test_Filtering_Process.py

"""

import os
import sys
import getopt

python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import pickle

import numpy as np

try:
    import ConfigParser
except:
    import configparser as ConfigParser


# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../../'))
import dl_setup
dl_setup.main()

import DL_utility as utility
import DLidarVec

# import forward operator class:
import forward_model
from forward_model import ForwardOperator

from MergeWRF_TSkin_with_HyPar import save_and_plot_state, plot_ground_temperature, update_model_ground_temperature


# Get functionalities for reading WRF Data (From Emil's code)
import ReadNWPData  # a module that creates a forward operator with standard settings


__STANDARD_ENSEMBLE_SIZE =  50
__STANDARD_RESULTS_DIR = "/Users/attia/Research/POSTDOC/Argonne/Projects/Active/MACSER/LIDAR_IP/DLiDA/Results/Filtering_Results_WRF_TSkin_FreeRun"
__INITIL_ENSEMBLE_REPO = "/Users/attia/Research/POSTDOC/Argonne/Projects/Active/MACSER/LIDAR_IP/DLiDA/Results/Initial_Ensemble"


def get_args(input_args, output_dir=None,
             init_ens_repo=None,
             dl_stare_only=True,
             use_wrf_data=False,
             wrf_data_file=None,
             ensemble_size=None):
    """
    Get command line arguments; default values passed are used, if not entered in the initiating command
    """
    try:
        opts, args = getopt.getopt(input_args,"hd:s:i:u:w:e:",["help", "output-dir=", "stare-only=", "init-ensemble=", "use-wrf-data=", "wrf-data=", "ensemble-size="])
    except getopt.GetoptError:
        print('python/run MergeWRF_Wind_with_HyPar_FreeRun.py -d <results output dir> -s <stare-only flag> -i <initial ensemble folder> -u <use-wrf-data flag> -w <wrf data file> -e <ensemble size>')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print('python/run MergeWRF_Wind_with_HyPar_FreeRun.py -d <results output dir> -s <stare-only flag> -i <initial ensemble folder> -u <use-wrf-data flag> -w <wrf data file> -e <ensemble size>')
            sys.exit()
        elif opt in ("-s", "--stare-only"):
            dl_stare_only = utility.collection.str2bool(arg)
        elif opt in ("-i", "--init-ensemble"):
            init_ens_repo = arg
        elif opt in ("-d", "--output-dir"):
            output_dir = arg
        elif opt in ("-u", "--use-wrf-data"):
            use_wrf_data = utility.collection.str2bool(arg)
        elif opt in ("-w", "--wrf-data"):
            wrf_data_file = arg
        elif opt in ("-e", "--ensemble-size"):
            try:
                ensemble_size = int(arg)
            except(ValueError):
                print("Invalid ensemble size %s\n" % arg)
                raise
            if ensemble_size < 1:
                print("Invalid ensemble size (< 1)")
                sys.exit(2)
            elif ensemble_size == 1:
                print("WARNING: ensemble size of size 1 won't be useful for filtering!")
            else:
                pass

    # overwrite None values
    if output_dir is None:
        output_dir = __STANDARD_RESULTS_DIR
    if init_ens_repo is None:
        init_ens_repo = __INITIL_ENSEMBLE_REPO
    if ensemble_size is None:
        ensemble_size = __STANDARD_ENSEMBLE_SIZE

    return output_dir, dl_stare_only, init_ens_repo, use_wrf_data, wrf_data_file, ensemble_size


def initial_ensemble_mean(ensemble_size, ens_dir, prefix='analysis_ensemble'):
    """
    """
    ens = DLidarVec.state_ensemble_from_file(ens_dir, prefix, ensemble_size)
    return ens.mean()


if __name__ == '__main__':

    # get results directory (if passed as argument)
    results_dir, dl_stare_only, init_ensemble_repo, use_wrf_data, wrf_data_file, filter_ensemble_size = get_args(sys.argv[1: ])
    # ====================================================================================== #
    #        Create Forward Operator: Dynamical model + Observation handler/operator         #
    # ====================================================================================== #
    # 
    model_configs = forward_model.standard_model_configs()
    # model_configs.update({})  # <-- update/modify model configs if needed

    obs_configs = forward_model.standard_observation_configs(site='sgp',
                                                              facility='C1',
                                                              prognostic_variable='radial-velocity',
                                                              num_gates=120,
                                                              stare_only=dl_stare_only)

    # Forward Operator: Dynamics+Observations; using HyPar, and DL-Data settings
    model = ForwardOperator(model_configs, obs_configs)
    model_grid = model.get_model_grid()
    dynamical_model = model.dynamical_model
    if use_wrf_data:
        model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical model grid
    print("Model Initialized")
    sys.stdout.flush()
    #
    # ====================================================================================== #


    # ====================================================================================== #
    #      Read WRF data; and get information needed for calculating ground temperature      #
    # ====================================================================================== #
    # 
    if use_wrf_data:
        filename_base = "TSKin_Integration"
        Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, \
            U, V, W, T, RHO, TSkin = ReadNWPData.get_WRF_data()

        # Get DL instrument coordinates
        dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']
        model_grid = model.model_grid()
        ground_inds = np.where(model_grid[:, -1]==0)
        target_grid = (model_grid[ground_inds, 0].flatten(), model_grid[ground_inds, 1].flatten())  # Only the X-Y directioins

    else:
        filename_base = "Free_Run"
        timespan = ReadNWPData.get_WRF_data(datafile=wrf_data_file)[1]  # we just need the timespan
    #
    # ====================================================================================== #
    if not os.path.isdir(results_dir):
        os.makedirs(results_dir)

    # inspect existing files
    state = None
    init_index = 0  # the index corresponding to non-existing state
    for t_ind in range(len(timespan)):
        filename = "%s_%05d_State.dlvec" % (filename_base, t_ind)
        filename = os.path.join(results_dir, filename)
        # print("Looking for %s" % filename)
        if not os.path.isfile(filename):
            init_index = t_ind
            if t_ind == 0:
                state = initial_ensemble_mean(filter_ensemble_size, init_ensemble_repo)
                file_prefix = "%s_%05d" % (filename_base, 0)
                save_and_plot_state(state, model, output_dir=results_dir, filename_base=file_prefix)
                # print("Initial state written to: %s" % filename)
                init_index = 1
            else:
                filename = "%s_%05d_State.dlvec" % (filename_base, (t_ind-1))
                filename = os.path.join(results_dir, filename)
                state = DLidarVec.state_vector_from_file(filename)
                # print("Initial state Loaded from: %s" % filename)
            break
        elif t_ind == len(timespan)-1:
            print("All states on this timespan were found. Nothing to do.")
            sys.exit()
        else:
            # File Exists, and more to lookup
            pass

    #
    # Iterate over the experiment_timespan, and apply filtering at each cycle
    for t_ind, t1 in enumerate(timespan[1: ]):

        # Time settings
        t0 = timespan[t_ind-1]
        checkpoints = [t0, t1]
        checkpoints = utility.timespan_to_scalars(checkpoints)
        print("Time Winds: %s --> %s" % (t0, t1))

        if t_ind < init_index:
            # State found
            print("File found. skipping...")
            continue

        if use_wrf_data:
            # retrieve WRF grid coordinates at the current time
            WRF_X, WRF_Y, _ = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                                HeightLevels[t_ind, :, :, :],
                                                                TerrainHeight,
                                                                dl_lat, dl_lon, dl_alt
                                                               )

            # update grid to match dynamical model grid, since instrument is centered in the XY domain
            WRF_X += model_dl_coordinates[0]
            WRF_Y += model_dl_coordinates[1]

            # Source Grid (WRF), and Source Values (WRF ground temperature
            source_grid = (WRF_X, WRF_Y)  # we need to do interpolation in XY domain
            source_values = TSkin[t_ind, ...].T  # trasposing because WRF data is organized as Y X

            # Carry out the interpolation (from WRF cartesian grid to HyPar grid)
            # interpolated_TSkin is the groudn temperature at XY points of HyPar grid (Z is ommitted)
            interpolated_TSkin = utility.interpolate_field(source_grid=source_grid,
                                                           target_grid=target_grid,
                                                           source_field=source_values,
                                                           method='linear'
                                                          )
            try:
                #
                filename_base = "TSKin_Integration_%05d" % (t_ind+1)
                plot_ground_temperature(target_grid[0], target_grid[1], interpolated_TSkin, output_dir=results_dir, filename_base=filename_base)
            except:
                print(target_grid[0].size)
                print(target_grid[1].size)
                print(interpolated_TSkin.shape)
                raise
            # update mdoel temperature field on file
            update_model_ground_temperature(dynamical_model, interpolated_TSkin)

        _, traject = model.integrate_state(state, checkpoints)
        state = traject[-1]
        file_prefix = "%s_%05d" % (filename_base, (t_ind))
        save_and_plot_state(state, model, output_dir=results_dir, filename_base=file_prefix)
        print("Final state written to: %s under %s" % (file_prefix, results_dir))

    #
    # ====================================================================================== #
    #                                     <<<<DONE>>>>                                       #
    # ====================================================================================== #
    #
