

__RESULTS_FILENAME = "WRF_DL_Data_results.pcl"
__PLOT_FILENAME = "WRF_DL_Data_results.png"
__SELECTIVE_GATES = range(8, 15)  # selective gates for vertical profiling

import numpy as np
from scipy.io import netcdf as Dataset
import matplotlib.pyplot as plt
import pickle
import datetime as datetime
import os
import os.path as pathfile
import urllib
import sys
import re

python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

# Prepare paths, and sub-packages:
parent_path = os.path.abspath('../../')
if parent_path not in sys.path:
    sys.path.append(parent_path)

try:
    import forward_model
    from forward_model import ForwardOperator
except ImportError:
    import dl_setup
    dl_setup.main()
    import forward_model
    from forward_model import ForwardOperator

import DL_utility as utility
import ReadNWPData


def create_model(obs_variable='radial-velocity'):
    """
    """
    # create forward operator (dynamical model + observations):
    model = forward_model.create_standard_forward_operator(obs_variable)
    return model

def get_target_grid(model, gates=None):
    """
    """
    # Check gates
    if gates is None:
        gates = __SELECTIVE_GATES  # default gates to use
    if utility.isscalar(gates):
        gates = np.asarray([gates])
    elif utility.isiterable(gates):
        for g in gates:
            if not isinstance(g, int):
                print("Entries of gates must all be integers; some entries are of type %s" % type(g))
                raise TypeError
        gates = np.asarray(gates)
    else:
        print("gates must be None, scalar, or iterable of gates indexes; received type %s " % type(gates))
        raise TypeError

    # Doppler lidar instrument Geo location
    dl_geo_coordinates = model.get_observation_configs()['dl_coordinates']  # Latitude, longitude, elevation

    # DL (from Hypar/DLiDA) coordiante system
    obs_cartesian_grid = model.observation_grid('cartesian')
    obs_spherical_grid = model.observation_grid('spherical')

    # gates coordinates are fixed over time; for each gate store
    gates_indexes = gates
    gates_obs_indexes = []
    cartesian_coordinates = []
    spherical_coordinates = []
    geo_coordinates = []
    site_obs = model._site_observation
    for g in gates:
        elevation = azimuth = 90
        obs_index = site_obs.index_from_local_coordinates(elevation=90, azimuth=90, gate=g)  # index in observation vector
        gates_obs_indexes.append(obs_index)

        c_coord = tuple(obs_cartesian_grid[obs_index, :])
        cartesian_coordinates.append(c_coord)

        s_coord = tuple(obs_spherical_grid[obs_index, :])
        spherical_coordinates.append(s_coord)

        g_coord = (dl_geo_coordinates[0], dl_geo_coordinates[1], dl_geo_coordinates[2]+s_coord[0])  # Just add altitude of gate from DL
        geo_coordinates.append(g_coord)

    return gates_indexes, gates_obs_indexes, cartesian_coordinates, spherical_coordinates, geo_coordinates


def time_series_stats(model=None, gates=None):
    """
    """
    if model is None:
        model = create_model()

    # TODO:
    # We need to compare vertical velocity (w) at each of the gates
    # Save Gates; and coordiantes of these gates, along with Lidar data and the interpolated WRF data

    # Rad WRF and Lidar data
    # Load WRF data from file
    Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, \
            U, V, W, \
            T, RHO, TSkin = ReadNWPData.get_WRF_data()

    # DL data from file
    cont, dl_observations, dl_unfiltered_observations = get_dl_observations(__RESULTS_FILENAME)

    # New origin based on the dl instrument coordinates
    dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']
    model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical model grid

    # get the cartesian coordinates of the model grid
    model_cartesian_grid = model_grid = model.get_model_grid()
    model_spherical_grid = utility.cartesian_to_spherical_grid(model_cartesian_grid)
    observation_cartesian_grid = observation_grid = model.get_observation_grid()
    observation_spherical_grid = utility.cartesian_to_spherical_grid(observation_cartesian_grid)
    elevations = observation_spherical_grid[:, 1]
    azimuths = observation_spherical_grid[:, 2]

    # Get target grid points in the observation space
    target_gates_coordinates = get_target_grid(model, gates)
    gates_indexes, gates_obs_indexes, cartesian_coordinates, spherical_coordinates, geo_coordinates = target_gates_coordinates

    gates_grid_Xs = np.asarray([c[0] for c in cartesian_coordinates])
    gates_grid_Ys = np.asarray([c[1] for c in cartesian_coordinates])
    gates_grid_Zs = np.asarray([c[2] for c in cartesian_coordinates])

    # Placeholders
    errors_list = []
    observations_list = []
    unfiltered_observations_list = []
    unfiltered_errors_list = []
    WRF_observations_list = []
    WRF_grids = []
    dl_grids = []
    #

    # Loop over all time points;
    for ti in range(1, np.size(HeightLevels, 0)-1):
        try:
            print("Iteration : [ %d / %d ] " % (ti, np.size(HeightLevels, 0) - 2))
            # get WRF grid at this time point
            WRF_X, WRF_Y, WRF_Z = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                                    HeightLevels[ti, :, :, :],
                                                                    TerrainHeight,
                                                                    dl_lat, dl_lon, dl_alt)
            # update grid to match dynamical model grid
            WRF_X += model_dl_coordinates[0]
            WRF_Y += model_dl_coordinates[1]
            WRF_Z += model_dl_coordinates[2]
            source_grid = (WRF_X, WRF_Y, WRF_Z)

            # Doppler lidar observations and grid
            observations = dl_observations[gates_obs_indexes, ti]
            unfiltered_observations = dl_unfiltered_observations[gates_obs_indexes, ti]
            target_grid = (gates_grid_Xs, gates_grid_Ys, gates_grid_Zs)

            # MAP WRF data to model grid (Linear Interpolation)
            interp_method = 'linear'
            WRF_radial_velocity = WRF_W = utility.interpolate_field(source_grid=source_grid,
                                                                    source_field=W[ti, ...],
                                                                    target_grid=target_grid,
                                                                    method=interp_method)
            #
            # Compare WRF data to Lidar data
            errors = observations - WRF_radial_velocity
            unfiltered_errors = unfiltered_observations - WRF_radial_velocity

            # Append things
            WRF_grids.append((WRF_X, WRF_Y, WRF_Z))
            WRF_observations_list.append(WRF_radial_velocity)
            #
            dl_grids.append(target_grid)
            observations_list.append(observations)
            unfiltered_observations_list.append(unfiltered_observations)
            unfiltered_errors_list.append(unfiltered_errors)
            errors_list.append(errors)

            print("Time: %s " % timespan[ti])
            print("Un-Filtered Error stats: ")
            print("Min: %f " % np.nanmin(unfiltered_errors))
            print("Max: %f " % np.nanmax(unfiltered_errors))
            print("Mean: %f " % np.nanmean(unfiltered_errors))
            print("STD: %f " % np.nanstd(unfiltered_errors))
            #
            print("Filtered Error stats: ")
            print("Min: %f " % np.nanmin(errors))
            print("Max: %f " % np.nanmax(errors))
            print("Mean: %f " % np.nanmean(errors))
            print("STD: %f " % np.nanstd(errors))

        except(IndexError):
            print("Loop Ended Prematurely; this is unexpected!")
            break

    # Wrap up results
    Errors = np.asarray(errors_list).T
    Unfiltered_Errors = np.asarray(unfiltered_errors_list).T
    WRF_observations = np.asarray(WRF_observations_list).T
    DL_observations = np.asarray(observations_list).T
    DL_unfiltered_observations = np.asarray(unfiltered_observations_list).T
    # Save plots & statistics
    # Update errors, and written file
    results_dict = {'WRF_observations':WRF_observations,
                    'Errors':Errors,
                    'Unfiltered_Errors':Unfiltered_Errors,
                    'WRF_grids':WRF_grids,
                    'DL_grids':dl_grids,
                    'DL_observations':DL_observations,
                    'DL_unfiltered_observations':DL_unfiltered_observations,
                    }
    pickle.dump(results_dict, open("Time_Series_%s"%__RESULTS_FILENAME, 'wb'))


    # Now; plot collective results, and calculate statistics
    fig = plt.figure(facecolor='white', figsize=(12, 6))
    ax_0 = fig.add_subplot(221)
    min_val = np.nanmin([DL_observations, WRF_observations]) - 0.05
    max_val = np.nanmax([DL_observations, WRF_observations]) + 0.05
    lines = []
    labels = []
    for i, g_ind in enumerate(grid_indexes):
        label = 'Gate %d'%g_ind
        labels.append(label)
        l = ax_0.plot(DL_observations[i, :], label=label)
        lines.append(l)
        # ax_0.legend(labels, framealpha=0.5)
        ax_0.set_title("DL Stare Observations")
        ax_0.set_ylim([min_val, max_val])
    #
    ax_1 = fig.add_subplot(222)
    for i, g_ind in enumerate(grid_indexes):
        label = 'Gate %d'%g_ind
        l = ax_1.plot(WRF_observations[i, :], label=label)
        # ax_1.legend(labels, framealpha=0.5)
        ax_1.set_title(r"Interpolated WRF $w$")
        ax_1.set_ylim([min_val, max_val])
    #
    ax_2 = fig.add_subplot(223)
    for i, g_ind in enumerate(grid_indexes):
        label = 'Gate %d'%g_ind
        l = ax_2.plot(Errors[i, :], label=label)
        # ax_2.legend(labels, framealpha=0.5)
        ax_2.set_title(r"WRF vs. DL Errors $H(x)-y$")
    #
    # Histogram of a selected Gate (over time)
    g_ind = 2
    g = grid_indexes[g_ind]
    err = Errors[g_ind]
    ax_3 = fig.add_subplot(224)
    n, bins, patches = ax_3.hist(err, 60, normed=1, facecolor='green', alpha=0.75)
    ax_3.set_title("Error Histogram gate %d" % g)
    #
    fig.legend(["Gate %d"%d for d in grid_indexes], ncol=8, fancybox=True, framealpha=0.5)
    filename = "Time_Series%s"%__PLOT_FILENAME
    plt.savefig(filename, dpi=dpi, facecolor='w', transparent=True, bbox_inches='tight')



    return results_dict


def  get_dl_observations(filename):
    """
    """
    if os.path.isfile(filename):
        print("Found previous runs; Loading observations")
        cont = pickle.load(open(filename, 'rb'))
        dl_observations = cont['DL_observations']
        dl_unfiltered_observations = cont['DL_unfiltered_observations']
    else:
        # Doppler Lidar real observations:
        print("No previous runs of this script were initiated properly; Collecting DL real observations at %d time points..." % len(timespan))
        yno = utility.query_yes_no("Do you want to Syncrhonize DL data with with ANL data host?")
        print("Collecting DL observations: Filtered")
        _, dl_observations = model.get_real_observations(timespan, collect_remote_data=yno)
        print("Collecting DL observations: Unfiltered")
        _, dl_unfiltered_observations = model.get_real_observations(timespan, snr_threshold=None)
        dl_observations = np.asarray(dl_observations).T
        unfiltered_dl_observations = np.asarray(dl_unfiltered_observations).T
        cont = dict(DL_observations=dl_observations,
                    DL_unfiltered_observations=unfiltered_dl_observations,
                    WRF_observations=WRF_observations_list,
                    Errors=errors_list,
                    Unfiltered_Errors=errors_list)
        pickle.dump(cont, open(filename, 'wb'))
    #
    return cont, dl_observations, dl_unfiltered_observations


def calculate_interpolation_errors(model):
    """
    """
    if model is None:
        model = create_model()

    # Load WRF data from file
    Times, timespan, DLat, DLon, HeightLevels, NDims, TerrainHeight, \
            U, V, W, \
            T, RHO, TSkin = ReadNWPData.get_WRF_data()

    # DL data from file
    cont, dl_observations, dl_unfiltered_observations = get_dl_observations(__RESULTS_FILENAME)

    # New origin based on the dl instrument coordinates
    dl_lat, dl_lon, dl_alt = model.get_observation_configs()['dl_coordinates']
    model_dl_coordinates = model.get_DL_coordinates()  # coordiantes of DL instrument w.r.t dynamical model grid

    # get the cartesian coordinates of the model grid
    model_cartesian_grid = model_grid = model.get_model_grid()
    model_spherical_grid = utility.cartesian_to_spherical_grid(model_cartesian_grid)
    observation_cartesian_grid = observation_grid = model.get_observation_grid()
    observation_spherical_grid = utility.cartesian_to_spherical_grid(observation_cartesian_grid)
    elevations = observation_spherical_grid[:, 1]
    azimuths = observation_spherical_grid[:, 2]

    # Placeholders
    errors_list = []
    unfiltered_errors_list = []
    WRF_observations_list = []
    WRF_grids = []
    dl_grids = []

    # We can resume from last point if file exists
    for ti in range(1, np.size(HeightLevels, 0)-1):
        try:
            print("Iteration : [ %d / %d ] " % (ti, np.size(HeightLevels, 0) - 2))
            WRF_X, WRF_Y, WRF_Z = ReadNWPData.shift_WRF_coordinates(DLat, DLon,
                                                                    HeightLevels[ti, :, :, :],
                                                                    TerrainHeight,
                                                                    dl_lat, dl_lon, dl_alt)
            # update grid to match dynamical model grid
            WRF_X += model_dl_coordinates[0]
            WRF_Y += model_dl_coordinates[1]
            WRF_Z += model_dl_coordinates[2]
            source_grid = (WRF_X, WRF_Y, WRF_Z)

            # MAP WRF data to model grid (Linear Interpolation)
            interp_method = 'linear'
            WRF_U = utility.interpolate_field(source_grid=source_grid, source_field=U[ti, ...], target_grid=observation_grid, method=interp_method)
            WRF_V = utility.interpolate_field(source_grid=source_grid, source_field=V[ti, ...], target_grid=observation_grid, method=interp_method)
            WRF_W = utility.interpolate_field(source_grid=source_grid, source_field=W[ti, ...], target_grid=observation_grid, method=interp_method)

            # Calculate radial velocity [apply observation operator]
            WRF_radial_velocity = utility.velocity_vector_to_radial_velocity((WRF_U, WRF_V, WRF_W), elevation=elevations, azimuth=azimuths)

            # Get Lidar real data
            observations = dl_observations[:, ti]
            unfiltered_observations = dl_unfiltered_observations[:, ti]

            errors = observations - WRF_radial_velocity
            unfiltered_errors = unfiltered_observations - WRF_radial_velocity

            # Append things
            WRF_grids.append((WRF_X, WRF_Y, WRF_Z))
            WRF_observations_list.append(WRF_radial_velocity)
            #
            dl_grids.append(target_grid)
            observations_list.append(observations)
            unfiltered_observations_list.append(unfiltered_observations)
            unfiltered_errors_list.append(unfiltered_errors)
            errors_list.append(errors)

            # Compare WRF data to Lidar data
            print("Time: %s " % timespan[ti])
            print("Un-Filtered Error stats: ")
            print("Min: %f " % np.nanmin(unfiltered_errors))
            print("Max: %f " % np.nanmax(unfiltered_errors))
            print("Mean: %f " % np.nanmean(unfiltered_errors))
            print("STD: %f " % np.nanstd(unfiltered_errors))
            #
            print("Filtered Error stats: ")
            print("Min: %f " % np.nanmin(errors))
            print("Max: %f " % np.nanmax(errors))
            print("Mean: %f " % np.nanmean(errors))
            print("STD: %f " % np.nanstd(errors))

        except(IndexError):
            print("Loop Ended Prematurely; this is unexpected!")
            break

    # Save plots & statistics
    # Wrap up results
    Errors = np.asarray(errors_list).T
    Unfiltered_Errors = np.asarray(unfiltered_errors_list).T
    WRF_observations = np.asarray(WRF_observations_list).T
    DL_observations = np.asarray(observations_list).T
    DL_unfiltered_observations = np.asarray(unfiltered_observations_list).T
    # Save plots & statistics
    # Update errors, and written file
    results_dict = {'WRF_observations':WRF_observations,
                    'Errors':Errors,
                    'Unfiltered_Errors':Unfiltered_Errors,
                    'WRF_grids':WRF_grids,
                    'DL_grids':dl_grids,
                    'DL_observations':DL_observations,
                    'DL_unfiltered_observations':DL_unfiltered_observations,
                    }
    pickle.dump(results_dict, open(__RESULTS_FILENAME, 'wb'))

    # Now; plot collective results, For every gate plot DL data, and WRF interpolation, and errors
    # TODO...
    pass

    return cont



if __name__ == '__main__':

    # create forward operator (dynamical model + observations):
    model = create_model()

    # Time series of vertical radial velocity at selective gate(s) from lidar, and WRF
    time_series_results = time_series_stats(model)

    # Calculate and save interpolation errors (WRF vs. Lidar radial velocity)
    interpolation_results = calculate_interpolation_errors(model)



