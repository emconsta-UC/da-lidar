#! ~/usr/bin/env python3
"""
    This is a very Simple scrip to test the Filtering_Process class that iterates over a filter, e.g. EnKF with HyPar model.
    If MPI is initiated, model, fitler, and process are all initiated on all nodes in the communicator
"""

import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import pickle

import numpy as np

try:
    import mpi4py
    mpi4py.rc.recv_mprobe = False
    from mpi4py import MPI
    use_MPI = True
except(ImportError):
    use_MPI = False

# Initialize DATeS with default settings
if use_MPI:
    comm = MPI.COMM_WORLD
    my_rank = comm.rank
    comm_size = comm.size
else:
    comm = None
    my_rank = 0
    comm_size = 1

# Prepare paths, and sub-packages:
sys.path.append(os.path.abspath('../'))
import dl_setup
dl_setup.main()

if False and use_MPI:
    comm.Barrier()


import DL_utility as utility
import DLidarVec

# import forward operator class:
from forward_model import ForwardOperator
from EnKF import DEnKF as KalmanFilter

from filtering_process import FilteringProcess


def _match_model_configs(config1, config2):
    """
    Match essential model configs, and return a binary flag
    The configuratios to compare are:
        1- 
        2- 
        3- 

    """
    compare_keys = ['size',
                    'nvars',
                    'ndims',
                    'domain_lower_bounds',
                    'domain_upper_bounds',
                    'boundary',
                    'thermal_source',
                    'physics']
    match = True
    for key in compare_keys:
        if config1[key] != config2[key]:
            # print("\n\n Configuration mismatch in key : %s\n\n" % key)
            # print(config1[key])
            # print(config2[key])
            match = False
            break
        else:
            pass
            # print("Key %s match yeeeeeeey!!!" % key)

    # print("****** Final match : %s " % match)
    return match


def generate_process_IC(model, load_from_file=True, file_path=None, repository=None, repo_config_basename='IC_config', timespan=None):
    """
    Generate an initial condition (true/reference state) for the assimilation process.
    This IC will be generated once, and written to file; after that, it will be re-used for future runs.
    The initial condition depends on the model configurations, so we need to match the model configurations with those of the initial condition before loading. Either we store the
    configurations withing the state file, or in a separate file; I'll follow the latter, i.e. I'll save the configuration dictionary with the IC, and upon loading the IC, I'll
    test for match. This will be helpful for testing multiple model setttings

    Args:
        model: model object, used to create state_vector object
        load_from_file: if False, an IC will be regenerated given the passed timespan, with IC generated at timespan[-1]
        file_path: The path to file from/to which the IC will be loaded/written;
            if None, the default is ./filteringProcessIC.dlvec
        repository: path to the directory that contains the initial conditions, along with corresponding configurations
        repo_config_basename: used only if repository is not None; this is the base name of configurations files
        timespan: used if the initial condition fails to be loaded from file.
            The standard HyPar initial solution is loaded, and propagated over timespan,
            the IC is the result at the upper bound of timespan. If None, the default is [0, 300], i.e. 5 minutes

    Returns:
        IC: the model state, to be used as IC

    """
    ic_base_filename = 'filteringProcessIC'
    model_configs = model.get_model_configs().copy()
    config_file_path = None
    #
    if file_path is None:
        if load_from_file and repository is not None:
            if not os.path.isdir(repository):
                os.makedirs(repository)
            # Look for the initial condition inside the given repository
            # Match configurations to get the initial condition filename
            trgt_filename = None
            config_files = utility.get_list_of_files(repository, recursive=False)
            proper_config_files = []
            for fname in config_files:
                _, fname = os.path.split(fname)
                if fname.startswith(repo_config_basename):
                    proper_config_files.append(fname)

            # print(config_files, proper_config_files)
            # Loop over proper configuration files
            for fname in proper_config_files:
                configs = pickle.load(open(os.path.join(repository, fname), 'rb'))
                if _match_model_configs(configs, model_configs):
                    # print("Match configs; found %s" % fname)
                    try:
                        trgt_filename = configs['IC_filename']
                    except KeyError:
                        pass
                    if trgt_filename is None:
                        continue
                    else:
                        break
            if trgt_filename is not None:
                file_path = os.path.join(repository, trgt_filename)
                # print("found exisiting matching configs file %s " % file_path)
            else:
                # print("NO matching configs in %s. Will create a new one " % repository)
                # Now, generate a filepath to save IC to:
                load_from_file = False  # to force file-saving
                filename = utility.try_file_name(repository, file_prefix=ic_base_filename, extension='dlvec', return_abspath=False)
                file_path = os.path.join(repository, filename)
                model_configs.update({'IC_filename':filename})
                config_file_path = utility.try_file_name(repository, file_prefix=repo_config_basename, return_abspath=True)
                #
            # print(">>>>> configurations file: %s " % config_file_path)
            # print(">>>>> ic filepath: %s " % file_path)

    else:
        # a specific filepath is given:
        this_dir = os.path.dirname(os.path.abspath(__file__))
        ic_filename = "%s.dlvec" % ic_base_filename
        file_path = os.path.join(this_dir, ic_filename)
        if not load_from_file:
            model_configs.update({'IC_filename':ic_filename})
            config_file_path = utility.try_file_name(this_dir, file_prefix=repo_config_basename, return_abspath=True)

    if load_from_file:
        if not os.path.isfile(file_path):
            IC = None
            location, ic_filename = os.path.split(file_path)
            model_configs.update({'IC_filename':ic_filename})
            config_file_path = utility.try_file_name(location, file_prefix=repo_config_basename, return_abspath=True)
            save_to_file = True
            # print("IC file '%s' doesn't exist" % file_path)
            # print("The IC will be created and saved to file for later use")
        else:
            # try to load from file, check size, etc., and then assign to state_vector entries
            print("Loading Filtering Process IC from file...")
            IC = model.state_vector(data_file=file_path)
            save_to_file = False
            print("done...")
            try:
                pass
            except (ValueError, IOError):
                print("\nFailed to load the IC from file; Will recreate, and save for later use")
                IC = None
                location, ic_filename = os.path.split(file_path)
                model_configs.update({'IC_filename':ic_filename})
                config_file_path = utility.try_file_name(location, file_prefix=repo_config_basename, return_abspath=True)
                save_to_file = True
    else:
        IC = None
        save_to_file = True

    if IC is None:
        # Create IC
        print("Creating the Process IC for the first time")
        if timespan is None:
            timespan = [0, 300]
        else:
            assert len(timespan) >= 2, "The timespan must be an iterable of length >= 2"
            timespan = [timespan[0], timespan[-1]]
        IC = model.current_model_state()  # Reference initial solution,
        _, traject = model.integrate_state(IC, checkpoints=timespan)
        IC = traject[-1].copy()

    if save_to_file:
        print("Writitng Process IC to file %s ..." % file_path),
        sys.stdout.flush()
        # write the configurations first:
        if config_file_path is not None:
            print("Saving configurations file into %s " % config_file_path)
            with open(config_file_path, 'wb') as f_id:
                pickle.dump(model_configs, f_id)
        print("Saving initial condition in %s " % file_path)
        IC.write_to_file(file_path)
        print("done...")
    return IC


def generate_process_initial_ensemble(ensemble_size,
                                      model=None,
                                      load_from_file=True,
                                      repo_dir=None,
                                      time=0,
                                      repo_config_file='ensemble_config',  # TODO: utilize this one
                                      filename_prefix='ensemble_member',
                                      files_ext='dlvec'
                                     ):
    """
    Generate an initial ensemble for the assimilation process.
    This IC will be generated once, and written to file; after that, it will be re-used for future runs.

    Args:
        ensemble_size:
        model: model object, used to create state_vector object
        load_from_file: if False, an IC will be regenerated given the passed timespan, with IC generated at timespan[-1]
        repo_dir: The path under/to which the initial_ensemble files will be loaded/written;
            if None, the default is ./filteringProcessIC.dlvec
        time: time assigned to ensemble members

    Returns:
        ensemble:

    Remarks:
        This does NOT check the grid at all; it just blindly load ensemble from files; you need to make your own checks if you wish

    """
    if load_from_file:
        if repo_dir is None:
            print("Loading from file requires a valid directory 'repo_dir'; None is not acceptable value")
            raise ValueError
    else:
        if model is None:
            print("With 'load_from_file' turned off, you must pass a model instance")
            raise ValueError

    ensemble = None
    if load_from_file:
        try:
            print("Loading initial ensemble from file...")
            ensemble = DLidarVec.state_ensemble_from_file(file_name_prefix=filename_prefix,
                                                         directory=repo_dir,
                                                         max_ens_size=ensemble_size,
                                                         files_ext=files_ext
                                                         )
            recreate = False
            for i in range(ensemble.size):
                if model is not None:
                    if ensemble[i].size != model.state_size():
                        ensemble = None
                        recreate = True
                        # print("***sizes DON'T match***")
                        break
                    else:
                        # print("***sizes match***")
                        pass
                ensemble[i].time = time
            if not recreate and  (ensemble.size == ensemble_size):
                recreate = False
            else:
                recreate = True
                #
        except(IOError, ValueError):
            recreate = True
            #
    else:
        recreate = True
        #

    if recreate:
        write_ensemble = True
        if ensemble is None:
            if model is None:
                print("You must pass a model instance if you want full ensmeble;\nReturning only what I found")
                raise ValueError
            ensemble = model.create_state_ensemble(ensemble_size, fill_ensemble=True, t=t0)  # get empty ensemble
        elif ensemble.size == 0:
            if model is None:
                print("You must pass a model instance if you want full ensmeble;\nReturning only what I found")
                raise ValueError
            ensemble = model.create_state_ensemble(ensemble_size, fill_ensemble=True, t=t0)  # get empty ensemble
        else:
            print("Only %d ensemble members found in the passed repo_dir" % ensemble.size)
            if model is None:
                write_ensemble = False
                print("You must pass a model instance if you want full ensmeble;\nReturning only what I found")
            else:
                write_ensemble = True
                print("Will use the model to create the rest of the ensemble members")
                add_ens_size = ensemble_size - ensemble.size
                add_ensemble = model.create_state_ensemble(add_ens_size, fill_ensemble=True, t=t0)  # get empty ensemble
                for i in range(add_ensemble.size):
                    ensemble.append(add_ensemble[i])

        # Save ensemble for future use : this will overwrite existing ones if newely created vectors are found
        if write_ensemble:
            print("Writing ensembles for later use")
            if not os.path.isdir(repo_dir):
                os.makedirs(repo_dir)
            ensemble.write_to_file(directory=repo_dir,
                                   file_name_prefix=filename_prefix,
                                   files_ext=files_ext
                                  )
    print("Initial ensemble loaded")
    return ensemble



if __name__ == '__main__':
    # ======================================================================================== #
    #                                         Settings                                         #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #  This is where you can play with experiment and plotting settins                         #
    #  You only need to play with settings here.                                               #
    # ======================================================================================== #

    # ---------------------------------------------------------------------------------------- #
    # 1- Hypar Model Settings:
    # ---------------------------------------------------------------------------------------- #

    # i   ) domain size in the x, y, z direction (i.e. Nx, Ny, Nz)
    _size = [31, 31, 31]

    # ii  ) grid limits (lower and upper limits the grid in each direction)
    _domain_upper_bounds = [2000, 2000, 1000]
    _domain_lower_bounds = [0, 0, 0]

    # iii ) Number of process in each direction
    _iproc =[2, 2, 2]
    # iv  ) number of ghost points:
    _ghost = 3
    # v   ) (Maximum) time step of the time-integration scheme
    _dt = 0.05
    # vi  ) timestepping method and PETSc Runtime options
    _ts            = 'rk'
    _ts_type       = 'ssprk3'
    _hyp_scheme    = 'weno5'  # TODO: I haven't involved this yet; will be ignored for now
    _PETSc_options = '-ts_type rk -ts_rk_type 4'
    # vii ) Other hypar-specific settings
    _hyp_flux_split = 'no'
    _hyp_int_type   = 'components'
    _par_type       = 'none'
    _par_scheme     = 'none'
    # viii) Physics Settings:
    _gamma     = 1.4
    _upwinding = "rusanov"
    _gravity   = [0.0, 0.0, 9.8]
    _rho_ref   = 1.1612055171196529
    _p_ref     = 100000.0
    _R         = 287.058
    _HB        = 2

    # ix  ) Boundary Settings
    # 6 sides of the boundary x-lower, x-upper, y-lower, y-upper, z-lower, z-upper; see example below
    _faces_types = ['periodic',             # x-lower
                    'periodic',             # x-upper
                    'periodic',             # y-lower
                    'periodic',             # y-upper
                    'thermal-noslip-wall',     # z-lower
                    'extrapolate'              # z-upper
                   ]
    # boundary settings for each of the 6 sides. Put None if neither slip nor thermal in the side type
    _faces_settings=[None,
                     None,
                     None,
                     None,
                     (0.0, 0.0, 0.0, 'temperature_data.dat'),
                     None
                    ]
    # Example of boundary settings:
    # faces_types=['periodic', 'periodic', 'periodic', 'periodic', 'thermal-slip-wall', 'slip-wall'],
    # faces_settings=[None, None, None, None, (0.0, 0.0, 0.0, 'temperature_data.dat'), (0.0, 0.0, 0.0)]

    # x   ) Thermal source settings; used for any wall having 'thermal' in its face_type value
    #       Now, you can create multiple sources, by passing iterables instead of scalars for x,y,z,r
    _thermal_shape = 'multiple-discs'  # this is more general than disc; Py_HyPar.pyx will be refactored accordingly
    _thermal_source_center = [[2500,  2500],
                              [17500, 4000],
                              [500]]  # x-y-z coordinates of the the thermal source; scalars and 1d iterables  are replicated
    _rcent = [1500, 1500]  # radius/radii (for multiple-discs shape) of the thermal spot
    _T_diff = [10, 10]  # Temperature (K) differnece used to create thermal spot

    # xi  )


    # ---------------------------------------------------------------------------------------- #
    # 2- Experiment Settings:
    # ---------------------------------------------------------------------------------------- #
    if True:
        start_time = '2017:08:13:12:10:00'
        end_time   = '2017:08:13:12:20:00'
        delta_time = '0000:00:00:00:00:30'
    else:  # for quick debugging
        start_time = '2017:08:13:12:12:00'
        end_time   = '2017:08:13:12:12:30'
        delta_time = '0000:00:00:00:00:10'
    prtrb_initial_ensemble = False
    ensemble_repository = 'Model_Ensemble_Repository'
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                                   END OF Settings                                        #
    # ======================================================================================== #

    # Prepre the model and plot things
    # model configureations
    HyPar_model_configs = {'size':                _size,
                         'domain_upper_bounds': _domain_upper_bounds,
                         'domain_lower_bounds': _domain_lower_bounds,
                         'iproc':               _iproc,
                         'dt':                  _dt,
                         'PETSc_options':       _PETSc_options,
                         'init_time':           0.0,  # (optional) time of the initial state (default is zero)
                         'ghost':               _ghost,
                         'rest_iter':           0,
                         'ts':                  _ts,
                         'ts_type':             _ts_type,
                         'hyp_scheme':          _hyp_scheme,
                         'hyp_flux_split':      _hyp_flux_split,
                         'hyp_int_type':        _hyp_int_type,
                         'par_type':            _par_type,
                         'par_scheme':          _par_scheme,
                         'cleanup_dir':False}
    # Physics
    ph_dict = dict(gamma     = _gamma,
                   upwinding = _upwinding,
                   gravity   = _gravity,
                   rho_ref   = _rho_ref,
                   p_ref     = _p_ref,
                   R         = _R,
                   HB        = _HB)
    HyPar_model_configs.update({'physics':ph_dict})
    # Boundary
    b_dict = dict(boundary=dict(num_faces=len(_faces_types),
                                faces_types=_faces_types,
                                faces_settings=_faces_settings),
                  thermal_source=dict(shape=_thermal_shape,
                                      xcent= _thermal_source_center[0],
                                      ycent= _thermal_source_center[1],
                                      zcent= _thermal_source_center[2],
                                      rcent=_rcent,
                                      T_diff=_T_diff))
    HyPar_model_configs.update(b_dict)

    # ====================================================================================== #
    #                                    GENERAL SETTINGS                                    #
    # ====================================================================================== #
    model_dist_unit = 'M'  # model grid distance usit 'M' for meters, 'KM' for Kilometers
    filter_ensemble_size = 30
    use_real_observations = True # True/False  --> real/synthetic Observations
    collect_remote_data = False # Set only once for a give timespan,, otherwise it will take time everytimt you run the script
    filter_localization_function = 'Gaspari-Cohn'
    filter_localization_radius = 500  # loc radius im dist_unit
    filter_inflation_factors = (1.0, 1.0)  # (forecast_infaltion_factor, analysis_inflation_factor)
    verbose = False
    random_seed = 2345  # passed to the random number generator used; numpy here
    #
    # ====================================================================================== #


    # ====================================================================================== #
    #                                 Model and Observation                                  #
    # ====================================================================================== #

    # Create Checkpoints for observation and assimilation:
    experiment_timespan = utility.create_timespan(start_time=start_time,
                                                  end_time=end_time,
                                                  delta_time=delta_time,
                                                  return_string=False)

    obs_checkpoints = experiment_timespan[1: ]
    da_checkpoints = obs_checkpoints
    scalar_experiment_timespan = utility.timespan_to_scalars(experiment_timespan)

    t0 = scalar_experiment_timespan[0]

    site_facility = 'sgpdlC1'                     # sgpdlE32, sgpdlC1, etc.
    field         = 'radial-velocity'             # 'radial_velocity', 'doppler', 'wind-velocity', etc


    DLiDA_obs_configs = {'site_facility':site_facility,
                         't':t0,
                         'dl_coordinates': '0,0,0',
                         'prog_var': field,
                         'num_gates': 120,
                         'range_gate_length': 30,
                         'elevations': [90],  # [60, 90],
                         'azimuthes': [90],  # [0, 90, 180, 270]
                        }

    #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Forward Operator)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #
    # Forward Operator: Dynamics+Observations; using HyPar, and DL-Data settings
    model_configs = HyPar_model_configs
    obs_configs = DLiDA_obs_configs
    model = ForwardOperator(model_configs, obs_configs, dist_unit=model_dist_unit)  # model distance M/KM

    # Propagate the initial state, and use it as a reference initial condition
    if my_rank == 0:  # it will be broadcasted in the filter initiation
        # model.integrate_state(checkpoints=[0, 5])
        # xtrue = model.current_model_state()  # Reference initial solution,
        xtrue = generate_process_IC(model, load_from_file=True, repository='FilteringProcess_ICRepo')
        xtrue += 0.5
        _, _tr = model.integrate_state(xtrue, [0, 60])
        xtrue = _tr[-1]
        # xtrue = generate_process_IC(model, load_from_file=True, file_path=None, timespan=None)
        xtrue.time = t0
    else:
        xtrue = None

    if comm is not None:
        xtrue = comm.bcast(xtrue, root=0)
        if my_rank == 0:
            print("Done broadcasting reference solution")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~(Synthetic or Real Observations)~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    #
    # Genrate real or synthetic observations
    if use_real_observations:
        ref_initial_state = None
        if my_rank == 0:
            print("Collecting Real Observations")
        # collect real observations...
        if my_rank == 0:  # it will be broadcasted in the filter initiation
            obs_times, _observations = model.get_real_observations(obs_checkpoints,
                                                                   return_ensemble=False,
                                                                   aggregate_data=True,
                                                                   mismatch_policy='exact',
                                                                   # snr_threshold =0.008, 
                                                                   collect_remote_data=collect_remote_data)
            scalar_obs_times = utility.timespan_to_scalars(obs_times)
            for i in range(len(_observations)):
                _observations[i].time = scalar_obs_times[i]  # for compatability with ensemble membles
            # match tims to experimental time span
            observations = []
            for i, t in enumerate(obs_checkpoints):
                if t in obs_times:
                    obs = _observations[obs_times.index(t)]
                else:
                    obs = None
                observations.append(obs)
            print("Collecting Real Observations")
        else:
            # Observations will be broadcasted inside filtering process if passed through arguments.
            observations = None
    else:
        ref_initial_state = xtrue
        if my_rank ==0 and xtrue is None:
            # No need to create synthetic observations here; filtering process will do it on the fly if xtrue is just defined above
            print("xtrue must be valid reference IC, to be usef for creating synthetic observations")
            raise ValueError
        else:
            observations = None
    #
    if comm is not None:
        observations = comm.bcast(observations, root=0)
        print("Observations broadcasted")
        print("Rank: %d" % my_rank, observations)
    sys.stdout.flush()

    # ====================================================================================== #

    # ====================================================================================== #
    #                                     Assimilation                                       #
    # ====================================================================================== #

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Filter Object/Cycle)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #
    # Generate perturbed initial forecast state, and initial ensemble (real method needed for real observations!)
    ensemble_size = filter_ensemble_size
    # if my_rank == 0:
    # else:
    #     init_ensemble = None

    if my_rank == 0:
        if prtrb_initial_ensemble:
            xf = model._frcst_err_model.add_noise(xtrue, in_place=False)
            xf.time = t0
            init_ensemble = model.create_state_ensemble(0, fill_ensemble=False, t=t0)
            for i in range(ensemble_size):
                member = model._frcst_err_model.add_noise(xf, in_place=False)  # The noise is not normalized
                member.time = t0  # TODO: Check; do we need it?:
                init_ensemble.append(member)
            del xf
            del member
        else:
            init_ensemble = generate_process_initial_ensemble(ensemble_size, model=model, repo_dir=ensemble_repository, time=scalar_experiment_timespan[0])
        #
        for e in init_ensemble:
            e +=  0.5   #  Just shift the initial ensemble to test quick improvements
        infl_vec = model.create_inflation_vector(1.1)  # get a  standard space-dependent inflation vector
        init_ensemble.inflate(infl_vec)  # At the beginning, we don't want to trust the model very much
    else:
        init_ensemble = None

    #
    if comm is not None:
        comm.Barrier()
    sys.stdout.flush()

    # Create the filter object:
    filter_configs={'model': model,
                    'MPI_COMM':comm,
                    'ensemble_size':ensemble_size,
                    'localize_covariances':True,
                    'localization_radius':filter_localization_radius,
                    'localization_function':filter_localization_function,
                    'forecast_inflation_factor':filter_inflation_factors[0],
                    'analysis_inflation_factor':filter_inflation_factors[1]
                   }
    filter_output_configs = {'file_output_moment_only':False, 'verbose':verbose}
    filter_obj = KalmanFilter(filter_configs=filter_configs, output_configs=filter_output_configs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Assimiltion Process)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #
    # Observation time setting: (Create proper timespan)
    ref_initial_time = scalar_experiment_timespan[0]
    obs_checkpoints = utility.timespan_to_scalars(obs_checkpoints)
    da_checkpoints = obs_checkpoints
    assimilation_configs = {'filter':filter_obj,
                            'MPI_COMM':comm,
                            'obs_checkpoints':obs_checkpoints,
                            'da_checkpoints':da_checkpoints,
                            'ref_initial_condition':ref_initial_state,
                            'ref_initial_time':ref_initial_time,
                            'initial_ensemble':init_ensemble,
                            'forecast_first':True,
                            'random_seed':random_seed}
    assim_output_configs = {'scr_output':True,
                            'scr_output_iter':1,
                            'file_output':True,
                            'file_output_iter':1,
                            'verbose':verbose,}
    assim_experiment = FilteringProcess(assimilation_configs=assimilation_configs,
                                        output_configs=assim_output_configs)
    
    # Iterate over the experiment_timespan, and apply filtering at each cycle
    assim_experiment.start_assimilation_process(observations=observations, obs_checkpoints=obs_checkpoints, da_checkpoints=da_checkpoints)
    # assim_experiment.start_assimilation_process()

    #
    # ====================================================================================== #
    #                                     <<<<DONE>>>>                                       #
    # ====================================================================================== #
    #
