# Here, setup essential constants you need to be visible to all modules in the LIDAR-data inversion package
# This includes LIDAR-DATA director and host (e.g. login1.mcs.anl.gov),
# To leave a field empty (to be ignored), either set it as an empty string, or set it to None

import os

# ========================================================================================== #
#                          LIDAR-Data information:
#                         -------------------------
# - This includes LIDAR-DATA director and host (e.g. login1.mcs.anl.gov),
# - To leave a field empty (to be ignored), either set it as an empty string,
#   or set it to None
#
# ------------------------------------------------------------------------------------------ #
#

DL_NUM_GATES = 400  # this is mandatory to fix the size of an observation vector

# 1) Path to DL-Code (eigher on a local machine or on a remote sever);
# This should be the parent directory of where this file is
DL_ROOT_PATH = os.path.split(os.path.abspath(__file__))[0]  # remove this one...
DL_ROOT_PATH = os.path.split(DL_ROOT_PATH)[0]  # out of src.

#
# 2) Information about the host on which data is stored; if empty, data is assumed to be stored locally
DL_DATA_HOST_ADDRESS = "mcs.anl.gov"
DL_DATA_HOST_NAME = "login"  # --> directs to login1, login2, etc.
DL_DATA_REPO_ROOT_PATH = "/nfs/proj-emconsta/DL-Data"  # this is the original repository, to be read from,
DL_DATA_INCOMING_ROOT_PATH = "/nfs/proj-emconsta/DL-Data/Incoming"  # this is where new tar'd files exist

DL_DATA_RAW_RELATIVE_PATH = "Raw"
DL_DATA_PARSED_RELATIVE_PATH = "Parsed"

DL_DATA_CATEGORIZATION = 'year/month/facilitysite/scan-type'  # How data files are coategorized inside the raw or parsed directories

#
# 3) Local path in which parsed DATA files are stored (build gradually upon request);
# This section is ignored if the code runs at 'DL_DATA_HOST_ADDRESS'
# This is needed for fast access of data, e.g. for plotting, testing, assimilation, etc.
DL_DATA_LOCAL_PATH = ""  # if provided, taken as the path in which data is stored locally. Directory structure is similar to that on the original host
DL_DATA_LOCAL_RELATIVE_PATH = "local_dl_data"  # used only if LIDAR_DATA_Local_path if NOT provided. Data directory relative to (Sub)package root directory (DL_ROOT_PATH)

# ========================================================================================== #

DL_TIME_EPS = 1e-7  # can be used to compare time (seconds) instances

#
# ========================================================================================== #
#                                  VALIDATE constants                                        #
#                    (Don't touch these unless you add new constants):                       #
# ------------------------------------------------------------------------------------------ #
if DL_DATA_HOST_ADDRESS is not None:
    if not isinstance(DL_DATA_HOST_ADDRESS, str):
        print("Constant: 'DL_DATA_HOST_ADDRESS' must be either None, or a string!")
        raise TypeError
    else:
        DL_DATA_HOST_ADDRESS = DL_DATA_HOST_ADDRESS.strip(' ')
        if not DL_DATA_HOST_ADDRESS:  # empty strings are evaluated to False in Boolean context
            DL_DATA_HOST_ADDRESS =  None

if DL_DATA_HOST_NAME is not None:
    if not isinstance(DL_DATA_HOST_NAME, str):
        print("Constant: 'DL_DATA_HOST_NAME' must be either None, or a string!")
        raise TypeError
    else:
        DL_DATA_HOST_NAME = DL_DATA_HOST_NAME.strip(' ')
        if not DL_DATA_HOST_NAME:
            DL_DATA_HOST_NAME =  None

if DL_ROOT_PATH is not None:
    if not isinstance(DL_ROOT_PATH, str):
        print("Constant: 'DL_ROOT_PATH' must be either None, or a string!")
        raise TypeError
    elif not DL_ROOT_PATH:
        DL_ROOT_PATH =  None
    else:
        pass  # a non-empty string.
if DL_ROOT_PATH is None:
    pass  # we will see about that!

#
if DL_DATA_LOCAL_PATH is not None:
    if not isinstance(DL_DATA_LOCAL_PATH, str):
        print("Constant: 'DL_DATA_LOCAL_PATH' must be either None, or a string!")
        raise TypeError
    else:
        DL_DATA_LOCAL_PATH = DL_DATA_LOCAL_PATH.strip(' ')
        if not DL_DATA_LOCAL_PATH:
            DL_DATA_LOCAL_PATH =  None
else:
    pass

if DL_DATA_LOCAL_RELATIVE_PATH is not None:
    if not isinstance(DL_DATA_LOCAL_RELATIVE_PATH, str):
        print("Constant: 'DL_DATA_LOCAL_RELATIVE_PATH' must be either None, or a string!")
        raise TypeError
        #
    else:
        DL_DATA_LOCAL_RELATIVE_PATH = DL_DATA_LOCAL_RELATIVE_PATH.strip(' ')
        if not DL_DATA_LOCAL_RELATIVE_PATH:
            DL_DATA_LOCAL_RELATIVE_PATH =  None
else:
    pass

if DL_DATA_LOCAL_PATH is None and DL_DATA_LOCAL_RELATIVE_PATH is None:
    if DL_DATA_REPO_ROOT_PATH is not None:
        print("CAUTION: Both DL_DATA_LOCAL_PATH, DL_DATA_LOCAL_RELATIVE_PATH are NOT set!")
        print("CAUTION: this is a situation that will work only when the code runs on the server hosting the DL data repository!")
        #
    else:
        sep = "\n%s\n" % ("*"*80)
        msg = """ %s If you want to run this on the server hosting the DL data repository,
        \r\t> You MUST set the variable 'DL_DATA_REPO_ROOT_PATH'
        \r If you want to run this code on another client,
        \r\t> You MUST set either 'DL_DATA_LOCAL_PATH', or 'DL_DATA_LOCAL_RELATIVE_PATH' %s \n""" %(sep, sep)
        print(msg)
        raise AssertionError
        #
elif DL_DATA_LOCAL_PATH is None and DL_DATA_LOCAL_RELATIVE_PATH is not None:
    DL_DATA_LOCAL_PATH = os.path.join(DL_ROOT_PATH, DL_DATA_LOCAL_RELATIVE_PATH)
else:
    pass

# ========================================================================================== #
#
