
# 1- Add package path
# 2- setup Linalg modules as they are imported by Model, and DL_SITE_OBS
# 3- setup HyPar_Model
# 4- setup DL_DATA_HANDLER


import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)

import subprocess
import numpy as np

try:
    from mpi4py import MPI
    use_MPI = True
except(ImportError):
    use_MPI = False

# Only rank 0 should sequentially build extension modules, and everyone elese waits!
if use_MPI:
    MPI_COMM = MPI.COMM_WORLD
    my_rank = MPI_COMM.Get_rank()
    comm_size = MPI_COMM.Get_size()
else:
    MPI_COMM = None
    my_rank = 0
    comm_size = 1

#
f_dir, f_id = os.path.split(os.path.abspath(__file__))
for root, _, _ in os.walk(os.path.join(f_dir,'src')):
    if os.path.isdir(root) and not ('/.' in root or '__' in root):  # avoid special or hidden directories
        # in case this is not the initial run. We don't want  to add duplicates to the system paths' list.
        if root not in sys.path:
            if root not in sys.path:
                sys.path.insert(1, root)
            # sys.path.append(root)

try:
    from Hypar_constants import *
    no_hypar = False
except ValueError:
    no_hypar = True
from DL_constants import *


c_path, _ = os.path.split(os.path.abspath(__file__))
#
linalg_path = 'src/LinAlg'
linalg_path = os.path.join(c_path, linalg_path)

model_path = 'src/Model_Dynamics'
model_path = os.path.join(c_path, model_path)

if no_hypar:
    hypar_path = None
    hypar_path = None
else:
    hypar_path = 'src/Forward_Operator/Py_HyPar'
    hypar_path = os.path.join(c_path, hypar_path)

dl_data_path = 'src/DL_data_handler'
dl_data_path = os.path.join(c_path, dl_data_path)

assimilation_path = 'src/Assimilation'
dl_data_path = os.path.join(c_path, assimilation_path)


#
def add_src_paths():
    """
    Add directories containing source files to the system directories.
    All package-specified paths are inserted in the beginning of the system paths' list

    Args:

    Returns:

    """
    if DL_ROOT_PATH not in sys.path:
        sys.path.insert(1, DL_ROOT_PATH)
    #
    for root, _, _ in os.walk(os.path.join(DL_ROOT_PATH,'src')):
        if os.path.isdir(root) and not ('/.' in root or '__' in root):  # avoid special or hidden directories
            # in case this is not the initial run. We don't want  to add duplicates to the system paths' list.
            if root not in sys.path:
                sys.path.insert(2, root)
                # sys.path.append(root)

def setup_pack(rel_dir, verbose=False):
    """
    """
    cwd = os.getcwd()
    os.chdir(rel_dir)

    cmd = "%s setup.py build_ext -i" % sys.executable
    try:
        if verbose:
            print("Building Cython Module: %s" % os.path.join(os.path.abspath(rel_dir), "setup.py"))
        if not verbose:
            logfile = os.path.join(cwd,"__build_logfile__.tmp")
            cmd += " > %s " % logfile
            os.system(cmd)
            os.remove(logfile)
        else:
            os.system(cmd)
    except:
        print("Building failed for: %s" % os.path.join(os.path.abspath(rel_dir), "setup.py"))
        os.chdir(cwd)
        raise
    else:
        os.chdir(cwd)

#
def setup(verbose=False):
    """
    Setup up Environment Variables (If needed) and add source directories to PYTHONPATH.

    Args:

    Returns:

    """
    #
    if my_rank == 0:
        # Install packages:
        if not verbose:
            sys.stdout.write("Setting up DLiDA extension modules on the fly")
            sys.stdout.write(".")
            sys.stdout.flush()
        # 1- Linalg:
        setup_pack(linalg_path, verbose=verbose)
        if not no_hypar:
            # 2- Dunamical model:
            setup_pack(hypar_path, verbose=verbose)
            if not verbose:
                sys.stdout.write(".")
                sys.stdout.flush()
        else:
            print("\n\nWARNING: If you are planning to use the dynamical model, you must hypar Installed!\n\n")
        # 3- DL Observation:
        setup_pack(dl_data_path, verbose=verbose)
        if not verbose:
            sys.stdout.write(".")
            sys.stdout.flush()
        # 4- Data Assimilation:
        setup_pack(dl_data_path, verbose=verbose)
        if not verbose:
            sys.stdout.write(".\n")
            sys.stdout.flush()
    else:
        if verbose:
            print("Rank %d: Awaiting for modules to build..." % my_rank)

    # All nodes wait for compilation to be done.
    if comm_size > 1:
        MPI_COMM.Barrier()


def main(verbose=False):
    """
    1- install sub-packages (if they are changed)
    2- export environment variables

    """
    setup(verbose=verbose)

#
if __name__ == "__main__":
    # Initialize DATeS with default settings
    #
    main(verbose=True)
