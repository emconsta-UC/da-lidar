
import sys, os
pckg_dir = os.path.abspath(os.path.dirname(__file__))

if pckg_dir not in sys.path:
    sys.path.insert(0, pckg_dir)

import src
core = src  # alias
del sys
del os
