
# TODO: for all file IO, make sanity checks!
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import re

cimport numpy as np
import numpy as np
import os

cimport DLidarVec_IO as vec_io

from libc.stdlib cimport calloc, malloc, free, system
from libc.math cimport sqrt, pow, abs

# Unify used data types:
ctypedef double DTYPE_DOUBLE_t
DTYPE_DOUBLE = np.float64



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                 Useful functions; will be moved to utility module SOON                     #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
def isiterable(x):
  try:
    iter(x)
    out = True
  except:
    out = False
  return out
def isscalar(x):
  return np.isscalar(x)
def isinteger(x):
  flag = False
  if isscalar(x):
    if int(x) == x:
      flag = True
  return flag
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#              Useful functions; wrappers to one or more of the classes herein               #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
cpdef StateVector state_vector_from_file(file_name, files_ext='dlvec'):
  """
  Read DLidarVec from binary file
  """
  # read everything from file; ignore size, sequential if passed!
  # Validate file name
  if os.path.isfile(file_name):
    vec = StateVector(data_file=file_name)
  else:
    fdir, fname = os.path.split(file_name)
    f, e = os.path.splitext(fname)
    e = e.strip('. ')
    if not re.match(r'\A%s\Z', files_ext.strip('. '), re.IGNORECASE):
      if len(e) == 0:
        e = files_ext.strip('. ').lower()
      else:
        e = e.lower()
      #
      fname = "%s.%s" % (f, e)
      file_name = os.path.join(fdir, fname)
      #
      if os.path.isfile(file_name):
        vec = StateVector(data_file=file_name)
      else:
        print("Not valid file: %s" % file_name)
        raise IOError()
    else:
      vec = StateVector(data_file=file_name)
    #
  return vec


cpdef StateVector state_vector_from_np_array(np.ndarray data, sequential=True, time=0.0):
  """
  create a DLidarVec of a given size, and double array value, which size is passed as first argument
  """
  # read everything from file; ignore size, sequential if passed!
  cpdef long size = data.size
  vec = StateVector(size=size, sequential=sequential, time=time)
  for i in range(size):
    vec[i] = data[i]
  return vec


cpdef Ensemble state_ensemble_from_file(directory, file_name_prefix, max_ens_size=150, files_ext='dlvec'):
  """
  Read State Ensemble from binary file(s)
  IOError is raised if directory is not found, or <file_name_prefix>_0 is not found; termination happens if <file_name_prefix_<max_ens_size> is reached; with increment of 1 at a
  time.
  
  Args:
    directory:
    file_name_prefix:
    max_ens_size (150):
    files_ext (dlvec):

  Returns:
    ensemble: DLidarVec.Ensmble

  """
  file_name_prefix, _ = os.path.splitext(file_name_prefix)
  if files_ext is None:
    files_ext = ''
  else:
    files_ext = ".%s" % files_ext.strip(' .')

  if not os.path.isdir(directory):
    print("No such directory: %s " % directory)
    raise IOError

  ens_size = 0
  # get initial vector
  ens_ind = 0
  file_name = "%s_%d%s" % (file_name_prefix, ens_ind, files_ext)
  file_path = os.path.join(directory, file_name)
  if not os.path.isfile(file_path):
    print("Couldn't find any matching ensemble members to the pattern '%s_ind_.dlvec' " % file_name_prefix)
    raise IOError
  # Good to start reading, at least one ensemble member
  vec = StateVector(data_file=file_path)
  ensemble = Ensemble(0, vec.size, sequential=vec.is_sequential())
  ensemble.append(vec)
  while True:
    ens_ind += 1
    file_name = "%s_%d%s" % (file_name_prefix, ens_ind, files_ext)
    file_path = os.path.join(directory, file_name)
    if os.path.isfile(file_path):
      vec = StateVector(data_file=file_path)
      ensemble.append(vec)
      if ensemble.size == max_ens_size:
        print("Maximum ensemble size of %d reached, terminating..." % max_ens_size)
        break
    else:
      break
  return ensemble
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#         A class implementing a State Vecotor object (similar to PETSc.Vec);                #
#         however, this is fine tuned for data assimilation.                                 #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
cdef class StateVector(object):
  """
  A data structure that stores a state vector, and gives access to linear algebra operations
  This is an initial class that allocates a contiguous block of memory for the who state vector; PETScVec will follow soon
  
  Args:
    size
    sequential
    data_file
    time

  Returns:
    StateVector instance
  """

  def __cinit__(self, size=None, sequential=True, data_file=None, time=None, **kwargs):
    if size is data_file is None:
      print("Cannot initial a DLidarVec withougt either data_file or size! Both are None!")
      raise ValueError

    cdef long _size
    cdef double _time
    cdef int _sequential
    cdef double *_data

    if data_file is not None:
      # read everything from file; ignore size, sequential if passed!
      if python_version >= (3, 0, 0):
        data_file = str.encode(data_file)
      try:
        out = vec_io.alloc_and_read_DLidar_Vec(data_file, &_size, &_time, &_sequential, &_data)
        if out:
          raise IOError
      except(IOError):
        print("New-style reading (with time) failed; Attempting old-style format")
        out = vec_io.alloc_and_read_DLidar_Vec_OldFormat(data_file, &_size, &_sequential, &_data)
        print("Received out=%d" % out)
        _time = 0.0
        print("OldStyle rading worked *** CONSIDER UPDATED SAVED FILE WITH PROPER TIME ***...\n")

      if out:
        print("Failed to read data from file %s" % data_file)
        raise IOError

      if time is not None:
        assert time >= 0, "Time can't be negative!"
        print("Overwriting state time with passed time")
        _time = time
      else:
        pass

      if size is not None:
        assert (isinteger(size) and size>0), "size must be positive integer!"
        if size != _size:
          print("WARNNG: The size found in file is different from size passed to the constructor!")
          print("Passed : %d" % size)
          print("Found: %d" % _size)
          print("Ignoring passed value")
      else:
        pass

      # Attach to self:
      self._size = _size
      self._time = _time
      self._sequential = <bint>_sequential
      self.data = _data

    else:
      assert (isinteger(size) and size>0), "size must be positive integer!"
      self._size = size
      if time is None:
        time = 0.0
      self._time = time
      self._sequential = sequential # the default until either self.create_sequential or self.create_parallel is called
      if self._sequential:
        self.data = <double*> calloc(self._size, sizeof(double))
      else:
        self.data = NULL
      #
    #

  def __dealloc__(self):
    """
    """
    if self.data != NULL:
      free(self.data)


  def __init__(self, size=None, sequential=True, data_file=None, state=None, **kwargs):
    """
    """
    pass

  def is_sequential(self):
    return self._sequential

  cdef int c_create_sequential(self, double init_val) except -1:
    """
    """
    cdef int out, i
    self._sequential = True
    if self.data == NULL:
      self.data = <double*> calloc(self._size, sizeof(double))
    #
    if init_val == 0.0:
      # self.data = <double*> calloc(self._size, sizeof(double))
      if not self.data:
        # raise MemoryError("Failed to allocate memory for self.data")
        out = 1
      else:
        out = 0
    else:
      # self.data = <double*> malloc(self._size * sizeof(double))
      if not self.data:
        # raise MemoryError("Failed to allocate memory for self.data")
        out = 1
      else:
        for i in range(self._size):
          self.data[i] = init_val
        out = 0
    return out

  cpdef int create_sequential(self, double init_val=0.0):
    """
    """
    cdef int out
    out = self.c_create_sequential(init_val)
    return out
  createSequential = create_sequential


  cpdef int create_parallel(self,  double init_val=0.0):
    """
    Will depend on how to incorporate PETSc...
    """
    raise NotImplementedError
  createParallel = create_parallel


  # TODO: Enable reading/writing with time; convert to/from old format from/to new format (with time)


  cpdef write_to_file(self, file_name, file_ext='dlvec'):
    """
    Write data DLidarVec to binary file
    """
    files_ext = file_ext.strip(' .')
    _, passed_ext = os.path.splitext(file_name)
    if len(passed_ext.strip()) == 0:
      file_name += ".%s" % file_ext
    else:
      pass

    if isinstance(file_name, str):
      file_name = str.encode(file_name)
    # TODO: Add time to the file (both I/O)
    cdef int sz, seq
    _sz = <int> self._size
    _tm = <double> self._time
    _seq = <int> self._sequential

    if self._sequential:
      _seq = 1
      out = vec_io.write_DLidar_Vec(file_name, _sz, _tm, _seq, self.data)
      if out:
        print("Failed to write data to file %s" % file_name)
        raise IOError

    else:
      print("NonSequential IO is not Supported yet!")
      raise NotImplementedError

  cpdef read_from_file(self, file_name):
    """
    Write data DLidarVec to binary file
    """
    if python_version >= (3, 0, 0):
      if isinstance(file_name, bytes):
        pass
      elif isinstance(file_name, str):
        file_name = str.encode(file_name)
      else:
        print("file_name must be either string or bytes; received %s" %type(file_name))
        raise TypeError
    if not os.path.isfile(file_name):
      print("File Not FOUND!")
      print("No such file: %s " % file_name)
      raise IOError
    # read everything from file; ignore size, sequential if passed!
    cdef int _sequential
    out = vec_io.read_DLidar_Vec(file_name, <long*>self._size, &self._time, &_sequential, self.data)
    if out:
      print("Failed to read data from file %s" % file_name)
      raise IOError
    # self._sequential = <bint> _sequential
    self._sequential = _sequential

  def read_from_file_OldFormat(self, file_name):
    """
    Read saved files without time
    """
    if python_version >= (3, 0, 0):
      if isinstance(file_name, bytes):
        pass
      elif isinstance(file_name, str):
        file_name = str.encode(file_name)
      else:
        print("file_name must be either string or bytes; received %s" %type(file_name))
        raise TypeError
    if not os.path.isfile(file_name):
      print("File Not FOUND!")
      print("No such file: %s " % file_name)
      raise IOError
    # read everything from file; ignore size, sequential if passed!
    cdef int _sequential
    out = vec_io.read_DLidar_Vec_OldFormat(file_name, <long*>self._size, &_sequential, self.data)
    if out:
      print("Failed to read data from file %s" % file_name)
      raise IOError
    # self._sequential = <bint> _sequential
    self._sequential = _sequential


  cdef int c_set_value(self, double value) except -1:
    """
    """
    cdef int i
    cdef int out = -1
    if self.data != NULL:
      for i in range(self._size):
        self.data[i] = value
      out = 0
    return out

  cdef double c_get_value(self, int key) except -1:
    """
    Do we need check on key? Should be done on Python side!
    """
    return self.data[key]
    # return val

  cdef int c_set_values(self, int count, int* keys, double* values) except -1:
    """
    """
    cdef int i, j
    cdef int out = -1
    if self.data != NULL:
      for i in range(count):
        j = keys[i]
        self.data[j] = values[i]
      out = 0
    return out

  cdef int c_get_values(self, int count, int* keys, double* values) except -1:
    """
    """
    cdef int i, j
    cdef int out = -1
    if self.data != NULL:
      for i in range(count):
        j = keys[i]
        values[i] = self.data[j]
      out = 0
    return out

  cdef int c_set_values_np(self, np.ndarray keys, np.ndarray values) except -1:
    """
    """
    cdef int out = -1
    cdef int i, j, count = 0

    if (keys.size==0) and (values.size==self._size):
      count = self._size
      for i in range(self._size):
        self.data[i] = <double>values[i]
      out = 0

    elif keys.size != values.size:
      pass
    else:
      count = keys.size
      if self.data != NULL:
        for i in range(count):
          j = keys[i]
          self.data[j] = <double>values[i]
        out = 0
    return out

  cdef int c_get_values_np(self, np.ndarray keys, np.ndarray values) except -1:
    """
    """
    cdef int out = -1
    cdef int i, j, count = 0

    if (keys.size==0) and (values.size==self._size):
      count = self._size
      for i in range(self._size):
        values[i] = self.data[i]
      out = 0

    elif keys.size != values.size:
      pass
    else:
      if self.data != NULL:
        for i in range(count):
          j = keys[i]
          values[i] = self.data[j]
        out = 0
    return out

  def get_numpy_array(self):
    """
    Return a copy of the internal vector (underlying reference data structure) converted to Numpy ndarray.

    Returns:
        a Numpy representation of the nuderlying data state vector structure.

    """
    if self._sequential:
      state = np.empty(self._size, order='c', dtype=DTYPE_DOUBLE)
      self.c_get_values_np(np.empty(0), state)
      return state
    else:
      raise NotImplementedError
    #
  get_np_array = get_numpy_array

  def where_inf(self):
    """
    Return a numpy array with indexes corresponding to +/-Inf value

    """
    return np.where(np.isinf(self.get_numpy_array()))[0]
    #

  def where_not_inf(self):
    """
    Return a numpy array with indexes corresponding to not +/-Inf value

    """
    return np.where(~np.isinf(self.get_numpy_array()))[0]
    #

  def where_nan(self):
    """
    Return a numpy array with indexes corresponding to NaN value

    """
    return np.where(np.isnan(self.get_numpy_array()))[0]
    #

  def where_not_nan(self):
    """
    Return a numpy array with indexes corresponding to not NaN value

    """
    return np.where(~np.isnan(self.get_numpy_array()))[0]
    #


  cdef int c_copy_data(self, StateVector other) except -1:
    """
    copy self.data to other.data

    """
    cdef int i
    if self._sequential:
      for i in range(self._size):
        other.data[i] = self.data[i]
    return 0

  def copy(self):
    """
    Return a copy of the vector.

    """
    cdef int i
    if self._sequential:
      new_copy = StateVector(self._size, sequential=True, time=self._time)
      self.c_copy_data(new_copy)
    else:
      print("Parallel Copying is not Yet supported!")
      raise NotImplementedError
    return new_copy


  def __reduce__(self):
    """
    A serialization (pickling) functionality;
    This is mandatory for pickle-based MPI4PY communications
    """
    # TODO: Refactor to avoid conversion to numpy array
    return state_vector_from_np_array, (self.get_numpy_array(), self._sequential, self._time)

  cdef int c_scale(self, double alpha, StateVector X) except -1:
    """
    """
    cdef long int i
    #
    if self._sequential:
      for i in range(self._size):
        X.data[i] *= alpha
    else:
      raise NotImplementedError
    return 0

  def scale(self, alpha, in_place=True):
    """
    BLAS-like method; scale the underlying StateVector by the constant (scalar) alpha.

    Args:
        alpha: scalar
        in_place: If true scaling is applied (in-place) to self.
            If False, a new copy will be returned.

    Returns:
        The StateVector object scaled by the constant (scalar) alpha.

    """
    cdef long int i
    assert isscalar(alpha), "alpha must be a scalar not %s" % type(alpha)
    if self._sequential:
      if not in_place:
          result_vec = self.copy()
      else:
          result_vec = self
      self.c_scale(alpha, result_vec)
    else:
      raise NotImplementedError
    return result_vec
    #

  cdef double c_dot(self, StateVector X, StateVector Y) except -1:
    """
    Perform a dot product with other.

    Args:
        other: another StateVector object.

    Returns:
        The (scalar) dot-product of self with other

    """
    cdef long int i
    cdef double res
    #
    if self._sequential:
      res = 0.0
      for i in range(self._size):
        res += X.data[i] * Y.data[i]
    else:
      raise NotImplementedError
    #
    return res

  def dot(self, other):
    """
    Perform a dot product with other.

    Args:
        other: another StateVector object.

    Returns:
        The (scalar) dot-product of self with other

    """
    assert isinstance(other, StateVector), " 'other' must be an instance of StateVector, not %s" %type(other)
    assert other.size == self.size, " Size Mismatch; input vector of size %d; expected %d" %(other.size, self.size)
    res = self.c_dot(self, other)
    return res
    #

  cdef int c_add(self, StateVector X, StateVector Y) except -1:
    """
    update X += Y

    Args:
        other: another StateVector object
        in_place: If true addition is applied (in-place) to self.
            If False, a new copy will be returned.

    Returns:
        StateVector; self + other is returned

    """
    cdef long int i
    #
    if self._sequential:
      for i in range(self._size):
        X.data[i] += Y.data[i]
    else:
      raise NotImplementedError
      #
    return 0

  def add(self, other, in_place=True):
    """
    Add other to the vector.

    Args:
        other: another StateVector object
        in_place: If true addition is applied (in-place) to self.
            If False, a new copy will be returned.

    Returns:
        StateVector; self + other is returned

    """
    if self._sequential:
      if not in_place:
        result_vec = self.copy()
      else:
        result_vec = self
      #
      if isscalar(other):
        result_vec[:] += other
      else:
        assert other.size == self.size, " Size Mismatch; input vector of size %d; expected %d" %(other.size, self.size)
        self.c_add(result_vec, other)
    else:
      raise NotImplementedError
      #
    return result_vec

  cdef int c_axpy(self, double alpha, StateVector X, StateVector Y) except -1:
    """
    update X += alpha Y

    """
    cdef long int i
    #
    if self._sequential:
      for i in range(self._size):
        X.data[i] += alpha * Y.data[i]
    else:
      raise NotImplementedError
      #
    return 0

  def axpy(self, alpha, other, in_place=True):
    """
    Add other to the vector.

    Args:
        other: another StateVector object
        in_place: If true addition is applied (in-place) to self.
            If False, a new copy will be returned.

    Returns:
        StateVector; self + other is returned

    """
    assert isscalar(alpha), "'alpha' must be a scalar, not %s !" % type(alpha)
    assert other.size == self.size, " Size Mismatch; input vector of size %d; expected %d" %(other.size, self.size)
    if self._sequential:
      if not in_place:
        result_vec = self.copy()
      else:
        result_vec = self
      self.c_axpy(<double>alpha, result_vec, other)
    else:
      raise NotImplementedError
      #
    return result_vec

  def subtract(self, other, in_place=True):
    """
    Subtract other from self

    Args:
        other: another StateVector object
        in_place: If true addition is applied (in-place) to self.
            If False, a new copy will be returned.

    Returns:
        StateVector; self - other is returned

    """
    if self._sequential:
      if not in_place:
        result_vec = self.copy()
      else:
        result_vec = self
      #
      if isscalar(other):
        result_vec[:] -= other
      else:
        result_vec.axpy(-1, other, in_place=True)
    else:
      raise NotImplementedError
    return result_vec

  cdef int c_reciprocal(self, StateVector X) except -1:
    """
    Evaluate 1/X

    """
    cdef long int i
    #
    if X._sequential:
      for i in range(X._size):
        X.data[i] = 1.0 / X.data[i]
    else:
      raise NotImplementedError
      #
    return 0

  cdef int c_multiply(self, StateVector X, StateVector Y) except -1:
    """
    update X *= alpha Y

    """
    cdef long int i
    #
    if self._sequential:
      for i in range(self._size):
        X.data[i] *= Y.data[i]
    else:
      raise NotImplementedError
      #
    return 0

  def multiply(self, other, in_place=True):
    """
    Return point-wise multiplication with other

    Args:
        other: another StateVector object
        in_place: If true addition is applied (in-place) to self.
            If False, a new copy will be returned.

    Returns:
        StateVector; self * other is returned

    """
    if self._sequential:
      if isscalar(other):
        result_vec = self.scale(other, in_place=in_place)
      else:
        assert other.size == self.size, " Size Mismatch; input vector of size %d; expected %d" %(other.size, self.size)
        if not in_place:
          result_vec = self.copy()
        else:
          result_vec = self
        #
        self.c_multiply(result_vec, other)
    else:
      raise NotImplementedError
      #
    return result_vec

  cdef int c_divide(self, StateVector X, StateVector Y) except -1:
    """
    Update X /= Y

    """
    cdef long int i
    #
    if self._sequential:
      for i in range(self._size):
        X.data[i] /= Y.data[i]
    else:
      raise NotImplementedError
      #
    return 0

  def divide(self, other, in_place=True):
    """
    Return point-wise divisioin with other

    Args:
        other: another StateVector object
        in_place: If true addition is applied (in-place) to self.
            If False, a new copy will be returned.

    Returns:
        StateVector; self / other is returned

    """
    if self._sequential:
      if isscalar(other):
        result_vec = self.scale(1.0/other, in_place=in_place)
      else:
        assert other.size == self.size, " Size Mismatch; input vector of size %d; expected %d" %(other.size, self.size)
        if not in_place:
          result_vec = self.copy()
        else:
          result_vec = self
        #
        self.c_divide(result_vec, other)
    else:
      raise NotImplementedError
      #
    return result_vec

  cpdef double norm2(self, bint ignore_nan=False):
    """
    Return the 2-norm of the StateVector (self).

    """
    cdef long int i
    cdef double res, val
    #
    if self._sequential:
      res = 0.0
      if ignore_nan:
        non_nans_inds = self.where_not_nan()
        if non_nans_inds.size == 0:
          res = np.NaN  # to highlight that all entries are NaN
        else:
          for i in non_nans_inds:
            val = self.data[i]
            res += val ** 2
          res = res ** (0.5)
      else:
        for i in range(self._size):
          res += self.data[i] ** 2
        res = res ** (0.5)
    else:
      raise NotImplementedError
    return res
    #

  cpdef double sum(self):
    """
    Return sum of entries of the vector

    """
    cdef long int i
    cdef double res
    #
    if self._sequential:
      res = 0.0
      for i in range(self._size):
        res += self.data[i]
    else:
      raise NotImplementedError
    return res
    #

  cpdef double mean(self):
    """
    Return average of entries of the vector

    """
    avg = self.sum() / self.size
    return avg

  cpdef double variance(self, double ddof=1.0):
    """
    Return variance of entries of the vector

    """
    cdef double avg, var
    cdef long int i
    avg = self.mean()
    for i in range(self.size):
      # var += pow(self.__getitem__(i)-avg, 2)
      var += pow(self.data[i]-avg, 2)
    var /= (self.size-ddof)
    return var

  def var(self, ddof=1.0):
    """
    Wrapper around self.variance()
    """
    return self.variance(ddof)

  cpdef double stdev(self, double ddof=1.0):
    """
    Return variance of entries of the vector

    """
    std = sqrt(self.variance())
    return std

  def standard_deviatioin(self, ddof=1.0):
    """
    Wrapper around self.stdev()
    """
    return self.stdev(ddof)

  cpdef double max(self):
    """
    Return maximum of entries of the vector

    """
    cdef long int i
    cdef double res
    #
    if self._sequential:
      res = self.data[0]
      for i in range(1, self._size):
        if self.data[i] > res:
          res = self.data[i]
    else:
      raise NotImplementedError
    return res
    #

  cpdef double min(self):
    """
    Return minimum of entries of the vector

    """
    cdef long int i
    cdef double res
    #
    if self._sequential:
      res = self.data[0]
      for i in range(1, self._size):
        if self.data[i] < res:
          res = self.data[i]
    else:
      raise NotImplementedError
    return res
    #

  cdef int c_power(self, StateVector X, double p) except -1:
    """
    """
    cdef long int i
    #
    if self._sequential:
      for i in range(self._size):
        X.data[i] = pow(X.data[i], p)
    else:
      raise NotImplementedError
    return 0

  def power(self, exponent, in_place=True):
    """
    Evaluate power of all entries.

    Args:
      exponent: power to raise to
      in_place: If true the squares are evaluated (in-place) to self.
          If False, a new copy will be returned.

    Returns:
      the power (of all entries) of the StateVector (self)

    """
    assert isscalar(exponent), "The exponent must be  a scalar; received %s " % type(exponent)
    #
    if self._sequential:
      if not in_place:
        result_vec = self.copy()
      else:
        result_vec = self
      self.c_power(result_vec, exponent)
    else:
      raise NotImplementedError
    return result_vec

  def square(self, in_place=True):
    """
    Evaluate square of all entries.

    Args:
      in_place: If true the squares are evaluated (in-place) to self.
          If False, a new copy will be returned.

    Returns:
      the square (of all entries) of the StateVector (self)

    """
    return self.power(2,  in_place=in_place)

  def sqrt(self, in_place=True):
    """
    Evaluate square root of all entries.

    Args:
      in_place: If true the squares are evaluated (in-place) to self.
          If False, a new copy will be returned.

    Returns:
      the square (of all entries) of the StateVector (self)

    """
    return self.power(0.5,  in_place=in_place)

  cdef int c_abs(self, StateVector X) except -1:
    """
    """
    cdef long int i
    #
    if self._sequential:
      for i in range(self._size):
        X.data[i] = abs(X.data[i])
    else:
      raise NotImplementedError
    return 0

  def abs(self, in_place=True):
    """
    Evaluate square root of all entries.

    Args:
      in_place: If true the squares are evaluated (in-place) to self.
          If False, a new copy will be returned.

    Returns:
      the square (of all entries) of the StateVector (self)

    """
    cdef long int i
    if self._sequential:
      if not in_place:
        result_vec = self.copy()
      else:
        result_vec = self
      self.c_abs(result_vec)
    else:
      raise NotImplementedError
    return result_vec

  def reciprocal(self, in_place=False):
    """
    Return a copy (or self) with x replaced by 1/x for each entry of self
    """
    # TODO: can be replace with self.power(-1)!
    if self._sequential:
      if not in_place:
        result_vec = self.copy()
      else:
        result_vec = self
      self.c_reciprocal(result_vec)
    else:
      raise NotImplementedError
    return result_vec


  # Properties' Setters, and Getters:
  # ------------------------------------
  #
  @property
  def size(self):
    """
    Get the size of the state vector.

    """
    return self._size
    #
  @size.setter
  def size(self, value):
    """
    set the size of state vector. This should not be allowed to be updated by users.

    """
    print("Can't change model state vector after initialization!")
    raise NotImplementedError
    # self._size = value
    #

  #
  @property
  def time(self):
    """
    Get the time at which this state is generated (for time-dependent models).
    """
    return self._time
    #
  @time.setter
  def time(self, value):
    """
    Set the time at which this state is generated (for time-dependent models)
    """
    if value is None:
      value= 0
    self._time = value
    #
  #

  cdef int c_get_slice(self, int start, int stop, int step, np.ndarray state) except -1:
    """
    """
    cdef long size, k, ind = 0
    cdef int out = 0
    if start>=stop:
      out = 0
    else:
      if ((stop-start)%step)==0:
        size = int( (stop-start)/step )
      else:
        size = int( (stop-start)/step ) + 1

      if (size != state.size):
        print("sizes: size, state.size: %d %d" %(size, state.size))
        out = -1
      else:
        ind = 0
        for k in range(start, stop, step):
          state[ind] = self.data[k]
          ind += 1
    return out

  def get_slice(self, i, j):
    """
    """
    return self.__getitem__(slice(i, j))

  cdef int c_set_slice(self, int start, int stop, int step, np.ndarray state) except -1:
    """
    """
    cdef long size, k, ind = 0
    cdef int out = 0
    if start>=stop:
      out = 0
    else:
      if ((stop-start)%step)==0:
        size = int( (stop-start)/step )
      else:
        size = int( (stop-start)/step ) + 1

      if (size != state.size):
        print("sizes: size, state.size: %d %d" %(size, state.size))
        out = -1
      elif size == 0:
        print("WARNING: DLidarVec; setting empty slice...")
      else:
        ind = 0
        for k in range(start, stop, step):
          # print("Setting from %d to %d with step %d in size %d" % (start, stop, step, self.size))
          self.data[k] = <double>state[ind]
          ind += 1
    return out

  def set_slice(self, i, j, sequence):
    """
    """
    self.__setitem__(slice(i, j), sequence)

  #
  # Emulate Python Descriptors/Decorators:
  # --------------------------------------
  def __StringRepresentation__(self):
    """
    Create a string representation of self
    """
    max_length = 200  # gotta be even
    vals_per_line = 5
    if self.size > max_length:
      out = "DLidarVec.StateVector \nData-Type:%s \nData:[" % DTYPE_DOUBLE
      for i in range(max_length//2):
        if (i % vals_per_line) == 0:
          out += "\n  "
        out += "%10.8g, " % self.data[i]
      out += "\n  ...... \n  "
      init_ind = self.size-(max_length//2) - 1
      for i in range(init_ind, self.size-1):
        if (i % vals_per_line) == 0:
          out += "\n  "
        out += "%10.8g, " % self.data[i]
      out += "%10.8g \n]" % self.data[self.size-1]
    else:
      #
      out = "DLidarVec.StateVector \nData-Type:%s \nData:[" % DTYPE_DOUBLE
      for i in range(self.size-1):
        if (i % 5) == 0:
          out += "\n  "
        out += "%10.8g, " % self.data[i]
      out += "%10.8g\n]" % self.data[self.size-1]
    return out

  def __repr__(self):
    return self.__StringRepresentation__()

  def __str__(self):
    return self.__StringRepresentation__()

  def __iter__(self):
    for i in range(self._size):
      yield self.data[i]

  def __len__(self):
    return self._size

  # Python Unary operators:
  # -----------------------
  def __pos__(self):
    """
    Return +x (copy of self)
    """
    return self.copy()
  def __neg__(self):
    """
    Return copy with entries negated
    """
    return self.scale(-1, in_place=False)
  def __abs__(self):
    """
    Return copy with absolute value of entries
    """
    return self.abs(in_place=False)

  # Python Binary operators
  # -----------------------
  def __add__(self, other):
    return self.add(other, in_place=False)
  def __iadd__(self, other):
    return self.add(other, in_place=True)

  def __sub__(self, other):
    return self.subtract(other, in_place=False)
  def __isub__(self, other):
    return self.subtract(other, in_place=True)

  def __mul__(self, other):
    return self.multiply(other, in_place=False)
  def __imul__(self, other):
    return self.multiply(other, in_place=True)

  def __div__(self, other):  # __div/idiv__ for python2.x; truediv/itruediv__ for python 3.x
    return self.divide(other, in_place=False)
  def __idiv__(self, other):
    return self.divide(other, in_place=True)
  def __truediv__(self, other):
    return self.divide(other, in_place=False)
  def __itruediv__(self, other):
    return self.divide(other, in_place=True)


  # Python slicing operators:
  # -------------------------
  def __getitem__(self, key):
    if isinstance(key, slice):
      start = 0 if key.start is None else key.start
      stop = self._size if key.stop is None else key.stop
      step = 1 if key.step is None else key.step

      start = self._size+start if start <0 else start
      stop = self._size+stop if stop <0 else stop
      stop = min(stop, self._size)

      if (start <0 or start > self._size-1):
        out= np.empty(0, order='c', dtype=DTYPE_DOUBLE)
      elif (stop <0 or stop > self._size):
        out= np.empty(0, order='c', dtype=DTYPE_DOUBLE)
      else:
        # start, stop are non -ve
        if start < stop:
          if step >0:
            size = (stop-start)/step if (stop-start)%step==0 else (stop-start)/step+1
            size = int(size)
            out = np.empty(size, order='c', dtype=DTYPE_DOUBLE)
            res = self.c_get_slice(start, stop, step, out)
          else:
            out= np.empty(0, order='c', dtype=DTYPE_DOUBLE)
            #
        elif start > stop:
          if step >0:
            out= np.empty(0, order='c', dtype=DTYPE_DOUBLE)
          else:
            size = abs((stop-start)/step) if abs((stop-start)%step)==0 else abs((stop-start)/step)+1
            size = int(size)
            print("\n\n\nAccessing: stop -> start: abs(step) with size[%d] with xsize:%d (%d, %d, %d)\n\n\n" % (size, self._size, stop, start, abs(step)))
            out = np.empty(size, order='c', dtype=DTYPE_DOUBLE)
            k = abs(step)
            res = self.c_get_slice(stop, start, k, out)
            if not res:
              out = np.flip(out, 0)
            else:
              print("\n\nSORRY: Failed to read with given range! DEBUGGING is required with start=%d, stop=%d, step=%d\n\n" %(start, stop, step))
              raise ValueError
        else:
          out = np.empty(0, order='c', dtype=DTYPE_DOUBLE)

    elif isinstance(key, np.ndarray):
      if not issubclass(key.dtype.type, np.integer):
        print("The array of indexes must be of type integer")
        raise TypeError
      out = np.empty(key.size)
      for i, ind in enumerate(key):
        out[i] = self.data[ind]

    elif isinteger(key):
      _key = key
      if -self._size <= _key < 0:
        _key += self._size
      elif 0 <= _key < self._size:
        pass
      else:
        print("index %d is out of bound 0:%d" %(_key, self._size-1))
        raise IndexError
      out = self.data[_key]
    else:
      print("key must be either a slice or an integer!")
      print("Received: ", key, type(key))
      raise TypeError
    return out

  def __setitem__(self, key, value):
    if isinstance(key, slice):
      start = 0 if key.start is None else key.start
      stop = self._size if key.stop is None else key.stop
      step = 1 if key.step is None else key.step

      start = self._size+start if start <0 else start
      stop = self._size+stop if stop <0 else stop
      stop = min(stop, self._size)

      if (start <0 or start > self._size-1):
        # out= np.empty(0, order='c', dtype=DTYPE_DOUBLE)
        pass
      elif (stop <0 or stop > self._size):
        # out= np.empty(0, order='c', dtype=DTYPE_DOUBLE)
        pass
      else:
        # start, stop are non -ve
        if start < stop:
          if step >0:
            size = (stop-start)/step if (stop-start)%step==0 else (stop-start)/step+1
            size = int(size)
            if isscalar(value):
              seq = np.ones(size)*value
            else:
              seq = np.asarray(value).flatten()
            if size != seq.size:
              print("passed value and sequence are not conformable! Can't prodcast arrays with sizes %d, %d" %(size, seq.size))
              print("seq, ", seq)
              print(self.size)
              print("start, stop, step", start, stop, step)
              raise ValueError
            res = self.c_set_slice(start, stop, step, seq)
            if res:
              print("\n\nSORRY: Failed to read with given range! DEBUGGING is required with start=%d, stop=%d, step=%d\n\n" %(start, stop, step))
              raise ValueError
          else:
            # out= np.empty(0, order='c', dtype=DTYPE_DOUBLE)
            pass
            #
        elif start > stop:
          if step >0:
            # out= np.empty(0, order='c', dtype=DTYPE_DOUBLE)
            pass
          else:
            size = abs((stop-start)/step) if abs((stop-start)%step)==0 else abs((stop-start)/step)+1
            size = int(size)
            if isscalar(value):
              seq = np.ones(size)*value
            else:
              seq = np.flip(np.asarray(value).flatten(), 0)
            if size != seq.size:
              print("passed value and sequence are not conformable!")
              raise ValueError

            k = abs(step)
            res = self.c_set_slice(stop, start, k, seq)
            if res:
              print("\n\nSORRY: Failed to read with given range! DEBUGGING is required with start=%d, stop=%d, step=%d\n\n" %(start, stop, step))
              raise ValueError
        else:
          # out = np.empty(0, order='c', dtype=DTYPE_DOUBLE)
          pass

    elif isinteger(key):
      _key = key
      if -self._size <= _key < 0:
        _key += self._size
      elif 0 <= _key < self._size:
        pass
      else:
        print("index %d is out of bound 0:%d" %(_key, self._size-1))
        raise IndexError
      self.data[_key] = <double>value

    elif isinstance(key, np.ndarray):
      assert (isscalar(value) or isiterable(value)), "The pased value must be either a scalar, or an iterable of equal size as key!"
      if not issubclass(key.dtype.type, np.integer):
        print("The array of indexes must be of type integer")
        raise TypeError
      if isscalar(value):
        for ind in key:
          self.data[ind] = value
      elif isiterable(value):
        if len(value) != key.size:
          print("The dimension of the pased value must be of equal size as key")
          print("Failed to broadcast %d values into %d locations" % (len(value), key.size) )
          raise ValueError

        for i, ind in enumerate(key):
          self.data[ind] = value[i]

    else:
      print("key must be either a slice, an integer, or a one dimensional numpy array of integers!")
      print("Received: ", key, type(key))
      raise TypeError

  def __delitem__(self, key):
    raise NotImplementedError("Deleting an item from a StateVector is Not Supported!")

  def __contains__(self, item):
    # This function shouldn't be developed; do we really need it!
    raise NotImplementedError("Looking inside the vector can be very expensive; would develop if needed!")

  # def __getslice__(self, i, j):
  #   return self.get_slice(i, j)
  #
  # def __setslice__(self, i, j, sequence):
  #   self.set_slice(i, j, sequence)
  #
  # def __delslice__(self, i, j):
  #   raise NotImplementedError("Not Allowed Operation!")

  #
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#  A collection of StateVector instances, with access to ensemble-related functionalities;   #
#  this includes; ensemble mean, variance, inflation, etc.                                   #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
cdef class Ensemble(object):
  """
  A class implementing an ensemble of StateVector objects;
  constructor __init__ --> __cinit__ with arguments as below

  Args:
    ensemble_size
    vec_size
    sequential (default True)
    iniit_val (default 0.0)

  """
  def __cinit__(self, ensemble_size, vec_size, sequential=True, init_val=0.0, **kwargs):
    assert (isinteger(vec_size) and vec_size>0), "vec_size must be positive integer!"
    assert (isinteger(ensemble_size) and ensemble_size>=0), "ensemble_size must be a non-negative integer!"
    self._vec_size = vec_size
    self._ensemble_size = ensemble_size
    self._sequential = sequential # the default until either self.create_sequential or self.create_parallel is called
    self.data = {}

    cdef StateVector v
    cdef int i

    for i in range(ensemble_size):
      v = StateVector(vec_size, sequential=sequential)
      if sequential:
        v.create_sequential(init_val)
      else:
        v.create_parallel(init_val)

      self.data.update({i: v.copy()})
      #

  def __dealloc__(self):
    """
    """
    cdef int i
    for i in range(self._ensemble_size):  # just in-case!
      try:
        self.data[i].__dealloc__()
      except:
        pass


  def __init__(self, ensemble_size, vec_size, sequential=True, init_val=0.0, **kwargs):
    """
    Args:
      size: positive integer
    """
    pass

  def is_sequential(self):
    return self._sequential

  def copy(self):
    """
    return a copy of the ensemble
    """
    copied_ensemble = Ensemble(0, self._vec_size, sequential=self._sequential)
    for i in range(self.size):
      copied_ensemble.append(self.data[i].copy())
    return copied_ensemble

  def copy_empty(self):
    """
    Return a copy of the ensemble with no states in it, however preserve all settings.

    """
    copied_ensemble = Ensemble(0, self._vec_size, sequential=self._sequential)
    return copied_ensemble

  # List operations (append, pop, insert):
  def pop(self, key=None):
    """
    remove, and return the entry at the given key (last if not give) from the ensemble, and update ensemble size
    """
    if self.size == 0:
      print(" The ensemble is empty")
      raise ValueError

    if key is None:
      key = self.size - 1

    if 0 <= key < self.size:
      vec = self.data[key]  # TODO: test if copying is needed
      for i in range(key, self.size-1):
        self.data[i] = self.data[i+1]
      del self.data[self.size-1]
    else:
      print("Can't remove element at the given index %s" % str(key))
      raise IndexError
    self._ensemble_size -= 1
    return vec

  def insert(self, vec, key):
    """
    insert a copy of vec in the given location of the ensemble, and update ensemble size
    """
    assert isinstance(vec, StateVector), "passed vec must be in instance of StateVector not %s " %type(vec)
    if vec.size != self._vec_size:
      print("Vectors added must be of equal sizes!")
      print("Expected vector of size %d; Received vector of size %d" % (self._vec_size, vec.size))
      raise ValueError
    if vec.is_sequential() is not self._sequential:
      print("Vectors added must be of same type; either sequential or parallel, not mixed")
      raise ValueError

    if 0 <= key < self.size:
      self.data.update({self.size:self.data[self.size-1]})
      i = self.size - 1
      while i>key:
        self.data[i] = self.data[i-1]
        i -= 1
      self.data[key]= vec.copy()
    elif key == self.size:
      key = self.size
      self.data.update({key: vec.copy()})
    else:
      print("Can't insert at the given index %s" % str(key))
      raise ValueError
    self._ensemble_size += 1

  def append(self, vec):
    """
    append a copy of vec to the end of the ensemble, and update ensemble size
    """
    self.insert(vec, self.size)


  # Statistical operations (mean, variance, stdev, inflate):
  cpdef StateVector mean(self):
    """
    """
    if self.size == 0:
      print("The ensemble is empty; Can't evaluate the mean!")
      raise ValueError
    cdef int i
    cdef StateVector v
    v = self.data[0].copy()
    for i in range(1, self._ensemble_size):
      v.add(self.data[i], in_place=True)
    v.scale(1.0/self.size, in_place=True)
    return v

  cpdef StateVector variance(self, int ddof=1):
    """
    """
    if self.size == 0:
      print("The ensemble is empty; Can't evaluate the variances!")
      raise ValueError
    # Sum of squared deviations
    cdef int i
    cdef StateVector avg, sqr_devs, sqr_devs_sum, var

    sqr_devs_sum = StateVector(self._vec_size, sequential=self._sequential)
    if self._sequential:
      sqr_devs_sum.create_sequential(0.0)
    else:
      sqr_devs_sum.create_parallel(0.0)

    avg = self.mean()
    for i in range(self._ensemble_size):
      sqr_devs = self.data[i].axpy(-1.0, avg, in_place=False)
      sqr_devs.abs(in_place=True).square(in_place=True)
      sqr_devs_sum.add(sqr_devs, in_place=True)
    var = sqr_devs_sum.scale(1.0/(self.size-ddof), in_place=True)
    return var

  cpdef StateVector stdev(self, int ddof=1):
    """
    """
    if self.size == 0:
      print("The ensemble is empty; Can't evaluate standard deviations!")
      raise ValueError
    cdef StateVector std
    std = self.variance(ddof=ddof)
    std = std.sqrt(in_place=True)
    return std

  cpdef Ensemble inflate(self, inflation_fac, in_place=True):
    """
    """
    cdef int i
    cdef StateVector avg, dev

    if not in_place:
      inflated_ensemble = self.copy()
    else:
      inflated_ensemble = self

    if inflated_ensemble.size > 1:
      avg = inflated_ensemble.mean()
      #
      if isscalar(inflation_fac):
        if inflation_fac != 1:
          for i in range(inflated_ensemble.size):
            inflated_ensemble[i].axpy(-1.0, avg, in_place=True)
            inflated_ensemble[i].scale(inflation_fac, in_place=True)
            inflated_ensemble[i].add(avg, in_place=True)

      elif isiterable(inflation_fac):
        assert len(inflation_fac)==self._vec_size, "space-dependent/iterable inflation factor must be of size %d; passed vector of size %d"  % (self._vec_size, len(inflation_fac))
        #
        for i in range(inflated_ensemble.size):
          inflated_ensemble[i].axpy(-1.0, avg, in_place=True)
          # TODO: Test timing vs. inflated_ensemble[i][:] *= inflation_fac[:]
          if isinstance(inflation_fac, StateVector):
            inflated_ensemble[i] *= inflation_fac
          else:
            inflated_ensemble[i][:] *= inflation_fac[:]
          inflated_ensemble[i].add(avg, in_place=True)
      else:
        print("inflation_fac must be either a scalar, or a valid Python iterable")
        raise TypeError
    return inflated_ensemble

  cpdef Ensemble extend(self, other, in_place=True):
    """
    Expand the ensmble be adding (copies) of elements from other

    Args:
      self:
      other:
      in_place:

    Returns:
      result_ensemble:

    """
    assert isinstance(other, Ensemble), "The passed ensemble must be an instance of DLidarVec.Ensemble; received %s" % type(other)
    assert other.state_size == self.state_size, "State vector size mismatch mine vs. passed (%d, %d)" % (self.state_size, other.state_size)

    if not in_place:
      result_ensemble = self.copy()
    else:
      result_ensemble = self
    
    for e in other:
      result_ensemble.append(e)

    return result_ensemble

  def get_numpy_array(self):
    """
    Return a copy of the internal set of vectors converted to an aggreagated Numpy ndarray.

    Returns:
        state_vec x ensemble_size  Numpy representation of the nuderlying state vectors.

    """
    ensemble_np = np.empty((self._vec_size, self.size))
    for i in range(self.size):
      ensemble_np[:, i] = self.data[i].get_numpy_array()
    return ensemble_np
    #
  get_np_array = get_numpy_array


  cpdef write_to_file(self, directory, file_name_prefix, files_ext='dlvec'):
    """
    Write the state vectors within this ensemble to file.
    files are postfixed with ensemble index; from 0 to ensemble_size-1
    
    Args:
      directory
      file_name_prefix
      files_ext

    """
    files_ext = files_ext.strip(' .')
    file_name_prefix, _ = os.path.splitext(file_name_prefix)
    if self._sequential:
      if not os.path.isdir(directory):
        os.makedirs(directory)
      for ens_ind in range(self._ensemble_size):
        file_name = "%s_%d.%s" % (file_name_prefix, ens_ind, files_ext)
        file_path = os.path.join(directory, file_name)
        self.data[ens_ind].write_to_file(file_path)
    else:
      print("NonSequential IO is not Supported yet!")
      raise NotImplementedError

  cpdef Ensemble read_from_file(self, directory, file_name_prefix, max_ens_size=150):
    """
    Create an Ensemble object, by reading form files with a predefined file_name_prefix

    Args:
      file_name_prefix: <file_name_prefix>_num.dlvec is the name of files to look for
      directory: location of lidar vectors
      max_ens_size: how many ensembles to look for

    Returns:
      ensemble: an instance of DLidarVec.Ensmble with instances of DLidarVec.StateVector

    """
    files_ext = 'dlvec'
    file_name_prefix, _ = os.path.splitext(file_name_prefix)

    if not os.path.isdir(directory):
      print("No such directory: %s" % directory)
      raise IOError

    ens_size = 0
    # get initial vector
    ens_ind = 0
    file_name = "%s_%d.%s" % (file_name_prefix, ens_ind, files_ext.strip(' .'))
    file_path = os.path.join(directory, file_name)
    if not os.path.isfile(file_path):
      print("Couldn't find any matching ensemble members to the pattern 'prefix_ensind_.dlvec' ")
      raise IOError
    # Good to start reading, at least one ensemble member
    vec = StateVector(data_file=file_path)
    ensemble = Ensemble(0, vec.size, sequential=vec.is_sequential())
    ensemble.append(vec)
    while True:
      ens_ind += 1
      file_name = "%s_%d.%s" % (file_name_prefix, ens_ind, files_ext.strip(' .'))
      file_path = os.path.join(directory, file_name)
      if os.path.isfile(file_path):
        vec = StateVector(data_file=file_path)
        ensemble.append(vec)
        if ensemble.size == max_ens_size:
          print("Maximum ensemble size of %d reached, terminating..." % max_ens_size)
          break
      else:
        break

    return ensemble

  def __StringRepresentation__(self):
    """
    Create a string representation of self
    """
    msg = """%s
    An Ensemble of DLidarVec.StateVector instances.
    Ensemble Size (Number of state vectors):  %d
    State  Vector Size:  %d
    """ % (type(self), self.size, self.state_size)
    return msg
  #
  @property
  def size(self):
    """
    Get the size of the state vector.

    """
    return self._ensemble_size
    #
  @size.setter
  def size(self, value):
    """
    set the size of state vector. This should not be allowed to be updated by users.

    """
    print("To change ensemble_size, you need to use; append, pop, insert!")
    raise TypeError

  @property
  def state_size(self):
    """
    The size of the state vectors stored in the ensemble
    """
    return self._vec_size
    #
  @state_size.setter
  def state_size(self, value):
    print("To change state_size, you MUST create a new instance. Only homogenous states are allowed here")
    raise TypeError

  def __iter__(self):
    for i in range(self._ensemble_size):
      yield self.data[i]

  def __getitem__(self, key):
    if key<0:
      key += self.size
    if 0 <= key< self.size:
      return self.data[key]
    else:
      print("Can't access passed index! %s" % str(key))
      print("Ensemble size is [%d]" % self.size)
      raise IndexError

  def __setitem__(self, key, vec):
    if key<0:
      key += self.size
    if 0 <= key< self.size:
      assert isinstance(vec, StateVector), "To assign an entry in Ensemble instance, you must pass a StateVector object"
      assert (vec.size == self._vec_size), "Passed StateVector must be of size %d not %d " % (self._vec_size, vec.size)
      self.data[key]= vec
    else:
      print("Can't access passed index! %s" % str(key))
      print("Ensemble size is [%d]" % self.size)
      raise IndexError

  def __len__(self):
    return self.size

  def __repr__(self):
    return self.__StringRepresentation__()
  def __str__(self):
    return self.__StringRepresentation__()
  #
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
