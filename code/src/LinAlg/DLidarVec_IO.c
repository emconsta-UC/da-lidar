/* Functions to Read and Write DLidarVec information to/from file */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int write_DLidar_Vec(char *file, long size, double state_time, int sequential, double *data)
{

  size_t bytes;


  /* Write to file in the format HyPar wants */
  FILE *out;

  out = fopen(file,"wb");
  if (!out){
    fprintf(stdout, "Error: File '%s' Couldn't be open for output! Terminating!\n", file);
  	return(1);
  }else{
    bytes = fwrite((const void*) &size, sizeof(long), 1, out);
    if ((int)bytes != 1) {
      fprintf(stdout, "Error in write_DLidar_Vec(): Unable to write size. Expected %d, Written %ld.\n",
              1, (long)bytes);
    }
    bytes = fwrite((const void*) &state_time, sizeof(double), 1, out);
    if ((int)bytes != 1) {
      fprintf(stdout, "Error in write_DLidar_Vec(): Unable to write state_time. Expected %d, Written %ld.\n",
              1, (long)bytes);
    }
    bytes = fwrite((const void*) &sequential, sizeof(int), 1, out);
    if ((int)bytes != 1) {
      fprintf(stdout, "Error in write_DLidar_Vec(): Unable to write sequential. Expected %d, Written %d.\n",
              1, (int)bytes);
    }

    bytes = fwrite(data, sizeof(double), size, out);
    if ((long)bytes != size) {
      fprintf(stdout, "Error in write_DLidar_Vec(): Unable to write the DLidarVec.data. Expected %ld, Written %ld.\n",
              size, (long)bytes);
      fclose(out);
      return(-1);
    }

  }

  fclose(out);

  /* Done */

	return(0);
}


int alloc_and_read_DLidar_Vec_OldFormat(char *file, long *size, int *sequential, double **data)
{
  long data_size;

  FILE *in;
  in = fopen(file,"rb");
  if (!in){
    fprintf(stdout, "Error: File '%s' Couldn't be open for input! Terminating!\n", file);
  	return(1);
  }else{
    /* fprintf(stdout, "Reading DLidarVec from binary file %s (Serial mode).\n",file); */
    size_t bytes;

    /* Read sequential, and size */
    fread(size, sizeof(long), 1, in);
    fread(sequential, sizeof(int), 1, in);

    data_size = *size;

    /* allocate global solution array */
    *data = calloc(data_size, sizeof(double));

    if (*data == NULL){fprintf(stdout, "FAILED to allocate data!\n");}

    /* read data */
    bytes = fread(*data, sizeof(double), data_size, in);
    if ((long)bytes != data_size) {
      fprintf(stdout, "Error in ReadArray(): Unable to read DLidarVec.data . Expected %ld, Read %ld.\n",
              data_size, (long)bytes);
      fclose(in);
      return(-1);
    }

  }

  fclose(in);

	return(0);
}

int alloc_and_read_DLidar_Vec(char *file, long *size, double *state_time, int *sequential, double **data)
{
  long data_size;

  FILE *in;
  in = fopen(file,"rb");
  if (!in){
    fprintf(stdout, "Error: File '%s' Couldn't be open for input! Terminating!\n", file);
  	return(1);
  }else{
    /* fprintf(stdout, "Reading DLidarVec from binary file %s (Serial mode).\n",file); */
    size_t bytes;

    /* Read sequential, and size */
    fread(size, sizeof(long), 1, in);
    fread(state_time, sizeof(double), 1, in);
    fread(sequential, sizeof(int), 1, in);

    data_size = *size;

    /* allocate global solution array */
    *data = calloc(data_size, sizeof(double));

    if (*data == NULL){fprintf(stdout, "FAILED to allocate data!\n");}

    /* read data */
    bytes = fread(*data, sizeof(double), data_size, in);
    if ((long)bytes != data_size) {
      fprintf(stdout, "Error in ReadArray(): Unable to read DLidarVec.data . Expected %ld, Read %ld.\n",
              data_size, (long)bytes);
    fclose(in);
    return(-1);
    }

  }

  fclose(in);

	return(0);
}

int read_DLidar_Vec(char *file, long *size, double *state_time, int *sequential, double *data)
{

  FILE *in;
  in = fopen(file,"rb");
  if (!in){
    fprintf(stdout, "Error: File '%s' Couldn't be open for input! Terminating!\n", file);
  	return(1);
  }else{
    /* fprintf(stdout, "Reading DLidarVec from binary file %s (Serial mode).\n",file); */
    size_t bytes;

    if (data == NULL){fprintf(stdout, "data must be allocated before passing!\n");}

    /* Read sequential, and size */
    fread(size, sizeof(long), 1, in);
    fread(state_time, sizeof(double), 1, in);
    fread(sequential, sizeof(int), 1, in);

    /* read grid */
    bytes = fread(data, sizeof(double), (int)size, in);
    if ((long)bytes != *size) {
      fprintf(stdout, "Error in read_DLidar_Vec(): Unable to read DLidarVec.data . Expected %ld, Read %ld.\n",
              *size, (long)bytes);
      return(-1);
    }

  }

  fclose(in);

  return(0);
}


int read_DLidar_Vec_OldFormat(char *file, long *size, int *sequential, double *data)
{

  FILE *in;
  in = fopen(file,"rb");
  if (!in){
    fprintf(stdout, "Error: File '%s' Couldn't be open for input! Terminating!\n", file);
  	return(1);
  }else{
    /* fprintf(stdout, "Reading DLidarVec from binary file %s (Serial mode).\n",file); */
    size_t bytes;

    if (data == NULL){fprintf(stdout, "data must be allocated before passing!\n");}

    /* Read sequential, and size */
    fread(size, sizeof(long), 1, in);
    fread(sequential, sizeof(int), 1, in);

    /* read grid */
    bytes = fread(data, sizeof(double), (int)size, in);
    if ((long)bytes != *size) {
      fprintf(stdout, "Error in read_DLidar_Vec(): Unable to read DLidarVec.data . Expected %ld, Read %ld.\n",
              *size, (long)bytes);
      fclose(in);
      return(-1);
    }

  }

  fclose(in);

  /* // Remove after debugging
  int i;
  for(i=0; i<50; i++){
      fprintf(stdout, "Inside read_DLidar_Vec(); Returning: X[%d] = %lf \n" ,i, data[i]);
    }
  */

	return(0);
}
