
cimport numpy as np

cpdef StateVector state_vector_from_file(file_name, files_ext=*)
cpdef Ensemble state_ensemble_from_file(directory, file_name_prefix, max_ens_size=*, files_ext=*)
cpdef StateVector state_vector_from_np_array(np.ndarray data, sequential=*, time=*)

cdef class StateVector:
  cdef :
    long int _size
    double* data
    double _time
    int _sequential

  cpdef write_to_file(self, file_name, file_ext=*)
  cpdef read_from_file(self, file_name)

  cdef int c_create_sequential(self, double init_val) except -1
  cpdef int create_sequential(self, double init_val =*)
  cpdef int create_parallel(self, double init_val =*)
  cdef int c_set_value(self, double value) except -1
  cdef double c_get_value(self, int key) except -1
  cdef int c_set_values(self, int count, int* keys, double* values) except -1
  cdef int c_get_values(self, int count, int* keys, double* values) except -1
  cdef int c_set_values_np(self, np.ndarray keys, np.ndarray values) except -1
  cdef int c_get_values_np(self, np.ndarray keys, np.ndarray values) except -1
  cdef int c_copy_data(self, StateVector other) except -1
  cdef int c_scale(self, double alpha, StateVector X) except -1
  cdef int c_reciprocal(self, StateVector X) except -1
  cdef double c_dot(self, StateVector X, StateVector Y) except -1
  cdef int c_add(self, StateVector X, StateVector Y) except -1
  cdef int c_axpy(self, double alpha, StateVector X, StateVector Y) except -1
  cdef int c_multiply(self, StateVector X, StateVector Y) except -1
  cdef int c_divide(self, StateVector X, StateVector Y) except -1
  cpdef double norm2(self, bint ignore_nan=*)
  cpdef double sum(self)
  cpdef double mean(self)
  cpdef double variance(self, double ddof=*)
  cpdef double stdev(self, double ddof=*)
  cpdef double max(self)
  cpdef double min(self)
  cdef int c_power(self, StateVector X, double p) except -1
  cdef int c_abs(self, StateVector X) except -1

  cdef int c_get_slice(self, int start, int stop, int step, np.ndarray state) except -1
  cdef int c_set_slice(self, int start, int stop, int step, np.ndarray state) except -1

cdef class Ensemble:
  cdef:
    long int _vec_size
    long int _ensemble_size
    dict data
    double _time
    int _sequential

  cpdef StateVector mean(self)
  cpdef StateVector variance(self, int ddof=*)
  cpdef StateVector stdev(self, int ddof=*)
  cpdef Ensemble inflate(self, inflation_fac, in_place=*)
  cpdef Ensemble extend(self, other, in_place=*)
  cpdef write_to_file(self, directory, file_name_prefix, files_ext=*)
  cpdef Ensemble read_from_file(self, directory, file_name_prefix, max_ens_size=*)

