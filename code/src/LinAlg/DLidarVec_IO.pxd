

cdef extern from "DLidarVec_IO.c":
  cdef int write_DLidar_Vec(char *file, long size, double state_time, int sequential, double *data)
  cdef int alloc_and_read_DLidar_Vec(char *file, long *size, double *state_time, int *sequential, double **data)
  cdef int alloc_and_read_DLidar_Vec_OldFormat(char *file, long *size, int *sequential, double **data)
  cdef int read_DLidar_Vec(char *file, long *size, double *state_time, int *sequential, double *data)
  cdef int read_DLidar_Vec_OldFormat(char *file, long *size, int *sequential, double *data)
