

"""
    IterativeFilteringProcess:
    A class implementing functionalities of a filtering process.
    A filtering process here refers to repeating a filtering cycle over a specific observation/assimilation timespan

    The differences between this module and filtering_process are:
        1- this one enables resume/restart, and forces cycles of equal length
        2- Uses real data only
        3-
"""

import re

import numpy as np
import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import random
import shutil

sys.path.append("../")
sys.path.append("../Utility")
sys.path.append("../LinAlg")
import DL_utility as utility
from DLidarVec import StateVector, Ensemble, state_vector_from_file

try:
    import DL_constants
except(ImportError):
    pass


try:
    import mpi4py
    mpi4py.rc.recv_mprobe = False
    from mpi4py import MPI
    use_MPI = True
except(ImportError):
    use_MPI = False


class IterativeFilteringProcess(object):
    """
    A class implementing the steps of a filtering process similar to filtering_process.FilteringProcess,
    but it can run iteratively from the last checkpoints
    This recursively/iteratively apply filtering on several assimilation cycles given assimilation/observation timespan.

    The input configurarions are used to control the assimilation process behavious over several cycles.

    Args:
        assimilation_configs (default None); a dictionary containing assimilation configurations.
        Supported configuarations:
            * filter (default None): filter object
            * MPI_COMM
            * time_unit
            * collect_remote_DL_data
            * cleanup_out_dir
            random_seed

        output_configs (default None); a dictionary containing screen/file output configurations:
        Supported configuarations:
            * scr_output (default True): Output results to screen on/off switch
            * scr_output_iter (default 1): number of iterations/windows after which outputs are printed
            * file_output (default False): save results to file on/off switch
            * file_output_iter (default 1): number of iterations/windows after which outputs are saved to files
            * file_output_dir (default None): defaul directory in which results are written to files
            * verbose

    """
    # Default filtering process configurations
    _def_assimilation_configs = dict(filter=None,
                                     MPI_COMM=None,
                                     time_unit=None,
                                     collect_remote_DL_data=False,
                                     cleanup_out_dir=False,  # set to True if
                                     random_seed=None,  # Reset the random seed before carrying out any filtering steps.
                                     )

    _def_output_configs = dict(scr_output=True,
                               file_output=False,
                               file_output_iter=1,
                               file_output_dir=None,
                               verbose=False,
                               )


    def __init__(self, assimilation_configs, output_configs):
        """
        """
        self.__initialized = False
        #
        self.assimilation_configs = self.validate_assimilation_configs(assimilation_configs,
                                                                       IterativeFilteringProcess._def_assimilation_configs)
        self.output_configs = self.validate_output_configs(output_configs, IterativeFilteringProcess._def_output_configs)
        self._verbose = self.output_configs['verbose']
        #
        # Make sure the directory is created and cleaned up at this point...
        cleanup_out_dir = self.assimilation_configs['cleanup_out_dir']
        self.set_filtering_output_dir(self.output_configs['file_output_dir'], cleanup=cleanup_out_dir)

        # Added model, observation, and filter configurations to a setup file in that directory
        filter_obj = self.assimilation_configs['filter']
        model = filter_obj.model
        model_conf = model.get_model_configs()
        obs_conf = model.get_observation_configs()
        filter_conf = filter_obj.get_filter_configs(details='basic')
        filter_out_conf = filter_obj.get_output_configs()
        file_output_dir= self.output_configs['file_output_dir']
        config_filename = "experiment_setup.dat"
        if not os.path.isfile(os.path.join(file_output_dir, config_filename)):
            utility.write_dicts_to_config_file(config_filename, file_output_dir,
                                               [model_conf, obs_conf, filter_conf, filter_out_conf],
                                               ['Model Configs', 'Observation Configs', 'Filter Configs', 'Filter Output Configs'])

        # Save model information to file in the same location
        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        if my_rank == 0:
            model.save_error_models(location=file_output_dir, save_obs_error=True, save_prior_error=False, save_model_error=False)  # overwrite is off by default
        #
        self.__ASSIMILATION_STATUS_FILE = "last_assimilated_cycle.dat"
        #
        self.__initialized = True


    def start_assimilation_process(self,
                                   initial_time,
                                   window_size,
                                   number_of_windows=1,
                                   read_initial_ensemble_from=None,
                                   load_ensemble_prefix=None,
                                   free_run_state_filename='free_run_state.dlvec',
                                   load_ensemble_size=None):
        """
        Args:
            initial_time: The starting time of the assimilation experiment
            window_size: time-length of an assimilation cycle (forecast/analysis)
            number_of_windows: number of assimilation cycles (analysis+forecast)
            read_initial_ensemble_from: path to the directory to read the initial ensememble from
            load_ensemble_prefix = file-name-prefix used to load ensmble members (all files starting with this prefix are loaded)
            free_run_state_filename e.g., 'free_run_state.dlvec',  # the free run state must be in the same directory as initial ensemble
            load_ensemble_size: number of ensemble members to load from folder; if None, all available states are loaded

        Return:
            None

        Remarks:
            - If initial time is None,  the initial_time will be assigned to each member of initial_ensemble
            - read_initial_ensemble_from is used only if (a valid path) and initial_ensemble is None

        """
        filter_obj = self.assimilation_configs['filter']
        model_obj = filter_obj.model

        # Setting verbosity of the filter to match this object
        filter_obj.verbose = self._verbose

        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        if MPI_COMM is not None:
            MPI_COMM.Barrier()

        if MPI_COMM is not None and my_rank==0:
            print("\n%s\n\t Root node; an MPI-COMM is found with %d parallel nodes..." % ("~"*80, comm_size))
            print("\t Starting An Assimilation Process using [%s Filter] \n%s\n" % (filter_obj.filter_name, "~"* 80))
        elif my_rank==0:
            print("Serial run")

        # Set/Update random states generator seed:
        random_seed = self.assimilation_configs['random_seed']
        if random_seed is not None:
            # get the current state of the random number generator
            curr_rnd_state = np.random.get_state()
            np.random.seed(random_seed)
            try:
                filter_obj.random_seed = None
            except(AttributeError):
                pass

        # LOAD/VALIDATE ensembles
        # Every rank/process IS ASSUMED TO BE initialized with it's own ensemble
        # Also, a free-run-stat is inspected or created from the initial ensemble
        # 1- Check ensemble size; and syncronize with all other ranks to make sure everyone has it's own bunch of ensembles; otherwise load from file
        # 2- if ensemble is loaded from file, the ensemble size per node is decided in round-robin fashion with semi-equal balance
        file_exists, bad_format, assimilation_status_file, _, _, model_states_dir, _ = self.inspect_status_file(self.assimilation_status_filename, correct_contents=True)
        save_initial_results = False
        if not file_exists:
            if my_rank == 0:
                print("***\nCan't find the status file: \n%s" % assimilation_status_file)
                print("This means no previous runs will be considered, and assimilation should start here\n***")

            # Start looking for initial ensemble
            if read_initial_ensemble_from is None:
                if my_rank == 0:
                    print("No results found; but 'read_initial_ensemble_from' is not preset. Can't proceed with initial ensemble!")
                raise ValueError
            else:
                initial_ensemble = self.load_ensemble_from_files(ensemble_directory=read_initial_ensemble_from,
                                                                 total_ensemble_size=load_ensemble_size)
                ensemble_size = initial_ensemble.size  # this is the local ensemble
                save_initial_results = True
                #
        elif bad_format:
            if my_rank == 0:
                print("Status file exists, however the format in this file is tampered, or values do not match! please remove/correct, or cleanup results dir to reset")
                print("Status file: %s " % assimilation_status_file)
            raise ValueError
        else:
            # found assimilation_status_file; load initial ensemble from it
            read_initial_ensemble_from = model_states_dir
            # print("Reading initial Ensememble from : %s" % model_states_dir)
            # print(filter_obj.get_filter_configs(details='basic'))
            # start loading ensemble members from folder
            initial_ensemble = self.load_ensemble_from_files(ensemble_directory=read_initial_ensemble_from,
                                                             total_ensemble_size=load_ensemble_size,
                                                             filename_prefix=load_ensemble_prefix)
            ensemble_size = initial_ensemble.size

        #
        # Look for free state; if not found, create it from the loaded ensemble;
        free_run_state_filepath = os.path.join(read_initial_ensemble_from, free_run_state_filename)
        if os.path.isfile(free_run_state_filepath):
            free_run_state = state_vector_from_file(free_run_state_filepath)
        else:
            free_run_state = self._calculate_ensemble_mean(initial_ensemble)
        #
        if ensemble_size == 0:
            print("WASTED RESOURCES WARNING: node/process %d has no ensemble members!" % my_rank)
        #

        # sync and validate ensemble_size
        if MPI_COMM is not None:
            ensemble_sizes = MPI_COMM.allgather(ensemble_size)
        else:
            ensemble_sizes = [ensemble_size]

        if np.sum(ensemble_sizes) == 0:
            if my_rank == 0:
                print("No ensemble members are loaded to any of the involved processes! Nothing to do here; Terminating...")
            raise ValueError
            #

        # Validate initial time, update ensemble times; and sync initial time over all nodes
        # Verify initial time, and sync with ensemble memblers;
        # Time unit should be defaulted to 's' i.e., seconds
        time_unit = self.assimilation_configs['time_unit']
        if initial_time is not None:
            # convert initial time to a valid timestamp
            initial_time = utility.time_to_timestamp(initial_time, time_diff_unit=time_unit)
            t0 = utility.time_to_scalar(initial_time)
        else:
            # No passed initial time
            if ensemble_size > 0:
                t0 = initial_ensemble[0].time
                initial_time = utility.time_to_timestamp(t0, time_diff_unit=time_unit)
            else:
                t0 = initial_time = None

        if comm_size > 1:
            initial_times = MPI_COMM.allgather(t0)
            if initial_times[0] is None:
                print("Terminating; Root node doesn't have proper initial time")
                raise ValueError
            elif t0 is None or save_initial_results:  # save_initial_results is True only for initial ensemble, and is adjusted from root node
                t0 = initial_times[0]
            elif t0 == initial_times[0]:
                pass
            else:
                print("Node %d has initial scalr time %d, while the root node has scalar initial time %d; Sync issues! Terminating" % (my_rank, t0, initial_times[0]))
                raise ValueError
        elif t0 is None:
            print("No valid initial time found! Terminating")
            raise ValueError
        #
        # Update initial time; in case it changed during sync
        initial_time = utility.timestamp_from_scalar(t0)

        # Make sure all states have the right initial time
        for state in initial_ensemble:
            state.time = t0

        # Now, check the free_run_state
        if free_run_state is not None:
            free_run_state.time = t0


        # >>> Now, each process has it's own assigned ensemble members; Proceed  <<<
        # Save results of the initial cycle if no previous results found
        # Initilize things to filter
        filter_obj.analysis_ensemble = initial_ensemble
        filter_obj.free_run_state = free_run_state  # Won't matter
        if save_initial_results:
            if my_rank == 0:
                print("Saving Initial Ensemble Information...")
            # Now, write this ensemble to the results directory
            filter_obj.forecast_ensemble = initial_ensemble
            filter_obj.tspan = [t0, t0]
            # filter_obj.observation = None
            file_output_directory, filter_statistics_dir, model_states_dir, observations_dir = filter_obj.save_cycle_results()

            # Now, upadte state file
            if my_rank == 0:
                with open(assimilation_status_file, 'w') as f_id:
                    f_id.write("file_output_directory: %s\n" % file_output_directory)
                    f_id.write("filter_statistics_dir: %s\n" % filter_statistics_dir)
                    f_id.write("model_states_dir: %s\n" % model_states_dir)
                    f_id.write("observations_dir: %s\n" % observations_dir)
            #
            # print("Node: %d ; sync after cycle" % my_rank)


        # Good to proceed with assimilation cycles given window_size, number_of_windows=1
        dt = utility.deltatime_to_scalar(window_size, time_diff_unit=time_unit)
        t1 = t0 + dt

        # create proper timespan
        ts = [t0, t1]
        # ts = utility.timespan_to_scalars([initial_time, initial_time+window_size, time_diff_unit=time_unit)
        # print("Inside iterative_filtering_process.start_assimilation_process; ts: ", ts)
        #
        collect_remote_DL_data = self.assimilation_configs['collect_remote_DL_data']
        for cycle_ind in range(number_of_windows):
            cycle_tspan = [t0+dt*cycle_ind, t0+dt*(cycle_ind+1)]
            if my_rank == 0:
                print("Assimilation [Cycle %d/%d] over time interval: %s to %s" % (cycle_ind+1,
                                                                               number_of_windows,
                                                                               utility.timestamp_from_scalar(t0, time_diff_unit=time_unit, return_string=True),
                                                                               utility.timestamp_from_scalar(t1, time_diff_unit=time_unit, return_string=True)))
                sys.stdout.flush()

            # Collect doppler lidar data/observations
            if my_rank == 0:
                obs_tspan = [utility.timestamp_from_scalar(t, time_diff_unit=time_unit, return_string=False) for t in cycle_tspan]
                obs_times, observations = model_obj.get_real_observations(obs_tspan,
                                                                          return_ensemble=False,
                                                                          aggregate_data=True,
                                                                          mismatch_policy='exact',
                                                                          # snr_threshold =0.008,
                                                                          collect_remote_data=collect_remote_DL_data
                                                                         )
                if len(observations) == 0:
                    print("No observations found on the interval: %s " % repr(obs_times))
                    observation = None
                    # raise ValueError
                elif len(observations) != 1:
                    print("Received more than one observation instance")
                    print("obs_times: ", obs_times)
                    print("len(observations): ", len(observations))
                    raise ValueError
                else:
                    observation = observations[0]
                    observation.time = cycle_tspan[-1]
            else:
                observation = None

            if comm_size > 1:
                observation = MPI_COMM.bcast(observation, root=0)

            # Statistics at the beginning of the initial cycle (Forecast=Analysis)

            # Update/Set Timspan and observation: The initial ensmble is already hooked before this loop, and is marched by the filter automatically
            filter_obj.observation = observation
            filter_obj.tspan = cycle_tspan

            # Update output configs (based on flag and iterations)
            file_output = scr_output = False
            if self.output_configs['scr_output']:
                    scr_output = True  # switch on/off screen output in the current filtering cycle
            if self.output_configs['file_output'] :
                    file_output = True  # switch on/off file output in the current filtering cycle
            if filter_obj.scr_output != scr_output:
                filter_obj.scr_output = scr_output
            if filter_obj.file_output != file_output:
                filter_obj.file_output = file_output

            # Now start the filtering cycle.
            if not filter_obj.forecast_first:
                filter_obj.forecast_first = True

            if self._verbose:
                print("Node[%d] Starting filtering cycle" % my_rank)
                sys.stdout.flush()
            # Forecast Step + Forecast Statistics
            # Analysis Step + Analysis Statistics
            file_output_directory, filter_statistics_dir, model_states_dir, observations_dir = filter_obj.filtering_cycle()

            # SAve filter_statistics_dir, model_states_dir, observations_dir, for later use, and to replace load_ensemble_from_files
            if my_rank == 0:
                with open(assimilation_status_file, 'w') as f_id:
                    f_id.write("file_output_directory: %s\n" % file_output_directory)
                    f_id.write("filter_statistics_dir: %s\n" % filter_statistics_dir)
                    f_id.write("model_states_dir: %s\n" % model_states_dir)
                    f_id.write("observations_dir: %s\n" % observations_dir)

            #
            # Sync after each cycle
            if comm_size > 1:
                MPI_COMM.Barrier()
            # print("Node: %d ; sync after cycle" % my_rank)
            # sys.stdout.flush()


        # Restore NP random state
        if random_seed is not None:
            # get the current state of the random number generator
            np.random.set_state(curr_rnd_state)

        # for MPI comm with more than one node; might be overkill!
        # print("Node: %d ; sync before exiting assimilation process" % my_rank)
        if comm_size > 1:
            MPI_COMM.Barrier()
        # sys.stdout.flush()


    def _calculate_ensemble_mean(self, local_ensemble, broadcast_state=False):
        """
        Given the local_ensemble (on each node/process; calculated the pooled ensemble mean, send it back;
            broadcast to all nodes only if bcast flag is on, otherwise, only root posesses it

        Args:
            local_ensemble:
            broadcast_state: if True, the mean is broadcasted to all involved processes, otherwise, only root Keeps it, and all other nodes get None

        Returns:
            ens_mean (or None)

        Remarks:
            If the total ensemble size is zero (or all local ensembles are None) ens_mean=None is returned

        """
        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        # Calculate local mean
        if local_ensemble is None:
            local_mean = None
            local_ens_size = 0
        else:
            assert isinstance(local_ensemble, Ensemble), "local_ensemble must be an instance of Ensemble class; Found: type(local_ensemble) is %s" % type(local_ensemble)
            if local_ensemble.size > 0:
                local_mean = local_ensemble.mean()
                local_ens_size = local_ensemble.size
            else:
                local_mean = None
                local_ens_size = 0

        if MPI_COMM is not None:
            MPI_COMM.Barrier()

        # Now, communicate calculated local means (sub-sample mean);
        # Calculate total ensemble size
        ensemble_size = local_ens_size
        if comm_size > 1:
            ensemble_size = MPI_COMM.allreduce(ensemble_size, op=MPI.SUM)

        if ensemble_size == 0:
            ens_mean = None
        else:
            if my_rank == 0:
                if self._verbose:
                    print("ROOT: Total ensmble size = %d " % ensemble_size)
                    sys.stdout.flush()
                weight = float(local_ens_size)/ensemble_size
                if local_mean is not None:
                    ens_mean = local_mean.scale(weight, in_place=False)
                else:
                    ens_mean = None

                # Sync local ensemble sizes
                aggr_ens_sizes = local_ens_size
                if self._verbose:
                    print("ROOT: Starting with aggr_ens_sizes %d " % aggr_ens_sizes)
                    sys.stdout.flush()
                rmt_ens_sizes = [local_ens_size]
                for node_rank in range(1, comm_size):
                    rmt_ens_size = MPI_COMM.recv(source=node_rank, tag=node_rank+7001)
                    rmt_ens_sizes.append(rmt_ens_size)
                    aggr_ens_sizes += rmt_ens_size
                    if self._verbose:
                        print("Node %d has %d ensemble members..." % (node_rank, rmt_ens_size))
                        sys.stdout.flush()

                # Start syncing state vectors
                for node_rank in range(1, comm_size):
                    rmt_ens_size = rmt_ens_sizes[node_rank]
                    if rmt_ens_size > 0:
                        if self._verbose:
                            print("Receiving the mean of [%d] ensembles from node [%d]..." % (rmt_ens_size, node_rank))
                            sys.stdout.flush()
                        vec = MPI_COMM.recv(source=node_rank, tag=node_rank+8001)
                        weight = float(rmt_ens_size)/ensemble_size
                        vec.scale(weight, in_place=True)
                        if ens_mean is not None:
                            ens_mean.add(vec, in_place=True)
                        else:
                            ens_mean = vec.copy()
                        if self._verbose:
                            print("___---DONE----___")
                            sys.stdout.flush()
                    else:
                        if self._verbose:
                            print("Receiving NOTHING from node [%d]..." % node_rank)

            else:
                ens_mean = None
                # Non-root nodes send to root, their local ensemble sizes:
                MPI_COMM.send(local_ens_size, dest=0, tag=my_rank+7001)
                #
                # Now, send the ensemble members:
                if local_ens_size > 0:
                    if self._verbose:
                        print("Node %d; sending my local_mean..." % my_rank)
                        sys.stdout.flush()
                    MPI_COMM.send(local_mean, dest=0, tag=my_rank+8001)
                    if self._verbose:
                        print("Node %d; DONE..." % my_rank)
                        sys.stdout.flush()

            # Broadcast state if requested
            if broadcast_state and comm_size > 1:
                ens_mean = MPI_COMM.bcast(ens_mean, root=0)

        return ens_mean


    def load_ensemble_from_files(self, ensemble_directory, total_ensemble_size=None, filename_prefix=None, out_ensemble=None, shuffle_ensemble=False):
        """
        Load ensemble members from files

        Args:
            ensemble_directory: path to directory that contains the ensemble memers to load
            total_ensemble_size: number of ensemble members to read from files; if None, all valid files are read
            filename_prefix: only files starting with that prefix is considered; if None, all files are considered

        Returns:
            ensemble assigned to the calling process in MPICOMM (if not None)

        """
        # 1- Check ensemble directory
        if not os.path.isdir(ensemble_directory):
            print("Directory not found!\n The ensemble directory[%s] is not valid!" % ensemble_directory)
            raise IOError

        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        if my_rank == 0:
            list_of_files = utility.get_list_of_files(root_dir=ensemble_directory, return_abs=True, extension='dlvec')
            if filename_prefix is None:
                valid_files = list_of_files
            else:
                valid_files = []
                for f in list_of_files:
                    _, fname = os.path.split(f)
                    if fname.startswith(filename_prefix) and fname.endswith('.dlvec'):
                        valid_files.append(f)

            if shuffle_ensemble:
                random.shuffle(valid_files)
                # randomize entries of the list
                pass

            if total_ensemble_size is not None:
                if 0 < total_ensemble_size < len(valid_files):
                    valid_files = valid_files[: total_ensemble_size]
        else:
            valid_files = []

        if comm_size > 1:
            # This is not how scatter works; TODO Refactoring...
            if my_rank == 0:
                files_per_rank = [len(valid_files)//comm_size]*comm_size
                ind = 0
                while np.sum(files_per_rank) < len(valid_files):
                    files_per_rank[ind] += 1
                    ind += 1
                if np.sum(files_per_rank) != len(valid_files):
                    if my_rank == 0:
                        print("Failed to split files among running processes!!")
                    raise ValueError
                if self._verbose and my_rank == 0:
                    print("Root: Loading ensemble members; # of files for each process: ", files_per_rank)
                    sys.stdout.flush()
            else:
                files_per_rank = None

            files_per_rank = MPI_COMM.bcast(files_per_rank, root=0)
            local_num_of_files = files_per_rank[my_rank]
            # print("Node[%d]: My local number of files is %d" % (my_rank, files_per_rank[my_rank]))
            # sys.stdout.flush()

            if my_rank == 0:
                for rank in range(1, comm_size):
                    num_files = files_per_rank[rank]
                    if num_files > 0:
                        send_files = valid_files[np.sum(files_per_rank[:rank]): np.sum(files_per_rank[:rank+1])]
                        MPI_COMM.send(send_files, dest=rank, tag=rank+1000)
                valid_files = valid_files[: files_per_rank[0]]
            else:
                if local_num_of_files > 0:
                    valid_files = MPI_COMM.recv(source=0, tag=my_rank+1000)
                if local_num_of_files != len(valid_files):
                    print("NODE [%d]: didn't receive proper number of valid files" % my_rank)
                    raise ValueError
                else:
                    pass
                    # print("Node [%d] received %d files properly" % (my_rank, local_num_of_files))

            # sys.stdout.flush()

        if out_ensemble is None:
            model_obj = self.assimilation_configs['filter'].model
            out_ensemble = Ensemble(0, model_obj.state_size())
        elif out_ensemble.size > 0:
            print("The passed ensemble is not Empty; it contains %d ensemble members already!" % out_ensemble.size)
            raise TypeError
        else:
            pass

        for state_file in valid_files:
            # print("Initial ensemble readieng... loading : %s " % state_file)
            state = state_vector_from_file(state_file)
            if state.size != out_ensemble.state_size:
                print("Error in loading state from: %s" % (state_file))
                print("Size mismatch while loading ensemble\n\tLoaded state is of size %d\n\tEnsemble state size %d\n" % (state.size, out_ensemble.state_size))
                if True:
                    pass  # decide what you want  # TODO
                else:
                    raise AssertionError
            else:
                out_ensemble.append(state)

        # gather all ensemble sizes to make sure total ensmble size is correct
        ensemble_size = out_ensemble.size
        if comm_size > 1:
            ensemble_sizes = MPI_COMM.allgather(ensemble_size)
            ensemble_size = np.sum(ensemble_sizes)

        if total_ensemble_size is not None:
            if ensemble_size != total_ensemble_size:
                print("Loaded ensemble size doesn't match asserted total ensemble size.")
                print("Requested %d ensembles, but loaded %d" % (total_ensemble_size, ensemble_size))
                if total_ensemble_size > ensemble_size:
                    print("Not Enough ensembles found in the passed directory; This could be incomplete assimilation cycle!")
                elif total_ensemble_size < ensemble_size:
                    print("Loaded more ensembles than should be loaded!")
                raise ValueError

        return out_ensemble

    def set_filtering_output_dir(self, file_output_dir, cleanup=True, backup_existing=True):
        """
        Set the output directory of filtering results.

        Args:
            file_output_dir_path: path/directory to save results under
            backup_existing (default True): if True the existing folder is archived as *.zip file.

        Returns:
            None

        """
        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        # Make sure the directory is created and cleaned up at this point...
        file_output = self.output_configs['file_output']
        if file_output and my_rank==0:
            #
            DLiDA_root_dir = os.environ.get('DL_ROOT_PATH')
            if DLiDA_root_dir is None:
                try:
                    DLiDA_root_dir = DL_constants.DL_ROOT_PATH
                except:
                    DLiDA_root_dir = os.getcwd()
            #
            if not os.path.isabs(file_output_dir):
                file_output_dir = os.path.join(DLiDA_root_dir, file_output_dir)
            parent_path, out_dir = os.path.split(file_output_dir)
            #
            if not os.path.isdir(file_output_dir):
                os.makedirs(file_output_dir)
            elif cleanup:
                utility.cleanup_directory(directory_name=out_dir, parent_path=parent_path, backup_existing=True, zip_backup=False)
            else:
                # Do nothing
                pass
            # Override output configurations of the filter:
            # self.file_output_dir = file_output_dir
        else:
            file_output_dir = None

        if MPI_COMM is not None:
            file_output_dir = MPI_COMM.bcast(file_output_dir, root= 0)
        #
        self.output_configs['file_output_dir'] = file_output_dir
        # Override output configurations of the filter:
        filter_obj = self.assimilation_configs['filter']
        filter_obj.file_output = file_output
        filter_obj.file_output_dir = file_output_dir
        #

    # TODO: Consider the case when results are moved from one machine to another; i.e. correct structure
    def inspect_status_file(self, assimilation_status_file=None,  correct_contents=True):
        """
        Inspect the status file containing information about the last assimilation cycle (if any)

        Args:
            assimilation_status_file: file name/path

        Return:
            file_exists
            bad_format
            assimilation_status_file
            file_output_directory
            filter_statistics_dir
            model_states_dir
            observations_dir

        """
        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        if my_rank == 0:
            # inspect, and extract info
            if assimilation_status_file is None:
                assimilation_status_file = self.assimilation_status_filename
            if not os.path.isabs(assimilation_status_file):
                file_output_directory = self.output_configs['file_output_dir']
                assimilation_status_file = os.path.join(file_output_directory, assimilation_status_file)
            # Now check if file exists, and extract information
            bad_format = False  # if True; the file exists but its format is incorrect or the information is incomplete
            # print("Checking file: %s" % assimilation_status_file)
            if os.path.isfile(assimilation_status_file):
                file_exists = True
                with open(assimilation_status_file, 'r') as f_id:
                    cont = f_id.readlines()
                    fle_file_output_directory = None  # the one found on file
                    filter_statistics_dir = None
                    model_states_dir = None
                    observations_dir = None
                    for ln in cont:
                        lr_ln = ln.lower()  # TODO: consider using 're' instead!
                        if lr_ln.startswith('file_output_directory'):
                            fle_file_output_directory = ln.split(':')[-1].strip()
                        elif lr_ln.startswith('filter_statistics_dir'):
                            filter_statistics_dir = ln.split(':')[-1].strip()
                        elif lr_ln.startswith('model_states_dir'):
                            model_states_dir = ln.split(':')[-1].strip()
                        elif lr_ln.startswith('observations_dir'):
                            observations_dir = ln.split(':')[-1].strip()
                        else:
                            # A line that contains unrecognized information; may be for future additions
                            pass
                    # Sanity checks
                    if fle_file_output_directory != file_output_directory:
                        if correct_contents:
                            print("The 'last_assimilation_cycle' status file seems to have been created in a different location. Attempting to correct contents as requested.")
                            # Correct model, observations, and statistics directories
                            if observations_dir is not None:
                                observations_dir = observations_dir.replace(fle_file_output_directory, file_output_directory)
                            if model_states_dir is not None:
                                model_states_dir = model_states_dir.replace(fle_file_output_directory, file_output_directory)
                            if filter_statistics_dir is not None:
                                filter_statistics_dir = filter_statistics_dir.replace(fle_file_output_directory, file_output_directory)
                        else:
                            print("fle_file_output_directory: ", fle_file_output_directory)
                            print("file_output_directory: ", file_output_directory)
                            bad_format = True
                    if (file_output_directory is None) or (filter_statistics_dir is None) or (model_states_dir is None) or (observations_dir is None):
                        bad_format = True
                pass
            else:
                # No file found
                file_exists = False
                bad_format = file_output_directory = filter_statistics_dir = model_states_dir = observations_dir = None

        else:
            # Intialize, and wait to sync
            file_exists = None
            bad_format = None
            assimilation_status_file = None
            file_output_directory = None
            filter_statistics_dir = None
            model_states_dir = None
            observations_dir = None

        if comm_size > 1:
            file_exists = MPI_COMM.bcast(file_exists, root=0)
            bad_format = MPI_COMM.bcast(bad_format, root=0)
            assimilation_status_file = MPI_COMM.bcast(assimilation_status_file, root=0)
            file_output_directory = MPI_COMM.bcast(file_output_directory, root=0)
            filter_statistics_dir = MPI_COMM.bcast(filter_statistics_dir, root=0)
            model_states_dir = MPI_COMM.bcast(model_states_dir, root=0)
            observations_dir = MPI_COMM.bcast(observations_dir, root=0)

        if False:  # TODO: remove after debugging
            print("Returning: %s" % ('\n'.join([str(x) for x in [file_exists, bad_format, assimilation_status_file, file_output_directory, filter_statistics_dir, model_states_dir,
                                                                 observations_dir]])) )
        return file_exists, bad_format, assimilation_status_file, file_output_directory, filter_statistics_dir, model_states_dir, observations_dir


    @property
    def assimilation_status_filename(self):
        return self.__ASSIMILATION_STATUS_FILE
    @assimilation_status_filename.setter
    def assimilation_status_filename(self, value):
        value = utility.validate_string(value)
        self.__ASSIMILATION_STATUS_FILE = value

    @staticmethod
    def validate_assimilation_configs(assimilation_configs, def_assimilation_configs):
        """
        Aggregate the passed dictionaries with default configurations then make sure parameters are consistent.
        The first argument (assimilation_configs) is validated, updated with missing entries, and returned.

        Args:
            assimilation_configs: dict,
                A dictionary containing assimilation configurations. This should be the assimilation_configs dict
                passed to the constructor.

            def_assimilation_configs: dict,
                A dictionary containing the default assimilation configurations.

        Returns:
            assimilation_configs: dict,
                Same as the first argument (assimilation_configs) but validated, adn updated with missing entries.

        """
        assimilation_configs = utility.aggregate_configurations(assimilation_configs, def_assimilation_configs)
        # Since aggregate never cares about the contents, we need to make sure now all parameters are consistent
        for key in assimilation_configs:
            if key not in def_assimilation_configs:
                print("Caution: Unknown key/feature detected: '%s' is ignored" % key)
        if assimilation_configs['filter'] is None:
            print("You have to create a filter object and attach it here so that I can use it for sequential DA!")
            raise ValueError

        # Reference to filter and model objects:
        filter_obj = assimilation_configs['filter']
        if filter_obj is None:
            print("Failed to retrieve a filter object!")
            raise ValueError
        model = filter_obj.model
        if model is None:
            print("Failed to retrieve a forward operator object!")
            raise ValueError

        # Check for parallel processes
        my_MPI_COMM = assimilation_configs['MPI_COMM']
        if my_MPI_COMM is not None:
            comm_size = my_MPI_COMM.Get_size()
            my_rank = my_MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0
        if comm_size > 1 and my_MPI_COMM is not None:
            if my_rank == 0:
                print("ROOT: started validating the assimilation configurations for %d Nodes..." % (comm_size))

        # TODO: that approach here for handling MPI-COMM MUST BE TESTED, and REVISED
        filter_MPI_COMM = filter_obj.MPI_COMM

        if my_MPI_COMM is None and filter_MPI_COMM is None:
            print("Serial run ... Validating Assimilation Configs ...")
        elif not(my_MPI_COMM is None or filter_MPI_COMM is None):
            #
            if my_MPI_COMM is filter_MPI_COMM:
                pass  # both use the same communicaotr; ...Normal...
                # print("Process [%d/%d] started validating the assimilation configurations..." % (my_rank, comm_size))
            else:
                # Two different communicators!!!
                if my_rank == 0:
                    print("Two communicators are passed; This might be OverKilling.\n Behaviour of this setting should be tested...")
                raise AssertionError
        else:
            print("Filtering process must be configured with/woithout MPI exactly same way as the filter object.")
            print("Set 'MPI_COMM' key in the configurations dictionary of both similarily.")
            print("Both must be either None, or must be set to the same MPI.COMM_WORLD")
            raise VaueError

        #
        time_unit = assimilation_configs['time_unit']
        if time_unit is None:
            try:
                time_unit = model.model_time_unit
            except:
                try:
                    time_unit = model.time_unit
                except:
                    print("YOU MUST SET TIME UNIT to a valid string; 'M' for Minutes, 'H' for Hours, 'S' for Seconds.")
                    print("This is required to handle real time instances correctly")
                    print("Trying with None!")
                    time_unit = None
            finally:
                assimilation_configs.update({'time_unit':time_unit})

        elif not isinstance(time_unit, str):
            print("YOU MUST SET TIME UNIT to a valid string; 'M' for Minutes, 'H' for Hours, 'S' for Seconds")
            raise TypeError
        else:
            time_unit = time_unit.lower().strip()
            if time_unit not in ['h', 'm', 's']:
                print("YOU MUST SET TIME UNIT to a valid string; 'M' for Minutes, 'H' for Hours, 'S' for Seconds")
                raise ValueError
            else:
                # good to go
                assimilation_configs['time_unit'] = time_unit

        #
        if my_MPI_COMM is not None:
            my_MPI_COMM.Barrier()

        if my_rank == 0:
            print("ROOT: DONE validating assimilation configurations on all [%d] nodes..." % (comm_size))

            #
        return assimilation_configs

    @staticmethod
    def validate_output_configs(output_configs, def_output_configs):
        """
        Aggregate the passed dictionaries with default configurations then make sure parameters are consistent.
        The first argument (output_configs) is validated, updated with missing entries, and returned.

        Args:
            output_configs: dict,
                A dictionary containing output configurations. This should be the output_configs dict
                passed to the constructor.

            def_output_configs: dict,
                A dictionary containing the default output configurations.

        Returns:
            output_configs: dict,
                Same as the first argument (output_configs) but validated, adn updated with missing entries.

        """
        output_configs = utility.aggregate_configurations(output_configs, def_output_configs)
        # screen output

        # file output
        if output_configs['file_output']:
            #
            if output_configs['file_output_dir'] is None:
                output_configs['file_output_dir'] = 'Results/Filtering_Results'  # relative to DLiDA directory of course
            # Create the full path of the output directory ( if only relative dir is passed)
            DLiDA_root_dir = os.environ.get('DL_ROOT_PATH')
            if DLiDA_root_dir is None:
                try:
                    DLiDA_root_dir = DL_constants.DL_ROOT_PATH
                except:
                    DLiDA_root_dir = os.getcwd()
            # if not str.startswith(output_configs['file_output_dir'], DLiDA_root_dir):
            #     output_configs['file_output_dir'] = os.path.join(DLiDA_root_dir, output_configs['file_output_dir'])
            output_configs['file_output_dir'] = output_configs['file_output_dir'].rstrip(os.path.sep)

        else:
            output_configs['file_output_dir'] = None

        return output_configs
        #
