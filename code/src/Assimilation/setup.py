
# setup extension modules

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

import numpy as np


include_dirs = ['./']+[np.get_include()]

ext_modules = cythonize([
    Extension("EnKF", sources=['EnKF.pyx'], include_dirs=include_dirs)
    ])

setup(ext_modules=ext_modules)
