
# TODO: REFACTOR EVERYTHIN TO BE AWARE OF MPI POSSIBILITY

# Stochastic and Deterministic Ensemble Kalman Filters

from libc.math cimport pow, exp, sqrt

import DL_utility as utility
cimport numpy as np
import numpy as np

import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
  pass
else:
  range = xrange

import os
import re

try:
  import mpi4py
  mpi4py.rc.recv_mprobe = False
  from mpi4py import MPI
  use_MPI = True
except:
  use_MPI = False

try:
  import h5py
  use_h5py = True
except(ImportError):
  use_h5py = False

from DLidarVec import StateVector, Ensemble
from forward_model import ForwardOperator


def isiterable(x):
  try:
    iter(x)
    out = True
  except:
    out = False
  return out


cdef class EnKF(object):

  # cdef dict filter_configs
  # cdef dict output_configs

  # cdef dict _filter_statistics
  # cdef bint _initialized
  # cdef bint _verbose

  _filter_name = "EnKF"
  _def_filter_configs = dict(model=None,
                             filter_name=_filter_name,
                             MPI_COMM=None,
                             forecast_first=True,
                             tspan=None,  # a tuple with two entries ([previous time, current time])
                             observation_error_model=None,  # TO be decided!
                             background_error_model=None,  # TO be decided!
                             hybrid_background_coeff=0.0,
                             forecast_inflation_factor=1.0,  # applied to forecast ensemble
                             analysis_inflation_factor=1.05,  # applied to analysis ensemble
                             obs_covariance_scaling_factor=1.0,
                             localize_covariances=True,
                             localization_method='covariance_filtering',
                             localization_radius=np.infty,
                             localization_function='gaspari-cohn',
                             localization_matrices_repo=None,  # repository of localization matrices to look into, or save to
                             ensemble_size=None,
                             analysis_ensemble=None,
                             forecast_ensemble=None,
                             observation=None,
                             reference_state=None,  # True solution if available
                             free_run_state=None,
                             apply_post_processing=False,
                             )
  _def_output_configs = dict(scr_output=True,
                             file_output=True,
                             filter_statistics_dir='Filter_Statistics',
                             model_states_dir='Model_States_Repository',
                             observations_dir='Observations_Repository',
                             file_output_moment_only=True,
                             file_output_moment_name='mean',
                             file_output_dir='DEnKF_results',
                             file_output_file_format='txt',
                             verbose=False,
                             debug_mode=False
                            )


  def __cinit__(self, filter_configs=None, output_configs=None, **kwargs):
    self._initialized = 0
    self.__init__(filter_configs, output_configs, **kwargs)
    #

  def __init__(self, filter_configs=None, output_configs=None, **kwargs):
    if not self._initialized:
      # Aggregate, validate, and set filter and output configurations
      self.filter_configs = utility.aggregate_configurations(filter_configs, DEnKF._def_filter_configs)
      self._validate_filter_configs()
      self.output_configs = utility.aggregate_configurations(output_configs, DEnKF._def_output_configs)
      self._validate_output_configs()
      #
      try:
        self._verbose = self.output_configs['verbose']
      except(AttributeError, KeyError, NameError):
        self._verbose = False
      #
      self._initialized = 1
      #


  def filtering_cycle(self):
    """
    Carry out a single filtering cycle. Forecast followed by analysis or the other way around.
    All required variables are obtained from 'filter_configs' dictionary.
    """
    tspan = self.filter_configs['tspan']
    #
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is not None:
      try:
        comm_size = MPI_COMM.size
        my_rank = MPI_COMM.rank
      except(AttributeError):
        comm_size = 1
        my_rank = 0
    else:
      comm_size = 1
      my_rank = 0

    model = self.filter_configs['model']
    #
    forecast_first = self.filter_configs['forecast_first']
    observation = self.filter_configs['observation']
    if observation is None:
      print("?????????????? OBSERVATION IS NOEN ON NODE {%d}" % my_rank)
      raise ValueError

    #
    if my_rank == 0:
      # reference_state, if given, is assumed to be at time tspan[0]
      # free_state, if given, is assumed to be at time tspan[0]; free_state.time will be inspected though!

      if observation is None:
        observation_forecast_rmse = observation_rmse = np.infty
      #
      reference_state = self.filter_configs['reference_state']
      if reference_state is None:
        forecast_rmse = analysis_rmse = initial_rmse = final_rmse = np.infty  # TODO: consider np.infty instead of -1
      else:
        # Inspect the reference time
        ref_time = reference_state.time
        if ref_time is None:
          print("WARNING: Time property in the reference state vector is not set; Assuming it is at the beggining of the assimilation cycle")
          print("Setting reference time to %f" % tspan[0])
          ref_time = tspan[0]
          reference_state.time = ref_time
        #
        if ref_time != tspan[0]:
          print("The reference State must be at time instance equal to the beginning of hte assimilation timespan")
          print("Reference State is given at time: %s" % str(ref_time))
          print("Reference State MUST be at time: %s" % tspan[0])
          raise AssertionError
        else:
          pass
      #
      if self._verbose:
        sep = "*"* 70
        print("\n%s\n%s: Starting An Assimilation Cycle... \n" % (self.filter_name, sep))
        sys.stdout.flush()
    #
    #
    if forecast_first:
      analysis_state = self.get_analysis_state(bcast_state=False)
      if my_rank == 0:
        # Main node cacluates statistics, e.g. RMSE
        if reference_state is not None:
          initial_rmse = utility.calculate_rmse(analysis_state, reference_state)
      else:
        # Other workers do nothing with reference state
        pass

      # given analysis ensemble at time tspan[0], observation is given at time t1, and analysis is to be done at time tspan[-1]
      # TODO: Now, all nodes have copies of the whole ensemble; consider splitting from the beginning

      # one node does all forward propagation:
      init_ensemble = self.filter_configs['analysis_ensemble']
      #
      if init_ensemble is None:
        if my_rank == 0:
          print("With 'forecast_first' set to True, you must provide an 'analysis_ensemble' at tspan[0]")
          raise ValueError
        else:
          # Some imbalance or wasted resources
          print("This is process %d: I am not doing anything in the forecast step!!!" % my_rank)
        forecast_ensemble = None
      else:
        if init_ensemble is not None:
          forecast_ensemble = self.forecast(init_ensemble, tspan[0], tspan[-1])
        else:
          forecast_ensemble = None
      self.filter_configs.update({'forecast_ensemble':forecast_ensemble})
      #
      #
      if my_rank == 0:
        # march the reference_state
        if reference_state is not None:
          print("\n $$$$$$ Marching the reference state forward in time... from %f to %f $$$$$$\n" %(tspan[0], tspan[-1]))
          tspan = self.filter_configs['tspan']
          _, _traject = self.model.integrate_state(reference_state, [tspan[0], tspan[-1]], model_screen_output=self._verbose)
          reference_state = _traject[-1]
          self.reference_state = reference_state.copy()
          del _traject
          if self._verbose:
            print("DONE...\nNew reference state at time %s " % repr(self.reference_state.time))
          #
      # self.get_forecast_state is collective:
      forecast_state = self.get_forecast_state(bcast_state=False)
      if my_rank == 0:
        if reference_state is not None:
          forecast_rmse = utility.calculate_rmse(forecast_state, reference_state)
          #
        if observation is None:
          observation_forecast_rmse = np.infty
        else:
          observation_forecast_rmse = utility.calculate_rmse(model.evaluate_theoretical_observation(forecast_state, forecast_state.time), observation)
      analysis_ensemble = self.analysis(forecast_ensemble, observation)
      self.filter_configs.update({'analysis_ensemble': analysis_ensemble})
      #
      analysis_state = self.get_analysis_state(bcast_state=False)
      if my_rank == 0:
        if reference_state is not None:
          analysis_rmse = utility.calculate_rmse(analysis_state, reference_state)
          final_rmse = analysis_rmse
        if observation is None:
          observation_rmse = np.infty
        else:
          observation_rmse = utility.calculate_rmse(model.evaluate_theoretical_observation(analysis_state, analysis_state.time), observation)
    else:
      # analysis first:
      # given forecast ensemble and observation at time tspan[0], generate analysis ensemble at tspan[0], then forecast_ensemble at time tspan[-1]
      init_ensemble = self.filter_configs['forecast_ensemble']
      #
      if init_ensemble is None:
        if my_rank == 0:
          print("With 'forecast_first' set to True, you must provide an 'forecast_ensemble' at tspan[0]")
          raise ValueError
        else:
          # Some imbalance or wasted resources
          print("This is process %d: I am not doing anything in the forecast step!!!" % my_rank)
          #
      forecast_state = self.get_forecast_state(bcast_state=False)
      if my_rank == 0:
        if reference_state is not None:
          initial_rmse = utility.calculate_rmse(forecast_state, reference_state)
          forecast_rmse = initial_rmse
        if observation is None:
          observation_forecast_rmse = np.infty
        else:
          observation_forecast_rmse = utility.calculate_rmse(model.evaluate_theoretical_observation(forecast_state, forecast_state.time), observation)

      analysis_ensemble = self.analysis(init_ensemble, observation)
      self.filter_configs.update({'analysis_ensemble': analysis_ensemble})
      #
      analysis_state = self.get_analysis_state(bcast_state=False)
      if my_rank == 0:
        if reference_state is not None:
          analysis_rmse = utility.calculate_rmse(analysis_state, reference_state)
        if observation is None:
          observation_rmse = np.infty
        else:
          observation_rmse = utility.calculate_rmse(model.evaluate_theoretical_observatio(analysis_state, analysis_state.time), observation)

      #
      forecast_ensemble = self.forecast(analysis_ensemble, tspan[0], tspan[-1])
      self.filter_configs['forecast_ensemble'] = forecast_ensemble

      if my_rank == 0:
        # march the reference_state
        if reference_state is not None:
          print("\n\n $$$$$$ Marching the reference state forward in time... from %f to %f $$$$$$\n\n" %(tspan[0], tspan[-1]))
          _, _traject = self.model.integrate_state(reference_state, [tspan[0], tspan[-1]], model_screen_output=self._verbose)
          reference_state = _traject[-1]
          self.reference_state = reference_state.copy()
          del _traject
          if self._verbose:
            print("DONE...\nNew reference state at time %s " % repr(self.reference_state.time))

      forecast_state = self.get_forecast_state(bcast_state=False)
      if my_rank == 0:
        if reference_state is not None:
          final_rmse = utility.calculate_rmse(forecast_state, reference_state)

    # Generate free-run if available
    if my_rank == 0:
      free_run_state = self.filter_configs['free_run_state']
      # march the free_run_state
      if free_run_state is not None:
        print("\n\n $$$$$$ Marching the Free-Run state forward in time... from %f to %f $$$$$$\n\n" %(tspan[0], tspan[-1]))
        _, _traject = self.model.integrate_state(free_run_state, [tspan[0], tspan[-1]], model_screen_output=self._verbose)
        self.free_run_state = _traject[-1].copy()
        if self._verbose:
          print("DONE...\nNew Free_run state at time %s " % repr(_traject[-1].time))
        del _traject


    # Analysis and Forecast steps are done...
    # Reference  and Free-Run states are also generated

    # Align and do Postprocessing (before saving results)
    if MPI_COMM is not None and comm_size > 1:
      MPI_COMM.Barrier()
      
    try:
      apply_post_processing = self.filter_configs['apply_post_processing']
    except:
      apply_post_processing = False
    if apply_post_processing:
      self._cycle_post_processing()

    # Update statistics:
    if my_rank == 0:
      try:
        self.output_configs['filter_statistics']
      except KeyError:
        self.output_configs.update({'filter_statistics':{}})
      self.output_configs['filter_statistics'].update({'forecast_rmse':forecast_rmse,
                                                       'analysis_rmse':analysis_rmse,
                                                       'observation_rmse':observation_rmse,
                                                       'observation_forecast_rmse':observation_forecast_rmse,
                                                       'initial_rmse':initial_rmse,
                                                       'final_rmse':final_rmse,
                                                      })
      if self._verbose:
        print("\n%s: Finishing The Assimilation Cycle... \n%s\n" % (self.filter_name, sep))
        sys.stdout.flush()
        #

      # Output Cycle Results to Screen
      if self.output_configs['scr_output']:
        self.print_cycle_results()
        #

    # All nodes must work on that, so that analyses are collected, and/or eanmble means
    # Save Cycle Results
    if self.output_configs['file_output']:
      file_output_directory, filter_statistics_dir, model_states_dir, observations_dir = self.save_cycle_results()
    else:
      file_output_directory = filter_statistics_dir = model_states_dir = observations_dir = None

    if self._verbose:
      print("***Rank %d done with filtering cycle***" % my_rank)
    elif my_rank == 0:
      print("filtering cycle is done...")
    else:
      pass
    

    return file_output_directory, filter_statistics_dir, model_states_dir, observations_dir

  def _pooled_sample_variance(self, ensemble):
    """
    Given a local ensemble, calculate the variance of the pooled ensemble; this is a collective operation
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      my_rank = 0
      comm_size = 1
    else:
      my_rank = MPI_COMM.rank
      comm_size =  MPI_COMM.size
    if comm_size == 1:
      variances = ensemble.variance()
    elif comm_size > 1:
      # pool the ensemble on the root node, and calculate the variance, then broadcast

      if ensemble is None:
        local_ensemble_size = 0
      else:
        local_ensemble_size = ensemble.size

      # 1- Rank 0 collects all ensemble ensemble membbers, and calculate variances
      rcv_ensemble_sizes = local_ensemble_size
      rcv_ensemble_sizes = MPI_COMM.gather(rcv_ensemble_sizes, root=0)

      if my_rank == 0:
        # Start with local ensemble, and append other members
        aggr_ensemble = ensemble.copy()
        #
        # gather ensemble members
        for node_rank in range(1, comm_size):
          for ens_ind in range(rcv_ensemble_sizes[node_rank]):
            vec = MPI_COMM.recv(source=node_rank, tag=node_rank+4100+ens_ind)
            aggr_ensemble.append(vec)
        #
        # Once done collecting; calculate variances
        # Remark; we could use pooled variances, but this would require covariances; which should be avioded!
        # Also, instead of collecting the whole ensemble, we could calculate aggregated mean, then gather only ensemble deviations communication-memory tradeoff!
        if aggr_ensemble.size == 0:
          variances = self.filter_configs['model'].state_vector()
          variances[:] = np.nan
        else:
          variances = aggr_ensemble.variance()
      else:
        # Now, send the ensemble members:
        for ens_ind in range(local_ensemble_size):
          MPI_COMM.send(ensemble[ens_ind], dest=0, tag=my_rank+4100+ens_ind)
        variances = None

      # broadcast variances to all nodes (just in case!)
      variances = MPI_COMM.bcast(variances, root=0)
    else:
      print("Empty COMM! How!")
      raise TypeError

    return variances

  def _cycle_post_processing(self):
    """
    Only called after an assimilation cycle
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      my_rank = 0
      comm_size = 1
    else:
      my_rank = MPI_COMM.rank
      comm_size =  MPI_COMM.size

    # TODO: Refactor for performance!
    # calculate forecast and analysis state, and monitor relative change
    xf = self.get_forecast_state()
    xa = self.get_analysis_state()
    #
    # calculated forecast and analysis variances/standard deviation and monitor forecast ensemble collapse/overdispersion
    ens = self.forecast_ensemble
    forecast_variance = self._pooled_sample_variance(ens)
    ens = self.analysis_ensemble
    analysis_variance = self._pooled_sample_variance(ens)

    if my_rank == 0:
      state_correction_ratio = xa.axpy(-1, xf, in_place=False)
      state_correction_ratio /= xf
      variance_correction_ratio = analysis_variance.axpy(-1, forecast_variance, in_place=False) 
      variance_correction_ratio /= forecast_variance
      #
      print("***\nEnKF State-Update i.e., Analysis-Forecast updated with:\n----\n State Change rate (xa-xf)/xf with statistics:")
      print("  Average    : %f "  % state_correction_ratio.mean())
      print("  StDev      : %f "  % state_correction_ratio.stdev())
      print("  Maximum    : %f "  % state_correction_ratio.max())
      print("  Minimum    : %f "  % state_correction_ratio.min())
      print("  L2-norm    : %f "  % state_correction_ratio.norm2())
      #
      print("----\n Ensemble Variance Trace:")
      print("  Forecast   : %f "  % forecast_variance.sum())
      print("  Analysis   : %f "  % analysis_variance.sum())
      print("----\n Variance update (var(xa)-var(xf))/var(xf) with statistics:")
      print("  Average    : %f "  % variance_correction_ratio.mean())
      print("  StDev      : %f "  % variance_correction_ratio.stdev())
      print("  Maximum    : %f "  % variance_correction_ratio.max())
      print("  Maximum    : %f "  % variance_correction_ratio.min())
      print("  L2-norm    : %f "  % variance_correction_ratio.norm2())
      print("***")
      res = dict(forecast_state=xf,
                 analysis_state=xa,
                 state_correction_ratio=state_correction_ratio,
                 forecast_variance=forecast_variance,
                 analysis_variance=analysis_variance,
                 variance_correction_ratio=variance_correction_ratio
                )
    else:
      res = None

    # No need to broadcast; only root need to update results and print things out!
    self.filter_configs.update({'post_processing_results':res})
    # align...
    if MPI_COMM is not None:
      MPI_COMM.Barrier()
    #  return res  # No need to fill the name space by returning this.


  def forecast(self, ensemble, t0, t1, in_place=False):
    """
    Given an ensemble, propagagte each member of the ensemble from time t0 to time t1 using the passed model
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      my_rank = 0
      comm_size = 1
    else:
      my_rank = MPI_COMM.rank
      comm_size =  MPI_COMM.size

    if ensemble is None:
      if my_rank == 0:
        print("Forecast ensemble is None! Can't perform forecast!")
        raise ValueError
      else:
        print("WARNING: This is Node <<%d>>; No ensemble is passed to be forecasted!." % my_rank)
        print("Nothing to be done on this node. WASTED resources!")
    else:
      pass

    if self._verbose and my_rank==0:
      sep = "-"* 70
      print("\n%s\n%s: Starting Forecast Step... \n" % (self.filter_name, sep))
      sys.stdout.flush()
    else:
      pass

    if in_place:
      prop_ensemble = ensemble
    else:
      prop_ensemble = ensemble.copy()
      #
    ens_size = prop_ensemble.size
    for i in range(ens_size):
      print("Forecast Step: Node [%2d/%2d] Propagating Ensemble Member [%3d/%3d] over the time interval %s    " % (my_rank, comm_size, i+1, ens_size, repr([t0, t1])))
      sys.stdout.flush()
      out = self.model.integrate_state(prop_ensemble[i], [t0, t1], model_screen_output=self._verbose)
      prop_ensemble[i] = out[1][-1]

    if MPI_COMM is not None:
      MPI_COMM.Barrier()

    if self._verbose:
      sep = "-"* 70
      print("\n%s: Finishing Forecast Step on NODE [%d]... \n%s\n" % (self.filter_name, my_rank, sep))
      sys.stdout.flush()
    elif my_rank==0 and comm_size==1:
      print("\n")
    else:
      pass

    return prop_ensemble

  def analysis(self, local_forecast_ensemble, observation, all_to_numpy=True, parallel_EnKF=False, use_linearized_H=False):
    """
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      comm_size = 1
      my_rank = 0
    else:
      comm_size = MPI_COMM.size
      my_rank = MPI_COMM.rank

    if MPI_COMM is not None:
      MPI_COMM.Barrier()

    print("Node %d entering Analysis stage with %d forecast ensembble members " % (my_rank, local_forecast_ensemble.size))
    if comm_size == 1 and parallel_EnKF:
      # just-in-case
      parallel_EnKF = False

    # If no observations passed to this node, return a copy of the passed local_forecast_ensemble
    if observation is None:
      # no assimilation required
      if local_forecast_ensemble is None:
        local_analysis_ensemble = None
      else:
        local_analysis_ensemble = local_forecast_ensemble.copy()
      return local_analysis_ensemble

    if not parallel_EnKF:
      # Serial EnKF:

      if self._verbose and my_rank==0:
        sep = "-"* 70
        print("\n%s\n%s: Starting Analysis Step... \n" % (self.filter_name, sep))
        print("Forecast first: %s " % self.forecast_first)
        print("Cycle Timespan: %s \n" % repr(self.timespan))
        if observation is not None:
          print("Observation time: " % observation.time)
      elif my_rank==0:
        print("%s: Analysis Step..." % (self.filter_name))
      sys.stdout.flush()

      # local_ensemble_size on all nodes
      if local_forecast_ensemble is None:
        local_ensemble_size = 0
      else:
        local_ensemble_size = local_forecast_ensemble.size

      # 1- Rank 0 collects all ensemble forecast ensemble membbers, and carry-out the analysis, and then distribute members back
      rcv_ensemble_sizes = local_ensemble_size
      rcv_ensemble_sizes = MPI_COMM.gather(rcv_ensemble_sizes, root=0)

      if my_rank == 0:
        #
        print("Root", rcv_ensemble_sizes)
        if local_ensemble_size==0:
          print("The root node DOES NOT accept empty ensembles for assimilation!")
          raise ValueError
        # Start with local ensemble, and append other members
        forecast_ensemble = local_forecast_ensemble
        #
        # gather ensemble members
        for node_rank in range(1, comm_size):
          for ens_ind in range(rcv_ensemble_sizes[node_rank]):
            vec = MPI_COMM.recv(source=node_rank, tag=node_rank+4000+ens_ind)
            forecast_ensemble.append(vec)
      else:
        # Now, send the ensemble members:
        for ens_ind in range(local_ensemble_size):
          MPI_COMM.send(local_forecast_ensemble[ens_ind], dest=0, tag=my_rank+4000+ens_ind)

      #
      # 2- Rank 0 does all analysis work:
      if my_rank == 0:
        # Collect forecast enemble
        # get reference to model object:
        model = self.filter_configs['model']

        if not all_to_numpy:
          # Use LinAlg submodule (after finishing it), or use PETSc
          print("Only Numpy-based implementation is currently supported. This is a TODO!")
          raise NotImplementedError
          #
        else:
          print("ROOT NODE started analysis with %d ensemble members" % forecast_ensemble.size)
          # PREPARE for ASSIMILATION:
          # ---------------------------
          # Get and inflate the forecast ensemble
          Xf = forecast_ensemble  # .copy()
          alpha = self.filter_configs['forecast_inflation_factor']
          Xf.inflate(alpha, in_place=True)
          ensemble_size = Xf.size

          # Generate the background state:
          xb = Xf.mean()
          state_size = xb.size

          # copy observation vector
          y = observation.copy()
          obs_size = y.size
          obs_time = observation.time

          # Get noise information
          obs_err_model = self.observation_error_model
          sqrtR_inv_VecProd = obs_err_model.sqrtR_inv_VecProd  # A function to evaluate R^(-1/2) * * x, where x could be StateVector, or np.ndarray of comformable shape (e.g. Nobs x *)
          sqrtR_VecProd = obs_err_model.sqrtR_VecProd  # A function to evaluate R^(-1/2)

          print("Getting matrix of ensemble anomalies"),
          # Matrix of ensemble anomalies:
          A = Xf.get_numpy_array()
          xf = xb.get_numpy_array()
          for i in range(ensemble_size):
            A[:, i] -= xf
          print("done")

          # Model-observation (and associated mean) of the forecast ensemble members:
          if use_linearized_H:
            HA = np.empty((obs_size, ensemble_size))
            state = model.state_vector()
            for ens_ind in range(ensemble_size):
              state[:] = A[:]
              HA[:, ens_ind] = model.observation_operator_Jacobian_VecProd(eval_state=xb, state=state, t=obs_time).get_numpy_array()
          else:
            HA = np.empty((obs_size, ensemble_size))
            # HA[...] = np.NaN
            for ens_ind in range(ensemble_size):
              obs_vec = model.evaluate_theoretical_observation(forecast_ensemble[ens_ind], obs_time)
              HA[:, ens_ind] = obs_vec.get_numpy_array()
            del obs_vec
            Hx = np.squeeze(np.mean(HA, 1))
            # Convert to matrix of observations anomalies
            for i in range(ensemble_size):
              HA[:, i] -= Hx
            del Hx

          # observation innovation: y - H(xb)
          Hxb = model.evaluate_theoretical_observation(xb, obs_time)

          obs_innov = y.axpy(-1.0, Hxb, in_place=False)
          # print("xb:", xb)
          # print("Hxb: ", Hxb)
          # print("y: ", y)
          # print("obs_innov:", obs_innov)

          # observation covariance scaling factor:
          rfactor = float(self.filter_configs['obs_covariance_scaling_factor'])

          # get the current state of the random number generator
          curr_rnd_state = np.random.get_state()

          # START ASSIMILATION:
          # -------------------
          print("Constructing S, s "),
          s = sqrtR_inv_VecProd(obs_innov, in_place=False).scale(1.0 / np.sqrt(ensemble_size-1.0)).get_numpy_array()
          S = sqrtR_inv_VecProd(HA, in_place=False) / np.sqrt(ensemble_size-1.0)
          print("Done.")


          localize_covariances = self.filter_configs['localize_covariances']
          if not localize_covariances:
            print("NO localization; proceed with analysis...")
            # DONOT Apply covariance localization (Global analysis)

            # Generate the random observations (global perturbations of observations for the EnKF)
            D = np.random.randn(obs_size, ensemble_size)
            D *= 1.0/(rfactor * np.sqrt(ensemble_size-1.0))
            d = np.mean(D, 1)
            for ens_ind in range(ensemble_size):
              D[:, ens_ind] -= d
            del d
            D *= np.sqrt(ensemble_size / (ensemble_size-1.0))
            # Restore the state of the gaussian random number generator:
            np.random.set_state(curr_rnd_state)

            if ensemble_size <= obs_size:
              # Calculte G = (I + (S^T * S))^{-1} * S^T
              G = S.T.dot(S)
              G[np.diag_indices_from(G)] += 1.0
              G = np.linalg.inv(G).dot(S.T)  # TODO: <<-- User Iterative solver
            else:
              # Calculte G = S^T * (I + S * S^T)^{-1}
              G = S.dot(S.T)
              G[np.diag_indices_from(G)] += 1.0
              G = S.T.dot(np.linalg.inv(G))  # TODO: <<-- User Iterative solver

            # Evaluate the Ensemble-Mean update:
            xf_update = A.dot(G).dot(s)  # dx
            del s

            # Evaluate Ensemble-Anomalies update:
            if rfactor != 1.0:
              # rescale S, and G
              S *= 1.0 / np.sqrt(rfactor)
              if ensemble_size <= obs_size:
                # RE-Calculte G = (I + (S^T * S))^{-1} * S^T
                G = np.dot(S.T, S)
                G[np.diag_indices_from(G)] += 1.0
                G = np.dot(np.linalg.inv(G), S.T)
              else:
                # RE-Calculte G = S^T * (I + S * S^T)^{-1}
                G = S.dot(S.T)
                G[np.diag_indices_from(G)] += 1.0
                G = S.T.dot(np.linalg.inv(G))
            else:
              pass

            # Now Evaluate A = A * (I - 0.5 * G * S):
            D -= S
            G = G.dot(D)
            del D
            G[np.diag_indices_from(G)] += 1.0
            A = A.dot(G)  # Analysis Ensemble of Anomalies
            del G
            print("done.")

          else:
            # Apply covariance localization
            localization_method = self.filter_configs['localization_method']
            localization_function = self.filter_configs['localization_function']
            localization_radius = self.filter_configs['localization_radius']
            #
            if re.match(r'\Acovariance(-|_)*filtering\Z', localization_method, re.IGNORECASE):
              # Evaluate the Kalman gain matrix (with HPH^T, and PH^T localized based on the filter settings)
              K = self._calc_Kalman_gain(A, HA)

              # Evaluate the Ensemble-Mean update (dx):
              xf_update = K.dot(obs_innov.get_numpy_array())

              # Recalculate the Kalman gain with observation variances/covariances multiplied by rfactor
              if rfactor != 1:
                K = self._calc_Kalman_gain(A, HA, rfactor)

              # get the current state of the random number generator
              curr_rnd_state = np.random.get_state()
              # Generate the random observations (global perturbations of observations for the EnKF)
              D = np.random.randn(obs_size, ensemble_size)
              D *= np.sqrt(rfactor)
              D = sqrtR_VecProd(D)
              d = np.mean(D, 1)
              for ens_ind in range(ensemble_size):
                D[:, ens_ind] -= d
              del d
              D *= np.sqrt(ensemble_size / (ensemble_size-1.0))
              # Restore the state of the gaussian random number generator:
              np.random.set_state(curr_rnd_state)

              # Now Evaluate A = A + K * (D - HA):
              D -= HA
              # del HA
              A += K.dot(D)
              # del D

            else:
              print("Localization method '%s' is not Supported/Recognized!" % localization_method)
              raise ValueError
            #
            # Now we are good to go; update the ensemble mean, and ensemble-anomalies using ens_mean_update, and A

          if self.output_configs['debug_mode']:
            print(" xf, xf_updat: ", xf, xf_update)
            print("obs_innov: ", obs_innov)
            print(" K: ", K)

          # Inflate analysis ensemble (Xa <- analysis_ensemble)
          alpha = self.filter_configs['analysis_inflation_factor']
          if alpha > 1.0:
            print("Inflating analysis ensemble... "),
            A *= alpha
            print("done.")

          # update ensemble mean:
          xa = xf + xf_update
          # analysis_state = model.state_vector()
          # analysis_state[:] = xa[:]
          #
          analysis_ensemble = forecast_ensemble.copy_empty()
          member = model.state_vector()
          for ens_ind in range(ensemble_size):
            member[:] = A[:, ens_ind] + xa[:]
            analysis_ensemble.append(member.copy())

      else:
        # Analysis is carried out on root node only; move on to receiving local aalysis ensemble members
        pass

      # Sync all ensemble sizes
      # Node 0 scatters ensemble mebers
      if my_rank == 0:

        # Local ensemble sizes
        dist_ens_sizes = rcv_ensemble_sizes  # send back same number of members
        local_analysis_ensemble = analysis_ensemble
        print("ROOT about to send back the analysis ensemble; I have %d members" % local_analysis_ensemble.size)
        # Now, distribute ensmbles
        for node_rank in range(comm_size-1, 0, -1):
          out_ens_size = dist_ens_sizes[node_rank]
          if out_ens_size > 0:
            for ens_ind in range(out_ens_size):
              vec = local_analysis_ensemble.pop()
              _ = forecast_ensemble.pop()  # same for forecast ensemble
              MPI_COMM.send(vec, dest=node_rank, tag=node_rank+6000+ens_ind)
              if self._verbose:
                print("ROOT:VVV: Now, I have %d members" % local_analysis_ensemble.size)
                sys.stdout.flush()
      else:
        # Collect ensembles; start with ensemble size
        local_analysis_ensemble = None
        print("Node %d; receiving %d ensemble members" % (my_rank, local_ensemble_size))
        if local_ensemble_size > 0:
          # Now, receive ensemble members
          for ens_ind in range(local_ensemble_size):
            # receive a state:
            vec = MPI_COMM.recv(source=0, tag=my_rank+6000+ens_ind)
            if ens_ind == 0:
              # initialize an ensemble to the given state
              local_analysis_ensemble = Ensemble(0, vec.size)
            local_analysis_ensemble.append(vec)
            if self._verbose:
              print("NODE %d:>>>: Now, I have %d members" % (my_rank, local_analysis_ensemble.size))
              sys.stdout.flush()

    else:  # parallel EnKF
      print("Parallel EnKF is YET to be implemented")
      raise NotImplementedError

    #
    if MPI_COMM is not None:
      MPI_COMM.Barrier()

    #
    if self._verbose and my_rank==0:
      print("\n%s: Finishing Analysis Step... \n%s\n" % (self.filter_name, sep))
      sys.stdout.flush()

    return local_analysis_ensemble
    #

  cpdef _calc_Kalman_gain(self, np.ndarray in_A, np.ndarray in_HA, float rfactor=1.0, bint ignore_nans=True):
    """
    Calculate and return Kalman gain.
    All matrices passed and returned from this function are Numpy-based

    Args:
      A: Forecast Ensemble Anomalies matrix [forecast ensemble members - ensemble mean]
      HA: Forecast Ensemble Observations Anomalies matrix [HE(e) - Hx], e = 1,2,..., ensemble_size

    Returns:
      K: Kalman gain matrix

    """
    # TODO: This will be transformed into LinearOperator soon for efficiency

    cdef int row_ind, batch_length  # faster loop/cython
    cdef int i, j, v
    cdef np.ndarray HA, A
    #
    if self._verbose:
      sep = "~" * 25
      print("\n%s\n%s: Constructing Kalman Gain Matrix (InEfficient for now)... \n" % (self.filter_name, sep))
      sys.stdout.flush()

    # Retrieve model, and dimensionalities:
    model = self.filter_configs['model']
    state_size = model.state_size()
    ensemble_size = in_A.shape[1]
    obs_size = model.observation_size()

    # Get localization info:
    localize_covariances = self.filter_configs['localize_covariances']

    A = in_A.copy()  # TODO: Remove this, after debugging
    #
    nan_rows = non_nan_rows = None
    if ignore_nans:
      nan_locs = np.isnan(in_HA)
      if nan_locs.any():
        nan_rows, _ = np.where(nan_locs)
        non_nan_rows, _ = np.where(~nan_locs)
        #
        nan_rows = np.unique(nan_rows)
        non_nan_rows = np.unique(non_nan_rows)
        #
    #
    if self.output_configs['debug_mode']:
      # Check xb for Nans
      print("\nKALMAN GAIN CALCULATIONS\n***")
      #
      print("A.shape", np.shape(A))
      print("in_HA.shape", np.shape(in_HA))
      #
      if np.isnan(A).any():
        print("A has NANs")
      else:
        print("A has NANs: NO")

      if np.isnan(in_HA).any():
        print("in_HA has NANs? YES")
        non_nans = np.where(~np.isnan(in_HA))
        print("Non NaN values at entries:")
        print(non_nans)
        print("Values are: ")
        print(in_HA[non_nans])
      else:
        print("in_HA has NANs? NO")
      print("\n***")
      pass
    #

    # Extract valid entries only
    if ignore_nans and non_nan_rows is not None :
      HA = in_HA[non_nan_rows, :].copy()
    else:
      HA = in_HA

    # Calculate Kalman Gain, and carry out localization if needed:
    print("Constructing HPHT, and PHT"),
    cor_fac = float(ensemble_size) - 1.0
    HPHT = np.dot(HA, HA.T)
    HPHT /= cor_fac
    PHT = A.dot(HA.T)
    PHT /= cor_fac
    print("done.")

    #
    if self.output_configs['debug_mode']:
      print("\nKALMAN GAIN CALCULATIONS\n***")
      print("HPHT.shape", HPHT.shape)
      print("A.shape", np.shape(A))
      print("HA.shape", np.shape(HA))
      print("PHT.shape", PHT.shape)
      print("\n***")
      pass
    #

    if localize_covariances:
      print("Retrieve model, and observation grids"),
      # Get model and observational grid ((x,y,z) coordinates)
      model_grid = model.model_grid()  #  (Nx*Ny*Nz) x 3 np array, with each row giving a xyz combination of one grid point
      # print("model_grid", model_grid)
      obs_grid = model.observation_grid()  # Same as model grid, but representing observation grid points
      print("done.")
      #
      model_grid_size = model_grid.shape[0]
      obs_grid_size = obs_grid.shape[0]
      #
      try:
        model_nvars = model.get_model_configs(key='nvars')
      except:
        print("Failed to retrieve model number of prognostic variables! Trying here...")
        model_nvars = state_size // model_grid.shape[0]
      try:
        obs_nvars = model.get_observation_configs(key='nvars')
      except:
        print("Failed to retrieve observation number of prognostic variables! Trying here...")
        obs_nvars = obs_size // obs_grid.shape[0]
        #
      # Localization coefficients can be preevaluated and stored
      print("Localization started... ")
      # Apply covariance localization (to PHT, and HPHT) if requested (assuming a 3d model):
      loc_fun = self.filter_configs['localization_function']
      loc_rad = self.filter_configs['localization_radius']
      #
      # TODO: Here is an initial trial; after developing it, move to a local utility function/method
      localization_matrices_repo = self.filter_configs['localization_matrices_repo']
      matching_decorr_file = self._get_decorr_mat_file(localization_matrices_repo, model_grid, obs_grid, loc_fun, loc_rad, file_ext='hdf5', use_h5py=use_h5py)
      #
      # TODO: WIP: Refactor to store only coefficients based on grid points to save space, and computations
      # ===================================================================================================
      #
      if ignore_nans and non_nan_rows is not None:
        target_cols = non_nan_rows[(non_nan_rows % obs_nvars) == 0] // obs_nvars
      else:
        target_cols = range(0, obs_grid_size)
        #
      if self.output_configs['debug_mode']:
        print("....")
        print("non_nan_rows of HPHT are: ", non_nan_rows)
        print("The number of observation prog. variables is %d " % obs_nvars)
        print("Ignore NaN indexes? %s" % ignore_nans) 
        print("Will access following columns in PHT and rows/columns in HPHT:>>[*obs_nvars:*(obs_nvars+1)]<<  ", target_cols)
        print("....")
        #
      if matching_decorr_file is not None and not use_h5py:
        # ---------------------------------------------------------------------------------------------------
        print("H5Py is not available! Can't read the localization matrices; please install h5py!")
        print("Will reconstruct the decorrelation matrices on the fly ... This will take some time ...")
        matching_decorr_file = None
        #
      # print("... matching_decorr_file ... ", matching_decorr_file)
      # Now, localize
      if matching_decorr_file is not None:  # implicitly means use_h5py
        mat_decorr_file = matching_decorr_file
        #
        # Masking PHT
        with h5py.File(os.path.join(localization_matrices_repo, mat_decorr_file), 'r') as f_id:
          print(" Masking PHT... (Load Mode)"),
          PHT_Decorr = f_id["Decorr/PHT"][...]
          #
          if not (np.size(PHT_Decorr, 1) == obs_grid_size) and (np.size(PHT_Decorr, 0) == model_grid_size):
            print("The localization coefficients matrix has shape different from PHT!")
            print("C.shape: ", PHT_Decorr.shape)
            print("PHT.shape: ", PHT.shape)
            raise ValueError
          #
        # Loop over columns (and skip Columns corresponding to NaN indexes)
        print("model_nvars, obs_nvars, obs_grid_size")
        print(model_nvars, obs_nvars, obs_grid_size)
        for col_ind in target_cols:
          coeffs = PHT_Decorr[:, col_ind]
          for var_ind in range(model_nvars):
            for j in range(obs_nvars):
              # print("Accessing [%d: %d, %d]  in PHT " % (var_ind*model_grid_size, model_grid_size*(var_ind+1),  col_ind*obs_nvars+j))
              PHT[var_ind*model_grid_size: model_grid_size*(var_ind+1), col_ind*obs_nvars+j] *= coeffs
          # print("."),
        # print
        #
        # Mask HPHT
        with h5py.File(os.path.join(localization_matrices_repo, mat_decorr_file), 'r') as f_id:
          print(" Masking HPHT... (Load Mode)"),
          HPHT_Decorr = f_id["Decorr/HPHT"][...]
          #
          if not (HPHT_Decorr.shape == HPHT_Decorr.shape == (obs_grid_size, obs_grid_size)):
            print("The localization coefficients matrix has shape different from HPHT!")
            print("C.shape: ", HPHT_Decorr.shape)
            print("PHT.shape: ", HPHT.shape)
            raise ValueError
        #
        #
        # stride = np.arange(obs_grid_size) * obs_nvars
        for row_ind in target_cols:
          coeffs = HPHT_Decorr[row_ind, target_cols].repeat(obs_nvars)  # HPHT_Decorr rows with each entry repeated #-times = obs.prog.variables
          for j in range(obs_nvars):
            # HPHT[stride[row_ind: ]+j, row_ind*obs_nvars+j] *= coeffs[row_ind: ]
            HPHT[row_ind+j, :] *= coeffs
          # print("."),
        # print
        # ---------------------------------------------------------------------------------------------------
      else:
        # Couldn't find matching file, and decorrelation matrices couldn't be created-to-be saved;  create on the fly, and use
        PHT_Decorr, HPHT_Decorr = self.build_localization_matrices(model_grid, obs_grid, loc_fun, loc_rad, save_to_file=False)
        #
        # Masking PHT
        if PHT_Decorr.shape != PHT.shape:
          print("The localization coefficients matrix has shape different from PHT!")
          print("C.shape: ", PHT_Decorr.shape)
          print("PHT.shape: ", PHT.shape)
          raise ValueError
        # Loop over columns
        for col_ind in target_cols:
          coeffs = PHT_Decorr[:, col_ind]
          for var_ind in range(model_nvars):
            for j in range(obs_nvars):
              PHT[var_ind*model_grid_size: model_grid_size*(var_ind+1), col_ind*obs_nvars+j] *= coeffs
          # print("."),
        # print
        #
        # Masking HPHT
        # stride = np.arange(obs_grid_size) * obs_nvars
        for row_ind in target_cols:
          coeffs = HPHT_Decorr[row_ind, target_cols].repeat(obs_nvars)  # HPHT_Decorr rows with each entry repeated #-times = obs.prog.variables
          for j in range(obs_nvars):
            # HPHT[stride[row_ind: ]+j, row_ind*obs_nvars+j] *= coeffs[row_ind: ]
            HPHT[row_ind+j, :] *= coeffs
          # print("."),
        # print
        # ---------------------------------------------------------------------------------------------------
        #
      print("done.")
      # ===================================================================================================
      #
    else:
      # No localization;
      pass

    # HPHT_Decorr and PHT_Decorr are in the full size, regardless NaNs exist or no in HPHT/PHT.

    #
    if self.output_configs['debug_mode']:
      print("\nKALMAN GAIN CALCULATIONS\n***")
      if np.isnan(HPHT).any():
        print("HPHT has nans? YES")
        print(np.where(np.isnan(HPHT)))
      else:
        print("HPHT has nans? NO")
      print("\n***")
      pass
    #


    # Now formulate the Kalman gain K = PHT (HPHT + R)^{-1}
    obs_err_model = self.observation_error_model

    # R = obs_err_model.R.get_numpy_array()  # TODO after implementing ErrorModel classes (see forward_model for now!)
    R = obs_err_model.R.toarray() #
    if ignore_nans and non_nan_rows is not None:
      R = R[:, non_nan_rows][non_nan_rows, :]
      #
      if rfactor != 1 :
        R *= rfactor
      K = np.zeros((state_size, obs_size))
      K[:, non_nan_rows] = PHT.dot(np.linalg.inv(HPHT+R))
    else:
      #
      if rfactor != 1 :
        R *= rfactor
      K = PHT.dot(np.linalg.inv(HPHT+R))
    #
    if self.output_configs['debug_mode']:
      print("Observation Error Covariance Matrix:", R)
      print("\nKALMAN GAIN CHECK\n***")
      print("Max, Min entry of K: ", K.max(), K.min())
      if np.isnan(K).any():
        print("K has nans? , YES")
        print(np.where(np.isnan(K)))
        print("\n")
        if np.isnan(R).any():
          print("R Has NaNs? YES")
        else:
          print("R Has NaNs? NO")
        # Check non-NANs
        non_nans = np.where(~np.isnan(K))
        print("Non NaN values at entries:")
        print(non_nans)
        print("Values are: ")
        print(K[non_nans])
      else:
        print("K has nans? NO")
      print("Returning K with shape: ", np.shape(K))
      print("\n***")
      pass
    #
    #
    # print("type(HPHT): ", type(HPHT))
    # print("type(R): ", type(R))
    # print("type(K): ", type(K))
    # print("K.shape: ", K.shape)
    if self._verbose:
      print("%s: Kalman Gain Matrix is ready ... \n%s\n" % (self.filter_name, sep))
      sys.stdout.flush()
    #
    return K

  def _get_decorr_mat_file(self, localization_matrices_repo, model_grid, obs_grid, loc_fun, loc_rad, file_ext=None, construct_ifnot_found=True, use_h5py=True):
    """
    """
    decorr_mat_files = []
    _decorr_mat_files = os.listdir(localization_matrices_repo)
    for f_name in _decorr_mat_files:
      f_path = os.path.join(localization_matrices_repo, f_name)
      if os.path.isfile(f_path):
        if file_ext is not None:
          if not f_name.lower().endswith('.%s' % file_ext.lower().strip('. ')):
            continue
        decorr_mat_files.append(f_path)

    # TODO: we need faster lookup to find matching localization matrices; otherwise, add config entry with path to loca-mats!
    matching_decorr_file = None
    if not use_h5py:
      # h5py is not available; will reconstruct the decorrelation matrices!
      # consider pickle!
      pass
    else:
      for f_name in decorr_mat_files:
        if self._verbose:
          print("Inspecting: %s " % f_name),
        try:
          with h5py.File(f_name, 'r') as f_cont:
            f_model_grid = f_cont["Grids/model_grid"][...]
            f_obs_grid = f_cont["Grids/obs_grid"][...]
            f_loc_fun = f_cont["Decorr"].attrs['loc_fun']
            f_loc_rad = f_cont["Decorr"].attrs['loc_rad']
            #
            if not(loc_rad==f_loc_rad and loc_fun.lower()==f_loc_fun.lower()):
              print("Mismatch")
              sys.stdout.flush()
              continue
            if model_grid.shape != f_model_grid.shape:
              print("Mismatch")
              sys.stdout.flush()
              continue
            if obs_grid.shape != f_obs_grid.shape:
              print("Mismatch")
              sys.stdout.flush()
              continue

            if np.isclose(model_grid, f_model_grid).all() and np.isclose(obs_grid, f_obs_grid).all():
              matching_decorr_file = f_name
              print("Match")
              sys.stdout.flush()
              break
            else:
              print("Mismatch")
              sys.stdout.flush()
            #
        except(IOError, KeyError):
          print("Key/IO Error incurred! Moving forward to another file")
          sys.stdout.flush()
          continue  # no need!
    # print("matching_decorr_file  << ", matching_decorr_file)

    if matching_decorr_file is None:
      if construct_ifnot_found:
        mat_decorr_file = utility.try_file_name(localization_matrices_repo, u"Decorr_Mat", 'hdf5', return_abspath=True)
        matching_decorr_file = self.build_localization_matrices(model_grid, obs_grid, loc_fun, loc_rad, save_to_file=True, file_name=mat_decorr_file, use_h5py=use_h5py)
      else:
        # The following is an overkill now!
        print("Failed to find a decorrelation matrix in the repo that matches the passed coordinates; Turn 'construct_ifnot_found' ON if you want to reconstruct them")

    return matching_decorr_file


  def build_localization_matrices(self, model_grid, obs_grid, loc_fun, loc_rad, save_to_file=True, file_name=None, use_h5py=True):
    """
    Construct the decorrelation matrices for PHT, and HPHT, and optionally save them to file; you can load them one at a time, or parts, to avoid filling memory
    if save_to_file is False, both matrices are returned
    """
    if save_to_file and file_name is None:
      print("You must pass a valid file name/path, if you want to save the decorrelation matrix!")
      raise ValueError
    else:
      mat_decorr_file = file_name  # won't matter if it's not None, and save_to_file is False

    # Construct the localization kernel, and save it
    model = self.filter_configs['model']
    state_size = model.state_size()
    obs_size = model.observation_size()
    #
    try:
      model_nvars = model.get_model_configs(key='nvars')
    except:
      print("Failed to retrieve model number of prognostic variables! Trying here...")
      model_nvars = state_size // model_grid.shape[0]
    try:
      obs_nvars = model.get_observation_configs(key='nvars')
    except:
      print("Failed to retrieve observation number of prognostic variables! Trying here...")
      obs_nvars = obs_size // obs_grid.shape[0]

    # TODO: WIP: Refactored to store localization ignoring the prognostic variables...
    #
    print(" Masking PHT... (Create Mode)"),
    print(state_size, obs_size, model_nvars, state_size//model_nvars)
    if False: # TODO: remove False branch after refactoring
      PHT_Decorr = np.zeros((state_size, obs_size))
    else:
      PHT_Decorr = np.zeros((state_size//model_nvars, obs_size//obs_nvars))
    dists = np.empty(state_size//model_nvars, dtype=np.float32)
    coeffs = dists  # aliasing
    for i in range(obs_size//obs_nvars):
      self._c_dists(obs_grid[i, 0], obs_grid[i, 1], obs_grid[i, 2], model_grid, dists)
      coeffs = self.localization_coefficients(dists, loc_rad, loc_fun, coeffs)  # see if coeffs should be copied!
      if False: # TODO: remove False branch after refactoring
        for j in range(state_size//model_nvars):
          PHT_Decorr[j*model_nvars: (j+1)*model_nvars, i*obs_nvars:(i+1)*obs_nvars] = coeffs[j]
      else:
        PHT_Decorr[:, i] = coeffs[:]
      print("Constructing PHT", i, coeffs)
      # print("."),
    # print

    # save decorrelation matrix
    if save_to_file:
      if use_h5py:
        with h5py.File(mat_decorr_file, 'w') as f_id:
          f_id.create_group(u"Grids")
          _ds = f_id.create_dataset(u"Grids/model_grid", data=model_grid)
          _ds = f_id.create_dataset(u"Grids/obs_grid", data=obs_grid)
          #
          decorr_data = f_id.create_group(u"Decorr")
          decorr_data.attrs[u"loc_fun"] = u"%s" % loc_fun
          decorr_data.attrs[u"loc_rad"] = loc_rad
          _ds = decorr_data.create_dataset(u"PHT", data=PHT_Decorr)
        del PHT_Decorr
      else:
        print("Only h5py is currently supported; Will return the constructed decorrelation matrices")
        save_to_file = False
        mat_decorr_file = None
        # raise ValueError
    #
    print(" Masking HPHT... (Create Mode)"),
    # print(obs_size, obs_nvars, obs_size//obs_nvars)
    if False: # TODO: remove False branch after refactoring
      HPHT_Decorr = np.zeros((obs_size, obs_size))
    else:
      HPHT_Decorr = np.zeros((obs_size//obs_nvars, obs_size//obs_nvars))
    dists = np.empty(obs_size//obs_nvars, dtype=np.float32)
    coeffs = dists  # aliasing
    for i in range(obs_size//obs_nvars):
      self._c_dists(obs_grid[i, 0], obs_grid[i, 1], obs_grid[i, 2], obs_grid, dists)
      coeffs = self.localization_coefficients(dists, loc_rad, loc_fun, coeffs)  # see if coeffs should be copied!
      if False: # TODO: remove False branch after refactoring
        for j in range(i, obs_size//obs_nvars-1):
          for v in range(obs_nvars):
              HPHT_Decorr[j+v, (j+1)*obs_nvars: (j+2)*obs_nvars] = coeffs[j]
              HPHT_Decorr[(j+1)*obs_nvars: (j+2)*obs_nvars, j+v] = coeffs[j]
      else:
        # no need to overwrite the diagonal!
        HPHT_Decorr[i, i:] = coeffs[i: ]
        HPHT_Decorr[i:, i] = coeffs[i: ]
      print("Constructing HPHT", i, coeffs[i: ])
      # print("."),
    # print
    # save decorrelation matrix
    if save_to_file:
      if use_h5py:
        print("...Writing the HPHT_Decorr...")
        with h5py.File(mat_decorr_file, 'r+') as f_id:
          #
          decorr_data = f_id["Decorr"]
          _ds = decorr_data.create_dataset(u"HPHT", data=HPHT_Decorr)
          print("done.")
        del HPHT_Decorr
      else:
        pass
        # raise ValueError
        #
    if save_to_file:
      return mat_decorr_file
    else:
      return PHT_Decorr, HPHT_Decorr


  cdef int _c_dists(self, double x, double y, double z, np.ndarray grid_points, np.ndarray dists) except -1:
    """
    dists is a numpy array of 3 columns x:y:z to be matched to the passed x, y, z coordinates
    """
    cdef int i
    cdef double x2, y2, z2

    cdef int out = -1
    if grid_points.shape[0] != dists.size:
      print("grid_points.shape[0] != dists.size !" )
      print("grid_points.shape[0] = %d" %(grid_points.shape[0]))
      print("dists.size = %d" %(dists.size))
      print("Can't do it! Terminating _c_dists ...")
      return out
    else:
      pass

    if grid_points.shape[1] != 3:
      print("Passed grid MUST have three columns! ")
      print("Can't do it! Terminating _c_dists ...")
      return out
    else:
      pass

    for i in range(dists.size):
      x2, y2, z2 = grid_points[i, :]
      # print("x, y, z: %f, %f, %f " %(x, y, z) )
      # print("x2, y2, z2: %f, %f, %f " %(x2, y2, z2) )
      # print("\n")
      dists[i] = sqrt(pow(x-x2, 2) + pow(y-y2, 2) + pow(z-z2, 2))
    out = 0
    return out

  def update_statistics(self):
    """
    Calculate statistics of the filter, and append them to the fitler_statistics dictionary
    """
    raise NotImplementedError

  def localization_coefficients(self, dist, radius, method, in_coeffs=None):
    """
    Calculate the localization coefficients of passed dist, radius
    """
    #
    if re.match(r'\Agauss', method, re.IGNORECASE):
      fun = 1
    elif re.match(r'\A((G(-|_)*C)|(Gaspari(-|_)*cohn))\Z', method, re.IGNORECASE):
      fun = 2
    else:
      print("Only Gauss and Gaspari-cohn are currently supported")
      raise ValueError

    if np.isscalar(dist) and np.isscalar(radius):
      coeffs = self.c_localization_coeff(<double>dist, <double>radius, <int>fun)
    elif isiterable(dist):
      _dist = np.asarray(dist[:]).flatten()
      if in_coeffs is not None:
        assert in_coeffs.size == _dist.size, "Passed in_coeffs must have size equal to dist.size!"
        coeffs = in_coeffs
      else:
        coeffs = np.zeros(_dist.size)

      if np.isscalar(radius):
        out = self.c_localization_coeffs(_dist, <double>radius, coeffs, <int>fun)
        if out == -1:
          print("Failed inside c_localization_coeffs!")
          raise ValueError
      elif isiterable(radius):
        radii = np.asarray(radius).flatten()
        self.c_localization_coeffs_multiple(_dist, radii, coeffs, <int>fun)
      else:
        print("radius must be either a scalar, or an iterable! Unknown Type[%s] ! " % type(radius))
        raise TypeError
    else:
      print("distances must be either a scalar, or an iterable! Unknown Type[%s] ! " % type(dist))
      raise TypeError
      #
    return coeffs

  cdef double c_localization_coeff(self, double dist, double radius, int method) except -1:
    """
    Calculate the localization coefficient given distnace, and localization radius and mothod

    Args:
      method:
        1: Gauss
        2: Gaspari-cohn

    Returns:
      coeff: localization coefficient; -1 if method is not supported
    """
    cdef double coeff, thresh
    cdef double red, r1, r2, r3

    if method == 1:
      # Gauss localization
      coeff = exp(-0.5 * pow(dist/radius, 2) )
    elif method == 2:
      # Gaspari-Cohn 5th order step function localization
      thresh = radius * 1.7386
      if dist <= thresh:
          red = dist/thresh
          r2 = red ** 2
          r3 = red ** 3
          coeff = 1.0 + r2 * (-r3/4.0 + r2/2.0) + r3 * (5.0/8.0) - r2 * (5.0/3.0)
      elif dist <= thresh*2:
          red = dist/thresh
          r1 = red
          r2 = red ** 2
          r3 = red ** 3
          coeff = r2 * (r3/12.0 - r2/2.0) + r3 * (5.0/8.0) + r2 * (5.0/3.0) - r1 * 5.0 + 4.0 - (2.0/3.0) / r1
      else:
          coeff = 0.0
    else:
      coeff = -1
      #
    return coeff
    #

  cdef int c_localization_coeffs(self, np.ndarray dists, double radius, np.ndarray coeffs, int method) except -1:
    """
    Calculate the localization coefficient given distnace, and localization radius and mothod

    Args:
      method:
        1: Gauss
        2: Gaspari-cohn

    Returns:
      coeff: localization coefficient; -1 if method is not supported
    """
    cdef double coeff
    cdef int out = 0
    cdef int count, i

    if dists.size != coeffs.size:
      print("dists, radii, and coeffs must be of equal size!")
      out = -1
      return out
    else:
      count = dists.size
      for i in range(count):
        coeff = self.c_localization_coeff(dists[i], radius, method)
        # print("I am insice 'c_localization_coeffs('; Coeff = %f, dists[%d]=%f, radius=%f" % (coeff, i, dists[i], radius))
        if coeff !=-1:
          coeffs[i] = coeff
        else:
          out = -1
          break
      #
    return out
    #

  cdef int c_localization_coeffs_multiple(self, np.ndarray dists, np.ndarray radii, np.ndarray coeffs, int method) except -1:
    """
    Calculate the localization coefficient given distnace, and localization radius and mothod

    Args:
      method:
        1: Gauss
        2: Gaspari-cohn

    Returns:
      coeff: localization coefficient; -1 if method is not supported
    """
    cdef double coeff
    cdef int out = 0
    cdef int count, i

    if dists.size != radii.size or dists.size != coeffs.size:
      print("dists, radii, and coeffs must be of equal size!")
      out = -1
      return out
    else:
      count = dists.size
      for i in range(count):
        coeff = self.c_localization_coeff(dists[i], radii[i], method)
        if coeff !=-1:
          coeffs[i] = coeff
        else:
          out = -1
          break
      #
    return out
    #


  def get_forecast_state(self, bcast_state=True):
    """
    """
    # TODO: Check if this works when the ensemble is None everywhere!
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      comm_size = 1
      my_rank = 0
    else:
      comm_size = MPI_COMM.size
      my_rank = MPI_COMM.rank

    if MPI_COMM is not None:
      MPI_COMM.Barrier()

    # Calculate local mean
    forecast_ensemble = self.filter_configs['forecast_ensemble']
    if forecast_ensemble is None:
      local_forecast_state = None
      local_ens_size = 0
    else:
      assert isinstance(forecast_ensemble, Ensemble), "forecast_ensemble must be an instance of Ensemble class"
      if forecast_ensemble.size > 0:
        local_forecast_state = forecast_ensemble.mean()
        local_ens_size = forecast_ensemble.size
      else:
        local_forecast_state = None
        local_ens_size = 0
    #
    if self._verbose:
      print("In calculate forecast state: NODE %d has %d ensembles "  % (my_rank, local_ens_size))
    # Now, communicate calculated local means (sub-sample mean);
    ensemble_size = self.get_forecast_ensemble_size()  # this should be the total ensemble size
    if my_rank == 0:
      if self._verbose:
        print("ROOT: Total ensmble size = %d " % ensemble_size)
        sys.stdout.flush()
      weight = float(local_ens_size)/ensemble_size
      if local_forecast_state is not None:
        forecast_state = local_forecast_state.scale(weight, in_place=False)
      else:
        forecast_state = None

      # Sync local ensemble sizes
      aggr_ens_sizes = local_ens_size
      if self._verbose:
        print("ROOT: Starting with aggr_ens_sizes %d " % aggr_ens_sizes)
        sys.stdout.flush()
      rmt_ens_sizes = [local_ens_size]
      for node_rank in range(1, comm_size):
        rmt_ens_size = MPI_COMM.recv(source=node_rank, tag=node_rank+7000)
        rmt_ens_sizes.append(rmt_ens_size)
        aggr_ens_sizes += rmt_ens_size
        if self._verbose:
          print("Node %d has %d ensemble members..." % (node_rank, rmt_ens_size))
          sys.stdout.flush()

      # Start syncing state vectors
      for node_rank in range(1, comm_size):
        rmt_ens_size = rmt_ens_sizes[node_rank]
        if rmt_ens_size > 0:
          if self._verbose:
            print("Receiving the mean of [%d] ensembles from node [%d]..." % (rmt_ens_size, node_rank))
            sys.stdout.flush()
          vec = MPI_COMM.recv(source=node_rank, tag=node_rank+8000)
          weight = float(rmt_ens_size)/ensemble_size
          vec.scale(weight, in_place=True)
          if forecast_state is not None:
            forecast_state.add(vec, in_place=True)
          else:
            forecast_state = vec.copy()
          if self._verbose:
            print("___---DONE----___")
            sys.stdout.flush()
        else:
          if self._verbose:
            print("Receiving NOTHING from node [%d]..." % node_rank)
      if aggr_ens_sizes != ensemble_size:
        if aggr_ens_sizes == 0:
          if self._verbose:
            print("Aggregated Ensemble size is 0; this indicates no forecast ensemble/state")
            sys.stdout.flush()
        else:
          print("Expected to gather means of %d ensemble members from all nodes; However, received means of %d ensemble members!" % (ensemble_size, aggr_ens_sizes))
          raise ValueError
    else:
      forecast_state = None
      # Non-root nodes send to root, their local ensemble sizes:
      MPI_COMM.send(local_ens_size, dest=0, tag=my_rank+7000)
      #
      # Now, send the ensemble members:
      if local_ens_size > 0:
        MPI_COMM.send(local_forecast_state, dest=0, tag=my_rank+8000)

    # broadcast forecast mean: TODO: Refactor all MPI send/receive operations after initial debugging!
    if MPI_COMM is not None:
      MPI_COMM.Barrier()
      if bcast_state:
        forecast_state = MPI_COMM.bcast(forecast_state, root=0)

      if self._verbose:
        print("my rank %d; I am getting out of calculate forecast state" % my_rank)
        sys.stdout.flush()
    return forecast_state


  def get_forecast_ensemble_size(self):
    """
    Read collective forecast_ensemble size
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      comm_size = 1
      my_rank = 0
    else:
      comm_size = MPI_COMM.size
      my_rank = MPI_COMM.rank

    if self.filter_configs['forecast_ensemble'] is None:
      ensemble_size = 0
    else:
      ensemble_size = self.filter_configs['forecast_ensemble'].size
    if MPI_COMM is not None:
      ensemble_size = MPI_COMM.allreduce(ensemble_size, op=MPI.SUM)
    return ensemble_size

  def get_analysis_ensemble_size(self):
    """
    Read collective analysis_ensemble size
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      comm_size = 1
      my_rank = 0
    else:
      comm_size = MPI_COMM.size
      my_rank = MPI_COMM.rank

    if self.filter_configs['analysis_ensemble'] is None:
      ensemble_size = 0
    else:
      ensemble_size = self.filter_configs['analysis_ensemble'].size
    if MPI_COMM is not None:
      ensemble_size = MPI_COMM.allreduce(ensemble_size, op=MPI.SUM)
    return ensemble_size


  def get_analysis_state(self, bcast_state=True):  # TODO: DEBUG; entrapment in parallel runs
    """
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      comm_size = 1
      my_rank = 0
    else:
      comm_size = MPI_COMM.size
      my_rank = MPI_COMM.rank

    if MPI_COMM is not None:
      MPI_COMM.Barrier()

    # Calculate local mean
    analysis_ensemble = self.filter_configs['analysis_ensemble']
    if analysis_ensemble is None:
      local_analysis_state = None
      local_ens_size = 0
    else:
      assert isinstance(analysis_ensemble, Ensemble), "analysis_ensemble must be an instance of Ensemble class"
      if analysis_ensemble.size > 0:
        local_analysis_state = analysis_ensemble.mean()
        local_ens_size = analysis_ensemble.size
      else:
        local_analysis_state = None
        local_ens_size = 0

    if MPI_COMM is not None:
      MPI_COMM.Barrier()

    #
    if self._verbose:
      print("In calculate analysis state: NODE %d has %d ensembles "  % (my_rank, local_ens_size))
      sys.stdout.flush()
    # Now, communicate calculated local means (sub-sample mean);
    ensemble_size = self.get_analysis_ensemble_size()  # Collective analysis ensemble size
    if my_rank == 0:
      if self._verbose:
        print("ROOT: Total ensmble size = %d " % ensemble_size)
        sys.stdout.flush()
      weight = float(local_ens_size)/ensemble_size
      if local_analysis_state is not None:
        analysis_state = local_analysis_state.scale(weight, in_place=False)
      else:
        analysis_state = None
      # sync local ensemble sizes
      aggr_ens_sizes = local_ens_size
      if self._verbose:
        print("ROOT: Starting with aggr_ens_sizes %d " % aggr_ens_sizes)
        sys.stdout.flush()

      # Sync local ensemble sizes
      aggr_ens_sizes = local_ens_size
      if self._verbose:
        print("ROOT: Starting with aggr_ens_sizes %d " % aggr_ens_sizes)
        sys.stdout.flush()
      rmt_ens_sizes = [local_ens_size]
      for node_rank in range(1, comm_size):
        rmt_ens_size = MPI_COMM.recv(source=node_rank, tag=node_rank+7001)
        rmt_ens_sizes.append(rmt_ens_size)
        aggr_ens_sizes += rmt_ens_size
        if self._verbose:
          print("Node %d has %d ensemble members..." % (node_rank, rmt_ens_size))
          sys.stdout.flush()

      # Start syncing state vectors
      for node_rank in range(1, comm_size):
        rmt_ens_size = rmt_ens_sizes[node_rank]
        if rmt_ens_size > 0:
          if self._verbose:
            print("Receiving the mean of [%d] ensembles from node [%d]..." % (rmt_ens_size, node_rank))
            sys.stdout.flush()
          vec = MPI_COMM.recv(source=node_rank, tag=node_rank+8001)
          weight = float(rmt_ens_size)/ensemble_size
          vec.scale(weight, in_place=True)
          if analysis_state is not None:
            analysis_state.add(vec, in_place=True)
          else:
            analysis_state = vec.copy()
          if self._verbose:
            print("___---DONE----___")
            sys.stdout.flush()
        else:
          if self._verbose:
            print("Receiving NOTHING from node [%d]..." % node_rank)
      if aggr_ens_sizes != ensemble_size:
        if aggr_ens_sizes == 0:
          if self._verbose:
            print("Aggregated Ensemble size is 0; this indicates no analysis ensemble/state")
            sys.stdout.flush()
        else:
          print("Expected to gather means of %d ensemble members from all nodes; However, received means of %d ensemble members!" % (ensemble_size, aggr_ens_sizes))
          raise ValueError
    else:
      analysis_state = None
      # Non-root nodes send to root, their local ensemble sizes:
      MPI_COMM.send(local_ens_size, dest=0, tag=my_rank+7001)
      #
      # Now, send the ensemble members:
      if local_ens_size > 0:
        if self._verbose:
          print("Node %d; sending my local_analysis state..." % my_rank)
          sys.stdout.flush()
        MPI_COMM.send(local_analysis_state, dest=0, tag=my_rank+8001)
        if self._verbose:
          print("Node %d; DONE sending my local_analysis state..." % my_rank)
          sys.stdout.flush()

    # broadcast forecast mean: TODO: Refactor all MPI send/receive operations after initial debugging!
    if MPI_COMM is not None:
      MPI_COMM.Barrier()
      if bcast_state:
        analysis_state = MPI_COMM.bcast(analysis_state, root=0)

      if self._verbose:
        print("my rank %d; I am getting out of calculate analysis state" % my_rank)
        sys.stdout.flush()
    return analysis_state


  def _validate_filter_configs(self):
    """
    """
    pass
    # TODO: If validation added, properties must be updated accordingly
    # TODO: use the properties added recently to validate things such as ensemble, sizes , etc...
    try:
      MPI_COMM = self.filter_configs['MPI_COMM']
    except(KeyError, AttributeError):
      self.filter_configs.update({'MPI_COMM':None})
      MPI_COMM = self.filter_configs['MPI_COMM']
    finally:
      if MPI_COMM is None:
        comm_size = 1
        my_rank = 0
      else:
        comm_size = MPI_COMM.size
        my_rank = MPI_COMM.rank

    if self._verbose:
      print(">>> Rank [%d / %d]: Validating filter configs..." % (my_rank, comm_size))
      sys.stdout.flush()

    # Split Ensembles on all working nodes:
    # The assumptions are as follows:
    # 1- If the ensemble size on node 0 is larger than the ensemble it has,
    #    it will check with other nodes to make sure the total numer of
    #    ensemble members on all nodes is equal to the ensemble size it has,
    #    otherwise an assertion error is raised.
    # 2- If the ensemble size on node 0 is equal to the ensembble it has, it will start distributing to other workers even if they have ensembles (they will be distroyed)

    if MPI_COMM is not None and comm_size > 0:
      MPI_COMM.Barrier()
      if my_rank == 0 and self._verbose:
        if comm_size > 1:
          print("Started validation of EnKF object configurations on all %d nodes" % comm_size)
        else:
          print("Started validation of EnKF object configurations")
        sys.stdout.flush()

      # Check collective ensemble size
      forecast_ensemble = self.filter_configs['forecast_ensemble']
      analysis_ensemble = self.filter_configs['analysis_ensemble']
      if forecast_ensemble is None:
        my_forecast_ensemble_size = 0
      else:
        my_forecast_ensemble_size = len(forecast_ensemble)
      if analysis_ensemble is None:
        my_analysis_ensemble_size = 0
      else:
        my_analysis_ensemble_size = len(analysis_ensemble)

      # Forecat vs. Analysis first
      forecast_first = self.filter_configs['forecast_first']
      #
      local_ensemble_sizes = (my_forecast_ensemble_size, my_analysis_ensemble_size)
      if self._verbose and my_rank==0:
        print("Gathering local ensemble sizes...")
      collective_ensemble_sizes = MPI_COMM.gather(local_ensemble_sizes, root=0)
      if self._verbose and my_rank==0:
        print("DONE")
        sys.stdout.flush()

      #
      # We shouldn't do this here; the filter must be initialized with members those are already distributed. Maybe do a check for balance, and do that ONLY  needed
      # TODO: Also, refactor because parallel runs-get entrapped after updates made recently
      if False:
        if my_rank == 0:
            # got all ensemble sizes; Now, validate ensemble sizes
            dist_ens_sizes = [0] * comm_size

            # Calculate aggregated ensemble sizes
            aggr_forecast_ens_sizes = 0
            aggr_analysis_ens_sizes = 0
            for p in collective_ensemble_sizes:
              aggr_forecast_ens_sizes += p[0]
              aggr_analysis_ens_sizes += p[1]
            #
            ensemble_size = max(self.ensemble_sizes)
            if self._verbose:
              print("Root node; expecting ensemble size %d " % ensemble_size)
            if ensemble_size == 0:
              print("Your are trying to initialize the filter with no ensemble members. Will allow initialization, but won't be able to do assimilation without updating analysis/forecast ensmble(s)!")
              sys.stdout.flush()
            else:
              #
              if forecast_first:
                # analysis ensmble sizes are to be checked
                if ensemble_size == my_analysis_ensemble_size:
                  # rank 0 has the whole ensemble to be splitted
                  # prepare a list of ensemble members to send out:
                  for i in range(ensemble_size):
                    dist_ens_sizes[i%comm_size] += 1
                elif ensemble_size > my_analysis_ensemble_size:
                  # rank 0 has less ensmble members than aggregated size
                  if ensemble_size == aggr_analysis_ens_sizes:
                    # Great, ensembles are already splitted
                    dist_ens_sizes[0] = ensemble_size
                  elif my_analysis_ensemble_size == 0:
                    if self._verbose:
                      print("Root node has no ensemble members; this will NOT WORK  in the assimilation phase unless set manually, or by a looping process, e.g. filtering_process")

                else:
                  # rank 0 has more ensembles that ensmble size!
                  print("Rank 0 has more ensembles that ensmble size!")
                  raise ValueError
              else:
                # analysis first
                # analysis ensmble sizes are to be checked
                if ensemble_size == my_forecast_ensemble_size:
                  # rank 0 has the whole ensemble to be splitted
                  # prepare a list of ensemble members to send out:
                  for i in range(ensemble_size):
                    dist_ens_sizes[i%comm_size] += 1
                elif ensemble_size > my_forecast_ensemble_size:
                  # rank 0 has less ensmble members than aggregated size
                  if self._verbose:
                    print("Root Node has less ensmble members than aggregated size!")
                    print("Root node has %d; while aggregated ensemble size is %d" % (ensemble_size, aggr_forecast_ens_sizes))
                  if ensemble_size == aggr_forecast_ens_sizes:
                    # Great, ensembles are already splitted
                    dist_ens_sizes[0] = ensemble_size
                else:
                  # rank 0 has more ensembles that ensmble size!
                  print("Rank 0 has more ensembles than ensmble size!")
                  raise ValueError

        # Sync all ensemble sizes
        if my_rank == 0:
          # Distribute Ensemles
          # Total ensemble size:
          for node_rank in range(1, comm_size):
            if self._verbose:
              print("Root NODE: Sending ensemble size to Rank %d " % node_rank)
            MPI_COMM.send(ensemble_size, dest=node_rank, tag=node_rank+500)
            if self._verbose:
              print("done.")

          # Local ensemble sizes
          local_ensemble_size = dist_ens_sizes[0]
          for node_rank in range(1, comm_size):
            if self._verbose:
              print("Root NODE: Sending local ensemble size [%d] to Rank [%d] " % (dist_ens_sizes[node_rank], node_rank))
            MPI_COMM.send(dist_ens_sizes[node_rank], dest=node_rank, tag=node_rank+1000)
            if self._verbose:
              print(".done.")

          if forecast_first:
            local_ensemble = self.filter_configs['analysis_ensemble']
            self.filter_configs['forecast_ensemble'] = None
          else:
            local_ensemble = self.filter_configs['forecast_ensemble']
            self.filter_configs['analysis_ensemble'] = None

          # Now, distribute ensmbles
          for node_rank in range(1, comm_size):
            ens_sent = 0
            for ens_ind in range(dist_ens_sizes[node_rank]):
              if self._verbose:
                print("Root NODE: [%03d] ensembles successfully sent to out to NODE [%d] " %(ens_sent, node_rank))
              vec = local_ensemble.pop()
              MPI_COMM.send(vec, dest=node_rank, tag=node_rank+2000+ens_ind)
              if self._verbose:
                print("Now, ROOT has %d ensemble members" % local_ensemble.size)
              ens_sent += 1
        else:
          # Collect ensembles
          ensemble_size = MPI_COMM.recv(source=0, tag=my_rank+500)
          if self._verbose:
            print("\n\t\tNode %d; got full ensemble size of %d" % (my_rank, ensemble_size))
          local_ensemble_size = MPI_COMM.recv(source=0, tag=my_rank+1000)
          if self._verbose:
            print("\n\t\tNode %d; got my local ensemble size of %d" % (my_rank, local_ensemble_size))

          local_ensemble = None
          # Now, receive ensemble members
          for ens_ind in range(local_ensemble_size):
            if self._verbose:
              print("Node: %d; waiting for ensemble [%d/%d]" % (my_rank, ens_ind, local_ensemble_size))
            # receive a state:
            vec = MPI_COMM.recv(source=0, tag=my_rank+2000+ens_ind)
            if self._verbose:
              print("Node: %d; done.." % my_rank)

            if ens_ind == 0:
              # initialize an ensemble to the given state
              local_ensemble = Ensemble(0, vec.size)
            local_ensemble.append(vec)

          self.filter_configs['ensemble_size'] = ensemble_size

          if forecast_first:
            self.filter_configs['analysis_ensemble'] = local_ensemble
            self.filter_configs['forecast_ensemble'] = None
          else:
            self.filter_configs['analysis_ensemble'] = None
            self.filter_configs['forecast_ensemble'] = local_ensemble
        #
        if local_ensemble is not None:
          chk_ens_size = local_ensemble.size
        else:
          chk_ens_size = 0

        if chk_ens_size != local_ensemble_size:
          print("After distributing ensembles; ensembles sizes DON'T match!")
          print("NODE [%d] : Expected %d, however received %d" % (my_rank, local_ensemble_size, chk_ens_size))
          raise ValueError
        else:
          pass
          # print("\n++++++ DOUBLE CHECK\n\t Node [%d] HAS [%03d] ensemble members\n++++++  " % (my_rank, chk_ens_size))
      #
    else:  # comm_size == 0
      # Only one node is initiated;
      pass

    # Broadcast observation (for parallel EnKF implementation) to all nodes from node 0
    # maybe, we can ignore those with no local ensembles!
    if MPI_COMM is not None and comm_size > 0:
      # Check collective ensemble size
      observation = self.filter_configs['observation']
      observation = MPI_COMM.bcast(observation, root=0)
      if my_rank != 0:
        self.filter_configs['observation'] = observation
      #

    # Update localization matrices repository
    localization_matrices_repo = self.filter_configs['localization_matrices_repo']
    if localization_matrices_repo is None:
      localization_matrices_repo, _ = os.path.split(__file__)
      localization_matrices_repo = os.path.join(localization_matrices_repo, 'Decorr_Mats')
      self.filter_configs.update({'localization_matrices_repo':localization_matrices_repo})
      #
    if my_rank == 0:
      if not os.path.isdir(localization_matrices_repo):
        os.makedirs(localization_matrices_repo)

    # Sync all processes:
    if MPI_COMM is not None and comm_size > 0:
      MPI_COMM.Barrier()
      #
      if my_rank == 0 and self._verbose:
        print("DONE validation on %d nodes" % comm_size)
        sys.stdout.flush()
    #
    #  TODO:  add more validation
    # raise NotImplementedError

  def _validate_output_configs(self):
    """
    """
    pass
    # raise NotImplementedError

  def get_filter_configs(self, details='full'):
    """
    """
    if re.match(r"\Abasic\Z", details, re.IGNORECASE):
      configs = dict(filter_name=self.filter_configs['filter_name'],
                     forecast_first=self.filter_configs['forecast_first'],
                     hybrid_background_coeff=self.filter_configs['hybrid_background_coeff'],
                     forecast_inflation_factor=self.filter_configs['forecast_inflation_factor'],
                     analysis_inflation_factor=self.filter_configs['analysis_inflation_factor'],
                     obs_covariance_scaling_factor=self.filter_configs['obs_covariance_scaling_factor'],
                     localize_covariances=self.filter_configs['localize_covariances'],
                     localization_method=self.filter_configs['localization_method'],
                     localization_radius=self.filter_configs['localization_radius'],
                     localization_function=self.filter_configs['localization_function'],
                     ensemble_size=self.filter_configs['ensemble_size'],
                    )
    elif re.match(r"\Afull\Z", details, re.IGNORECASE):
      configs = self.filter_configs
    else:
      print("details argument supports only 'full', and 'basic'. The passed value [%s] is not supported" % details)
      raise ValueError

    return configs

  def get_output_configs(self):
    configs = self.output_configs
    return configs

  def print_filter_configs(self):
    for k in self.filter_configs:
      print("%s: %s" % (k, self.filter_configs[k]))

  def print_otput_configs(self):
    for k in self.output_configs:
      print("%s: %s" % (k, self.output_configs[k]))

  def print_configs(self):
    print("Filtering Configurationss:")
    self.print_filter_configs()
    print("\nOutput Configurationss:")
    self.print_filter_configs()

  #
  def print_cycle_results(self):
    """
    Print filtering results from the current cycle to the main terminal
    A check on the corresponding options in the configurations dictionary is made to make sure
    saving is requested.

    Args:

    Returns:
        None

    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      my_rank = 0
    else:
      my_rank = MPI_COMM.rank
      #
    if my_rank == 0:
      # forecast and analysis time instances:
      forecast_first = self.filter_configs['forecast_first']
      t0 , t1 = self.filter_configs['tspan'][0], self.filter_configs['tspan'][-1]
      if forecast_first:
        forecast_time = t1
        analysis_time = t1
      else:
        forecast_time = t0
        analysis_time = t0

      initial_rmse = self.output_configs['filter_statistics']['initial_rmse']
      forecast_rmse = self.output_configs['filter_statistics']['forecast_rmse']
      analysis_rmse = self.output_configs['filter_statistics']['analysis_rmse']
      observation_rmse = self.output_configs['filter_statistics']['observation_rmse']
      observation_forecast_rmse = self.output_configs['filter_statistics']['observation_forecast_rmse']
      final_rmse = self.output_configs['filter_statistics']['final_rmse']

      print("Filtering:%s: Initial[time:%5.3e > RMSE:%8.5e]  :: Final[time:%5.3e > RMSE:%8.5e] :: FORECAST[time:%5.3e > RMSE:%8.5e]  :: ANALYSIS[time:%5.3e > RMSE:%8.5e]  ::  OBS-FRCST-RMSE > %8.5e :: OBS-ANLS-RMSE > %8.5e"
            % (self.filter_configs['filter_name'], t0, initial_rmse, t1, final_rmse, forecast_time, forecast_rmse, analysis_time, analysis_rmse, observation_forecast_rmse, observation_rmse))
      #
    else:
      pass

  # TODO: Update save_cycle_results to save the initial results only if needed!

  def save_cycle_results(self, output_dir=None, cleanup_out_dir=False, initial_cycle=False):
    """
    Save filtering results from the current cycle to file(s).
    This code is initially borrowed from DATeS: https://arxiv.org/abs/1704.05594

    Check the output directory first. If the directory does not exist, create it.

    Args:
        output_dir: full path of the directory to save results in
        cleaup_out_dir: erase the output directory if exists, before storing results
        initial_cycle: if this is True; will only look for initial ensemble members  (forecast/analysis) and copy the provided one as neded!

    Returns:
        None

    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      my_rank = 0
      comm_size = 1
    else:
      my_rank = MPI_COMM.rank
      comm_size = MPI_COMM.size

    if MPI_COMM is not None and comm_size > 1:
      MPI_COMM.Barrier()

    if my_rank==0:
      print("Saving Filtering Cycle Results to File...")

    #
    output_configs = self.output_configs
    #
    if my_rank == 0 and self._verbose:
      print("Started saving files")
      sys.stdout.flush()

    if my_rank == 0:
      model = self.filter_configs['model']
      # Retrieve output configurations
      file_output = output_configs['file_output']
      if not file_output:
        print("The output flag is turned off. The method 'save_cycle_results' is called though!")
        raise ValueError()

      # Good to go! --> Start preparing directories (if necessary) then save results...
      # Output Directories
      if output_dir is not None:
        file_output_directory = output_dir
      else:
        file_output_directory = output_configs['file_output_dir']
      # check the output sub-directories...
      filter_statistics_dir = os.path.join(file_output_directory, output_configs['filter_statistics_dir'])
      model_states_dir = os.path.join(file_output_directory, output_configs['model_states_dir'])
      observations_dir = os.path.join(file_output_directory, output_configs['observations_dir'])

      # clean-up output directory; this is set to true only if the filter is called once, otherwise filtering_process should handle it.
      if cleanup_out_dir:
        parent_path, out_dir = os.path.split(file_output_directory)
        utility.cleanup_directory(directory_name=out_dir, parent_path=parent_path)

      # Create directories (statistics, states, observations) if not available:
      if not os.path.isdir(filter_statistics_dir):
        os.makedirs(filter_statistics_dir)
      if not os.path.isdir(model_states_dir):
        os.makedirs(model_states_dir)
      if not os.path.isdir(observations_dir):
        os.makedirs(observations_dir)

      # check if results are to be saved to separate files or appended on existing files.
      # This may be overridden if not adequate for some output (such as model states), we will see!
      # This is useful for saving filter statistics but not model states or observations as models should handle both
      file_output_file_format = output_configs['file_output_file_format'].lower()

      # SAVING MODEL STATES (Either Moments Only or Full Ensembles)
      # write cycle configurations:
      try:
        if not os.path.isdir(file_output_directory):
          os.makedirs(file_output_directory)
        if not os.path.isfile(os.path.join(file_output_directory, 'setup.dat')):
          model_conf = model.get_model_configs()
          obs_conf = model.get_observation_configs()
          utility.write_dicts_to_config_file('setup.dat', file_output_directory,
                                           [model_conf, obs_conf], ['Model Configs', 'Observation Configs'])
      except:
        # pass
        raise  # remove after debugging
      # get a proper name for the folder (cycle_*) under the model_states_dir path
      suffix = 0
      cycle_prefix = 'cycle_'
      while True:
        cycle_dir = cycle_prefix + str(suffix)
        cycle_states_out_dir = os.path.join( model_states_dir, cycle_dir)  # full path where states will be saved for the current cycle
        cycle_observations_out_dir = os.path.join( observations_dir, cycle_dir)
        if not os.path.isdir(cycle_states_out_dir):
          if os.path.isdir(cycle_observations_out_dir):
            print("There is inconsistency problem. Naming mismatch in cycles folders for states and observations!")
            raise IOError
          os.makedirs(cycle_states_out_dir)
          os.makedirs(cycle_observations_out_dir)
          break
        else:
          suffix += 1

      # Now we have all directories cleaned-up and ready for outputting.
      output_dir_structure_file = os.path.join(file_output_directory, 'output_dir_structure.txt')
      if not os.path.isfile(output_dir_structure_file):
        # print('writing output directory structure to config file \n \t%s \n' % output_dir_structure_file)
        out_dir_tree_structure = dict(file_output_directory=file_output_directory,
                                      model_states_dir=model_states_dir,
                                      observations_dir=observations_dir,
                                      filter_statistics_dir=filter_statistics_dir,
                                      cycle_prefix=cycle_prefix
                                      )
        utility.write_dicts_to_config_file(file_name='output_dir_structure.txt',
                                           out_dir=file_output_directory,
                                           dicts=out_dir_tree_structure,
                                           sections_headers='out_dir_tree_structure'
                                           )
      # Ensemble Size:

    else:
      observations_dir = model_states_dir = filter_statistics_dir = file_output_directory = cycle_observations_out_dir = cycle_states_out_dir = None


    if self._verbose:
      print("NODE {%d} START BROADCASTING ..." % my_rank)
      sys.stdout.flush()
    if MPI_COMM is not None and comm_size > 1:
      file_output_directory = MPI_COMM.bcast(file_output_directory, root=0)
      cycle_states_out_dir = MPI_COMM.bcast(cycle_states_out_dir, root=0)
      cycle_observations_out_dir = MPI_COMM.bcast(cycle_observations_out_dir, root=0)
      observations_dir = MPI_COMM.bcast(observations_dir, root=0)
      model_states_dir = MPI_COMM.bcast(model_states_dir, root=0)
      filter_statistics_dir = MPI_COMM.bcast(filter_statistics_dir, root=0)

    if self._verbose and my_rank == 0:
      print("DONE BROADCASTING ...")
      sys.stdout.flush()

    # What follows is on all nodes... UPDATE
    file_output_moment_only = output_configs['file_output_moment_only']
    #  save states
    if file_output_moment_only:
      file_output_moment_name = output_configs['file_output_moment_name'].lower()
      if file_output_moment_name in ['mean', 'average']:
        # Root node gets the forecast and analysis states if output moments only
        # TODO: Need to check this for intial cycle where either is missing! 
        forecast_state = self.get_forecast_state(bcast_state=False)
        analysis_state = self.get_analysis_state(bcast_state=False)
        # start outputting ensemble means... (both forecast and analysis of course).
        # save forecast mean
        if my_rank==0:
          if self._verbose:
            print("ROOT: Writing forecast state to file")
          forecast_state.write_to_file(os.path.join(cycle_states_out_dir, 'forecast_mean'))
        # save analysis mean
        if my_rank==0:
          if self._verbose:
            print("ROOT: Writing analysis state to file")
          analysis_state.write_to_file(os.path.join(cycle_states_out_dir, 'analysis_mean'))
      else:
        print("Unsupported ensemble moment: '%s' !" % (file_output_moment_name))
        raise ValueError
    else:
      if self._verbose:
        print("This is node: %d; started saving my own files..." % my_rank)
      # start outputting the whole ensemble members (both forecast and analysis ensembles of course).
      try:
        fcst_ens_size = self.filter_configs['forecast_ensemble'].size
      except(AttributeError):
        fcst_ens_size = 0
      try:
        anl_ens_size = self.filter_configs['analysis_ensemble'].size
      except(AttributeError):
        anl_ens_size = 0

      local_ens_sizes = (fcst_ens_size, anl_ens_size)
      #
      if MPI_COMM is not None:
        # Communicate starting index to write local ensemble members
        # gather local ensemble sizes, and determine how to name local files:
        gather_local_ens_sizes = MPI_COMM.gather(local_ens_sizes, root=0)
      else:
        gather_local_ens_sizes = [(fcst_ens_size, anl_ens_size)]

      if my_rank==0:
        assigned_ensembles = [0, 0]
        initial_index = []
        # Root specifiy the start index of each node:
        for ens_size in gather_local_ens_sizes:
          #
          inds = []

          if ens_size[0] == 0:
            inds.append(None)
          else:
            inds.append(assigned_ensembles[0])
            assigned_ensembles[0] += ens_size[0]

          if ens_size[1] == 0:
            inds.append(None)
          else:
            inds.append(assigned_ensembles[1])
            assigned_ensembles[1] += ens_size[1]

          #
          initial_index.append(tuple(inds))
          #
        # for DEBUGGING... TODO: Remove when checked...
        # print("Forecast ensemble size: %d " % assigned_ensembles[0])
        if self._verbose:
          for i, ind in enumerate(initial_index):
            print("Node %d; has ensembles of size %s, and takes initial indexes %s " % (i, gather_local_ens_sizes[i], ind))
          #
      else:
        initial_index = None

      #
      if MPI_COMM is not None:
        # Scatter initial index to start writing
        initial_index = MPI_COMM.scatter(initial_index, root=0)
      else:
        initial_index = (0, 0)  # for forecast and analysis ensembles respectively

      if self._verbose:
        print("NODE %d -> local ensemble size: %s" % (my_rank, repr(local_ens_sizes)))
        print("NODE %d -> initial indexes    : %s" % (my_rank, repr(initial_index)))

      # Each core outputs it's own ensemble membersi
      local_ens_size = local_ens_sizes[0]
      for ens_ind in range(local_ens_size):
        if False and self._verbose:
          print("Node %d ; I am writing forecast ensemble member %d ." % (my_rank, ens_ind+initial_index[0]))
          print('saving FORECAST ensemble member to separate files: %d' % ens_ind)
        forecast_ensemble_member = self.filter_configs['forecast_ensemble'][ens_ind]
        forecast_ensemble_member.write_to_file(os.path.join(cycle_states_out_dir, 'forecast_ensemble_'+str(ens_ind+initial_index[0])))

      local_ens_size = local_ens_sizes[1]
      for ens_ind in range(local_ens_size):
        if self.output_configs['debug_mode']:
          print('saving ANALYSIS ensemble member to separate files: %d' % ens_ind)
          print("Node %d ; I am writing analysis ensemble member %d ." % (my_rank, ens_ind+initial_index[1]))
        analysis_ensemble_member = self.filter_configs['analysis_ensemble'][ens_ind]
        analysis_ensemble_member.write_to_file(os.path.join(cycle_states_out_dir, 'analysis_ensemble_'+str(ens_ind+initial_index[1])))

    sys.stdout.flush()

    ensemble_sizes = self.ensemble_sizes
    # save reference state, free-run state, and observation
    if my_rank == 0:
      reference_state = self.filter_configs['reference_state']
      if reference_state is not None:
        reference_state.write_to_file(os.path.join(cycle_states_out_dir, 'reference_state'))
      #
      free_run_state = self.filter_configs['free_run_state']
      if free_run_state is not None:
        free_run_state.write_to_file(os.path.join(cycle_states_out_dir, 'free_run_state'))
      #
      # Save observation to file; use model to write observations to file(s)
      observation = self.filter_configs['observation']
      if observation is not None:
        observation.write_to_file(os.path.join(cycle_observations_out_dir, 'observation'))

    # Save filter statistics to file
    # check statistics availability first
    if my_rank == 0:
      try:
        self.output_configs['filter_statistics']
      except KeyError:
        self.output_configs.update({'filter_statistics':{}})
      try:
        self.output_configs['filter_statistics']['initial_rmse']
      except KeyError:
        self.output_configs['filter_statistics'].update({'initial_rmse':np.NaN})
      try:
        self.output_configs['filter_statistics']['final_rmse']
      except KeyError:
        self.output_configs['filter_statistics'].update({'final_rmse':np.NaN})
      try:
        self.output_configs['filter_statistics']['forecast_rmse']
      except KeyError:
        self.output_configs['filter_statistics'].update({'forecast_rmse':np.NaN})
      try:
        self.output_configs['filter_statistics']['analysis_rmse']
      except KeyError:
        self.output_configs['filter_statistics'].update({'analysis_rmse':np.NaN})
      try:
        self.output_configs['filter_statistics']['observation_forecast_rmse']
      except KeyError:
        self.output_configs['filter_statistics'].update({'observation_forecast_rmse':np.NaN})
      try:
        self.output_configs['filter_statistics']['observation_rmse']
      except KeyError:
        self.output_configs['filter_statistics'].update({'observation_rmse':np.NaN})

      # 1- Output filter RMSEs: RMSEs are saved to the same file. It's meaningless to create a new file for each cycle
      rmse_file_name = 'rmse'
      if file_output_file_format in ['txt', 'ascii']:
        rmse_file_name += '.dat'
        rmse_file_path = os.path.join(filter_statistics_dir, rmse_file_name)
        if not os.path.isfile(rmse_file_path):
          # rmse file does not exist. create file and add header.
          header = "RMSE Results: Filter: '%s' \n %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \n" % (self.filter_configs['filter_name'],
                                                                                    'Observation-Time'.rjust(18),
                                                                                    'Initial-Time'.rjust(18),
                                                                                    'Final-Time'.rjust(18),
                                                                                    'Forecast-Time'.rjust(18),
                                                                                    'Analysis-Time'.rjust(18),
                                                                                    'Initial-RMSE'.rjust(18),
                                                                                    'Final-RMSE'.rjust(18),
                                                                                    'Forecast-RMSE'.rjust(18),
                                                                                    'Analysis-RMSE'.rjust(18),
                                                                                    'OBS-FRCST-RMSE'.rjust(18),
                                                                                    'OBS-ANLS-RMSE'.rjust(18)
                                                                                    )
          # get the initial RMSE and add it if forecast is done first...
          if False and self.filter_configs['forecast_first']:  # No need for this!
            initial_time = self.filter_configs['tspan'][0]
            initial_rmse = self.output_configs['filter_statistics']['initial_rmse']
            header += " %18s \t %18.14e \t %18.14e \t %18.14e \t %18.14e \t %18.14e \t %18.14e \t %18.14e \n" % ('nan',
                                                                                      initial_time,
                                                                                      initial_time,
                                                                                      initial_time,
                                                                                      initial_rmse,
                                                                                      initial_rmse,
                                                                                      initial_rmse,
                                                                                      initial_rmse
                                                                                      )
          # dump the header to the file
          with open(rmse_file_path, mode='w') as file_handler:
            file_handler.write(header)
        else:
          # rmse file is already created; start actual results:
            pass

        # forecast and analysis time instances:
        t0 , t1 = self.filter_configs['tspan'][0], self.filter_configs['tspan'][-1]
        forecast_first = self.filter_configs['forecast_first']
        if forecast_first:
          forecast_time = t1
          analysis_time = t1
        else:
          forecast_time = t0
          analysis_time = t0

        # rmse file is now available; append rmse results to the file.
        observation_time = analysis_time
        #
        initial_rmse = self.output_configs['filter_statistics']['initial_rmse']
        forecast_rmse = self.output_configs['filter_statistics']['forecast_rmse']
        analysis_rmse = self.output_configs['filter_statistics']['analysis_rmse']
        observation_rmse = self.output_configs['filter_statistics']['observation_rmse']
        observation_forecast_rmse = self.output_configs['filter_statistics']['observation_forecast_rmse']
        final_rmse = self.output_configs['filter_statistics']['final_rmse']

        output_line = u" {0:18.12e} \t {1:18.12e} \t {2:18.12e} \t {3:18.12e} \t {4:18.12e} \t {5:18.14e} \t {6:18.14e} \t {7:18.14e} \t {8:18.14e} \t {9:18.14e} \t {10:18.14e} \n".format(observation_time,
                                                                                                       t0,
                                                                                                       t1,
                                                                                                       forecast_time,
                                                                                                       analysis_time,
                                                                                                       initial_rmse,
                                                                                                       final_rmse,
                                                                                                       forecast_rmse,
                                                                                                       analysis_rmse,
                                                                                                       observation_forecast_rmse,
                                                                                                       observation_rmse
                                                                                                       )
        #
        with open(rmse_file_path, mode='a') as file_handler:
          file_handler.write(output_line)

        # save filter and model configurations (a copy under observation directory and another under state directory)...
        filter_configs = self.filter_configs
        filter_conf= dict(filter_name=filter_configs['filter_name'],
                          ensemble_size=ensemble_sizes,
                          timespan=filter_configs['tspan'],
                          analysis_time=analysis_time,
                          observation_time=observation_time,
                          forecast_time=forecast_time,
                          forecast_first=filter_configs['forecast_first']
                          )
        utility.write_dicts_to_config_file('setup.dat', cycle_observations_out_dir,
                                           [filter_conf, output_configs], ['Filter Configs', 'Output Configs'])
        utility.write_dicts_to_config_file('setup.dat', cycle_states_out_dir,
                                           [filter_conf, output_configs], ['Filter Configs', 'Output Configs'])
      else:
        print("Unsupported output format: '%s' !" % file_output_file_format)
        raise ValueError()
        #
      # TODO: Pickle the whole dictionary, or even the 'post_processing_results' key
      # ROOT NODE level done...
      pass

    if MPI_COMM is not None and comm_size > 1:
      MPI_COMM.Barrier()

    #
    # Function level:
    if self._verbose:
      if my_rank==0:  #  and self._verbose:
        print("Filtering Cycle Results Saved successfully...")
      else:
        print("NODE %d is getting out of save cycle results" % my_rank)
    sys.stdout.flush()
    return file_output_directory, filter_statistics_dir, cycle_states_out_dir, cycle_observations_out_dir

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
  #                 Define useful properties to be retrieved/set externally                  #
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

  @property
  def MPI_COMM(self):
    """
    return a reference to the MPI COMM WORLD, if available, otherwise None is returned
    """
    return self.filter_configs['MPI_COMM']
  @MPI_COMM.setter
  def MPI_COMM(self, MPI_COMM):
    self.filter_configs['MPI_COMM'] = MPI_COMM

  @property
  def model(self):
    """
    Return a reference to the forward operator in self.filter_configs['model']
    """
    model = self.filter_configs['model']
    return model

  @model.setter
  def model(self, value):
    """
    Return a reference to the forward operator in self.filter_configs['model']
    """
    print("WARNING: The behaviour of the model update might be unexpected; TOBE tested...")
    self.filter_configs.update({'model':value})


  @property
  def forecast_first(self):
    """
    Return self.filter_configs['forecast_first']
    """
    return self.filter_configs['forecast_first']

  @forecast_first.setter
  def forecast_first(self, value):
    """
    Update self.filter_configs['forecast_first']
    """
    self.filter_configs.update({'forecast_first':value})


  @property
  def tspan(self):
    """
    Return self.filter_configs['tspan']
    """
    return self.filter_configs['tspan']

  @tspan.setter
  def tspan(self, value):
    """
    Update self.filter_configs['tspan']
    """
    assert utility.isiterable(value), "timespan must be an iterable, Received %s of Type: %s" % (value, type(value))
    self.filter_configs.update({'tspan':[v for v in value]})
  # Alias:
  @property
  def timespan(self):
    return self.tspan
  @timespan.setter
  def timespan(self, value):
    self.tspan(value)


  @property
  def observation(self):
    """
    Return self.filter_configs['observation']
    """
    return self.filter_configs['observation']

  @observation.setter
  def observation(self, value):
    """
    Update self.filter_configs['observation']
    """
    if value is None:
      print("Setting filter observation to None; an observation vector with all NaN's will be used instead!")
      value = self.model.observation_vector()
      value[:] = np.NaN
      self.filter_configs.update({'observation':value})
    else:
      try:
        self.filter_configs.update({'observation':value.copy()})
      except(AttributeError):
        print("Passed observation  doesn't support copying!!!\n invalid observation structure")
        raise
    # print("Set my observation; ", value)

  @property
  def free_run_state(self):
    """
    Return self.filter_configs['free_run_state']
    """
    return self.filter_configs['free_run_state']

  @free_run_state.setter
  def free_run_state(self, value):
    """
    Update self.filter_configs['free_run_state']
    """
    self.filter_configs.update({'free_run_state':value})

  @property
  def reference_state(self):
    """
    Return self.filter_configs['reference_state']
    """
    return self.filter_configs['reference_state']

  @reference_state.setter
  def reference_state(self, value):
    """
    Update self.filter_configs['reference_state']
    """
    self.filter_configs.update({'reference_state':value})


  @property
  def observation_error_model(self):
    """
    Return self.filter_configs['observation_error_model']
    """
    obs_err_model = self.filter_configs['observation_error_model']
    if obs_err_model is None:
      try:
        obs_err_model = self.filter_configs['model']._obs_err_model
      except:
        print("Failed to retrieve an observation error model!")
        print("'None found in self.filter_configs, and attemted to retrieve from the model object, but failed!'")
        raise
      else:
        if obs_err_model is None:
          print("No observation error model found either in the filter configuraitons, or the model configurations!\n Returning None; ")
    return obs_err_model

  @observation_error_model.setter
  def observation_error_model(self, value):
    """
    Update self.filter_configs['observation_error_model']
    """
    self.filter_configs.update({'observation_error_model':value})


  @property
  def background_error_model(self):
    """
    Return self.filter_configs['background_error_model']
    """
    return self.filter_configs['background_error_model']

  @background_error_model.setter
  def background_error_model(self, value):
    """
    Update self.filter_configs['background_error_model']
    """
    self.filter_configs.update({'background_error_model':value})


  @property
  def hybrid_background_coeff(self):
    """
    Return self.filter_configs['hybrid_background_coeff']
    """
    return self.filter_configs['hybrid_background_coeff']

  @hybrid_background_coeff.setter
  def hybrid_background_coeff(self, value):
    """
    Update self.filter_configs['hybrid_background_coeff']
    """
    assert np.isscalar(value), "hybrid_background_coeff must be a scalar"
    assert 0 <= value <= 1, "hybrid_background_coeff must be between [0 , 1]"
    self.filter_configs.update({'hybrid_background_coeff':value})


  @property
  def forecast_inflation_factor(self):
    """
    Return self.filter_configs['forecast_inflation_factor']
    """
    return self.filter_configs['forecast_inflation_factor']

  @forecast_inflation_factor.setter
  def forecast_inflation_factor(self, value):
    """
    Update self.filter_configs['forecast_inflation_factor']
    """
    assert np.isscalar(value), "forecast_inflation_factor must be a scalar"
    assert 1 <= value , "forecast_inflation_factor must be >= 1.0 "
    self.filter_configs.update({'forecast_inflation_factor':value})


  @property
  def analysis_inflation_factor(self):
    """
    Return self.filter_configs['analysis_inflation_factor']
    """
    return self.filter_configs['analysis_inflation_factor']

  @analysis_inflation_factor.setter
  def analysis_inflation_factor(self, value):
    """
    Update self.filter_configs['analysis_inflation_factor']
    """
    assert np.isscalar(value), "analysis_inflation_factor must be a scalar"
    assert 1 <= value , "analysis_inflation_factor must be >= 1.0 "
    self.filter_configs.update({'analysis_inflation_factor':value})


  @property
  def obs_covariance_scaling_factor(self):
    """
    Return self.filter_configs['obs_covariance_scaling_factor']
    """
    return self.filter_configs['obs_covariance_scaling_factor']

  @obs_covariance_scaling_factor.setter
  def obs_covariance_scaling_factor(self, value):
    """
    Update self.filter_configs['obs_covariance_scaling_factor']
    """
    assert np.isscalar(value), "obs_covariance_scaling_factor must be a scalar"
    self.filter_configs.update({'obs_covariance_scaling_factor':value})


  @property
  def localize_covariances(self):
    """
    Return self.filter_configs['localize_covariances']; True/False
    """
    return self.filter_configs['localize_covariances']

  @localize_covariances.setter
  def localize_covariances(self, value):
    """
    Update self.filter_configs['localize_covariances']
    """
    self.filter_configs.update({'localize_covariances':bool(value)})


  @property
  def localization_method(self):
    """
    Return self.filter_configs['localization_method']
    """
    return self.filter_configs['localization_method']

  @localization_method.setter
  def localization_method(self, value):
    """
    Update self.filter_configs['localization_method']
    """
    assert isinstance(value, str), "localization_method must be a scalar"
    self.filter_configs.update({'localization_method':value})

  @property
  def localization_radius(self):
    """
    Return self.filter_configs['localization_radius']
    """
    return self.filter_configs['localization_radius']

  @localization_radius.setter
  def localization_radius(self, value):
    """
    Update self.filter_configs['localization_radius']
    """
    assert np.isscalar(value), "localization_radius must be a scalar"
    assert 0 < value , "localization_radius must be > 0.0 "
    self.filter_configs.update({'localization_radius':value})

  @property
  def localization_function(self):
    """
    Return self.filter_configs['localization_function']
    """
    return self.filter_configs['localization_function']

  @localization_function.setter
  def localization_function(self, value):
    """
    Update self.filter_configs['localization_function']
    """
    assert isinstance(value, str), "localization_function must be a scalar"
    self.filter_configs.update({'localization_function':value})

  @property
  def ensemble_sizes(self):
    """
    Return forecast and analysis ensemble sizes
    """
    nens_f = self.get_forecast_ensemble_size()
    nens_a = self.get_analysis_ensemble_size()
    #
    return (nens_f, nens_a)

  @property
  def analysis_ensemble(self):
    """
    Return self.filter_configs['analysis_ensemble']
    """
    return self.filter_configs['analysis_ensemble']

  @analysis_ensemble.setter
  def analysis_ensemble(self, value):
    """
    Update self.filter_configs['analysis_ensemble']
    """
    if value is None:
      self.filter_configs.update({'analysis_ensemble':value})
      return
    assert isinstance(value, Ensemble), "analysis ensemble must be an instance of DLidarVec.Ensemble, received %s" % type(value)
    a_ens_size = value.size
    # assert a_ens_size>0, "You cannot pass an ensemble of less than one member!"
    if a_ens_size == 0:
      print("\n" + "*"*60)
      print("WARNING: The ensemble size is 0; An empty ensemble indicates WASTED nodes!")
      print("*"*60 + "\n")
    #
    if self.filter_configs['analysis_ensemble'] is not None:
      f_ens_size = self.filter_configs['analysis_ensemble'].size
    else:
      f_ens_size = a_ens_size

    if self.filter_configs['analysis_ensemble'] is not None:
      curr_ens_size = self.filter_configs['analysis_ensemble'].size
    else:
      curr_ens_size = None
    if curr_ens_size is None:
      curr_ens_size = a_ens_size

    new_ens_size = a_ens_size
    if a_ens_size == f_ens_size == curr_ens_size:
      pass

    elif new_ens_size == f_ens_size:
      print("WARNINIG: The ensemble size in filter_configs['ensemble_size'] was previously set incorrectly to %d " % curr_ens_size)
      print("Correcting to %d" % new_ens_size)

    elif new_ens_size == curr_ens_size:
      print("The current ensemble size and forecast ensemble size do not match! ")
      print("Will distroy the forecast ensemble")
      self.filter_configs.update({'analysis_ensemble':None})

    elif f_ens_size == curr_ens_size:
      print("The new ensemble has different ensemble size; Updating and destroying the current forecast ensemble")
      self.filter_configs.update({'analysis_ensemble':None})

    else:
      print("This situation is unpleasent!")
      print("a_ens_size != f_ens_size != curr_ens_size")
      print("WARNINIG: The ensemble size in filter_configs['ensemble_size'] was previously set incorrectly to %d " % curr_ens_size)
      print("Correcting to %d" % new_ens_size)
      #
      print("The current ensemble size and forecast ensemble size do not match! ")
      print("Will distroy the forecast ensemble size")
      self.filter_configs.update({'analysis_ensemble':None})
      #
    # Now, update the analysis ensemble and ensemble size
    self.filter_configs.update({'analysis_ensemble':value})

  @property
  def analysis_state(self):
    return self.get_analysis_state()


  @property
  def forecast_ensemble(self):
    """
    Return self.filter_configs['forecast_ensemble']
    """
    return self.filter_configs['forecast_ensemble']

  @forecast_ensemble.setter
  def forecast_ensemble(self, value):
    """
    Update self.filter_configs['forecast_ensemble']
    """
    if value is None:
      self.filter_configs.update({'forecast_ensemble':value})
      return
    assert isinstance(value, Ensemble), "forecast ensemble must be an instance of DLidarVec.Ensemble, received %s" % type(value)
    f_ens_size = value.size
    # assert f_ens_size>0, "You cannot pass an ensemble of less than one member!"
    if f_ens_size == 0:
      print("\n" + "*"*60)
      print("WARNING: The ensemble size is 0; An empty ensemble indicates WASTED nodes!")
      print("*"*60 + "\n")
    #
    if self.filter_configs['forecast_ensemble'] is not None:
      a_ens_size = self.filter_configs['forecast_ensemble'].size
    else:
      a_ens_size = f_ens_size

    if self.filter_configs['forecast_ensemble'] is not None:
      curr_ens_size = self.filter_configs['forecast_ensemble'].size
    else:
      curr_ens_size = None
    if curr_ens_size is None:
      curr_ens_size = f_ens_size

    new_ens_size = f_ens_size
    if f_ens_size == a_ens_size == curr_ens_size:
      pass

    elif new_ens_size == a_ens_size:
      print("WARNINIG: The ensemble size in filter_configs['ensemble_size'] was previously set incorrectly to %d " % curr_ens_size)
      print("Correcting to %d" % new_ens_size)

    elif new_ens_size == curr_ens_size:
      print("The current ensemble size and forecast ensemble size do not match! ")
      print("Will distroy the forecast ensemble")
      self.filter_configs.update({'forecast_ensemble':None})

    elif a_ens_size == curr_ens_size:
      print("The new ensemble has different ensemble size; Updating and destroying the current forecast ensemble")
      self.filter_configs.update({'forecast_ensemble':None})

    else:
      print("This situation is unpleasent!")
      print("f_ens_size != a_ens_size != curr_ens_size")
      print("WARNINIG: The ensemble size in filter_configs['ensemble_size'] was previously set incorrectly to %d " % curr_ens_size)
      print("Correcting to %d" % new_ens_size)
      #
      print("The current ensemble size and forecast ensemble size do not match! ")
      print("Will distroy the forecast ensemble size")
      self.filter_configs.update({'forecast_ensemble':None})
      #
    # Now, update the forecast ensemble and ensemble size
    self.filter_configs.update({'forecast_ensemble':value})


  @property
  def forecast_ensemble_size(self):
    """
    Return self.filter_configs['forecast_ensemble']
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      comm_size = 1
      my_rank = 0
    else:
      comm_size = MPI_COMM.size
      my_rank = MPI_COMM.rank

    local_ensemble = self.filter_configs['forecast_ensemble']
    if local_ensemble is None:
      local_ensemble_size = 0
    else:
      local_ensemble_size = local_ensemble.size

    if comm_size == 1 :
      ensemble_size = local_ensemble_size
    elif MPI_COMM is not None and comm_size > 1:
      ensemble_size = MPI_COMM.reduce(local_ensemble, op=MPI.SUM, root=0)
    else:
      print("IMPOSSIBLE SITUATION!!!")
      raise ValueError
    return ensemble_size

  @property
  def analysis_ensemble_size(self):
    """
    Return self.filter_configs['analysis_ensemble']
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      comm_size = 1
      my_rank = 0
    else:
      comm_size = MPI_COMM.size
      my_rank = MPI_COMM.rank

    local_ensemble = self.filter_configs['analysis_ensemble']
    if local_ensemble is None:
      local_ensemble_size = 0
    else:
      local_ensemble_size = local_ensemble.size

    if comm_size == 1 :
      ensemble_size = local_ensemble_size
    elif MPI_COMM is not None and comm_size > 1:
      ensemble_size = MPI_COMM.reduce(local_ensemble, op=MPI.SUM, root=0)
    else:
      print("IMPOSSIBLE SITUATION!!!")
      raise ValueError
    return ensemble_size


  @property
  def forecast_state(self):
    return self.get_forecast_state()

  @property
  def filter_name(self):
    return self.filter_configs['filter_name']
  @filter_name.setter
  def filter_name(self, value):
    self.filter_configs.update({'filter_name': str(value)})

  @property
  def file_output(self):
    """
    Return self.output_configs['file_output']
    """
    return self.output_configs['file_output']

  @file_output.setter
  def file_output(self, value):
    """
    Update self.output_configs['file_output']
    """
    self.output_configs.update({'file_output':bool(value)})

  @property
  def scr_output(self):
    """
    Return self.output_configs['scr_output']
    """
    return self.output_configs['scr_output']

  @scr_output.setter
  def scr_output(self, value):
    """
    Update self.output_configs['scr_output']
    """
    self.output_configs.update({'scr_output':bool(value)})


  @property
  def filter_statistics_dir(self):
    """
    Return self.output_configs['filter_statistics_dir']
    """
    return self.output_configs['filter_statistics_dir']

  @filter_statistics_dir.setter
  def filter_statistics_dir(self, value):
    """
    Update self.output_configs['filter_statistics_dir']
    """
    assert isinstance(value, str), "filter_statistics_dir must be a string!"
    self.output_configs.update({'filter_statistics_dir':value})

  @property
  def model_states_dir(self):
    """
    Return self.output_configs['model_states_dir']
    """
    return self.output_configs['model_states_dir']

  @model_states_dir.setter
  def model_states_dir(self, value):
    """
    Update self.output_configs['model_states_dir']
    """
    assert isinstance(value, str), "model_states_dir must be a string!"
    self.output_configs.update({'model_states_dir':value})


  @property
  def observations_dir(self):
    """
    Return self.output_configs['observations_dir']
    """
    return self.output_configs['observations_dir']

  @observations_dir.setter
  def observations_dir(self, value):
    """
    Update self.output_configs['observations_dir']
    """
    assert isinstance(value, str), "observations_dir must be a string!"
    self.output_configs.update({'observations_dir':value})


  @property
  def file_output_moment_only(self):
    """
    Return self.output_configs['file_output_moment_only']
    """
    return self.output_configs['file_output_moment_only']

  @file_output_moment_only.setter
  def file_output_moment_only(self, value):
    """
    Update self.output_configs['file_output_moment_only']
    """
    self.output_configs.update({'file_output_moment_only':bool(value)})

  @property
  def verbose(self):
    """
    Return self.output_configs['verbose']
    """
    return self._verbose

  @verbose.setter
  def verbose(self, value):
    """
    Update self.output_configs['verbose']
    """
    self._verbose = bool(value)
    self.filter_configs.update({'verbose':self._verbose})

  @file_output_moment_only.setter
  def file_output_moment_only(self, value):
    """
    Update self.output_configs['file_output_moment_only']
    """
    self.output_configs.update({'verbose':bool(value)})
    self._verbose = self.output_configs['verbose']


  @property
  def file_output_moment_name(self):
    """
    Return self.output_configs['file_output_moment_name']
    """
    return self.output_configs['file_output_moment_name']

  @file_output_moment_name.setter
  def file_output_moment_name(self, value):
    """
    Update self.output_configs['file_output_moment_name']
    """
    assert isinstance(value, str), "file_output_moment_name must be a string!"
    self.output_configs.update({'file_output_moment_name':value})

  @property
  def file_output_dir(self):
    """
    Return self.output_configs['file_output_dir']
    """
    return self.output_configs['file_output_dir']

  @file_output_dir.setter
  def file_output_dir(self, value):
    """
    Update self.output_configs['file_output_dir']
    """
    assert isinstance(value, str), "file_output_dir must be a string!"
    self.output_configs.update({'file_output_dir':value})


  @property
  def file_output_file_format(self):
    """
    Return self.output_configs['file_output_file_format']
    """
    return self.output_configs['file_output_file_format']

  @file_output_file_format.setter
  def file_output_file_format(self, value):
    """
    Update self.output_configs['file_output_file_format']
    """
    assert isinstance(value, str), "file_output_file_format must be a string!"
    self.output_configs.update({'file_output_file_format':value})


  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #



cdef class DEnKF(EnKF):

  def __init__(self, filter_configs=None, output_configs=None, **kwargs):
    """
    """
    if not self._initialized:
      # Aggregate, validate, and set filter and output configurations
      self.filter_configs = utility.aggregate_configurations(filter_configs, DEnKF._def_filter_configs)
      self._validate_filter_configs()
      self.output_configs = utility.aggregate_configurations(output_configs, DEnKF._def_output_configs)
      self._validate_output_configs()

      #
      try:
        self._verbose = self.output_configs['verbose']
      except(AttributeError, NameError):
        self._verbose = False
      #
      self._initialized = 1
      #
    self.filter_name = "DEnKF"
    #


  def analysis(self, local_forecast_ensemble, observation, all_to_numpy=True, parallel_EnKF=False, use_linearized_H=False):
    """
    """
    MPI_COMM = self.filter_configs['MPI_COMM']
    if MPI_COMM is None:
      comm_size = 1
      my_rank = 0
    else:
      comm_size = MPI_COMM.size
      my_rank = MPI_COMM.rank

    if MPI_COMM is not None:
      MPI_COMM.Barrier()

    if comm_size == 1 and parallel_EnKF:
      # just-in-case
      parallel_EnKF = False

    print("Node %d entering Analysis stage with %d forecast ensembble members " % (my_rank, local_forecast_ensemble.size))
    #
    if local_forecast_ensemble is observation is None:
      if my_rank == 0:
        print("Both observation, and forecast ensemble are None! Can't perform analysis step")
        raise ValueError
      else:
        print("WARNING: This is Node <<%d>>; Neither observations nor forecast ensemble is passed." % my_rank)
        print("Nothing to be done on this node. WASTED resources!")
    elif local_forecast_ensemble is None:
      if my_rank == 0:
        print("The forecast ensemble is None! Can't perform analysis step")
        raise ValueError
      else:
        print("WARNING: This is Node <<%d>>; No forecast ensemble is passed." % my_rank)
        print("An observation is passed, but no forecast ensemble! Nothing to be done on this node. WASTED resources!")
    elif observation is None:
      if my_rank == 0:
        print("***WARNING*** The observation is None! The model-based forecast will be copied as the analysis...")
        # raise ValueError
      else:
        print("WARNING: This is Node <<%d>>; No forecast ensemble is passed." % my_rank)
        print("Nothing to be done on this node. WASTED resources!")
    else:
      pass

    # If no observations passed to this node, return a copy of the passed local_forecast_ensemble
    if observation is None:
      # no assimilation required
      if local_forecast_ensemble is None:
        local_analysis_ensemble = None
      else:
        local_analysis_ensemble = local_forecast_ensemble.copy()
      print("Node %d Has no observations; Analysis step will be skipped... " % (my_rank))
      # return local_analysis_ensemble  # If any collaboration is done next, a disaster will happen!

    if not parallel_EnKF:
      # Serial EnKF:

      if self._verbose and my_rank==0:
        sep = "-"* 70
        print("\n%s\n%s: Starting Analysis Step... \n" % (self.filter_name, sep))
        print("Forecast first: %s " % self.forecast_first)
        print("Cycle Timespan: %s \n" % repr(self.timespan))
        if observation is not None:
          print("Observation time: %s" % self.observation.time)
      elif my_rank==0:
        print("%s: Analysis Step..." % (self.filter_name))

      # local_ensemble_size on all nodes
      if local_forecast_ensemble is None:
        local_ensemble_size = 0
      else:
        local_ensemble_size = local_forecast_ensemble.size

      # print("Node %d entering Analysis stage with %d forecast ensembble members " % (my_rank, local_ensemble_size))

      # 1- Rank 0 collects all ensemble forecast ensemble membbers, and carry-out the analysis, and then distribute members back
      rcv_ensemble_sizes = local_ensemble_size
      rcv_ensemble_sizes = MPI_COMM.gather(rcv_ensemble_sizes, root=0)

      #
      # print("1: NODE {%d}" % my_rank)
      # sys.stdout.flush()
      #

      if my_rank == 0:
        #
        # print("Root", rcv_ensemble_sizes)
        if local_ensemble_size==0:
          print("The root node DOES NOT accept empty ensembles for assimilation!")
          raise ValueError
        # Start with local ensemble, and append other members
        forecast_ensemble = local_forecast_ensemble
        #
        # gather ensemble members
        for node_rank in range(1, comm_size):
          for ens_ind in range(rcv_ensemble_sizes[node_rank]):
            vec = MPI_COMM.recv(source=node_rank, tag=node_rank+4000+ens_ind)
            forecast_ensemble.append(vec)
      else:
        # Now, send the ensemble members:
        for ens_ind in range(local_ensemble_size):
          MPI_COMM.send(local_forecast_ensemble[ens_ind], dest=0, tag=my_rank+4000+ens_ind)

      #
      # sys.stdout.flush()
      #

      #
      # 2- Rank 0 does all analysis work:
      if my_rank == 0:
        # Collect forecast enemble
        # get reference to model object:
        model = self.filter_configs['model']

        if not all_to_numpy:
          # Use LinAlg submodule (after finishing it), or use PETSc
          print("Only Numpy-based implementation is currently supported. This is a TODO!")
          raise NotImplementedError
          #
        else:
          if self._verbose:
            print("ROOT NODE started analysis with %d ensemble members" % forecast_ensemble.size)
          # PREPARE for ASSIMILATION:
          # ---------------------------
          # Get and inflate the forecast ensemble
          # XXXXXXXXXXXXXXXXXXXXXX
          Xf = forecast_ensemble  # .copy()
          alpha = self.filter_configs['forecast_inflation_factor']
          try:
            alpha = model.create_inflation_vector(alpha)
          except:
            pass
          Xf.inflate(alpha, in_place=True)
          ensemble_size = Xf.size

          # Generate the background state:
          xb = Xf.mean()
          state_size = xb.size
          #
          if self.output_configs['debug_mode']:
            # Check xb for Nans or Infs
            print("\n\n***")
            nan_locs = xb.where_nan()
            if nan_locs.size > 0:
              print("Forecast State (xb) has Nan Values at [%d] entries, as follows: " % nan_locs.size)
              print(nan_locs)
            inf_locs = xb.where_inf()
            if inf_locs.size > 0:
              print("Forecast State (xb) has Inf Values at [%d] entries, as follows: " % inf_locs.size)
              print(inf_locs)
            print("\n\n***")
            pass
          #

          # copy observation vector and NaN information
          y = observation.copy()
          obs_size = y.size
          obs_time = observation.time
          y_nan_locs = observation.where_nan()
          y_not_nan_locs = observation.where_not_nan()

          # Now check the projection of a model state, to see if more NaNs are present; e.g. u, v for stare data
          # Do that with ensemble mean
          Hxb = model.evaluate_theoretical_observation(xb, obs_time)
          Hxb_nan_locs = Hxb.where_nan()
          Hxb_not_nan_locs = Hxb.where_not_nan()

          # Joint Nan and not Nan Locs
          observation_nan_locs = np.union1d(y_nan_locs, Hxb_nan_locs)
          observation_not_nan_locs = np.intersect1d(y_not_nan_locs, Hxb_not_nan_locs)

          if observation_not_nan_locs.size > 0:
            # Valid observation exists, and the analysis step should be carried out
            if self.output_configs['debug_mode']:
              # Check xb for Nans or Infs
              print("\n\n***")
              print("Assimilation with observation of size: %d " % obs_size)
              print("Number of valid intries: %d" % observation_not_nan_locs.size)
              print("\n\n***")
            #

            # Get noise information
            obs_err_model = self.observation_error_model
            sqrtR_inv_VecProd = obs_err_model.sqrtR_inv_VecProd  # A function to evaluate R^(-1/2) * * x, where x could be StateVector, or np.ndarray of comformable shape (e.g. Nobs x *)
            sqrtR_VecProd = obs_err_model.sqrtR_VecProd  # A function to evaluate R^(-1/2)

            print("Getting matrix of ensemble anomalies"),
            # Matrix of ensemble anomalies:
            A = Xf.get_numpy_array()
            xf = xb.get_numpy_array()
            #
            if self.output_configs['debug_mode']:
              # Check Ansemble of anomalies for Nans or Infs
              print("\n\n***")
              if np.isnan(A).any():
                print("Forecast ensemble has NaN values")
                raise ValueError
              if np.isnan(xf).any():
                print("Forecast state has NaN values")
                raise ValueError
              print("\n\n***")
              #
            for i in range(ensemble_size):
              A[:, i] -= xf
            print("done")

            #
            if self.output_configs['debug_mode']:
              # Check Ansemble of anomalies for Nans or Infs
              print("\n\n***")
              if np.isnan(A).any():
                print("Forecast ensemble has NaN values")
                raise ValueError
              print("\n\n***")
              #
            #
            if self.output_configs['debug_mode']:
              # Check Ansemble of anomalies for Nans or Infs
              print("\n\n***")
              nan_locs = np.where(np.isnan(A))[0]
              if nan_locs.size > 0:
                print("Matrix of ensemble Anomalies (A) has Nan Values at [%d] entries, as follows: " % nan_locs.size)
                print(nan_locs)
              inf_locs = np.where(np.isinf(A))[0]
              if inf_locs.size > 0:
                print("Matrix of ensemble Anomalies (A) has Inf Values at [%d] entries, as follows: " % inf_locs.size)
                print(inf_locs)
              print("\n\n***")
            #


            # Model-observation (and associated mean) of the forecast ensemble members:
            if use_linearized_H:
              HA = np.empty((obs_size, ensemble_size))
              state = model.state_vector()
              for ens_ind in range(ensemble_size):
                state[:] = A[:]
                HA[:, ens_ind] = model.observation_operator_Jacobian_VecProd(eval_state=xb, state=state, t=obs_time).get_numpy_array()
            else:
              HA = np.empty((obs_size, ensemble_size))
              # HA[...] = np.NaN
              for ens_ind in range(ensemble_size):
                obs_vec = model.evaluate_theoretical_observation(forecast_ensemble[ens_ind], obs_time)
                HA[:, ens_ind] = obs_vec.get_numpy_array()
              # del obs_vec
              # Hxb = np.squeeze(np.mean(HA, 1))  # This is wrhong!
              # Convert to matrix of observations anomalies
              for i in range(ensemble_size):
                HA[:, i] -= Hxb
              # del Hx

            #
            if self.output_configs['debug_mode']:
              # Check xb for Nans or Infs
              print("\n\n***")
              projected_anomalies = HA[observation_not_nan_locs, :]
              if np.isnan(projected_anomalies).any():
                  print("the matrix of projected ensemble of anomalies contains NaN values at the observation entries")
                  print(projected_anomalies)
                  # raise ValueError
              else:
                  print("the matrix of projected ensemble of anomalies does NOT contains any NaN values at the observation entries")
              print("\n\n***")
            #

            # observation innovation: y - H(xb)
            obs_innov = y.axpy(-1.0, Hxb, in_place=False)
            obs_innov[observation_nan_locs] = 0.0

            #
            if self.output_configs['debug_mode']:
              print("\n\n***")
              print("xb: ", xb)
              print("Hxb: ", Hxb)
              print("Inside EnKF; observation_not_nan_locs : ", observation_not_nan_locs)
              print("Hxb[observation_not_nan_locs] : ", Hxb[observation_not_nan_locs])
              print("y[observation_not_nan_locs] ", y[observation_not_nan_locs])
              #
              print("obs_innov Y-H(xb): ", obs_innov)
              print("\n***")
            #

            # print("xb:", xb)
            # print("Hxb: ", Hxb)
            # print("y: ", y)
            # print("obs_innov:", obs_innov)

            # observation covariance scaling factor:
            rfactor = float(self.filter_configs['obs_covariance_scaling_factor'])

            # get the current state of the random number generator
            curr_rnd_state = np.random.get_state()

            # START ASSIMILATION:
            # -------------------
            if self._verbose or self.output_configs['debug_mode']:
              print("Constructing S, s "),
            _s = sqrtR_inv_VecProd(obs_innov, in_place=False)
            _s.scale(1.0 / np.sqrt(ensemble_size-1.0), in_place=True)
            s = _s.get_numpy_array()
            S = sqrtR_inv_VecProd(HA, in_place=False) / np.sqrt(ensemble_size-1.0)
            if self._verbose or self.output_configs['debug_mode']:
              print("Done.")

            localize_covariances = self.filter_configs['localize_covariances']
            if not localize_covariances:
              print("NO localization; proceed with analysis...")
              # DONOT Apply covariance localization (Global analysis)

              if ensemble_size <= obs_size:
                # Calculte G = (I + (S^T * S))^{-1} * S^T
                G = np.dot(S.T, S)
                G[np.diag_indices_from(G)] += 1.0
                G = np.linalg.inv(G).dot(S.T)  # TODO: <<-- User Iterative solver
              else:
                # Calculte G = S^T * (I + S * S^T)^{-1}
                G = np.dot(S, S.T)
                G[np.diag_indices_from(G)] += 1.0
                G = np.dot(S.T, np.linalg.inv(G))  # TODO: <<-- User Iterative solver

              # Evaluate the Ensemble-Mean update:
              xf_update = A.dot(G).dot(s)  # dx
              # print("type(xf_update)", type(xf_update))
              # print("type(G)", type(G))
              # print("type(s)", type(s))
              # del s

              # Evaluate Ensemble-Anomalies update:
              if rfactor != 1.0:
                # rescale S, and G
                S *= 1.0 / np.sqrt(rfactor)
                if ensemble_size <= obs_size:
                  # RE-Calculte G = (I + (S^T * S))^{-1} * S^T
                  G = S.T.dot(S)
                  G[np.diag_indices_from(G)] += 1.0
                  G = np.linalg.inv(G).dot(S.T)
                else:
                  # RE-Calculte G = S^T * (I + S * S^T)^{-1}
                  G = np.dot(S, S.T)
                  G[np.diag_indices_from(G)] += 1.0
                  G = S.T.dot(np.linalg.inv(G))
              else:
                pass

              # Now Evaluate A = A * (I - 0.5 * G * S):
              T_R = -0.5 * G.dot(S)
              T_R[np.diag_indices_from(T_R)] += 1.0
              A = A.dot(T_R)
              print("done.")

            else:
              # Apply covariance localization
              localization_method = self.filter_configs['localization_method']
              localization_function = self.filter_configs['localization_function']
              localization_radius = self.filter_configs['localization_radius']
              #
              if re.match(r'\Acovariance(-|_)*filtering\Z', localization_method, re.IGNORECASE):
                # Evaluate the Kalman gain matrix (with HPH^T, and PH^T localized based on the filter settings)
                K = self._calc_Kalman_gain(A, HA)

                if self.output_configs['debug_mode']:
                  np.save('./KalmanGain.npy', K)

                # print("type(K)", type(K))
                # print("K.shape: ", K.shape)

                # Evaluate the Ensemble-Mean update (dx):
                y_np = obs_innov.get_numpy_array()
                # print("type(y_np)", type(y_np))
                xf_update = K.dot(y_np)
                #

                # print("type(xf_update)", type(xf_update))

                # Recalculate the Kalman gain with observation variances/covariances multiplied by rfactor
                if rfactor != 1:
                  K = self._calc_Kalman_gain(A, HA, rfactor)

                # get the current state of the random number generator
                #
                if observation_nan_locs.size > 0:
                  A -= (0.5 * K[:, observation_not_nan_locs].dot(HA[observation_not_nan_locs, :]))
                else:
                  A -= (0.5 * K.dot(HA))

              else:
                print("Localization method '%s' is not Supported/Recognized!" % localization_method)
                raise ValueError
              #
              # Now we are good to go; update the ensemble mean, and ensemble-anomalies using ens_mean_update, and A

            #
            if self.output_configs['debug_mode']:
              print("\n\n***")
              print("obs_innov : ", obs_innov)
              print(" Kalman Gain: ", K)
              print("xf_update K * (y-H(xb)): ", xf_update)
              if np.isnan(xf_update).any():
                print("XF UPDATE has NaNs????")
                print("NAN LOCS:", np.where(np.isnan(xf_update)))
                print("NON-NAN LOCS:", np.where(~np.isnan(xf_update)))
                raise ValueError
              print("\n\n***")
            #

            #
            t0 , t1 = self.filter_configs['tspan'][0], self.filter_configs['tspan'][-1]
            analysis_time = t1 if self.filter_configs['forecast_first'] else t0
            
            xa = xf + xf_update
            analysis_state = model.state_vector()
            analysis_state[:] = xa.copy()
            analysis_state.time = analysis_time
            #
            if self.output_configs['debug_mode']:
              print("\n\n***")
              nan_locs = analysis_state.where_nan()
              if nan_locs.size > 0:
                print("Analysis state has NaNs at locations:")
                print(nan_locs)
                raise ValueError
              else:
                print("Analysis state does NOT have NaNs... CLEAN ...")
              print("\n\n***")
            #

            #
            analysis_ensemble = forecast_ensemble.copy_empty()
            member = model.state_vector()
            #
            for ens_ind in range(ensemble_size):
              member[:] = A[:, ens_ind] + xa
              member.time = analysis_time
              analysis_ensemble.append(member)
              #
              if self.output_configs['debug_mode']:
                print("-----------")
                nan_locs = member.where_nan()
                if nan_locs.size > 0:
                  print("Ensemble member [%d] has nan at localtions" % ens_ind)
                  print(nan_locs)
                else:
                  print("Ensemble member [%d] is ... CLEAN ..." % ens_ind)
                print("-----------")
              #

            print("Inflating analysis ensemble... "),
            # Inflate analysis ensemble (Xa <- analysis_ensemble)
            alpha = self.filter_configs['analysis_inflation_factor']
            try:
              alpha = model.create_inflation_vector(alpha)
            except:
              pass
            analysis_ensemble.inflate(alpha, in_place=True)
            print(".done.")
            # XXXXXXXXXXXXXXXXXXXXXX

          else:
            # Observation entries are all NaN; missing data, which means the analysis step should be waived
            print("No valid observation entries were found; All are NaN, i.e. missing data")
            print("*** Applying the forecast only ***")
            analysis_ensemble = forecast_ensemble.copy()

      else:
        # Branch for non-root ranks/processes
        # Analysis is carried out on root node only; move on to receiving local aalysis ensemble members
        pass

      # Sync all ensemble sizes
      # Node 0 scatters ensemble mebers
      if my_rank == 0:

        # Local ensemble sizes
        dist_ens_sizes = rcv_ensemble_sizes  # send back same number of members
        local_analysis_ensemble = analysis_ensemble
        # print("ROOT about to send back the analysis ensemble; I have %d members" % local_analysis_ensemble.size)
        # Now, distribute ensmbles
        for node_rank in range(comm_size-1, 0, -1):
          out_ens_size = dist_ens_sizes[node_rank]
          if out_ens_size > 0:
            for ens_ind in range(out_ens_size):
              vec = local_analysis_ensemble.pop()
              _ = forecast_ensemble.pop()  # same for forecast ensemble
              MPI_COMM.send(vec, dest=node_rank, tag=node_rank+6000+ens_ind)
              # print("ROOT:VVV: Now, I have %d members" % local_analysis_ensemble.size)
      else:
        # Collect ensembles; start with ensemble size
        local_analysis_ensemble = None
        print("Node %d; receiving %d ensemble members" % (my_rank, local_ensemble_size))
        if local_ensemble_size > 0:
          # Now, receive ensemble members
          for ens_ind in range(local_ensemble_size):
            # receive a state:
            vec = MPI_COMM.recv(source=0, tag=my_rank+6000+ens_ind)
            if ens_ind == 0:
              # initialize an ensemble to the given state
              local_analysis_ensemble = Ensemble(0, vec.size)
            local_analysis_ensemble.append(vec)
            if self._verbose:
              print("NODE %d:>>>: Now, I have %d members" % (my_rank, local_analysis_ensemble.size))

    else:  # parallel EnKF
      print("Parallel EnKF is YET to be implemented")
      raise NotImplementedError

    #
    if MPI_COMM is not None:
      MPI_COMM.Barrier()

    #
    if self._verbose and my_rank==0:
      print("\n%s: Finishing Analysis Step... \n%s\n" % (self.filter_name, sep))

    return local_analysis_ensemble
    #
