

# Maintaining copyright of DATeS:
#
# ########################################################################################## #
#                                                                                            #
#   DATeS: Data Assimilation Testing Suite.                                                  #
#                                                                                            #
#   Copyright (C) 2016  A. Sandu, A. Attia, P. Tranquilli, S.R. Glandon,                     #
#   M. Narayanamurthi, A. Sarshar, Computational Science Laboratory (CSL), Virginia Tech.    #
#                                                                                            #
#   Website: http://csl.cs.vt.edu/                                                           #
#   Phone: 540-231-6186                                                                      #
#                                                                                            #
#   This program is subject to the terms of the Virginia Tech Non-Commercial/Commercial      #
#   License. Using the software constitutes an implicit agreement with the terms of the      #
#   license. You should have received a copy of the Virginia Tech Non-Commercial License     #
#   with this program; if not, please contact the computational Science Laboratory to        #
#   obtain it.                                                                               #
#                                                                                            #
# ########################################################################################## #
#


"""
    FilteringProcess:
    A class implementing functionalities of a filtering process.
    A filtering process here refers to repeating a filtering cycle over a specific observation/assimilation timespan

    The code here is a modified version of the corresponding module in DATeS:
        http://people.cs.vt.edu/~attia/DATeS/index.html

    Remarks:
    --------
        - The main methods here update the DA items and update the appropriate configs dictionaries in the filter object.
        - The filter object itself is responsible for input/output.
"""

import numpy as np
import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import shutil

sys.path.append("../")
sys.path.append("../Utility")
sys.path.append("../LinAlg")
import DL_utility as utility
from DLidarVec import StateVector, Ensemble

try:
    import DL_constants
except(ImportError):
    pass



# TODO: Check the update of time property of states and observations; The assigned values here could be non-scalar!

class FilteringProcess(object):
    """
    A class implementing the steps of a filtering process.
    This recursively/iteratively apply filtering on several assimilation cycles given assimilation/observation timespan.

    Filtering process class constructor.
    The input configurarions are used to control the assimilation process behavious over several cycles.

    Args:
        assimilation_configs (default None); a dictionary containing assimilation configurations.
        Supported configuarations:
            * filter (default None): filter object
            * obs_checkpoints (default None): iterable containing an observation timespan
               - thsese are the time instances at which observation (synthetic or not) are given/generated
            * da_checkpoints (default None): iterable containing an assimilation timespan
               - thsese are the time instances at which filtering is carried out
               - If same as obs_checkpoints, assimilation is synchronous.
            * synchronous (default None): Now it has no effect. this will be removed.
            * ref_initial_condition (default None): model.state_vector object used as a true initial state
            * ref_initial_time (default 0):  the time at which 'ref_initial_condition' is defined
            * forecast_first (default True): Forecast then Analysis or Analysis then Forecast steps
            * random_seed (default None): An integer to reset the random seed of Numpy.random
              - resets the seed of random number generator before carrying out any filtering steps.
            * callback (default None): A function to call after each assimilation (filtering) cycle)
            * callback_args (default None): arguments to pass to the callback function.


        output_configs (default None); a dictionary containing screen/file output configurations:
        Supported configuarations:
            * scr_output (default True): Output results to screen on/off switch
            * scr_output_iter (default 1): number of iterations/windows after which outputs are printed
            * file_output (default False): save results to file on/off switch
            * file_output_iter (default 1): number of iterations/windows after which outputs are saved to files
            * file_output_dir (default None): defaul directory in which results are written to files

    Returns:
        None

    """
    # Default filtering process configurations
    _def_assimilation_configs = dict(filter=None,
                                     MPI_COMM=None,
                                     obs_checkpoints=None,
                                     observations=None,
                                     da_checkpoints=None,
                                     time_unit=None,
                                     synchronous=None,
                                     ref_initial_condition=None,
                                     free_run_state=None,
                                     initial_ensemble=None,
                                     ref_initial_time=None,  # the time at which 'ref_initial_condition' is passed.
                                     forecast_first=True,  # Forecast then Analysis (default).
                                     random_seed=None,  # Reset the random seed before carrying out any filtering steps.
                                     callback=None,
                                     callback_args=None,
                                     )

    _def_output_configs = dict(scr_output=True,
                               scr_output_iter=1,
                               file_output=False,
                               file_output_iter=1,
                               file_output_dir=None,
                               verbose=False,
                               )

    #
    def __init__(self, assimilation_configs=None, output_configs=None):
        #
        self.assimilation_configs = self.validate_assimilation_configs(assimilation_configs,
                                                                       FilteringProcess._def_assimilation_configs)
        self.output_configs = self.validate_output_configs(output_configs, FilteringProcess._def_output_configs)
        self._verbose = self.output_configs['verbose']
        #
        # Make sure the directory is created and cleaned up at this point...
        self.set_filtering_output_dir(self.output_configs['file_output_dir'], rel_to_root_dir=True)
        #
        # Added model, observation, and filter configurations to a setup file in that directory
        filter_obj = self.assimilation_configs['filter']
        model = filter_obj.model
        model_conf = model.get_model_configs()
        obs_conf = model.get_observation_configs()
        filter_conf = filter_obj.get_filter_configs(details='basic')
        filter_out_conf = filter_obj.get_output_configs()
        file_output_dir= self.output_configs['file_output_dir']
        utility.write_dicts_to_config_file('setup.dat', file_output_dir,
                                           [model_conf, obs_conf, filter_conf, filter_out_conf],
                                           ['Model Configs', 'Observation Configs', 'Filter Configs', 'Filter Output Configs'])

        # Save model information to file in the same location
        model.save_error_models(location=file_output_dir, save_obs_error=True, save_prior_error=False, save_model_error=False)
        #
    #
    def start_assimilation_process(self, observations=None, obs_checkpoints=None, da_checkpoints=None):
        """
        Loop over all assimilation cycles and output/save results (forecast, analysis, observations)
        for all the assimilation cycles.

        Args:
            observations (default None): list of obs.observation_vector objects,
                A list containing observaiton vectors at specific obs_checkpoints to use for sequential filtering
                If not None, len(observations) must be equal to len(obs_checkpoints).
                If it is None, synthetic observations should be created sequentially

            obs_checkpoints (default None): iterable containing an observation timespan
                Thsese are the time instances at which observation (synthetic or not) are given/generated

            da_checkpoints (default None): iterable containing an assimilation timespan
                Thsese are the time instances at which filtering is carried out.
                If same as obs_checkpoints, assimilation is synchronous.

        Returns:
            None

        """
        # Get reference to the filtering object, and the forward operator (model)
        filter_obj = self.assimilation_configs['filter']
        model_obj = filter_obj.model

        # Setting verbosity of the filter to match this object
        filter_obj.verbose = self._verbose

        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        if MPI_COMM is not None:
            MPI_COMM.Barrier()

        if MPI_COMM is not None and my_rank==0:
            print("\n%s\n\t Root node; an MPI-COMM is found with %d parallel nodes..." % ("~"*80, comm_size))
            print("\t Starting An Assimilation Process using [%s Filter] \n%s\n" % (filter_obj.filter_name, "~"* 80))
        elif my_rank==0:
            print("Serial run")

        # Check availability of true/reference solution:
        reference_state = self.assimilation_configs['ref_initial_condition']
        if reference_state is None:
            reference_time = None
            if self._verbose and my_rank==0:
                print("No reference soltution found; No synthetic observations can be created, nor statistics e.g. RMSE can be calculated!")
        else:
            reference_time = reference_state.time
            self.assimilation_configs['ref_initial_time'] = reference_time
            # if self.assimilation_configs['ref_initial_time'] is None:
            #     self.assimilation_configs['ref_initial_time'] = reference_state.time
            # reference_time = self.assimilation_configs['ref_initial_time']

        #
        # TODO: All nodes need to check if observations passed as argument if not None, and communicate to syncronize; then add barrier
        print("observations is None: ", observations is None)
        print("obs_checkpoints is None: ", obs_checkpoints is None)

        #
        # Check availability of observations and observations' and assimilations checkpoints
        if observations is not None and obs_checkpoints is not None:
            # override process configurations and use given obs_checkpoints and observations list.
            # This will be useful if non-synthetic observations are to be used.
            if len(obs_checkpoints) != len(observations):
                print("The number of observations %d does not match the number of observation checkpoints %d" % (len(obs_checkpoints), len(observations)))
                raise ValueError
            else:
                self.assimilation_configs['obs_checkpoints'] = [obs for obs in obs_checkpoints]
                self.assimilation_configs['observations'] = [obs for obs in observations]

            if da_checkpoints is None:
                if self.assimilation_configs['da_checkpoints'] is not None:
                    da_checkpoints = self.assimilation_configs['da_checkpoints']
                else:
                    da_checkpoints = [d for d in obs_checkpoints]
                    self.assimilation_configs['da_checkpoints'] = da_checkpoints
            else:
                self.assimilation_configs['da_checkpoints'] = [d for d in da_checkpoints]

            # No need to create synthetic observations, as real observations exist
            create_synthetic_observations = False
        else:
            #
            if observations is None:
                if self.assimilation_configs['observations'] is None and reference_state is not None:
                    create_synthetic_observations = True
                elif reference_state is None:
                    create_synthetic_observations = False
                    observations = None
                    if my_rank == 0:
                        print("Couldn't find reference state of observations. Will try to collect real observations, using the underlying model.")
                else:
                    observations = [obs for obs in self.assimilation_configs['observations']]
                    create_synthetic_observations = False

            if obs_checkpoints is None:
                if self.assimilation_configs['obs_checkpoints'] is None:
                    raise ValueError
                else:
                    obs_checkpoints = [o for o in self.assimilation_configs['obs_checkpoints']]

            if da_checkpoints is None:
                if self.assimilation_configs['da_checkpoints'] is None:
                    da_checkpoints = obs_checkpoints
                else:
                    da_checkpoints = [o for o in self.assimilation_configs['da_checkpoints']]

        #
        if create_synthetic_observations and reference_state is None:
            if my_rank == 0:
                print("synthetic observations are to be created, yet the reference state couldn't be found in the assimilation_configs dictionary!")
                raise ValueError
            else:
                print("Starting the assimilation process without observations on Node %d " % my_rank)

        if my_rank == 0 and not create_synthetic_observations:
            # Another sanity check for entries of the observations list
            if observations is not None:
                all_is_none = True
                for obs in observations:
                    if obs is not None:
                        all_is_none = False
                if all_is_none:
                    print("WARNING: All entries of the observation list are None! This means the model states will be trusted; this is just a forecast step***")

        # Get inintial forecast/analysis ensemble
        initial_ensemble = self.assimilation_configs['initial_ensemble']
        if initial_ensemble is None:
            init_ensemble_time = None
        else:
            if initial_ensemble.size > 0:
                init_ensemble_time = initial_ensemble[0].time
            else:
                init_ensemble_time = None
        #
        #  Construct a proper experiment_timespan:
        if my_rank == 0:
            if init_ensemble_time < da_checkpoints[0]:
                # print("init_ensemble_time < da_checkpoint: ", init_ensemble_time, da_checkpoints)
                experiment_timespan = da_checkpoints
                experiment_timespan = [init_ensemble_time] + experiment_timespan
                self.assimilation_configs['forecast_first'] = True
            elif init_ensemble_time == da_checkpoints[0]:
                experiment_timespan = da_checkpoints
                if self.assimilation_configs['forecast_first']:
                    self.assimilation_configs['forecast_first'] = False
            else:
                if my_rank == 0:
                    print("\nThe forecast ensemble is given after the initial assimialtion time point!")
                    print("Initial ensemble time: %f " % init_ensemble_time)
                    print("First assimialtion time instance: %s \n\n" % repr(da_checkpoints[0]))
                raise AssertionError
            forecast_first = self.assimilation_configs['forecast_first']
        else:
            experiment_timespan = None
            forecast_first = None

        if MPI_COMM is not None:
            experiment_timespan = MPI_COMM.bcast(experiment_timespan, root=0)
            forecast_first = MPI_COMM.bcast(forecast_first, root=0)
        self.assimilation_configs.update({'forecast_first':forecast_first,
                                          'experiment_timespan':experiment_timespan})

        # print(init_ensemble_time, experiment_timespan[0], initial_ensemble[0].time)

        if initial_ensemble is None:
            if my_rank==0:
                print("Failed to read the initial ensemble!")
                raise ValueError
            else:
                init_ensemble_time = experiment_timespan[0]
        else:
            try:
                if initial_ensemble.size > 0:
                    init_ensemble_time = initial_ensemble[0].time
                else:
                    init_ensemble_time = experiment_timespan[0]
            except:
                init_ensemble_time = experiment_timespan[0]

        # print(my_rank, init_ensemble_time, experiment_timespan[0])
        # print(">>> NODE [%d/%d]" % (my_rank, comm_size))
        # print(experiment_timespan)

        if False:  # TODO: remove after debugging
            print(">>> NODE [%d/%d]" % (my_rank, comm_size))
            print(">>> init_ensemble_time: ", init_ensemble_time)
            print(">>> experiment_timespan: ", experiment_timespan)
            print(">>> da_checkpoints: ", da_checkpoints)

        if my_rank == 0:
            if reference_time is not None:
                #
                if reference_time < experiment_timespan[0]:
                    if self._verbose:
                        print("Propagating the initial reference state to the beginning of the experiment_timespan; i.e. from %s to %s" %(reference_time, experiment_timespan[0]))
                    # propagate the reference state to the beginning of the experiment_timespan:
                    _, traject = model_obj.integrate_state(reference_state, [reference_time,
                                                                             experiment_timespan[0]],
                                                           model_screen_output=self._verbose)
                    self.assimilation_configs['reference_state'] = traject[-1]
                    reference_state = self.assimilation_configs['reference_state']  # no need!

                elif reference_time == experiment_timespan[0]:
                    pass
                else:
                    print("\nThe reference state is given after the beginning of the experiment_timespan!")
                    print("Reference state time: %f " % reference_time)
                    print("Initial time: %s \n\n" % repr( experiment_timespan[0]))
                    raise AssertionError

        # experiment_timespan to scalars
        time_unit = self.assimilation_configs['time_unit']
        if time_unit is None:
            try:
                time_unit = model.model_time_unit
            except:
                try:
                    time_unit = model.time_unit
                except:
                    if my_rank == 0:
                        print("YOU MUST SET TIME UNIT to a valid string; 'M' for Minutes, 'H' for Hours, 'S' for Seconds.\n\
                            \r This is required to handle real time instances correctly")
                        print("Trying with None!")
                    time_unit = None
            finally:
                self.assimilation_configs.update({'time_unit':time_unit})

        # All is GOOD!

        # Additional sanity check:
        if init_ensemble_time != experiment_timespan[0]:
            if my_rank == 0:
                print("This is unexpected! experiment_timespan[0] is different from initial ensemble time!")
                print("assimilation_configs['initial_ensemble'][0].time = %f" % self.assimilation_configs['initial_ensemble'][0].time)
                print("experiment_timespan[0] = %s" % repr(experiment_timespan[0]))
                raise ValueError
            else:
                if initial_ensemble is not None:
                   print(my_rank, init_ensemble_time, experiment_timespan[0])
                   raise ValueError

        # Pass the initial ensemble to the filter to start assimilation cycles
        if forecast_first:
            # given the analysis ensemble:
            filter_obj.analysis_ensemble = self.assimilation_configs['initial_ensemble']
            # filter_obj.forecast_ensemble = None  # <- this will through an assertion exception; put empty ensemble if you need!
        else:
            filter_obj.forecast_ensemble = self.assimilation_configs['initial_ensemble']
            # filter_obj.analysis_ensemble = None

        # Set/Update random states generator seed:
        random_seed = self.assimilation_configs['random_seed']
        if random_seed is not None:
            # get the current state of the random number generator
            curr_rnd_state = np.random.get_state()
            np.random.seed(random_seed)
            try:
                filter_obj.random_seed = None
            except(AttributeError):
                pass

        # Initialize the output variable for callback returns:
        callback_function = self.assimilation_configs['callback']
        if callback_function is not None:
            callback_args = self.assimilation_configs['callback_args']
            callback_output = dict()
        else:
            callback_args = callback_output = None

        #
        # Set the reference state in the filter (for the initial cycle):
        # check the reference initial condition
        if reference_state is not None:
            filter_obj.reference_state = reference_state.copy()
        
        # Set free-run-state (if available)
        free_run_state = self.assimilation_configs['free_run_state']
        if free_run_state is not None:
            filter_obj.free_run_state = free_run_state
        else:
            print("WARNING: Couldn't retreive a Free-Run-State; won't be passed to the filter object.")

        if my_rank==0 and self._verbose:
            print("\n%s\n  CHECKPOINTS  \n%s\n\n" % ("*"*50, "*"*50))
            #
            print("forecast_first: ", forecast_first)
            print("range(len(experiment_timespan)-1), experiment_timespan[:-1], experiment_timespan[1:]: VVV")
            print(range(len(experiment_timespan)-1), experiment_timespan[:-1], experiment_timespan[1:])
            print("create_synthetic_observations: ", create_synthetic_observations)
            print("obs_checkpoints: ", obs_checkpoints)
            print("da_checkpoints: ", da_checkpoints)
            print("experiment_timespan: ", experiment_timespan)
            print("\n%s\n  DONE  \n%s\n\n" % ("*"*50, "*"*50))

        #
        # Loop over each two consecutive points in the experiment_timespan to create cycle limits
        for time_ind, t0, t1 in zip(range(len(experiment_timespan)-1), experiment_timespan[:-1], experiment_timespan[1:]):
            #
            cycle_checkpoints = [t0, t1]
            if my_rank == 0:
                cycle_obs_times = []
                cycle_observations = []
                for i, obs_time in enumerate(obs_checkpoints):
                    if forecast_first:
                        if t0 < obs_time <= t1:
                            cycle_obs_times.append(obs_time)
                            if observations is not None:
                                cycle_observations.append(observations[i])
                            else:
                                cycle_observations = None
                    else:
                        if  t0 <= obs_time < t1:
                            cycle_obs_times.append(obs_time)
                            if observations is not None:
                                cycle_observations.append(observations[i])
                            else:
                                cycle_observations = None
            else:
                cycle_obs_times = None
                cycle_observations = None

            if MPI_COMM is not None:
                cycle_obs_times = MPI_COMM.bcast(cycle_obs_times, root=0)
                cycle_observations = MPI_COMM.bcast(cycle_observations, root=0)


            if my_rank == 0 and cycle_observations is not None:
                print("+ Cycle %s; Done broadcasting REAL obbservations." % repr(cycle_checkpoints))
            elif my_rank == 0:
                print("No real observations found; will attempt to create synthetic observations")
                create_synthetic_observations = True
            else:
                create_synthetic_observations = None

            # broadcast synthetic observations
            if MPI_COMM is not None:
                create_synthetic_observations = MPI_COMM.bcast(create_synthetic_observations, root=0)

            # Check for valid observations!
            if len(cycle_obs_times) > 1:
                print("Assimilating multiple observations at the same time is not supported!")
                raise ValueError
            elif len(cycle_obs_times) == 0:
                if my_rank == 0:
                    print("NODE [%d/%d] Found no observations in this Cycle [t0, t1] = %s.\n Proceeding to next cycle..." % (my_rank, comm_size, cycle_checkpoints))
                print("\n\n\nCONTI[Node: %d]NUEING >>>\n\n\n" % my_rank)
                sys.stdout.flush()
                continue
            else:
                # One observation time
                observation_time = cycle_obs_times[0]

                if cycle_observations is not None:
                    observation = cycle_observations[0]
                    if observation is None:
                        pass
                    else:
                        if observation.time != observation_time:
                            observation.time = observation_time
                else:
                    observation = None
                    if my_rank == 0 and not create_synthetic_observations:
                        print("Failed to get the observations while create_synthetic_observations is False!")
                        print("This means the filter will pass this cycle, and just move forward")
                        # raise ValueError

            #
            if create_synthetic_observations:
                print("Node [%d/%d]; getting into synthethizing observations..." %(my_rank, comm_size)),
                sys.stdout.flush()

                if my_rank == 0:
                    print("ROOT Creating synthetic observations ... "),
                    sys.stdout.flush()
                    xtrue = reference_state.copy()
                    if forecast_first:
                        _, ref_traject = model_obj.integrate_state(xtrue, cycle_checkpoints,
                                                                   model_screen_output=self._verbose)
                        xtrue = ref_traject[-1]
                        del ref_traject
                    observation = model_obj.evaluate_theoretical_observation(xtrue)
                    # print('Theoritical REFERENCE Observation', observation)
                    observation = model_obj._obs_err_model.add_noise(observation, in_place=True)
                    # print('SCALED Observation', observation)
                    # print("done...")
                else:
                    if observation is not None:
                        print("Node %d has a real observation that will be destroyed!" % my_rank)
                    observation = None


                if MPI_COMM is not None:
                    # broadcast observation
                    observation = MPI_COMM.bcast(observation, root=0)
                    print("Cycle %s; Done broadcasting synthetic obbservations to NODE {%d/%d}..." % (repr(cycle_checkpoints), my_rank, comm_size))
                    print("done...")

            elif my_rank==0:
                print("Using real observations")  # remove after debugging

            #
            # Output settings:
            if self.output_configs['scr_output']:
                if (time_ind % self.output_configs['scr_output_iter']) == 0:
                    scr_output = True  # switch on/off screen output in the current filtering cycle
                else:
                    scr_output = False
            else:
                scr_output = False  # switch on/off screen output in the current filtering cycle

            if self.output_configs['file_output']:
                if (time_ind % self.output_configs['file_output_iter']) == 0:
                    file_output = True  # switch on/off file output in the current filtering cycle
                else:
                    file_output = False
            else:
                file_output = False  # switch on/off screen output in the current filtering cycle

            if MPI_COMM is not None:
                MPI_COMM.Barrier()

            if time_ind == 0:
                # Save initial results to file:
                try:
                    filter_obj.tspan = [t0, t1]
                    filter_obj.save_cycle_results()
                except:
                    print("Failed to save cycle information at the beginning of the of the process")
                    # raise
            else:
                pass

            if MPI_COMM is not None:
                MPI_COMM.Barrier()
            #

            # attempt to udpate the model's observation operator:
            try:
                model_obj.update_observation_operator(observation=observation)
            except(TypeError):
                print("Type Error caught while attempting to update the observation operator with an observation instance")
                try:
                    model_obj.update_observation_operator()
                except:
                    print("Check the arguments of the model function updating the observation oprator or the observational grid!")
                    raise
            except(AttributeError, NotImplementedError):
                if my_rank == 0:
                    print("Model does not support updating the observation oprator or the observational grid. SKIPPING UPDATE")
                pass

            if False:
                print("Updated observation operator...")
                print("Observation: ", observation)
                print("Observation valid indexes")
                print(model_obj._observation_valid_indexes)

            # Apply one assimilation/filtering cycle:
            self.assimilation_cycle(assimilation_timespan=cycle_checkpoints,
                                    observation=observation,
                                    scr_output=scr_output,
                                    file_output=file_output)


            # Call the callback function if provided:
            if callback_function is not None:
                _cout = callback_function(callback_args)
                callback_output.update({str(time_ind): _cout})

            # Sync point
            if MPI_COMM is not None:
                MPI_COMM.Barrier()

            print("POST LOOP check: Node: %d " % my_rank)
            sys.stdout.flush()


        # Restore NP random state
        if random_seed is not None:
            # get the current state of the random number generator
            np.random.set_state(curr_rnd_state)

        # print("\n%s\n\t <<< Assimilation Process Complete on Node [%d] \n%s\n\n" % ("~"* 80, my_rank, "~"* 80))
        #
        return callback_output
        #

    #
    def assimilation_cycle(self, assimilation_timespan, observation, scr_output=False, file_output=False):
        """
        Carry out filtering for the next assimilation cycle.
        Given the assimilation cycle is carried out by applying filtering using filter_object.filtering_cycle().
        filter_object.filtering_cycle() itself should carry out the two steps: forecast, analysis.
        This function modifies the configurations dictionary of the filter object to carry out a single
        filtering cycle.

        Args:
            assimilation_timespan: an iterable containing the beginning and the end of the assimilation cycle i.e. len=2
                A timespan for the filtering process on the current assimilation cycle.


            observation: model.observation_vector object,
                The observaiton vector to use for assimilation/filtering

            scr_output (default False): bool,
                A boolean flag to cotrol whether to output results to the screen or not
                The per-cycle screen output options are set by the filter configs.

            file_output (default False): bool,
                A boolean flag to cotrol whether to save results to file or not.
                The per-cycle file output options are set by the filter configs.

        Returns:
            None

        """
        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        #
        filter_obj = self.assimilation_configs['filter']
        assimilation_configs = self.assimilation_configs
        #

        # TODO: think; each node will update filter instance on the corresponding core;  what if there are more than one communicator!
        # get and update the configurations of the filter. Both output and filter configs are handled here
        # Update filter configs
        if filter_obj.forecast_first != assimilation_configs['forecast_first']:
            filter_obj.forecast_first = assimilation_configs['forecast_first']

        filter_obj.observation = observation
        filter_obj.tspan = assimilation_timespan

        # Update output configs (based on flag and iterations)
        if filter_obj.scr_output != scr_output:
            filter_obj.scr_output = scr_output
        if filter_obj.file_output != file_output:
            filter_obj.file_output = file_output

        # Now start the filtering cycle.
        filter_obj.filtering_cycle()

        # for MPI comm with more than one node; might be overkill!
        if comm_size > 1:
            MPI_COMM.Barrier()

    #
    def set_filtering_output_dir(self, file_output_dir, rel_to_root_dir=True, backup_existing=True):
        """
        Set the output directory of filtering results.

        Args:
            file_output_dir_path: path/directory to save results under
            rel_to_root_dir (default True): the path in 'output_dir_path' is relative to DLiDA root dir or absolute
            backup_existing (default True): if True the existing folder is archived as *.zip file.

        Returns:
            None

        """
        # check MPI Communicator:
        MPI_COMM = self.assimilation_configs['MPI_COMM']
        if MPI_COMM is not None:
            comm_size = MPI_COMM.Get_size()
            my_rank = MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0

        # Make sure the directory is created and cleaned up at this point...
        # TODO: think; each node will update filter instance on the corresponding core;  what if there are more than one communicator!
        file_output = self.output_configs['file_output']
        if file_output:
            #
            DLiDA_root_dir = os.environ.get('DL_ROOT_PATH')
            if DLiDA_root_dir is None:
                try:
                    DLiDA_root_dir = DL_constants.DL_ROOT_PATH
                except:
                    DLiDA_root_dir = os.getcwd()
            if rel_to_root_dir:
                file_output_dir = os.path.join(DLiDA_root_dir, file_output_dir)
            #
            parent_path, out_dir = os.path.split(file_output_dir)
            if my_rank == 0:
                utility.cleanup_directory(directory_name=out_dir, parent_path=parent_path, backup_existing=True, zip_backup=False)
            # Override output configurations of the filter:
            # self.file_output_dir = file_output_dir
            self.output_configs['file_output_dir'] = file_output_dir
        else:
            file_output_dir = None
        #
        filter_obj = self.assimilation_configs['filter']
        # Override output configurations of the filter:
        filter_obj.file_output = file_output
        filter_obj.file_output_dir = file_output_dir
        #
        if comm_size > 1:
            MPI_COMM.Barrier()

    #
    @staticmethod
    def validate_assimilation_configs(assimilation_configs, def_assimilation_configs):
        """
        Aggregate the passed dictionaries with default configurations then make sure parameters are consistent.
        The first argument (assimilation_configs) is validated, updated with missing entries, and returned.

        Args:
            assimilation_configs: dict,
                A dictionary containing assimilation configurations. This should be the assimilation_configs dict
                passed to the constructor.

            def_assimilation_configs: dict,
                A dictionary containing the default assimilation configurations.

        Returns:
            assimilation_configs: dict,
                Same as the first argument (assimilation_configs) but validated, adn updated with missing entries.

        """
        assimilation_configs = utility.aggregate_configurations(assimilation_configs, def_assimilation_configs)
        # Since aggregate never cares about the contents, we need to make sure now all parameters are consistent
        for key in assimilation_configs:
            if key not in def_assimilation_configs:
                print("Caution: Unknown key/feature detected: '%s' is ignored" % key)
        if assimilation_configs['filter'] is None:
            print("You have to create a filter object and attach it here so that I can use it for sequential DA!")
            raise ValueError

        # Reference to filter and model objects:
        filter_obj = assimilation_configs['filter']
        if filter_obj is None:
            print("Failed to retrieve a filter object!")
            raise ValueError
        model = filter_obj.model
        if model is None:
            print("Failed to retrieve a forward operator object!")
            raise ValueError

        # Check for parallel processes
        my_MPI_COMM = assimilation_configs['MPI_COMM']
        if my_MPI_COMM is not None:
            comm_size = my_MPI_COMM.Get_size()
            my_rank = my_MPI_COMM.Get_rank()
        else:
            comm_size = 1
            my_rank = 0
        if comm_size > 1 and my_MPI_COMM is not None:
            if my_rank == 0:
                print("ROOT: started validating the assimilation configurations for %d Nodes..." % (comm_size))

        # TODO: that approach here for handling MPI-COMM MUST BE TESTED, and REVISED
        filter_MPI_COMM = filter_obj.MPI_COMM

        if my_MPI_COMM is None and filter_MPI_COMM is None:
            if self._verbose:
                print("Serial run ... Validating Assimilation Configs ...")
        elif not(my_MPI_COMM is None or filter_MPI_COMM is None):
            #
            if my_MPI_COMM is filter_MPI_COMM:
                pass  # both use the same communicaotr; ...Normal...
                # print("Process [%d/%d] started validating the assimilation configurations..." % (my_rank, comm_size))
            else:
                # Two different communicators!!!
                if my_rank == 0:
                    print("Two communicators are passed; This might be OverKilling.\n Behaviour of this setting should be tested...")
                raise AssertionError
        else:
            print("Filtering process must be configured with/woithout MPI exactly same way as the filter object.")
            print("Set 'MPI_COMM' key in the configurations dictionary of both similarily.")
            print("Both must be either None, or must be set to the same MPI.COMM_WORLD")
            raise VaueError

        #
        time_unit = assimilation_configs['time_unit']
        if time_unit is None:
            try:
                time_unit = model.model_time_unit
            except:
                try:
                    time_unit = model.time_unit
                except:
                    print("YOU MUST SET TIME UNIT to a valid string; 'M' for Minutes, 'H' for Hours, 'S' for Seconds.")
                    print("This is required to handle real time instances correctly")
                    print("Trying with None!")
                    time_unit = None
            finally:
                assimilation_configs.update({'time_unit':time_unit})

        elif not isinstance(time_unit, str):
            print("YOU MUST SET TIME UNIT to a valid string; 'M' for Minutes, 'H' for Hours, 'S' for Seconds")
            raise TypeError
        else:
            time_unit = time_unit.lower().strip()
            if time_unit not in ['h', 'm', 's']:
                print("YOU MUST SET TIME UNIT to a valid string; 'M' for Minutes, 'H' for Hours, 'S' for Seconds")
                raise ValueError
            else:
                # good to go
                assimilation_configs['time_unit'] = time_unit

        # Check for observation and assimilation checkpoints and update synchronous accordingly
        obs_checkpoints = assimilation_configs['obs_checkpoints']
        da_checkpoints = assimilation_configs['da_checkpoints']
        #
        if my_rank == 0:
            # if assimilation_configs['da_checkpoints'] is not None and obs_checkpoints is not None:
            if 'obs_checkpoints' is not None:
                # observation checkpoints is built in full.
                if utility.isscalar(obs_checkpoints):
                    obs_checkpoints = [obs_checkpoints]

                if not utility.isiterable(obs_checkpoints):
                    print("obs_checkpoints isn't an iterable!\n\
                          \rPassed: %s" % str(obs_checkpoints))
                    raise TypeError
                else:
                    obs_checkpoints = [to for to in obs_checkpoints]
                    num_observations = len(obs_checkpoints)

                # Now check the assimilation checkpoints
                if da_checkpoints is not None:
                    if utility.isscalar(da_checkpoints):
                        da_checkpoints = [da_checkpoints]

                    if not utility.isiterable(da_checkpoints):
                        print("da_checkpoints isn't an iterable!\n\
                              \rPassed: %s" % str(da_checkpoints))
                        raise TypeError
                    else:
                        da_checkpoints = [to for to in da_checkpoints]
                        assimilation_configs['da_checkpoints'] = da_checkpoints
                        num_assimilation_cycles = len(da_checkpoints)

                    # compare the time instances at which assimilation is made vs. observations
                    if num_assimilation_cycles != num_observations:
                        print("Number of observations and number of assimilation cycles must match!\n\
                              \rNumber of assimilation cycles passed = %d\n\
                              \rNumber of observation time points = %d" % (num_assimilation_cycles, num_observations)
                              )
                        # TODO: Consider validation and/or aggregation rather than throwing an error!
                        raise ValueError
                    else:
                        # We are all good to go now. now check if the assimilation should be synchronous or not
                        assimilation_configs['synchronous'] = True
                        for t0, t1 in zip(obs_checkpoints, da_checkpoints):
                            if t0 != t1:
                                assimilation_configs['synchronous'] = False
                                break
                else:
                    assimilation_configs['da_checkpoints'] = assimilation_configs['obs_checkpoints']
                    assimilation_configs['synchronous'] = True  # No matter what!

            #
            elif assimilation_configs['da_checkpoints'] is not None:
                assimilation_configs['synchronous'] = True

            else:
                print("da_checkpoints and obs_checkpoints are not provided. "
                      "The alternatives: da_time_spacing and num_filtering_cycles are not provided either!"
                      "Filtering process needs one of the two alternatives!")
                raise ValueError
            #
            da_checkpoints = assimilation_configs['da_checkpoints']
            obs_checkpoints = assimilation_configs['obs_checkpoints']
            synchronous = assimilation_configs['synchronous']
        else:
            synchronous = None

        # sync da_checkpoints and obbs_checkpoints
        if my_MPI_COMM is not None and comm_size > 1:
            synchronous = my_MPI_COMM.bcast(synchronous, root=0)
            da_checkpoints = my_MPI_COMM.bcast(da_checkpoints, root=0)
            obs_checkpoints = my_MPI_COMM.bcast(obs_checkpoints, root=0)
            if my_rank != 0:
                assimilation_configs.update({'da_checkpoints':da_checkpoints,
                                             'obs_checkpoints':obs_checkpoints,
                                             'synchronous':synchronous})

        if my_rank == 0:
            # ref_initial_time
            ref_t0 =  assimilation_configs['ref_initial_time']
            if ref_t0 is not None:
                if type(ref_t0) is type(obs_checkpoints[0]):
                    pass  # TODO: to be updated...
                else:
                    print("ref_initial_time must be of the same type as obs_checkpoints entries!")
                    raise TypeError
            else:
                if utility.isscalar(obs_checkpoints[0]):
                    ref_ic = assimilation_configs['ref_initial_condition']
                    if ref_ic is not None:
                        ref_t0 = ref_ic.time
                    else:
                        ref_t0 = None
                    if ref_ic is None:
                        print("Since no reference initial time is given, and no ref_IC and/or attached time,")
                        print("We assume reference initial time is observation_time[0]!")
                        ref_t0 = obs_checkpoints[0]


        # Now, Duplicate timespans with scalar entities (to match model.state_vector.time)
        if assimilation_configs['synchronous']:
            scalar_obs_checkpoints = utility.timespan_to_scalars(obs_checkpoints, time_diff_unit=time_unit)
            scalar_da_checkpoints = scalar_obs_checkpoints
        else:
            scalar_obs_checkpoints = utility.timespan_to_scalars(obs_checkpoints, time_diff_unit=time_unit)
            scalar_da_checkpoints = utility.timespan_to_scalars(da_checkpoints, time_diff_unit=time_unit)
        # Set checkpoints properly:
        assimilation_configs.update({'obs_checkpoints': scalar_obs_checkpoints,
                                     'da_checkpoints': scalar_da_checkpoints})

        # print("NODE, obs_timespan, da_timespan ", my_rank, scalar_obs_checkpoints, scalar_da_checkpoints)
        assimilation_configs.update({'original_obs_checkpoints': obs_checkpoints,
                                     'original_da_checkpoints': da_checkpoints})

        if my_rank == 0:
            assimilation_configs.update({'ref_initial_time':0.0,
                                         'original_ref_initial_time':ref_t0})


        # check the reference initial condition and the reference initial time
        if my_rank == 0:
            ref_ic = assimilation_configs['ref_initial_condition']
            if ref_ic is None:
                print("Reference Solution is not give; Statistics, e.g. RMSE, won't be calculated!")
            else:
                # ref_ic.time = 0.0
                # reference state and reference time:
                if ref_ic.time > np.min(assimilation_configs['da_checkpoints']):
                    print("Some observation times or assimilation times are set before the time of the reference initial condition!")
                    raise ValueError
                elif ref_ic.time == assimilation_configs['da_checkpoints'][0]:
                    assimilation_configs['num_filtering_cycles'] = np.size(assimilation_configs['da_checkpoints']) - 1
                else:
                    assimilation_configs['num_filtering_cycles'] = np.size(assimilation_configs['da_checkpoints'])

                # check the first observation time against the reference initial time...
                if ref_ic.time > np.min(assimilation_configs['obs_checkpoints']):
                    print("Some observation times or assimilation times are set before the time of the reference initial condition!")
                    raise ValueError

        # update the number of assimilation cycles
        num_assimilation_cycles = len(da_checkpoints)
        assimilation_configs.update({'num_assimilation_cycles':num_assimilation_cycles})

        #
        # Split initial ensemble over all nodes, assuming node 0 has all ensemble members actually
        # TODO: This should be waived with the new implementation to resume from any cycle
        if my_MPI_COMM is not None and comm_size > 1:
            if my_rank == 0:
                # root node specifies who's taking what
                initial_ensemble = assimilation_configs['initial_ensemble']
                if initial_ensemble is None:
                    initial_ensemble = Ensemble(0, model.state_size())
                ens_size = initial_ensemble.size
                if  ens_size > 0:
                    free_run_state = initial_ensemble.mean()
                else:
                    free_run_state = None
                dist_ensemble_size = [ens_size//comm_size] * comm_size
                for i in range(ens_size%comm_size):
                    dist_ensemble_size[-i] += 1
                local_ensemble_size = [v for v in dist_ensemble_size]
            else:
                free_run_state = None
                dist_ensemble_size = None
                local_ensemble_size = None
            local_ensemble_size = my_MPI_COMM.scatter(local_ensemble_size, root=0)

            if my_rank == 0:
                for node_rank in range(1, comm_size):
                    ens_sent = 0
                    for ens_ind in range(dist_ensemble_size[node_rank]):
                        vec = initial_ensemble.pop()
                        # print("Sending...", my_rank, vec.time)
                        my_MPI_COMM.send(vec, dest=node_rank, tag=node_rank+2000+ens_ind)
                        ens_sent += 1
                    print("Root NODE: [%03d] ensembles successfully sent to out to NODE [%d] " %(ens_sent, node_rank))
                    print("Now, ROOT has %d ensemble members" % initial_ensemble.size)
            else:
                #
                initial_ensemble = Ensemble(0, model.state_size())
                for ens_ind in range(local_ensemble_size):
                    vec = my_MPI_COMM.recv(source=0, tag=my_rank+2000+ens_ind)
                    initial_ensemble.append(vec)
                    # print("Receiving...", my_rank, vec.time)

            assimilation_configs.update({'initial_ensemble':initial_ensemble, 'free_run_state':free_run_state})

        else:
            try:
                initial_ensemble = assimilation_configs['initial_ensemble']
                if initial_ensemble.size > 0:
                    free_run_state = initial_ensemble.mean()
                else:
                    free_run_state = None
            except(AttributeError, KeyError):
                free_run_state = None
            #
            assimilation_configs.update({'free_run_state':free_run_state})


        if my_MPI_COMM is not None:
            my_MPI_COMM.Barrier()

        if my_rank == 0:
            print("ROOT: DONE validating assimilation configurations on all [%d] nodes..." % (comm_size))

            #
        return assimilation_configs

    #
    @staticmethod
    def validate_output_configs(output_configs, def_output_configs):
        """
        Aggregate the passed dictionaries with default configurations then make sure parameters are consistent.
        The first argument (output_configs) is validated, updated with missing entries, and returned.

        Args:
            output_configs: dict,
                A dictionary containing output configurations. This should be the output_configs dict
                passed to the constructor.

            def_output_configs: dict,
                A dictionary containing the default output configurations.

        Returns:
            output_configs: dict,
                Same as the first argument (output_configs) but validated, adn updated with missing entries.

        """
        output_configs = utility.aggregate_configurations(output_configs, def_output_configs)
        # screen output
        if output_configs['scr_output']:
            # screen output is turned on, make sure iterations are positive
            if output_configs['scr_output_iter'] is not None:
                if isinstance(output_configs['scr_output_iter'], float):
                    output_configs['scr_output_iter'] = np.int(output_configs['scr_output_iter'])
                if output_configs['scr_output_iter'] <= 0:
                    output_configs['scr_output'] = 1
                if not isinstance(output_configs['scr_output_iter'], int):
                    output_configs['scr_output'] = 1
        else:
            output_configs['scr_output_iter'] = np.infty  # just in case

        # file output
        if output_configs['file_output']:
            if output_configs['file_output_iter'] is not None:
                if isinstance(output_configs['file_output_iter'], float):
                    output_configs['file_output_iter'] = np.int(output_configs['file_output_iter'])
                if output_configs['file_output_iter'] <= 0:
                    output_configs['file_output'] = 1
                if not isinstance(output_configs['file_output_iter'], int):
                    output_configs['file_output'] = 1
            #
            if output_configs['file_output_dir'] is None:
                output_configs['file_output_dir'] = 'Results/Filtering_Results'  # relative to DLiDA directory of course
            # Create the full path of the output directory ( if only relative dir is passed)
            DLiDA_root_dir = os.environ.get('DL_ROOT_PATH')
            if DLiDA_root_dir is None:
                try:
                    DLiDA_root_dir = DL_constants.DL_ROOT_PATH
                except:
                    DLiDA_root_dir = os.getcwd()
            # if not str.startswith(output_configs['file_output_dir'], DLiDA_root_dir):
            #     output_configs['file_output_dir'] = os.path.join(DLiDA_root_dir, output_configs['file_output_dir'])
            output_configs['file_output_dir'] = output_configs['file_output_dir'].rstrip(os.path.sep)

        else:
            output_configs['file_output_iter'] = np.infty  # just in case
            output_configs['file_output_dir'] = None

        return output_configs
        #
