
cimport numpy as np


cdef class EnKF:

  cdef :
    dict filter_configs
    dict output_configs

    dict _filter_statistics
    bint _initialized
    bint _verbose


  cpdef _calc_Kalman_gain(self, np.ndarray A, np.ndarray HA, float rfactor=*, bint ignore_nans=*)

  cdef int _c_dists(self, double x, double y, double z, np.ndarray grid_points, np.ndarray dists) except -1
  cdef double c_localization_coeff(self, double dist, double radius, int method) except -1
  cdef int c_localization_coeffs(self, np.ndarray dists, double radius, np.ndarray coeffs, int method) except -1
  cdef int c_localization_coeffs_multiple(self, np.ndarray dists, np.ndarray radii, np.ndarray coeffs, int method) except -1


cdef class DEnKF(EnKF):
  pass
