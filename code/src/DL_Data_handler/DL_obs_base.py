#! /usr/bin/env python


class DL_OBS_BASE(object):

    """
    A base class for an observation vector containing DL data

    Here are the assumptions:
        1- Observations in an observation vector are taken within dt from a specific time instances
        2- Observations in an observation vector are collected from one site facility with specific coordinats
        3- Observations are stored at a given time instance

    """

    def __init__(self, *args, **kwargs):
        raise NotImplementedError

    def size(self, *args, **kwargs):
        raise NotImplementedError

    def time(self, *args, **kwargs):
        raise NotImplementedError

    def site_facility(self, *args, **kwargs):
        raise NotImplementedError

    def dl_coordinates(self, *args, **kwargs):
        raise NotImplementedError

    def num_gates(self, *args, **kwargs):
        raise NotImplementedError

    def elevation_degrees(self, *args, **kwargs):
        raise NotImplementedError

    def azimuth_degrees(self, *args, **kwargs):
        raise NotImplementedError

    def _construct_local_grid(self, *args, **kwargs):
        raise NotImplementedError

    def set_val(self, *args, **kwargs):
        raise NotImplementedError

    def add_val(self, *args, **kwargs):
        raise NotImplementedError

    def get_val(self, *args, **kwargs):
        raise NotImplementedError

    def set_values(self, *args, **kwargs):
        raise NotImplementedError

    def set_from_array(self, *args, **kwargs):
        raise NotImplementedError

    def get_np_array(self, *args, **kwargs):
        raise NotImplementedError

    def add_values(self, *args, **kwargs):
        raise NotImplementedError

    def get_index_from_local_coordinates(self, *args, **kwargs):
        raise NotImplementedError

    def index_to_local_coordinates(self, *args, **kwargs):
        raise NotImplementedError

    def construct_global_grid(self, *args, **kwargs):
        raise NotImplementedError

    def collect_measurements(self, *args, **kwargs):
        raise NotImplementedError
