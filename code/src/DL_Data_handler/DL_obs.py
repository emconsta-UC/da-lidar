#! /usr/bin/env python

import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import numpy as np
import re

import copy

from DLidarVec import StateVector as ObservationVector
from DLidarVec import Ensemble as ObservationEnsemble
from DL_obs_base import DL_OBS_BASE
from DL_constants import DL_NUM_GATES

import DL_utility as utility

class DL_SITE_OBS(DL_OBS_BASE):  # this is just an initial trial of creating an observation vector

    __supported_prog_vars = ['doppler', 'intensity', 'backscatter', 'radial_velocity']

    # This is hard-coded based on the application in-hand, however, they can be overridden by constructor arguments
    __elevation_degrees = [60, 90]
    __azimuth_degrees = [0, 45, 90, 135, 180, 225, 270, 315]
    __range_gate_length = 30
    __number_of_gates = DL_NUM_GATES
    # Measurement altitude (center of gate) = (range gate index + 0.5) * range gate length

    __site_facility = None  # name of site facility
    __dl_coordinats = None

    __dt = 1e-2  # time interval around (time_data) at which observations are collected
    time_date = None  # "YYYY:MM:DD:hh:mm:ss.*"

    def __init__(self, t, site_facility, dl_coordinates, prog_var, num_gates=None, range_gate_length=None, elevations=None, azimuthes=None):
        """
        A class for local observation vector containing DL data from a given fascility_site

        Here are the assumptions:
        1- Observations in an observation vector are taken within dt of a specific time instances
        2- Observations in an observation vector are collected from one site facility with specific coordinats
        3- Observations are stored at a given time instance as

        The observation vector is of dimension ???
        - for every elevation degree(other than 90) , all azimuth degrees are collected
        (VAD): number of azimuth degrees x number of elevation degrees other than 90 x number of gates
        + (Vert) number of gates

        Args:
            pass

        Returns:

        """
        self.__t = str(t)
        self.__site_facility = site_facility
        if isinstance(dl_coordinates, str):
            dl_coordinates = dl_coordinates.split(',')
        elif dl_coordinates is None:
            site, facility = site_facility.split('dl')
            [lon, lat], alt = utility.get_ARM_DL_coordinates(site=site, facility=facility, format='degrees')
            dl_coordinates = tuple([lon, lat, alt])
        elif utility.isiterable(dl_coordinates):
            assert len(dl_coordinates) == 3, "DL Coordinates must be a tuple/list of three entries corresponding to longitude, latitude, and altitude!"
        else:
            print("dl_coordinates can be a string with format 'X.X.X', a tuple/list with three entries or None.\n Receivec %s " % type(dl_coordinates))
            raise TypeError

        assert len(dl_coordinates) == 3, "DL Coordinates must be a tuple/list of three entries corresponding to longitude, latitude, and altitude!"
        dl_coordinates = tuple([float(c) for c in dl_coordinates])

        if dl_coordinates == (0, 0, 0):
            site, facility = site_facility.split('dl')
            [lon, lat], alt = utility.get_ARM_DL_coordinates(site=site, facility=facility, format='degrees')
            dl_coordinates = tuple([lon, lat, alt])

        self.__dl_coordinats = dl_coordinates


        assert isinstance(prog_var, str), "The prognostic variable 'prog_var', must be a string e.g. 'doppler' "
        self.__prog_var = prog_var.lower()

        if num_gates is not None:
            self.__number_of_gates = num_gates
        if range_gate_length is not None:
            self.__range_gate_length = range_gate_length

        if elevations is not None:
            self.__elevation_degrees = np.sort(np.array([e for e in elevations]).flatten(), axis=None).tolist()
        # self.__elevation_degrees = np.asarray(self.__elevation_degrees).flatten()
        if self.__elevation_degrees[-1] > 90:
            print("Elevation degrees can't exceed 90!")
            raise ValueError

        if azimuthes is not None:
            self.__azimuth_degrees = np.sort(np.array([a for a in azimuthes]).flatten(), axis=None).tolist()
            self.__azimuth_degrees = [a for a in azimuthes]
        # self.__azimuth_degrees = np.asarray(self.__azimuth_degrees).flatten()

        if 90 in self.__elevation_degrees:
            self.__obs_dimension = len(self.__azimuth_degrees) * self.__number_of_gates * (len(self.__elevation_degrees)-1)
            self.__obs_dimension += self.__number_of_gates
        else:
            self.__obs_dimension = len(self.__azimuth_degrees) * self.__number_of_gates * len(self.__elevation_degrees)

        #
        if re.match(r'\A(doppler|intensity|back(_|-)*scatter|radial(_|-)*velocity)\Z', self.__prog_var, re.IGNORECASE):
            self.__nvars = 1
        elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', self.__prog_var, re.IGNORECASE):
            self.__nvars = 3
        else:
            print("Unsupported prognostic variable %s" % self.__prog_var)
            raise ValueError
        self.__obs_dimension *= self.__nvars  # u, v, w are stored for each observation gridpoint

        self.data = ObservationVector(self.__obs_dimension)
        self.data[:] = np.NaN

        self.__local_coordinates = self._construct_local_grid()

    def observation_vector(self, **kwargs):
        y = ObservationVector(**kwargs)
        return y

    def number_of_valid_entries(self):
        """
        """
        return self.size - self.number_of_nans()

    def extract_valid_entries(self):
        """
        return coordinates, and corresponding observations for entries with available (non-missing)
            observations
        Args:
            None
        Returns:
            coordinates: list of tuples containing coordinates in the observational space with valid
                observations
            observations: list of tuples containing observations collected at corrsponding grid
                points stored in coordinates
        """
        nan_inds = self.where_nan()
        avail_inds = list(set(range(self.__obs_dimension)).difference(set(nan_inds)))
        avail_inds.sort()
        coordinates = []
        observations = []
        for i in range(0, len(avail_inds), self.__nvars):
            coord = self.index_to_local_coordinates(avail_inds[i])
            coordinates.append(coord)
            obs = []
            for d in range(self.__nvars):
                obs.append(self.data[avail_inds[i] + d])
            obs = tuple(obs)
            observations.append(obs)

        return coordinates, observations

    def set_val(self, val, elevation, azimuth, gate, mismatch_policy='nearest'):
        """
        Set val to the entry in the observation vector given elevation, azimuth, gate index
        gate index enumeration starts at 0

        val is either a scalar (e.g. doppler backscatter, radial velocity, etc), or a tuple containing (u,v,w) the three components of a wind file at a the given grid point
        """

        # get index of given coordinates (elevation, azimuth, gate):
        index = self.index_from_local_coordinates(elevation=elevation, azimuth=azimuth, gate=gate, mismatch_policy=mismatch_policy)
        if index is None:
            # pass
            print("Not set [coordinates not found]: val, elevation, azimuth, gate > ", val, elevation, azimuth, gate)
        else:
            print("Setting self.data[%d] = %f" %(index, val))

            #
            if re.match(r'\A(doppler|intensity|back(_|-)*scatter|radial(_|-)*velocity)\Z', self.__prog_var, re.IGNORECASE):
                self.data[index] = val
            elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', self.__prog_var, re.IGNORECASE):
                # convert doppler d, to wind field given elevation e, and azimuth a
                assert len(val) == 3, "Expected val to be an iterable of length 3, but Received %s" % repr(val)
                #
                for i in range(3):
                    self.data[index+i] = val[i]  # TODO: review

            elif False:  # add more
                pass
            else:
                print("Unsupported prognostic variable %s" % self.__prog_var)
                raise ValueError

        return ind

    def add(self, other, in_place=True):
        """
        Add the values of other to self (or a copy based on 'in_place')

        """
        assert isinstance(other, self.__class__), "other must be an instance of %s not %s!" % (self.__class__, type(other))
        assert self.size == other.size(), "Seriously! adding two observation instances of different dimensions!"
        # TODO: Add more assertion to guarantee equivalent settings!
        if in_place:
            result_obs = self
        else:
            result_obs = self.copy()
        result_obs.data.add(other.data, in_place=True)  # TODO: consider adding non-nan values only!
        return result_obs

    def add_val(self, val, elevation, azimuth, gate, mismatch_policy='nearest'):
        """
        Set val to the entry in the observation vector given elevation, azimuth, gate index
        gate index enumeration starts at 0
        """
        # get index of given coordinates (elevation, azimuth, gate):
        index = self.index_from_local_coordinates(elevation=elevation, azimuth=azimuth, gate=gate, mismatch_policy=mismatch_policy)
        if index is None:
            # pass
            print("Not set [coordinates not found]: val, elevation, azimuth, gate > ", val, elevation, azimuth, gate)
        else:
            #
            if re.match(r'\A(doppler|intensity|back(_|-)*scatter|radial(_|-)*velocity)\Z', self.__prog_var, re.IGNORECASE):
                if np.isnan(self.data[index]):
                    self.data[index] = val
                else:
                    self.data[index] += val

            elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', self.__prog_var, re.IGNORECASE):
                # convert doppler d, to wind field given elevation e, and azimuth a
                assert len(val) == 3, "Expected val to be an iterable of length 3, but Received %s" % repr(val)
                #
                for i in range(3):  # TODO: Review...
                    self.data[index+i] = val[i]

                    if np.isnan(self.data[index+i]):
                        self.data[index+i] = val[i]
                    else:
                        self.data[index+i] += val[i]

            elif False:  # add more
                pass
            else:
                print("Unsupported prognostic variable %s" % self.__prog_var)
                raise ValueError

        return index

    def get_val(self, elevation, azimuth, gate, mismatch_policy='nearest'):
        """
        Set val to the entry in the observation vector given elevation, azimuth, gate index
        gate index enumeration starts at 0
        """

        # get index of given coordinates (elevation, azimuth, gate):
        index = self.index_from_local_coordinates(elevation=elevation, azimuth=azimuth, gate=gate, mismatch_policy=mismatch_policy)
        if index is None:
            # pass
            print("Not set [coordinates not found]: elevation, azimuth, gate > ", elevation, azimuth, gate)
        else:

            #
            if re.match(r'\A(doppler|intensity|back(_|-)*scatter|radial(_|-)*velocity)\Z', self.__prog_var, re.IGNORECASE):
                val = self.data[index]
            elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', self.__prog_var, re.IGNORECASE):
                # convert doppler d, to wind field given elevation e, and azimuth a
                # val = tuple(self.data[index*3: index*3+3])
                # val = (self.data[index*3], self.data[index*3+1], self.data[index*3+2])
                val = self.data[index: index+3]

            elif False:  # add more
                pass
            else:
                print("Unsupported prognostic variable %s" % self.__prog_var)
                raise ValueError

        return val

    def set_values(self, values, elevations, azimuthes, gates=None, altitudes=None, mismatch_policy='nearest'):
        """
        iterate by calling set_val over enumeration of elevations, azimuthes, gates (no replication)
        """
        gates, altitudes, match = self._altitude_gate_validator(gates=gates, altitudes=altitudes)
        if not match:
            print("Gates and altitudes DONOT match!")
            raise ValueError
        else:
            pass

        missed_coordinates = []
        updated_indices = []
        for v, e, a, g in zip(values, elevations, azimuthes, gates):
            index = self.set_val(val=v, elevation=e, azimuth=a, gate=g, mismatch_policy=mismatch_policy)
            if index is None:
                missed_coordinates.append((e, a, g))
            else:
                updated_indices.append(index)
        if len(missed_coordinates)>0:
            print("Some coordinates wern't found in the observation vector: ", missed_coordinates)
        return missed_coordinates, updated_indices

    def set_from_array(self, x):
        """
        set values of self.data from the values of an iterable x the same size as self.data
        """
        _x =  np.asarray(x).squeeze()
        obs_size = self.size
        if _x.size != obs_size:
            print("Passed array must be of the same size as the observation vector of dimension %d", obs_size)
            raise ValueError
        else:
            self.data[:] = _x[:]

    def get_np_array(self):
        """
        return a (copy) of self.data as a 1D numpy array
        """
        return self.data.get_np_array()
    get_numpy_array = get_np_array


    def map_to_coordinates(self, coordinates, coordinate_system='spherical', mismatch_policy='nearest', return_array=False):
        """
        project self.data onto the passed coordinates
        Args:
            coordinates: 2D numpy array, with each row representing the 3components of coordinates; 
                the coordinates are interpreted to be cartesian or spherical, based on 'coordinate_system
            coordinate_system: 'cartesian' or 'spherical'
            mismatch_policy: 'nearest' or 'exact'

        Returns:
            projected_data: a list of tuples, with each tuple containing the values of prognostic variables at passed coordiantes, np.NaN is used for missing data

        """
        coordinates_np = np.asarray(coordinates)
        if not (coordinates_np.ndim ==2 and np.size(coordinates_np, 1)==3):
            print("coordinates must be a two dimensional array with 3columns")

        if re.match(r'\Acart(es|ez)*(ian)*\Z', coordinate_system, re.IGNORECASE):
            # Convert to spherical coordinates coordinates_np --> spherical
            print("Not Yet Implemented: TODO")
            raise NotImplementedError
        elif re.match(r'\Aspher(e|i)(cal)*\Z', coordinate_system, re.IGNORECASE):
            pass
        else:
            print("Only 'spherical' or 'cartesian' are accepted as coordiantes system! Received %s" % coordinate_system)
            raise ValueError
        #
        nvars = self.__nvars
        mapped_observations = []
        if re.match(r'\Aexact\Z', mismatch_policy, re.IGNORECASE):
            for i in range(np.size(coordinates_np, 0)):
                coord = coordinates_np[i, :]
                match_inds = self.find_matching_indexes([coordinates_np[i, 0]], [coordinates_np[i, 1]], gates=None, altitudes=[coordinates_np[i, 2]])
                obs = []
                if len(match_inds)==0:
                    obs = tuple([np.NaN]*self.__nvars)
                elif len(match_inds)==1:
                    obs = []
                    for ind in match_inds[0]:
                        obs.append(self.data[ind])
                else:
                    print("This shouldn't happen; more than one matching tuple for unique coordinates! Logical-Bug-fix is required!")
                    raise ValueError
                mapped_observations.append(tuple(obs))
            if return_array:
                mapped_observations = np.asarray(mapped_observations)
                #
        elif re.match(r'\Anearest\Z', mismatch_policy, re.IGNORECASE):
            raise NotImplementedError("TODO: WIP")
            for i in range(np.size(coordinate_np, 0)):
                coord = coordinates_np[i, :]
                match_inds = self.index_from_local_coordinates(coordinates_np[i, 0], azimuth=coordinates_np[i, 1], altitude=coordinates_np[i, 2], mismatch_policy=mismatch_policy)
                # TODO: to be implemented while DA is running...
                #
        else:
            print("Unsupported mismatch policy: %s" % mismatch_policy)
            raise ValueError

        return mapped_observations
    
    
    def doppler_to_radial_velocity(self, doppler):
        """convert doppler shift to radial velocity"""
        rv = - 1.5 / (4 * np.pi) * doppler
        return rv

    def radial_velocity_to_doppler(self, radial_velocity):
        """convert radial velocity to doppler shift"""
        doppler = - (4 * np.pi) / 1.5 * radial_velocity
        return doppler

    def doppler_to_velocity_field(self, doppler, elevation, azimuth, correct_with_nan=True):
        """
        Calculate the wind-velocity  field components, from doppler shift, elevation and azimuth
        if correct_with_nan is True, values along u-v-w axis directions are corrected properly replaced with NaN
        """
        if not utility.isiterable(doppler):
            doppler = np.array([doppler])
        radial_velocity = self.doppler_to_radial_velocity(np.asarray(doppler))
        values = [(0, 0, 0)] * doppler.size()
        alpha = np.radians(elevation)
        beta = np.radians(azimuth)
        for i, rv in enumerate(radial_velocity):
            #
            u = rv * np.cos(alpha) * np.cos(beta)
            v = rv * np.cos(alpha) * np.sin(beta)
            w = rv * np.sin(alpha)
            #
            if correct_with_nan:
                if elevation == 90:
                    u = v = np.NaN
                else:
                    if elevation == 0:
                        w = np.NaN
                    if azimuth in [0, 180, 360]:
                        v = np.NaN
                    if azimuth in [90, 270]:
                        u = np.NaN
            #
            values[i][:] = u, v, w
        #
        if len(values) == 1:
            values = values[0]
        #
        return values


    def add_values(self, values, elevations, azimuthes, gates=None, altitudes=None, mismatch_policy='nearest'):
        """
        iterate by calling add_val over enumeration of elevations, azimuthes, gates (no replication)
        """
        gates, altitudes, match = self._altitude_gate_validator(gates=gates, altitudes=altitudes)
        if not match:
            print("Gates and altitudes DONOT match!")
            raise ValueError
        else:
            pass

        missed_coordinates = []
        updated_indices = []
        for v, e, a, g in zip(values, elevations, azimuthes, gates):
            index = self.add_val(val=v, elevation=e, azimuth=a, gate=g, mismatch_policy=mismatch_policy)
            if index is None:
                missed_coordinates.append((e, a, g))
            else:
                updated_indices.append(index)
        if len(missed_coordinates)>0:
            print("Some coordinates wern't found in the observation vector: ", missed_coordinates)
        return missed_coordinates, updated_indices

    def extract_entries(self, elevations=None, azimuths=None, gates=None, altitudes=None, use_def_for_none=True):
        """
        Return a numpy array containing entries of the observation vector satysfying all passed criteria

        Args:
            elevations:
            azimuths:
            gates:
            altitudes:

        Returns:
            indexes

        """
        if use_def_for_none:
            if elevations is None:
                elevations=self.__elevation_degrees
            if azimuths is None:
                azimuths = self.__azimuth_degrees
            if gates is altitudes is None:
                gates = np.arange(self.__number_of_gates).tolist()

        # TODO: This should be refactored to return a pair of coordinates with correspo
        matching_inds = self.find_matching_indexes(elevations=elevations, azimuths=azimuths, gates=gates, altitudes=altitudes, return_array=True)

        if False:
            print("Inside extract_entries, looking for :")
            print("Elevations: ", elevations)
            print("Azimuths: ", azimuths)
            print("Gates: ", gates)
            print("Altitudes: ", altitudes)
            print("Matching Indexes found: [out of obs size %d ]" % self.size)
            print(matching_inds)

        coordinates = []
        observations = []
        for i in range(0, matching_inds.shape[0]):
            coord = self.index_to_local_coordinates(matching_inds[i, 0])    
            coordinates.append(coord)
            obs = []
            for c_i in matching_inds[i,:]:
                obs.append(self.data[c_i])
            observations.append(tuple(obs))
        return coordinates, observations

    def get_prognostic_variables_indexes(self):
        """
        return the indexes in the model state corresponding to prognostic variables respectively

        """
        indexes = []
        for prog_ind in range(self.__nvars):
            inds = np.arange(prog_ind, self.size, self.__nvars)
            indexes.append(inds)
        #
        return indexes

    def get_global_cartesian_grid(self, dl_coordinates=None):
        """
        Given dl_coordinates (cartesian [x, y, z]), shift the local cartezian grid to make the

        Args:
            dl_coordinates: iterable of length 3 with cartesian coordinates of the doppler-lidar instrument.
                The local grid will be shifted by shifting the current dl_coordinates to the passed one.

        Returns:
            global_grid: cartesian grid, with doppler lider instrument set to the passed dl_coordinates

        """
        try:
            x0, y0, z0 = dl_coordinates
            coordinate_center = (x0, y0, z0)
        except(TypeError, IndexError):
            print("dl_coordinates must be an iterable with 3 components (x0, y0, z0)")
            raise AssertionError
        global_grid = self._get_local_grid_cartesian()
        for i in range(np.size(global_grid, 0)):
            global_grid[i, :] += coordinate_center
        return global_grid
    # add alias(es)
    get_shifted_coordinates = get_global_cartesian_grid

    def collect_measurements(self):
        """ """
        raise NotImplementedError

    def fill_nan_with(self, val, in_place=True):
        """
        replace any occurances of np.NaN with the passed val

        Args:
            val: value to replace np.NaN

        Return:
            self

        """
        if not in_place:
            result_vec = self.copy()
        else:
            result_vec = self

        if val == 0:
            result_vec.data[:] = np.nan_to_num(result_vec.data)
        else:
            result_vec.data[np.argwhere(np.isnan(result_vec.data))] = val
        return result_vec

    def where_nan(self):
        """
        return a list of indices corresponding to nan values

        Args:

        Return:
            indexes: list of indexes corresponding to nan values in self.data

        """
        indexes = np.argwhere(np.isnan(self.data)).flatten().tolist()
        return indexes

    def number_of_nans(self):
        """
        """
        return np.count_nonzero(np.isnan(self.data))

    def __str__(self):
        """
        return a string representation of self; coordinates, and data
        """
        sep = "\n" + "*"*30 + "\n"
        _str = sep + self.__repr__() + sep

        max_print_lines = 100
        max_dim = self.__obs_dimension // self.__nvars
        # remember that all observation at a specific grid point are consequtive in memory
        if max_dim > max_print_lines:
            get_inds = range(max_print_lines//2)
            for i in get_inds:
                coord = self.index_to_local_coordinates(i*self.__nvars)
                obs = []
                for d in range(self.__nvars):
                    obs.append(self.data[i*self.__nvars + d])
                obs = tuple(obs)
                line_repr = "Coordinates:[elevation=%5.2f; azimuth=%5.2f, altitude=%9.2f]" % (coord[0], coord[1], coord[2])
                line_repr += "\tObservation:[%s: %s] " % (self.__prog_var, str(obs))
                _str += line_repr + "\n"
            _str += "...\n...\n...\n"
            get_inds = range(-max_print_lines//2, 0)
            for i in get_inds:
                coord = self.index_to_local_coordinates(i*self.__nvars)

                obs = []
                for d in range(self.__nvars):
                    obs.append(self.data[i*self.__nvars + d])
                obs = tuple(obs)
                line_repr = "Coordinates:[elevation=%5.2f; azimuth=%5.2f, altitude=%9.2f]" % (coord[0], coord[1], coord[2])
                line_repr += "\tObservation:[%s: %s] " % (self.__prog_var, str(obs))
                _str += line_repr + "\n"
            _str += sep
        else:
            get_inds = range(max_dim)
            for i in get_inds:
                coord = self.index_to_local_coordinates(i*self.__nvars)
                obs = []
                for d in range(self.__nvars):
                    obs.append(self.data[i*self.__nvars + d])
                obs = tuple(obs)
                line_repr = "Coordinates:[elevation=%5.2f; azimuth=%5.2f, altitude=%9.2f]" % (coord[0], coord[1], coord[2])
                line_repr += "\tObservation:[%s: %s] " % (self.__prog_var, str(obs))
                _str += line_repr + "\n"
            _str += sep

        return _str

    def copy(self):
        """
        return a copy of self
        (t, site_facility, dl_coordinates, prog_var, num_gates=None, range_gate_length=None, elevations=None, azimuthes=None)
        """
        obs_copy = DL_SITE_OBS(t=self.__t,
                               site_facility=self.__site_facility,
                               dl_coordinates=self.__dl_coordinats,
                               prog_var=self.__prog_var,
                               num_gates=self.__number_of_gates,
                               range_gate_length=self.__range_gate_length,
                               elevations=self.__elevation_degrees,
                               azimuthes=self.__azimuth_degrees)
        obs_copy.data[:] = self.data[:]
        return obs_copy

    def copy_observation_configs(self, key=None):
        """
        """
        observation_configs = {'t':self.__t,
                               'nvars':self.__nvars,
                               'site_facility':self.__site_facility,
                               'dl_coordinates':self.__dl_coordinats,
                               'prog_var':self.__prog_var,
                               'num_gates':self.__number_of_gates,
                               'range_gate_length':self.__range_gate_length,
                               'elevations':self.__elevation_degrees,
                               'azimuthes':self.__azimuth_degrees,
                               'obs_dimension':self.__obs_dimension,
                               }
        return observation_configs

    #
    # Grid and coordinate information
    # -----------------------------------------------------------
    def _construct_local_grid(self):
        # local_grid (3 rows x observation dimension); the rows are elevation, azimuth, gate index)
        local_grid = np.empty((3, self.__obs_dimension//self.__nvars))
        #
        ind = 0
        for e in self.__elevation_degrees:
            if e == 90:
                local_grid[0, ind: ind+self.__number_of_gates] = e
                local_grid[1, ind: ind+self.__number_of_gates] = e  # for elevation 90, azimuth is fixed to 90 following DL-data convention
                local_grid[2, ind: ind+self.__number_of_gates] = range(self.__number_of_gates)
                #
                ind += self.__number_of_gates

            else:
                for a in self.__azimuth_degrees:
                    local_grid[0, ind: ind+self.__number_of_gates] = e
                    local_grid[1, ind: ind+self.__number_of_gates] = a
                    local_grid[2, ind: ind+self.__number_of_gates] = range(self.__number_of_gates)
                    #
                    ind += self.__number_of_gates

        return local_grid

    def find_matching_indexes(self, elevations=None, azimuths=None, gates=None, altitudes=None, return_array=False):
        """
        return a list (or a numpy array) of matching indexes in the observation vector

        Args:
            elevations:
            azimuths:
            gates:
            altitudes:

        Returns:
            indexes

        """
        # I will use gate index if exists
        if elevations is azimuths is gates is altitudes is None:
            print("Common! Are you serious! all is None?! This will return an empty list !")
            indexes = []
        else:
            if gates is altitudes is None:
                gates = []
            else:
                gates, altitudes, match = self._altitude_gate_validator(gates=gates, altitudes=altitudes)
                if not match:
                    print("Gates and altitudes DONOT match!")
                    raise ValueError
                else:
                    pass

            # Get indexes (w.r.t local coordinates) satisfying all criteria the rows are elevation, azimuth, gate index
            indexes = set(range(self.observation_size()))

            if elevations is not None:
                e_match =[]
                for e in elevations:
                    e_match += np.where(np.isclose(self.__local_coordinates[0, :], e))[0].tolist()
                indexes = indexes.intersection(set(e_match))

            if azimuths is not None:
                a_match = []
                for a in azimuths:
                    a_match += np.where(np.isclose(self.__local_coordinates[1, :], a))[0].tolist()
                indexes = indexes.intersection(set(a_match))

            if gates is not None:
                g_match = []
                for g in gates:
                    g_match += np.where(np.isclose(self.__local_coordinates[2, :], g))[0].tolist()
                indexes = indexes.intersection(set(g_match))

            indexes = list(indexes)
            indexes.sort()

        # Convert to indexes in the state vector
        indexes = [i*self.__nvars for i in indexes]
        out_indexes = []
        for index in indexes:
            out_indexes.append(tuple([index+i for i in range(self.__nvars)]))

        if return_array:
            out_indexes = np.asarray(out_indexes)

        return out_indexes

    def index_from_local_coordinates(self, elevation, azimuth, gate=None, altitude=None, mismatch_policy='nearest'):
        """
        given local (elevation, azimuth, gate ind), get the (first) matching index in the observation vector;
        return None if no match exists

        """
        if altitude is None and gate is None:
            #
            print("Either set the altitude or the gate index to retrieve observation at! Both can't be None!")
            raise ValueError
        elif altitude is not None and gate is not None:
            #
            calc_altitude = self._gate_index_to_altitude(gate)
            if altitude == calc_altitude:
                pass
            else:
                print("The values set for gate index (gate) and altitude DON'T match! ")
                print("Passed gate index: %d" % int(gate))
                print("Passed altitude: %f" % altitude)
                print("Calculated altitude based on range gate length of (%f) is: %f" % (self.__range_gate_length, calc_altitude))
                raise ValueError
        elif gate is not None:
            #
            if gate >= self.__number_of_gates:
                print("Gate index must lie between [0, %d)" % self.__number_of_gates)
                raise ValueError
            else:
                altitude = self._gate_index_to_altitude(gate)
        elif altitude is not None:
            gate = self._altitude_to_gate_index(altitude)
        else:
            print("This is impossible to reach!")
            raise ValueError

        #
        e = elevation
        a = azimuth
        #
        if mismatch_policy is None:
            if not (e in self.__elevation_degrees and a in self.__azimuth_degrees):
                index = None
        elif re.match(r'\A(near|close)(est)*\Z', mismatch_policy, re.IGNORECASE):
            if e not in self.__elevation_degrees:
                e = min(self.__elevation_degrees, key=lambda x:abs(x-e))
            if a not in self.__azimuth_degrees:
                a = min(self.__azimuth_degrees, key=lambda x:abs(x-a))
            #
            e_ind = self.__elevation_degrees.index(e)
            a_ind = self.__azimuth_degrees.index(a)

        else:
            print("Unsupported mismatch policy: %s" % mismatch_policy)
            raise ValueError

        if e == 90.0:
            index = gate - self.__number_of_gates  # backward counting
            index = self.__obs_dimension // self.__nvars + index
        else:
            # index = gate + (a_ind * self.__number_of_gates) + (e_ind * len(self.__azimuth_degrees) * self.__number_of_gates)
            index = gate + ((a_ind  + e_ind * len(self.__azimuth_degrees)) * self.__number_of_gates)

        #
        if index is not None:
            index *= self.__nvars

        if index >= self.__obs_dimension:
            print("Coordinates %s >> index in observation vector = %d; self.__obs_dimension=%d" %(repr([elevation, azimuth, gate]), index, self.__obs_dimension))
            #
        return index

    def index_to_local_coordinates(self, index):
        """
        Reverse index_from_local_coordinates;
        if you want gate index, use self._altitude_to_gate_index applied to the last entry of the output here

        Args:
            index

        Returns:
            coordinates: (elevation, azimuth, altitude)
        """
        assert int(index)==index, "passed index must be an integer! not %s" %type(index)
        if -self.size<=index <0:
            index += self.size
        elif 0<= index <self.size:
            pass
        else:
            print("Index %d out of range, with dimensions size %d !" %(index, self.size))
            raise IndexError
        index = int(index/self.__nvars)  # handles multiple prognostic variables

        gate = index % self.__number_of_gates
        ind_gate_diff = float(index - gate ) / self.__number_of_gates

        e_ind = int(ind_gate_diff / len(self.__azimuth_degrees))
        # a_ind = int(ind_gate_diff % len(self.__azimuth_degrees))

        elevation = self.__elevation_degrees[e_ind]
        if elevation == 90:
            azimuth = 90
        else:
            a_ind = int(ind_gate_diff - (e_ind * len(self.__azimuth_degrees)))
            azimuth = self.__azimuth_degrees[a_ind]
        altitude = self._gate_index_to_altitude(gate)

        return (elevation, azimuth, altitude)

    def gridded_observations_to_numpy(self, obs=None):
        """
        Get observations at each grid pioint depending on the observed variable
        Args:
            obs: the observation vector; if None, self.data is used

        Returns:
            (x, y, z): the three components of the cartesian coordinates (for eeach grid point)
            observations: A list of iterables, each gives the value of an observed variable (depending on the observed prognostic variable).
                for example if u, v, w are observed, observations = [u, v, w]

        Remarks:
            ith entry of (x, y, z) is the grid at with the ith entry of each of observation's entry is/are given

        """
        if obs is None:
            obs = self.data
        else:
            assert isinstance(obs, (np.ndarray, ObservationVector)), "obs must be either a numpy array or instance of DL_vec.StateVector"
            if obs.size != self.size:
                print("The passed observation vector has improper size!")
                print("Expected: %d" % self.size)
                print("Received: %d" % obs.size)
                raise ValueError

        local_grid = self._get_local_grid_cartesian()
        indexes = self.get_prognostic_variables_indexes()
        if len(indexes) != self.__nvars:
            print("Returned indexes length do not match number of prognostic variables!")
            raise ValueError
        #
        x = np.tile(local_grid[:, 0], self.__nvars)
        y = np.tile(local_grid[:, 1], self.__nvars)
        z = np.tile(local_grid[:, 2], self.__nvars)
        #
        observations = []
        for index in indexes:
            data= obs[index]
            observations.append(data)
        return (x, y, z), observations

    def _get_local_grid_spherical(self, gate_index=True):
        """
        local_grid (observation dimension x 3 columns); the columns are elevation, azimuth, gate index)
        if gate_index is True return local grid containing coordinates (elevation, azimuth, gate index)
        if gate_index is False return local grid containing coordinates (elevation, azimuth, altitude)
        """
        try:
            local_grid = self.__local_coordinates.copy()
        except(AttributeError):
            local_grid = None

        if local_grid is None:
            local_grid = self._construct_local_grid().copy()

        if not gate_index:
            # convert gate indices to meters
            local_grid[-1, :] = self._gate_index_to_altitude(local_grid[-1, :])
            pass
        local_grid = local_grid.T
        return local_grid

    def _get_local_grid_cartesian(self):
        """
        Return the spherical coordinates of each entry of the observation vector, using ISO format
        i.e., (radial distance, inclination, azimuth)
            The inclination falls in [0, 90], and the azimuth is in [0, 360]

        """
        # get spherical grid
        sph_grid = self._get_local_grid_spherical(gate_index=False)
        rho = sph_grid[:, 2]  # radial distance (radius)
        theta = np.radians(90 - sph_grid[:, 0])   # inclination from elevation
        phi = np.radians(sph_grid[:, 1])  # azimuth
        # print(rho, theta, phi)
        # convert spherical to cartesian coordinates:
        cart_grid = np.empty_like(sph_grid)
        cart_grid[:, 0] = rho * np.sin(theta) * np.cos(phi)   # x = radius sin(inclination) cos(azimuth)
        cart_grid[:, 1] = rho * np.sin(theta) * np.sin(phi)   # y = radius sin(inclination) sin(azimuth)
        cart_grid[:, 2] = rho * np.cos(theta)                 # z = radius cos(inclination)
        # print("cart_grid[:, 0]: ", cart_grid[:, 0])
        # print("cart_grid[:, 1]: ", cart_grid[:, 1])
        # print("cart_grid[:, 2]: ", cart_grid[:, 2])
        #
        return cart_grid
        #

    def get_local_grid(self, coordinates='spherical'):
        """
        """
        if re.match(r'\Aspher(e|i)(cal)*\Z', coordinates, re.IGNORECASE):
            grid = self._get_local_grid_spherical(gate_index=False)
        elif re.match(r'\Acart(es|ez)*(ian)*\Z', coordinates, re.IGNORECASE):
            grid = self._get_local_grid_cartesian()
        else:
            print("Unsupported coordinate system [%s]!" % coordinates)
            raise ValueError
        return grid
    # a useful alias
    get_local_coordinates = get_local_grid

    def _altitude_to_gate_index(self, altitude):
        """ """
        index = int( altitude / self.__range_gate_length - 0.5 )
        return index

    def _gate_index_to_altitude(self, index):
        """ altitude is actually the radial distance """
        altitude = (index + 0.5) * self.__range_gate_length
        return altitude

    def _altitude_gate_validator(self, gates=None, altitudes=None):
        """
        """
        if altitudes is None and gates is None:
            #
            print("Either pass gates or altitudes!")
            raise ValueError
        elif altitudes is not None and gates is not None:
            #
            _gates = []
            match = True
            for i, altitude in enumerate(altitudes):
                if gates[i] != self._altitude_to_gate_index(altitude):
                    # print("The gates and altitudes DONOT match!")
                    match = False
                    break
        elif gates is not None:
            #
            # altitudes = [self._gate_index_to_altitude(g) for g in gates]
            altitudes = []
            for gate in gates:
                if gate >= self.__number_of_gates:
                    print("Gate index must lie between [0, %d)" % self.__number_of_gates)
                    print("Some gate indexes are out of range!")
                    raise ValueError
                else:
                    altitudes.append(self._gate_index_to_altitude(gate))
            match = True
        elif altitudes is not None:
            gates = [self._altitude_to_gate_index(altitude) for altitude in altitudes]
            # gates = []
            # for altitude in altitudes:
            #     gates.append(self._altitude_to_gate_index(altitude))
            match = True
        else:
            print("This is impossible to reach!")
            raise ValueError
        return gates, altitudes, match

    #
    # Properties of the observation grid, and observation vector
    # -----------------------------------------------------------
    def obs_dimension(self):
        return self.__obs_dimension
    observation_size = obs_dimension

    @property
    def size(self):
        return self.obs_dimension()
    @size.setter
    def size(self, value):
        print("Can't upated dimension of the observation vector after creation with a give grid. You must create a new object, or extrace elements at specific grid points")
        raise NotImplementedError

    def obs_time(self):
        return self.__t

    @property
    def time(self):
        return self.obs_time()
    @time.setter
    def time(self, value):
        print("WARNING: You are updating time only; this won't updated the data")
        self.__t = str(value)

    def site_facility(self):
        return self.__site_facility

    @property
    def dl_coordinates(self):
        return self.__dl_coordinats

    @property
    def num_gates(self):
        return self.__num_gates
    num_of_gates = num_gates

    @property
    def elevation_degrees(self):
        return self.__elevation_degrees

    @property
    def azimuth_degrees(self):
        return self.__azimuth_degrees

    @property
    def range_gate_length(self):
        return self.__range_gate_length

    @property
    def range_gates(self):
        return range(self.__number_of_gates)

    @property
    def prognostic_variable(self):
        return self.__prog_var

if __name__ == '__main__':
    pass
