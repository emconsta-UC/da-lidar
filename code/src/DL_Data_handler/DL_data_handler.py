#! /usr/bin/env python

"""
DL-LIDAR data inversion project;

This module contains the following classes:
  1- DL_DATA_HANDLER:
  -------------------
  Provides functionalities to read/validate/parse data locally or remotely;


"""
# TODO: After initial design, move static methods to a utility module
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

import os

# ----------------- This block will be updated once we have a final packge ----------------- #
sys.path.append("../")
sys.path.append("../Utility/")
import DL_utility as utility
import DL_constants
from  DL_obs import DL_SITE_OBS  # TODO: update import after rearrangement of classes
# ------------------------------------------------------------------------------------------ #

import socket
import getpass
from stat import S_ISDIR

from datetime import datetime, timedelta

import itertools
import re
import warnings

import tarfile
import shutil

import h5py
import numpy as np

try:  # TODO: Maybe we should make the COMM part of the configurations dictionary!
    from mpi4py import MPI
    use_MPI = True
except(ImportError):
    use_MPI = False
try:
    import paramiko  # this should be just an option (naive SSH, or copying should be an option)
    use_paramiko = True
except(ImportError):
    use_paramiko = False


class DL_DATA_HANDLER(object):

    # attributes/fields expected to be available in a data file
    __supported_DL_fields = ['intensity', 'doppler', 'backscatter', 'time', 'pitch', 'roll', 'range_gate_length', 'radial_velocity', 'wind_velocity_field']  # keep everything lowercase
    __supported_DL_attributes = ['num_gates', 'range_gate_length', 'base_time', '']  # TODO: update based on the raw data header contents
    __supported_DL_raw_formats = ['hpl']  # without dot
    __supported_DL_parsed_formats = ['hdf5', 'h5']  # without dot


    def __init__(self, verbose=False):
        """
        Provides functionalities to read/validate/parse data locally or remotely;

        Args:
            verbose: verbosity of the object True/False to print stuff to screen

        Returns:
            None

        """
        if use_MPI:
            MPI_COMM = MPI.COMM_WORLD
            my_rank = MPI_COMM.Get_rank()
            comm_size = MPI_COMM.Get_size()
        else:
            MPI_COMM = None
            my_rank = 0
            comm_size = 1

        #
        # TODO: Based on information in the constants script/file, initialize the data handler
        #   This object should be aware of the following:
        #       a) Name of the host (server) on which raw data reside, and parsed data are maintained
        #       b) Username to SSH to upon accessing data directory on the remote host
        #       c) Whether this machine is the server on which raw and parsed data are kept and maintained or just a client
        #       d) whether parmiko is available for use or not (mmmm, maybe not!)
        #       e)
        #
        self._verbose = verbose

        # # Retrieve information from DL_constants
        # DL_DATA_HOST_NAME = "login"  # --> directs to login1, login2, etc.
        # DL_DATA_HOST_ADDRESS = "mcs.anl.gov"
        # DL_DATA_REPO_ROOT_PATH = "/nfs/proj-emconsta/DL_Data"  # this is the original repository, to be read from,
        # DL_DATA_RAW_RELATIVE_PATH = "raw"
        # DL_DATA_PARSE_RELATIVE_PATH = "parsed"
        #
        # DL_DATA_CATEGORIZATION = 'year/month/site-name/scan-type/'  # How data files are coategorized inside the raw or parsed directories

        # Get host info, and directory info...
        self.__host_name = socket.gethostname()
        self.__host_byaddress_info = socket.gethostbyname_ex(self.__host_name)  # (true host name, list of aliases, host name or IP address)
        self.__host_address = self.__host_byaddress_info[0][len(self.__host_name)+1: ]
        # print("This code is running on host with: name, address, address info:", self.__host_name, self.__host_byaddress_info, self.__host_address)

        self.__I_AM_HOST = True if (DL_constants.DL_DATA_HOST_ADDRESS == self.__host_address) else False
        # print(">>>>> HOST? " , self.__I_AM_HOST, DL_constants.DL_DATA_HOST_ADDRESS, self.__host_address)
        if self.__I_AM_HOST:
            # This is *.mcs.anl.gov
            if my_rank == 0 and self._verbose:
                print("Cool, I am %s\n I will proceed..." % self.__host_address)
            # Eveything is local; get directory settings from DL_constants
            if my_rank == 0:
                if not os.path.isdir(DL_constants.DL_DATA_REPO_ROOT_PATH):
                    print("The path to DL-Data directory '%s' DOES NOT EXIST!" % DL_constants.DL_DATA_REPO_ROOT_PATH)
                    msg = "Do you wish to create this directory for future data collection?"
                    create_dir = utility.query_yes_no(msg, default="no")
                    if create_dir:
                        os.makedirs(DL_constants.DL_DATA_REPO_ROOT_PATH)
                    else:
                        print("Sorry, can't work on the DATA-REPO MAIN-HOST, without a local data directory!")
                        raise IOError
	    
            self.__dl_data_repo_root_path = DL_constants.DL_DATA_REPO_ROOT_PATH
            # print(">>>>>>>>>>>>>> self.__dl_data_repo_root_path, ", self.__dl_data_repo_root_path)
            if MPI_COMM is not None and comm_size > 1:
                MPI_COMM.Barrier()
                if not os.path.isdir(DL_constants.DL_DATA_REPO_ROOT_PATH) and my_rank != 0:
                    print("This is process [%d]; I can't find DL-Data directory! You need to respond to Root node request to create data directory!")
                    raise IOError

            self.__local_raw_data_path = os.path.join(self.__dl_data_repo_root_path, DL_constants.DL_DATA_RAW_RELATIVE_PATH)
            self.__incoming_data_root_path = DL_constants.DL_DATA_INCOMING_ROOT_PATH
            #
            self.__remote_server_info = None
            #
        else:
            # This is a client side (e.g. a laptop):
            if my_rank == 0 and self._verbose:
                sep = '\n' + '*'*80 + '\n'
                print("%sI am: '%s'@'%s'; I will proceed ... %s" % (sep,self.__host_name, self.__host_address, sep))

            # To keep a clean workflow, parsing is only done on the machine where raw data is
            local_data_path = DL_constants.DL_DATA_LOCAL_PATH
            if local_data_path is None:
                self.__dl_data_repo_root_path = os.path.join(DL_constants.DL_ROOT_PATH, Dl.constants.DL_DATA_LOCAL_RELATIVE_PATH)
            else:
                self.__dl_data_repo_root_path = local_data_path
            self.__local_raw_data_path = os.path.join(self.__dl_data_repo_root_path, DL_constants.DL_DATA_RAW_RELATIVE_PATH)  # TODO: change to None, after finishing client side
            self.__incoming_data_root_path = None
                #
            # remote data
            DL_DATA_HOST_ADDRESS = "mcs.anl.gov"
            DL_DATA_HOST_NAME = "login"  # --> directs to login1, login2, etc.
            DL_DATA_REPO_ROOT_PATH = "/nfs/proj-emconsta/DL-Data"  # this is the original repository, to be read from,
            DL_DATA_INCOMING_ROOT_PATH = "/nfs/proj-emconsta/DL-Data/Incoming"  # this is where new tar'd files exist

            self.__remote_server_info = dict(
                                         host_name = DL_constants.DL_DATA_HOST_NAME,  # --> directs to login1, login2, etc.
                                         host_address = DL_constants.DL_DATA_HOST_ADDRESS,
                                         dl_data_repo_root_path = DL_constants.DL_DATA_REPO_ROOT_PATH,
                                         dl_data_raw_relative_path = DL_constants.DL_DATA_RAW_RELATIVE_PATH,
                                         dl_data_parse_relative_path = DL_constants.DL_DATA_PARSED_RELATIVE_PATH)

        # Following are common
        self.__data_directories_categorization = DL_constants.DL_DATA_CATEGORIZATION
        self.__local_parsed_data_path = os.path.join(self.__dl_data_repo_root_path, DL_constants.DL_DATA_PARSED_RELATIVE_PATH)
        #

        # Checking availability of local data repositories
        if my_rank == 0:
            if not os.path.isdir(self.__dl_data_repo_root_path):
                create_dir = utility.query_yes_no("Do you wish to create __dl_data_repo_root_path: [%s] for future data collection?" % self.__dl_data_repo_root_path, default="yes")
                if create_dir:
                    os.makedirs(self.__dl_data_repo_root_path)
            #
            if not os.path.isdir(self.__local_parsed_data_path):
                create_dir = utility.query_yes_no("Do you wish to create __local_parsed_data_path [%s]  for future data collection?" % self.__local_parsed_data_path, default="yes")
                if create_dir:
                    os.makedirs(self.__local_parsed_data_path)
            #
            if not os.path.isdir(self.__local_raw_data_path):
                create_dir = utility.query_yes_no("Do you wish to create __local_raw_data_path [%s] for future data collection?" % self.__local_raw_data_path, default="yes")
                if create_dir:
                    os.makedirs(self.__local_raw_data_path)

        if MPI_COMM is not None and comm_size > 1:
            MPI_COMM.Barrier()  # wait for all processes to get here


    def process_incoming_raw_data(self, incoming_path=None, recursive=False, parse=True, keep_incoming=True, keep_extracted_raw=True, overwrite_raw=False, raw_format='hpl', parse_format='hdf', overwrite_parsed=False, extract_dir_warn=True, ignore_marked_done=True):  # file_extension may be ignored!
        """
        If a file (e.g. tar file) is given, it will be extracted in a temporary directory,
            and the files will be moved to the right place
        If file is None, all valid files (based on file_extension), under DL_constants will be
            extracted, and distributed to it's designated destination under the raw data directory;
            directories (and subdirectories) based on the categorization
            (in DL_constants.DL_DATA_CATEGORIZATION) will be created if don't exist

        Args:
            incoming_path: path of incoming data. This could be a path to a (e.g. tar) file, or a directory
                if it's a directory, all files in it will be processed (this could easily be made recusrive, but it is NOT...)
            parse: if True, extracted files will be processed upon extraction
            keep_incoming: if False, the incoming raw files/directories will be removed (BECAREFULE with this)
            keep_extracted_raw: This has nothing to-do with incoming_path; if False, distributed files to the extracted directory are kept
            raw_format: only files with raw_format extension will be parsed (if None, everything extracted will be passed to the parser)
            parse_formt:
            extract_dir_warn: Warning if the extraction directory already exists

        Returns:
            distributed_files_list: a list containing full paths of files successfully distributed (will be empty list if keep_extracted_raw is False)to distination in the raw data directory
            parsed_files_list: a list containing full paths of files successfully parsed (will be empty if parse is False) to distination in the parsed data directory


        """
        if not self.__I_AM_HOST:
            msg = "This function MUST run only on the machine where raw data is stored!\n"
            print(msg)
            msg = "Do you want to contact '%s' to pares incoming data files in '%s' ? " %(DL_constants.DL_DATA_HOST_ADDRESS, DL_constants.DL_DATA_INCOMING_ROOT_PATH)
            proceed = utility.query_yes_no(msg, default="no")
            if proceed:
                sep = '\n' + '*'*80 + '\n'
                print("%s Parsing from a laptop is used to copy local file/directory to the DL-Data repo host, and parse it over there!!!!\n This could be impractical, but will be implemented SOON anyways!%s" % (sep, sep))
                sys.exit()
                #
            else:
                #
                print("Terminating")
                return [], []
                #

        #
        if not parse and not (keep_incoming or keep_extracted_raw):
            print("\nThe passed parameters will result in deleting the incoming file, and everything processed (if any)! NOT ACCEPTABLE!!!\n\n")
            raise AssertionError
        else:
            pass
        # 1- check if file is passed, or not;
        # 2- check the extension of the passed file (or every file in the incoming data directory)
        # 3- For each file, if the extension is fine, extract it in a temporary directory,
        #    and send each file to destination

        # 1- Prepare the temporary directory to extract data files (this is hrd-coded; no need to make it optional!)
        tmp_dir_name = '.tmp_untar__'
        extraction_path = os.path.join(self.__local_raw_data_path, tmp_dir_name)
        if os.path.isdir(extraction_path):
            if extract_dir_warn:
                message = "WARNING: Extraction Path Already exist; this could mean SOMEONE Else is using it, or it was NOT CLEANED-UP properly on a previous run!"
                message += "\n Do you want to proceed and overwrite existing files?"
                proceed = utility.query_yes_no(message, default="no")
            else:
                proceed = True
            if not proceed:
                print("Nothing will be done. Terminating upon request...")
                sys.exit()
            else:
                utility.cleanup_directory(tmp_dir_name, self.__local_raw_data_path, backup_existing=True)  # after debugging, switch backup_existing to False

        #
        distributed_files_list = []
        parsed_files_list = []

        # 2- Extract the passed file/directory, or all files with the incoming-data directory
        if incoming_path is not None:
            incoming_abs_path = os.path.abspath(incoming_path)

            if ignore_marked_done:
                _head, _tail = os.path.split(incoming_abs_path)
                if re.match(r'\A_DONE_', _tail, re.IGNORECASE):
                    print("SKIPPING File Marked Done: %s" % incoming_path)
                    return distributed_files_list, parsed_files_list

            #
            if os.path.isdir(incoming_abs_path):
                msg = "\nYou passed a directory: %s" % incoming_abs_path
                msg += "\nThe files inside the passed directory will be processed, but files in subdirectories are expolored based on the value of 'recursive'."
                msg += "\nIf you want to extract all files in subdirectories anyways, move the incoming files to the following path:"
                msg += "\n\t%s\n\n or set 'recursive' to True \n\n ...proceeding...\n" % self.__incoming_data_root_path
                print(msg)
                # process all files inside that directory (don't look in subdirectories)
                incoming_files_list = utility.get_list_of_files(incoming_abs_path, recursive=recursive, return_abs=True)
                for fle in incoming_files_list:
                    d_l, p_l = self.process_incoming_raw_data(incoming_path=fle, parse=parse, keep_incoming=True, keep_extracted_raw=True, raw_format=raw_format, parse_format=parse_format, extract_dir_warn=False)
                    distributed_files_list += d_l
                    parsed_files_list += p_l

            elif os.path.isfile(incoming_abs_path):
                #
                try:
                    if tarfile.is_tarfile(incoming_abs_path):
                        #
                        with tarfile.open(incoming_abs_path) as tar_obj:
                            print(" Extracting [%s] extractall to [%s] >>" % (incoming_abs_path, extraction_path)),
                            tar_obj.extractall(path=extraction_path)
                        print("DONE.")
                        print("\n")

                    elif False:  # this will be ignored; Add more extensions here if you wish
                        pass
                    else:
                        print("Not a TAR file; unsupporte file type found in : %s" % incoming_abs_path)

                        _, fl_ext = os.path.splitext(incoming_abs_path)
                        print("Unsupport file type [YET!] '%s \n ... passing this file [%s] ...' " % (fl_ext, incoming_abs_path))
                        return distributed_files_list, parsed_files_list
                        # raise ValueError
                except IOError as err:
                    err_message = err.message
                    if 'permission' in err_message.lower() or 'permission' in str(err).lower():
                        _head, _tail = os.path.split(incoming_abs_path)
                        if _tail.startswith("_PERMISSION_DENIED_"):
                            print("Skipping file with Permission Issues: %s" % incoming_abs_path)
                        else:
                            _tail = "_PERMISSION_DENIED_%s" % _tail
                            os.rename(incoming_abs_path, os.path.join(_head, _tail))
                            print("Marked, with '_PERMISSION_DENIED_', a File with Permission Issues: %s" % incoming_abs_path)
                        print("Original Error Message: %s" % err_message )
                        return distributed_files_list, parsed_files_list
                    else:
                        # print("\n\nError Message: %s\n\n" % err.message)
                        # print(err)
                        raise err


            else:
                print("\n\nPASSED INCOMING RAW DATA FILE DOES NOT EXIST!\n passed file name: %s\n\n" % incoming_abs_path)
                raise IOError

        else:
            # Look into incoming data directory, and get a list of all files (recursively)
            msg = "\nYou didn't pass a file or a directory of incoming raw data to process"
            msg += "\nThe files inside the default incoming-raw-data directory will be processed; all files in the subdirectories will be processed."
            msg += "\n> Looking inside: %s\n...\n" % self.__incoming_data_root_path
            print(msg)
            # Recursion...
            incoming_files_list = utility.get_list_of_files(self.__incoming_data_root_path, recursive=recursive, return_abs=True)
            for fle in incoming_files_list:
                d_l, p_l = self.process_incoming_raw_data(incoming_path=fle, parse=parse, keep_incoming=True, keep_extracted_raw=True, raw_format=raw_format, parse_format=parse_format, extract_dir_warn=False)
                distributed_files_list += d_l
                parsed_files_list += p_l
            #


        #
        # At this point, Incoming file is extracted in 'extraction_path'
        # Now, look inside the extraction path, and distribute each file
        #
        if not os.path.isdir(extraction_path):
            # upon recursion, the extraction path may be cleaned
            return distributed_files_list, parsed_files_list
            #
        # Now, get a list of all files (recursively) inside 'extraction_path', and distribute it to the right destination...
        raw_files_list = utility.get_list_of_files(extraction_path, recursive=True, return_abs=True)
        # distributed_files_list = []
        for fle in raw_files_list:
            target_file = self._raw_file_to_distination(fle, overwrite=overwrite_raw)
            if target_file is not None:
                distributed_files_list.append(target_file)
        #
        # cleanup the 'tmp_dir_name':
        shutil.rmtree(extraction_path)  # this is empty by now...
        print("Cleaning up extraction path %s" %extraction_path)
        print("Done.")

        if parse:  # parse each of the incoming files
            # parsed_files_list = []
            for fle in distributed_files_list:
                f_name, f_ext = os.path.splitext(fle)
                if raw_format is None:
                    pass  # proceed with the extraction
                elif not re.match(r"\A(.)*%s\Z"%raw_format.lstrip('.') , f_ext, re.IGNORECASE):
                    print("File not to be parsed [skipped raw_format]: %s" % fle)
                    continue
                else:
                    pass
                target = self._raw_path_to_parsed(f_name, parse_format)
                #
                if self._verbose:
                    print("Parsing", fle, "INTO > ", target),
                #
                self.parse_DL_raw_data(fle, target, parsed_file_format=parse_format, overwrite_parsed=overwrite_parsed)
                parsed_files_list.append(target)
                #
                if self._verbose:
                    print("DONE...")
            # processed_files_list = parsed_files_list
        else:
            pass
            # parsed_files_list = []
            # processed_files_list = distributed_files_list

        if not keep_incoming:
            message = "\n Do you want to proceed and delete the INCOMING RAW FILES?!!\n"
            message += "Be CAREFUL?!! "
            proceed = utility.query_yes_no(message, default="no")
            #
            if proceed:
                if os.path.isfile(incoming_abs_path):
                    os.remove(incoming_abs_path)
                elif os.path.isdir(incoming_abs_path):
                    shutil.rmtree(incoming_abs_path)
                else:
                    print("\n\n\t>>>This branch should NEVER be reached! DEBUG<<<\n\n")
                    raise ValueError
            else:
                print("OK; Keeping incoming files/directories...")
                pass
        else:
            # mark as done
            head, tail = os.path.split(incoming_abs_path)
            tail = "_DONE_%s" % tail
            new_abs_path = os.path.join(head, tail)
            os.rename(incoming_abs_path, new_abs_path)
            print(" >> Marked '%s' as _DONE_ \n" % new_abs_path)

        if not keep_extracted_raw:
            message = "\n Do you want to proceed and delete the INCOMING RAW FILES?!!\n"
            message += "Be CAREFUL?!! "
            proceed = utility.query_yes_no(message, default="no")
            #
            if proceed:
                for fle in distributed_files_list:
                    os.remove(fle)
            else:
                print("OK; Keeping extracted raw files...")

        #
        return distributed_files_list, parsed_files_list


    def synchronize_DL_data_files(self, site_facility, timespan,
                                  scan_type='all',
                                  parse_remote_raw_data=False,
                                  parsed_file_format='hdf',
                                  raw_file_format='hpl'):
        """
        Return a timespan, and list or an ensemble (based on return_ensemble) of observations aggregated between consecutive enries of timespan;
        observations are assumed to be taken at the beginning of each subinterval in timespan

        Args:
            field:
            site_facility:
            timespan:
            parse_remote_raw_data:
            parsed_file_format:
            raw_file_format:


        Returns:
            times: a list containing timepoints at which observations are collected (or aggregated)
            observations: a list of observations (DL_obs instances or numpy arrays)

        """
        assert isinstance(timespan, list), "Time span must be a list of time formatted as string/datatime.datetime; see utility module!"
        if len(timespan) == 0:
            print("WARNING: Empty time interval is passed!")
            return [], []
        elif len(timespan) == 1:
            print("It is very UNLIKELY that an observation will be found at a single time instance!")
        else:
            pass
            # print("Looking for data over the interval: ", timespan)
        _start_time, _end_time = timespan[0], timespan[-1]

        #
        # HOST/CLIENT step (remote data collection and parsing) is TODO...
        if self.__I_AM_HOST:
            # print("***** RUNING ON REMOTE ***** ")
            if parse_remote_raw_data:
                # GEt matching files list (parsed, and raw)
                _, raw_list = self._lookup_matching_files(_start_time, _end_time, site_facility=site_facility,
                                                                                  scan_type=scan_type,
                                                                                  parsed_file_format=parsed_file_format,
                                                                                  lookup_rawfiles=True,
                                                                                  raw_file_format=raw_file_format)
                # Parse files (don't overwrite existing parsed files)  # TODO: refactor to remove unnecessary work!
                for fle in raw_list:
                    f_name, f_ext = os.path.splitext(fle)
                    if raw_file_format is None:
                        pass  # proceed with the extraction
                    elif not re.match(r"\A(.)*%s\Z"%raw_file_format.lstrip('.') , f_ext, re.IGNORECASE):
                        print("File not to be parsed [skipped raw_format]: %s" % fle)
                        continue
                    else:
                        pass
                    target = self._raw_path_to_parsed(f_name, parsed_file_format)
                    #
                    if self._verbose:
                        print("Parsing", fle, "INTO > ", target),
                    #
                    self.parse_DL_raw_data(fle, target, parsed_file_format=parsed_file_format, overwrite_parsed=False)
                    #
                    if self._verbose:
                        print("DONE...")
            else:
                # Don't parse raw files; just proceed
                pass

        else:
            if self._verbose:
                print("***** RUNING ON LOCALHOST ***** ")
            # Not Host; ping host to get remote files list...
            if parse_remote_raw_data:
                # GEt matching files list (parsed, and raw)
                print("\n >>> Remote Parsing is not yet supported!\nYou need to re-run this on  the server side...\n\n")
                raise NotImplementedError

            self.get_remote_matching_parsed_files(start_time=_start_time,
                                                   end_time=_end_time,
                                                   site_facility=site_facility,
                                                   scan_type=scan_type,
                                                   parsed_file_format=parsed_file_format,
                                                   silent_mode=True)

        # Prepare a list of files with measurements within the passed time bounds
        loc_pars_list, _ = self._lookup_matching_files(_start_time, _end_time, site_facility=site_facility,
                                                                               scan_type=scan_type,
                                                                               parsed_file_format=parsed_file_format,
                                                                               lookup_rawfiles=False)

        #
        # print("\n*** \nDL data files on the time interval:")
        # print("From: %s " % _start_time)
        # print("To  : %s " % _end_time)
        # print("\nNumber of files found is: %d\n*** \n" % len(loc_pars_list))
        #
        return loc_pars_list


    def get_DL_site_observations(self, field, site_facility, timespan, return_ensemble=False,
                                                                       missing_as='NaN',
                                                                       aggregate_data=True,
                                                                       skip_unavailable=False,
                                                                       snr_threshold=False,
                                                                       snr_threshold_val=0.008,
                                                                       collect_remote_data=False,
                                                                       parse_remote_raw_data=False,
                                                                       parsed_file_format='hdf',
                                                                       raw_file_format='hpl'):
        """
        Return a timespan, and list or an ensemble (based on return_ensemble) of observations aggregated between consecutive enries of timespan;
        observations are assumed to be taken at the beginning of each subinterval in timespan

        Args:
            field:
            site_facility:
            timespan:
            return_ensemble:
            missing_as:
            aggregate_data: If True, data will be aggregated between consecutive enries of timespan.
                If False, the beginning and endpoint in timespan only will be taken, and
                the timepoints between them will be redefined based on data collection frequency
            skip_unavailable: skip the times at which observations are unavailable (all NaNs)
            snr_threshold:
            snr_threshold_val:
            collect_remote_data:
            parse_remote_raw_data:
            parsed_file_format:
            raw_file_format:


        Returns:
            times: a list containing timepoints at which observations are collected (or aggregated)
            observations: a list of observations (DL_obs instances or numpy arrays)

        """
        assert isinstance(timespan, list), "Time span must be a list of time formatted as string/datatime.datetime; see utility module!"
        if len(timespan) == 0:
            print("WARNING: Empty time interval is passed!")
            return [], []
        elif len(timespan) == 1:
            print("It is very UNLIKELY that an observation will be found at a single time instance!")
        else:
            pass
            # print("Looking for data over the interval: ", timespan)
        #
        if not isinstance(field, str):
            print("The 'field' argument must be a string representing the filed to read from the DL repository files!")
            raise TypeError

        _start_time, _end_time = timespan[0], timespan[-1]
        #
        # HOST/CLIENT step (remote data collection and parsing) is TODO...
        if self.__I_AM_HOST:
            # print("***** RUNING ON REMOTE ***** ")
            if parse_remote_raw_data:
                # GEt matching files list (parsed, and raw)
                _, raw_list = self._lookup_matching_files(_start_time, _end_time, site_facility=site_facility,
                                                                                  scan_type='all',
                                                                                  parsed_file_format=parsed_file_format,
                                                                                  lookup_rawfiles=True,
                                                                                  raw_file_format=raw_file_format)
                # Parse files (don't overwrite existing parsed files)  # TODO: refactor to remove unnecessary work!
                for fle in raw_list:
                    f_name, f_ext = os.path.splitext(fle)
                    if raw_file_format is None:
                        pass  # proceed with the extraction
                    elif not re.match(r"\A(.)*%s\Z"%raw_file_format.lstrip('.') , f_ext, re.IGNORECASE):
                        print("File not to be parsed [skipped raw_format]: %s" % fle)
                        continue
                    else:
                        pass
                    target = self._raw_path_to_parsed(f_name, parsed_file_format)
                    #
                    if self._verbose:
                        print("Parsing", fle, "INTO > ", target),
                    #
                    self.parse_DL_raw_data(fle, target, parsed_file_format=parsed_file_format, overwrite_parsed=False)
                    #
                    if self._verbose:
                        print("DONE...")
            else:
                # Don't parse raw files; just proceed
                pass

        else:
            if self._verbose:
                print("***** RUNING ON LOCALHOST ***** ")
            # Not Host; ping host to get remote files list...
            if parse_remote_raw_data:
                # GEt matching files list (parsed, and raw)
                print("\n >>> Remote Parsing is not yet supported!\n\n")
                raise NotImplementedError
                # pass

            if collect_remote_data:
                self.get_remote_matching_parsed_files(start_time=_start_time,
                                                       end_time=_end_time,
                                                       site_facility=site_facility,
                                                       scan_type='all',
                                                       parsed_file_format=parsed_file_format)
            else:
                pass

        # Prepare a list of files with measurements within the passed time bounds
        loc_pars_list, _ = self._lookup_matching_files(_start_time, _end_time, site_facility=site_facility,
                                                                               scan_type='all',
                                                                               parsed_file_format=parsed_file_format,
                                                                               lookup_rawfiles=False)

        # placeholder, for checking/debugging
        observations = None

        if aggregate_data:
            # Read things from local repo
            times = []
            # observations = None
            # start reading local data

            # TODO: Where is this coming from!!!
            # if not overwrite and os.path.isfile(target_file):
            #     print("Skipping Match Exisiting Raw File: %s" % target_file)
            # else:
            #     shutil.move(passed_abs_path, target_file)

            # iterate over entries of the timespan, and aggregate by averaging
            if self._verbose:
                print("Data Collection Starting...")
            for i in range(len(timespan)-1):
                t0 , t1 = timespan[i], timespan[i+1]
                if self._verbose:
                    print("\r > [%04d/%04d] > Collecting data between %40s, %40s ..." %(i+1, len(timespan)-1 ,repr(t0), repr(t1))),
                    sys.stdout.flush()
                obs = self.aggregate_DL_data(start_time=t0, end_time=t1, site_facility=site_facility, prog_var=field, snr_threshold=snr_threshold, snr_threshold_val=snr_threshold_val, parsed_file_format=parsed_file_format)

                if i==0 :
                    if return_ensemble:
                        observations = DL_SITE_OBS.Ensemble(ensemble_size=0, vec_size=obs.size, sequential=obs.is_sequential())
                    else:
                        observations = []
                #
                # Check validity of data:
                if obs.size == obs.number_of_nans():
                    if self._verbose:
                        print("No observation found at time : %s " % str(t0))
                    if skip_unavailable:
                        continue
                else:
                    # save results
                    times.append(t0)
                    observations.append(obs)
            if self._verbose:
                print("\nDone...")
        else:
            # No data aggregation required. Collect data as they are collected, and modify the timespan accordingly
            times, observations = self.gather_DL_data(start_time=timespan[0], end_time=timespan[-1], site_facility=site_facility, prog_var=field, snr_threshold=snr_threshold, snr_threshold_val=snr_threshold_val, parsed_file_format=parsed_file_format)
        
        if observations is None:
            print("*** \nNo obsrvatations found over the time interval:")
            print(times)
            print("***")
        return times, observations
        #


    # TODO: Update the docstring based on the new design of this method
    # Refactor this method for no aggregation!
    def filter_DL_site_observations(self, field, site_facility, timespan=None,
                                                                time_bounds=None,
                                                                aggregate_data=True,
                                                                elevations=None,
                                                                azimuths=None,
                                                                altitudes=None,
                                                                range_gates=None,
                                                                # site_facilities=None,  # spgdlE32, etc...
                                                                scan_type=None,
                                                                vad_scan_degree=60,  # only of scan_type is not None, and is 'vad'
                                                                obs_as_arrays=False,
                                                                missing_as='NaN',
                                                                snr_threshold=False,
                                                                snr_threshold_val=0.008,
                                                                collect_remote_data=False,
                                                                parse_remote_raw_data=False,
                                                                parsed_file_format='hdf',
                                                                raw_file_format='hpl'):
        """
        Filter observations based on passed criteria; observations satisfying intersection/all passed criteria are returned along with corresponding coordinates;

        Given both local, and remote, data directories, start looking for DL data of the passed (fields, site, etc)
        Either pass a timespan, or time_bounds
            - If timespan is passed (must be an iterable with entries each is a timedate instance or string represetation of timedate YYYY:MM:DD:hh:mm:ss )
                In this case, observations are aggregated (averaged) between time instances (assigned to beginning of the time span)
            - If time_bounds are passed (tuple with (start_date, end_date)), observations will be collected a each signle time instance as they are measured
                In this case the returned timespan may be irregular, and observations will be too sparse!

        Observations are returned as a list of DL_obs instances unless obs_as_arrays is set to True, in such case numpy arrays are extracted from observation vectors

        Args:
            TODO: get from old docstring (below, ) and update info
            aggregate_data: If True, data will be aggregated between consecutive enries of timespan.
                If False, the beginning and endpoint in timespan only will be taken, and
                the timepoints between them will be redefined based on data collection frequency


        Returns:
            times: a list containing timepoints at which observations are collected (or aggregated)
            observations: a list of observations (DL_obs instances or numpy arrays)

        # Next is the old documentation: TODO: update
        in the given time limits (start_time , <= end_time),
        and return a dictionary of concatenated numpy arrays corresponding to the requested fields.
        Time will be returned as a key in the returned dictionary whether requested or no.
        the result is None if no data exist that matches the passed settings.
        If collect_remote_data is True, the reader will ping the host server (as set in DL_constants script) for other parsed data files.
        If we want, we can also look for raw data!

        Read contents of a DL-Data a SPECIFIC parsed file, in a prespecified format,
            and return the measurements collected in a specific time interval
            If any of the fields (if not None) is not available, then replace missing data with np.NaN

            This carries out the following:
            --------------------------------
            a) Look for available data in the local data directory (specified in DL_constants.py)
            b) Prepare a list data pieces availablel locally (ll),
                and a list (lr) of data to retrieve from remote server (specified in DL_constants.py)
                + If the list (lr) is not empty a list of data to retrieve from remote server
                    (specified in DL_constants.py), Connect (via SSH) to the remote host, and
                    look inside the parsed-data directory for the required information,
                    - If parsed data segments are not ready, look for corresponding raw-data files,
                        and attempt to parse data,
                    - Once parsed data from lr is available, copy the parsed file
                        to the local data directory on which the DL_DATA_HANDLER is instantiated.
            c) combine the two lists (ll, and lr), and read them from the local directory.
                This way, we make sure we have data we use locally, and copy data only upon request.

            Remark:
            -------
                Make sure you maintain a list of raw data, and parsed data remotely, and locally!

        Args:
            start_time: initial time to collect observbations at;
                start_time can take any of the following format
                    - string: 'YYYY:MM:DD:hh:mm:ss ', Please stick to this format!
                    - datetime.datetime instance
            end_time: final time to collect data from; format is similar to start_time
            field: the prognostic variable (from the raw-> parsed file to read),
                the following prognostic variables are supported:
                    - 'doppler'
                    - 'backscatter'
                    - 'intensity'

            range_gates: an iterable with rnge-gates (enumerated from 0) to collect data from;
                if None, all available range_gates are inspected
            site_facilities: an iterable with site facilities to collect data from;
                if None, all site facilities are inspected
            missing_as_nan: If True, any missing data or fields will be replaced with np.NaN,
                otherwise a ValueError is raised
            format: format of the parsed file;
                Currently supported are:
                - 'hdf5': save the parsed data into hdf5 format with extension '.h5'
            collect_remote_data: Look for data on the remote server as well as locally;
                if False, only local data will be retrieved,
                and None is returned if no local data satysfying the requested criteris exist!
            parse_remote_raw_data: If raw data happen to exist, and ( if collect_remote is True
                OR the code runs on the host server), the parser is called to convert raw data to
                a suitable parsed format (the default in the parser method)
            parsed_file_format: read from the parsed data of this type (only hdf is supported for now)

        Returns:
            dl_data: a dictionary containing all required information, along with metainformation
                all returned elements in the dictionary are represented as numpy arrays (TODO: update this!)

        """
        # print("\n\n STAY TUNED \nTHIS METHOD IS BEING Updated\n\n ")
        # raise TypeError

        if isinstance(missing_as, str):
            if re.match(r'nan\Z', missing_as, re.IGNORECASE):
                missing_as = np.nan
            else:
                print("missing_as can be either a numeric value or a 'nan' string! Received: %s!" % missing_as)
                raise ValueError
        elif np.isscalar(missing_as):
            pass
        else:
            print("missing_as must be either a 'nan' string, or a numeric value! Received: type<%s>" % type(missing_as))
            raise TypeError

        print("Collecting observations...")
        delim = ':'
        min_year = 2000   # the minimum year, data is known to be avaialble (just in case user is carelles enought to input right time format!)
        if time_bounds is None:
            pass
        else:
            assert isinstance(time_bounds, tuple), "time_bounds must be either None, or a tuple with time limits!"
            if len(time_bounds) != 2:
                print("time_bounds must be either None, or a tuple of length 2, with time limits!")
                raise AssertionError

        if timespan is None and time_bounds is None:
            print("Either set timespan, or time_bounds")
            raise ValueError
        elif timespan is not None and time_bounds is not None:
            print("Only one of the two arguments timespan, OR time_bounds can be provided!")
            raise ValueError
            # TODO: We can add a conformability check later
        elif timespan is not None:
            assert utility.isiterable(timespan)
            start_time, end_time = timespan[0], timespan[-1]
        elif time_bounds is not None:
            start_time, end_time = time_bounds
        else:
            print("Impossible branch; coding error!")
            raise RuntimeError

        assert isinstance(start_time, (str, datetime)), "'start_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, but not %s" %type(start_time)
        assert isinstance(end_time, (str, datetime)), "'end_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, but not %s" %type(end_time)
        if isinstance(start_time, str):
            lst = start_time.split(delim)
            _inits = [str(min_year), '01', '01', '00', '00', '00', '00']  #
            if 1 <= len(lst)<= 6:
                lst += _inits[len(lst): ]
            elif len(lst) == len(_inits):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            _start_time = delim.join(lst)

        elif isinstance(start_time, datetime):
            _start_time = "%04d:%02d:%02d:%02d:%02d:%02d:%d" %(start_time.year, start_time.month, start_time.day, start_time.hour, start_time.minute, start_time.second, start_time.microsecond)
        else:
            print("'start_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(start_time))
            raise TypeError

        if isinstance(end_time, str):
            lst = end_time.split(delim)
            today = datetime.today()
            _fins = [str(i) for i in (today.year, today.month, today.day, today.hour, today.minute, today.second, today.microsecond)]
            if 1 <= len(lst)<= 6:
                lst += _fins[len(lst): ]
            elif len(lst) == len(_fins):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            _end_time = delim.join(lst)

        elif isinstance(end_time, datetime):
            _end_time = "%04d:%02d:%02d:%02d:%02d:%02d:%d" %(end_time.year, end_time.month, end_time.day, end_time.hour, end_time.minute, end_time.second, end_time.microsecond)
        else:
            print("'end_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(end_time))
            raise TypeError

        if not isinstance(field, str):
            print("The 'field' argument must be a string representing the filed to read from the DL repository files!")
            raise TypeError
        # elif not field.lower() in DL_DATA_HANDLER.__supported_DL_fields:
        #     print("Unknown or unsupported field %s!" %field)
        #     print("The 'filed' must be either of the following:")
        #     for f in DL_DATA_HANDLER.__supported_DL_fields: print(" - %s" % f)
        #     raise ValueError
        else:
            pass  # Good to go

        #
        # HOST/CLIENT step (remote data collection and parsing) is TODO...
        if self.__I_AM_HOST:
            if parse_remote_raw_data:
                # GEt matching files list (parsed, and raw)
                _, raw_list = self._lookup_matching_files(_start_time, _end_time, site_facility=site_facility,
                                                                                  scan_type=scan_type,  # scan_type
                                                                                  parsed_file_format=parsed_file_format,
                                                                                  lookup_rawfiles=True,
                                                                                  raw_file_format=raw_file_format)
                # Parse files (don't overwrite existing parsed files)  # TODO: refactor to remove unnecessary work!
                for fle in raw_list:
                    f_name, f_ext = os.path.splitext(fle)
                    if raw_file_format is None:
                        pass  # proceed with the extraction
                    elif not re.match(r"\A(.)*%s\Z"%raw_file_format.lstrip('.') , f_ext, re.IGNORECASE):
                        print("File not to be parsed [skipped raw_format]: %s" % fle)
                        continue
                    else:
                        pass
                    target = self._raw_path_to_parsed(f_name, parsed_file_format)
                    #
                    if self._verbose:
                        print("Parsing", fle, "INTO > ", target),
                    #
                    self.parse_DL_raw_data(fle, target, parsed_file_format=parsed_file_format, overwrite_parsed=False)
                    #
                    if self._verbose:
                        print("DONE...")
            else:
                # Don't parse raw files; just proceed
                pass

        else:
            # Not Host; ping host to get remote files list...
            if collect_remote_data:
                self.get_remote_matching_parsed_files(start_time=_start_time,
                                                       end_time=_end_time,
                                                       site_facility=site_facility,
                                                       scan_type=scan_type,
                                                       parsed_file_format=parsed_file_format)
            else:
                pass

        # # Prepare a list of files with measurements within the passed time bounds
        # loc_pars_list, _ = self._lookup_matching_files(_start_time, _end_time, site_facility=site_facility,
        #                                                                        scan_type=scan_type,
        #                                                                        parsed_file_format=parsed_file_format,
        #                                                                        lookup_rawfiles=False)

        # print("Number of files matching: %d" % len(loc_pars_list))
        # for e, fl in enumerate(loc_pars_list):
        #     print e, fl

        # Read things from local repo
        times = []
        coordinates = []
        observations = []
        #
        #
        if aggregate_data:
            # start reading local data (aggregate over each subinterval in the timespan/time-bounds)
            if timespan is not None:
                # iterate over entries of the timespan, and aggregate by averaging
                print("Data Collection Started over time-interval [%s >--> %s] ..." %(str(timespan[0]), str(timespan[-1])))
                for i in range(len(timespan)-1):
                    t0 , t1 = timespan[i], timespan[i+1]
                    print("\r > [%04d/%04d] > Collecting data between %40s, %40s" %(i+1, len(timespan)-1, repr(t0), repr(t1))),
                    sys.stdout.flush()
                    _obs = self.aggregate_DL_data(start_time=t0, end_time=t1, site_facility=site_facility, prog_var=field, scan_type=scan_type, snr_threshold=snr_threshold, snr_threshold_val=snr_threshold_val, parsed_file_format=parsed_file_format)
                    coords, obs = self._obs_extractor(_obs, elevations, azimuths, range_gates, altitudes, scan_type, vad_scan_degree, missing_as)
                    #
                    # append results
                    times.append(t0)
                    coordinates.append(coords)
                    observations.append(obs)
                print("\nDone...")

            elif time_bounds is not None:
                sep = "\n" + "<>" * 40 + "\n"
                d_ms = 1e+6  # timedelta in microseconds    # TODO: MAYBE Refactor!!!
                print(sep + "WARNING: You passed only time-bbbounds, and switched data aggregation flag 'aggregate_data' to True" + sep)
                print(sep + "WARNING: This may take too long as I will be looking for observations collected over every tiny interval (basically %d MICROSECONDS)" % d_ms + sep)

                print("Data Collection Started over time-interval [%s >--> %s] ..." %(str(time_bounds[0]), str(time_bounds[-1])))

                time_iterator = utility.TIME_ITERATOR(start_time=_start_time, end_time=_end_time, delta_time=timedelta(microseconds=d_ms), return_string=False)
                for t0 in time_iterator:
                    try:
                        t1 = time_iterator.whats_next()
                    except(StopIteration):
                        break
                        #
                    _obs = self.aggregate_DL_data(start_time=t0, end_time=t1, site_facility=site_facility, prog_var=field, scan_type=scan_type, parsed_file_format=parsed_file_format)
                    coords, obs = self._obs_extractor(_obs, elevations, azimuths, range_gates, altitudes, scan_type, vad_scan_degree, missing_as)

                    if len(obs) == 0:
                        continue

                    # pass missing data
                    if (np.isnan(obs)).all():
                        continue
                    #
                    # save results
                    times.append(t0)
                    coordinates.append(coords)
                    observations.append(obs)
            else:
                print("Impossible! Check the code!")
                raise RuntimeError

        else:
            # Collect data as they appear; ignore steps in timespa--if any--, and collect data as they appear between start, and end times
            if timespan is not None:
                t0, t1 = timespan[0], timespan[-1]
            elif time_bounds is not None:
                t0, t1 = time_bounds[0], time_bounds[-1]
            else:
                print("Impossible! Check the code!")
                raise RuntimeError

            print("Data Collection Started over time-interval [%s >--> %s] ..." % (str(t0), str(t1)) )
            times_list, obs_list = self.gather_DL_data(start_time=t0, end_time=t1, site_facility=site_facility, prog_var=field, scan_type=scan_type, snr_threshold=snr_threshold, snr_threshold_val=snr_threshold_val, parsed_file_format=parsed_file_format)
            for _t, _obs in zip(times_list, obs_list):
                # num_valid_entries = _obs.number_of_valid_entries()
                # if num_valid_entries > 0:
                #   pass
                coords, obs = self._obs_extractor(_obs, elevations, azimuths, range_gates, altitudes, scan_type, vad_scan_degree, missing_as)
                # append results

                if len(obs) == 0:
                    continue
                # pass missing data
                if (np.isnan(obs)).all():
                    continue

                times.append(_t)
                coordinates.append(coords)
                observations.append(obs)

        return times, coordinates, observations
        #

    def aggregate_DL_data(self, start_time, end_time, site_facility, prog_var='doppler', scan_type='all', snr_threshold=False, snr_threshold_val=0.008, correct_with_nan=True, parsed_file_format='hdf'):
        """
        collect DL data taken from a given site facility within the interval [start_time, end_time)
        Measurements over this time interval is are averaged to generate an observation vector of specific size

        snr_threshold: If True, truncate values (set to NaN) corresponding to intensity (SNR+1) i.e. below 1+snr_threshold_val
            The threshold value 0.008 comes from https://www.arm.gov/publications/tech_reports/doe-sc-arm-tr-149.pdf , Page 5

        """
        assert 0 <= snr_threshold_val < 1, "The Signal-To-Noise Ratio 'snr_threshold_val' must be 0 <= SNR < 1"
        #
        time_delim = ':'
        #
        if isinstance(start_time, datetime):
            _start_time = self._timestamp_to_str(start_time, time_delim)
        elif isinstance(start_time, str):
            _start_time = start_time
        else:
            print("start_time must be either a string or datetime.datetime instance! Received: %s of type: %s" % (start_time, type(start_time)))
            raise TypeError
        if isinstance(end_time, datetime):
            _end_time = self._timestamp_to_str(end_time, time_delim)
        elif isinstance(end_time, str):
            _end_time = end_time
        else:
            print("end_time must be either a string or datetime.datetime instance! Received: %s of type: %s" % (end_time, type(end_time)))
            raise TypeError


        matching_files, _ = self._lookup_matching_files(_start_time, _end_time, site_facility, scan_type=scan_type, parsed_file_format=parsed_file_format, lookup_rawfiles=False)
        # print(">> site_facility, _start_time, end_time, number of matching files:", site_facility, _start_time, _end_time, len(matching_files))
        beg_time = _start_time.split(time_delim)
        fin_time = _end_time.split(time_delim)
        if not (6<=len(beg_time)<=7 and 6<=len(fin_time)<=7):
            print("The arguments: 'start_time', and 'end_time' must be strings with the following format: 'YYYY:MM:DD:hh:mm:ss:<ms>'\n Please check docstring of this function!")
            raise AssertionError
        else:
            pass

        init_decimal_date = int(''.join(beg_time[0:3]))  # YYYYMMDD
        init_decimal_time = self._str_to_decimal_time(time_delim.join(beg_time[3:]), delimeter=time_delim)  #
        #
        fin_decimal_date = int(''.join(fin_time[0:3]))  # YYYYMMDD
        fin_decimal_time = self._str_to_decimal_time(time_delim.join(fin_time[3:]), delimeter=time_delim)

        # time bounds
        year_bnds = [int(beg_time[0]), int(fin_time[0])]
        month_bnds = [int(beg_time[1]), int(fin_time[1])]
        day_bnds = [int(beg_time[2]), int(fin_time[2])]


        # Get Timestamps for comparison:
        _h, _m, _s, _ms = self._time_from_decimal(init_decimal_time)
        initial_timestamp = datetime(year_bnds[0], month_bnds[0], day_bnds[0], _h, _m, _s, _ms)
        if self._verbose:
            print("**** Incoming time: ", _start_time)
            print("initial_timestamp", initial_timestamp, 'from', init_decimal_time)
        #
        _h, _m, _s, _ms = self._time_from_decimal(fin_decimal_time)
        final_timestamp = datetime(year_bnds[1], month_bnds[1], day_bnds[1], _h, _m, _s, _ms)
        if self._verbose:
            print("final_timestamp", final_timestamp)


        if re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            msg = "BE CAREFUL HERE; \n\t THIS [RADIAL-VELOCITY --> WIND-VELOCITY] PROJECTION HERE IS NOT UNIQUE! THIS SHOULDN'T BE USED "
            warnings.warn(msg)
        # 
        #
        # initialize an observation object:
        coords = self._get_site_facility_coordinates(None, site_facility, None, init_decimal_date)
        obs_num_gates = DL_constants.DL_NUM_GATES
        obs = DL_SITE_OBS(t=_start_time, site_facility=site_facility, dl_coordinates=coords, prog_var=prog_var, num_gates=obs_num_gates, elevations=None, azimuthes=None)
        y = obs.data  # just reference to the underlying data structure, i.e. DLidarVec instance
        #
        # fill up the observation object:
        for parsed_file in matching_files:
            # print("Collecting data from: %s" % parsed_file)
            try:
                with h5py.File(parsed_file, mode='r') as f_id:
                    timespan = f_id['time'][...]
                    azimuth = f_id['azimuth'][...]
                    elevation = f_id['elevation'][...]
                    if re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['doppler'][...]
                        obs_var = extracted_var
                    elif re.match(r'\Aintensity\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['intensity'][...]
                        obs_var = extracted_var
                    elif re.match(r'\Aback(_|-)*scatter\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['backscatter'][...]
                        obs_var = extracted_var
                    elif re.match(r'\Aradial(_|-)*velocity\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['doppler'][...]
                        obs_var = - 1.5/(4*np.pi) * extracted_var
                    elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['doppler'][...]
                        obs_var = extracted_var
                        # convert doppler to wind field

                    elif False:  # add more
                        pass
                    else:
                        print("Unsupported prognostic variable %s" % prog_var)
                        raise ValueError

                    # Threshold based on Signal Intensity if asked
                    _intensity = f_id['intensity'][...]


                    # TODO: Check with Paytsar if pitch and roll could be actually ignored as I am doing over here!
                    pitch = f_id['pitch'][...]
                    roll = f_id['roll'][...]
                    #
                    # TODO: match attributes in file with passed parameters, and raise error if mismatch happens!
                    num_gates = f_id.attrs['num_gates']
                    gate_length = f_id.attrs['range_gate_length']
                    st_decim_date = str(f_id.attrs['start_date'])
                    st_decim_time = f_id.attrs['start_time_decimal']
                    #

                    altitudes = [(i +0.5)*gate_length for i in range(num_gates)]  # (range gate index + 0.5) * range gate length

                    slider = np.ones(obs_num_gates)
                    gates_indices = range(num_gates)
                    #

                    _year = int(st_decim_date[: 4])
                    _month = int(st_decim_date[4:6])
                    _day = int(st_decim_date[6:])
                    _h, _m, _s, _ms = self._time_from_decimal(st_decim_time)
                    flt_timestamp = datetime(_year, _month, _day, _h, _m, _s, _ms)
                    average_cnt = np.zeros(y.size, dtype=int)
                    #
                    for e, a in zip(elevation, azimuth):

                        for t_ind in range(timespan.size):
                            if t_ind > 0:
                                h_diff = (24.0 + timespan[t_ind] - timespan[t_ind-1]) % 24.0
                            else:
                                h_diff = 0.0
                            flt_timestamp += timedelta(hours=h_diff)

                            if flt_timestamp > final_timestamp:
                                # get our out of this file
                                # print("Breaking")
                                break
                            elif flt_timestamp >= initial_timestamp:
                                #
                                loc_intensity = _intensity[t_ind, :]
                                if snr_threshold:
                                    threshold_locs = np.where( loc_intensity < (1+snr_threshold_val) )[0]
                                    non_thresh_locs = np.asarray(list(set(range(loc_intensity.size)).difference( set(threshold_locs.flatten().tolist()))))
                                    non_thresh_locs.sort()
                                else:
                                    threshold_locs = np.array([], dtype=int)
                                    non_thresh_locs = range(loc_intensity.size)

                                #
                                if re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
                                    values = obs_var[t_ind, :]
                                    if snr_threshold:
                                        values[threshold_locs] = np.nan
                                elif re.match(r'\Aintensity\Z', prog_var, re.IGNORECASE):
                                    values = obs_var[t_ind, :]
                                    if snr_threshold:
                                        values[threshold_locs] = np.nan
                                elif re.match(r'\Aback(_|-)*scatter\Z', prog_var, re.IGNORECASE):
                                    values = obs_var[t_ind, :]
                                    if snr_threshold:
                                        values[threshold_locs] = np.nan
                                elif re.match(r'\Aradial(_|-)*velocity\Z', prog_var, re.IGNORECASE):
                                    values = obs_var[t_ind, :]
                                    if snr_threshold:
                                        values[threshold_locs] = np.nan
                                elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                                    # convert doppler d, to wind field given elevation e, and azimuth a
                                    doppler = obs_var[t_ind, :]
                                    radial_velocity = - 1.5 / ( 4 * np.pi ) * doppler
                                    # values = np.asarray([[0,0,0]]* doppler.size)
                                    values = np.empty((doppler.size, 3))
                                    values[...] = np.nan
                                    #
                                    alpha = np.radians(e)
                                    beta  = np.radians(a)
                                    for i, rv in enumerate(radial_velocity[non_thresh_locs]):
                                        u = rv * np.cos(alpha) * np.cos(beta)
                                        v = rv * np.cos(alpha) * np.sin(beta)
                                        w = rv * np.sin(alpha)
                                        #
                                        if correct_with_nan:
                                            if e == 90:
                                                u = v = np.NaN
                                            else:
                                                if e == 0:
                                                    w = np.NaN
                                                if a in [0, 180, 360]:
                                                    v = np.NaN
                                                if a in [90, 270]:
                                                    u = np.NaN
                                        #
                                        values[i][:] = u, v, w

                                    # print("DLOBS: e, a, rv, u, v, w", e, a, rv, u, v, w)  # TODO: Remove after Debugging

                                elif False:  # add more
                                    pass
                                else:
                                    print("Unsupported prognostic variable %s" % prog_var)
                                    raise ValueError
                            else:
                                # out of time index
                                continue

                            _, updated_inds = obs.add_values(values=values, elevations=slider*e, azimuthes=slider*a, altitudes=altitudes, mismatch_policy='nearest')  # TODO: too slow!
                            # print("obs:", obs.get_np_array())
                            average_cnt[updated_inds] += 1

                    average_cnt[average_cnt==0] = 1
                    # TODO: add more functions inside obs.__class__

                    # average only over updated indexes
                    for i in range(y.size):
                        y[i] /= average_cnt[i]

            except IOError:
                print("\nFile: %s failed to be opened! \nMostly, sync was intterrupted, or the parsed file is buggy; see error message below...\n" % parsed_file)
                raise
        #
        return obs
        #


    def gather_DL_data_file_contents(self, start_time, end_time, site_facility, scan_type='all', 
                                     collect_remote_data=False, parsed_file_format='hdf'):
        """
        Similar to gather_DL_data, however it returns hdf5 data handler that you can directly read from
        Args:
            start_time
            end_time
            site_facility
            scan_type
            collect_remote_data
            parsed_file_format

        Returns:
            times: a list with ordered times falling in the passed interval; scan types are ignored in this ordering
            observations: a list of dictionaries; each dictionary contains all possible information corresponding to each time in tims. Each entry represents the contens of a
            parsed file with start time maching corresponding to the same entry in  times.
        """
        #
        time_delim = ':'
        #
        if isinstance(start_time, datetime):
            _start_time = self._timestamp_to_str(start_time, time_delim)
        elif isinstance(start_time, str):
            _start_time = start_time
        else:
            print("start_time must be either a string or datetime.datetime instance! Received: %s of type: %s" % (start_time, type(start_time)))
            raise TypeError
        if isinstance(end_time, datetime):
            _end_time = self._timestamp_to_str(end_time, time_delim)
        elif isinstance(end_time, str):
            _end_time = end_time
        else:
            print("end_time must be either a string or datetime.datetime instance! Received: %s of type: %s" % (end_time, type(end_time)))
            raise TypeError
        
        if collect_remote_data:
            self.synchronize_DL_data_files(site_facility=site_facility,
                                           timespan=[start_time, end_time],
                                           scan_type=scan_type,
                                           parse_remote_raw_data=False,
                                           parsed_file_format=parsed_file_format)

        matching_files, _ = self._lookup_matching_files(_start_time, _end_time, site_facility, scan_type=scan_type, parsed_file_format=parsed_file_format, lookup_rawfiles=False)
        # print(">> site_facility, _start_time, end_time, number of matching files:", site_facility, _start_time, _end_time, len(matching_files))
        # for e, fle in enumerate(matching_files):
        #     print e, fle, scan_type

        beg_time = _start_time.split(time_delim)
        fin_time = _end_time.split(time_delim)
        if not (6<=len(beg_time)<=7 and 6<=len(fin_time)<=7):
            print("The arguments: 'start_time', and 'end_time' must be strings with the following format: 'YYYY:MM:DD:hh:mm:ss:<ms>'\n Please check docstring of this function!")
            raise AssertionError

        init_decimal_date = int(''.join(beg_time[0:3]))  # YYYYMMDD
        init_decimal_time = self._str_to_decimal_time(time_delim.join(beg_time[3:]), delimeter=time_delim)  #
        #
        fin_decimal_date = int(''.join(fin_time[0:3]))  # YYYYMMDD
        fin_decimal_time = self._str_to_decimal_time(time_delim.join(fin_time[3:]), delimeter=time_delim)

        # time bounds
        year_bnds = [int(beg_time[0]), int(fin_time[0])]
        month_bnds = [int(beg_time[1]), int(fin_time[1])]
        day_bnds = [int(beg_time[2]), int(fin_time[2])]


        # Get Timestamps for comparison:
        _h, _m, _s, _ms = self._time_from_decimal(init_decimal_time)
        initial_timestamp = datetime(year_bnds[0], month_bnds[0], day_bnds[0], _h, _m, _s, _ms)
        if self._verbose:
            print("**** Incoming time: ", _start_time)
            print("initial_timestamp", initial_timestamp, 'from', init_decimal_time)
        #
        _h, _m, _s, _ms = self._time_from_decimal(fin_decimal_time)
        final_timestamp = datetime(year_bnds[1], month_bnds[1], day_bnds[1], _h, _m, _s, _ms)
        if self._verbose:
            print("final_timestamp", final_timestamp)

        #
        # initialize an observation object:
        coords = self._get_site_facility_coordinates(None, site_facility, None, init_decimal_date)
        obs_num_gates = DL_constants.DL_NUM_GATES
        #

        times = []
        observations = []
        # Loop over each file, and get timespan inside that file, then read contents of that file, along with corresponding observations
        #
        for parsed_file in matching_files:
            # print("Collecting data from: %s" % parsed_file)
            try:
                # open file in hand, and read data:
                with h5py.File(parsed_file, mode='r') as f_id:
                    num_gates = f_id.attrs['num_gates']
                    gate_length = f_id.attrs['range_gate_length']
                    start_date = str(f_id.attrs['start_date'])
                    start_decim_time = f_id.attrs['start_time_decimal']
                    
                    dl_coordinates = f_id.attrs['dl_coordinates']
                    focus_range = f_id.attrs['focus_range']
                    num_rays = f_id.attrs['num_rays']
                    pulses = f_id.attrs['pulses']
                    range_gate_length = f_id.attrs['range_gate_length']
                    resolution = f_id.attrs['resolution']
                    file_scan_type = f_id.attrs['scan_type']
                    stop_date = f_id.attrs['stop_date']
                    stop_time = f_id.attrs['stop_time']
                    system_id = f_id.attrs['system_id']
                    stop_time_decimal = f_id.attrs['stop_time_decimal']

                    #
                    pitch = f_id['pitch'][...]
                    roll = f_id['roll'][...]
                    #
                    timespan = f_id['time'][...]
                    azimuth = f_id['azimuth'][...]
                    elevation = f_id['elevation'][...]
                    doppler = f_id['doppler'][...]
                    intensity = f_id['intensity'][...]
                    backscatter = f_id['backscatter'][...]
                    radial_velocity = - 1.5/(4*np.pi) * doppler
                    altitudes = [(i +0.5)*gate_length for i in range(num_gates)]  # (range gate index + 0.5) * range gate length
                    #
                    _h, _m, _s, _ms = self._time_from_decimal(start_decim_time)
                    _yyyy, _mm, _dd = int(start_date[:4]), int(start_date[4:6]), int(start_date[6:])
                    #
                    start_time = "%02d:%02d:%02d.%d" % (_h, _m, _s, _ms)
                    start_date = "%4d:%02d:%02d" % (_yyyy, _mm, _dd)
                    start_date_time = "%s:%s" % (start_date, start_time)
                    #
                    # timestamp = datetime(year_bnds[0], month_bnds[0], day_bnds[0], _h, _m, _s, _ms)
                    #
                    obs_dict = dict(data_file=parsed_file,
                                    num_gates=num_gates,
                                    gate_length=gate_length,
                                    start_date=start_date,
                                    start_decim_time=start_decim_time,
                                    start_time=start_time,
                                    #
                                    dl_coordinates=dl_coordinates,
                                    focus_range=focus_range,
                                    num_rays=num_rays,
                                    pulses=pulses,
                                    range_gate_length=range_gate_length,
                                    resolution=resolution,
                                    scan_type=file_scan_type,
                                    stop_date=stop_date,
                                    stop_time=stop_time,
                                    system_id=system_id,
                                    stop_time_decimal=stop_time_decimal,
                                    #
                                    pitch=pitch,
                                    roll=roll,
                                    timespan=timespan,
                                    azimuth=azimuth,
                                    elevation=elevation,
                                    doppler=doppler,
                                    intensity=intensity,
                                    backscatter=backscatter,
                                    radial_velocity=radial_velocity,
                                    altitudes=altitudes,
                                    )
                    #
                    times.append(start_date_time)
                    observations.append(obs_dict)
                #
                # reorder based on timming!
                sorter = np.argsort(times)
                times = [times[i] for i in sorter]
                obbservations = [observations[i] for i in sorter]
            except IOError:
                print("\nFile: %s failed to be opened! \nMostly, sync was intterrupted, or the parsed file is buggy; see error message below...\n" % parsed_file)
                raise

        return times, observations

    def gather_DL_data(self, start_time, end_time, site_facility, prog_var='doppler', scan_type='all', snr_threshold=False, snr_threshold_val=0.008, correct_with_nan=True, parsed_file_format='hdf'):
        """
        Similar to aggregate_DL_data, except that data are read as they are collected without any aggregation; Could return really sparse observations, and long timespans

        Args:
            start_time:
            end_time:
            site_facility:
            prog_var:
            snr_threshold:
            snr_threshold_val:
            parsed_file_format:

        Returns:
            times:
            observations:

        """
        assert 0 <= snr_threshold_val < 1, "The Signal-To-Noise Ratio 'snr_threshold_val' must be 0 <= SNR < 1"
        #
        time_delim = ':'
        #
        if isinstance(start_time, datetime):
            _start_time = self._timestamp_to_str(start_time, time_delim)
        elif isinstance(start_time, str):
            _start_time = start_time
        else:
            print("start_time must be either a string or datetime.datetime instance! Received: %s of type: %s" % (start_time, type(start_time)))
            raise TypeError
        if isinstance(end_time, datetime):
            _end_time = self._timestamp_to_str(end_time, time_delim)
        elif isinstance(end_time, str):
            _end_time = end_time
        else:
            print("end_time must be either a string or datetime.datetime instance! Received: %s of type: %s" % (end_time, type(end_time)))
            raise TypeError


        matching_files, _ = self._lookup_matching_files(_start_time, _end_time, site_facility, scan_type=scan_type, parsed_file_format=parsed_file_format, lookup_rawfiles=False)
        # print(">> site_facility, _start_time, end_time, number of matching files:", site_facility, _start_time, _end_time, len(matching_files))
        # for e, fle in enumerate(matching_files):
        #     print e, fle, scan_type

        beg_time = _start_time.split(time_delim)
        fin_time = _end_time.split(time_delim)
        if not (6<=len(beg_time)<=7 and 6<=len(fin_time)<=7):
            print("The arguments: 'start_time', and 'end_time' must be strings with the following format: 'YYYY:MM:DD:hh:mm:ss:<ms>'\n Please check docstring of this function!")
            raise AssertionError
        else:
            pass

        init_decimal_date = int(''.join(beg_time[0:3]))  # YYYYMMDD
        init_decimal_time = self._str_to_decimal_time(time_delim.join(beg_time[3:]), delimeter=time_delim)  #
        #
        fin_decimal_date = int(''.join(fin_time[0:3]))  # YYYYMMDD
        fin_decimal_time = self._str_to_decimal_time(time_delim.join(fin_time[3:]), delimeter=time_delim)

        # time bounds
        year_bnds = [int(beg_time[0]), int(fin_time[0])]
        month_bnds = [int(beg_time[1]), int(fin_time[1])]
        day_bnds = [int(beg_time[2]), int(fin_time[2])]


        # Get Timestamps for comparison:
        _h, _m, _s, _ms = self._time_from_decimal(init_decimal_time)
        initial_timestamp = datetime(year_bnds[0], month_bnds[0], day_bnds[0], _h, _m, _s, _ms)
        if self._verbose:
            print("**** Incoming time: ", _start_time)
            print("initial_timestamp", initial_timestamp, 'from', init_decimal_time)
        #
        _h, _m, _s, _ms = self._time_from_decimal(fin_decimal_time)
        final_timestamp = datetime(year_bnds[1], month_bnds[1], day_bnds[1], _h, _m, _s, _ms)
        if self._verbose:
            print("final_timestamp", final_timestamp)


        #
        # initialize an observation object:
        coords = self._get_site_facility_coordinates(None, site_facility, None, init_decimal_date)
        obs_num_gates = DL_constants.DL_NUM_GATES
        #

        if re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            msg = "BE CAREFUL HERE; \n\t THIS [RADIAL-VELOCITY --> WIND-VELOCITY] PROJECTION HERE IS NOT UNIQUE! THIS SHOULDN'T BE USED "
            warnings.warn(msg)
        # 

        times = []
        observations = []
        # Loop over each file, and get timespan inside that file, then read contents of that file, along with corresponding observations
        #
        for parsed_file in matching_files:
            # print("Collecting data from: %s" % parsed_file)
            try:
                # open file in hand, and read data:
                with h5py.File(parsed_file, mode='r') as f_id:
                    timespan = f_id['time'][...]
                    azimuth = f_id['azimuth'][...]
                    elevation = f_id['elevation'][...]
                    if re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['doppler'][...]
                        obs_var = extracted_var
                    elif re.match(r'\Aintensity\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['intensity'][...]
                        obs_var = extracted_var
                    elif re.match(r'\Aback(_|-)*scatter\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['backscatter'][...]
                        obs_var = extracted_var
                    elif re.match(r'\Aradial(_|-)*velocity\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['doppler'][...]
                        obs_var = - 1.5/(4*np.pi) * extracted_var
                    elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                        extracted_var = f_id['doppler'][...]
                        obs_var = extracted_var
                        # convert doppler to wind field
                    elif False:  # add more
                        pass
                    else:
                        print("Unsupported prognostic variable %s" % prog_var)
                        raise ValueError

                    # Threshold based on Signal Intensity if asked
                    _intensity = f_id['intensity'][...]


                    # TODO: Check with Paytsar if pitch and roll could be actually ignored as I am doing over here!
                    pitch = f_id['pitch'][...]
                    roll = f_id['roll'][...]
                    #
                    # TODO: match attributes in file with passed parameters, and raise error if mismatch happens!
                    num_gates = f_id.attrs['num_gates']
                    gate_length = f_id.attrs['range_gate_length']
                    st_decim_date = str(f_id.attrs['start_date'])
                    st_decim_time = f_id.attrs['start_time_decimal']
                    #

                    altitudes = [(i +0.5)*gate_length for i in range(num_gates)]  # (range gate index + 0.5) * range gate length

                    slider = np.ones(obs_num_gates)
                    gates_indices = range(num_gates)
                    #

                    _year = int(st_decim_date[: 4])
                    _month = int(st_decim_date[4:6])
                    _day = int(st_decim_date[6:])
                    _h, _m, _s, _ms = self._time_from_decimal(st_decim_time)
                    flt_timestamp = datetime(_year, _month, _day, _h, _m, _s, _ms)


                    for t_ind in range(timespan.size):
                        if t_ind > 0:
                            h_diff = (24.0 + timespan[t_ind] - timespan[t_ind-1]) % 24.0
                        else:
                            h_diff = 0.0
                        flt_timestamp += timedelta(hours=h_diff)

                        if flt_timestamp > final_timestamp:
                            # get our out of this file
                            # print("Breaking")
                            break
                        elif flt_timestamp >= initial_timestamp:
                            #

                            # get corresponding Azimuth, and Elevation (all gates are loaded)
                            e = elevation[t_ind]
                            a = azimuth[t_ind]

                            loc_intensity = _intensity[t_ind, :]  # all gates at this time instance, and elevation/azimuth combination
                            if snr_threshold:
                                threshold_locs = np.where( loc_intensity < (1+snr_threshold_val) )[0]
                                non_thresh_locs = np.asarray(list(set(range(loc_intensity.size)).difference( set(threshold_locs.flatten().tolist()))))
                                non_thresh_locs.sort()
                            else:
                                threshold_locs = np.array([], dtype=int)
                                non_thresh_locs = range(loc_intensity.size)

                            #
                            if re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
                                values = obs_var[t_ind, :]
                                if snr_threshold:
                                    values[threshold_locs] = np.nan
                            elif re.match(r'\Aintensity\Z', prog_var, re.IGNORECASE):
                                values = obs_var[t_ind, :]
                                if snr_threshold:
                                    values[threshold_locs] = np.nan
                            elif re.match(r'\Aback(_|-)*scatter\Z', prog_var, re.IGNORECASE):
                                values = obs_var[t_ind, :]
                                if snr_threshold:
                                    values[threshold_locs] = np.nan
                            elif re.match(r'\Aradial(_|-)*velocity\Z', prog_var, re.IGNORECASE):
                                values = obs_var[t_ind, :]
                                if snr_threshold:
                                    values[threshold_locs] = np.nan
                            elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                                # THIS PROJECTION HERE IS NOT UNIQUE! THIS SHOULDN'T BE USED
                                # convert doppler d, to wind field given elevation e, and azimuth a
                                doppler = obs_var[t_ind, :]
                                radial_velocity = - 1.5 / ( 4 * np.pi ) * doppler
                                # values = np.asarray([[0,0,0]]* doppler.size)
                                values = np.empty((doppler.size, 3))
                                values[...] = np.nan

                                for i, rv in enumerate(radial_velocity[non_thresh_locs]):
                                    alpha = np.radians(e)
                                    beta = np.radians(a)
                                    u = rv * np.cos(alpha) * np.cos(beta)
                                    v = rv * np.cos(alpha) * np.sin(beta)
                                    w = rv * np.sin(alpha)
                                    #
                                    if correct_with_nan:
                                        if e == 90:
                                            u = v = np.NaN
                                        else:
                                            if e == 0:
                                                w = np.NaN
                                            if a in [0, 180, 360]:
                                                v = np.NaN
                                            if a in [90, 270]:
                                                u = np.NaN
                                    #
                                    values[i][:] = u, v, w

                                    # print("DLOBS: e, a, rv, u, v, w", e, a, rv, u, v, w)  # TODO: Remove after Debugging

                            elif False:  # add more
                                pass
                            else:
                                print("Unsupported prognostic variable %s" % prog_var)
                                raise ValueError

                            # Add time to times list
                            try:
                                found_ind = times.index(flt_timestamp)
                            except(ValueError):
                                found_ind = None

                            # print("Check this:", flt_timestamp)
                            if found_ind is None:
                                obs = DL_SITE_OBS(t=flt_timestamp, site_facility=site_facility, dl_coordinates=coords, prog_var=prog_var, num_gates=obs_num_gates, elevations=None, azimuthes=None)
                                # print("Fresh Observation:")
                                # print(obs.get_local_grid())

                                if len(times)==0:
                                    times.append(flt_timestamp)
                                    observations.append(obs)
                                    found_ind = 0
                                else:
                                    if flt_timestamp < times[0]:
                                        times.insert(0, flt_timestamp)
                                        observations.insert(0, obs)
                                        found_ind = 0
                                    elif flt_timestamp > times[-1]:
                                        found_ind = len(times)
                                        times.append(flt_timestamp)
                                        observations.append(obs)
                                    else:
                                        i = 0
                                        while i<len(times):
                                            if flt_timestamp == times[i]:
                                                # TODO: is it possible!
                                                print("This time stamp has been previously added!")
                                                print("Check for duplicate files!")
                                                if True:  # TODO: remove branching after Debugging
                                                    raise ValueError
                                                else:
                                                    # I've added this new method 'add' to DL_obs, but it shouldn't bbe reached here!
                                                    observations[i].add(obs, in_place=True)
                                                    found_ind = i
                                                break
                                            elif flt_timestamp < times[i]:
                                                times.insert(i, flt_timestamp)
                                                observations.insert(i, obs)
                                                found_ind = i
                                                break
                                            else:
                                                pass
                                            i +=1
                                            if i==len(times):
                                                print("THIS SHOULDN't HAPPEN! time index exceeded last entry in the timespan!")

                                # TODO: Remve after debugging
                                if found_ind is None:
                                    print("This is impossible: No proper time indices found to insert observation! Check format of flt_timestamp ")
                                    print("flt_timestamp: ", flt_timestamp)
                                    print("times: ", times)
                                    raise ValueError

                            obs = observations[found_ind]
                            # update observation:
                            # print("Adding at elevation, azimuth, altitudes: ", e, a, altitudes)
                            # print("Values:", values)
                            _, updated_inds = obs.add_values(values=values, elevations=slider*e, azimuthes=slider*a, altitudes=altitudes, mismatch_policy='nearest')

                            if observations[found_ind].number_of_valid_entries() == 0:
                                print("Empty observation vector detected; pop..")
                                observations.pop(found_ind)
                                times.pop(found_ind)

                            # # Check updates:
                            # for g, al in zip(range(num_gates), altitudes):
                            #     ind = observations[found_ind].get_index_from_local_coordinates(e, a, altitude=al)
                            #     print(ind, observations[found_ind].get_val(e,a,g))

                        else:
                            # out of time index
                            continue

            except IOError:
                print("\nFile: %s failed to be opened! \nMostly, sync was intterrupted, or the parsed file is buggy; see error message below...\n" % parsed_file)
                raise

        return times, observations


    def observation_vector(self, start_time, end_time, site_facility, prog_var='doppler', parsed_file_format='hdf'):
        """
        collect DL data taken from a given site facility within the interval [start_time, end_time)
        Measurements over this time interval is are averaged to generate an observation vector of specific size

        """
        time_delim = ':'
        matching_files, _ = self._lookup_matching_files(start_time, end_time, site_facility, parsed_file_format=parsed_file_format, lookup_rawfiles=False)

        beg_time = start_time.split(time_delim)
        fin_time = end_time.split(time_delim)
        if not (6<=len(beg_time)<=7 and 6<=len(fin_time)<=7):
            print("The arguments: 'start_time', and 'end_time' must be strings with the following format: 'YYYY:MM:DD:hh:mm:ss:<ms>'\n Please check docstring of this function!")
            raise AssertionError
        else:
            pass

        init_decimal_date = int(''.join(beg_time[0:3]))  # YYYYMMDD
        init_decimal_time = self._str_to_decimal_time(time_delim.join(beg_time[3:]), delimeter=time_delim)  #
        #
        fin_decimal_date = int(''.join(fin_time[0:3]))  # YYYYMMDD
        fin_decimal_time = self._str_to_decimal_time(time_delim.join(fin_time[3:]), delimeter=time_delim)

        # time bounds
        year_bnds = [int(beg_time[0]), int(fin_time[0])]
        month_bnds = [int(beg_time[1]), int(fin_time[1])]
        day_bnds = [int(beg_time[2]), int(fin_time[2])]


        # Get Timestamps for comparison:
        _h, _m, _s, _ms = self._time_from_decimal(init_decimal_time)
        initial_timestamp = datetime(year_bnds[0], month_bnds[0], day_bnds[0], _h, _m, _s, _ms)
        if self._verbose:
            print("**** Incoming time: ", start_time)
            print("initial_timestamp", initial_timestamp, 'from', init_decimal_time)
        #
        _h, _m, _s, _ms = self._time_from_decimal(fin_decimal_time)
        final_timestamp = datetime(year_bnds[1], month_bnds[1], day_bnds[1], _h, _m, _s, _ms)
        if self._verbose:
            print("final_timestamp", final_timestamp)


        #
        # initialize an observation object:
        coords = self._get_site_facility_coordinates(None, site_facility, None, init_decimal_date)
        obs_num_gates = DL_constants.DL_NUM_GATES
        obs = DL_SITE_OBS(t=start_time, site_facility=site_facility, dl_coordinates=coords, prog_var=prog_var, num_gates=obs_num_gates, elevations=None, azimuthes=None)
        # y = obs.data
        #
        return obs

    def parse_DL_raw_data(self, raw_file, parsed_file, raw_file_format='hpl', parsed_file_format='hdf', overwrite_parsed=False):
        """
        Parse raw-data file, and write into a predefined clean format
            If the code runs on the server where raw data reside, everything is done locally,
            otherwise, an attempt to connect to the remote server, and parse data remotely is carried out.
            Once parsing is done, (MAYBE) an attempt to remove raw data is made!

        Args:
            raw_file: raw-data file name/path (cwd is used if only file-name is given)
            parsed_file: parsed-data file name/path (cwd is used if only file-name is given)
            raw_file_format: format of the raw-data file;
                Currently supported are:
                - 'hpl': ASCCII format with extension '.hpl'
            parsed_file_format: format of the parsed file;
                Currently supported are:
                - 'hdf5': save the parsed data into hdf5 format with extension '.h5'

        Returns:
            status: True if the parsing process is completed successfully

        """
        # if parsed_file_format.lower() not in DL_DATA_HANDLER.__supported_DL_parsed_formats:
        #     print("Unsupported parsed_file_format: '%s' " % parsed_file_format)
        #     raise ValueError
        # else:
        #     pass

        if re.match(r'\Ahdf(-|_)*(5)*\Z', parsed_file_format, re.IGNORECASE):
            # parsing DL raw data into HDF5 file
            status = self.parse_DL_hdf(raw_file, parsed_file, raw_file_format=raw_file_format, overwrite=overwrite_parsed)
        else:
            print("Unsupported parsed_file_format: '%s' " % parsed_file_format)
            raise ValueError
        return status

    def parse_DL_hdf(self, raw_file, parsed_file, raw_file_format='hpl', header_size=17, load_to_mem=False, compression=None, overwrite=False):
        """
        Parse, and write contents of a DL raw Data file, in a given raw_file_format (currently 'hpl' is supported)
            to an HDF5 file.
            This is done by fully loading the file into memory (fast parsing but may cause memory overloading)
            - The following fields are saved as datasets in the hdf5 file:
                a) time: time instances (from base_time) at which observations are taken
                b) intensity: signal intensity; {signale-to-noise-ratio(SNR) + 1 }
                c) doppler: doppler frequency shift
                d) roll:
                e) pitch:
                f) elevation:
                g) azimuth:
            - The following attributes are returned writtedn well:
                a)  num_gates: number of range gates
                b) range_gate_length:
                c) base_time:
        Remark:
            If a file with name as in 'parsed_file' exists, it will be overwritten...

        Args:
            raw_file:
            parsed_file:
            raw_file_format:
            header_size: number of lines in the raw file header
            load_to_mem: load the raw file into memory (this will be fast, buy may overload memory if the file size is too large!)
            compression; 'gzip', etc.  this is something to discuss before incorporating it... (TODO)

        Returns:
            status: Falg True if the file successfully read, and written

        """
        if not self.__I_AM_HOST:  # TODO: I am still thinking about the optimal way of doing it; will refactor for sure
            print("Remote Parsing is to be implemented... ")
            # raise NotImplementedError("TODO")
            status = None
        # else:
        if True:  # TMP: Testig locally...
            # Running on the server side
            if load_to_mem:
                try:
                    status = self._parse_DL_hdf_byloading(raw_file, parsed_file, raw_file_format=raw_file_format, overwrite=overwrite, header_size=header_size, compression=compression)
                except(MemoryError):
                    print("Failed to dump the whole file to memory!\n Attempting to read the file slowly...")
                    status = self._parse_DL_hdf_byline(raw_file, parsed_file, raw_file_format=raw_file_format, overwrite=overwrite, header_size=header_size, compression=compression)
                    #
            else:
                status = self._parse_DL_hdf_byline(raw_file, parsed_file, raw_file_format=raw_file_format, overwrite=overwrite, header_size=header_size, compression=compression)
        return status
        #

    #
    #
    def _lookup_matching_files(self, start_time, end_time, site_facility, scan_type='all', parsed_file_format='hdf', lookup_rawfiles=False, raw_file_format='hpl'):
        """
        Look in the parsed (and optionally in the raw) data directory for files matching passed criteria,

        Args:
            start_time:
            end_time:
            site_facility:
            scan_type: if 'all', all scan types are included, otherwise, only defined scan type, e.g. 'stare' is seeked
            parsed_file_format:
            lookup_rawfiles:
        Returns:
            matching_parsed_files:
            matching_raw_files: will be empty if lookup_rawfiles is False

        """
        time_delim = ':'
        min_year = 2000
        #
        assert isinstance(start_time, (str, datetime)), "'start_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, but not %s" %type(start_time)
        assert isinstance(end_time, (str, datetime)), "'end_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, but not %s" %type(end_time)
        if isinstance(start_time, str):
            lst = start_time.split(time_delim)
            _inits = [str(min_year), '01', '01', '00', '00', '00', '00']  #
            if 1 <= len(lst)<= 6:
                lst += _inits[len(lst): ]
            elif len(lst) == len(_inits):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            _start_time = time_delim.join(lst)

        elif isinstance(start_time, datetime):
            _start_time = "%04d:%02d:%02d:%02d:%02d:%02d:%d" %(start_time.year, start_time.month, start_time.day, start_time.hour, start_time.minute, start_time.second, start_time.microsecond)
        else:
            print("'start_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(start_time))
            raise TypeError

        if isinstance(end_time, str):
            lst = end_time.split(time_delim)
            today = datetime.today()
            _fins = [str(i) for i in (today.year, today.month, today.day, today.hour, today.minute, today.second, today.microsecond)]
            if 1 <= len(lst)<= 6:
                lst += _fins[len(lst): ]
            elif len(lst) == len(_fins):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            _end_time = time_delim.join(lst)

        elif isinstance(end_time, datetime):
            _end_time = "%04d:%02d:%02d:%02d:%02d:%02d:%d" %(end_time.year, end_time.month, end_time.day, end_time.hour, end_time.minute, end_time.second, end_time.microsecond)
        else:
            print("'end_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(end_time))
            raise TypeError

        beg_time = _start_time.split(time_delim)
        fin_time = _end_time.split(time_delim)
        if not (6<=len(beg_time)<=7 and 6<=len(fin_time)<=7):
            print("The arguments: 'start_time', and 'end_time' must be strings with the following format: 'YYYY:MM:DD:hh:mm:ss:<ms>'\n Please check docstring of this function!")
            raise AssertionError
        else:
            pass

        init_decimal_date = int(''.join(beg_time[0:3]))  # YYYYMMDD
        init_decimal_time = self._str_to_decimal_time(time_delim.join(beg_time[3:]), delimeter=time_delim)  #
        #
        fin_decimal_date = int(''.join(fin_time[0:3]))  # YYYYMMDD
        fin_decimal_time = self._str_to_decimal_time(time_delim.join(fin_time[3:]), delimeter=time_delim)

        # time bounds
        year_bnds = [int(beg_time[0]), int(fin_time[0])]
        month_bnds = [int(beg_time[1]), int(fin_time[1])]
        day_bnds = [int(beg_time[2]), int(fin_time[2])]

        # Get Timestamps for comparison:
        _h, _m, _s, _ms = self._time_from_decimal(init_decimal_time)
        initial_timestamp = datetime(year_bnds[0], month_bnds[0], day_bnds[0], _h, _m, _s, _ms)
        if self._verbose:
            print("**** Incoming time: ", start_time)
            print("initial_timestamp", initial_timestamp, 'from', init_decimal_time)
        #
        _h, _m, _s, _ms = self._time_from_decimal(fin_decimal_time)
        final_timestamp = datetime(year_bnds[1], month_bnds[1], day_bnds[1], _h, _m, _s, _ms)
        if self._verbose:
            print("final_timestamp", final_timestamp)
        #

        # Look inside the parsed directory 'self.__local_parsed_data_path', and retrieve the data files
        local_parsed_path = self.__local_parsed_data_path
        if re.match(r'\Ahdf(-|_)*(5)*\Z', parsed_file_format, re.IGNORECASE):
            # parsing DL raw data into HDF5 file
            local_parsed_path = os.path.join(local_parsed_path, 'hdf')
        elif False:  # add other formats!
            pass
        else:
            print("Unsupported data_file_format: '%s' " % data_file_format)
            raise ValueError

        # Lookup the parsed data directory categorization:
        categ_lst = self.__data_directories_categorization.split(os.path.sep)

        #
        # Start retrieving and stacking data in data structures to be returned to the user,
        if not os.path.isdir(local_parsed_path):
            print("Is this the first time your run the Dl-data collector!")
            print("HINT; you need to synchronize with remote data repo, or manually copy observations to : %s " % local_parsed_path)
            return [], []
        else:
            # local_parsed_path exists, and should be able to look into it!
            pass

        parsed_dirs_list = [local_parsed_path]
        matching_parsed_files = []
        for categ in categ_lst:
            # print("\n\n\t ******* [%s] ******* " % categ)
            if re.match(r'\Ayear\Z', categ, re.IGNORECASE):
                #
                loc_dirs = []
                for fold in parsed_dirs_list:
                    subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                    for d in subdirs:
                        _, leaf = os.path.split(d)
                        # print("d, leaf: ", d, leaf)
                        if year_bnds[0] <= int(leaf) <= year_bnds[-1]:
                            loc_dirs.append(os.path.abspath(d))
                        else:
                            pass
                # print("Found loc_dirs:", loc_dirs)
                # update 'parsed_dirs_list'
                parsed_dirs_list = loc_dirs

                #
            elif re.match(r'\Amonth\Z', categ, re.IGNORECASE):
                #
                loc_dirs = []
                for fold in parsed_dirs_list:
                    subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                    for d in subdirs:
                        _, leaf = os.path.split(d)
                        # TODO: Double check this comparison
                        if month_bnds[0] <= month_bnds[-1]:
                            if month_bnds[0] <= int(leaf) <= month_bnds[-1]:
                                loc_dirs.append(os.path.abspath(d))
                        else:
                            if month_bnds[0] <= int(leaf) or int(leaf) <= month_bnds[-1]:
                                loc_dirs.append(os.path.abspath(d))
                # update 'parsed_dirs_list'
                parsed_dirs_list = loc_dirs
                #
            elif re.match(r'\Asite(-|_)*(name)*\Z', categ, re.IGNORECASE):
                #
                site, facility = site_facility.split('dl')

                loc_dirs = []
                for fold in parsed_dirs_list:
                    subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                    for d in subdirs:
                        _, leaf = os.path.split(d)
                        if re.match(r'\A%s\Z' % site, leaf, re.IGNORECASE):
                            loc_dirs.append(os.path.abspath(d))
                # update 'parsed_dirs_list'
                parsed_dirs_list = loc_dirs
                #
            elif re.match(r'\Afacility(-|_)*(name)*\Z', categ, re.IGNORECASE):
                #
                loc_dirs = []
                for fold in parsed_dirs_list:
                    subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                    for d in subdirs:
                        _, leaf = os.path.split(d)
                        if re.match(r'\A%s\Z' % facility, leaf, re.IGNORECASE):
                            loc_dirs.append(os.path.abspath(d))
                # update 'parsed_dirs_list'
                parsed_dirs_list = loc_dirs
            elif re.match(r'\A(site(-|_)*(dl)*facility|facility(-|_)*(dl)*site)\Z', categ, re.IGNORECASE):
                #
                loc_dirs = []
                for fold in parsed_dirs_list:
                    subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                    for d in subdirs:
                        _, leaf = os.path.split(d)
                        if re.match(r'\A%s\Z' % site_facility, leaf, re.IGNORECASE):
                            loc_dirs.append(os.path.abspath(d))
                # update 'parsed_dirs_list'
                parsed_dirs_list = loc_dirs
            elif re.match(r'\Ascan(-|_)*(type)*\Z', categ, re.IGNORECASE):
                pass
                # Add everything
                #
                loc_dirs = []
                for fold in parsed_dirs_list:
                    subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                    for d in subdirs:
                        _, leaf = os.path.split(d)
                        if re.match(r'\Astare\Z', scan_type, re.IGNORECASE):
                            if re.match(r'\A%s\Z' % scan_type, leaf, re.IGNORECASE):
                                loc_dirs.append(os.path.abspath(d))
                            else:
                                pass
                        else:
                            loc_dirs.append(os.path.abspath(d))
                # update 'parsed_dirs_list'
                parsed_dirs_list = loc_dirs
                #
            elif re.match(r'\A( )*\Z', categ, re.IGNORECASE):  # handles trailer separators
                pass
            else:
                print("Undefined categorization [%s] found in DL_constants: DL_DATA_CATEGORIZATION !" % categ, categ_lst, self.__data_directories_categorization)
                raise ValueError

        # print("Found directories with given categorization")
        # print(parsed_dirs_list)

        # Now it's time to loop over these directories, and get files withing the given timespan
        if re.match(r'\Ahdf(-|_)*(5)*\Z', parsed_file_format, re.IGNORECASE):
            # parsing DL raw data into HDF5 file
            parse_extension = 'h5'
        elif False:  # add other formats!
            pass
        else:
            print("Unsupported data_file_format: '%s' " % data_file_format)
            raise ValueError

        for d in parsed_dirs_list:
            loc_files = utility.get_list_of_files(d, recursive=False, return_abs=True)
            for f in loc_files:
                # print("Inspecting: ", f)
                head, tail = os.path.splitext(f)
                if not re.match(r"\A(.)*%s\Z" % parse_extension.lstrip('.') , tail.lstrip('.'), re.IGNORECASE):
                    continue
                else:
                    f_decim_date, _file_time = head.split('_')[-2: ]

                    if len(_file_time)<1:
                        print("File is bad! now time recorded in file: %s " %f)
                        raise ValueError
                    elif len(_file_time) <=2:
                        _h, _m, _s = int(_file_time), 0, 0

                    elif len(_file_time) <=4:
                        _h, _m, _s = int(_file_time[:2]), int(_file_time[2:]), 0
                    else:
                        _h, _m, _s = int(_file_time[:2]), int(_file_time[2:4]), int(_file_time[4:])
                    _ms = 0
                    f_timestamp = datetime(int(f_decim_date[:4]), int(f_decim_date[4:6]), int(f_decim_date[6:]), _h, _m, _s, _ms)
                    # print("Checking time:", _file_time, f_timestamp, initial_timestamp, final_timestamp)

                    if initial_timestamp <= f_timestamp <= final_timestamp:
                        # file initial time is between the given time points
                        matching_parsed_files.append(os.path.abspath(f))
                        # print("Caught...")
                    elif f_timestamp < initial_timestamp:
                        # file initial time is before the given time points; must open to see end time point
                        # print("Collecting data from: %s" % parsed_file)
                        with h5py.File(f, mode='r') as f_id:
                            #
                            # TODO: match attributes in file with passed parameters, and raise error if mismatch happens!

                            st_decim_date = str(f_id.attrs['stop_date'])
                            st_decim_time = f_id.attrs['stop_time_decimal']
                            #
                            _year = int(st_decim_date[: 4])
                            _month = int(st_decim_date[4:6])
                            _day = int(st_decim_date[6:])
                            _h, _m, _s, _ms = self._time_from_decimal(st_decim_time)
                            f_final_timestamp = datetime(_year, _month, _day, _h, _m, _s, _ms)
                        if f_final_timestamp>=initial_timestamp:
                            # print("Match found earlier in :", f)
                            matching_parsed_files.append(os.path.abspath(f))

        # Now, lookup raw files if requested
        matching_raw_files = []
        if lookup_rawfiles:
            # Look inside the Raw directory 'self.__local_raw_data_path', and retrieve the data files
            local_raw_path = self.__local_raw_data_path
            raw_dirs_list = [local_raw_path]
            for categ in categ_lst:
                # print("\n\n\t ******* [%s] ******* " % categ)
                if re.match(r'\Ayear\Z', categ, re.IGNORECASE):
                    #
                    loc_dirs = []
                    for fold in raw_dirs_list:
                        subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                        for d in subdirs:
                            _, leaf = os.path.split(d)
                            # print("d, leaf: ", d, leaf)
                            if year_bnds[0] <= int(leaf) <= year_bnds[-1]:
                                loc_dirs.append(os.path.abspath(d))
                            else:
                                pass
                    # print("Found loc_dirs:", loc_dirs)
                    # update 'raw_dirs_list'
                    raw_dirs_list = loc_dirs

                    #
                elif re.match(r'\Amonth\Z', categ, re.IGNORECASE):
                    #
                    loc_dirs = []
                    for fold in raw_dirs_list:
                        subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                        for d in subdirs:
                            _, leaf = os.path.split(d)
                            # TODO: Double check this comparison
                            if month_bnds[0] <= month_bnds[-1]:
                                if month_bnds[0] <= int(leaf) <= month_bnds[-1]:
                                    loc_dirs.append(os.path.abspath(d))
                            else:
                                if month_bnds[0] <= int(leaf) or int(leaf) <= month_bnds[-1]:
                                    loc_dirs.append(os.path.abspath(d))
                    # update 'raw_dirs_list'
                    raw_dirs_list = loc_dirs
                    #
                elif re.match(r'\Asite(-|_)*(name)*\Z', categ, re.IGNORECASE):
                    #
                    site, facility = site_facility.split('dl')

                    loc_dirs = []
                    for fold in raw_dirs_list:
                        subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                        for d in subdirs:
                            _, leaf = os.path.split(d)
                            if re.match(r'\A%s\Z' % site, leaf, re.IGNORECASE):
                                loc_dirs.append(os.path.abspath(d))
                    # update 'raw_dirs_list'
                    raw_dirs_list = loc_dirs
                    #
                elif re.match(r'\Afacility(-|_)*(name)*\Z', categ, re.IGNORECASE):
                    #
                    loc_dirs = []
                    for fold in raw_dirs_list:
                        subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                        for d in subdirs:
                            _, leaf = os.path.split(d)
                            if re.match(r'\A%s\Z' % facility, leaf, re.IGNORECASE):
                                loc_dirs.append(os.path.abspath(d))
                    # update 'raw_dirs_list'
                    raw_dirs_list = loc_dirs
                elif re.match(r'\A(site(-|_)*(dl)*facility|facility(-|_)*(dl)*site)\Z', categ, re.IGNORECASE):
                    #
                    loc_dirs = []
                    for fold in raw_dirs_list:
                        subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                        for d in subdirs:
                            _, leaf = os.path.split(d)
                            if re.match(r'\A%s\Z' % site_facility, leaf, re.IGNORECASE):
                                loc_dirs.append(os.path.abspath(d))
                    # update 'raw_dirs_list'
                    raw_dirs_list = loc_dirs
                elif re.match(r'\Ascan(-|_)*(type)*\Z', categ, re.IGNORECASE):
                    pass
                    # Add everything
                    #
                    loc_dirs = []
                    for fold in raw_dirs_list:
                        subdirs = utility.get_list_of_subdirectories(fold, ignore_root=True, return_abs=False, recursive=False)
                        for d in subdirs:
                            loc_dirs.append(os.path.abspath(d))
                    # update 'raw_dirs_list'
                    raw_dirs_list = loc_dirs
                    #
                elif re.match(r'\A( )*\Z', categ, re.IGNORECASE):  # handles trailer separators
                    pass
                else:
                    print("Undefined categorization [%s] found in DL_constants: DL_DATA_CATEGORIZATION !" % categ, categ_lst, self.__data_directories_categorization)
                    raise ValueError

            #
            # Now it's time to loop over these directories, and get files withing the given timespan
            if re.match(r'\Ahpl\Z', raw_file_format, re.IGNORECASE):
                raw_extension = 'hpl'
            elif False:  # add other formats!
                pass
            else:
                print("Unsupported data_file_format: '%s' " % data_file_format)
                raise ValueError


            for d in raw_dirs_list:
                loc_files = utility.get_list_of_files(d, recursive=False, return_abs=True)
                for f in loc_files:
                    head, tail = os.path.splitext(f)
                    if not re.match(r"\A(.)*%s\Z" % raw_extension.lstrip('.') , tail.lstrip('.'), re.IGNORECASE):
                        continue
                    else:
                        f_decim_date, _file_time = head.split('_')[-2: ]

                        if len(_file_time)<1:
                            print("File is bad! now time recorded in file: %s " %f)
                            raise ValueError
                        elif len(_file_time) <=2:
                            _h, _m, _s = int(_file_time), 0, 0

                        elif len(_file_time) <=4:
                            _h, _m, _s = int(_file_time[:2]), int(_file_time[2:]), 0
                        else:
                            _h, _m, _s = int(_file_time[:2]), int(_file_time[2:4]), int(_file_time[4:])
                        _ms = 0
                        f_timestamp = datetime(int(f_decim_date[:4]), int(f_decim_date[4:6]), int(f_decim_date[6:]), _h, _m, _s, _ms)
                        # print("Checking time:", _file_time, f_timestamp, initial_timestamp, final_timestamp)

                        if initial_timestamp <= f_timestamp <= final_timestamp:
                            matching_raw_files.append(os.path.abspath(f))
                            # print("Caught...")
                        elif (f_timestamp < initial_timestamp) and ((f_timestamp + timedelta(hours=24)) >= initial_timestamp):
                            matching_raw_files.append(os.path.abspath(f))
                        else:  # f_timestamp > initial_timestamp  ==> don't match
                            pass
                            #

            if self._verbose:
                print("\n\n\n\nFinal list of matching RAW files to be returned .....:")
                for i, f in enumerate(matching_raw_files):
                    print("Matching File [%d]: %s" %(i, f))

        return matching_parsed_files, matching_raw_files
    
    
    def get_remote_matching_raw_files(self, start_time, end_time, site_facility, scan_type='all', parsed_file_format='hdf'):
        """
        Same as get_remote_matching_parsed_files, but looks for raw files instead!

        """
        raise NotImplementedError("TODO....")


    def get_remote_matching_parsed_files(self, start_time, end_time, site_facility, scan_type='all', parsed_file_format='hdf', silent_mode=False):
        """

        """
        time_delim = ':'
        min_year = 2000
        #
        assert isinstance(start_time, (str, datetime)), "'start_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, but not %s" %type(start_time)
        assert isinstance(end_time, (str, datetime)), "'end_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, but not %s" %type(end_time)

        if isinstance(start_time, str):
            lst = start_time.split(time_delim)
            _inits = [str(min_year), '01', '01', '00', '00', '00', '00']  #
            if 1 <= len(lst)<= 6:
                lst += _inits[len(lst): ]
            elif len(lst) == len(_inits):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            _start_time = time_delim.join(lst)

        elif isinstance(start_time, datetime):
            _start_time = "%04d:%02d:%02d:%02d:%02d:%02d:%d" %(start_time.year, start_time.month, start_time.day, start_time.hour, start_time.minute, start_time.second, start_time.microsecond)
        else:
            print("'start_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(start_time))
            raise TypeError

        if isinstance(end_time, str):
            lst = end_time.split(time_delim)
            today = datetime.today()
            _fins = [str(i) for i in (today.year, today.month, today.day, today.hour, today.minute, today.second, today.microsecond)]
            if 1 <= len(lst)<= 6:
                lst += _fins[len(lst): ]
            elif len(lst) == len(_fins):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            _end_time = time_delim.join(lst)

        elif isinstance(end_time, datetime):
            _end_time = "%04d:%02d:%02d:%02d:%02d:%02d:%d" %(end_time.year, end_time.month, end_time.day, end_time.hour, end_time.minute, end_time.second, end_time.microsecond)
        else:
            print("'end_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(end_time))
            raise TypeError

        beg_time = _start_time.split(time_delim)
        fin_time = _end_time.split(time_delim)
        if not (6<=len(beg_time)<=7 and 6<=len(fin_time)<=7):
            print("The arguments: 'start_time', and 'end_time' must be strings with the following format: 'YYYY:MM:DD:hh:mm:ss:<ms>'\n Please check docstring of this function!")
            raise AssertionError
        else:
            pass

        init_decimal_date = int(''.join(beg_time[0:3]))  # YYYYMMDD
        init_decimal_time = self._str_to_decimal_time(time_delim.join(beg_time[3:]), delimeter=time_delim)  #
        #
        fin_decimal_date = int(''.join(fin_time[0:3]))  # YYYYMMDD
        fin_decimal_time = self._str_to_decimal_time(time_delim.join(fin_time[3:]), delimeter=time_delim)

        # time bounds
        year_bnds = [int(beg_time[0]), int(fin_time[0])]
        month_bnds = [int(beg_time[1]), int(fin_time[1])]
        day_bnds = [int(beg_time[2]), int(fin_time[2])]

        # Get Timestamps for comparison:
        _h, _m, _s, _ms = self._time_from_decimal(init_decimal_time)
        initial_timestamp = datetime(year_bnds[0], month_bnds[0], day_bnds[0], _h, _m, _s, _ms)
        if self._verbose:
            print("**** Incoming time: ", start_time)
            print("initial_timestamp", initial_timestamp, 'from', init_decimal_time)
        #
        _h, _m, _s, _ms = self._time_from_decimal(fin_decimal_time)
        final_timestamp = datetime(year_bnds[1], month_bnds[1], day_bnds[1], _h, _m, _s, _ms)
        if self._verbose:
            print("final_timestamp", final_timestamp)
        #

        # Look inside the parsed directory 'self.__local_parsed_data_path', and retrieve the data files
        local_parsed_path = self.__local_parsed_data_path
        if re.match(r'\Ahdf(-|_)*(5)*\Z', parsed_file_format, re.IGNORECASE):
            # parsing DL raw data into HDF5 file
            local_parsed_path = os.path.join(local_parsed_path, 'hdf')
        elif False:  # add other formats!
            pass
        else:
            print("Unsupported data_file_format: '%s' " % data_file_format)
            raise ValueError

        # Lookup the parsed data directory categorization:
        categ_lst = self.__data_directories_categorization.split(os.path.sep)

        if not self.__I_AM_HOST:
            # connect remote side, and find matching files; no copying is done
            if not use_paramiko:
                print("*"*50)
                print("Can't ping the remote side without paramiko currently")
                print("Consider installing paramiko package: e.g.,")
                print(" run --> pip install paramiko")
                print("*"*50)
                raise NotImplementedError
            else:
                if self.__remote_server_info is None:
                    print("self.__remote_server_info can't be None")
                    raise ValueError
                host_name = self.__remote_server_info['host_name']
                host_address = self.__remote_server_info['host_address']
                dl_data_repo_root_path = self.__remote_server_info['dl_data_repo_root_path']
                dl_data_raw_relative_path = self.__remote_server_info['dl_data_raw_relative_path']
                dl_data_parse_relative_path = self.__remote_server_info['dl_data_parse_relative_path']

                # Check if a private key should be used
                loc_key_path_file = ".private_key_file_path.dat"
                loc_key_path_file = os.path.join(os.path.dirname(__file__), loc_key_path_file)
                try:
                    with open(loc_key_path_file, 'r') as f_id:
                        privatekeyfile = f_id.read().strip()

                    if re.match(r'use( |_|-|=)*password', privatekeyfile, re.IGNORECASE):
                        pass
                    elif not os.path.isfile(privatekeyfile):
                        raise IOError
                except IOError:
                    privatekeyfile = None

                if privatekeyfile is None:
                    use_def_key = utility.query_yes_no("Do you with to Attempt using the default private key? ")
                    if use_def_key:
                        privatekeyfile = os.path.expanduser('~/.ssh/id_rsa')
                    else:
                        # No private key exist; try getting it, and storing it for future use
                        store_private_key = utility.query_yes_no("DO you want to store another private key for future use? ")
                        if store_private_key:
                            privatekeyfile = raw_input("Please input the path to the private key file you want to use: ")
                            if not os.path.isfile(privatekeyfile):
                                print("The private key file you entered is not a valid path to a file!")
                                raise IOError
                        else:
                            privatekeyfile = "USE PASSWORD"

                    with open(loc_key_path_file, 'w') as f_id:
                        f_id.write(privatekeyfile)
                else:
                    # privatekeyfile is not None; inspect it's value next
                    pass

                if re.match(r'use( |_|-|=)*password', privatekeyfile, re.IGNORECASE):
                    use_password = True
                    sec_pass = getpass.getpass("Input your %s password then hit Enter: " % host_address)
                    #
                elif os.path.isfile(privatekeyfile):
                    use_password = False
                    # just in case:
                    if not os.path.isfile(privatekeyfile):
                        print("The private key file [%s] is not a valid path to a file!" % privatekeyfile)
                        raise IOError
                    else:
                        pass
                else:
                    print("privatekeyfile holds unknown value %s ! Please either store a path to a private key; or store 'use password' in it!" %privatekeyfile)
                    try:
                        os.remove(loc_key_path_file)
                    except OSError:
                        pass
                    raise ValueError

                # Establish connection:
                host = '.'.join([host_name, host_address])
                client = paramiko.SSHClient()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                if use_password:
                    try:
                        client.load_system_host_keys()
                        client.connect(host, password=sec_pass)
                    except:
                        print("\n** Failed to setup connection with: %s; using the given password **\n" % host_address)
                        print("\n ** SORRY! ** \n\n")
                        raise
                else:
                    # Try using the saved RSA key:
                    try:
                        print("Connecting to %s ... " % host),
                        sys.stdout.flush()
                        # mykey = paramiko.RSAKey.from_private_key_file(privatekeyfile)
                        # client.connect(host, pkey=mykey)
                        client.load_system_host_keys(privatekeyfile)
                        client.connect(host)
                        print("Connection SuccessfullyEstablished.")
                    except paramiko.PasswordRequiredException:
                        try:
                            # One-time process to add the keys to trusted agents permenantly
                            print("Password is need; Trying to add the host to trusted agents; this is a one-time-process..."),
                            cmd = "ssh-add -K %s" % privatekeyfile
                            res = os.system(cmd)
                            if res:
                                print("Failed; Sorry...")
                                raise
                            else:
                                # perfect
                                client.load_system_host_keys(privatekeyfile)
                                client.connect(host)
                                print("Connection SuccessfullyEstablished.")

                        except paramiko.PasswordRequiredException:
                            print("\nConnection using stored RSA file without password failed; retrying with password...")
                            try:
                                print("The private key on file %s; requires a password" % privatekeyfile)
                                sec_pass = getpass.getpass("Input your %s password then hit Enter: " % host_address)
                                print("Connecting to %s ... " % host),
                                sys.stdout.flush()
                                mykey = paramiko.RSAKey.from_private_key_file(privatekeyfile)
                                client.connect(host, pkey=mykey, password=sec_pass)
                                print("Connection SuccessfullyEstablished.")
                            except paramiko.SSHException:
                                try:
                                    print("The intended howst was not found in known_hosts;\n Retrying without key on file...")
                                    sys.stdout.flush()
                                    print("Connecting to %s ... " % host),
                                    sys.stdout.flush()
                                    client.load_system_host_keys()
                                    client.connect(host, password=sec_pass)
                                    print("Connection SuccessfullyEstablished.")
                                    sys.stdout.flush()
                                    with open(loc_key_path_file, 'w') as f_id:
                                        f_id.write('use password')
                                except:
                                    print("Failed...")
                                    if os.path.isfile(loc_key_path_file):
                                        os.remove(loc_key_path_file)
                                    raise

                    else:
                        # connection successfule...
                        pass

                def sftp_walk(sftp, remotepath):
                    path=remotepath
                    files=[]
                    folders=[]
                    for f in sftp.listdir_attr(remotepath):
                        if S_ISDIR(f.st_mode):
                            folders.append(f.filename)
                        else:
                            files.append(f.filename)
                    if files:
                        yield path, files
                    for folder in folders:
                        new_path=os.path.join(remotepath,folder)
                        for x in sftp_walk(sftp, new_path):
                            yield x

                #
                if re.match(r'\Ahdf(-|_)*(5)*\Z', parsed_file_format, re.IGNORECASE):
                    # parsing DL raw data into HDF5 file
                    prs_frmt = 'h5'
                elif False:  # add other formats!
                    pass
                else:
                    print("Unsupported data_file_format: '%s' " % data_file_format)
                    raise ValueError

                # get list of files:
                match_remote_files = []
                sftp = paramiko.SFTPClient.from_transport(client.get_transport())
                slider_cnt = 0
                match_cnt = 0
                print("... Collecting parsed data from the data Host repo at %s ... \n ...This may take a few minutes...\n" % host_address),
                for path,files  in sftp_walk(sftp, os.path.join(dl_data_repo_root_path, dl_data_parse_relative_path)):
                    for file in files:
                        slider_cnt += 1
                        if re.match(r'\Ahdf(-|_)*(5)*\Z', parsed_file_format, re.IGNORECASE):
                            # parsing DL raw data into HDF5 file
                            prs_frmt = 'h5'
                            prs_dir_name = 'hdf'
                            if file.lower().endswith(prs_frmt):
                                pass
                            else:
                                continue
                        elif False:  # add other formats!
                            pass
                        else:
                            print("Unsupported data_file_format: '%s' " % data_file_format)
                            raise ValueError

                        # check if file matches the criteria:
                        rel_file_path_info = path.split(os.path.join(dl_data_repo_root_path, dl_data_parse_relative_path, prs_dir_name))[-1]
                        match = self._file_meet_criteria(os.path.join(rel_file_path_info.lstrip(os.path.sep), file), categ_lst, site_facility, initial_timestamp, final_timestamp)
                        if match:
                            match_remote_files.append(os.path.join(path,file))
                            match_cnt += 1
                        slider_cnt += 1
                        if not silent_mode:
                            print('\r %05d files match; out of %05d files inspected' % (match_cnt, slider_cnt)),
                            sys.stdout.flush()
                        else:
                            pass
                print("...Done >>> Syncing Maching files ...")

                # copy files:
                remote_parsed_dir = os.path.join(dl_data_repo_root_path, dl_data_parse_relative_path)
                local_parsed_dir = self.__local_parsed_data_path
                msg_len = 1
                for f_path in match_remote_files:
                    loc_path = f_path.replace(remote_parsed_dir, local_parsed_dir)
                    if not os.path.isfile(loc_path) or True:  # TODO: remove true or the whole condition
                        msg = "Syncing: %s " % loc_path
                        msg_len = max(msg_len, len(msg))
                        l_er = " "*msg_len
                        if not silent_mode:
                            print("\r%s\r" % l_er),
                            print(msg),
                        sys.stdout.flush()

                        f_d, f_n = os.path.split(loc_path)
                        if not os.path.isdir(f_d):
                            os.makedirs(f_d)
                        sftp.get(f_path, loc_path)
                print("\n...Syncing Dl-Data: Complete...")
                # close connections
                sftp.close()
                client.close()
                #
        else:
            pass
            #

    def _file_meet_criteria(self, rel_file_path_info, categ_lst, site_facility, initial_timestamp, final_timestamp, relaxed=False, relaxed_h_diff=1):
        ind = 0
        match = True
        rel_file_path_info_list = rel_file_path_info.split(os.path.sep)
        for categ in categ_lst:
            val = rel_file_path_info_list[ind]

            # print("\n\n\t ******* [%s] ******* " % categ)
            if re.match(r'\Ayear\Z', categ, re.IGNORECASE):
                if not initial_timestamp.year <= int(val) <= final_timestamp.year:
                    match = False
                #
            elif re.match(r'\Amonth\Z', categ, re.IGNORECASE):
                #
                if initial_timestamp.month <= final_timestamp.month:
                    if not initial_timestamp.month <= int(val) <= final_timestamp.month:
                        match = False
                else:
                    if not initial_timestamp.month <= int(val) or int(val) <= final_timestamp.month:
                        match = False
                #
            elif re.match(r'\Asite(-|_)*(name)*\Z', categ, re.IGNORECASE):
                #  TODO: Check the order: site, facility vs facility, site!
                site, _ = site_facility.split('dl')
                if not re.match(r'\A%s\Z' % site, val, re.IGNORECASE):
                    match = False
                #
            elif re.match(r'\Afacility(-|_)*(name)*\Z', categ, re.IGNORECASE):
                #
                _, facility = site_facility.split('dl')
                if not re.match(r'\A%s\Z' % facility, val, re.IGNORECASE):
                    match = False
                #
            elif re.match(r'\A(site(-|_)*(dl)*facility|facility(-|_)*(dl)*site)\Z', categ, re.IGNORECASE):
                #
                if not re.match(r'\A%s\Z' % site_facility, val, re.IGNORECASE):
                    match = False
                #
            elif re.match(r'\Ascan(-|_)*(type)*\Z', categ, re.IGNORECASE):
                pass
                #
            elif re.match(r'\A( )*\Z', categ, re.IGNORECASE):  # handles trailer separators
                pass
            else:
                print("Undefined categorization [%s] found in DL_constants: DL_DATA_CATEGORIZATION !" % categ, categ_lst, self.__data_directories_categorization)
                raise ValueError

            if not match:
                return match
            ind += 1

        # check file itself:
        head, tail = os.path.splitext(rel_file_path_info_list[ind])  # should be last thing now!

        f_decim_date, _file_time = head.split('_')[-2: ]

        if len(_file_time)<1:
            print("File is bad! now time recorded in file: %s " %f)
            raise ValueError
        elif len(_file_time) <=2:
            _h, _m, _s = int(_file_time), 0, 0

        elif len(_file_time) <=4:
            _h, _m, _s = int(_file_time[:2]), int(_file_time[2:]), 0
        else:
            _h, _m, _s = int(_file_time[:2]), int(_file_time[2:4]), int(_file_time[4:])
        _ms = 0
        f_timestamp = datetime(int(f_decim_date[:4]), int(f_decim_date[4:6]), int(f_decim_date[6:]), _h, _m, _s, _ms)
        # print("Checking time:", _file_time, f_timestamp, initial_timestamp, final_timestamp)

        if initial_timestamp <= f_timestamp <= final_timestamp:
            #match
            match = True
            # print("Caught...")
        elif f_timestamp < initial_timestamp:
            # match = False
            # TODO: There is the othe case where the end time in file is betweend initial and final time spans
            if relaxed:
                relax_window = timedelta(hours=h_diff)
            else:
                relax_window = final_timestamp - initial_timestamp
                #
            if initial_timestamp <= (f_timestamp + relax_window) <= final_timestamp:
                match = True
            else:
                match = False
                #
        else:  # f_timestamp > initial_timestamp  ==> don't match
            match = False
            #
        return match
        #


    def _read_DL_hdf_file(self, path, start_time=None,
                                      end_time=None,
                                      first_range_gate=None,
                                      last_range_gate=None,
                                      site_facility=None,
                                      fields=None,
                                      missing_as_nan=True):
        """
        Read contents of a DL-Data parsed file, in hdf5 format,
            and return the measurements collected in a specific time interval.
            The following attributes are returned as well:
            a)  num_gates: number of range gates
            b) range_gate_length:
            c) base_time:


            This carries out the following:
            --------------------------------
            a) Look for available data in the local data directory (specified in DL_constants.py)
            b) Prepare a list data pieces availablel locally (ll),
                and a list (lr) of data to retrieve from remote server (specified in DL_constants.py)
                + If the list (lr) is not empty a list of data to retrieve from remote server
                    (specified in DL_constants.py), Connect (via SSH) to the remote host, and
                    look inside the parsed-data directory for the required information,
                    - If parsed data segments are not ready, look for corresponding raw-data files,
                        and attempt to parse data,
                    - Once parsed data from lr is available, copy the parsed file
                        to the local data directory on which the DL_DATA_HANDLER is instantiated.
            c) combine the two lists (ll, and lr), and read them from the local directory.
                This way, we make sure we have data we use locally, and copy data only upon request.

            Remark:
            -------
                Make sure you maintain a list of raw data, and parsed data remotely, and locally!
                This method reads data from a single file given a date TODO: Specify the format of start and end time!

        Args:
            path: parsed data file (cwd is used if only file-name is given)
            start_time: initial time to collect observbations at (or after),
                if None, start from first time in file
            end_time: final time to collect observations at (or before),
                if None, read upto last time in file
            first_range_gate: first gate is 0, etc. Will start from 0 if None is passed
            last_range_gate: index (enumeration starting at 0) of the range after the last one to read;
                e.g. 7 if you want gates 0, 1, ..., 6. Will read till last gate if None is passed
            site_facility: name of the site_facility (e.g. C1, E32, etc.) to collect observation from;
                If None, observations from all site_facilitys are returned, (The RETURN FORMAT is to be considered)
            fields: a list of strings naming the fields to retrieve from the
                parsed data file (if parsing format is keyed such as hdf5).
                if None (or format doesn't support key-value), all fields are read and returned
            missing_as_nan: fill missing data with np.NaN?

        Returns:
            dl_data: a dictionary containing all required information, along with metainformation
                all returned elements in the dictionary are represented as numpy arrays (TODO: update this!)
            attrs: attributes associated witht the DL data contents

        """
        dl_data = dict()
        attrs = dict()
        #
        with h5py.File(path, mode='r') as hdf_file:
            if fields is None:
                # read everything,
                _loc_fields = hdf_file.keys()
            else:
                # Check for available fields
                pass
                # update after validation, and comparison against hdf_file.keys()
                # TODO: make sure all keys are same case in the saved hdf5 file
                _loc_fields = fields

            if len(_loc_fields) > 0:

                # Get timespan first:
                timespan = hdf_file['time'][...]

                #
                if start_time is None: start_time = timespan[0]
                if end_time is None: end_time = timespan[-1]
                #
                # Find indices in the timespan corresponding to start_time, and end_time
                initial_index = np.where(timespan >= start_time)[0][0]
                final_index = np.where(timespan <= end_time)[0][-1]  # this is the next index to collect actually from


                #
                # read fields from the , and add to output dictionary

                # TODO: get attributes (if stored): get all other attributes from the header template
                num_gates = None
                gate_range = None
                #

                # build attributes dictionary:
                for key, val in hdf_file.attrs.iteritems():
                    attrs.update({key: val})
                        #

                # Now, get Doppler values:
                for key in _loc_fields:
                    # check if key is a key to a dataset, or a group
                    # if it's a dataset, read it, otherwise recursively read it
                    #
                    field = str(key).strip(' ').lower()  # will be updated by recursion to check if key is key to a group vs a dataset
                    #
                    # slice everything as a numpy array
                    if re.match(r'\A(time|azimuth|elevation|pitch|roll)\Z', field, re.IGNORECASE ):
                        dl_data.update({key:hdf_file[field][initial_index: final_index]})
                        #
                    elif re.match(r'\A(intensity|doppler|back(_|-)*scatter)\Z', field, re.IGNORECASE ):
                        dl_data.update({key:hdf_file[field][initial_index: final_index, first_range_gate:last_range_gate]})  # slice everything as a numpy array
                    else:
                        print("Unknown Field %s!" %key)
                        pass
                        # raise ValueError

                    # TODO" Check for availability of all attributes the dictionary 'attrs':
                    pass

            else:
                dl_data = None

        return dl_data, attrs
        #

    def _parse_DL_hdf_byloading(self, raw_file, parsed_file, raw_file_format='hpl', overwrite=False, header_size=17, compression=None):
        """
        Parse, and write contents of a DL raw Data file, in a given raw_file_format (currently 'hpl' is supported)
            to an HDF5 file.
            This is done by fully loading the file into memory (fast parsing but may cause memory overloading)
            - The following fields are saved as datasets in the hdf5 file:
                a) time: time instances (from base_time) at which observations are taken
                b) intensity: signal intensity; {signale-to-noise-ratio(SNR) + 1 }
                c) doppler: doppler frequency shift
                d) roll:
                e) pitch:
                f) elevation:
                g) azimuth:
            - The following attributes are returned writtedn well:
                a)  num_gates: number of range gates
                b) range_gate_length:
                c) base_time:
        Remark:
            If a file with name as in 'parsed_file' exists, it will be overwritten...

        Args:
            raw_file:
            parsed_file:
            raw_file_format:
            header_size: number of lines in the raw file header
            compression; 'gzip', etc.  this is something to discuss before incorporating it... (TODO)

        Returns:
            status: Falg True if the file successfully read, and written

        """
        #
        if raw_file_format.lower() not in DL_DATA_HANDLER.__supported_DL_raw_formats:
            print("Raw File Format ''%s' is not supported!" % format)
            raise ValueError
        #
        if os.path.isfile(parsed_file) and not overwrite:
            print("A parsed file already exist. If you want to overwrite existing files; set argument 'overwtite' to 'True' \n Returning without exception")
            return False
        #
        # 1- Read header information, and extract/validate all attributes,
        with open(raw_file, mode='r') as file_obj:
            raw_file_txt = file_obj.readlines()

        # header:
        header_txt_list = raw_file_txt[: header_size]

        # File name in the header:
        raw_file_name = header_txt_list[0].split(':')[-1].strip()
        #
        print("Parsing DL-Data raw file '%s' " % raw_file_name)

        # Extract info from the header:
        if not header_txt_list[header_size-1].strip().startswith("****"):
            print("The header in file (%s) doesn't end at line %d as it is supposed to!" % (raw_file_name, header_size))
            raise ValueError

        # good to go with: line1, line2, etc...
        raw_filename = header_txt_list[0].split(':')[-1].strip()
        system_id = header_txt_list[1].split(':')[-1].strip()
        num_gates = int(header_txt_list[2].split(':')[-1].strip())
        range_gate_length = float(header_txt_list[3].split(':')[-1].strip())
        gate_length = float(header_txt_list[4].split(':')[-1].strip())
        pulses = int(header_txt_list[5].split(':')[-1].strip())
        num_rays = int(header_txt_list[6].split(':')[-1].strip())
        scan_type = header_txt_list[7].split(':')[-1].strip()
        focus_range = int(header_txt_list[8].split(':')[-1].strip())
        _start_time_stamp = ':'.join(header_txt_list[9].split(':')[1:])
        _start_time_stamp = _start_time_stamp.split(' ')
        start_date = int(_start_time_stamp[0].strip())
        start_time =  _start_time_stamp[1].strip()  # hdf5 doesn't support datetime, so I will keep it as a string
        start_time_decimal = self._str_to_decimal_time(start_time)
        resolution = float(header_txt_list[10].split(':')[-1].strip())

        # TODO: Is there a cheaper way!?
        num_lines = self._file_length(raw_file)
        num_time_points = (num_lines-header_size) / (num_gates+1)

        expec_num_lines = header_size + (num_gates+1)*num_time_points
        if num_lines > expec_num_lines:
            num_lines = expec_num_lines

        if self._verbose:
            sep = "*" * 60 + "\n"
            attr_msg = """ %s\t\t...Header Information...\n %s
            \r\traw_filename: %s
            \r\tsystem_id: %s
            \r\tnum_gates: %d
            \r\trange_gate_length: %d
            \r\tgate_length: %f
            \r\tpulses: %f
            \r\tnum_rays: %d
            \r\tscan_type: %s
            \r\tfocus_range: %f
            \r\tstart_date: %d
            \r\tstart_time: %s
            \r\tstart_time_decimal: %f
            \r\tresolution: %f \n\n%s
            """ % (sep, sep, raw_filename, system_id, num_gates, range_gate_length, gate_length, pulses, num_rays, scan_type, focus_range, start_date, start_time, start_time_decimal, resolution, sep)
            print(attr_msg)
            #
            print("\n%s\t\t...Additional MetaData... \n%s" % (sep, sep))
            print("num_lines: %d " % num_lines)
            print("num_time_points: %d " % num_time_points)
            print("\n%s\n Now Parsing DL-Data from %s \n " % (sep, raw_file_name))

        # 2- Create the hdf5 file, and update sequentially from the raw_file
        if not parsed_file.endswith('.h5'):
            parsed_file += '.h5'

        # Create the path of the file if it doesn't already exist (if called from outside...):
        head, tail = os.path.split(parsed_file)
        if not os.path.isdir(head) and len(head.strip(' '))>0:
            os.makedirs(head)

        with h5py.File(parsed_file, mode='w') as hdf_file:

            # Add Attributes:
            hdf_file.attrs['raw_filename'] = raw_filename
            hdf_file.attrs['system_id'] = system_id
            hdf_file.attrs['num_gates'] = num_gates
            hdf_file.attrs['range_gate_length'] = range_gate_length
            hdf_file.attrs['gate_length'] = gate_length
            hdf_file.attrs['pulses'] = pulses
            hdf_file.attrs['num_rays'] = num_rays  # this holds number of rays (or number of waypints in file based on scan_type)
            hdf_file.attrs['scan_type'] = scan_type
            hdf_file.attrs['focus_range'] = focus_range
            hdf_file.attrs['start_date'] = start_date
            hdf_file.attrs['start_time'] = start_time
            hdf_file.attrs['start_time_decimal'] = start_time_decimal
            hdf_file.attrs['resolution'] = resolution


            # # TODO: Add cite coordinates...  Saving it as string of zeros then we will updat hte hdf5 files
            coordinates = self._get_site_facility_coordinates(None, None, system_id, start_date)
            hdf_file.attrs['dl_coordinates'] = coordinates


            # Create datasets:
            intensity_ds = hdf_file.create_dataset('intensity', shape=(num_time_points, num_gates), dtype=np.float64)
            doppler_ds = hdf_file.create_dataset('doppler', shape=(num_time_points, num_gates), dtype=np.float64)
            backscatter_ds = hdf_file.create_dataset('backscatter', shape=(num_time_points, num_gates), dtype=np.float64)
            time_ds = hdf_file.create_dataset('time', shape=(num_time_points, ), dtype=np.float64)
            azimuth_ds = hdf_file.create_dataset('azimuth', shape=(num_time_points, ), dtype=np.float64)
            elevation_ds = hdf_file.create_dataset('elevation', shape=(num_time_points, ), dtype=np.float64)
            pitch_ds = hdf_file.create_dataset('pitch', shape=(num_time_points, ), dtype=np.float64)
            roll_ds = hdf_file.create_dataset('roll', shape=(num_time_points, ), dtype=np.float64)

            #
            # The main point of creating iteraotrs is to avoid dumping the file into memory
            # All the debugging checks can be removed for slight improvement of performance!
            #
            file_slice_iterator = range(header_size, num_lines, num_gates+1)
            for (time_ind, line_ind) in zip(range(num_time_points), file_slice_iterator):
                line = raw_file_txt[line_ind]
                l = [float(val.strip(' \x00 \r\n')) for val in line.split()]  # a list of numbers in a line
                time_ds[time_ind] = l[0]
                azimuth_ds[time_ind] = l[1]
                elevation_ds[time_ind] = l[2]
                pitch_ds[time_ind] = l[3]
                roll_ds[time_ind] = l[4]

            # Figure number of turnpoints in the current timespan (to get the number of midnight occurances --> number of days spanned in file)
            timespan = time_ds[...]  # this could be embeded in the loop above while reading data entries to save some more memory usage!
            _num_day_shifts = 0  # number of points where timespan crosses midnight (t-dt>t<d+dt)
            for t_ind in range(1, num_time_points):
                if timespan[t_ind] < timespan[t_ind-1]:  # look for turnpoints:
                    _num_day_shifts += 1

            # Add final time info to the attributes (extract from last l, and time datasets):
            final_time_decimal = l[0]
            stop_date = start_date
            try:
                for i in range(_num_day_shifts):
                    stop_date = self._next_day(stop_date)
            except(ValueError):
                print("Day Seems out of range for month!")
                stop_date = -1
            #
            hdf_file.attrs['stop_time_decimal'] = final_time_decimal
            hdf_file.attrs['stop_time'] = ':'.join((str(s) for s in self._time_from_decimal(final_time_decimal)[-1]))
            hdf_file.attrs['stop_date'] = stop_date
            #

            #
            __cntr = 0  # number of data lines successfully collected
            #
            # Data line 1: Decimal time (hours)  Azimuth (degrees)  Elevation (degrees) Pitch (degrees) Roll (degrees)
            with open(raw_file, mode='r') as file_obj:
                # iterate over the file lines at which data1 line exist:
                file_slice_iterator = itertools.islice(file_obj, header_size, num_lines, num_gates+1)

            #
            # iterate over the observations:
            # Data line 2: Range Gate  Doppler (m/s)  Intensity (SNR + 1)  Beta (m-1 sr-1)
            _local_place_holder = np.empty((num_gates, 3))  # reduces time to access the hdf5 file
            #
            # Skip the header:
            line_cntr = header_size
            # Read data-lines
            for time_ind in range(num_time_points):
                #
                _local_place_holder[...] = 0
                #
                line_cntr += 1  # Skip dataline 1
                # if (time_ind % 100 == 0):  # TODO: Remove after debugging
                #     print("ti = %d" %time_ind)
                for gate_ind in range(num_gates):
                    line = raw_file_txt[line_cntr]
                    if '\x00' in line:  # Null bytes exist and cause headache in Python
                        _line = ''.join(line.split('\x00'))
                        if len(_line) < 4:
                            if self._verbose:
                                print("Moving away from gate ", gate_ind, line)
                            continue
                        else:
                            pass
                    else:
                        _line = line
                    if self._verbose:
                        print("Parsing at time_ind, gate: line", time_ind, gate_ind, _line)
                    l = [float(v) for v in _line.split()[1:]]  # a list of numbers in a line
                    #
                    _local_place_holder[gate_ind, 0] = l[0]
                    _local_place_holder[gate_ind, 1] = l[1]
                    _local_place_holder[gate_ind, 2] = l[2]
                    #
                    line_cntr += 1
                    __cntr += 1
                    #
                #
                doppler_ds[time_ind, :] = _local_place_holder[:, 0]
                intensity_ds[time_ind, :] = _local_place_holder[:, 1]
                backscatter_ds[time_ind, :] = _local_place_holder[:, 2]
                #

        if __cntr == num_time_points * num_gates:
            status = True
        else:
            status = False
            sep = "\n" + "|^|_" * 20 + "\n"
            print("%sFailed to parse the file [%s] correctly!%s" % (sep, raw_file_name, sep))
            print("Expected Number of Time instances: %d " % num_time_points)
            print("Expected Number of range-gates: %d " % num_gates)
            print("Expected Number of data lines (number-of-gates x number-of-timepoints): %d " % num_lines)
            print("Collected Number of dta lines: %d " % __cntr)
            #
            print("%s\t...Cleaning-up attempted HDF5 file...%s" % (sep, sep))
            if os.path.isfile(parsed_file):
                os.remove(parsed_file)
            #
        return status

    def _parse_DL_hdf_byline(self, raw_file, parsed_file, raw_file_format='hpl', overwrite=False, header_size=17, compression=None):
        """
        Parse, and write contents of a DL raw Data file, in a given raw_file_format (currently 'hpl' is supported)
            to an HDF5 file.
            - The following fields are saved as datasets in the hdf5 file:
                a) time: time instances (from base_time) at which observations are taken
                b) intensity: signal intensity; {signale-to-noise-ratio(SNR) + 1 }
                c) doppler: doppler frequency shift
                d) roll:
                e) pitch:
                f) elevation:
                g) azimuth:
            - The following attributes are returned writtedn well:
                a)  num_gates: number of range gates
                b) range_gate_length:
                c) base_time:
        Remark:
            If a file with name as in 'parsed_file' exists, it will be overwritten...

        Args:
            raw_file:
            parsed_file:
            raw_file_format:
            header_size: number of lines in the raw file header
            compression; 'gzip', etc.  this is something to discuss before incorporating it... (TODO)

        Returns:
            status: Falg True if the file successfully read, and written

        """
        #
        if raw_file_format.lower() not in DL_DATA_HANDLER.__supported_DL_raw_formats:
            print("Raw File Format ''%s' is not supported!" % format)
            raise ValueError

        #
        if os.path.isfile(parsed_file) and not overwrite:
            print("A parsed file already exist. If you want to overwrite existing files; set argument 'overwtite' to 'True' \n Returning without exception")
            return None

        #
        # 1- Read header information, and extract/validate all attributes,
        header_txt_list = []
        with open(raw_file, mode='r') as file_obj:
            for line in itertools.islice(file_obj, 0, header_size):
                header_txt_list.append(line.strip('\n '))

        # File name in the header:
        raw_file_name = header_txt_list[0].split(':')[-1].strip()

        # Extract info from the header:
        if not header_txt_list[header_size-1].strip().startswith("****"):
            print("The header in file (%s) doesn't end at line %d as it is supposed to!" % (raw_file_name, header_size))
            raise ValueError

        #
        print("Parsing DL-Data raw file '%s' " % raw_file_name)

        # good to go with: line1, line2, etc...
        raw_filename = header_txt_list[0].split(':')[-1].strip()
        system_id = header_txt_list[1].split(':')[-1].strip()
        num_gates = int(header_txt_list[2].split(':')[-1].strip())
        range_gate_length = float(header_txt_list[3].split(':')[-1].strip())
        gate_length = float(header_txt_list[4].split(':')[-1].strip())
        pulses = int(header_txt_list[5].split(':')[-1].strip())
        num_rays = int(header_txt_list[6].split(':')[-1].strip())
        scan_type = header_txt_list[7].split(':')[-1].strip()
        focus_range = int(header_txt_list[8].split(':')[-1].strip())
        _start_time_stamp = ':'.join(header_txt_list[9].split(':')[1:])
        _start_time_stamp = _start_time_stamp.split(' ')
        try:
            start_date = int(_start_time_stamp[0].strip())
        except:
            print("header_txt_list: ", header_txt_list)
            print("\n\n")
            print("raw_filename: ", raw_filename)
            print("header_txt_list: ", header_txt_list)
            print("':'.join(header_txt_list[9].split(':') ... ", (':'.join(header_txt_list[9].split(':'))))
            print("header_txt_list[9]: ", header_txt_list[9])

            print("_start_time_stamp[0]: ", _start_time_stamp[0].strip())
            print("_start_time_stamp[0].strip(): ", _start_time_stamp[0].strip())
            print("_start_time_stamp: ", _start_time_stamp)
            raise
        start_time =  _start_time_stamp[1].strip()  # hdf5 doesn't support datetime, so I will keep it as a string
        start_time_decimal = self._str_to_decimal_time(start_time)
        resolution = float(header_txt_list[10].split(':')[-1].strip())

        # TODO: Is there a cheaper way!?
        num_lines = self._file_length(raw_file)
        num_time_points = (num_lines-header_size) / (num_gates+1)

        expec_num_lines = header_size + (num_gates+1)*num_time_points
        if num_lines > expec_num_lines:
            num_lines = expec_num_lines

        if self._verbose:
            sep = "*" * 60 + "\n"
            attr_msg = """ %s\t\t...Header Information...\n %s
            \r\traw_filename: %s
            \r\tsystem_id: %s
            \r\tnum_gates: %d
            \r\trange_gate_length: %d
            \r\tgate_length: %f
            \r\tpulses: %f
            \r\tnum_rays: %d
            \r\tscan_type: %s
            \r\tfocus_range: %f
            \r\tstart_date: %d
            \r\tstart_time: %s
            \r\tstart_time_decimal: %f
            \r\tresolution: %f \n\n%s
            """ % (sep, sep, raw_filename, system_id, num_gates, range_gate_length, gate_length, pulses, num_rays, scan_type, focus_range, start_date, start_time, start_time_decimal, resolution, sep)
            print(attr_msg)
            #
            print("\n%s\t\t...Additional MetaData... \n%s" % (sep, sep))
            print("num_lines: %d " % num_lines)
            print("num_time_points: %d " % num_time_points)
            print("\n%s\n Now Parsing DL-Data from %s \n " % (sep, raw_file_name))

        # 2- Create the hdf5 file, and update sequentially from the raw_file
        if not parsed_file.endswith('.h5'):
            parsed_file += '.h5'

        # Create the path of the file if it doesn't already exist (if called from outside...):
        head, tail = os.path.split(parsed_file)
        if not os.path.isdir(head) and len(head.strip(' '))>0:
            os.makedirs(head)

        with h5py.File(parsed_file, mode='w') as hdf_file:

            # Add Attributes:
            hdf_file.attrs['raw_filename'] = raw_filename
            hdf_file.attrs['system_id'] = system_id
            hdf_file.attrs['num_gates'] = num_gates
            hdf_file.attrs['range_gate_length'] = range_gate_length
            hdf_file.attrs['gate_length'] = gate_length
            hdf_file.attrs['pulses'] = pulses
            hdf_file.attrs['num_rays'] = num_rays  # this holds number of rays (or number of waypints in file based on scan_type)
            hdf_file.attrs['scan_type'] = scan_type
            hdf_file.attrs['focus_range'] = focus_range
            hdf_file.attrs['start_date'] = start_date
            hdf_file.attrs['start_time'] = start_time
            hdf_file.attrs['start_time_decimal'] = start_time_decimal
            hdf_file.attrs['resolution'] = resolution

            # # TODO: Add cite coordinates...  Saving it as string of zeros then we will updat hte hdf5 files
            coordinates = self._get_site_facility_coordinates(None, None, system_id, start_date)
            hdf_file.attrs['dl_coordinates'] = coordinates

            # Create datasets:
            intensity_ds = hdf_file.create_dataset('intensity', shape=(num_time_points, num_gates), dtype=np.float64)
            doppler_ds = hdf_file.create_dataset('doppler', shape=(num_time_points, num_gates), dtype=np.float64)
            backscatter_ds = hdf_file.create_dataset('backscatter', shape=(num_time_points, num_gates), dtype=np.float64)
            time_ds = hdf_file.create_dataset('time', shape=(num_time_points, ), dtype=np.float64)
            azimuth_ds = hdf_file.create_dataset('azimuth', shape=(num_time_points, ), dtype=np.float64)
            elevation_ds = hdf_file.create_dataset('elevation', shape=(num_time_points, ), dtype=np.float64)
            pitch_ds = hdf_file.create_dataset('pitch', shape=(num_time_points, ), dtype=np.float64)
            roll_ds = hdf_file.create_dataset('roll', shape=(num_time_points, ), dtype=np.float64)

            #
            # The main point of creating iteraotrs is to avoid dumping the file into memory
            # All the debugging checks can be removed for slight improvement of performance!
            #
            # Data line 1: Decimal time (hours)  Azimuth (degrees)  Elevation (degrees) Pitch (degrees) Roll (degrees)
            with open(raw_file, mode='r') as file_obj:
                # iterate over the file lines at which data1 line exist:
                file_slice_iterator = itertools.islice(file_obj, header_size, num_lines, num_gates+1)

                for (time_ind, line) in zip(range(num_time_points), file_slice_iterator):
                    try:
                        l = [float(val.strip(' \x00 \r\n')) for val in line.split()]  # a list of numbers in a line
                        time_ds[time_ind] = l[0]
                        azimuth_ds[time_ind] = l[1]
                        elevation_ds[time_ind] = l[2]
                        pitch_ds[time_ind] = l[3]
                        roll_ds[time_ind] = l[4]
                    except:
                        print("Failed with: ", [repr(v) for v in line.split()])
                        raise
            #

            # Figure number of turnpoints in the current timespan (to get the number of midnight occurances --> number of days spanned in file)
            timespan = time_ds[...]  # this could be embeded in the loop above while reading data entries to save some more memory usage!
            _num_day_shifts = 0  # number of points where timespan crosses midnight (t-dt>t<d+dt)
            for t_ind in range(1, num_time_points):
                if timespan[t_ind] < timespan[t_ind-1]:  # look for turnpoints:
                    _num_day_shifts += 1
                    if self._verbose:
                        print("Midnight hit:  t_ind, timespan[t_ind-1: t_ind+1]", t_ind, timespan[t_ind-1: t_ind+1])

            # Add final time info to the attributes (extract from last l, and time datasets):
            final_time_decimal = l[0]
            stop_date = start_date
            try:
                for i in range(_num_day_shifts):
                    stop_date = self._next_day(stop_date)
            except(ValueError):
                print("Day Seems out of range for month!")
                stop_date = -1
            #
            hdf_file.attrs['stop_time_decimal'] = final_time_decimal
            hdf_file.attrs['stop_time'] = ':'.join((str(s) for s in self._time_from_decimal(final_time_decimal)))
            hdf_file.attrs['stop_date'] = stop_date
            #

            #
            __cntr = 0  # number of data lines successfully collected
            #
            # iterate over the observations:
            # Data line 2: Range Gate  Doppler (m/s)  Intensity (SNR + 1)  Beta (m-1 sr-1)
            #

            #
            _local_place_holder = np.empty((num_gates, 3))  # reduces time to access the hdf5 file
            #
            with open(raw_file, mode='r') as file_obj:
                # Skip the header:
                line_ind = 0
                while line_ind < header_size:
                    file_obj.readline()
                    line_ind += 1
                #
                # Read data-lines
                for time_ind in range(num_time_points):
                    _local_place_holder[...] = 0
                    _ = file_obj.readline()  # pass to nextline
                    # if (time_ind % 100 == 0):  # TODO: Remove after debugging
                    #     print("ti = %d" %time_ind)
                    for gate_ind in range(num_gates):
                        line = file_obj.readline()
                        if '\x00' in line:  # Null bytes exist and cause headache in Python
                            _line = ''.join(line.split('\x00'))
                            if len(_line) < 4:
                                if self._verbose:
                                    print("Moving away from gate ", gate_ind, line)
                                continue
                            else:
                                pass
                        else:
                            _line = line
                        if self._verbose:
                            print("Parsing at time_ind, gate: line", time_ind, gate_ind, _line)
                        l = [float(v) for v in _line.split()[1:]]  # a list of numbers in a line
                        #
                        _local_place_holder[gate_ind, 0] = l[0]
                        _local_place_holder[gate_ind, 1] = l[1]
                        _local_place_holder[gate_ind, 2] = l[2]
                        #
                        __cntr += 1
                        #
                    #
                    doppler_ds[time_ind, :] = _local_place_holder[:, 0]
                    intensity_ds[time_ind, :] = _local_place_holder[:, 1]
                    backscatter_ds[time_ind, :] = _local_place_holder[:, 2]
                    #

        if __cntr == num_time_points * num_gates:
            status = True
        else:
            status = False
            sep = "\n" + "|^|_" * 20 + "\n"
            print("%sFailed to parse the file [%s] correctly!%s" % (sep, raw_file_name, sep))
            print("Expected Number of Time instances: %d " % num_time_points)
            print("Expected Number of range-gates: %d " % num_gates)
            print("Expected Number of data lines (number-of-gates x number-of-timepoints): %d " % num_lines)
            print("Collected Number of dta lines: %d " % __cntr)
            #
            print("%s\t...Cleaning-up attempted HDF5 file...%s" % (sep, sep))
            if os.path.isfile(parsed_file):
                os.remove(parsed_file)
            #

        return status
        #

    def _diagnose_DL_raw_data(self, file):
        """
        Access LIDAR raw data file, and check the file integrity;
            This includes:
            - header is complete,
            - header information match exactly what's in the data section
            - data files, don't contain Null bytes,

        Args:
            file: raw-data file name/path (cwd is used if only file-name is given)

        Returns:
            status:

        """
        raise NotImplementedError("TODO")

    def _raw_file_to_distination(self, path, overwrite=False, ignore_unknown_ext=True, ignore_unknown_patterns=True):
        """
        Given the file name or path, send (copy/move) the file from the passed location,
            to it's designated destination under the raw data directory;
                directories (and subdirectories) based on the categorization
                (in DL_constants.DL_DATA_CATEGORIZATION) will be created if don't exist
                >>> Extract file name, and based on pattern (year, month, site_info, scan_type),
                    send it to the right destination
        Args:
            path: path (rel or abs) to a file
            overwrite: overwrite existin raw files with the same data!

        Returns:
            target_file: absolute path of the written file

        """
        #
        # TODO: Add assertion
        if not os.path.isfile(path):
            print("Invalid file name/path: %s" % path)
            raise IOError

        # Extract files attributes:
        passed_abs_path = os.path.abspath(path)
        file_dir, file_name = os.path.split(passed_abs_path)  # head, tail <--
        #
        def _unrec_file_name_err(fle, raise_err=None):
            if raise_err is None:
                raise_err = not ignore_unknown_patterns
            print("Unrecognized file naming convention! Please update the code in DL_data_handler._raw_file_to_distination ?!!!")
            print("File: %s" %fle)
            if raise_err:
                raise ValueError
            else:
                return None
        #
        # TODO: Consider having a staticmethod that extracts all possible info from file name! However extraction from the parsed file is more reliable!
        if '.raw.' in file_name:
            # this is the format we have so far!
            # <site name>dl<facility>.<00>.<decimal_date>.<?????>.raw.<User or Scan type!!!>_<system_ID>_<decimal_date>_<inaccurate-time>.hpl
            lpart, rpart = file_name.split(".raw.")
            if len(lpart) == 0 or len(rpart) == 0:
                target_file = _unrec_file_name_err(file_name)
                return target_file
                #
            else:
                name, extension = os.path.splitext(file_name)
                if re.match('\A(.)*hpl\Z', extension, re.IGNORECASE):
                    # Correct extension (hpl)
                    rpart_list = rpart.strip('. hpl').split('_')
                    if len(rpart_list) != 4:
                        target_file = _unrec_file_name_err(file_name)
                        return target_file
                    else:
                        scan_type = rpart_list[0]  # this is not necessary correct; Stare, User5!!! Will be  validated while parsing... TODO: Talk to Paytsar
                        system_ID = rpart_list[1]
                        decimal_date = rpart_list[2]

                    lpart_list = lpart.split('.')
                    if len(lpart_list) != 4:
                        target_file = _unrec_file_name_err(file_name)
                        return target_file
                    else:
                        site_facility = lpart_list[0]
                        if 'dl' not in site_facility:
                            target_file = _unrec_file_name_err(file_name)
                            return target_file
                        site_name, facility = site_facility.split('dl')
                        if len(site_name.strip()) == 0:
                            site_name = 'unknown'
                        if len(facility.strip()) == 0:
                            facility = 'unknown'

                elif False:  # Add more extension
                    pass
                else:
                    if not ignore_unknown_ext:
                        print("Unsupported file convension/extension Found! '%s' !" % extension)
                        raise ValueError
                    else:
                        print("Unsupported file extension [%s] found... passing..." % extension)
                        return None

        elif False:
            pass
        else:
            target_file = _unrec_file_name_err(file_name)
            return target_file

        # File meta data needed are ready:
        # Specify the distination of the file:
        target_dir = self.__local_raw_data_path
        categ_lst = self.__data_directories_categorization.split(os.path.sep)

        for categ in categ_lst:
            if re.match(r'\Ayear\Z', categ, re.IGNORECASE):
                target_dir = os.path.join(target_dir, decimal_date[: 4])
            elif re.match(r'\Amonth\Z', categ, re.IGNORECASE):
                target_dir = os.path.join(target_dir, decimal_date[4: 6])
            elif re.match(r'\Asite(-|_)*(name)*\Z', categ, re.IGNORECASE):
                target_dir = os.path.join(target_dir, site_name)
            elif re.match(r'\Afacility(-|_)*(name)*\Z', categ, re.IGNORECASE):
                target_dir = os.path.join(target_dir, facility)
            elif re.match(r'\A(site(-|_)*(dl)*facility|facility(-|_)*(dl)*site)\Z', categ, re.IGNORECASE):
                target_dir = os.path.join(target_dir, site_facility)
            elif re.match(r'\Ascan(-|_)*(type)*\Z', categ, re.IGNORECASE):
                target_dir = os.path.join(target_dir, scan_type)
            elif re.match(r'\A( )*\Z', categ, re.IGNORECASE):  # handles trailer separators
                pass
            else:
                print("Undefined categorization [%s] found in DL_constants: DL_DATA_CATEGORIZATION !" % categ, categ_lst, self.__data_directories_categorization)
                raise ValueError

        #
        # Now 'target_dir' is an absolutepath containing all specified categorizations
        #

        # Create directory (and intermediate ones) if it doesn't exist
        if not os.path.isdir(target_dir):
            os.makedirs(target_dir)
        else:
            pass

        #
        target_file_name = file_name  # No need to change it actually!
        target_file = os.path.join(target_dir, target_file_name)

        if not overwrite and os.path.isfile(target_file):
            print("Found matching exisiting raw file: %s" % target_file)
        else:
            shutil.move(passed_abs_path, target_file)

        # return the full path of the distributed file (distination)
        return target_file
        #


    #
    #
    @staticmethod
    def _obs_extractor(y, elevations, azimuths, range_gates=None, altitudes=None, scan_type=None, vad_scan_degree=None, missing_as=None, parsed_file_format='hdf'):
        """
        This is a local function to avoid code redundancy; maybe to be refactored!
        if elevations, azimuths, range_gates, altitudes are all None return DL_SITE_OBS instance,
        otherwise numpy array corresponding to
        """
        if range_gates is altitudes is None:
            print("You must pass either range gates, or altitudes")
            raise ValueError

        # TODO: Refactor; especially the return type/value
        if missing_as is None:
            missing_as = np.nan
        elif isinstance(missing_as, str):
            if re.match(r'\A(np|numpy)*(.)*nan\Z', missing_as, re.IGNORECASE):
                missing_as = np.nan
        elif np.isscalar(missing_as):
            pass
        else:
            print("missing_as must be either a 'nan' string, or a numeric value! Received: type<%s>" % type(missing_as))
            raise TypeError

        if not np.isnan(missing_as):
            y.fill_nan_with(_val)

        # Now, extract specific observations if a criterion (or more) is (are) passed
        if re.match(r'\A(all|any)\Z', scan_type, re.IGNORECASE):
            scan_type = None
        if scan_type is not None:
            if re.match(r'\A(stare|vert)\Z', scan_type, re.IGNORECASE):
                elevations = [90]
                azimuths = [90]
            elif re.match(r'\Avad\Z', scan_type, re.IGNORECASE):
                elevations = np.asarray([vad_scan_degree]).flatten()
                elevations = elevations.tolist()
            else:
                print("Unknown scan_type '%s' !" % scan_type)
                raise ValueError

        if False:
            print("Inside _obs_extractor")
            print("Looking for :")
            print("elevations: ", elevations)
            print("azimuths: ", azimuths)
            print("range_gates: ", range_gates)
            print("altitudes: ", altitudes)
            print("scan_type: ", scan_type)
            print("vad_scan_degree: ", vad_scan_degree)
            print("y.get_val(90, 90, y._altitude_to_gate_index(9405.0): ", y.get_val(90, 90, y._altitude_to_gate_index(15)))

        #
        coord, obs = y.extract_entries(elevations=elevations, azimuths=azimuths, gates=range_gates, altitudes=altitudes)
        #
        return coord, obs
        #

    @staticmethod
    def _raw_path_to_parsed(raw_path, parse_format):
        """ Convert a raw path to parased path (search for last occurances of 'raw relative path name') """
        if not DL_constants.DL_DATA_RAW_RELATIVE_PATH in raw_path:
            print("The passed path is not recognized as raw data path!")
            raise ValueError
        #
        # replace final 'raw' subdirectory
        splitter = os.path.sep + DL_constants.DL_DATA_RAW_RELATIVE_PATH.strip(os.path.sep) + os.path.sep
        splitted = raw_path.rsplit(splitter)
        joiner = os.path.join(DL_constants.DL_DATA_PARSED_RELATIVE_PATH, parse_format)
        joiner = os.path.sep + joiner.strip(os.path.sep) + os.path.sep
        parsed_path = joiner.join(splitted)
        #
        # replace '.raw.' with '.rarsed.' % consider making this case insensitive
        splitter = '.raw.'
        joiner = '.parsed.'
        splitted = parsed_path.rsplit(splitter)
        parsed_path = joiner.join(splitted)
        #
        return parsed_path
        #

    @staticmethod
    def _get_site_facility_coordinates(system_name, site_facility, system_id, decimal_date):
        """
        Given the site_facility name (e.g. C1, E32, etc.), and the system ID, and the decimal_date (YYYYMMDD),

        Args:
            site_facility:
            system_ID: integer or string representation of the system ID, i.e. '07' and 7 are treated equally
            decimal_date: ould be a string or an integer representation of date of formt YYYYMMDD

        Returns:
            coordinates: a tuple with (latitude[deg North], longitude[deg East], altitude[m MSL]) of the given site_facility information

        Raises:
            ValueError: If the passed info do not match

        """
        # save stuff
        return "+00.000000, +00.000000, +00.000000"
        # TODO: Discuss this with Paytsar, and Chris!
        # #
        _dec_date = str(decimal_date)
        _year = int(_dec_date[: 4])
        _month = int(_dec_date[4:6])
        _day = int(_dec_date[6:])
        _h, _m, _s = 1, 0, 0
        in_date = datetime(_year, _month, _day, _h, _m, _s)
        #
        latitude = None
        longitude = None
        altitude = None
        #
        return(latitude, longitude, altitude)

    @staticmethod
    def _next_day(decimal_date):
        """
        Find the next day in decimal format (YYYYMMDD), and account for calender.
            'decimal_date' could be a string or an integer.

        Args:
            decimal_date: ould be a string or an integer representation of date of formt YYYYMMDD

        Returns:
            new_decim_date: add one day (accounting for calender) to the passed day, and returned on the same type (str/int)
        """
        _dec_date = str(decimal_date)
        _year = int(_dec_date[: 4])
        _month = int(_dec_date[4:6])
        _day = int(_dec_date[6:])
        cur_date = datetime(_year, _month, _day, 1, 0, 0)
        _next_date = cur_date + timedelta(days=1)
        new_decim_date = "%4d%02d%02d" % (_next_date.year, _next_date.month, _next_date.day)

        if isinstance(decimal_date, int):
            new_decim_date = int(new_decim_date)
        elif isinstance(decimal_date, str):
            pass
        else:
            print("decimal_date has to be either an integer or a string! not %s" % type(decimal_date))
            raise TypeError
        return new_decim_date

    @staticmethod
    def _null_byte_strip(txt):
        l = txt.split('\x00 ')
        out = ''.join(l)
        return out

    @staticmethod
    def _file_length(txtfile):  # TODO: timeit...
        """
        Get the number of lines in a file
        This is intended only fro ASCII-like files
        """
        with open(txtfile) as f:
            for i, _ in enumerate(f):
                pass
        return i + 1

    @staticmethod
    def _time_from_decimal(decimal_time):
        """
        return a tuple of the form (hours, minutes, seconds) corresponding to the passed decimal time (string or scalar)
        return (h, m, s, ms): (hour, minutes, seconds, microseconds)
        """
        if isinstance(decimal_time, str):
            _dec_time = int(decimal_time)
        elif np.isscalar(decimal_time):
            _dec_time = decimal_time
        else:
            print("Unsupported DataType %s " % type(decimal_time))
            raise TypeError
        #
        h = int(_dec_time)
        m = int((_dec_time * 60) % 60)
        s = np.round((_dec_time * 3600) % 60, 2)

        if s == 60:
            s = 0
            m +=1
        if m == 60:
            m = 0
            h += 1
        #
        int_s = int(s)
        ms = int((s - int_s) * 1e6 )
        s = int_s
        return (h, m, s, ms)

    @staticmethod
    def _str_to_decimal_time(time_string, delimeter=':'):
        """
        convert a string representation of time h:m:s or h:m:s:ms into a scalar representation of the time (as hours)
        """
        assert isinstance(time_string, str)
        lst = time_string.split(delimeter)
        if 3 <= len(lst) <= 4:
            pass
        else:
            print("timestring must be of the format: 'hh:mm:ss' or 'hh:mm:ss:ms' ")
            raise ValueError

        h = int(lst[0])
        m = int(lst[1])
        s_f = float(lst[2])
        if int(s_f) == s_f:
            s = int(s_f)
            ms = 0
        else:
            s = int(s_f)
            ms = int(1e+6 * (s_f - s))
        #
        if len(lst) == 4:
            ms += int(lst[3])

        dec_time = h + (m/60.0) + (s/3600.0) + (ms * 1e-6/3600.0)
        return dec_time

    @staticmethod
    def _timestamp_to_str(timestamp, delimeter=':'):
        """
        convert a string representation of time h:m:s or h:m:s:ms into a scalar representation of the time (as hours)
        """
        assert isinstance(timestamp, datetime)
        time_str = delimeter.join([str(s) for s in [timestamp.year, timestamp.month, timestamp.day, timestamp.hour, timestamp.minute, timestamp.second, timestamp.microsecond]])
        return time_str


#
if __name__ == "__main__":

    # Create a DL_DATA_HANDLER OBJECT:
    dl_obj = DL_DATA_HANDLER(verbose=False)

    #
    # Some Tests (after debugging, will be moved to special driver files (with polishing of course)
    if False:
        # test parsing...
        # filename = 'sgpdlC1.00.20150417.050502.raw.VAD_07_20150417_010142.hpl.hlp'
        filename = 'sgpdlC1.00.20150417.200502.raw.Stare_07_20150417_19.hpl'  # 25-MB
        #
        # These files crash Julia code
        # filename = 'sgpdlC1.00.20151203.090502.raw.User5_07_20151202_231700.hpl'  # 499-MB
        # filename = 'sgpdlC1.00.20151201.090003.raw.User5_07_20151201_090000.hpl'  # 104-MB
        # filename = 'sgpdlC1.00.20170831.220501.raw.User5_107_20170831_210015.hpl'  # 50MB

        dl_obj = DL_DATA_HANDLER(verbose=False)
        #

        startTime = datetime.now()
        status = dl_obj.parse_DL_hdf(filename, 'out.h5')
        print("Success! ", status)
        print("Time with second branch: (m, s, m.s.)", datetime.now() - startTime)


    if False:  # Debug creating observation data structure
        dt_m =  1 # minutes  (Get observations within dt minutes from start_time) (observations are averaged )
        stare_gate_2 = []
        times = []
        vad_gate_3 = []
        for h in range(7, 9):
            for m in range(0, 54, dt_m):
                start_time = '2017:06:04:%02d:%02d:00' % (h, m)
                end_time = '2017:06:04:%02d:%02d:00' % (h, m+dt_m)
                print("\n\n > New observation vector at date/time: %s" % start_time)
                times.append(start_time)
                y = dl_obj.aggregate_DL_data(start_time=start_time, end_time=end_time, site_facility='sgpdlE32')
                stare_gate_2.append(y.get_val(elevation=90, azimuth=90, gate=2, mismatch_policy='nearest'))
                vad_gate_3.append(y.get_val(elevation=60, azimuth=0, gate=3, mismatch_policy='nearest'))
                # print("y.data: ", y.data, y.get_local_grid())
                #
        print("Stare observations at gate 2 over preset timespan is:", stare_gate_2)
        print("VAD observations at gate 3 over preset timespan is:", vad_gate_3)


    if True:
        # This tries to parse everything in 'Incoming' directory (recursively)
        dist_list, parsed_list = dl_obj.process_incoming_raw_data(incoming_path=None,
                                                                  recursive=True,
                                                                  parse=True,
                                                                  keep_incoming=True,
                                                                  keep_extracted_raw=True,
                                                                  overwrite_raw=False,
                                                                  raw_format='hpl',
                                                                  parse_format='hdf',
                                                                  overwrite_parsed=False,
                                                                  extract_dir_warn=True)

        with open('handled_files.txt', 'w') as f:
            f.write("  Distributed List of files:\n %s \n" %('='*30))
            for i, fname in enumerate(dist_list):
                f.write("%04d - %s \n" % (i, fname))

            f.write("%s  Parsed List of files:\n %s \n" %('\n'*3, '='*30))
            for i, fname in enumerate(parsed_list):
                f.write("%04d - %s \n" % (i, fname))
