import os

def create_plots_dir(dir_name='__PLOTS', overwrite=True):
    """
    """
    if dir_name is None:
        dir_name = '__PLOTS'
    elif isinstance(dir_name, str):
        if len(dir_name.strip()) == 0:
            dir_name = '__PLOTS'
    else:
        print("dir_name must be a valid string, or pass None for a default name!")
        raise TypeError
    #
    if os.path.isabs(dir_name):
        plots_path = dir_name
    else:
        plots_path = os.path.abspath(dir_name)

    if (os.path.isdir(plots_path) and (not overwrite)):
        print("Existing results directory: %s ; Skipping as requested... " % plots_path)
        plots_path = None
    else:
        if os.path.isdir(dir_name):
            shutil.rmtree(dir_name)
        os.makedirs(plots_path)
    return plots_path

def create_output_dir(path=None, remove_existing=True):
    """
    Create a directory for plotting;
    """
    if path is None:
        dir_name = dir_name = '__Plots'
    else:
        dir_name = path
    if os.path.isdir(dir_name):
        if remove_existing:
            # Remove existing one (with all contents)
            shutil.rmtree(dir_name)
            # Now create after removeing it
            os.makedirs(dir_name)
        else:
            pass  # Don't remove the directory if exists
    else:
        os.makedirs(dir_name)
    return dir_name

def altitude_to_gate_index(altitude, range_gate_length=30):
    """ """
    index = int( altitude / range_gate_length - 0.5 )
    return index

def gate_index_to_altitude(index, range_gate_length=30):
    altitude = (index + 0.5) * range_gate_length
    return altitude

def recreate_forward_model(output_dir_structure_file):
    """
    Lookup results directory, and recreate the forward operator
    """
    output_dir_strucutre = get_output_dir_structure(output_dir_structure_file, full_paths=True)
    file_output_dir  = output_dir_strucutre['file_output_dir']
    model_states_dir = output_dir_strucutre['model_states_dir']
    observations_dir = output_dir_strucutre['observations_dir']
    statistics_dir   = output_dir_strucutre['statistics_dir']
    cycle_prefix     = output_dir_strucutre['cycle_prefix']

    # get model and observation configs, and create a forward model instance:
    model_configs_file = os.path.join(file_output_dir, 'setup.dat')
    model_configs, obs_configs = read_model_configs(model_configs_file)
    model = ForwardOperator(model_configs, obs_configs, dist_unit='M')
    return model

def str2bool(v):
    """
    Convert a string to a boolean (if the string makes sense!, otherwise ValueError is raised)
    """
    if v.lower().strip() in ('yes', 'true', 't', 'y', '1'):
        val = True
    elif v.lower().strip() in ('no', 'false', 'f', 'n', '0'):
        val = False
    else:
        print("Boolean Value is Expected Yes/No, Y/N, True/False, T/F, 1/0")
        raise ValueError
    return val

