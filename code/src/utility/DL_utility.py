
import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    raw_input = input
else:
    range = xrange

import shutil
import zipfile

import pickle
from sys import exit as terminate
from time import time

import matplotlib.dates as mdates

import datetime
import calendar

import numpy as np
from scipy.interpolate import griddata
import re
if python_version >= (3, 0, 0):
    import configparser
else:
    import ConfigParser as configparser

try:
    from geopy import distance as geopy_distance
except(ImportError):
    geopy_distance = None

import urllib
if python_version >= (3, 0, 0):  # Python3.x vs Python2.x
    import urllib.request as urllib_request
else:
    import urllib2 as urllib_request

import plotters
import collection


__BASE_DATE = datetime.datetime(1990, 1, 1, 0,0,0)


# ************************* #
# ******   Classes   ****** #
# ************************* #

class TIME_ITERATOR:
    def __init__(self, start_time, end_time, delta_time, min_year=1990, return_string=True, time_delim=':'):
        """
        A generator class to help creating efficient timespans

        Args:
            start_time:
            end_time:
            delta_time:
            min_year:
            return_string:
            time_delim:

        """
        assert isinstance(start_time, (str, datetime.datetime)), "'start_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, but not %s" %type(start_time)
        assert isinstance(end_time, (str, datetime.datetime)), "'end_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, but not %s" %type(end_time)
        assert isinstance(delta_time, (str, datetime.timedelta)), "delta_time must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime.deltatime, but not %s" %type(delta_time)
        self._time_delim = time_delim
        if isstring(start_time):
            start_time = validate_string(start_time, return_type='str')
            lst = start_time.split(self._time_delim)
            _inits = [str(min_year), '01', '01', '00', '00', '00', '00']  #
            if 1 <= len(lst)<= 6:
                lst += _inits[len(lst): ]
            elif len(lst) == len(_inits):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            start_time = self._time_delim.join(lst)

        elif isinstance(start_time, datetime.datetime):
            pass
            # start_time = "%04d:%02d:%02d:%02d:%02d:%02d:%d" %(start_time.year, start_time.month, start_time.day, start_time.hour, start_time.minute, start_time.second, start_time.microsecond)
        else:
            print("'start_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(start_time))
            raise TypeError

        # Format checking and correction
        if isstring(end_time):
            end_time = validate_string(end_time, return_type='str')
            lst = end_time.split(self._time_delim)
            today = datetime.datetime.today()
            _fins = [str(i) for i in (today.year, today.month, today.day, today.hour, today.minute, today.second, today.microsecond)]
            if 1 <= len(lst)<= 6:
                lst += _fins[len(lst): ]
            elif len(lst) == len(_fins):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            end_time = self._time_delim.join(lst)

        elif isinstance(end_time, datetime.datetime):
            pass
            # end_time = "%04d:%02d:%02d:%02d:%02d:%02d:%d" %(end_time.year, end_time.month, end_time.day, end_time.hour, end_time.minute, end_time.second, end_time.microsecond)
        else:
            print("'end_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(end_time))
            raise TypeError

        if isstring(delta_time):
            delta_time = validate_string(delta_time, return_type='str')
            lst = delta_time.split(self._time_delim)
            _deltas = ['0000', '00', '00', '00', '00', '00', '00']  #
            if 1 <= len(lst)<= 6:
                lst += _deltas[len(lst): ]
            elif len(lst) == len(_deltas):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/data format!")
                raise ValueError
            delta_time = self._time_delim.join(lst)

            # TODO: Check if delta_time is all zero as this is an infinit loop!

        elif isinstance(delta_time, datetime.timedelta):
            pass
            #
        else:
            print("'delta_time' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.timedelta, and not %s" %type(delta_time))
            raise TypeError

        self._out_str = return_string
        #

        # Initialization
        if isstring(start_time):
            start_time = validate_string(start_time, return_type='str')
            lst = start_time.split(self._time_delim)
            Y, M, D, h, m, s, ms = [int(t) for t in lst]
            self._start_time = datetime.datetime(Y, M, D, h, m, s, ms)
        else:
            self._start_time = start_time
        if isstring(end_time):
            end_time = validate_string(end_time, return_type='str')
            lst = end_time.split(self._time_delim)
            Y, M, D, h, m, s, ms = [int(t) for t in lst]
            self._end_time = datetime.datetime(Y, M, D, h, m, s, ms)
        else:
            self._end_time = end_time
        if isstring(delta_time):
            delta_time = validate_string(delta_time, return_type='str')
            lst = delta_time.split(self._time_delim)
            Y, M, D, h, m, s, ms = [int(t) for t in lst]
            self._delta_time = datetime.timedelta(hours=h, minutes=m, seconds=s, microseconds=ms)
            self._delta_date = (Y, M, D)
        else:
            self._delta_time = delta_time
            self._delta_date = (0, 0, 0)
        #
        self._t = self._start_time
        #

    def __iter__(self):
        return self

    def next(self):  # for Python2
        return self.__next__()

    def __next__(self):  # for python3
        if self._t < self._end_time:
            # Extract current time
            t = self._t

            # Increment time
            if self._delta_date[0] !=0:
                self._increment_months(12*self._delta_date[0])  # add a year
            if self._delta_date[1] !=0:
                self._increment_months(self._delta_date[1])  # add months
            if self._delta_date[2] !=0:
                self._t += datetime.timedelta(days=self._delta_date[2]) # add days

            self._t += self._delta_time  # add hours, minutes, seconds, microseconds

            # return current time
            if self._out_str:
                return "%04d:%02d:%02d:%02d:%02d:%02d:%d" % (t.year, t.month, t.day, t.hour, t.minute, t.second, t.microsecond)
            else:
                return t
        else:
            raise StopIteration()

    def whats_next(self):
        if self._t < self._end_time:
            # Extract current time
            t = self._t

            # Increment time
            if self._delta_date[0] !=0:
                self._increment_months(12*self._delta_date[0])  # add a year
            if self._delta_date[1] !=0:
                self._increment_months(self._delta_date[1])  # add months
            if self._delta_date[2] !=0:
                self._t += datetime.timedelta(days=self._delta_date[2]) # add days

            t += self._delta_time  # add hours, minutes, seconds, microseconds

            # return current time
            if self._out_str:
                return "%04d:%02d:%02d:%02d:%02d:%02d:%d" % (t.year, t.month, t.day, t.hour, t.minute, t.second, t.microsecond)
            else:
                return t
        else:
            raise StopIteration()

    def _increment_months(self, months):
        month = self._t.month - 1 + months
        year = int(self._t.year + month / 12 )
        month = month % 12 + 1
        day = min(self._t.day, calendar.monthrange(year, month)[1])
        hour, minute, second, microsecond = self._t.hour, self._t.minute, self._t.second, self._t.microsecond
        self._t = datetime.datetime(year, month, day, hour, minute, second, microsecond)

class SSHClient(object):
    """
    A class that generates an SSH client capable of contacting a server,
    and execute commands remotely, and also copy files from/to the server side to/from the client side

    Args:
        None

    """
    def __init__(self, *args, **kwargs):
        raise NotImplementedError("TODO...")

class URLDownload(object):
    """
    Download a file. A simple class to download files using urllib

    Args:
        link:
        file_name:
        checksum:
        download_immediately:

    """

    def __init__(self, link=None, file_name=None, checksum='md5', download_immediately=False):

        if checksum.lower() not in ['md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512']:
            raise ValueError("Unrecognized checksum [%s]" % checksum)
        else:
            self.checksum = checksum.lower()

        self.file_link = link

        if file_name is not None:
            self.file_name = file_name
        elif link is not None:
            if '/' in self.file_link:
                self.file_name = self.file_link.split("/")[len(self.file_link.split('/')) - 1]
            else:
                self.file_name = link
        else:
            if download_immediately:
                download_immediately = False

        # retrieve the file from the given url upon initialization
        if download_immediately and link is not None:
            self.download(file_link=link, file_name=file_name)
        else:
            pass

    def download(self, file_link=None, file_name=None, print_summary=False, return_summary=False):
        """
        Download a file from web with showing progression and hash

        Args:
            file_link:
            file_name:
            print_summary:
            return_summary:

        """
        if file_link is None:
            raise ValueError("URL must be passed as a first argument!")
        else:
            if file_name is None:
                if '/' in file_link:
                    file_name = file_link.split("/")[len(file_link.split('/')) - 1]
                else:
                    file_name = file_link

        # Check for file first:
        try:
            f = urllib_request.urlopen(file_link)
            f.close()
        except urllib_request.HTTPError:
            terminate("\n>>> Failed to download the requested file:\n"
                      ">>> File Not Found. 404 Error returned!\n"
                      ">>> Please check the url: %s\n" % file_link)

        # File is available. Start downloading:
        timer = time()
        checksum = self.checksum

        urllib.urlretrieve(file_link, file_name, self.hook)
        if print_summary:
            print("\nFile name\t= %s\n ETA\t\t= %i second(s)\n%s checksum\t= %s\n" % (file_name,
                                                                                      int(time() - timer),
                                                                                      checksum
                                                                                      )
                  )
        else:
            pass

        if return_summary:
            out_dic = dict(file_name=file_name,
                           time_elapsed=int(timer - time()),
                           checksum=checksum
                           )
            return out_dic
        else:
            pass

    def hook(self, *data):
        """
        This hook function will be called once on establishment of the network connection and once after
        each block read thereafter.
        The hook will be passed three arguments; a count of blocks transferred so far, a block size in bytes,
        and the total size of the file. The third argument may be -1 on older FTP servers which do not return
        a file size in response to a retrieval request.
        """
        if data[-1] != -1:
            file_size = int(data[2] / 1024.0)
            if file_size >= 1024.0**2:
                file_size_unit = 'GB'
                file_size /= (1024.0**2)
            elif file_size > 1024.0:
                file_size_unit = 'MB'
                file_size /= (1024.0)
            else:
                file_size_unit = 'KB'
        else:
            file_size = None

        downloaded_so_far = data[0]*data[1] / 1024.0  # in KB.
        if downloaded_so_far >= 1024.0**2:
            downloaded_so_far_unit = 'GB'
            downloaded_so_far /= (1024.0**2)
        elif downloaded_so_far > 1024.0:
            downloaded_so_far_unit = 'MB'
            downloaded_so_far /= 1024.0
        else:
            downloaded_so_far_unit = 'KB'

        if file_size is not None:
            percent = float(data[0]) * data[1] / data[2] * 100.0
            out_msg = u"\r... Retrieving [{0:3.1f}%]: {1:3.2f} {2:s}/ {3:3.2f} {4:s}  \t".format(percent,
                                                                                                 downloaded_so_far,
                                                                                                 downloaded_so_far_unit,
                                                                                                 file_size,
                                                                                                 file_size_unit)
        else:
            out_msg = u"\r... Downloading file : {0:3.2f} {1:s} \t".format(downloaded_so_far,
                                                                           downloaded_so_far_unit)
        #
        sys.stdout.write(out_msg)
        sys.stdout.flush()



# ************************* #
# ******  Functions  ****** #
# ************************* #


# --------------------------------------- #
# *** Basic Python Enhanced Functions *** #
# --------------------------------------- #
def isiterable(x):
    try:
        iter(x)
        out = True
    except:
        out = False
    return out

def isscalar(a):
    """
    checks if an object is a scalar by calling isscalar
    """
    flg = True
    if not np.isscalar(a):
        flg = False
    elif isiterable(a):
        flg = False
    elif np.float(a)!=a:
            flg = False
    return flg

def string_to_bytes(s, encoding='utf-8'):
    assert isinstance(s, str), "b must be a string. Received %s of type %s" % (s, type(s))
    b = str.encode(s, encoding=encoding)
    return b

def bytes_to_string(b, encoding='utf-8'):
    assert isinstance(b, bytes), "b must be of type 'bytes'. Received %s of type %s" % (b, type(b))
    s = b.decode(encoding=encoding)
    return s

def validate_string(s, encoding='utf-8', return_type=None):
    """
    Given string or bytes; convert to the right type of string according to return_type <'str', 'bytes'>, or if None, according to python version
    """
    try:
        assert isinstance(s, (str, bytes)), "Passed value must be string or bytes. received instance of %s" % type(s)
    except(AssertionError):
        try:
            s = s.encode('ascii')
        except:
            print("Tried to convert the string to ascii but failed")
            print("Passed string is %s of type %s " % (s, type(s)))
            raise
    # Overkill!
    assert isinstance(s, (str, bytes)), "Passed value must be string or bytes. received instance of %s" % type(s)

    if return_type is not None:
        # convert to string
        if isinstance(s, bytes):
            s = bytes_to_string(s)
        if re.match(r'\Abyte(s)*\Z', return_type, re.IGNORECASE):
            s = string_to_bytes(s)
        elif re.match(r'\Astr(ing)*\Z', return_type, re.IGNORECASE):
            pass
        else:
            print("Unknown return type %s. Supported values are 'str', 'bytes' only!" % return_type)
            raise ValueError
    else:
        if python_version >= (3, 0, 0):
            # Return bytes
            if isinstance(s, bytes):
                pass
            elif isinstance(s, str):
                s = string_to_bytes(s, encoding=encoding)
            else:
                print("Passed string '%s' must be instance of str or bytes; recieved instance of %s" % (s, type(s)))
                raise TypeError
        else:
            # Return string
            if isinstance(s, bytes):
                s = string_to_bytes(s, encoding=encoding)
            elif isinstance(s, str):
                pass
            else:
                print("Passed string '%s' must be instance of str or bytes; recieved instance of %s" % (s, type(s)))
                raise TypeError
    return s

def isstring(s):
    """
    Check if s is s string-like variable
    """
    stype = type(s)
    if isinstance(s, (str, bytes)):
        flag = True
    else:
        try:
            s.encode('ascii')
            flag = True
        except:
            flag = False
    return flag

def query_yes_no(message, default="yes"):
    """
    Terminal-based query: Y/N.
    This keeps asking until a valid yes/no is passed by user.

    Args:
        message: a string prented on the termianl to the user.
        def_answer: the answer presumed, e.g. if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (in the latter case, the answer is required of the user).

    Returns:
        The "answer" return value is True for "yes" or False for "no".

    """
    # valid = {'yes': True, 'y': True, 'ye': True, 'yeah': True, 'yep': True, 'yup': True,
    #          'no': False, 'n': False, 'nope': False, 'nop': False}

    valid_yes = {'yes': True, 'y': True, 'ye': True, 'yeah': True, 'yep': True, 'yup': True}
    valid_no = {'no': False, 'n': False, 'nope': False, 'nop': False}
    valid = valid_yes.copy()
    valid.update(valid_no)

    if default is None:
        pass
    else:
        try:
            default = validate_string(default, return_type='str')
        except:
            print("How is this possible? Not 'None', and not valid string???")
            raise

    if default is None:
        prompt = " [y/n]:? "
    elif default.lower() in valid_yes:
        prompt = " [y/n]: default [Y] "
    elif default.lower() in valid_no:
        prompt = " [y/n]: default [N] "
    else:
        raise ValueError("invalid default answer: '%s' Please input a wide yes/no answer!" % default)

    while True:
        sys.stdout.write(message + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n') or at least something close such as yep, nop, etc.\n")

def read_pickled_data(file_path, encoding='latin1'):
    """
    read data from a binary pickled stream; this handles incompatability between python2, Numpy, and Python3
    """
    if python_version>=(3, 0, 0):
        with open(file_path, 'rb') as f_id:
            u = pickle._Unpickler(f_id)
            u.encoding = encoding
            cont = u.load()
    else:
        cont = pickle.load(open(file_path, 'rb'))
    return cont

def read_pickled_dicts(file_path):
    """
    """
    cont = read_pickled_data(file_path)
    sections_headers = cont.keys()
    #
    dicts = []
    for key in cont:
        dicts.append(cont[key])

    return dicts, sections_headers

def get_list_of_subdirectories(root_dir, ignore_root=False, return_abs=False, ignore_special=True, recursive=True):
    """
    Retrieve a list of sub-directories .

    Args:
        root_dir: directory to start constructing sub-directories of.
        ignore_root: True/False; if True, the passed root_dir is ignored in the returned list
        return_ab: True/False; if True, returned paths are absolute, otherwise relative (to root_dir)
        ignore_special: Ture/False; if True, this function ignores special files (starting with . or __ ).

    Returns:
        subdirs_list: a list containing subdirectories under the given root_dir.

    Returns:
        subdirs_list: list of subdirectories; the subdires are absolute paths or relative paths based the passed root
            returns empty list if root_dir has no subdirectories.

    Raises:
        IOError: If the passed path/directory does not exist
    """
    #
    if not os.path.isdir(root_dir):
        raise IOError(" ['%s'] is not a valid directory!" % root_dir)

    subdirs_list = []
    _passed_path = os.path.abspath(root_dir)
    if os.path.isabs(root_dir):
        pass  # OK, it's an absolute path, good to go
    else:
        # the passed path is relative; convert back, and guarantee it's of the right format for later comparison
        _passed_path = os.path.relpath(_passed_path)

    if recursive:
        for root, _, _ in os.walk(_passed_path):
            # '/.' insures that the iterator ignores any subdirectory of special directory such as '.git' subdirs.
            # '__' insures that the iterator ignores any cashed subdirectory.
            if ignore_special and ('/.' in root or '__' in root):
                continue

            # in case this is not the initial run. We don't want  to add duplicates to the system paths' list.
            if ignore_root and _passed_path == root:
                pass
            else:
                if return_abs:
                    subdirs_list.append(os.path.abspath(root))
                else:
                    subdirs_list.append(os.path.relpath(root))
    else:
        _sub = next(os.walk(_passed_path))[1]
        if not ignore_root:
            if return_abs:
                subdirs_list.append(os.path.abspath(_passed_path))
            else:
                subdirs_list.append(os.path.relpath(_passed_path))
        for d in _sub:
            if return_abs:
                subdirs_list.append(os.path.join(os.path.abspath(_passed_path), d))
            else:
                subdirs_list.append(os.path.join(os.path.relpath(_passed_path), d))

    #
    return subdirs_list

def get_list_of_files(root_dir, recursive=False, return_abs=True, extension=None):
    """
    Retrieve a list of files in the passed root_dir, (and optionally in all sub-directories).
    This function ignores special files (starting with . or __ ).

    Args:
        root_dir: directory to start constructing sub-directories of.
        recursive: True/False; if True, the passed files are found in subdirectories as well
        return_ab: True/False; if True, returned paths are absolute, otherwise relative (to root_dir)

    Returns:
        files_list: a list containing absolute (or relative) under the given root_dir.

    Raises:
        IOError: If the passed path/directory does not exist
   """
    if not os.path.isdir(root_dir):
        raise IOError(" ['%s'] is not a valid directory!" % root_dir)
    else:
        _passed_abs_path = os.path.abspath(root_dir)
        _passed_rel_path = os.path.relpath(root_dir)

    if extension is None:
        _ext = extension
    else:
        _ext = extension.lower().lstrip('* .').rstrip(' ')
    #
    files_list = []
    if recursive:
        dirs_list = get_list_of_subdirectories(root_dir=_passed_abs_path, return_abs=False, ignore_special=False)
    else:
        dirs_list = [_passed_rel_path]

    for dir_name in dirs_list:
        loc_files = os.listdir(dir_name)
        #
        for fle in loc_files:
            file_rel_path = os.path.relpath(os.path.join(dir_name, fle))  # relative path of file
            if os.path.isfile(file_rel_path):  # To ignore directories...
                if not ('/.' in fle or '__' in fle):
                    # Add this file to the aggregated list:
                    if _ext is not None:
                        head, tail = os.path.splitext(file_rel_path)
                        if not(re.match(r'\A(.)*%s\Z' % _ext, tail, re.IGNORECASE)):
                            continue
                    if return_abs:
                        files_list.append(os.path.abspath(file_rel_path))
                    else:
                        files_list.append(file_rel_path)
            else:  # not a file; pass
                pass  # or continue

    return files_list

def try_file_name(directory, file_prefix, extension=None, return_abspath=False):
    """
    Try to find a suitable file name file_prefix_<number>.<extension>
    Look into directory, and search for new filename with the given prefix; add _integer if pattern found
        extension is added in the end of filename

    Args:
        directory:
        file_prefix:
        extension:

    Returns:
        file_name:

    """
    #
    if not os.path.isdir(directory):
        raise IOError(" ['%s'] is not a valid directory!" % directory)

    if extension is None:
        file_name = file_prefix
    else:
        file_name = file_prefix + '.'+extension.strip('. ')

    if not os.path.isfile(os.path.join(directory, file_name)):
        pass
    else:
        #
        success = False
        counter = 0
        while not success:
            if extension is None:
                file_name = file_prefix +'_' + str(counter)
            else:
                file_name = file_prefix +'_' + str(counter) + '.' + extension.strip('. ')

            if not (os.path.isfile(os.path.join(directory, file_name)) ):
                success = True
                break
            else:
                pass
            counter += 1

    if return_abspath:
        file_name = os.path.join(directory, file_name)
        file_name = os.path.abspath(file_name)
    #
    return file_name

def try_directory_name(location, directory_prefix, return_abspath=False):
    """
    Similar to try_file_name, but works for folders

    Args:
        location:
        directory_prefix:
        return_abspath:

    Returns:
        directory:

    """
    #
    if not os.path.isdir(location):
        raise IOError(" ['%s'] is not a valid directory!" % location)

    directory = directory_prefix

    # if prefix exists, try appending numbers:
    if os.path.isdir(os.path.join(location, directory)):
        #
        success = False
        counter = 0
        while True:
            directory = '%s_%d' % (directory_prefix, counter)
            if not os.path.isdir(os.path.join(location, directory)):
                break
            counter += 1

    if return_abspath:
        directory = os.path.abspath(os.path.join(location, directory))
    #
    return directory

def cleanup_directory(directory_name, parent_path, backup_existing=True, zip_backup=False):
    """
    Try to find the directory name under the parent path. I.e. parent_path/directory
    IF the directory does not exist, create it, otherwise either delete it's contents or back them up.
    If backup_existing is True and zip_backup is true the backup is archived as *.zip file.

    Args:
        directory_name:
        parent_path:
        backup_existing:
        zip_backup: used if backup_existing is True; if this is False, the directory is only renamed, otherwise it's also zipped

    """
    directory_name = directory_name.strip('/ \\')
    parent_path = os.path.abspath(parent_path)
    directory_full_path = os.path.join(parent_path, directory_name)
    # print('directory_full_path', directory_full_path)
    #
    if not os.path.isdir(parent_path) or not os.path.isdir(directory_full_path):
        os.makedirs(directory_full_path)
    else:
        if backup_existing:
            # print("Backing up")
            if zip_backup:
                # print("Zipping")
                zip_dir(path=directory_full_path)
                # Remove the leaf directory only, keeping the parent path untouched.
                # the directory is recreated instead of searching for subdirectories and files.
                shutil.rmtree(directory_full_path)
            else:
                # print("Renaming")
                # Just rename the directory; Try other name:
                backup_directory = try_directory_name(parent_path, directory_name, return_abspath=False)
                backup_path = os.path.join(parent_path, backup_directory)
                # print(directory_name, backup_directory, directory_full_path, backup_path)
                os.rename(directory_full_path, backup_path)
                #
            # Now, create the target_path
            os.makedirs(directory_full_path)
        else:
            shutil.rmtree(directory_full_path)
            os.makedirs(directory_full_path)

def zip_dir(path, output_location=None, save_full_path=False, allowZip64=True):
    """
    Backup a directory in a zip archive in the given 'output_location'.
    If an archive with the same name in the output_location exists, a proper number-suffix will replace the archive name.

    Args:
        path: the path of the directory to zip. All files and subdirectory in the leaf directory will be archived.
        output_location: where to save the zip archive
        save_full_path: if true the whole path will be traced while archiving.
        allowZip64: allow zipping large files (> 2GB)

    """
    assert isinstance(path, str)
    path = path.rstrip('/ ')
    if not os.path.isdir:
        raise IOError("The directory you want to archive does not exist or not a valid directory!")
    else:
        # check if there is an archive with the same name in the given directory
        # get proper archive name:
        archive_extension = 'zip'
        if output_location is not None:
            if not os.path.isdir(output_location):
                raise IOError("the passed output_location' is not a valid directory!")
            else:
                archive_parent_dir = output_location
                parent_dir, target_dir = os.path.split(path)
                file_name = target_dir
        else:
            parent_dir, target_dir = os.path.split(path)
            if len(parent_dir) == 0:
                raise IOError("Parent directory is empty. Do you want to zip the root directory!?")
            else:
                archive_parent_dir = parent_dir
                file_name = target_dir

        # check for proper file name in the given directory
        archive_name = try_file_name(directory=archive_parent_dir,
                                  file_prefix=file_name,
                                  extension=archive_extension
                                  )
        if not archive_name.endswith('.' + archive_extension):
            archive_name += '.' + archive_extension
        archive_full_name = os.path.join(archive_parent_dir, archive_name)
        #
        if save_full_path:
            try:
                zip_handler = zipfile.ZipFile(archive_full_name, 'w')
            except(TypeError):
                print("\n *** Failed to Allow large files by setting allowZip64 to True...")
                print("Creating archive object with allowZip64 turned off...\n")
                zip_handler = zipfile.ZipFile(archive_full_name, 'w', allowZip64=allowZip64)

            for root, dirs, files in os.walk(path):
                for file in files:
                    zip_handler.write(os.path.join(root, file))
            zip_handler.close()
        else:
            cwd = os.getcwd()
            os.chdir(parent_dir)
            try:
                zip_handler = zipfile.ZipFile(archive_name, 'w', allowZip64=allowZip64)
            except(TypeError):
                zip_handler = zipfile.ZipFile(archive_name, 'w')
                print("\n *** Failed to Allow large files by setting allowZip64 to True...")
                print("Creating archive object with allowZip64 turned off...\n")

            for root, dirs, files in os.walk(target_dir):
                for file in files:
                    zip_handler.write(os.path.join(root, file))
            zip_handler.close()
            os.chdir(cwd)


# -------------------------- #
# *** Plotting functions *** #
# -------------------------- #
def unique_colors(size, np_seed=2456, gradual=False):
    """
    get uniqe random colors on the format (R, G, B) to be used for plotting with many lines
    """
    # Get the current state of Numpy.Random, and set the seed
    if gradual:
        G_vals = np.linspace(0.0, 0.5, size, endpoint=False)
        R_vals = np.linspace(0.0, 0.3, size, endpoint=False)
        B_vals = np.linspace(0.5, 1, size, endpoint=False)
    else:
        np_rnd_state = np.random.get_state()
        np.random.seed(np_seed)

        G_vals = np.linspace(0.0, 1.0, size, endpoint=False)
        R_vals = np.random.choice(range(size), size=size, replace=False) / float(size)
        B_vals = np.random.choice(range(size), size=size, replace=False) / float(size)

        # Resend Numpy random state
        np.random.set_state(np_rnd_state)

    return [(r, g, b) for r, g, b in zip(R_vals, G_vals, B_vals)]


# ---------------------------------------------------------------- #
# *** Functions for handling Configurations Files/Dictionaries *** #
# ---------------------------------------------------------------- #
def aggregate_configurations(configs=None, def_configs=None, copy_configs=False):
    """
    Blindly (and recursively) combine the two dictionaries. One-way copying: from def_configs to configs only.
    Add default configurations to the passed configs dictionary

    Args:
        configs:
        def_configs:
        copy_configs: if True, a new copy is returned, otherwise configs is updated

    Returns:
        aggr_configs: aggregate_configurations

    """
    if configs is not None:
        assert isinstance(configs, dict), "'configs' must be a dictionary!"
    if def_configs is not None:
        assert isinstance(def_configs, dict), "'def_configs' must be a dictionary!"

    if configs is None and def_configs is None:
        print("\n** Both inputs: (configs, def_configs) are None!\n\n")
        raise ValueError

    elif configs is None:
        aggr_configs = dict.copy(def_configs)

    elif def_configs is None:
        if copy_configs:
            aggr_configs = dict.copy(def_configs)
        else:
            aggr_configs = configs
    else:
        if copy_configs:
            aggr_configs = dict.copy(def_configs)
        else:
            aggr_configs = configs
        #
        for key in def_configs:
            if key not in aggr_configs:
                aggr_configs.update({key: def_configs[key]})
            elif aggr_configs[key] is None:
                    aggr_configs[key] = def_configs[key]
            elif isinstance(aggr_configs[key], dict) and isinstance(def_configs[key], dict):
                # recursively aggregate the dictionary-valued keys
                sub_configs = aggregate_configurations(aggr_configs[key], def_configs[key])
                aggr_configs.update({key: sub_configs})
    #
    return aggr_configs

def write_dicts_to_config_file(file_name, out_dir, dicts, sections_headers, pickle=False):
    """
    Write one or more dictionaries (passed as a list) to a configuration file.

    Args:
        param file_name:
        out_dir:
        dicts:
        sections_headers:

    """
    if not pickle:
        configs = configparser.ConfigParser()
        if isinstance(dicts, list) and isinstance(sections_headers, list):
            # configs = configparser.ConfigParser()
            num_sections = len(sections_headers)
            for sec_ind in range(num_sections):
                #
                curr_dict = dicts[sec_ind]
                if isinstance(curr_dict, dict):
                    if len(curr_dict) == 0:
                        continue
                    else:
                        header = sections_headers[sec_ind]
                        configs.add_section(header)
                        for key in curr_dict:
                            val = curr_dict[key]
                            if val is None:
                                val = 'none'
                            else:
                                val = str(val)
                            configs.set(header, key, val)
                elif curr_dict is None:
                    continue
                else:
                    raise AssertionError("An entry passed is neither a dictionary nor None. passed %s" %repr(curr_dict))
        #
        elif isinstance(dicts, dict) and isstring(sections_headers):
            sections_headers = validate_string(sections_headers, return_type='str')
            # This is a single dictionary --> a single section in the configuration file.
            if len(dicts) == 0:
                pass
            else:
                # configs = configparser.ConfigParser()
                configs.add_section(sections_headers)
                for key in dicts:
                    val = dicts[key]
                    if val is None:
                        val = 'none'
                    else:
                        val = str(val)
                    configs.set(sections_headers, key, val)
        #
        elif dicts is None:
            pass
        else:
            print(dicts, sections_headers)
            raise AssertionError("Either 'dicts' should be a dictionary and 'sections_headers' be a string, OR, \n"
                                 "both are lists of same length.")

        #  write the configurations to file:
        if len(configs.sections()) > 0:
            if not os.path.isdir(out_dir):
                raise IOError(" ['%s'] is not a valid directory!" % out_dir)
            file_path = os.path.join(out_dir, file_name)
            try:
                 # write, save, and close the configurations file
                 with open(file_path, 'w') as configs_file_ptr:
                    configs.write(configs_file_ptr)
            except IOError:
                raise IOError("A configuration template could not be written due to IO Error!")

    else:
        # pickle
        target_dict = {}
        for title, d in zip(sections_headers, dicts):
            target_dict.update({title:d})

        if not os.path.isdir(out_dir):
            raise IOError(" ['%s'] is not a valid directory!" % out_dir)
        file_path = os.path.join(out_dir, file_name)
        pckle.dump(target_dict, open(file_path, 'wb'))

def get_output_dir_structure(output_dir_structure_file, section_header='out_dir_tree_structure', full_paths=True):
    """
    Given the location of the of the output_dir_structure_file, infer the current paths of filter output

    Args:
        full_path: if False, only file_output_dir is abolution, and the rest are relative to it

    Returns:
        output_dir_strucutre: a dictionary with full paths, and configurations of the output directory
    """
    output_dir_structure_file = os.path.abspath(output_dir_structure_file)
    if not os.path.isfile(output_dir_structure_file):
        print("The output_dir_structure_file passed [%s] doesn't point to an existing file!" % output_dir_structure_file)
        raise IOError
    else:
        # The current results directory; this may be different from what's saved in file if the results directory is moved somewhere
        target_out_dir = os.path.dirname(output_dir_structure_file)

    dir_configparser = configparser.ConfigParser()
    dir_configparser.read(output_dir_structure_file)
    if not dir_configparser.has_section(section_header):
        print("Couldn't find the section header '%s' in the file!" % section_header)
        raise ValueError
    else:
        dir_configs = dir_configparser.options(section_header)
        for option in dir_configs:
            option_val = dir_configparser.get(section_header, option)
            if re.match(r'\Afile(-|_)*output(-|_)*dir', option, re.IGNORECASE):
                file_output_dir = option_val
            elif re.match(r'\A(model)*(-|_)*state(s)*(-|_)*dir', option, re.IGNORECASE):
                model_states_dir = option_val
            elif re.match(r'\Aobservation(s)*(-|_)*dir', option, re.IGNORECASE):
                observations_dir = option_val
            elif re.match(r'\A(filter)*(-|_)*statistics(-|_)*dir', option, re.IGNORECASE):
                statistics_dir = option_val
            elif re.match(r'\Acycle(-|_)*prefix', option, re.IGNORECASE):
                cycle_prefix = option_val
            else:
                print("Unknown option [%s] found in the output_dir_structur file: %s" % (option, output_dir_structure_file))
                raise ValueError
        # Correct path, wrt the current path
        if file_output_dir in model_states_dir:
            model_states_dir = model_states_dir.replace(file_output_dir, target_out_dir)
            if not full_paths:
                model_states_dir = os.path.relpath(model_states_dir, start=target_out_dir)
        else:
            print("The configuration in output_dir_structure_file are inconsistent!")
            raise ValueError
        if file_output_dir in observations_dir:
            observations_dir = observations_dir.replace(file_output_dir, target_out_dir)
            if not full_paths:
                observations_dir = os.path.relpath(observations_dir, start=target_out_dir)
        else:
            print("The configuration in output_dir_structure_file are inconsistent!")
            raise ValueError
        if file_output_dir in statistics_dir:
            statistics_dir = statistics_dir.replace(file_output_dir, target_out_dir)
            if not full_paths:
                statistics_dir = os.path.relpath(statistics_dir , start=target_out_dir)
        else:
            print("The configuration in output_dir_structure_file are inconsistent!")
            raise ValueError

    output_dir_structure = dict(file_output_dir=target_out_dir,
                                model_states_dir=model_states_dir,
                                observations_dir=observations_dir,
                                statistics_dir=statistics_dir,
                                cycle_prefix=cycle_prefix
                               )
    return output_dir_structure


# ---------------------------- #
# *** Statistics Functions *** #
# ---------------------------- #
def calculate_errors(first, second, criterion='rmse', vec_size=None, ignore_nan=True, second_is_truth=True):
    """
    Calculate the root mean squared error between two vectors of the same type.
    No exceptions are handled explicitly here

    Args:
        first:
        second:
        criterion: The criterion/measure to use for comparing first to second.
            - 'rmse': root-mean-squared error
            - 'rse': relative-squared error
            - 'mae': mean-absolute error
            - 'rae': relative-absolute error
        vec_size: length of each of the two vectors
        second_is_truth: if True, 'second' is assumed to be the truth, and 'first' is the prediction
            used only by 'rse' and 'rae' criteria

    Returns:
        rmse

    """
    if first is None or second is None:
        return np.nan

    if second_is_truth:
        vec1 = first[:]
        vec2 = second[:]
    else:
        vec1 = second[:]
        vec2 = first[:]

    if re.match(r'\Armse\Z', criterion, re.IGNORECASE):
        err = calculate_rmse(vec1, vec2, vec_size=vec_size, ignore_nan=ignore_nan)
        #
    elif re.match(r'\Amae\Z', criterion, re.IGNORECASE):
        err = calculate_mae(vec1, vec2, vec_size=vec_size, ignore_nan=ignore_nan)
        #
    elif re.match(r'\Arse\Z', criterion, re.IGNORECASE):
        err = calculate_rse(vec1, vec2, vec_size=vec_size, ignore_nan=ignore_nan, second_is_truth=True)
        #
    elif re.match(r'\Arae\Z', criterion, re.IGNORECASE):
        err = calculate_rae(vec1, vec2, vec_size=vec_size, ignore_nan=ignore_nan, second_is_truth=True)
        #
    else:
        valid_criteria = """
        - 'rmse': root-mean-squared error
        - 'rse' : relative-squared error
        - 'mae' : mean-absolute error
        - 'rae' : relative-absolute error
        """
        print("The criterion you passed [%s] is not supported; valid criteria are: %s " % (criterion, valid_criteria))
        raise ValueError
        #
    return err

def calculate_rae(first, second, vec_size=None, ignore_nan=True, second_is_truth=True):
    """
    Calculate the relative-absolute error between two vectors of the same type.
    No exceptions are handled explicitly here

    Args:
        first:
        second:
        vec_size: length of each of the two vectors

    Returns:
        rse

    """
    if first is None or second is None:
        print("in calculate rae; received None as one input; returning NaN")
        return np.nan

    if second_is_truth:
        vec1 = first[:]
        vec2 = second[:]
    else:
        vec1 = second[:]
        vec2 = first[:]

    diff = vec1 - vec2
    if ignore_nan:
        valid_inds = ~np.isnan(diff)
        vec1 = vec1[valid_inds]
        vec2 = vec2[valid_inds]
        diff = diff[valid_inds]

    abs_errs = np.abs(diff).sum()
    scle = np.abs(vec2-vec2.mean()).sum()
    if np.isnan(scle) or np.isinf(scle):
        rae = np.nan
    else:
        rae = abs_errs / scle
    #
    return rae

def calculate_rse(first, second, vec_size=None, ignore_nan=True, second_is_truth=True):
    """
    Calculate the relative-squared error between two vectors of the same type.
    No exceptions are handled explicitly here

    Args:
        first:
        second:
        vec_size: length of each of the two vectors

    Returns:
        rse

    """
    if first is None or second is None:
        print("in calculate rse; received None as one input; returning NaN")
        return np.nan

    if second_is_truth:
        vec1 = first[:]
        vec2 = second[:]
    else:
        vec1 = second[:]
        vec2 = first[:]

    diff = vec1 - vec2
    if ignore_nan:
        valid_inds = ~np.isnan(diff)
        vec1 = vec1[valid_inds]
        vec2 = vec2[valid_inds]
        diff = diff[valid_inds]


    sqrd_errs = np.power(diff, 2).sum()
    scle = np.power(vec2-vec2.mean(), 2).sum()
    if np.isnan(scle) or np.isinf(scle):
        rse = np.nan
    else:
        rse = sqrd_errs / scle
    #
    return rse

def calculate_mae(first, second, vec_size=None, ignore_nan=True):
    """
    Calculate the mean-absolute error between two vectors of the same type.
    No exceptions are handled explicitly here

    Args:
        first:
        second:
        vec_size: length of each of the two vectors

    Returns:
        mae

    """
    if first is None or second is None:
        print("in calculate mae; received None as one input; returning NaN")
        return np.nan

    diff = first[:] - second[:]
    if ignore_nan:
        valid_inds = ~np.isnan(diff)
        diff = diff[valid_inds]

    mae = np.abs(diff).sum() / diff.size
        #
    return mae

def calculate_rmse(first, second, vec_size=None, ignore_nan=True):
    """
    Calculate the root mean squared error between two vectors of the same type.
    No exceptions are handled explicitly here

    Args:
        first:
        second:
        vec_size: length of each of the two vectors

    Returns:
        rmse

    """
    if first is None or second is None:
        print("in calculate rmse; received None as one input; returning NaN")
        return np.nan

    diff = first[:].copy() - second[:].copy()
    if ignore_nan:
        diff = diff[~np.isnan(diff)]
    rmse = np.linalg.norm(diff) / np.sqrt(diff.size)
    #
    return rmse

def linear_translate(value, left_min, left_max, right_min, right_max):
    """
    map values in the range [left_min, left_max] intto [right_min, right_max] with linear transformation
    value should be a scalar or numpy array
    """
    # Figure out how 'wide' each range is
    left_span = left_max - left_min
    right_span = right_max - right_min

    # Convert the left range into a 0-1 range (float)
    scaled_values = (value - float(left_min)) / float(left_span)

    # Convert the 0-1 range into a value in the right range.
    return right_min + (scaled_values * right_span)



# ------------------------------------------------------------- #
# *** Functions for sequences represeing Time and TimeStams *** #
# ------------------------------------------------------------- #
def create_timespan(start_time, end_time, delta_time, return_string=False):
    """
    """
    timespan = [t for t in TIME_ITERATOR(start_time=start_time, end_time=end_time, delta_time=delta_time, return_string=return_string)]
    return timespan

def str_from_timestamp(timestamp, delim=':'):
    """
    """
    if not isinstance(timestamp, datetime.datetime):
        print("The passed timestamp must be an instance of datetime.datetime. received %s of type %s" %(str(timestamp), type(timestamp)))
        raise TypeError
    return timestamp.strftime("%Y:%m:%d:%H:%M:%S")

def timespan_from_scalars(sclr_timespan):
    """
    """
    out = [timestamp_from_scalar(t, time_diff_unit='s', return_string=True) for t in sclr_timespan]
    return out

def timespan_from_strings(str_timespan):
    """
    """
    return [str_to_timestamp(s) for s in str_timespan]

def str_to_timestamp(time_str, timeonly=False, delim=None):
    """
    if timeonly, the stamp doesn't include date
    """
    if isstring(time_str):
        time_str = validate_string(time_str, return_type='str')
        if delim is None:
            delims = [':', ',', ';', '/', '\\']
            for d in delims:
                if d in time_str:
                    delim = d
                    break
            if delim is None:
                print("Couldn't find a known delim; The time string must be separated by one of the following characters %s\n" % repr(delims))
                raise ValueError
        else:
            assert isstring(delim), "'delim' must be a string! Received %s!" % type(delim)
            delim = validate_string(delim, return_type='str')
            assert len(delim)==1, "'delim' must be a string of LENGTH equal to 1, Received %s" % delim

        lst = time_str.split(delim)

        if timeonly:
            _inits = [str(datetime.MINYEAR),'01', '01', '00', '00', '00', '00']  #

            if 2 <= len(lst)<= 6:
                lst += _inits[len(lst): ]
            elif len(lst) == len(_inits):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time format!")
                print("You must pass either HH, or HH:MM, or HH:MM:SS, etc.")
                raise ValueError
            lst = [int(v) for v in lst[3:]]
            timestamp = datetime.time(lst[0], lst[1], lst[2], lst[3])
        else:

            _inits = [str(datetime.MINYEAR),'01', '01', '00', '00', '00', '00']  #

            if 2 <= len(lst)<= 6:
                lst += _inits[len(lst): ]
            elif len(lst) == len(_inits):
                pass  # cool, full time/data Year:...: microseconds
            else:
                print("Unrecognized time/date format!")
                print("You must pass either YYYY, or YYYY:MM, or YYYY:MM:DD, etc.")
                raise ValueError

            lst = [int(v) for v in lst]
            timestamp = datetime.datetime(lst[0], lst[1], lst[2], lst[3], lst[4], lst[5], lst[6])

    elif isinstance(time_str, datetime.datetime):
        if timeonly:
            timestamp = datetime.time(time_str.hour, time_str.minute, time_str.second, time_str.microsecond)
        else:
            timestamp = time_str
    elif isinstance(time_str, datetime.time):
        timestamp = time_str
    else:
        print("'time_str' must be either a string with format 'YYYY:MM:DD:hh:mm:ss*' or an isinstance of datetime.datetime, and not %s" %type(time_str))
        raise TypeError
    return timestamp

def timestamp_from_scalar(timescalar, time_diff_unit='s', return_string=False):
    """
    """
    assert isscalar(timescalar), "timescalar must be a scalar"
    if int(timescalar) != timescalar:
        print("timescalar must be an integer")
        raise ValueError
    timescalar = int(timescalar)
    time_diff_unit = validate_string(time_diff_unit, return_type='str')
    time_diff_unit = time_diff_unit.lower().strip()

    timediff = timescalar
    if time_diff_unit == 's':
        pass
    elif time_diff_unit == 'm':
        timediff /= 60.0
    elif time_diff_unit == 'h':
        timediff /= 3600.0
    else:
        print("'time_diff_unit' must be: 's' for seconds, 'm' for minutes, 'h' for hours ; Received %s of type %s" % (repr(time_diff_unit), type(time_diff_unit)))
        raise ValueError
    #
    time_delta = datetime.timedelta(seconds=timediff)
    timestamp = __BASE_DATE + time_delta
    if return_string:
        timestamp = str_from_timestamp(timestamp)
    return timestamp

def validate_deltatime(time_delta, time_diff_unit='s', return_type=None):
    """
    Given a time_delta as string, bytes, scalar, or string, convert it to a valid time delta instance based on the return_type
    supported return types are None, 'td', 'scalar', 'str'

    if reuturn_type is None (handled exactly as 'td'); datetime.timedelta instance is returned by default

    """
    time_diff_unit = validate_string(time_diff_unit, return_type='str')
    if return_type is None:
        return_type = 'td'
    return_type = validate_string(return_type, return_type='str')

    if isstring(time_delta):
        time_delta = validate_string(time_delta, return_type='str')
    elif isinstance(time_delta, datetime.timedelta):
        pass
    elif isscalar(time_delta):
        time_delta = deltatime_from_scalar(time_delta, time_diff_unit=time_diff_unit)
    else:
        print("time_delta type [%s] is not supported; only str, bytes, scalar, datetime.timedelta are supported" % type(time_delta))
        raise TypeError
    # time_delta is instance of datetime.timedelta

    if re.match(r'\At(ime)*(-| |_)*d(elta)*\Z', return_type, re.IGNORECASE):
        pass

    elif re.match(r'\Ascalar\Z', return_type, re.IGNORECASE):
        if re.match(r'\As\Z', time_diff_unit, re.IGNORECASE):
            time_delta = time_delta.total_seconds()
        elif re.match(r'\Am\Z', time_diff_unit, re.IGNORECASE):
            time_delta = time_delta.total_seconds() / 60.0
        elif re.match(r'\Ah\Z', time_diff_unit, re.IGNORECASE):
            time_delta = time_delta.total_seconds() / 3600.0
        else:
            print("'time_diff_unit' must be: 's' for seconds, 'm' for minutes, 'h' for hours ; Received %s of type %s" % (repr(time_diff_unit), type(time_diff_unit)))
            raise ValueError

    elif re.match(r'\Astr(ing)*\Z', return_type, re.IGNORECASE):
        days = time_delta.days
        seconds = time_delta.seconds
        h = seconds // 3600
        m = (seconds - h*3600) // 60
        s = seconds - h*3600 - m*60
        time_delta = "0000:00:{0:02d}:{1:02d}:{2:02d}:{3:02d}".format(days, h, m, s)

    else:
        print("Unsupported return type %s" % return_type)
        raise ValueError
    #
    return time_delta

def deltatime_to_scalar(time_delta, time_diff_unit='s'):
    """
    Convert time_delta to scalar based on time_diff_unit
    """
    time_diff_unit = validate_string(time_diff_unit, return_type='str')
    time_delta = validate_deltatime(time_delta, time_diff_unit=time_diff_unit, return_type='td')
    timediff = time_delta.total_seconds()
    if time_diff_unit == 's':
        pass
    elif time_diff_unit == 'm':
        timediff /= 60.0
    elif time_diff_unit == 'h':
        timediff /= 3600.0
    else:
        print("'time_diff_unit' must be: 's' for seconds, 'm' for minutes, 'h' for hours ; Received %s of type %s" % (repr(time_diff_unit), type(time_diff_unit)))
        raise ValueError
    return timediff

def deltatime_from_scalar(timescalar, time_diff_unit='s', return_string=False):
    """
    """
    time_diff_unit = validate_string(time_diff_unit, return_type='str')
    time_diff_unit = time_diff_unit.lower().strip()

    timediff = timescalar
    if time_diff_unit == 's':
        pass
    elif time_diff_unit == 'm':
        timediff /= 60.0
    elif time_diff_unit == 'h':
        timediff /= 3600.0
    else:
        print("'time_diff_unit' must be: 's' for seconds, 'm' for minutes, 'h' for hours ; Received %s of type %s" % (repr(time_diff_unit), type(time_diff_unit)))
        raise ValueError
    #
    time_delta = datetime.timedelta(seconds=timediff)
    if return_string:
        days = time_delta.days
        seconds = time_delta.seconds
        h = seconds // 3600
        m = (seconds - h*3600) // 60
        s = seconds - h*3600 - m*60
        time_delta = "0000:00:{0:02d}:{1:02d}:{2:02d}:{3:02d}".format(days, h, m, s)
    return time_delta

def str_to_deltatime(timestr):
    timestr = validate_string(timestr, return_type='str')
    lst = timestr.split(':')
    _deltas = ['0000', '00', '00', '00', '00', '00', '00']  #
    if 1 <= len(lst)<= 6:
        lst += _deltas[len(lst): ]
    elif len(lst) == len(_deltas):
        pass  # cool, full time/data Year:...: microseconds
    else:
        print("Unrecognized time/data format!")
        raise ValueError
    delta_time = ':'.join(lst)
    lst = delta_time.split(":")
    Y, M, D, h, m, s, ms = [int(t) for t in lst]
    if Y > 0 or M > 0 or D > 0:
        print("Can't return timedelta more than a day")
    time_diff = datetime.timedelta(hours=h, minutes=m, seconds=s, microseconds=ms)
    return time_diff

def time_to_timestamp(t, time_diff_unit='s'):
    """
    Given a time t in any of the recognized formats (string, data/time-stamp, or scalar), return a timestamp
    """
    time_diff_unit = validate_string(time_diff_unit, return_type='str')
    if isscalar(t):
        stamp = timestamp_from_scalar(t, time_diff_unit=time_diff_unit)
    elif isstring(t):
        stamp = str_to_timestamp(t)
    elif isinstance(t, datetime.datetime):
        stamp = t
    elif isinstance(t, datetime.time):
        stamp = datetime.datetime(__BASE_DATE.year, __BASE_DATE.month, __BASE_DATE.day, t.hour, t.minute, t.second, t.microsecond)
    else:
        print("Initial time %s has unknown type: %s " %(repr(t), type(t)))
        raise TypeError
    return stamp

def timestamp_to_scalar(t, time_diff_unit='s'):
    """
    """
    time_diff_unit = validate_string(time_diff_unit, return_type='str')
    #
    sclr_t = (t - __BASE_DATE).total_seconds()
    if time_diff_unit == 's':
        pass
    elif time_diff_unit == 'm':
        sclr_t *= 60.0
    elif time_diff_unit == 'h':
        sclr_t *= 3600.0
    else:
        print("'time_diff_unit' must be: 's' for seconds, 'm' for minutes, 'h' for hours ; Received %s of type %s" % (repr(time_diff_unit), type(time_diff_unit)))
        raise ValueError
    return sclr_t

def time_to_scalar(t, time_diff_unit='s'):
    """
    Convert the time in t to a scalar representation. t could be scalar, string representation of a date, or a timestamp
    """
    t = time_to_timestamp(t)
    sclr_t = timestamp_to_scalar(t, time_diff_unit=time_diff_unit)
    return sclr_t

def timespan_to_scalars(timespan, ref_initial_time=None, time_diff_unit='s'):
    """
    Convert timespan to a list starting at ref_initial_time, and all entries are referenced to it
    time_diff_unit is 's' for seconds, 'm' for minutes, 'h' for hours. This is how difference is calculated w.r.t the reference (initial) point

    The first entry is checked, and everything else is assumed to be similar;
    Only pass sclars, time stamps, or
    """
    # TODO: This can be wrritten better, given: timestamp_to_scalar(time_to_timestamp(t)) over all entries, then shifting w.r.t. ref_initial_time
    assert isiterable(timespan), "timespan must be an iterble!\n Received timespan of type: %s" %type(timespan)
    time_diff_unit = validate_string(time_diff_unit, return_type='str')

    # if ref_initial_time is not None:
    #     assert type(ref_initial_time) is type(timespan[0]), "ref_initial_time MUST BE of the same type as timespan entries!!! Received %s of type %s" %(ref_initial_time, type(ref_initial_time))
    if len(timespan) == 0:
        print("WARNING: Empty timespan is passed; returning an empty list...")
        return []

    if ref_initial_time is None:
        ref_initial_time = timespan[0]

    # Convert reference initial time to datetime timestamp
    _ref_t0 = time_to_timestamp(ref_initial_time)

    time_diff = _ref_t0 - __BASE_DATE
    sclr_t0 = time_diff.total_seconds()
    if time_diff_unit == 's':
        pass
    elif time_diff_unit == 'm':
        sclr_t0 *= 60.0
    elif time_diff_unit == 'h':
        sclr_t0 *= 3600.0
    else:
        print("'time_diff_unit' must be: 's' for seconds, 'm' for minutes, 'h' for hours ; Received %s of type %s" % (repr(time_diff_unit), type(time_diff_unit)))
        raise ValueError


    # Convert timespan to datetimestamp
    adjusted_timespan = [time_to_timestamp(t) for t in timespan]
    # construct scalar timespan
    scalar_timespan = [sclr_t0]
    #
    # Loop over all timepoints
    for i, t1 in enumerate(adjusted_timespan[1: ]):
        t0 = adjusted_timespan[0]
        #
        time_diff = t1 - t0
        sec = time_diff.total_seconds()
        if time_diff_unit == 's':
            time_diff = sec
        elif time_diff_unit == 'm':
            time_diff = sec / 60.0
        elif time_diff_unit == 'h':
            time_diff = sec / 3600.0
        else:
            print("'time_diff_unit' must be: 's' for seconds, 'm' for minutes, 'h' for hours ; Received %s of type %s" % (repr(time_diff_unit), type(time_diff_unit)))
            raise ValueError
        scalar_timespan.append(time_diff+sclr_t0)

    return scalar_timespan


# ------------------------------------------------------------- #
# *** Functions to manipulate/transorm between Coordiantes ***  #
#         ***  (e.g., Cartesian, Spherical, Etc.)  ***          #
# ------------------------------------------------------------- #
# 1) Conversion of grid coordinates
def cartesian_to_spherical_grid(source_grid, return_degrees=True, adjust_azimuth=True, verbose=False):
    """
    Same as cartesian_to_spherical, but operates on columns of source_grid simultaneously

    The returned array has 3 columns; r, theta, phi respectively;
    where r is the length of the vector, i.e., it's norm, and theta is the inclination angle, and phi is the azimuth angle.

    """
    assert np.ndim(source_grid) == 2, "source_grid must be a numpy 2D array"
    assert np.size(source_grid, 1) == 3, "number of comumns in source_grid must be 3, not %d" % np.size(source_grid, 1)

    x, y, z = source_grid[:, 0], source_grid[:, 1], source_grid[:, 2]
    phi = np.empty_like(x)
    theta = np.empty_like(x)

    r = np.sqrt(x**2 + y**2 + z**2)
    zero_r = r == 0
    zero_x = x == 0

    # Theta
    theta[zero_r] = 0.0
    theta[~zero_r] = np.arccos(z[~zero_r] / r[~zero_r])

    # Phi
    # zero r;
    phi[zero_r] = 0.0

    # Non-zero r; zero x
    zerox_inds = np.bitwise_and(~zero_r, zero_x)
    phi[np.bitwise_and(zerox_inds, (y>=0))] = np.pi/2
    phi[np.bitwise_and(zerox_inds, (y<0))] = 3 * np.pi/2

    # Non-zero r; nonzero x
    target_inds = np.bitwise_and(~zero_r, ~zero_x)
    phi[target_inds] = np.arctan2(y[target_inds], x[target_inds])

    if adjust_azimuth:
        phi = ((2*np.pi) + phi ) % (2*np.pi)

    if return_degrees:
        theta = np.degrees(theta)
        phi = np.degrees(phi)

    spherical_grid = np.vstack((r, theta, phi)).T

    if verbose:
        print("Input Cartesian Grid: ", source_grid)
        print("Spherical Grid: ", spherical_grid)

    return spherical_grid

def cartesian_to_spherical(cart_vec, return_degrees=True, adjust_azimuth=True, verbose=False):
    """
    Convert a vector from cartesian to spherical coordinates

    Args:
        cart_vec: the vector in cartesian coordinate system.
            cart_vec can be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries

        return_degrees: if True, the returned vector is represented in degrees, otherwise, it is represented using radian units.
        adjust_azimuth; return azimuth in the interval [0, 2 pi], otherwise [-pi, pi]
        verbose: turn printing results on/off

    Return:
        sph_vec: The representation of the vector cart_vec, in the spherical coordinates with origin centered at the origin of the cartesian system.
            sph_vec = (r, theta, phi), where r is the length of the vector, i.e., it's norm, and theta is the inclination angle, and phi is the azimuth angle.

    Remarks:
        - The inclination is the angle made with the positive direction of the z axis,
        - The azimuth is the angle between the projection of the vector cart_vec onto the x-y plane, and the positive direction of the x-axis (anti-clock-wise).

    """
    try:
        u, v, w = cart_vec[:]
    except:
        print("Couldn't cast the input 'cart_vec' into a 1d-array of length 3. ")
        print("The input must be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries")
        raise ValueError

    # Calculate the length of the vector
    r = np.sqrt(u**2 + v**2 + w**2)
    if r == 0:
        # this is the zeor vector
        phi = theta = 0
    else:
        # the inclination
        theta = np.arccos(w/r)

        # calculate the azimuth
        if u == 0:
            # the vector is in the y-z plane (phi = pi/2 or 3pi/2 based on the sign of v
            if v >= 0:
                phi = np.pi/2
            else:
                phi = 3 * np.pi/2
        else:
            phi = np.arctan2(v, u)  # This is equivalent to arctan(v/u) with singularity issues handled properly

    if adjust_azimuth:
        phi = ((2*np.pi) + phi ) % (2*np.pi)

    if return_degrees:
        theta = np.degrees(theta)
        phi = np.degrees(phi)

    sph_vec = np.array([r, theta, phi])

    if verbose:
        sep = "*"*70
        print("\n%s\n\tTrnsform Cartesian vecotr into spherical coordiantes\n%s" % (sep, sep))
        print("Input:")
        print("Cartesian representation (X-Y-X) components:\n\t %12.8g, %12.8g, %12.8g" % (u, v, w))
        print("Spherical representation (length, inclination, azimuth):")
        if return_degrees:
            print("Degrees: %12.8g, %12.8g, %12.8g" % (r, theta, phi))
        else:
            print("Radians: %12.8g, %12.8g, %12.8g" % (r, theta, phi))
        print(sep)

    return sph_vec

def spherical_to_cartesian(sph_vec, use_degrees=True, verbose=False):
    """
    Convert a vector from spherical to cartesian coordinates. This is the iverse of cartesian_to_spherical

    Args:
        sph_vec: the vector in spherical  coordinate system.
            sph_vec=(r, theta, phi) can be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries

        use_degrees: if True, the input vector is assumed to be represented in degrees, otherwise, it is represented using radian units.

        verbose: turn printing results on/off

    Return:
        cart_vec: The representation of the vector sph_vec, in the cartesian coordinates with origin centered at the origin of the spherical system.
            cart_vec = (u, v, w)

    Remarks:
        - The inclination theta, is the angle made with the positive direction of the z axis,
        - The azimuth phi, is the angle between the projection of the vector cart_vec onto the x-y plane,
            and the positive direction of the x-axis (anti-clock-wise).

    """
    try:
        r, theta, phi = sph_vec[:]
    except:
        print("Couldn't cast the input 'sph_vec' into a 1d-array of length 3. ")
        print("The input must be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries")
        raise ValueError
    assert r >= 0, "The vector magnitude must be non-negative!"

    # We can check if r = 0, and reture immediately, but I'll just proceed
    # Convert to radians, if the input is in degrees
    if use_degrees:
        theta = np.radians(theta)
        phi = np.radians(phi)

    # Calclate the projections on the cartesian coordinates
    u = r * np.sin(theta) * np.cos(phi)
    v = r * np.sin(theta) * np.sin(phi)
    w = r * np.cos(theta)
    cart_vec = np.array([u, v, w])

    if verbose:
        sep = "*"*70
        print("\n%s\n\tTrnsform Spherical vector into Cartesian coordiantes\n%s" % (sep, sep))
        print("Input:")
        if use_degrees:
            print("** In Degrees **")
        else:
            print("** In Radians **")
        print("Spherical representation (r, theta, phi) components:\n\t %12.8g, %12.8g, %12.8g" % (sph_vec[0], sph_vec[1], sph_vec[2]))
        print("Cartesian representation (X-Y-Z) components:\n\t %12.8g, %12.8g, %12.8g" % (u, v, w))
        print(sep)

    return cart_vec

def geodetic_to_cartesian(lat, lon, alt, projection='ellipse', distance_unit='m'):
    """
    Convert geodetic coordinates (lat, lon, alt) to cartesian coordinates
        these are the Earth-Centered-Earth-Fixed (ECEF) coordinates.
        Conventional Terrestrial System (CTS) is used;
        The origin is the center of mass of the whole Earth including oceans and atmosphere, the geocenter
        The Prime Meridian corresponds to the x-axis.
        The y-axis is perpendicular to that at 90 degrees longitude east and west.
        The polar axis is the z-axis, which is known as the International Reference Pole (IRP).

    Args:
        lat: latidtude in decimal format
        lon: longitude in decimal format
        alt: altitude in meters (this is height above ground level (to be verified with Emil))
        projection: the assumption underlying the shape of the earth, or the map projection used
            - 'sphere': assume the earth is a sphere, and the x-y-z directiions are aligned with

    Returns:
        cart_coordiantes (x, y, z)


    """
    # Define earth constants from IUGG
    r1 = 6371008.7714  # mean radius (R1)
    r2 = 6371007.1810  # radius of sphere of same surface (R2)
    r3 = 6371000.7900  # radius of sphere of same volume (R3)
    a  = 6378137.0000  # WGS-84 ellipsoid, semi-major axis (a); Equatorial radius
    b  = 6356752.3142  # WGS-84 ellipsoid, semi-minor axis (b); Polar radius
    c  = 6399593.6258  # WGS-84 ellipsoid, polar radius of curvature (c)

    if isscalar(lat) and isscalar(lon):
        lat = np.array([lat])
        lon = np.array([lon])
    elif isscalar(lat):
        lat = np.ones_like(lon) * lat
    elif isscalar(lon):
        lon = np.ones_like(lat) * lon
    else:
        assert lat.shape == lon.shape, "Lat and Lon must be of equal shapes"

    #
    if alt is None:
        alt = np.zeros_like(lon)
    elif isinstance(alt, np.ndarray):
        pass
    elif isscalar(alt):
        if alt == 0:
            alt = np.zeros_like(lon)
        else:
            alt = np.ones_like(lon) * alt
    else:
        print("Incorrect type of alt: %s " % type(alt))
        raise TypeError

    if re.match(r'\Aspher(e|ical)\Z', projection, re.IGNORECASE):
        # use r2 as the earth's fixed radius
        radius = r2
        lat, lon = np.radians([lat, lon])
        if np.shape(lat) == np.shape(alt):
            x = radius * np.cos(lat) * np.cos(lon)
            y = radius * np.cos(lat) * np.sin(lon)
            z = radius * np.sin(lat) + alt
        elif np.ndim(lat) == np.ndim(alt)-1:
            x = np.empty_like(alt)
            y = np.empty_like(alt)
            z = np.empty_like(alt)
            cos_lat = np.cos(lat)
            sin_lat = np.sin(lat)
            cos_lon = np.cos(lon)
            sin_lon = np.sin(lon)
            for k in range(np.size(alt, 0)):
                h = alt[k, ...]
                scaled_radius = h + radius
                x[k, ...] = scaled_radius * cos_lat * cos_lon
                y[k, ...] = scaled_radius * cos_lat * sin_lon
                z[k, ...] = scaled_radius * sin_lat
        else:
            print("lat heights must be equal to or one dimensions less than alt")
            raise ValueError
        cart_coordinates = (x, y, z)
    elif re.match(r'\Aellips(e|oid)*(al)*\Z', projection, re.IGNORECASE):
        # Earth is an ellipsoid, and a, b are it's major and minor axes
        # Calculate the radius at the given latitude
        if geopy_distance is not None:
            # TODO: Refactoring is needed for faster iteration over elements, since geopy.distance operate on (lat, lon, alt) not numpy arrays!
            # Calculate distances using geopy
            # Everything is calculated w.r.t. geodetic origin.
            # Use latitude to calculate Y, and Longitude to calculate X
            if not lat.shape == lon.shape:
                print("Lat and Lon must be of equal dimensions!")
                raise ValueError
            #
            x = np.empty_like(lat)
            y = np.empty_like(lon)
            #
            xravel = x.ravel()
            yravel = y.ravel()
            #
            # Calculate X from longitude
            for il, l in enumerate(lon.ravel()):
                xravel[il] = geopy_distance.distance((0, l), (0, 0)).m
            x *= np.sign(lon)
            # Calculate Y from latitude
            for il, l in enumerate(lat.ravel()):
                yravel[il] = geopy_distance.distance((l, 0), (0, 0)).m
            y *= np.sign(lat)
            #
            if alt is None:
                z = None
            else:
                z = alt.copy()
            if re.match(r'\Am(eter(s)*)*\Z', distance_unit, re.IGNORECASE):
                pass
            elif re.match(r'\Ak(ilo)*(-| |_)*m(eter(s)*)*\Z', distance_unit, re.IGNORECASE):
                # meters to kilometers
                x /= 1000.0
                y /= 1000.0
                z /= 1000.0
            elif re.match(r'\Amile(s)*\Z', distance_unit, re.IGNORECASE):
                # meters to miles
                x /= 1609.344
                y /= 1609.344
                z /= 1609.344
            else:
                print("Distance Unit must be either meters, km, or miles; others are not supported!")
                raise ValueError

            if x.size == 1:
                x = x[0]
            if y.size == 1:
                y = y[0]
            if z.size == 1:
                z = z[0]
            #
        else:
            # We should use geopy to calcualte x-y distances, then find z from altitude...
            # Resource!
            lat, lon = np.radians([lat, lon])
            num   = (a**2*np.cos(lat))**2 + (b**2*np.sin(lat))**2
            denom = (a**np.cos(lat))**2 + (b*np.sin(lat))**2
            radius = np.sqrt(num/denom)
            x = radius * np.cos(lat) * np.cos(lon)
            y = radius * np.cos(lat) * np.sin(lon)
            if np.shape(lat) == np.shape(alt):
                z = radius * np.sin(lat) + alt
            elif np.ndim(lat) == np.ndim(alt)-1:
                z = np.empty_like(alt)
                scaled_sin_lat = radius * np.sin(lat)
                for k in range(np.size(alt, 0)):
                    z[k, ...] = scaled_sin_lat + alt[k, ...]
            else:
                print("lat heights must be equal to or one dimensions less than alt")
                raise ValueError
        cart_coordinates = (x, y, z)
    else:
        print("The projection [%s] is not supported" % projection)
        raise ValueError

    return cart_coordinates

def geodetic_to_cartesian_grid(goedetic_grid, projection='ellipse'):
    """
    Given a geodetic grid 'goedetic_grid' in the form of a 2d numpy array,
    with 3 columns representing latitude, longitude, and altitude (in meters),
    convert the grid into equivalent cariesian coorinates
    assuming the passed proejection.
    This function wraps the function 'geodetic_to_cartesian', with more
    detials given in it's docstring.

    """
    assert isinstance(geodetic_grid, np.ndarray), "geodetic_grid must be a numpy array not %s " % type(geodetic_grid)
    assert np.ndim(geodetic_grid) == 2, "geodetic_grid must be two dimensional; shape found is %s " % np.shape(geodetic_grid)
    assert np.size(geodetic_grid, 1) == 3, "the 1-axis of geodetic grid must consist of 3 columns"

    cartesian_grid = np.empty_like(geodetic_grid)
    for i in range(np.size(geodetic_grid, 0)):
        lat, lon, alt = geodetic_grid[i, :]
        cartesian_grid[i, :] = geodetic_to_cartesian(lat, lon, alt, projection=projection)

    return cartesian_grid

def get_rotation_matrices(alpha=0, beta=0, gamma=0, radians=False):
    """
    Create rotation matrices Rz, Ry, Rx
        1- A yaw is a counterclockwise rotation of $\alpha$ about the $ z$-axis.
            The rotation matrix is $R_z$
        2- A pitch is a counterclockwise rotation of $\beta$ about the $ y$-axis.
            The rotation matrix is $R_y$
        3- A roll is a counterclockwise rotation of $\gamma$ about the $ x$-axis.
            The rotation matrix is $R_x$

    Args:
        alpha
        beta
        gamma
        radians: regard the inputs as radians vs. degrees (if false, the input is degrees and is converted to radians first for numpy)
    """
    if not radians:
        alpha, beta, gamma = np.radians([alpha, beta, gamma])

    # create the arrays
    Rz = np.array([[ np.cos(alpha), -np.sin(alpha), 0],
                  [ np.sin(alpha),  np.cos(alpha), 0],
                  [             0,              0, 1]])
    Ry = np.array([[ np.cos(beta), 0, np.sin(beta)],
                  [            0, 1,            0],
                  [-np.sin(beta), 0, np.cos(beta)]])
    Rx = np.array([[1,             0,              0],
                  [0, np.cos(gamma), -np.sin(gamma)],
                  [0, np.sin(gamma),  np.cos(gamma)]])

    return Rz, Ry, Rx

def get_rotation_matrix(alpha=0, beta=0, gamma=0, radians=False):
    """
    """
    Rz, Ry, Rx = get_rotation_matrices(alpha=alpha, beta=beta, gamma=gamma, radians=radians)
    R = Rx.dot(Ry.dot(Rz))
    return R

#
# 2) Data/Fields Transformation/Interpolation/... Between grids
def interpolate_field(source_grid, source_field, target_grid, method='linear', fill_value=np.nan, verbose=False, overwrite_nan=True):
    """
    Given a grided filed 'source_field', over the grid defined by 'source_grid',
    calculate an interpolated filed over the grid 'target_grid'.

    Args:
        source_grid  :
        source_field :
        target_grid  :
        fill_value : float, optional
            Value used to fill in for requested points outside of the
            convex hull of the input points.  If not provided, then the
            default is ``nan``. This option has no effect for the
            'nearest' method.

    Returns:
        target_filed : ndarray
            Array of interpolated values.

    """
    # Check source grid dimensions, along with source field:
    if isinstance(source_grid, (tuple, list)):
        if verbose:
            print("\nInside Interpolation utility function; \nSource grid Ranges:")
            for d in range(len(source_grid)):
                print("Dim[%d]: From [%f] To [%f]" %(d, source_grid[d].min(), source_grid[d].max()))
            print("Source Values from %f to %f\n" % (source_field[d].min(), source_field[d].max()))

            print("Target Grid Ranges:")
            for d in range(len(source_grid)):
                print("Dim[%d]: From [%f] To [%f]" %(d, source_grid[d].min(), source_grid[d].max()))

        if 2<=len(source_grid) <=3:
            if np.ndim(source_grid[-1]) == 3:
                if source_grid[-1].ndim == source_grid[0].ndim+1:
                    if False:
                        # Assuming Z has multiple levels, and x, y are constant gridopints
                        # Replicate, and cast
                        assrt_msg = "source field must be of the same shape as z grid"
                        assrt_msg += "\n The order MUST BE X x Y x Z"
                        assrt_msg += "\nsource_field.shape: %s" % repr(source_field.shape)
                        assrt_msg += "\nsource_grid[-1].shape %s \n" % repr(source_grid[-1].shape)
                        assert source_field.shape == source_grid[-1].shape, assrt_msg

                        # print("Flatting grid")
                        grid_size = np.product(np.shape(source_field)[1: ])
                        full_size = np.product(np.shape(source_field))
                        updated_source_field = np.empty(full_size)
                        updated_grid = np.empty((full_size, 3))
                        xs = source_grid[0].flatten()
                        ys = source_grid[1].flatten()
                        for k in range(np.size(source_field, 0)):
                            zs = source_grid[2][k, ...].flatten()
                            vals = source_field[k, ...].flatten()
                            #
                            updated_grid[k*grid_size: (k+1)*grid_size, 0] = xs
                            updated_grid[k*grid_size: (k+1)*grid_size, 1] = ys
                            updated_grid[k*grid_size: (k+1)*grid_size, 2] = zs
                            updated_source_field[k*grid_size: (k+1)*grid_size] = source_field[k, ...].flatten()
                        source_grid = tuple([updated_grid[:, j] for j in range(3)])
                        source_field = updated_source_field
                    else:
                        print("Source grid shape must be equal in  all dimensions")
                        raise AssertionError

                elif source_grid[-1].ndim == source_grid[0].ndim:
                    # flatten only
                    source_grid = tuple([grid.flatten() for grid in source_grid])
                    source_field = source_field.flatten()
                    pass
                else:
                    raise TypeError
            else:
                # 2d Should be straightforward...
                # flatten only
                source_grid = tuple([grid.flatten() for grid in source_grid])
                source_field = source_field.flatten()

        else:
            print("Only 2 or 3D interpolation are supported")
            raise TypeError
        pass
    elif isinstance(source_grid, np.ndarray):
        print("WARNINIG: Assertion on NP-array is not yet implemented. TODO...")
        raise TypeError
    else:
        print("source grid must be a tuple, a list, or a numpy array")
        raise TypeError

    if np.any(np.isnan(source_field)):
        print("!!! Caution: some input  values are NaNs !!!")

    #
    try:
        target_field = griddata(source_grid,
                                source_field,
                                target_grid,
                                method=method,
                                fill_value=fill_value,
                                rescale=True)
        # identify nans
        if overwrite_nan:
            if re.match(r'\Anearest\Z', method, re.IGNORECASE):
                print("Can't fillup NaNs after nearest interpolation!")
                raise ValueError
            if verbose:
                print("Checking for NaNs...")
            nan_locs = np.where(np.isnan(target_field))[0]
            if nan_locs.size > 0:
                if verbose:
                    print("Updating NaNs to nearest gridpoints...")
                up_vals = griddata(source_grid,
                                   source_field,
                                   tuple([grid[nan_locs] for grid in target_grid]),
                                   method='nearest',
                                   fill_value=fill_value,
                                   rescale=True)
                if np.any(np.isnan(up_vals)):
                    print("Couldn't set nan values to nearest grid!")
                    raise ValueError
                else:
                    target_field[nan_locs] = up_vals

    except:
        print("FAILED TO INTERPOLATE!!!")
        print("\nSource grid shapes: ", [g.shape for g in source_grid])
        print("Source filed shape: ", source_field.shape)
        print("source_field: ")
        print(source_field)
        print("\nTarget grid: ")
        print("Target grid shapes: ", [g.shape for g in target_grid])
        print("target_grid")
        print(target_grid)
        print()
        raise

    if verbose:
        print("Interpolation done successfully...")

    return target_field

def velocity_vector_to_radial_velocity(velocity_vec, elevation, azimuth, verbose=False):
    """
    Given the velocity vector (u, v, w) in the cartesian system,
        claculate the radial velocity alog the line of sight
        having a predefined elevation and azimuth

    Args:
        velocity_vec: the velocity vector in cartesian coordinate system.
            velocity_vec can be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries

        elevation, azimuth are elevation angle, and azimuth angle (both in DEGREES) of the projection vector/direction.
            For DL , these are the elevation and azimuth of the line of sight as defined by the LIDAR instrument.

        verbose: turn printing results on/off

    Returns:

        radial_velocity: the velocity observed along the line of sight defined by the passed elevation and azimuth degrees

    """
    try:
        u, v, w = velocity_vec
    except:
        print("Couldn't cast the input 'velocity_vec' into a 1d-array of length 3. ")
        print("The input must be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries")
        raise ValueError

    # Check elevation and azimuth degrees
    assert np.bitwise_and(0 <= elevation, elevation <= 90).all(), "The elevation degree must fall in the interval [0, 90]"
    assert np.bitwise_and(0 <= azimuth, azimuth<= 360).all(), "The azimuth degrees must fall in the interval [0, 360]"

    alpha = np.radians(elevation)
    beta = np.radians(azimuth)

    # The final formula
    radial_velocity  = u * np.cos(alpha) * np.cos(beta)
    radial_velocity += v * np.cos(alpha) * np.sin(beta)
    radial_velocity += w * np.sin(alpha)

    if verbose:
        sep = "*"*70
        print("\n%s\n\tCalculte Radial Velocity from Velocity vecor\n%s" % (sep, sep))
        print("Passed velocity vector (%f, %f, %f)" % (u, v, w))
        print("Elevation Degrees: %f " % elevation)
        print("Azimuth Degrees: %f " % azimuth)
        print("\t Radial velocity is: %f " % radial_velocity)
        print(sep)

    return radial_velocity



# -------------------------------------------- #
# *** DL(doppler-lidar)-specific functions *** #
# -------------------------------------------- #
def get_ARM_DL_coordinates(site, facility, format='degrees'):
    """
    Get the latitude-longitude-altitude location of a DL device
    The return is a tuple of two entries, the first is the lat-lon in the specified format (def is degrees),
    and the second return value is the device altitude in Meters.

    The altitude returned is the height above mean sea level

    """
    # TODO: Involve the year/duration to get precice location, since devices move over time
    # locations in sexagesimal degree format (degrees, minutes, seconds)
    sgp_E32 = ((36, 49, 08.40), (-97, -49, -11.64), 328)  # (Latitude, Longitude, Altitude (m))
    sgp_C1  = ((36, 36, 18.00), (-97, -29, -06.00), 318)
    #
    if re.match(r'\ASGP\Z', site, re.IGNORECASE):
        if re.match(r'\AC1\Z', facility, re.IGNORECASE):
            location = sgp_C1
        elif re.match(r'\AE32\Z', facility, re.IGNORECASE):
            location = sgp_E32
        else:
            print("The facility [%s] under this site [], is not recognized!" % (faciility, site))
            raise ValueError
    else:
        print("The site [%s] is not recognized!" % site)
        raise ValueError

    # location is in sexagesimal-degree format
    altitude = location[-1]
    location = location[: -1]
    if re.match(r'\Adegrees\Z', format, re.IGNORECASE):
        # convert to degrees
        lat = location[0]
        lon = location[1]
        lat_degrees = lat[0] + lat[1] / 60.0 + lat[2] / 3600.0
        lon_degrees = lon[0] + lon[1] / 60.0 + lon[2] / 3600.0
        location = (lat_degrees, lon_degrees)
    elif re.match(r'\Asexagesimal(-|_| )*(degree)*(s)*\Z', format, re.IGNORECASE):
        pass
    else:
        print("Unrecognized format: %s" % format)
        print("Only decimal and sexagesimal-degrees format are supported")
        raise ValueError

    return location, altitude


# ---------------------------------------------------- #
# *** General-purpose Functions to aid simulations *** #
# ---------------------------------------------------- #
def state_to_observation_ensemble(ensemble, model):
    """
    Evaluate the theoritical observations corresponding to model states;
        i.e. apply the observation operator to each entry of the ensemble

    Args:
        ensemble: either an instance of DLidarVec.Ensemble or a list of model states
        model   : the forward operator that gives access to both dynamical model, andobservations

    Returns:
        obs_ensemble: the ensmble of observations equialento to the model states in the passed ensemble

    """
    try:
        obs_size = model.observation_size()
    except:
        print("The passed model doesn't give access to observation_size function! The model should be an instance of 'forward_model', not '%s'" % type(model))
        raise TypeError

    if 'Ensemble' in str(type(ensemble)):
        # obs_ensemble = ensemble.copy_empty_withsize(obs_size)  # TODO
        ens_size = ensemble.size
        obs_ensemble = ensemble.__class__(vec_size=obs_size, ensemble_size=0)
    elif isinstance(ensemble, list):
        ens_size = len(ensemble)
        obs_ensemble = []

    else:
        print("The type of the passed ensemble ['%s'] is not recognized!" % type(ensemble))
        raise TypeError

    # Start adding observation vectors
    for ens_ind in range(ens_size):
        obs_vec = model.evaluate_theoretical_observation(ensemble[ens_ind], ensemble[ens_ind].time)
        obs_ensemble.append(obs_vec)

    return obs_ensemble



