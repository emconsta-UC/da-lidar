
# this generates an object that combines and links funcitonalities of the model dynamics (Py_HyPar), and real
# ../dl_setup.main() MUST be called before instantiating this Model

import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

# Add DLiDA src  path to sys path
DLiDA_src_path = os.path.join(os.path.dirname(__file__), '../../')
if DLiDA_src_path not in sys.path:
    sys.path.insert(2, DLiDA_src_path)
try:
    from DLidarVec import StateVector, Ensemble
    from DL_obs import DL_SITE_OBS
    from DL_data_handler import DL_DATA_HANDLER
    import DL_utility as utility
    from Py_HyPar import HyPar_Model
except(ImportError):
    import dl_setup
    dl_setup.main()
    #
    from DLidarVec import StateVector, Ensemble
    from DL_obs import DL_SITE_OBS
    from DL_data_handler import DL_DATA_HANDLER
    import DL_utility as utility
    from Py_HyPar import HyPar_Model
    #

import numpy as np
import scipy.sparse as sp_sparce
import re

try:
    import ConfigParser
except(ImportError):
    import configparser as ConfigParser

try:
  import h5py
  use_h5py = True
except(ImportError):
  use_h5py = False


class ForwardOperator(object):

    def __init__(self,
                 model_configs,
                 obs_configs,
                 dist_unit='M',
                 verbose=False,
                 obs_err_model_file=None,
                 obs_err_variance=None,
                 space_dependent_obs_err=False):
        """

        model_configs:
            - size: iterable (e.g. list) with nx in each direction, e.g. [101, 101, 41],
            - iproc: iterable (e.g. list) with number of processors in each direction, e.g. [2,2,2]
            - dt: step size for forward propagation, e.g. 0.25
            - n_iter: number of iterations (number of steps) for forward propagation, .e.g 50 -> 50*dt

        obs_configs:
            - site_facility
            - t
            - site_facility
            - dl_coordinates
            - prog_var
            - num_gates=None
            - range_gate_length=None
            - elevations=None
            - azimuthes=None

        dist_unit: either 'M' for meters, or 'KM' for Kilometers. This unifies the units between HyPar model, and DL_obs

        verbose: print things detailed to screen or not
        space_dependent_obs_err: when the observation error model is created; the noise vector is created as a percent of average observations over time; typically 5%;
            if this is turned off; the  observation  variance is set to a constant; This is still naieve and will be updated once an observation error model is developed

        """

        # Initialize Dynamical Model (Py_HyPar)
        self.__model_configs = model_configs
        self._dynamical_model = HyPar_Model(model_configs=model_configs)
        self._model_state_dimension = self._dynamical_model.state_size()

        # Initialize observation handler (DL_DATA_HANDLER); TODO: The interaction between DL_DATA_HANDLER and DL_SITE_OBS needs to be refactored!
        self._observation_handler = DL_DATA_HANDLER()

        # site observation
        if 'dl_coordinates' not in obs_configs:
            obs_configs.update({'dl_coordinates':None})  # it will be updated automatically
        self._site_observation = DL_SITE_OBS(t=obs_configs['t'],
                                             site_facility=obs_configs['site_facility'],
                                             dl_coordinates=obs_configs['dl_coordinates'],
                                             prog_var=obs_configs['prog_var'],
                                             num_gates=obs_configs['num_gates'],
                                             range_gate_length=obs_configs['range_gate_length'],
                                             elevations=obs_configs['elevations'],
                                             azimuthes=obs_configs['azimuthes']
                                             )
        self.__obs_configs = obs_configs
        self._observation_dimension = self._site_observation.observation_size()  # we are observing (u, v, w) at each gridpoint

        # retrieve model grid, observation grid, and construct observation operator
        if dist_unit.lower() not in ['m', 'km']:
            print("'dist_unit' MUST be either 'm' or 'km' not %s" %dist_unit)
            raise ValueError
        self.__dists_unit = dist_unit.lower()
        self._model_grid = self.model_grid()

        # Given the model grid, create the observation grid (shifted to the center of the model grid domain
        # Cartesian and spherical coordinate system of the observation gridpoints
        self._observation_grids = (self.observation_grid('cartesian'), self.observation_grid('spherical'))

        # Observation operator
        self._observation_operator = None
        self._observation_valid_indexes = np.arange(self._observation_dimension)  # indexes in the observation vector (data) with non-NaN values. This will be useful for data asimilation with missing data

        self._verbose = verbose
        # print("Observation Configs: ", self.get_observation_configs())
        #

        # Error models (observation, forecast, and model errors)
        self._frcst_err_model = self._create_frcst_err_model()

        if obs_err_model_file is not None:
            self._obs_err_model = self._obs_error_model_from_file(obs_err_model_file)
        elif obs_err_variance is None:
            self._obs_err_model = self._create_obs_err_model(scale_by_state=space_dependent_obs_err)
        else:
            self._obs_err_model = self._create_obs_err_model(variance=obs_err_variance,
                                                             scale_by_state=space_dependent_obs_err)
        # Diagonal of the observation error covariance matrix; for fast construction
        self.__obs_err_variance = self._obs_err_model.pdf_variance
        self.__obs_err_mean = self._obs_err_model.pdf_mean

        self._model_err_model = self._create_model_err_model()

    def reset_observation_operator(self, *args, **kwargs):
        """
        Reset observation grid and operator to the defaul configs passed to the constructor
        """
        raise NotImplementedError

    def update_observation_operator(self, time=None, observation=None, valid_indexes=None, obs_configs=None):
        """
        Modify the observation grid, and/or the observation operator based on the passed arguments given the following rule (The first condition met, is the only one applied):
            if time is ot None: observation operator is updated givent the real settings at this time. This is not implemented for now
            if valid_indexes is observation is obs_configs is None: Do nothing
            if obs_configs is not None: the observation grid and the data handler are recreated
            if valid_indexes is not None, self valid indexes are updated
            if observation is not None: valid indexes are extracted, and self valid indexes are updted

        Args:
            valid_indexes: an iterable of indexes of in the current observation vector those should be observed; others are set to NaN
            observation: an observation vector from which valid entries (no.Nan) are extracted
            obs_cofiggs: a dictionary of observation configurations used to recreate the whole observation grid object

        Returns:
            None

        """
        if time is observation is valid_indexes is obs_configs is None:
            pass

        elif time is not None:
            print("Observation operator is updated givent the real settings at this time. This is not implemented for now")
            raise NotImplementedError

        elif obs_configs is not None:
            # Update everything related to the observation vector
            # self._observation_handler = DL_DATA_HANDLER()  # TODO: Once this is refactored in the constructor, it should be udpated here
            # site observation
            self._site_observation = DL_SITE_OBS(t=obs_configs['t'],
                                                 site_facility=obs_configs['site_facility'],
                                                 dl_coordinates=obs_configs['dl_coordinates'],
                                                 prog_var=obs_configs['prog_var'],
                                                 num_gates=obs_configs['num_gates'],
                                                 range_gate_length=obs_configs['range_gate_length'],
                                                 elevations=obs_configs['elevations'],
                                                 azimuthes=obs_configs['azimuthes']
                                                 )
            self.__obs_configs = obs_configs
            self._observation_dimension = self._site_observation.observation_size()  # we are observing (u, v, w) at each gridpoint
            self._observation_grids = (self.observation_grid('cartesian'), self.observation_grid('spherical'))
            self._observation_valid_indexes = np.arange(self._observation_dimension)  # indexes in the observation vector (data) with non-NaN values. This will be useful for data asimilation with missing data
            self._obs_err_model = self._create_obs_err_model(mean=self.__obs_err_mean, variance=self.__obs_err_variance)

        elif valid_indexes is not None:
            # Update valid indexes only
            self._observation_valid_indexes = np.asarray(valid_indexes[:]).flatten()
            self._obs_err_model.update_valid_indexes(self._observation_valid_indexes)

        else:
            # Only observation vector is passed
            try:
                valid_indexes = observation.where_not_nan()
            except(AttributeError):
                if not isinstance(observation, StateVector):
                    print("The passed observation is not an instance of StateVector?!")
                    print("Type(observation): %s " % type(observation))
                    raise TypeError
            self._observation_valid_indexes = valid_indexes
            self._obs_err_model.update_valid_indexes(self._observation_valid_indexes)

    def save_error_models(self,
                             location,
                             save_obs_error=True,
                             save_prior_error=False,
                             save_model_error=False,
                             overwrite=False,
                             obs_err_filename="Observation_Error_Model",
                             frcst_err_filename="Prior_Error_Model",
                             model_err_filename="Model_Error_Model"
                            ):
        """
        Write error model information

        Args:
            location :
            obs_error=True :
            prior_error=False :
            model_error=False :
            obs_err_filename="Observation_Error_Model" :
            frcst_err_filename="Prior_Error_Model" :
            model_err_filename="Model_Error_Model" :

        """
        obs_err_filename = "Observation_Error_Model"

        if save_obs_error:
            model_grid = self.model_grid()
            obs_grid = self.observation_grid()
            obs_err_model = self.observation_error_model

            if re.match(r'\A(gaussian|normal)\Z', obs_err_model.density_function, re.IGNORECASE):
                if use_h5py:  # h5py is available
                    filename = os.path.join(location, "%s.hpy" % obs_err_filename)
                    exists = os.path.isfile(filename)
                    if exists and not overwrite:
                        print("Couldn't save the observation error model information to file!")
                        print("File found: %s" % filename)
                        print("Either set overwrite flag to True, remove the file, or choose another destinatiion")
                        print("Nothing to be done...")
                        return
                    else:
                        # either overwrite, or create new file...
                        pass

                    with h5py.File(filename, 'w') as f_id:
                        f_id.create_group(u"Grids")
                        _ds = f_id.create_dataset(u"Grids/model_grid", data=model_grid)
                        _ds = f_id.create_dataset(u"Grids/obs_grid", data=obs_grid)
                        #
                        pdf_data = f_id.create_group(u"PDF")
                        pdf_data.attrs[u"pdf"] = u"%s" % obs_err_model.density_function
                        _ds = pdf_data.create_dataset(u"mean", data=obs_err_model.pdf_mean)
                        _ds = pdf_data.create_dataset(u"variance", data=obs_err_model.pdf_variance)
                    print("Observation Error Model Information saved to : \n  %s " % filename)
                    self.__model_configs.update({'obervation_error_model_file':filename})
                    #
                else:
                    print("Only h5py is currently supported, but it's not available to import; ")
                    # raise NotImplementedError()
                    raise ImportError()
            else:
                print("Only Gaussian/Normal distribution can be saved for now")
                print("%s is not supported" % obs_err_model.density_function)
                raise NotImplementedError()

        if save_prior_error:
            frcst_err_model = self.prior_error_model
            raise NotImplementedError("TODO")

        if save_model_error:
            model_err_model = self.model_error_model
            raise NotImplementedError("TODO")

    def _obs_error_model_from_file(self, filename):
        """
        Args:
            filename: fullpath to observation error data file
        """
        if not os.path.isfile(filename):
            print("Failed to load observation error data")
            print("FILE IO: Couldn't find file %s " % filename)
            raise IOError()
        #
        # attempt to open file with h5py; if not, throw an error
        try:
            with h5py.File(filename, 'r') as f_cont:
                # load data
                f_model_grid = f_cont["Grids/model_grid"][...]
                f_obs_grid = f_cont["Grids/obs_grid"][...]
                f_pdf = f_cont["PDF"].attrs['pdf']
                f_mean = f_cont["PDF/mean"][...]
                f_variance = f_cont["PDF/variance"][...]
                #
                # Assertions...
                compatible = True
                if not np.allclose(f_model_grid, self.model_grid()):
                    compatible = False
                elif not np.allclose(f_obs_grid, self.observation_grid()):
                    compatible = False
                elif not re.match(r'\A(Gaussian|Normal)\Z', f_pdf, re.IGNORECASE):
                    compatible = False
                elif not (f_variance.size == f_mean.size == self.observation_size()):
                    compatible = False
                else:
                    pass  # All good
                # Passed assertions
                if compatible:
                    err_model = self._create_obs_err_model(mean=f_mean, variance=f_variance, scale_by_state=False)
                else:
                    print("Failed to read data from file; Information stored don't match this model instance")
                    # TODO: Maybe printout more information...
                    raise AssertionError()
        except:
            print("Failed to read data from file; either a corrupted file, or wrong format; only h5py is supported!")
            raise IOError()
        #
        return err_model

    def _create_obs_err_model(self, mean=0, variance=1e-2, valid_indexes=None, scale_by_state=True):
        """
        """
        if self._verbose:
            print("CAUTION: This noise model is Naieve. Use it for Testing. A realistic observation error model is required.")

        if utility.isscalar(variance):
            if scale_by_state:
                # we need  to multiply the noise level by some realistic magnitude reflected by the model state
                # create a state ensemble, and use the average of corresponsing observations
                print("Creating Observation Error PDF moments; using an ensemble of model-based observations")
                ensemble = self.create_observation_ensemble(10, fill_ensemble=True)
                for mem in ensemble:
                    mem = mem.abs()
                y = ensemble.mean()
                shift_val = 1e-5
                y *= variance
                y += shift_val  # the  tiny value to avoid zero variance
            else:
                y = variance
        else:
            y = np.asarray(variance)
            assert y.size == self.observation_size(), "Observation error variance must of equal length as the observation dimension %d; received %d" % (y.size,
                                                                                                                                                        self.observation_size())

        return GaussianErrorModel(self._observation_dimension, mean=0.0, variance=y)

    def _create_frcst_err_model(self, variance=1e-4):
        """
        """
        print("Creating Modeled-Prior PDF moments")
        return GaussianErrorModel(self._model_state_dimension, mean=0.0, variance=variance)

    def _create_model_err_model(self):
        """
        """
        return None

    def get_model_configs(self, key=None):
        model_configs = self._dynamical_model.copy_model_configs()
        if key is not None:
            try:
                config = model_configs[key]
            except KeyError:
                config = None
            finally:
                if config is None:
                    print("Passed key [%s] couldn't be found in the model configs dictionary!" %key)
                    raise KeyError
        else:
            config = model_configs
        return config

    def get_observation_configs(self, key=None):
        obs_configs = self._site_observation.copy_observation_configs()
        if key is not None:
            try:
                config = obs_configs[key]
            except KeyError:
                config = None
            finally:
                if config is None:
                    print("Passed key [%s] couldn't be found in the observation configs dictionary!" %key)
                    raise KeyError
        else:
            config = obs_configs
        return config

    def state_size(self):
        return self._dynamical_model.state_size()

    def observation_size(self):
        return self._observation_dimension

    def current_model_state(self):
        """
        """
        return self._dynamical_model.get_current_state()

    def create_state_ensemble(self, ensemble_size, fill_ensemble=False, t=0.0, add_noise=True):
        """
        create an ensemble of model state vectors; if fill ensemble is False,
        the returned Ensemble members are just memory-allocated, with garbage entries

        Args:
            ensemble_size: number of entries of the returned ensemble object; won't be used unless fill_ensemble is True
            fill_ensemble (def False): fill the ensemble object from a forward-propagated trajectory
            t: (def 0.0): the time set to the entries of the ensemble members;

        Returns:
            ensemble: an instance of the DL_Vec.Ensemble, that supports list operations, and statistical QoIs

        """
        if fill_ensemble:
            print("Creating an ensemble of size: %d; " % ensemble_size)
            ensemble = Ensemble(0, self._model_state_dimension)  # TODO: check sequential option
            x0 = self.current_model_state().copy()
            wind_length = 10.0
            try:
                self._frcst_err_model
            except(AttributeError):
                if add_noise:
                    if self._verbose:
                        print("Can't use random purturbation to initialize model state. Skipping")
                    add_noise = False
            if add_noise:
                x0 = self._frcst_err_model.add_noise(x0, in_place=True)
            else:
                x0 += 1.0
            for i in range(ensemble_size):
                if self._verbose:
                    sys.stdout.write("\rEnsemble membber %03d" % i)
                    sys.stdout.flush()
                else:
                    sys.stdout.write(".")
                    sys.stdout.flush()
                #
                _, traject = self.integrate_state(x0,
                                                  checkpoints=[x0.time, x0.time+wind_length],
                                                  model_screen_output=self._verbose
                                                 )
                x0 = traject[-1]
                x0.time = t
                ensemble.append(x0)
        else:
            # allocate memory
            ensemble = Ensemble(ensemble_size, self._model_state_dimension)  #
            # set time
            for mem in ensemble:
                mem.time = t
        #
        print("done.")
        return ensemble

    def create_observation_ensemble(self, ensemble_size, fill_ensemble=False, t=0.0, add_noise=True):
        """
        create an ensemble of observation vectors;
        the returned Ensemble members are just memory-allocated, with garbage entries

        Args:
            ensemble_size: number of entries of the returned ensemble object

        Returns:
            ensemble: an instance of the DL_Vec.Ensemble, that supports list operations, and statistical QoIs
        """
        if fill_ensemble:
            print("Creating an ensemble of size: %d; " % ensemble_size)
            ensemble = Ensemble(0, self._observation_dimension)  # TODO: check sequential option
            x0 = self.current_model_state().copy()
            wind_length = 10.0
            if add_noise:
                x0 = self._frcst_err_model.add_noise(x0, in_place=True)
            else:
                x0 += 1.0
            for i in range(ensemble_size):
                if self._verbose:
                    sys.stdout.write("\rEnsemble membber %03d" % i)
                    sys.stdout.flush()
                else:
                    sys.stdout.write(".")
                    sys.stdout.flush()
                #
                _, traject = self.integrate_state(x0,
                                                  checkpoints=[x0.time, x0.time+wind_length],
                                                  model_screen_output=self._verbose
                                                 )
                x0 = traject[-1]
                x0.time = t
                ensemble.append(self.evaluate_theoretical_observation(x0, t=t))
        else:
            ensemble = Ensemble(ensemble_size, self._observation_dimension)
            # set time
            for mem in ensemble:
                mem.time = t
        #
        print("done.")
        return ensemble

    def get_real_observations(self, timespan, return_ensemble=False, aggregate_data=True, mismatch_policy='exact', snr_threshold =0.008, collect_remote_data=False):
        """
        Return a list or an ensemble instance of real LiDAR observations,
            give the observational grid settings of this model

        Args:
            timespan:
            return_ensemble
            aggregate_data
            mismatch_policy: 'nearest' or 'exact'
            snr_threshold
            collect_remote_data

        Returns:

        """
        site_obs_handler = self._observation_handler
        field = self.__obs_configs['prog_var']
        site_facility = self.__obs_configs['site_facility']

        times, _observations = site_obs_handler.get_DL_site_observations(field=field,
                                                                         site_facility=site_facility,
                                                                         timespan=timespan,
                                                                         aggregate_data=aggregate_data,
                                                                         snr_threshold=snr_threshold,
                                                                         collect_remote_data=collect_remote_data,
                                                                         return_ensemble=return_ensemble
                                                                        )

        # get site observations (spherical) coordinates
        obs_coordinates = self._site_observation.get_local_coordinates('spherical')

        # extract observations at the predefined observation grid points
        observations = []  # consider overwriting _observations instead
        if _observations is not None:
            for _obs in _observations:
                if _obs is None:
                    #  obs = None
                    obs = self.observation_vector()
                    obs[:] = np.NaN
                else:
                    obs = _obs
                    projected_obs = obs.map_to_coordinates(obs_coordinates, coordinate_system='spherical', mismatch_policy=mismatch_policy)
                    projected_obs = np.ravel(np.asarray(projected_obs))  # reshape, and assign to obs
                    obs = self.observation_vector()
                    try:
                        obs[:] = projected_obs[:]
                    except:
                        print("Failed to assign projected observations to the observation vector; check sizes, and prognostic varianbels")
                        print("obs.size", obs.size)
                        print("projected_obs.size", projected_obs.shape)
                        print("obs_coordinates.shape", obs_coordinates.shape)
                        raise
                    observations.append(obs)
        else:
            for t in times:
                obs = self.observation_vector()
                obs[:] = np.NaN
                observations.append(obs)
        return times, observations

    def evaluate_theoretical_observation(self, state, t=None, method='interpolate', missing_as_nan=True, correct_invalid_indexes=False, sp_interpolation=False):
        """
        Return H_t(x), where H_t is the observation operator at time t, and x is a model state
        Time here for DL data is expected to be ignored in general, and the observation operator will be time independent.
        Missing data will be handlded in the assimilation system.

        Here the observation operator extracts the wind velocity filed components (u, v, w) at each observation grid point;
            this involves trilinear interpolation for points inside a valid cubic stincil of model grid points
        if missing_as_nan is True, observations those couldn't be interpolated are replaced with nan
        if correct_invalid_indexes is True, given observation nan_indexes, fill entries with NaN values

        Remarks:
            All prognostic variables at a given gridpoint are stored near each other in the observation vector
            This function is under debugging; comparing scipy interpolationn to the interpolation developed here
        """
        if sp_interpolation:
            res = self._sp_evaluate_theoretical_observation(state=state, t=t, method=method, missing_as_nan=missing_as_nan, correct_invalid_indexes=correct_invalid_indexes)
        else:
            res = self._lc_evaluate_theoretical_observation(state=state, t=t, method=method, missing_as_nan=missing_as_nan, correct_invalid_indexes=correct_invalid_indexes)
        return res

    def _sp_evaluate_theoretical_observation(self, state, t=None, method='interpolate', missing_as_nan=True, correct_invalid_indexes=False):
        """
        return H_t(x), where H_t is the observation operator at time t, and x is a model state
        Time here for DL data is expected to be ignored in general, and the observation operator will be time independent.
        Missing data will be handlded in the assimilation system.

        Here the observation operator extracts the wind velocity filed components (u, v, w) at each observation grid point;
            this involves trilinear interpolation for points inside a valid cubic stincil of model grid points
        if missing_as_nan is True, observations those couldn't be interpolated are replaced with nan
        if correct_invalid_indexes is True, given observation nan_indexes, fill entries with NaN values

        Remarks:
            This function is similar to _lc_evaluate_theoretical_observation except that here, scipy interpolation functions are invoked
            All prognostic variables at a given gridpoint are stored near each other in the observation vector

        """
        prog_var = self.__obs_configs['prog_var']
        num_prog_vars = 1
        if re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
            pass
        elif re.match(r'\Aradial(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            pass
        elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            num_prog_vars *= 3
        else:
            print("Observation opertor is not defined for a prognostic variable: %s" % prog_var)
            raise ValueError

        model = self._dynamical_model

        # Of course, we won't keep that for long; we will convert x into y later on the fly (matrix free)
        # model_grid = self.model_grid()  #  (Nx*Ny*Nz) x 3 np array, with each row giving a xyz combination of one grid point

        # dimension of the observation operator depends on the prog_var
        obs_grid = self.observation_grid('cartesian')  # Same as model grid, but representing observation grid points
        obs_x, obs_y, obs_z = obs_grid[:, 0], obs_grid[:, 1], obs_grid[:, 2]
        spherical_coordinates = self.observation_grid('spherical')

        obs = self.observation_vector()
        #
        # Now, we need to interpolate the target variable from model spece into observation space

        # Check the bouns of the model and observation grid, and check if all observation gridpoints are within model spatial domain

        if re.match(r"\Anear(est)*\Z", method, re.IGNORECASE):
            print("Not Implemented Yet")
            raise NotImplementedError

        elif re.match(r"\Ainterp(olate|olation)*\Z", method, re.IGNORECASE):

            # Interpolated U, V, W from model grid to observation gridpoints
            model_x, model_y, model_z, model_u, model_v, model_w = self._dynamical_model.get_gridded_velocity_field(state)
            print("STARTING SCIPY-BASED Interpolating ...")
            print("THIS WILL TAKE LONG TIME; Use _lc_evaluate_theoretical_observation instead of this after validation")
            print("Interpolating U")
            # print("Source Grid: ", model_x, model_y, model_z)
            # print("Trarget Grid: ", obs_x, obs_y, obs_z)
            # print("Source Field: ", model_w)
            obs_u = utility.interpolate_field(source_grid=(model_x, model_y, model_z), source_field=model_u, target_grid=(obs_x, obs_y, obs_z), method='linear')
            print("Interpolating V")
            obs_v = utility.interpolate_field(source_grid=(model_x, model_y, model_z), source_field=model_v, target_grid=(obs_x, obs_y, obs_z), method='linear')
            print("Interpolating W")
            obs_w = utility.interpolate_field(source_grid=(model_x, model_y, model_z), source_field=model_w, target_grid=(obs_x, obs_y, obs_z), method='linear')
            print("Interpolating DONE...")

            # Now Check if uvw needs to be converted to other quantity (e.g. doppler, radial velocity, et.)
            # Append results to the observation vector
            if re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                # obs[obs_ind*3 :obs_ind*3+3] = u, v, w
                obs_u_inds,  obs_v_inds, obs_w_inds = self._site_observation.get_prognostic_variables_indexes()
                obs[u_inds] = obs_u
                obs[v_inds] = obs_v
                obs[w_inds] = obs_w

            elif re.match(r'\Aradial(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                # Radial velocity just projection onto the line of sight
                elevation = 90 - spherical_coordinates[:, 1]
                azimuth = spherical_coordinates[:, 2]
                rv = utility.velocity_vector_to_radial_velocity([obs_u, obs_v, obs_w], elevation=elevation, azimuth=azimuth, verbose=False)
                obs[:] = rv
            elif re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
                elevation = 90 - spherical_coordinates[:, 1]
                azimuth = spherical_coordinates[:, 2]
                rv = utility.velocity_vector_to_radial_velocity([obs_u, obs_v, obs_w], elevation=elevation, azimuth=azimuth, verbose=False)
                obs[:] = self._site_observation.radial_velocity_to_doppler(rv)
            else:
                print("Observation opertor is not defined for a prognostic variable: %s" % prog_var)
                raise ValueError

            # update observation time
            if t is None:
                try:
                    t = state.time
                except:
                    pass

            if t is not None:
                try:
                    if obs.time != t:
                        obs.time = t
                except:
                    print("Failed to update observation time")
                    pass

        else:
            print("Unsupported method %s !" % method)
            raise ValueError

        if correct_invalid_indexes and self._observation_valid_indexes is not None:
            if self._observation_valid_indexes.size == obs.size:
                pass
            elif self._observation_valid_indexes.size < obs.size:
                full_obs = obs
                obs = self.observation_vector()
                obs[:] = np.NaN
                obs[self._observation_valid_indexes] = full_obs[self._observation_valid_indexes]
            else:
                print("obs.size > self._observation_valid_indexes!!!")
                raise ValueError

        return obs

    def _lc_evaluate_theoretical_observation(self, state, t=None, method='interpolate', missing_as_nan=True, correct_invalid_indexes=False):
        """
        return H_t(x), where H_t is the observation operator at time t, and x is a model state
        Time here for DL data is expected to be ignored in general, and the observation operator will be time independent.
        Missing data will be handlded in the assimilation system.

        Here the observation operator extracts the wind velocity filed components (u, v, w) at each observation grid point;
            this involves trilinear interpolation for points inside a valid cubic stincil of model grid points
        if missing_as_nan is True, observations those couldn't be interpolated are replaced with nan
        if correct_invalid_indexes is True, given observation nan_indexes, fill entries with NaN values

        Remarks:
            All prognostic variables at a given gridpoint are stored near each other in the observation vector

        """
        prog_var = self.__obs_configs['prog_var']
        num_prog_vars = 1
        if re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
            pass
        elif re.match(r'\Aradial(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            pass
        elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            num_prog_vars *= 3
        else:
            print("Observation opertor is not defined for a prognostic variable: %s" % prog_var)
            raise ValueError

        model = self._dynamical_model

        # Of course, we won't keep that for long; we will convert x into y later on the fly (matrix free)
        model_grid = self.model_grid()
        model_Xs, model_Ys, model_Zs = list(set(model_grid[:, 0])), list(set(model_grid[:, 1])), list(set(model_grid[:, 2]))
        model_Xs.sort()
        model_Ys.sort()
        model_Zs.sort()
        model_Xs, model_Ys, model_Zs = np.asarray(model_Xs), np.asarray(model_Ys), np.asarray(model_Zs)

        # dimension of the observation operator depends on the prog_var
        obs_grid = self.observation_grid('cartesian')
        num_grid_points = self._observation_dimension // num_prog_vars
        spherical_coordinates = self.observation_grid('spherical')

        # sanity check
        if num_grid_points != np.size(obs_grid, 0):
            print("There is an issue with the observation grid!")
            print("Number of observaiton grid points SHOULD be: %d" % num_grid_points)
            print("The shape of the observation grid is: %s" %repr(np.shape(obs_grid)))
            raise ValueError
            #
        obs = self.observation_vector()
        #
        # Check the bouns of the model and observation grid, and check if all observation gridpoints are within model spatial domain
        model_grid_lim = model_grid[-1, :]
        obs_grid_lim = obs_grid[-1, :]
        if (obs_grid_lim > model_grid_lim).any():
            if self._verbose:
                print("Some of the the observation gridpoints are out of the model domain")
            if not missing_as_nan:
                print("Terminating since missing_as_nan is set to False")
            elif self._verbose:
                print("Nan will be set to values at such observation grid pints")

        if re.match(r"\Anear(est)*\Z", method, re.IGNORECASE):
            print("Not Implemented Yet")
            raise NotImplementedError

        elif re.match(r"\Ainterp(olate|olation)*\Z", method, re.IGNORECASE):

            P = np.zeros(8, dtype=np.float32)
            C = np.zeros(8, dtype=np.float32)
            Q = np.zeros(8, dtype=np.float32)
            B = np.zeros((8, 8), dtype=int)  # It's already tiny, but we can make it sparse, e.g. scipy.scr_matrix
            B[0, 0] = B[1, 4] = B[2, 2] = B[3, 1] = B[4, 0] = B[4, 6] = B[5, 0] = B[5, 3] = B[6, 0] = B[6, 5] = B[7, 1:3] = B[7, 4] = B[7, 7] = 1
            B[1, 0] = B[2, 0] = B[3, 0] = B[4, 2] = B[4, 4] = B[5, 1:3] = B[6, 1] = B[6, 4] = B[7, 0] = B[7, 3] = B[7, 5:7] = -1
            #
            # B = np.array([[ 1,  0,  0,  0,  0,  0,  0,  0],
            #               [-1,  0,  0,  0,  1,  0,  0,  0],
            #               [-1,  0,  1,  0,  0,  0,  0,  0],
            #               [-1,  1,  0,  0,  0,  0,  0,  0],
            #               [ 1,  0, -1,  0, -1,  0,  1,  0],
            #               [ 1, -1, -1,  1,  0,  0,  0,  0],
            #               [ 1, -1,  0,  0, -1,  1,  0,  0],
            #               [-1,  1,  1, -1,  1, -1, -1,  1]])
            #
            # TODO: Consider Prism Linear Interpolation for cases where observation gridpoint are outside the model domain but within a slice of a cubic stincil
            # Loop over all observation indexes, and calculate the observations
            #
            for obs_ind in range(num_grid_points):
                # print("Obs Ind: %d " %obs_ind)

                x, y, z = obs_grid[obs_ind, :]
                # print("Looking for index [%d] with coordinates: (%f, %f, %f)" % (obs_ind, x, y, z))

                # get nearest x above and below:
                try:
                    below = np.where(model_Xs<=x)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs>x)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print("This is IMPOSSIBLE unless no grid points exist!")
                    raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    xbelow = model_Xs[below]
                    xabove = model_Xs[above]

                # get nearest y above and below:
                try:
                    below = np.where(model_Ys<=y)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Ys>y)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print("This is IMPOSSIBLE unless not grid points exist!")
                    raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    ybelow = model_Ys[below]
                    yabove = model_Ys[above]

                # get nearest z above and below:
                try:
                    below = np.where(model_Zs<=z)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Zs>z)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        print("Failed to cover observation grid x,y,z >> ", x, y, z)
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    zbelow = model_Zs[below]
                    zabove = model_Zs[above]

                # # Make sure the dynamical model sees distances meters:
                # if self.__dists_unit == 'm':
                #     pass
                # elif self.__dists_unit == 'km':
                #     xbelow, ybelow, zbelow = xbelow*1000, ybelow*1000, zbelow*1000
                #     xabove, yabove, zabove = xbelow*1000, yabove*1000, zabove*1000
                # else:
                #     print("Unsupported distance unit %s" % self.__dists_unit)
                #     raise ValueError

                #
                # Corners of the lattice:
                X_000 = (xbelow, ybelow, zbelow)
                X_001 = (xbelow, ybelow, zabove)

                X_010 = (xbelow, yabove, zbelow)
                X_011 = (xbelow, yabove, zabove)

                X_100 = (xabove, ybelow, zbelow)
                X_101 = (xabove, ybelow, zabove)

                X_110 = (xabove, above, zbelow)
                X_111 = (xabove, above, zabove)

                # get indexes of these corners in the model state
                X_000_ind = model.gridpoint_to_index(xbelow, ybelow, zbelow)
                X_001_ind = model.gridpoint_to_index(xbelow, ybelow, zabove)
                X_010_ind = model.gridpoint_to_index(xbelow, yabove, zbelow)
                X_011_ind = model.gridpoint_to_index(xbelow, yabove, zabove)

                X_100_ind = model.gridpoint_to_index(xabove, ybelow, zbelow)
                X_101_ind = model.gridpoint_to_index(xabove, ybelow, zabove)
                X_110_ind = model.gridpoint_to_index(xabove, yabove, zbelow)
                X_111_ind = model.gridpoint_to_index(xabove, yabove, zabove)

                if (X_000_ind<0)or(X_001_ind<0)or(X_010_ind<0)or(X_011_ind<0)or(X_100_ind<0)or(X_101_ind<0)or(X_110_ind<0)or(X_111_ind<0):
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("Some of the grid points couldn't be found in the model grid, e.g.")
                        print(X_000, X_001, X_010, X_011, X_100, X_101, X_110, X_111)
                        print(X_000_ind, X_001_ind, X_010_ind, X_011_ind, X_100_ind, X_101_ind, X_110_ind, X_111_ind)
                        print(xbelow, ybelow, zbelow)
                        print(xabove, yabove, zabove)
                        print("\n Terminating...\n")
                        raise ValueError

                # At this points; observations at this grid point are not missing; proceed with calculations

                #
                # print(X_000_ind, X_001_ind, X_010_ind, X_011_ind, X_100_ind, X_101_ind, X_110_ind, X_111_ind, state[0], state[1])
                # Interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)
                dy = float(y - ybelow) / (yabove - ybelow)
                dz = float(z - zbelow) / (zabove - zbelow)

                Q[:] = 1, dx, dy, dz, dx*dy, dy*dz, dz*dx, dx*dy*dz

                # Interpolate u:
                P[0] = state[X_000_ind+1] / state[X_000_ind]
                P[1] = state[X_001_ind+1] / state[X_001_ind]
                P[2] = state[X_010_ind+1] / state[X_010_ind]
                P[3] = state[X_011_ind+1] / state[X_011_ind]
                P[4] = state[X_100_ind+1] / state[X_100_ind]
                P[5] = state[X_101_ind+1] / state[X_101_ind]
                P[6] = state[X_110_ind+1] / state[X_110_ind]
                P[7] = state[X_111_ind+1] / state[X_111_ind]
                u = np.dot( Q, B.dot(P) )

                # Interpolate v:
                P[0] = state[X_000_ind+2] / state[X_000_ind]
                P[1] = state[X_001_ind+2] / state[X_001_ind]
                P[2] = state[X_010_ind+2] / state[X_010_ind]
                P[3] = state[X_011_ind+2] / state[X_011_ind]
                P[4] = state[X_100_ind+2] / state[X_100_ind]
                P[5] = state[X_101_ind+2] / state[X_101_ind]
                P[6] = state[X_110_ind+2] / state[X_110_ind]
                P[7] = state[X_111_ind+2] / state[X_111_ind]
                v = np.dot( Q, B.dot(P) )

                # Interpolate w:
                P[0] = state[X_000_ind+3] / state[X_000_ind]
                P[1] = state[X_001_ind+3] / state[X_001_ind]
                P[2] = state[X_010_ind+3] / state[X_010_ind]
                P[3] = state[X_011_ind+3] / state[X_011_ind]
                P[4] = state[X_100_ind+3] / state[X_100_ind]
                P[5] = state[X_101_ind+3] / state[X_101_ind]
                P[6] = state[X_110_ind+3] / state[X_110_ind]
                P[7] = state[X_111_ind+3] / state[X_111_ind]
                w = np.dot( Q, B.dot(P) )

                # Append results to the observation vector
                if re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                    # obs[obs_ind*3 :obs_ind*3+3] = u, v, w
                    # print("u, v, w: ", u, v, w)
                    obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = u, v, w

                elif re.match(r'\Aradial(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                    # Radial velocity just projection onto the line of sight
                    elevation = 90 - spherical_coordinates[obs_ind, 1]
                    azimuth = spherical_coordinates[obs_ind, 2]
                    rv = utility.velocity_vector_to_radial_velocity([u, v, w], elevation=elevation, azimuth=azimuth, verbose=False)
                    obs[obs_ind] = rv
                elif re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
                    elevation = 90 - spherical_coordinates[obs_ind, 1]
                    azimuth = spherical_coordinates[obs_ind, 2]
                    rv = utility.velocity_vector_to_radial_velocity([u, v, w], elevation=elevation, azimuth=azimuth, verbose=False)
                    obs[obs_ind] = self._site_observation.radial_velocity_to_doppler(rv)
                else:
                    print("Observation opertor is not defined for a prognostic variable: %s" % prog_var)
                    raise ValueError

            # update observation time
            if t is None:
                try:
                    t = state.time
                except:
                    pass

            if t is not None:
                try:
                    if obs.time != t:
                        obs.time = t
                except:
                    print("Failed to update observation time")
                    pass

        else:
            print("Unsupported method %s !" % method)
            raise ValueError

        if correct_invalid_indexes and self._observation_valid_indexes is not None:
            if self._observation_valid_indexes.size == obs.size:
                pass
            elif self._observation_valid_indexes.size < obs.size:
                full_obs = obs
                obs = self.observation_vector()
                obs[:] = np.NaN
                obs[self._observation_valid_indexes] = full_obs[self._observation_valid_indexes]
            else:
                print("obs.size > self._observation_valid_indexes!!!")
                raise ValueError

        return obs

    def observation_operator_Jacobian_VecProd(self, eval_state, state, t=None, method='interpolate', missing_as_nan=True, correct_invalid_indexes=False):
        """
        Return the H * state, where H is the Jacobaian of the observation operator evaluated at eval_state

        if missing_as_nan is True, Jacobain entries at these points is set to zero, otherwise ValueError exception is raised
        correct_invalid_indexes: same as in evaluate_theoretical_observation
        """
        prog_var = self.__obs_configs['prog_var']
        num_prog_vars = 1
        if re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
            pass
        elif re.match(r'\Aradial(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            pass
        elif re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            num_prog_vars *= 3
        else:
            print("Observation opertor is not defined for a prognostic variable: %s" % prog_var)
            raise ValueError

        model = self._dynamical_model

        # Of course, we won't keep that for long; we will convert x into y later on the fly (matrix free)
        model_grid = self.model_grid()
        model_Xs, model_Ys, model_Zs = list(set(model_grid[:, 0])), list(set(model_grid[:, 1])), list(set(model_grid[:, 2]))
        model_Xs.sort()
        model_Ys.sort()
        model_Zs.sort()
        model_Xs, model_Ys, model_Zs = np.asarray(model_Xs), np.asarray(model_Ys), np.asarray(model_Zs)

        # dimension of the observation operator depends on the prog_var
        obs_grid = self.observation_grid('cartesian')
        num_grid_points = self._observation_dimension // num_prog_vars
        spherical_coordinates = self.observation_grid('spherical')
        # sanity check
        if num_grid_points != np.size(obs_grid, 0):
            print("There is an issue with the observation grid!")
            print("Number of observaiton grid points SHOULD be: %d" % num_grid_points)
            print("The shape of the observation grid is: %s" %repr(np.shape(obs_grid)))
            raise ValueError
            #
        obs = self.observation_vector()
        #
        if re.match(r"\Anear(est)*\Z", method, re.IGNORECASE):
            print("Not Implemented Yet")
            raise NotImplementedError

        elif re.match(r"\Ainterp(olate|olation)*\Z", method, re.IGNORECASE):

            P = np.zeros(8, dtype=np.float32)
            C = np.zeros(8, dtype=np.float32)
            Q = np.zeros(8, dtype=np.float32)
            B = np.zeros((8, 8), dtype=int)  # It's already tiny, but we can make it sparse, e.g. scipy.scr_matrix
            B[0, 0] = B[1, 4] = B[2, 2] = B[3, 1] = B[4, 0] = B[4, 6] = B[5, 0] = B[5, 3] = B[6, 0] = B[6, 5] = B[7, 1:3] = B[7, 4] = B[7, 7] = 1
            B[1, 0] = B[2, 0] = B[3, 0] = B[4, 2] = B[4, 4] = B[5, 1:3] = B[6, 1] = B[6, 4] = B[7, 0] = B[7, 3] = B[7, 5:7] = -1

            #
            for obs_ind in range(num_grid_points):
                # print("Obs Ind: %d " %obs_ind)

                x, y, z = obs_grid[obs_ind, :]
                # print("Looking for index [%d] with coordinates: (%f, %f, %f)" % (obs_ind, x, y, z))

                # get nearest x above and below:
                try:
                    below = np.where(model_Xs<=x)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs>x)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print("This is IMPOSSIBLE unless not grid points exist!")
                    raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    xbelow = model_Xs[below]
                    xabove = model_Xs[above]
                    #

                # get nearest y above and below:
                try:
                    below = np.where(model_Ys<=y)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Ys>y)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print("This is IMPOSSIBLE unless not grid points exist!")
                    raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    ybelow = model_Ys[below]
                    yabove = model_Ys[above]
                    #

                # get nearest z above and below:
                try:
                    below = np.where(model_Zs<=z)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Zs>z)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    zbelow = model_Zs[below]
                    zabove = model_Zs[above]
                    #

                # At this points; observations at this grid point are not missing; proceed with calculations

                #
                # # Make sure the dynamical model sees distances meters:
                # if self.__dists_unit == 'm':
                #     pass
                # elif self.__dists_unit == 'km':
                #     xbelow, ybelow, zbelow = xbelow*1000, ybelow*1000, zbelow*1000
                #     xabove, yabove, zabove = xbelow*1000, yabove*1000, zabove*1000
                # else:
                #     print("Unsupported distance unit %s" % self.__dists_unit)
                #     raise ValueError

                #
                # Corners of the lattice around the current observation grid point:
                X_000 = (xbelow, ybelow, zbelow)
                X_001 = (xbelow, ybelow, zabove)

                X_010 = (xbelow, yabove, zbelow)
                X_011 = (xbelow, yabove, zabove)

                X_100 = (xabove, ybelow, zbelow)
                X_101 = (xabove, ybelow, zabove)

                X_110 = (xabove, above, zbelow)
                X_111 = (xabove, above, zabove)

                # get indexes of these corners in the model state
                X_000_ind = model.gridpoint_to_index(xbelow, ybelow, zbelow)
                X_001_ind = model.gridpoint_to_index(xbelow, ybelow, zabove)
                X_010_ind = model.gridpoint_to_index(xbelow, yabove, zbelow)
                X_011_ind = model.gridpoint_to_index(xbelow, yabove, zabove)

                X_100_ind = model.gridpoint_to_index(xabove, ybelow, zbelow)
                X_101_ind = model.gridpoint_to_index(xabove, ybelow, zabove)
                X_110_ind = model.gridpoint_to_index(xabove, yabove, zbelow)
                X_111_ind = model.gridpoint_to_index(xabove, yabove, zabove)

                if (X_000_ind<0)or(X_001_ind<0)or(X_010_ind<0)or(X_011_ind<0)or(X_100_ind<0)or(X_101_ind<0)or(X_110_ind<0)or(X_111_ind<0):
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        # obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = [np.nan] * num_prog_vars
                        continue
                    else:
                        print("Some of the grid points couldn't be found in the model grid, e.g.")
                        print(X_000, X_001, X_010, X_011, X_100, X_101, X_110, X_111)
                        print(X_000_ind, X_001_ind, X_010_ind, X_011_ind, X_100_ind, X_101_ind, X_110_ind, X_111_ind)
                        print(xbelow, ybelow, zbelow)
                        print(xabove, yabove, zabove)
                        print("\n Terminating...\n")
                        raise ValueError

                #
                # print(X_000_ind, X_001_ind, X_010_ind, X_011_ind, X_100_ind, X_101_ind, X_110_ind, X_111_ind, state[0], state[1])
                # Interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)
                dy = float(y - ybelow) / (yabove - ybelow)
                dz = float(z - zbelow) / (zabove - zbelow)

                Q[:] = 1, dx, dy, dz, dx*dy, dy*dz, dz*dx, dx*dy*dz

                # Now, rather than interpolation at that gridpoint, we need derivative of (u, v, w), w.r.t each of the corners of
                # derivative of u:
                del_u = 0
                del_u += (  Q.dot(B[:, 0]) * (1.0/eval_state[X_000_ind]) ) * state[X_000_ind+1]
                del_u += (  Q.dot(B[:, 1]) * (1.0/eval_state[X_001_ind]) ) * state[X_001_ind+1]
                del_u += (  Q.dot(B[:, 2]) * (1.0/eval_state[X_010_ind]) ) * state[X_010_ind+1]
                del_u += (  Q.dot(B[:, 3]) * (1.0/eval_state[X_011_ind]) ) * state[X_011_ind+1]
                del_u += (  Q.dot(B[:, 4]) * (1.0/eval_state[X_100_ind]) ) * state[X_100_ind+1]
                del_u += (  Q.dot(B[:, 5]) * (1.0/eval_state[X_101_ind]) ) * state[X_101_ind+1]
                del_u += (  Q.dot(B[:, 6]) * (1.0/eval_state[X_110_ind]) ) * state[X_110_ind+1]
                del_u += (  Q.dot(B[:, 7]) * (1.0/eval_state[X_111_ind]) ) * state[X_111_ind+1]

                # derivative of v:
                del_v = 0
                del_v += (  Q.dot(B[:, 0]) * (1.0/eval_state[X_000_ind]) ) * state[X_000_ind+2]
                del_v += (  Q.dot(B[:, 1]) * (1.0/eval_state[X_001_ind]) ) * state[X_001_ind+2]
                del_v += (  Q.dot(B[:, 2]) * (1.0/eval_state[X_010_ind]) ) * state[X_010_ind+2]
                del_v += (  Q.dot(B[:, 3]) * (1.0/eval_state[X_011_ind]) ) * state[X_011_ind+2]
                del_v += (  Q.dot(B[:, 4]) * (1.0/eval_state[X_100_ind]) ) * state[X_100_ind+2]
                del_v += (  Q.dot(B[:, 5]) * (1.0/eval_state[X_101_ind]) ) * state[X_101_ind+2]
                del_v += (  Q.dot(B[:, 6]) * (1.0/eval_state[X_110_ind]) ) * state[X_110_ind+2]
                del_v += (  Q.dot(B[:, 7]) * (1.0/eval_state[X_111_ind]) ) * state[X_111_ind+2]

                # derivative of w:
                del_w = 0
                del_w += (  Q.dot(B[:, 0]) * (1.0/eval_state[X_000_ind]) ) * state[X_000_ind+3]
                del_w += (  Q.dot(B[:, 1]) * (1.0/eval_state[X_001_ind]) ) * state[X_001_ind+3]
                del_w += (  Q.dot(B[:, 2]) * (1.0/eval_state[X_010_ind]) ) * state[X_010_ind+3]
                del_w += (  Q.dot(B[:, 3]) * (1.0/eval_state[X_011_ind]) ) * state[X_011_ind+3]
                del_w += (  Q.dot(B[:, 4]) * (1.0/eval_state[X_100_ind]) ) * state[X_100_ind+3]
                del_w += (  Q.dot(B[:, 5]) * (1.0/eval_state[X_101_ind]) ) * state[X_101_ind+3]
                del_w += (  Q.dot(B[:, 6]) * (1.0/eval_state[X_110_ind]) ) * state[X_110_ind+3]
                del_w += (  Q.dot(B[:, 7]) * (1.0/eval_state[X_111_ind]) ) * state[X_111_ind+3]

                # Append results to the observation vector
                if re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                    # update entries
                    obs[obs_ind*num_prog_vars :(obs_ind+1)*num_prog_vars] = del_u, del_v, del_w

                elif re.match(r'\A(doppler|(radial(-|_)*(velocity|speed)(_|-)*(field)*))\Z', prog_var, re.IGNORECASE):
                    #
                    # Interpolate u:
                    P[0] = state[X_000_ind+1] / state[X_000_ind]
                    P[1] = state[X_001_ind+1] / state[X_001_ind]
                    P[2] = state[X_010_ind+1] / state[X_010_ind]
                    P[3] = state[X_011_ind+1] / state[X_011_ind]
                    P[4] = state[X_100_ind+1] / state[X_100_ind]
                    P[5] = state[X_101_ind+1] / state[X_101_ind]
                    P[6] = state[X_110_ind+1] / state[X_110_ind]
                    P[7] = state[X_111_ind+1] / state[X_111_ind]
                    u = np.dot( Q, B.dot(P) )

                    # Interpolate v:
                    P[0] = state[X_000_ind+2] / state[X_000_ind]
                    P[1] = state[X_001_ind+2] / state[X_001_ind]
                    P[2] = state[X_010_ind+2] / state[X_010_ind]
                    P[3] = state[X_011_ind+2] / state[X_011_ind]
                    P[4] = state[X_100_ind+2] / state[X_100_ind]
                    P[5] = state[X_101_ind+2] / state[X_101_ind]
                    P[6] = state[X_110_ind+2] / state[X_110_ind]
                    P[7] = state[X_111_ind+2] / state[X_111_ind]
                    v = np.dot( Q, B.dot(P) )

                    # Interpolate w:
                    P[0] = state[X_000_ind+3] / state[X_000_ind]
                    P[1] = state[X_001_ind+3] / state[X_001_ind]
                    P[2] = state[X_010_ind+3] / state[X_010_ind]
                    P[3] = state[X_011_ind+3] / state[X_011_ind]
                    P[4] = state[X_100_ind+3] / state[X_100_ind]
                    P[5] = state[X_101_ind+3] / state[X_101_ind]
                    P[6] = state[X_110_ind+3] / state[X_110_ind]
                    P[7] = state[X_111_ind+3] / state[X_111_ind]
                    w = np.dot( Q, B.dot(P) )

                    if re.match(r'\Aradial(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
                        # Radial velocity just projection onto the line of sight
                        alpha = np.radians(90 - spherical_coordinates[obs_ind, 1])   # Elevation
                        beta  = np.radians(spherical_coordinates[obs_ind, 2])        # Azimuth
                        obs[obs_ind]  = del_u*np.cos(alpha)*np.cos(beta) + del_v*np.cos(alpha)*np.sin(beta) + del_w*np.sin(alpha)

                    elif re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
                        rv = np.sqrt(u**2 + v**2 + w**2)
                        del_rv = del_u*np.cos(alpha)*np.cos(beta) + del_v*np.cos(alpha)*np.sin(beta) + del_w*np.sin(alpha)
                        obs[obs_ind] = self._site_observation.radial_velocity_to_doppler(del_rv)

                    else:
                        print("Impossible condition met!")
                        raise ValueError
                else:
                    print("Observation opertor is not defined for a prognostic variable: %s" % prog_var)
                    raise ValueError

                # update observation time
                if t is None:
                    try:
                        t = state.time
                    except:
                        pass

                if t is not None:
                    try:
                        if obs.time != t:
                            obs.time = t
                    except:
                        print("Failed to update observation time")
                        pass

                #
        else:
            print("Unsupported method %s !" % method)
            raise ValueError

        if correct_invalid_indexes and self._observation_valid_indexes is not None:
            if self._observation_valid_indexes.size == obs.size:
                pass
            elif self._observation_valid_indexes.size < obs.size:
                full_obs = obs
                obs = self.observation_vector()
                obs[:] = np.NaN
                obs[self._observation_valid_indexes] = full_obs[self._observation_valid_indexes]
            else:
                print("obs.size > self._observation_valid_indexes!!!")
                raise ValueError

        return obs

    def observation_operator_Jacobian_T_VecProd(self, eval_state, obs, t=None, method='interpolate', missing_as_nan=True):
        """
        Return the H^T * obs, where H is the Jacobaian of the observation operator evaluated at eval_state

        if missing_as_nan is True, Jacobain entries at these points is set to zero, otherwise ValueError exception is raised
        """
        raise NotImplementedError("Refactoring as done with observation_operator_Jacobian_VecProd")

        prog_var = self.__obs_configs['prog_var']
        num_prog_vars = 1
        if re.match(r'\Aradial(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            pass
        if re.match(r'\Awind(-|_)*(velocity|speed)(_|-)*(field)*\Z', prog_var, re.IGNORECASE):
            num_prog_vars *= 3
        elif re.match(r'\Adoppler\Z', prog_var, re.IGNORECASE):
            pass
        else:
            print("Observation opertor is not defined for a prognostic variable: %s" % prog_var)
            raise ValueError
        model = self._dynamical_model

        # Of course, we won't keep that for long; we will convert x into y later on the fly (matrix free)
        obs_grid = self.observation_grid('cartesian')
        spherical_coordinates = self.observation_grid('spherical')
        model_grid = self.model_grid()

        model_Xs, model_Ys, model_Zs = list(set(model_grid[:, 0])), list(set(model_grid[:, 1])), list(set(model_grid[:, 2]))
        model_Xs.sort()
        model_Ys.sort()
        model_Zs.sort()
        model_Xs, model_Ys, model_Zs = np.asarray(model_Xs), np.asarray(model_Ys), np.asarray(model_Zs)

        #
        state = self.state_vector()  # H^T * obs
        state[:] = 0.0
        #
        if re.match(r"\Anear(est)*\Z", method, re.IGNORECASE):
            print("Not Implemented Yet")
            raise NotImplementedError

        elif re.match(r"\Ainterp(olate|olation)*\Z", method, re.IGNORECASE):

            P = np.zeros(8, dtype=np.float32)
            C = np.zeros(8, dtype=np.float32)
            Q = np.zeros(8, dtype=np.float32)
            B = np.zeros((8, 8), dtype=int)  # It's already tiny, but we can make it sparse, e.g. scipy.scr_matrix
            B[0, 0] = B[1, 4] = B[2, 2] = B[3, 1] = B[4, 0] = B[4, 6] = B[5, 0] = B[5, 3] = B[6, 0] = B[6, 5] = B[7, 1:3] = B[7, 4] = B[7, 7] = 1
            B[1, 0] = B[2, 0] = B[3, 0] = B[4, 2] = B[4, 4] = B[5, 1:3] = B[6, 1] = B[6, 4] = B[7, 0] = B[7, 3] = B[7, 5:7] = -1
            for obs_ind in range(self._observation_dimension // 3):
                # print("Obs Ind: %d " %obs_ind)

                x, y, z = obs_grid[obs_ind, :]

                # get nearest x above and below:
                try:
                    below = np.where(model_Xs<=x)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs>x)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print("This is IMPOSSIBLE unless not grid points exist!")
                    raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        obs[obs_ind*3 :obs_ind*3+3] = np.nan, np.nan, np.nan
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    xbelow = model_Xs[below]
                    xabove = model_Xs[above]
                    #

                # get nearest y above and below:
                try:
                    below = np.where(model_Ys<=y)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Ys>y)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print("This is IMPOSSIBLE unless not grid points exist!")
                    raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    ybelow = model_Ys[below]
                    yabove = model_Ys[above]
                    #

                # get nearest z above and below:
                try:
                    below = np.where(model_Zs<=z)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Zs>z)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif above is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                elif below is None: # only below is valid
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        continue
                    else:
                        print("This observation operator is designed for model grid covering all observation points!")
                        raise ValueError
                else:  # both are valid
                    zbelow = model_Zs[below]
                    zabove = model_Zs[above]
                    #

                # # Make sure the dynamical model sees distances meters:
                # if self.__dists_unit == 'm':
                #     pass
                # elif self.__dists_unit == 'km':
                #     xbelow, ybelow, zbelow = xbelow*1000, ybelow*1000, zbelow*1000
                #     xabove, yabove, zabove = xbelow*1000, yabove*1000, zabove*1000
                # else:
                #     print("Unsupported distance unit %s" % self.__dists_unit)
                #     raise ValueError

                #
                # Corners of the lattice:
                X_000 = (xbelow, ybelow, zbelow)
                X_001 = (xbelow, ybelow, zabove)

                X_010 = (xbelow, yabove, zbelow)
                X_011 = (xbelow, yabove, zabove)

                X_100 = (xabove, ybelow, zbelow)
                X_101 = (xabove, ybelow, zabove)

                X_110 = (xabove, above, zbelow)
                X_111 = (xabove, above, zabove)

                # get indexes of these corners in the model state
                X_000_ind = model.gridpoint_to_index(xbelow, ybelow, zbelow)
                X_001_ind = model.gridpoint_to_index(xbelow, ybelow, zabove)
                X_010_ind = model.gridpoint_to_index(xbelow, yabove, zbelow)
                X_011_ind = model.gridpoint_to_index(xbelow, yabove, zabove)

                X_100_ind = model.gridpoint_to_index(xabove, ybelow, zbelow)
                X_101_ind = model.gridpoint_to_index(xabove, ybelow, zabove)
                X_110_ind = model.gridpoint_to_index(xabove, yabove, zbelow)
                X_111_ind = model.gridpoint_to_index(xabove, yabove, zabove)

                if (X_000_ind<0)or(X_001_ind<0)or(X_010_ind<0)or(X_011_ind<0)or(X_100_ind<0)or(X_101_ind<0)or(X_110_ind<0)or(X_111_ind<0):
                    if missing_as_nan:
                        if self._verbose:
                            print("*** filling index %d with np.nan ***" % obs_ind)
                        obs[obs_ind*3 :obs_ind*3+3] = 0, 0, 0
                        continue
                    else:
                        print("Some of the grid points couldn't be found in the model grid, e.g.")
                        print(X_000, X_001, X_010, X_011, X_100, X_101, X_110, X_111)
                        print(X_000_ind, X_001_ind, X_010_ind, X_011_ind, X_100_ind, X_101_ind, X_110_ind, X_111_ind)
                        print(xbelow, ybelow, zbelow)
                        print(xabove, yabove, zabove)
                        print("\n Terminating...\n")
                        raise ValueError

                #
                # print(X_000_ind, X_001_ind, X_010_ind, X_011_ind, X_100_ind, X_101_ind, X_110_ind, X_111_ind, state[0], state[1])
                # Interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)
                dy = float(y - ybelow) / (yabove - ybelow)
                dz = float(z - zbelow) / (zabove - zbelow)

                Q[:] = 1, dx, dy, dz, dx*dy, dy*dz, dz*dx, dx*dy*dz

                # Now, rather than interpolation at that gridpoint, we need derivative of (u, v, w), w.r.t each of the corners of
                # print("obs.size, self._observation_dimension, obs_ind*3, eval_state.size, X_000_ind", obs.size, self._observation_dimension, obs_ind*3, eval_state.size, X_000_ind)
                for l in range(num_prog_vars):
                    state[X_000_ind+1] += (Q.dot(B[:, 0]) * (1.0/eval_state[X_000_ind])) * obs[obs_ind*3+l]
                    state[X_001_ind+1] += (Q.dot(B[:, 1]) * (1.0/eval_state[X_001_ind])) * obs[obs_ind*3+l]
                    state[X_010_ind+1] += (Q.dot(B[:, 2]) * (1.0/eval_state[X_010_ind])) * obs[obs_ind*3+l]
                    state[X_011_ind+1] += (Q.dot(B[:, 3]) * (1.0/eval_state[X_011_ind])) * obs[obs_ind*3+l]
                    state[X_100_ind+1] += (Q.dot(B[:, 4]) * (1.0/eval_state[X_100_ind])) * obs[obs_ind*3+l]
                    state[X_101_ind+1] += (Q.dot(B[:, 5]) * (1.0/eval_state[X_101_ind])) * obs[obs_ind*3+l]
                    state[X_110_ind+1] += (Q.dot(B[:, 6]) * (1.0/eval_state[X_110_ind])) * obs[obs_ind*3+l]
                    state[X_111_ind+1] += (Q.dot(B[:, 7]) * (1.0/eval_state[X_111_ind])) * obs[obs_ind*3+l]
                    #
        else:
            print("Unsupported method %s !" % method)
            raise ValueError

        return state

    def integrate_state(self, state=None, checkpoints=None, reset_state=True, model_screen_output=True):
        """
        return a tuple containing timespan, and states integrated over the requested timespan
          if state is None current state is used;
          if tspan is None, n_iter*dt is the length of tspan with two states (current, new) returned

        """
        # Check and validate the checkpoints:
        # TODO: Update checkpoints to handle scalar entries as will as timestamps, e.g. datetime.datetime
        time_diff_unit = self.model_time_unit
        _checkpoints = utility.timespan_to_scalars(checkpoints, time_diff_unit=time_diff_unit)  # HyPar timescale is in hours
        # if np.isscalar(checkpoints[0]):
        #     _checkpoints = [i+checkpoints[0] for i in _checkpoints]
        #
        # print("forward_model.integrate_staet", checkpoints, _checkpoints)
        return self._dynamical_model.integrate_state(state=state, checkpoints=_checkpoints,
                                                     reset_state=reset_state,
                                                     screen_output=model_screen_output)

    def state_vector(self, **kwargs):
        x = self._dynamical_model.state_vector(**kwargs)
        return x

    def dynamical_model(self):
        return self._dynamical_model

    def get_model_prognostic_variables(self, variable='all'):
        """
        Extract the model grid (unified for all prognostic variables), the indexes of each variable,
            and the values of each variables. If 'variable' is set to 'all' all prognostic variables are extracted,
            otherwise, the specific variable, e.g. 'u', 'E', etc. is looked up.
            This returns three tuples, the first is (x, y, z) coordinates of the model grid,
            the second is indexes of each of the selected variable(s), and the last tuple contains numpy arrays for each of the prognostic variables, or the particular one passed
            in 'variable'.

        """
        model_grid = self.model_grid()

        rho_inds, u_inds, v_inds, w_inds, E_inds = self.get_model_prognostic_variables_indexes()
        x_grid, y_grid, z_grid = model_grid[:, 0] , model_grid[:, 1], model_grid[:, 2]
        grid = (x_grid, y_grid, z_grid)

        if re.match(r'\Arho\Z', variable, re.IGNORECASE):
            indexes = (rho_inds)
            values = ( self.current_model_state()[rho_inds] )
        elif re.match(r'\Au\Z', variable, re.IGNORECASE):
            indexes = (u_inds)
            values = ( self.current_model_state()[u_inds] )
        elif re.match(r'\Av\Z', variable, re.IGNORECASE):
            indexes = (v_inds)
            values = ( self.current_model_state()[v_inds] )
        elif re.match(r'\Aw\Z', variable, re.IGNORECASE):
            indexes = (w_inds)
            values = ( self.current_model_state()[w_inds] )
        elif re.match(r'\AE\Z', variable, re.IGNORECASE):
            indexes = (E_inds)
            values = ( self.current_model_state()[E_inds] )
        elif re.match(r'\Aall\Z', variable, re.IGNORECASE):
            indexes = (rho_inds, u_inds, v_inds, w_inds, E_inds)
            state = self.current_model_state()
            values = (state[rho_inds], state[u_inds], state[v_inds], state[w_inds], state[E_inds])
        else:
            print("Unrecognized value of the prognositic variable; either rho, u, v, w, or all values are supportecd. Received %s " % variable)
            raise ValueError

        return grid, indexes, values

    def get_observation_prognostic_variables(self):
        raise NotImplementedError("Do the same as in get_model_prognostic_variables")

    def get_model_prognostic_variables_indexes(self):
        return self._dynamical_model.get_prognostic_variables_indexes()

    def get_observation_prognostic_variables_indexes(self):
        return self._site_observation.get_prognostic_variables_indexes()

    def observation_vector(self, **kwargs):
        """
        Get a standard observation vector (LinAlg.Vec)
        """
        kwargs.update({'size':self._observation_dimension})
        y = self._site_observation.observation_vector(**kwargs)
        return y

    def create_inflation_vector(self, inflation_factors=(1.05, 1.05, 1.05, 1.05, 1.0005)):
        """
        Given inflation factors, create a space-dependent vector of inflation factors.

        Args:
            inflation_factors: either a scalar, or iterable of length equal to number of model prognostic variables
                If scalar is passed, the inflation is space independent; i.e. inflation is fixed over space,
                - If iterable of length equal to number of prognostic variables, each entry is taken for each of the prognostic variables, Inflation for each prognostic variable is fixed  in space
                - If iterable of size equal to model size, it is simply wrapped as an instance of state vector

        Returns:
            inflation_vector: a state vector of space-dependent inflation factors

        """
        if utility.isscalar(inflation_factors):
            assert inflation_factors > 1, "Inflation factors must be positive!"
            infl_facs = [inflation_factors]*(self.__model_configs['nvars']-1) + [(inflation_factors-1.0)/100.0]  # TODO: Discuss with Emil how to relate rho, E <<
            inflation_vector = self.create_inflation_vector(infl_facs)
        elif utility.isiterable(inflation_factors):
            sz = len(inflation_factors)
            if sz == 1:
                inflation_vector = self.create_inflation_vector(inflation_factors[0])
            elif sz == self.__model_configs['nvars']:
                inflation_vector = self.state_vector()
                for i, inds in  enumerate(self.get_model_prognostic_variables_indexes()):
                    inflation_vector[inds] = inflation_factors[i]
            elif sz == self.state_size():
                if isinstance(inflation_factors, StateVector):
                    inflation_vector = inflation_factors.copy()
                else:
                    inflation_vector = self.state_vector()
                    inflation_vector[:] = inflation_factors[:]
            else:
                msg = "inflation factos must be of size:"
                msg += "1 (scalar), %d (number of prognostic variables), or %d (state size). Received size %d" % (self.__model_configs['nvars'],
                                                                                                                  self.state_size(),
                                                                                                                  sz)
                print(msg)
                raise AssertionError
        else:
            print("The passed inflation_factors is of unsupported type [%s]" % type(inflation_factors))
            raise TypeError

        return  inflation_vector

    def _get_model_grid(self):
        """
        """
        grid = self._dynamical_model.get_model_grid()  # this is in Kilometers
        if self.__dists_unit == 'm':
            pass
        elif self.__dists_unit == 'km':
            grid /= 1000.0  # convert to meters to match observational grid
        else:
            print("Unsupported distance unit %s" % self.__dists_unit)
            raise ValueError
        return grid

    def model_grid(self):
        """
        """
        try:
            grid = self._model_grid
        except:
            grid = self._get_model_grid()
        finally:
            if grid is None:
                grid = self._get_model_grid()
        return grid
    # add alias; TODO: need to consider renaming all functions/methods properly
    get_model_grid = model_grid

    def _get_observation_grid(self, coordinate_system='cartesian'):
        """
        """
        grid = self._site_observation.get_local_grid(coordinate_system)  # this is in meters
        if self.__dists_unit == 'm':
            pass
        elif self.__dists_unit == 'km':
            print("Scaling Down")
            grid /= 1000.0  # convert to meters to match observational grid

        else:
            print("Unsupported distance unit %s" % self.__dists_unit)
            raise ValueError
        return grid

    def observation_grid(self, coordinate_system='cartesian'):
        """
        Return the coordinates of each entry of the observation vector.
        The result is an array of size (observation size x 3), with columns representing coordinates in teh passed coordinate system

        if cartesian is requested, entries are returned as (x, y, z) coordinates,
        if spherical is requested, entries are returned as (radial distance, inclination, azimuth)

        """
        try:
            if re.match(r'\Aspher(e|i)(cal)*\Z', coordinate_system, re.IGNORECASE):
                grid = self._observation_grids[1]
            elif re.match(r'\Acart(es|ez)*(ian)*\Z', coordinate_system, re.IGNORECASE):
                grid = self._observation_grids[0]
            else:
                print("Unsupported coordinate system [%s]!" % coordinate_system)
                raise ValueError
            if grid is None:
                raise TypeError
        except(TypeError, AttributeError, IndexError):
            if re.match(r'\Aspher(e|i)(cal)*\Z', coordinate_system, re.IGNORECASE):
                grid = self._get_observation_grid('spherical')
                grid[:, [0, -1]] = grid[:, [-1, 0]]  # swap radial distance with elevation
                grid[:, [1, -1]] = grid[:, [-1, 1]]  # swap azimuth with elevation
            elif re.match(r'\Acart(es|ez)*(ian)*\Z', coordinate_system, re.IGNORECASE):
                grid = self._get_observation_grid('cartesian')
                # Now get model grid, and shift observation grid to x-y ceter
                model_grid = self.model_grid()
                min_x, max_x = model_grid[:, 0].min(), model_grid[:, 0].max()
                min_y, max_y = model_grid[:, 1].min(), model_grid[:, 1].max()
                grid[:, 0] += (min_x + max_x)/ 2.0
                grid[:, 1] += (min_y + max_y)/ 2.0
            else:
                print("Unsupported coordinate system [%s]!" % coordinate_system)
                raise ValueError
            #
        return grid
    # add alias
    get_observation_grid = observation_grid


    def get_DL_coordinates(self):
        """
        """
        model_grid = self.model_grid()
        min_x, max_x = model_grid[:, 0].min(), model_grid[:, 0].max()
        min_y, max_y = model_grid[:, 1].min(), model_grid[:, 1].max()
        dl_coordinates = ((min_x + max_x)/ 2.0, (min_y + max_y)/ 2.0, 0.0)
        return dl_coordinates

    @property
    def observation_error_model(self):
        return self._obs_err_model

    @property
    def prior_error_model(self):
        return self._frcst_err_model

    @property
    def model_error_model(self):
        return self._model_err_model

    @property
    def dynamical_model(self):
        return self._dynamical_model

    @property
    def model_time_unit(self):
        return self._dynamical_model.time_unit

    @property
    def model_space_unit(self):
        return self.__dists_unit
    @property
    def model_distance_unit(self):
        return self.__dists_unit

#
class GaussianErrorModel(object):
    """
    A Trivial error model to get synthetic observations, and get EnKF working
    """
    def __init__(self, size, mean=0.0, variance=1, valid_indexes=None):
        self._size = size
        if np.isscalar(mean):
            _mean = np.ones(self._size) * mean
        else:
            _mean = np.array(mean).flatten()

        if np.isscalar(variance):
            _variance = np.ones(self._size) * variance
            _stdev = np.ones(self._size) * np.sqrt(variance)
        else:
            _variance = np.asarray(variance).flatten()
            _stdev = np.sqrt(_variance)

        self._mean = _mean
        self._variance = _variance
        self._dist_cov = sp_sparce.spdiags(_variance, 0, self._size, self._size, format='csr')
        self.R = self._dist_cov

        self.sqrtR = sp_sparce.spdiags(_stdev, 0, self._size, self._size, format='csr')
        self.sqrtR_inv = sp_sparce.spdiags(1.0/_stdev, 0, self._size, self._size, format='csr')

        if valid_indexes is None:
            valid_indexes = np.arange(self._size)
        self.update_valid_indexes(valid_indexes)


    def add_noise(self, x, in_place=False, correct_invalid_indexes=False):
        if isinstance(x, (StateVector, np.ndarray)):
            if in_place:
                out_x = x
            else:
                out_x = x.copy()
            whitenoise = np.random.randn(self._size)
            out_x[:] = self.sqrtR.dot(whitenoise) + x[:] + self._mean
        else:
            print("Unspoorted type (%s) of passed vector x" % type(x))
            raise TypeError
        if correct_invalid_indexes:
            self._update_valid_values(out_x)
        return out_x

    def sqrtR_VecProd(self, x, in_place=False, correct_invalid_indexes=False):
        if isinstance(x, StateVector):
            if in_place:
                out_x = x
            else:
                out_x = x.copy()
            out_x[:] = self.sqrtR.dot(x[:])

        elif isinstance(x, np.ndarray):
            if in_place:
                out_x = x
            else:
                out_x = np.empty_like(x)
            ndims = x.ndim
            if ndims ==1:
                out_x[:] = self.sqrtR.dot(x[:])
            elif ndims ==2:
                for i in range(x.shape[1]):
                    out_x[:, i] = self.sqrtR.dot(x[:, i])
            else:
                print("Unsupported dimension %d " % ndims)
                raise TypeError
        if correct_invalid_indexes:
            self._update_valid_values(out_x)
        return out_x

    def sqrtR_inv_VecProd(self, x, in_place=False, correct_invalid_indexes=False):
        if isinstance(x, StateVector):
            if in_place:
                out_x = x
            else:
                out_x = x.copy()
            out_x[:] = self.sqrtR_inv.dot(x[:])

        elif isinstance(x, np.ndarray):
            if in_place:
                out_x = x
            else:
                out_x = np.empty_like(x)
            ndims = x.ndim
            if ndims ==1:
                out_x[:] = self.sqrtR_inv.dot(x[:])
            elif ndims ==2:
                for i in range(x.shape[1]):
                    out_x[:, i] = self.sqrtR_inv.dot(x[:, i])
            else:
                print("Unsupported dimension %d " % ndims)
                raise TypeError
        if correct_invalid_indexes:
            self._update_valid_values(out_x)
        return out_x

    def update_valid_indexes(self, valid_indexes):
        if valid_indexes is not None:
            self._valid_indexes = np.asarray(valid_indexes[:]).flatten()
        else:
            self._valid_indexes = np.arange(self._size)

    def _update_valid_values(self, x):
        if self._valid_indexes is not None:
            if self._valid_indexes.size == self._size:
                pass
            elif self._valid_indexes.size < self._size:
                full_x = x.copy()
                # print("Full X: ", full_x)
                x[:] = np.NaN
                x[self._valid_indexes] = full_x[self._valid_indexes]
            else:
                print("x.size > self._valid_indexes!!!")
                raise ValueError
        return x

    @property
    def pdf(self):
        return 'Gaussian'
    density_function = pdf

    @property
    def pdf_mean(self):
        return self._mean.copy()

    @property
    def pdf_variance(self):
        return self._variance.copy()


def dynamical_model_configs():
    """
    Standard configuratiions of the dynamical model (HyPar)
    """
    return model_configs

def observation_configs(prognostic_variable='wind-velocity'):
    """
    Standard lidar observation configurations
    Remarks:
        Consider making it more flexible by adding options!
    """
    # Observations Settings (passed to DL_obs)
    return obs_configs


def standard_model_configs():
    """
    """
    model_configs = {'size': [101, 101, 41],
                     'iproc': [2, 2, 1],
                     'dt': 0.05,
                     'n_iter': 10,
                     'screen_op_iter':50,  # default is 1, but we want to reduce it a bit
                     # 'cleanup_dir':False,
                     'boundary':dict(num_faces=6,  # X-low, X-high, Y-low, Y-high, Z-low, Z-high
                                   faces_types=['periodic',
                                                'periodic',
                                                'periodic',
                                                'periodic',
                                                'thermal-noslip-wall',
                                                'slip-wall'],
                                   faces_settings=[None,
                                                   None,
                                                   None,
                                                   None,
                                                   (0.0, 0.0, 0.0, 'temperature_data.dat'),
                                                   (0.0, 0.0, 0.0)]
                                 ), #
                    }
    return model_configs

def standard_observation_configs(site='sgp', facility='C1', t=0, prognostic_variable='radial-velocity', num_gates=120, stare_only=True):
    """
    """
    if stare_only:
        elevations = azimuthes = [90]
    else:
        elevations = [45, 60, 90],
        azimuthes  = [0, 45, 90, 135, 180, 225, 270, 315]
    #
    site_facility = 'dl'.join([site, facility])
    obs_configs = {'site_facility': site_facility,
                   't': t,
                   # 'dl_coordinates': None,
                   'prog_var': prognostic_variable,
                   'num_gates': num_gates,
                   'range_gate_length': 30,
                   'elevations': elevations,
                   'azimuthes':azimuthes
                   }
    return obs_configs


def create_standard_forward_operator(prognostic_variable="radial_velocity", stare_obs_only=False):
    """
    """
    model_configs = standard_model_configs()
    obs_configs = standard_observation_configs(stare_only=stare_obs_only)

    # create forward operator (dynamical model + observations):
    model = ForwardOperator(model_configs, obs_configs)
    return model


def read_model_configs(configs_file):
    """
    read forward operator configs (dynamical model + observations)
    """
    # set nan value as it will be needed for config-parsers
    nan = np.NaN
    inf = np.infty
    none = None

    configs_filepath = os.path.abspath(configs_file)
    if not os.path.isfile(configs_filepath):
        print("The configs_file passed [%s] doesn't point to an existing file!" % configs_file)
        raise IOError

    configparser = ConfigParser.ConfigParser()
    configparser.read(configs_filepath)
    section_headers = ['Model Configs', 'Observation Configs']
    for section_header in section_headers:
        if not configparser.has_section(section_header):
            print("Couldn't find the section header '%s' in the file!" % section_header)
            raise ValueError

    section_header = 'Model Configs'
    configs = configparser.options(section_header)
    model_configs = {}
    for option in configs:
        if re.match(r'\Athermal_source|size|iproc|domain_lower_bounds|domain_upper_bounds|boundary|model_paths|physics\Z', option, re.IGNORECASE):
            option_val = configparser.get(section_header, option)
            option_val = eval(option_val)

        elif re.match(r'\Arest_iter|n_io|file_op_iter|nvars|ndims|ghost|n_iter|screen_op_iter\Z', option, re.IGNORECASE):
            option_val = configparser.getint(section_header, option)
        elif re.match(r'\Ainit_time|dt|\Z', option, re.IGNORECASE):
            option_val = configparser.getfloat(section_header, option)
        elif re.match(r'\Acleanup_dir\Z', option, re.IGNORECASE):
            option_val = configparser.getboolean(section_header, option)
        else:
            option_val = configparser.get(section_header, option)  # leave as strings
            if option_val.startswith("b'") and option_val.endswith("'"):
                option_val = option_val[2:-1]
            elif option_val.startswith('b"') and option_val.endswith('"'):
                option_val = option_val[2:-1]
            else:
                pass
            print("Unknown option: %s with value %s " % (option, option_val))
            # More!
        model_configs.update({option:option_val})

    section_header = 'Observation Configs'
    configs = configparser.options(section_header)
    obs_configs = {}
    for option in configs:
        if re.match(r'\Adl_coordinates|elevations|azimuthes\Z', option, re.IGNORECASE):
            option_val = configparser.get(section_header, option)
            option_val = eval(option_val)

        elif re.match(r'\Arange_gate_length|t\Z', option, re.IGNORECASE):
            option_val = configparser.getfloat(section_header, option)

        elif re.match(r'\Anum_gates|nvars|obs_dimension|\Z', option, re.IGNORECASE):
            option_val = configparser.getint(section_header, option)
        else:
            option_val = configparser.get(section_header, option)  # leave as strings
            # print("Unknown option: %s with value %s " % (option, option_val))
            # More!
        obs_configs.update({option:option_val})
    # The following will be removed after handling previous flows (fixed in current unpushed comit
    if 't' not in obs_configs:
        obs_configs.update({'t':0.0})

    return model_configs, obs_configs


def create_model_from_configs(configs_file, verbose=False, obs_err_file=None):
    """
    """
    try:
        model_configs, obs_configs = read_model_configs(configs_file)
    except:
        print("Failed to read model configs  from file: %s\n trace back error info below..." % configs_file)
        raise
    # Good to go with loaded configs...
    if obs_err_file is None:
        # Look at the obsevation error model data in conifgs if None is passed; useful if model results are moved around, and need to be reloaded
        try:
            obs_err_file = model_configs['obervation_error_model_file']
            obs_err_file = os.path.abspath(obs_err_file)
            if verbose:
                print("Observation error model infor was found in configurations file.")
        except(KeyError):
            if verbose:
                print("Observation error model infor was not found in configurations file. Will create with standard setup, and save for later use")
            obs_err_file = None
    #
    if obs_err_file  is None:
        model = ForwardOperator(model_configs, obs_configs, verbose=verbose)
    else:
        model = ForwardOperator(model_configs, obs_configs, verbose=verbose, obs_err_model_file=obs_err_file)
    #
    return model



if __name__ == '__main__':

    print("%s Why Am I here Now! %s" % ("??"*60, "??"*60))
    model_configs = {'size': [51, 51, 41],
                     'iproc': [2, 2, 2],
                     'dt': 0.15,
                     'n_iter': 10
                     }
    obs_configs = {'site_facility': 'spgdlE32',
                   't': 0,
                   'dl_coordinates': '0,0,0',
                   'prog_var': 'wind-velocity',
                   'num_gates': 400,
                   'range_gate_length': 30,
                   'elevations': [45, 60, 90],
                   'azimuthes': [0, 45, 90, 135, 180, 225, 270, 315]
                   }
    model = ForwardOperator(model_configs, obs_configs)

    x = model.state_vector()

    y = model.observation_vector()

    model_grid = model.model_grid()

    obs_grid = model.observation_grid('cartesian')
    obs_grid = model.observation_grid('spherical')

    model._dynamical_model.integrate_state()

    x = model._dynamical_model.get_current_state()
    y = model.evaluate_theoretical_observation(x)

    Hx = model.observation_operator_Jacobian_VecProd(x, x)

    HTx = model.observation_operator_Jacobian_T_VecProd(x, y)

    # print("x:, ", x)
    # print("y:, ", y)
    # for i in range(HTx.size/300):
    #     print("wind-velocity components (u,v,w) at index [%d] are: %s" %(i, repr(HTx[i*3: i*3+3])))
