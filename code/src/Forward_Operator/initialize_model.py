
# setup extension modules
import os
import sys

top_dir = os.path.abspath("../../")


def setup(top_dir):
    sys.path.insert(2, top_dir)
    import dl_setup
    #
    for root, _, _ in os.walk(os.path.join(top_dir,'src')):
        if os.path.isdir(root) and not ('/.' in root or '__' in root):  # avoid special or hidden directories
            # in case this is not the initial run. We don't want  to add duplicates to the system paths' list.
            if root not in sys.path:
                sys.path.insert(2, root)

    try:
        from Py_HyPar import HyPar_Model
        from DLidarVec import StateVector, Ensemble
        from forward_model import ForwardOperator
    except ImportError:
        dl_setup.main()
        try:
            from Py_HyPar import HyPar_Model
            from DLidarVec import StateVector, Ensemble
            from forward_model import ForwardOperator
        except:
            print("Failed to import, and also failed to recompile everything!")
            raise

if __name__ == "__main__":
    #
    setup(top_dir)
    #
