
# setup extension modules

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

import numpy as np

import os
import sys

HYPAR_ROOT_PATH = os.environ.get('HYPAR_ROOT_PATH')
if not HYPAR_ROOT_PATH:
    print("Can't compile the Hypar extension module without HyPar!")
    raise AssertionError
else:
    Hypar_Include = os.path.join(HYPAR_ROOT_PATH, 'Extras')

linalgpath = "../../LinAlg"
sys.path.append(linalgpath)

# Library in Hypar Extra are no longer used by DLiDA; our own code is added
# include_dirs = ['./'] + [np.get_include()] + [linalgpath] + [Hypar_Include]
include_dirs = ['./'] + [np.get_include()] + [linalgpath]


ext_modules = cythonize([
    Extension("Py_HyPar", sources=['Py_HyPar.pyx'], include_dirs=include_dirs)
    ])

setup(ext_modules=ext_modules)
