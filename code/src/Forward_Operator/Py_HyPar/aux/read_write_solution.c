/*
  This code writes the solution for the rising
  thermal bubble case for the 3D Navier-Stokes equations, in a binary file.

*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>




int write_binary_array(char *file, int NI, int NJ, int NK, int ndims, int nvars, double *x, double *y, double *z, double *U)
{
  
  /* Write to state in the binary format HyPar spits it's solution out into  */
  FILE *out;
  size_t bytes;
  long size = nvars*NI*NJ*NK;
  
  out = fopen(file,"wb");
  if (!out){
    fprintf(stdout, "Error: File '%s' Couldn't be open for output! Terminating!\n", file);
   	return(1);
  }
	
   
    /* write ndims, nvars */
    bytes = fwrite(&ndims,sizeof(int),1,out);
    fprintf(stdout, "write_initial_condition: ndims=%d written to output file.\n", ndims);
	if ((int)bytes != 1) {
      fprintf(stderr,"Error in write_binary_array(): Unable to write ndims to output file.\n");
    }
    bytes = fwrite(&nvars,sizeof(int),1,out);
    fprintf(stdout, "write_initial_condition: nvars=%d written to output file.\n", nvars);
    if ((int)bytes != 1) {
      fprintf(stderr,"Error in write_binary_array(): Unable to write nvars to output file.\n");
    }
	
	/* write dimensions */
    bytes = fwrite(&NI,sizeof(int),1,out);
	if ((int)bytes != 1) {
      fprintf(stderr,"Error in write_binary_array(): Unable to write NI to output file.\n");
    }
    bytes = fwrite(&NJ,sizeof(int),1,out);
	if ((int)bytes != 1) {
      fprintf(stderr,"Error in write_binary_array(): Unable to write NJ to output file.\n");
    }
    bytes = fwrite(&NK,sizeof(int),1,out);
	if ((int)bytes != 1) {
      fprintf(stderr,"Error in write_binary_array(): Unable to write NK to output file.\n");
    }

	/* write grid*/
	bytes = fwrite(x,sizeof(double),NI,out);
    if ((int)bytes != NI) {
      fprintf(stdout, "Error in write_initial_condition(): Unable to write grid X. Expected %d, Written %d.\n",
              NI, (int)bytes);
    }
    bytes = fwrite(y,sizeof(double),NJ,out);
    if ((int)bytes != NJ) {
      fprintf(stdout, "Error in write_initial_condition(): Unable to write grid Y. Expected %d, Written %d.\n",
              NJ, (int)bytes);
    }
    bytes = fwrite(z,sizeof(double),NK,out);
    if ((int)bytes != NK) {
      fprintf(stdout, "Error in write_initial_condition(): Unable to write grid Z. Expected %d, Written %d.\n",
              NK, (int)bytes);
    }

	/* write solution */
    bytes = fwrite(U,sizeof(double),size,out);
    if ((long)bytes != size) {
      fprintf(stdout, "Error in write_initial_condition(): Unable to write State U. Expected %ld, Written %ld.\n",
              size, (long)bytes);
    }

  fclose(out);
  /* Done */
	return(0);

}


int write_initial_condition(char *file, int NI, int NJ, int NK, int ndims, double *x, double *y, double *z, double *U)
{

  size_t bytes;
  long size = 5*NI*NJ*NK;

  // fprintf(stdout, "State size: %ld\n", size);
  /* some sanity checks */
  if (ndims != 3) {
    fprintf(stdout, "ndims is not 3 in solver.inp. this code is to generate 3D initial conditions\n");
    return(1);
  }


  /* Write to file in the format HyPar wants */
  FILE *out;

  out = fopen(file,"wb");
  if (!out){
    fprintf(stdout, "Error: File '%s' Couldn't be open for output! Terminating!\n", file);
  	return(1);
  }else{
    
	/* write grid */
	bytes = fwrite(x,sizeof(double),NI,out);
    if ((int)bytes != NI) {
      fprintf(stdout, "Error in write_initial_condition(): Unable to write grid X. Expected %d, Written %d.\n",
              NI, (int)bytes);
    }
    bytes = fwrite(y,sizeof(double),NJ,out);
    if ((int)bytes != NJ) {
      fprintf(stdout, "Error in write_initial_condition(): Unable to write grid Y. Expected %d, Written %d.\n",
              NJ, (int)bytes);
    }
    bytes = fwrite(z,sizeof(double),NK,out);
    if ((int)bytes != NK) {
      fprintf(stdout, "Error in write_initial_condition(): Unable to write grid Z. Expected %d, Written %d.\n",
              NK, (int)bytes);
    }
    
	/* write solution */
	bytes = fwrite(U,sizeof(double),size,out);
    if ((long)bytes != size) {
      fprintf(stdout, "Error in write_initial_condition(): Unable to write State U. Expected %ld, Written %ld.\n",
              size, (long)bytes);
    }
  }

  fclose(out);

  /* Done */

	return(0);
}


int alloc_and_read_initial_condition(char *file, int NI, int NJ, int NK, int ndims, double **x, double **y, double **z, double **U)
{

  /* read solver.inp */
  // fprintf(stdout, "Reading passed file \"%s...\" !\n", file);

  FILE *in;
  in = fopen(file,"rb");
  if (!in){
    fprintf(stdout, "Error: File '%s' Couldn't be open for input! Terminating!\n", file);
  	return(1);
  }else{
    // fprintf(stdout, "Reading array from binary file %s (Serial mode).\n",file);
    size_t bytes;
    int size = 5*NI*NJ*NK;

    /* allocate global solution array */
    // *x = (double*) calloc(NI, sizeof(double));
    // *y = (double*) calloc(NJ, sizeof(double));
    // *z = (double*) calloc(NK, sizeof(double));
    // *U = (double*) calloc(size, sizeof(double));
    *x = calloc(NI, sizeof(double));
    *y = calloc(NJ, sizeof(double));
    *z = calloc(NK, sizeof(double));
    *U = calloc(size, sizeof(double));

    if (*x == NULL){fprintf(stdout, "FAILED to allocate x!\n");}
    if (*y == NULL){fprintf(stdout, "FAILED to allocate y!\n");}
    if (*z == NULL){fprintf(stdout, "FAILED to allocate z!\n");}
    if (*U == NULL){fprintf(stdout, "FAILED to allocate U!\n");}


    /* read grid */
    bytes = fread(*x, sizeof(double), NI, in);
    if ((int)bytes != NI) {
      fprintf(stdout, "Error in ReadArray(): Unable to read grid. Expected %d, Read %d.\n",
              NI, (int)bytes);
    }
    bytes = fread(*y, sizeof(double), NJ, in);
    if ((int)bytes != NJ) {
      fprintf(stdout, "Error in ReadArray(): Unable to read grid. Expected %d, Read %d.\n",
              NJ, (int)bytes);
    }
    bytes = fread(*z, sizeof(double), NK, in);
    if ((int)bytes != NK) {
      fprintf(stdout, "Error in ReadArray(): Unable to read grid. Expected %d, Read %d.\n",
              NK, (int)bytes);
    }

    /* read solution */
    bytes = fread(*U, sizeof(double), size, in);
    if ((int)bytes != size) {
      fprintf(stdout, "Error in ReadArray(): Unable to read solution. Expected %d, Read %d.\n",
              (int)size, (int)bytes);
    }
  }

  fclose(in);

	return(0);
}


int read_initial_condition(char *file, int NI, int NJ, int NK, int ndims, double *x, double *y, double *z, double *U)
{

  /* read solver.inp */
  // fprintf(stdout, "Reading passed file \"%s\" !\n", file);

  FILE *in;
  in = fopen(file,"rb");
  if (!in){
    fprintf(stdout, "Error: File '%s' Couldn't be open for input! Terminating!\n", file);
    return(1);
  }else{
    // fprintf(stdout, "Reading array from binary file %s (Serial mode).\n",file);
    size_t bytes;
    long size = 5*NI*NJ*NK;

    /* allocate global solution array */
    // *x = (double*) calloc(NI, sizeof(double));
    // *y = (double*) calloc(NJ, sizeof(double));
    // *z = (double*) calloc(NK, sizeof(double));
    // *U = (double*) calloc(size, sizeof(double));
    // *x = calloc(NI, sizeof(double));
    // *y = calloc(NJ, sizeof(double));
    // *z = calloc(NK, sizeof(double));
    // *U = calloc(size, sizeof(double));

    if (x == NULL){fprintf(stdout, "X must be allocated before passing!\n");}
    if (y == NULL){fprintf(stdout, "Y must be allocated before passing!\n");}
    if (z == NULL){fprintf(stdout, "Z must be allocated before passing!\n");}
    if (U == NULL){fprintf(stdout, "U must be allocated before passing!\n");}


    /* read grid */
    bytes = fread(x, sizeof(double), NI, in);
    if ((int)bytes != NI) {
      fprintf(stdout, "Error in ReadArray(): Unable to read grid. Expected %d, Read %d.\n",
              NI, (int)bytes);
    }
    bytes = fread(y, sizeof(double), NJ, in);
    if ((int)bytes != NJ) {
      fprintf(stdout, "Error in ReadArray(): Unable to read grid. Expected %d, Read %d.\n",
              NJ, (int)bytes);
    }
    bytes = fread(z, sizeof(double), NK, in);
    if ((int)bytes != NK) {
      fprintf(stdout, "Error in ReadArray(): Unable to read grid. Expected %d, Read %d.\n",
              NK, (int)bytes);
    }

    /* read solution */
    bytes = fread(U, sizeof(double), size, in);
    if ((int)bytes != size) {
      fprintf(stdout, "Error in ReadArray(): Unable to read solution. Expected %d, Read %d.\n",
              (int)size, (int)bytes);
    }
  }

  fclose(in);

  /* // Remove after debugging
  int i;
  for(i=0; i<50; i++){
      fprintf(stdout, "Inside read_write_initial_condition.c; Returning: X[%d] = %lf \n" ,i, x[i]);
      fprintf(stdout, "Inside read_write_initial_condition.c; Returning: U[%d] = %lf \n" ,i, U[i]);
    }
  */

	return(0);
}


int read_binary_solution(char* filename, int *NI, int *NJ, int *NK, int *ndims, double *x, double *y, double *z, double *U)
{
  FILE *in, *inputs;
  char op_file_format[50];

  int loc_ndims = 3;
  int loc_nvars = 5;
  int nvars;

  inputs = fopen("solver.inp","r");
  if (!inputs) {
    fprintf(stdout, "Error: File \"solver.inp\" not found.\n");
    return(1);
  } else {
	  char word[100];
    fscanf(inputs,"%s",word);
    if (!strcmp(word, "begin")){
	    while (strcmp(word, "end")){
		    fscanf(inputs,"%s",word);
   			if      (!strcmp(word, "ndims"            ))  fscanf(inputs,"%d",ndims           );
   			else if (!strcmp(word, "nvars"            ))  fscanf(inputs,"%d",&nvars          );
   			else if (!strcmp(word, "op_file_format"   ))  fscanf(inputs,"%s" ,op_file_format);
      }
    }
  }
  fclose(inputs);

  if (strcmp(op_file_format,"binary") && strcmp(op_file_format,"bin")) {
    fprintf(stdout, "Error: solution output needs to be in binary files.\n");
    return(1);
  }
  if (nvars != loc_nvars) {
    fprintf(stdout, "nvars is not 5 in solver.inp. this code is for 3D Navier-Stokes with 5 prognostic variables rho, u, v, w, E\n");
    return(1);
  }
  if (*ndims != loc_ndims) {
    fprintf(stdout, "ndims is not 3 in solver.inp. this code is to generate 3D initial conditions\n");
    return(1);
  }

  in = fopen(filename,"rb");
  if (!in) {
    fprintf(stdout, "File %s Not Found!. Exiting.\n", filename);
  } else {

    // fprintf(stdout, "Reading file %s....\n",filename);

    /* read the file headers */
    fread(ndims,sizeof(int),1,in);
    fread(&nvars,sizeof(int),1,in);

    if (nvars != loc_nvars) {
      fprintf(stdout, "nvars is not 5 in solver.inp. this code is for 3D Navier-Stokes with 5 prognostic variables rho, u, v, w, E\n");
      fclose(in);
      return(1);
    }
    if (*ndims != loc_ndims) {
      fprintf(stdout, "ndims is not 3 in solver.inp. this code is to generate 3D initial conditions\n");
      fclose(in);
      return(1);
    }

    /* read dimensions */
    fread(NI, sizeof(int), 1, in);
    fread(NJ, sizeof(int), 1, in);
    fread(NK, sizeof(int), 1, in);

    /* read grid */
    fread(x, sizeof(double), *NI, in);
    fread(y, sizeof(double), *NJ, in);
    fread(z, sizeof(double), *NK, in);


    /* read state */
    fread(U, sizeof(double), loc_nvars*(*NI)*(*NJ)*(*NK), in);


    /* done reading */
    fclose(in);
    // fprintf(stdout, "Done .\n");

  }

  return(0);
}
