/*
  This code generates the initial solution for the rising
  thermal bubble case for the 3D Navier-Stokes equations.

  Note: this code allocates the global domain, so be careful
  when using it for a large number of grid points.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

double raiseto(double x, double a)
{
  return(exp(a*log(x)));
}

int create_initial_condition(char *filename, int ndims, int nvars, 
							 double Lx_l, double Ly_l, double Lz_l, 
							 double Lx_u, double Ly_u, double Lz_u, 
							 int NI, int NJ, int NK, 
							 double gamma, double R, double rho_ref, double p_ref, 
							 double grav_x,double grav_y, double grav_z, int HB, char *ip_file_type)
{

  /* // Debugging:...
   * fprintf(stdout, "Creating initial condition...\n");
   * fprintf(stdout, "filename: %s \n",  filename );
   * fprintf(stdout, "ndims: %d \n", ndims);
   * fprintf(stdout, "nvars: %d \n", nvars);
   * fprintf(stdout, "Lx_l: %f \n", Lx_l);
   * fprintf(stdout, "Lx_u: %f \n", Lx_u);
   * fprintf(stdout, "Ly_l: %f \n", Ly_l);
   * fprintf(stdout, "Ly_u: %f \n", Ly_u);
   * fprintf(stdout, "Lz_l: %f \n", Lz_l);
   * fprintf(stdout, "Lz_u: %f \n", Lz_u);
   * fprintf(stdout, "NI: %d \n", NI);
   * fprintf(stdout, "NJ: %d \n", NJ);
   * fprintf(stdout, "NK: %d \n", NK);
   * fprintf(stdout, "gamma: %f \n", gamma);
   * fprintf(stdout, "R: %f \n", R);
   * fprintf(stdout, "rho_ref: %f \n", rho_ref);
   * fprintf(stdout, "p_ref: %f \n", p_ref);
   * fprintf(stdout, "grav_x: %f \n", grav_x);
   * fprintf(stdout, "grav_y: %f \n", grav_y);
   * fprintf(stdout, "grav_z: %f \n", grav_z);
   * fprintf(stdout, "HB: %d \n", HB);
   * fprintf(stdout, "ip_file_type: %s \n", ip_file_type);
   */
  
  /* some sanity checks */
  if (ndims != 3) {
    printf("ndims is not 3 in solver.inp. this code is to generate 3D initial conditions\n");
    return(0);
  }
  if (HB != 2) {
    printf("Error: Specify \"HB\" as 2 .\n");
  }
  if ((grav_x != 0.0) || (grav_y != 0.0)) {
    printf("Error: gravity must be zero along x and y for this example.\n");
    return(0);
  }

   printf("Grid:\t\t\t%d X %d X %d\n",NI,NJ,NK);
   printf("Reference density and pressure: %lf, %lf.\n",rho_ref,p_ref);

   int i,j,k;
  /* compute grid resolution */
	double dx = (Lx_u - Lx_l) / ((double)(NI-1));
	double dy = (Ly_u - Ly_l) / ((double)(NJ-1));
	double dz = (Lz_u - Ly_l) / ((double)(NK-1));

  /* allocate arrays */
	double *x, *y, *z, *U;
	x   = (double*) calloc (NI        , sizeof(double));
	y   = (double*) calloc (NJ        , sizeof(double));
	z   = (double*) calloc (NK        , sizeof(double));
	U   = (double*) calloc (nvars*NI*NJ*NK, sizeof(double));

  /* temperature at zero altitude */
  double T_ref = p_ref / (R * rho_ref);

  /* specific heat coefficient */
  double Cp = gamma * R / (gamma-1.0);

  /* loop through all grid points */
	for (i = 0; i < NI; i++){
  	  for (j = 0; j < NJ; j++){
        for (k = 0; k < NK; k++){

        /* set spatial coordinates of this grid point */
	  	x[i] = Lx_l + i*dx;
	  	y[j] = Ly_l + j*dy;
        z[k] = Lz_l + k*dz;

        /* global 1D index for (i,j,k) */
        int p = i + NI*j + NI*NJ*k;

        /* velocity field */
        double uvel = 20.0;
        double vvel = 0.0;
        double wvel = 0.0;

        /* kinetic energy */
        double KE = 0.5 * (uvel*uvel + vvel*vvel + wvel*wvel);

        /* iso-potential-temperature hydrostatic balance - set potential temperature
         * to temperature at zero altitude */
        double theta = T_ref;
        /* compute Exner pressure as a function of altitude and temperature */
        double Pexner = 1.0 - (grav_z*z[k])/(Cp*T_ref);

        /* compute density from zero-altitude pressure, potential temperature, and Exner pressure */
        double rho = (p_ref/(R*theta)) * raiseto(Pexner, (1.0/(gamma-1.0)));
        /* compute energy from density, potential temperature, and Exner pressure */
        double E = rho * (R/(gamma-1.0)) * theta*Pexner + rho * KE;

        /* set conserved variables at this grid point */
        U[5*p+0] = rho;
        U[5*p+1] = rho * uvel;
        U[5*p+2] = rho * vvel;
        U[5*p+3] = rho * wvel;
        U[5*p+4] = E;
      }
	  }
	}

  /* Write to file in the format HyPar wants */
  FILE *out;
  if (!strcmp(ip_file_type,"ascii")) {

    printf("Error: sorry, ASCII format initial solution file not implemented. ");
    printf("Please choose \"ip_file_format\" here and in solver.inp as \"binary\".\n");
    return(0);

  } else if ((!strcmp(ip_file_type,"binary")) || (!strcmp(ip_file_type,"bin"))) {

    printf("Writing binary initial solution file initial.inp\n");
  	out = fopen(filename,"wb");
    fwrite(x, sizeof(double), NI, out);
    fwrite(y, sizeof(double), NJ, out);
    fwrite(z, sizeof(double), NK, out);
    fwrite(U, sizeof(double), nvars*NI*NJ*NK, out);
    fclose(out);
  }
  /* Done */
	free(x);
	free(y);
	free(z);
	free(U);

	return(0);
}
