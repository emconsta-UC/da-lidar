/*
  Utility functions for HyPar-based LIDAR project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*
  This code generates the initial solution for the rising
  thermal bubble case for the 3D Navier-Stokes equations.

  Note: this code allocates the global domain, so be careful
  when using it for a large number of grid points.
*/



int _ArrayIndex1D_(int N, int *imax, int *i, int ghost){
  int index = i[N-1]+ghost;
  int arraycounter;
  for (arraycounter = N-2; arraycounter > -1; arraycounter--) {
    index = (index*(imax[arraycounter]+2*(ghost))) + (i[arraycounter]+(ghost));
  }
  return(index);
}

int _ArrayIncrementIndex_(int N, int *imax, int *i)
{
  int done;
  int arraycounter = 0;
  while (arraycounter < (N)) {
    if (i[arraycounter] == imax[arraycounter]-1) {
      i[arraycounter] = 0;
      arraycounter++;
    } else {
      i[arraycounter]++;
      break;
    }
  }
  if (arraycounter == (N)) done = 1;
  else done = 0;

  return(done);
}


// Function to concatenate two paths
void combine_paths(const char* path1, const char* path2, char* destination)
{
    if(path1 == NULL && path2 == NULL) {
        strcpy(destination, "");
    }
    else if(path2 == NULL || strlen(path2) == 0) {
        strcpy(destination, path1);
    }
    else if(path1 == NULL || strlen(path1) == 0) {
        strcpy(destination, path2);
    }
    else {
        char directory_separator[] = "/";
#ifdef WIN32
        directory_separator[0] = '\\';
#endif
        const char *last_char = path1;
        while(*last_char != '\0')
            last_char++;
        int append_directory_separator = 0;
        if(strcmp(last_char, directory_separator) != 0) {
            append_directory_separator = 1;
        }
        strcpy(destination, path1);
        if(append_directory_separator)
            strcat(destination, directory_separator);
        strcat(destination, path2);
    }
}


int read_physics_input(char *filename, 
					   int ndims, 
					   double *rho_ref, 
					   double *p_ref, 
					   double *gamma, 
					   double *R,
					   double *HB,
					   double *gravity,
					   char *upwinding,
					   double *Pr,
					   double *Re,
					   double *Minf,
					   double *N_bv,
					   char *ib_surface_data)
{

  int i, ferr;
  FILE *in;
  in = fopen(filename, "r");
 
  if (!in) {
    fprintf(stdout, "Error: Input file \"%s\" not found.\n", filename);
    return(0);
  } else {
    char word[500];
    fscanf(in,"%s",word);
    if (!strcmp(word, "begin")) {
      while (strcmp(word, "end")) {
        fscanf(in,"%s",word);
        if      (!strcmp(word, "rho_ref")) ferr = fscanf(in,"%lf",rho_ref); if (ferr != 1) return(1);
        else if (!strcmp(word, "p_ref"  )) ferr = fscanf(in,"%lf",p_ref  ); if (ferr != 1) return(1);
		else if (!strcmp(word, "upwinding")) ferr = fscanf(in,"%s",upwinding); if (ferr != 1) return(1);
        else if (!strcmp(word, "Pr"  )) ferr = fscanf(in,"%lf",Pr  ); if (ferr != 1) return(1);
        else if (!strcmp(word, "Re"  )) ferr = fscanf(in,"%lf",Re  ); if (ferr != 1) return(1);
        else if (!strcmp(word, "Minf")) ferr = fscanf(in,"%lf",Minf); if (ferr != 1) return(1);
        else if (!strcmp(word, "Re"  )) ferr = fscanf(in,"%lf",Re  ); if (ferr != 1) return(1);
        else if (!strcmp(word, "ib_surface_data" )) ferr = fscanf(in,"%s",ib_surface_data ); if (ferr != 1) return(1);
		else if (!strcmp(word, "gamma"  )) ferr = fscanf(in,"%lf",gamma  ); if (ferr != 1) return(1);
        else if (!strcmp(word, "R"      )) ferr = fscanf(in,"%lf",R      ); if (ferr != 1) return(1);
        else if (!strcmp(word, "HB"     )) {
		  ferr = fscanf(in,"%lf" ,HB); if (ferr != 1) return(1);
		  if (*HB == 3) { 
	        ferr = fscanf(in,"%lf" ,N_bv); if (ferr != 1) return(1); 
		  }
		}
        else if (!strcmp(word, "gravity")) {
		  for (i=0; i<ndims; i++) ferr = fscanf(in,"%lf", &gravity[i]);
		  if (ferr != 1) return(1);
		}
      }
    } else fprintf(stdout, "Error: Illegal format in \"%s\". Crash and burn!\n", filename);
  }
  fclose(in);

return(0);
}
  


int read_model_configs_minimal(char *filename, int *NI, int *NJ, int *NK, int *ndims, int **iproc)
{

  int loc_ndims_ = 3, i, ferr;

  *iproc = malloc(loc_ndims_ * sizeof(int));
  if (*iproc == NULL){
    fprintf(stdout, "FAILED to allocate iproc!\n");
    return(1);
  }

  FILE *in;
  /* read solver.inp */
  // fprintf(stdout, "Reading file \"%s\"...\n", filename);
  in = fopen(filename, "r");
  if (!in) {
    fprintf(stdout, "Error: Input file \"%s\" not found.\n", filename);
    return(1);
  } else {
    char word[500];
    fscanf(in,"%s",word);
    if (!strcmp(word, "begin")) {
      while (strcmp(word, "end")) {
        fscanf(in,"%s",word);
        if (!strcmp(word, "ndims")) fscanf(in,"%d",ndims);
        else if (!strcmp(word, "size")) {
          fscanf(in,"%d",NI);
          fscanf(in,"%d",NJ);
          fscanf(in,"%d",NK);
        }
        else if(!strcmp(word, "iproc")){
           for (i=0; i<loc_ndims_; i++) ferr = fscanf(in,"%d", &**iproc+i);
           if (ferr != 1) return(1);
        }
      }
    } else fprintf(stdout, "Error: Illegal format in solver.inp. Crash and burn!\n");
  }
  fclose(in);

  /* some sanity checks */
  if (*ndims != 3) {
    fprintf(stdout, "ndims is not 3 in solver.inp. this code is to generate 3D initial conditions\n");
    return(1);
  }

	return(0);
}


int read_model_configs(char *filename, int *nvars, int *ndims, int **size, int **iproc, double **domain_lower_bounds, double **domain_upper_bounds, long *n_iter, double *dt, int *restart_iter)
{

  int loc_ndims_ = 3, i, ferr;

  *iproc = malloc(loc_ndims_ * sizeof(int));
  if (*iproc == NULL){fprintf(stdout, "FAILED to allocate iproc!\n");}
  *domain_lower_bounds = malloc(loc_ndims_ * sizeof(double));
  if (*domain_lower_bounds == NULL){fprintf(stdout, "FAILED to allocate domain_lower_bounds!\n");}
  *domain_upper_bounds = malloc(loc_ndims_ * sizeof(double));
  if (*domain_upper_bounds == NULL){fprintf(stdout, "FAILED to allocate domain_upper_bounds!\n");}
  *size = malloc(loc_ndims_ * sizeof(int));
  if (*size == NULL){fprintf(stdout, "FAILED to allocate size!\n");}

  FILE *in;
  /* read solver.inp */
  // fprintf(stdout, "Reading file \"%s\"...\n", filename);
  in = fopen(filename, "r");
  if (!in) {
    fprintf(stdout, "Error: Input file \"%s\" not found.\n", filename);
    return(1);
  } else {
    char word[500];
    fscanf(in,"%s",word);
    if (!strcmp(word, "begin")) {
      while (strcmp(word, "end")) {
        fscanf(in,"%s",word);
        if (!strcmp(word, "ndims")) fscanf(in,"%d",ndims);
        else if(!strcmp(word, "nvars")) fscanf(in,"%d",nvars);
        else if(!strcmp(word, "dt")) fscanf(in,"%lf",dt);
        else if(!strcmp(word, "restart_iter")) fscanf(in,"%d",restart_iter);
        else if(!strcmp(word, "iproc")){
           for (i=0; i<loc_ndims_; i++) ferr = fscanf(in,"%d", &**iproc+i);
           if (ferr != 1) return(1);
        }
        else if(!strcmp(word, "domain_lower_bounds")){
           for (i=0; i<loc_ndims_; i++) ferr = fscanf(in,"%lf", &**domain_lower_bounds+i);
           if (ferr != 1) return(1);
        }
        else if(!strcmp(word, "domain_upper_bounds")){
           for (i=0; i<loc_ndims_; i++) ferr = fscanf(in,"%lf", &**domain_upper_bounds+i);
           if (ferr != 1) return(1);
        }
        else if(!strcmp(word, "size")){
           for (i=0; i<loc_ndims_; i++) ferr = fscanf(in,"%d", &**size+i);
           if (ferr != 1) return(1);
        }
      }
    } else fprintf(stdout, "Error: Illegal format in solver.inp. Crash and burn!\n");
  }
  fclose(in);

  /* some sanity checks */
  if (*ndims != 3) {
    fprintf(stdout, "ndims is not 3 in solver.inp. this code is to generate 3D initial conditions\n");
    return(1);
  }

	return(0);
}


int write_model_configs(char* filename, int ndims, int nvars, int *size, int *iproc, int ghost, \
                        double *domain_lower_bounds, double *domain_upper_bounds, \
						long n_iter, int rest_iter, char *ts, char *ts_type, \
                        char *hyp_scheme, char *hyp_flux_split, char *hyp_int_type, \
                        char *par_type, char *par_scheme, double dt, char *cons_check,\
                        int screen_op_iter, int file_op_iter, char *op_format, char *ip_type, \
                        char *input_mode, char *output_mode, int n_io, char *op_overwrite, char *model)
{
  if (strcmp(filename,"solver.inp")){
    fprintf(stdout, "WARNING: Passed file name [%s] WILL NOT BE RECOGNIZED by HyPar\n\t Configurations MUST by written to 'solver.inp' \n", filename);
  }

  FILE *out;
  /* write to filename */
  // fprintf(stdout, "Preparing file: \"%s\" \n", filename);
  out = fopen(filename, "w");
  if (!out) {
    fprintf(stdout, "Error: Output file \"%s\" couldn't be open for output!.\n", filename);
    return(1);
  } else {
    int i;
    fprintf(out,"begin\n");
    fprintf(out,"\tndims              %d\n",ndims);
    fprintf(out,"\tnvars              %d\n",nvars);
    fprintf(out,"\tsize               ");
      for (i=0; i<ndims; i++)fprintf(out,"%d ",size[i]);
      fprintf(out,"\n");
    fprintf(out,"\tiproc              ");
      for (i=0; i<ndims; i++)fprintf(out,"%d ",iproc[i]);
      fprintf(out,"\n");
    fprintf(out,"\tghost              %d\n",ghost);
    fprintf(out,"\tdomain_lower_bounds              ");
      for (i=0; i<ndims; i++)fprintf(out,"%lf ",domain_lower_bounds[i]);
      fprintf(out,"\n");
    fprintf(out,"\tdomain_upper_bounds              ");
      for (i=0; i<ndims; i++)fprintf(out,"%lf ",domain_upper_bounds[i]);
      fprintf(out,"\n");
    fprintf(out,"\tn_iter             %ld\n",n_iter);
    fprintf(out,"\trestart_iter       %d\n",rest_iter);

    if (strcmp(ts, "none")){fprintf(out,"\ttime_scheme        %s\n",ts);}
    if (strcmp(ts_type, "none")){fprintf(out,"\ttime_scheme_type   %s\n",ts_type);}
    if (strcmp(hyp_scheme, "none")){fprintf(out,"\thyp_space_scheme   %s\n",hyp_scheme);}
    if (strcmp(hyp_flux_split, "none")){fprintf(out,"\thyp_flux_split     %s\n",hyp_flux_split);}
    if (strcmp(hyp_int_type, "none")){fprintf(out,"\thyp_interp_type    %s\n",hyp_int_type);}
    if (strcmp(par_type, "none")){fprintf(out,"\tpar_space_type     %s\n",par_type);}
    if (strcmp(par_scheme, "none")){fprintf(out,"\tpar_space_scheme   %s\n",par_scheme);}
    fprintf(out,"\tdt                 %lf\n",dt);
    if (strcmp(cons_check, "none")){fprintf(out,"\tconservation_check %s\n",cons_check);}
    fprintf(out,"\tscreen_op_iter     %d\n",screen_op_iter);
    fprintf(out,"\tfile_op_iter       %d\n",file_op_iter);
    if (strcmp(op_format, "none")){fprintf(out,"\top_file_format     %s\n",op_format);}
    if (strcmp(ip_type, "none")){fprintf(out,"\tip_file_type       %s\n",ip_type);}
    if (strcmp(input_mode, "none")){
      fprintf(out,"\tinput_mode         %s"  ,input_mode);
      if (strcmp(input_mode,"serial")){fprintf(out," %d\n",n_io);}else{fprintf(out,"\n");}
    }
    if (strcmp(output_mode, "none")){
      fprintf(out,"\toutput_mode        %s"  ,output_mode);
      if (strcmp(output_mode,"serial")){fprintf(out," %d\n",n_io);}else{fprintf(out,"\n");}
    }
    if (strcmp(op_overwrite, "none")){fprintf(out,"\top_overwrite       %s\n",op_overwrite);}

    if (strcmp(model, "none")){fprintf(out,"\tmodel              %s\n",model);}
    fprintf(out,"end\n");
  }

  fclose(out);

  return(0);
}



/* This function, and the following, are requred by the function 'write_temperature_to_file'
 * This function must be consistent with the same function
 * in hypar/src/MPIFunctions/MPIPartition1D.c
 */
int MPIPartition1D(int nglobal,int nproc,int rank)
{
  int nlocal;
  if (nglobal%nproc == 0) nlocal = nglobal/nproc;
  else {
    if (rank == nproc-1)  nlocal = nglobal/nproc + nglobal%nproc;
    else                  nlocal = nglobal/nproc;
  }
  return(nlocal);
}

/*  
 * This function must be consistent with the same function
 * in hypar/src/MPIFunctions/MPILocalDomainLimits.c
 */
int MPILocalDomainLimits(int ndims,int* ip,int *iproc,int *dim_global,int *is, int *ie)
{
  int i;
  for (i=0; i<ndims; i++) {
    int imax_local, isize, root = 0;
    imax_local = MPIPartition1D(dim_global[i],iproc[i],root );
    isize      = MPIPartition1D(dim_global[i],iproc[i],ip[i]);
    if (is)  is[i] = ip[i]*imax_local;
    if (ie)  ie[i] = ip[i]*imax_local + isize;
  }
  return(0);
}



int ReadGrid(
              int     ndims,          /*!< Number of spatial dimensions */
              int     *dim,           /*!< Integer array of size ndims with global grid size in each dimension */
              double  *x,             /*!< Grid associated with the array */
              double  *y,             /*!< Grid associated with the array */
              double  *z,             /*!< Grid associated with the array */
              char    *ip_file_type   /*!< Initial solution file type (ASCII or binary) */
            )
{
  int i, d, ferr, size;

  /* allocate grid array */
  size = 0;
  for (d=0; d<ndims; d++) size += dim[d];
  double *xg = (double*) calloc(size,sizeof(double));

  if (!strcmp(ip_file_type,"ascii")) {

    FILE *in;
    in = fopen("initial.inp","r");

    if (!in) {

      printf("ERROR: initial.inp not found!\n");
      return(1);

    } else {

      printf("Reading from ASCII file initial.inp.\n");

      /* read grid */
      int offset = 0;
      for (d = 0; d < ndims; d++) {
        for (i = 0; i < dim[d]; i++) ferr = fscanf(in,"%lf",&xg[i+offset]);
        offset += dim[d];
      }

      fclose(in);

    }
  } else if ((!strcmp(ip_file_type,"bin")) || (!strcmp(ip_file_type,"binary"))) {

    FILE *in;
    in = fopen("initial.inp","rb");

    if (!in) {

      printf("ERROR: initial.inp not found!\n");
      return(1);

    } else {

      printf("Reading array from binary file initial.inp.\n");
      size_t bytes;
      int size;

      size = 0; for (d=0; d<ndims; d++) size += dim[d];
      xg = (double*) calloc(size,sizeof(double));

      /* read grid */
      size = 0;
      for (d = 0; d < ndims; d++) size += dim[d];
      bytes = fread(xg, sizeof(double), size, in);
      if ((int)bytes != size) {
        fprintf(stderr,"Error in ReadArray(): Unable to read grid. Expected %d, Read %d.\n",
                size, (int)bytes);
      }

      fclose(in);
    }

  }


  /* copy to x,y,z */
  for (i = 0; i < dim[0]; i++) x[i] = xg[i];
  for (i = 0; i < dim[1]; i++) y[i] = xg[i+dim[0]];
  for (i = 0; i < dim[2]; i++) z[i] = xg[i+dim[0]+dim[1]];

  free(xg);
  return(0);
}
	


/* This function takes in the temperature data as a global array, the number of MPI ranks along each spatial dimension,
 * the global dimension along each spatial dimension, the dimension along which the thermal wall BC is being applied,
 * and the face at which it is being applied (i.e. the min face (lo end) or the max face (hi end)).
 *
 * It then partitions the global data into subdomains for each participating MPI ranks and writes them out to
 * a binary file. The domain decomposition is consistent with that in HyPar, and the file is written in a format
 * that HyPar wants.
 *
 * Note that all MPI ranks are not participants; only those abutting the boundary face at which the thermal wall BC
 * is being applied need this data.
 *
 * Along all dimensions that are not a_bc_dim, a_dims_global[i] must be equal to the global grid size of the
 * HyPar simulation along that dimension.
 *
 * If the temperature field is steady, i.e., the data is available for only one time level (t=0),
 *  a_dims_global[a_bc_dim] must be 1.
 *
 * If the temperature field is unsteady, HyPar will want to read in the stacked data for all the time levels,
 *  a_dims_global[a_bc_dim] must be the number of time levels
 * During the simulation, HyPar will apply the temperature data from the index (along a_bc_dim) corresponding to
 * the highest time level that is <= to the current simulation/stage time.
 *
 * For example, if a_bc_dim = 1 (the second spatial dimension), HyPar implements:
 *  T[i][j_boundary][k](t) = T_input[i][t_index][k],
 * where t_index is the highest integer in [0,a_dims_global[a_bc_dim]-1] satisfying
 *  a_time_levels[t_index] <= t
*/
int  write_temperature_to_file( int     ndims,          /* number of spatial dimensions */
                  double  *a_T_global,    /* global array of the temperature data */
                  int     *a_iproc,       /* int array with the number of MPI ranks along each spatial dimension */
                  int     *a_dims_global, /* int array with global dimension along each spatial dimension */
                  int     a_bc_dim,       /* dimension along which the thermal BC is being applied */
                  int     a_bc_dir,       /* face at which the thermal BC is being applied: 1 -> min face, -1 -> max face */
                  char    *a_filename,    /* filename to which to write the BC data */
                  double  *a_time_levels  /* the simulation time for each temperature data; this array must
                                             be of size a_dims_global[a_bc_dim] */
                )
{
 int ferr;
  FILE *out;
  out = fopen(a_filename,"wb");

  int ip[3];

  for (ip[0] = 0; ip[0] < a_iproc[0]; ip[0]++) {
    for (ip[1] = 0; ip[1] < a_iproc[1]; ip[1]++) {
      for (ip[2] = 0; ip[2] < a_iproc[2]; ip[2]++) {

        if (    ( (a_bc_dir ==  1) && (ip[a_bc_dim] == 0) )
            ||  ( (a_bc_dir == -1) && (ip[a_bc_dim] == a_iproc[a_bc_dim]-1) )  ) {

          int rank[ndims];
          rank[0] = ip[0];
          rank[1] = ip[1];
          rank[2] = ip[2];
 
          int rank_fake[ndims];
          rank_fake[0] = rank[0];
          rank_fake[1] = rank[1];
          rank_fake[2] = rank[2];
          rank_fake[a_bc_dim] = 0;
  
          int nproc[ndims]; 
          nproc[0] = a_iproc[0]; 
          nproc[1] = a_iproc[1]; 
          nproc[2] = a_iproc[2];
          nproc[a_bc_dim] = 1;
          
		  /*  // Debugging:...
		   *  printf("PY: >>>>>\n Py: Writing The temperature FILE\n");
		   *  printf("PY: Ranks; %d , %d , %d\n", rank[0],rank[1],rank[2]);
          */
		   ferr = fwrite(rank,sizeof(int),ndims,out);
          if (ferr != ndims) {
            printf("Error: (%d,%d,%d) Unable to write boundary data file (rank).\n",rank[0],rank[1],rank[2]);
            return(1);
          } 
          int size[ndims], is[ndims], ie[ndims];
          MPILocalDomainLimits(ndims,rank_fake,nproc,a_dims_global,is,ie);
          size[0] = ie[0] - is[0];
          size[1] = ie[1] - is[1];
          size[2] = ie[2] - is[2];
          if (size[a_bc_dim] != a_dims_global[a_bc_dim]) {
            printf("Error: Something went wrong. size[a_bc_dim] is %d, but a_dims_global[a_bc_dim] is %d.\n",
                    size[a_bc_dim], a_dims_global[a_bc_dim]);
            return(1);
          } 
          /* // Debugging:...
		   * printf("PY: a_bc_dim: %d \n" , a_bc_dim);
		   * printf("PY: a_dims_global[a_bc_dim]: %d \n" , a_dims_global[a_bc_dim]);
		   * printf("PY: ie; %d , %d , %d\n", ie[0],ie[1],ie[2]);
		   * printf("PY: is; %d , %d , %d\n", is[0],is[1],is[2]);
		   * printf("PY: Sizess; %d , %d , %d\n", size[0],size[1],size[2]);
          */
		  ferr = fwrite(size,sizeof(int),ndims,out);
          if (ferr != ndims) {
            printf("Error: (%d,%d,%d) Unable to write boundary data file (size).\n",rank[0],rank[1],rank[2]);
            return(1);
          } 

          /* printf("PY: TimeLevels; double of size; %d\n", size[a_bc_dim]); */
          ferr = fwrite(a_time_levels,sizeof(double),size[a_bc_dim],out);
          if (ferr != size[a_bc_dim]) {
            printf("Error: (%d,%d,%d) Unable to write boundary data file (time levels).\n",rank[0],rank[1],rank[2]);
            return(1);
          }

          int local_size = size[0] * size[1] * size[2];
          double *T_local = (double*) calloc (local_size,sizeof(double));
          int i,j,k;
          for (i = 0; i < size[0]; i++) {
            for (j = 0; j < size[1]; j++) {
              for (k = 0; k < size[2]; k++) {
                int p1 = i + size[0]*j + size[0]*size[1]*k;
                int p2 =    (i+is[0])
                          + a_dims_global[0]*(j+is[1])
                          + a_dims_global[0]*a_dims_global[1]*(k+is[2]);
                T_local[p1] = a_T_global[p2];
              }
            }
          }
          
		  /* // Debugging
		  // printf("DLiDA: Writing Temperature file as follows");
		  // for(i=0, i<local_size, i++){printf("DLiDATemperatureWriter: Index:%d; Temperature: %f\n", i, T_local[i]);}
		  //
		  */

		  /* printf("PY: Writing T_local; double;  of size: %d\n", local_size); */
		  ferr = fwrite(T_local,sizeof(double),local_size,out);
          if (ferr != local_size) {
            printf("Error: (%d,%d,%d) Unable to write boundary data file (T_local) (%d, expected %d).\n",
                   rank[0],rank[1],rank[2],ferr,local_size);
            return(1);
          }
          free(T_local);
        }
      }
    }
  }
  fclose(out);

  return(0);
}



