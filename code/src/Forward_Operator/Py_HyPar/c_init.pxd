

cdef extern from "./aux/init.c":
  cdef int create_initial_condition(char *filename, int ndims, int nvars,
                             double Lx, double Ly, double Lz, double Lx_u, double Ly_u, double Lz_u,
                             int NI, int NJ, int NK,
                             double gamma, double R, double rho_ref, double p_ref,
                             double grav_x, double grav_y, double grav_z, int HB, char *ip_file_type)

cdef extern from "./aux/read_write_solution.c":

  cdef int alloc_and_read_initial_condition(char* filename, int NI, int NJ, int NK, int ndims, double **x, double **y, double **z, double **U)
  cdef int read_initial_condition(char* filename, int NI, int NJ, int NK, int ndims, double *x, double *y, double *z, double *U)

  cdef int read_binary_solution(char* filename, int *NI, int *NJ, int *NK, int *ndims, double *x, double *y, double *z, double *U)

  cdef int write_initial_condition(char* filename, int NI, int NJ, int NK, int ndims, double *x, double *y, double *z, double *U)
  cdef int write_binary_array(char* filename, int NI, int NJ, int NK, int ndims, int nvars, double *x, double *y, double *z, double *U)



cdef extern from "./aux/utilities.c":

  cdef void combine_paths(const char* path1, const char* path2, char* destination)

  cdef int _ArrayIndex1D_(int N, int *imax, int *i, int ghost)
  cdef int _ArrayIncrementIndex_(int N, int *imax, int *i)

  cdef int read_model_configs_minimal(char* filename, int *NI, int *NJ, int *NK, int *ndims, int **iproc)
  cdef int read_model_configs(char* filename, int *nvars, int *ndims, int **size, int **iproc, double **domain_lower_bounds, double **domain_upper_bounds, long *n_iter, double *dt, int *restart_iter)

  cdef int write_model_configs(char* filename, int ndims, int nvars, int *size, int *iproc, int ghost, \
                               double *domain_lower_bounds, double *domain_upper_bounds, \
                               long n_iter, int rest_iter, char *ts, char *ts_type, \
                               char *hyp_scheme, char *hyp_flux_split, char *hyp_int_type, \
                               char *par_type, char *par_scheme, double dt, char *cons_check,\
                               int screen_op_iter, int file_op_iter, char *op_format, char *ip_type, \
                               char *input_mode, char *output_mode, int n_io, char *op_overwrite, char *model)

  cdef int read_physics_input(char *filename, int ndims, double *rho_ref, double *p_ref, double *gamma, double *R, double *HB, double *gravity, char *upwinding, double *Pr,
                             double *Re, double *Minf, double *N_bv, char *ib_surface_data)
  cdef int write_temperature_to_file(int ndims, double *a_T_global, int *a_iproc, int *a_dims_global, int a_bc_dim, int a_bc_dir, char *a_filename, double *a_time_levels)
