
# TODO: for all file IO, make sanity checks!

cimport c_init as _hypar_sol_handler

import os
import sys
python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    range = xrange

cimport numpy as np
import numpy as np

import shutil

import copy

# TODO: this use_MPI was exploratory, and will be removed in the future;
try:
  from mpi4py import MPI
  use_MPI = True
except(ImportError):
  use_MPI = False

import cython
import re

from libc.stdlib cimport calloc, malloc, free, system
from libc.math cimport sqrt, pow, abs

from DLidarVec cimport StateVector, Ensemble
from DLidarVec import StateVector, Ensemble

# Unify used data types:
DTYPE_DOUBLE = np.float64

MODEL_TIME_EPS = 1e-7


cdef _rel_model_path = "src/Forward_Operator/Py_HyPar"  # This shouldn't/won't be kept hard coded for long!


cdef class HyPar_Model:
  """
  This class defines a low-level interaction with HyPar input-settings-output files and executable.
  """

  cdef char* __SEPP__


  cdef int _NDIMS
  cdef int _NI
  cdef int _NJ
  cdef int _NK
  cdef int _NVARS
  cdef long int _STATE_SIZE
  cdef int _NUM_PROC
  # cdef double _DT
  cdef long _N_ITER
  cdef int _RESTART_ITER


  # Grid coordinates, and State vector:
  cdef double* _X
  cdef double* _Y
  cdef double* _Z
  # cdef double* _U
  cdef StateVector MODEL_STATE

  # Misc
  cdef dict _model_configs

  cdef dict _paths_dict

  cdef bint _INITIALIZED


  #
  #
  def __cinit__(self, model_configs=None, **kwargs):
    self._INITIALIZED = False
    self.__init__(model_configs=model_configs, **kwargs)


  def __dealloc__(self):
    if self._X != NULL:
      free(self._X)
    if self._Y != NULL:
      free(self._Y)
    if self._Z != NULL:
      free(self._Z)
    # if self.MODEL_STATE != NULL:
    #   free(self.MODEL_STATE)
    #
    # ... DONE ...
    #
    # cleanup dire
    try:
      cleanup_dir = self._model_configs['cleanup_dir']
    except(KeyError):
      cleanup_dir = True
    if cleanup_dir:
      model_path = self._paths_dict['model_path']
      if os.path.isdir(model_path):
        shutil.rmtree(model_path)
        print("Model path properly cleaned: %s " % model_path)
      else:
        print("DIDN'T find model path! Can't remove  [%s] !" % model_path)
    else:
      pass  # don't cleanup model working directory


  def __init__(self, model_configs=None, **kwargs):
    """
    Args:
      model_configs: a dictionary containing model settings to be written in 'solver.inp'
        supported configurations:
        ndims:
        nvars:
        domain_lower_bounds: iterable of size ndims, contatining lower bounds of the domain e.g. [0, 0, 0] for 3d model
        domain_upper_bounds: iterable of size ndims, contatining upper bounds of the domain e.g. [50000, 50000, 4000] for 3d model
        PETSc_options: (string) options passed to PETSc at runtime, e.g. "-ts_type rk -ts_rk_type 4"
        size:
        iproc:
        ghost:
        n_iter:
        rest_iter:
        ts:
        ts_type:
        hyp_scheme:
        hyp_flux_split:
        hyp_int_type:
        par_type:
        par_scheme:
        dt:  step size (in seconds)
        time_unit:  time unit 's', 'm', 'h' for second, minute, houre
        cons_check:
        screen_op_iter:
        file_op_iter:
        op_format:
        ip_type:
        input_mode:
        output_mode:
        n_io:
        op_overwrite:
        model:
        cleanup_dir: remove the model input/output directory upon termination

    """
    cdef bytes py_sepp
    cdef bytes py_path
    cdef char* model_path
    cdef py_cmd
    cdef char* model_run_cmd
    cdef char* filename

    if not self._INITIALIZED:
      if python_version >= (3, 0, 0):
        py_sepp = <bytes>("\n%s\n" % ("="* 50))
      else:
        py_sepp = <bytes>(b"\n%s\n" % ("="* 50))
      self.__SEPP__= py_sepp
      #

      model_configs = self.validate_model_configs(model_configs)

      self._INITIALIZED = False
      DL_ROOT_PATH = os.environ.get('DL_ROOT_PATH')
      HYPAR_ROOT_PATH = os.environ.get('HYPAR_ROOT_PATH')
      HYPAR_REL_EXEC = os.environ.get('HYPAR_REL_EXEC')
      HYPAR_MPI_CMD = os.environ.get('HYPAR_MPI_CMD')

      if python_version >= (3, 0, 0):
        DL_ROOT_PATH = str.encode(DL_ROOT_PATH)
        HYPAR_ROOT_PATH = str.encode(HYPAR_ROOT_PATH)
        HYPAR_REL_EXEC = str.encode(HYPAR_REL_EXEC)
        HYPAR_MPI_CMD = str.encode(HYPAR_MPI_CMD)

      if DL_ROOT_PATH is None or len(str(DL_ROOT_PATH).strip())==0:
        print("%s\n\tDL_ROOT_PATH must be defined as an environment variable before attempting to use this module\n" % self.__SEPP__)
        print("\n\t\t * You may need to look at how DL_Data_Handler is handled!\n\n%s\n" % self.__SEPP__)
        raise ValueError()

      if HYPAR_ROOT_PATH is None or len(str(HYPAR_ROOT_PATH).strip())==0:
        print("%s\n\HYPAR_ROOT_PATH must be defined as an environment variable before attempting to use this module\n" % self.__SEPP__)
        print("\n\t\t * You may need to load Hypar_constants before creating this model!\n\n%s\n" % self.__SEPP__)
        raise ValueError()

      if HYPAR_REL_EXEC is None or len(str(HYPAR_REL_EXEC).strip())==0:
        print("%s\n\HYPAR_REL_EXEC must be defined as an environment variable before attempting to use this module\n" % self.__SEPP__)
        print("\n\t\t * You may need to load Hypar_constants before creating this model!\n\n%s\n" % self.__SEPP__)
        raise ValueError()

      if HYPAR_MPI_CMD is None or len(str(HYPAR_MPI_CMD).strip())==0:
        print("%s\n\HYPAR_REL_EXEC must be defined as an environment variable before attempting to use this module\n" % self.__SEPP__)
        print("\n\t\t * You may need to load Hypar_constants before creating this model!\n\n%s\n" % self.__SEPP__)
        raise ValueError()


      # Get essential configurations:
      self._NDIMS = <int>model_configs['ndims']
      sizes = model_configs['size']
      iproc = model_configs['iproc']
      assert len(sizes)==self._NDIMS, "size must be an iterable of length equal to ndims, i.e. 3!"
      assert len(iproc)==self._NDIMS, "iproc must be an iterable of length equal to ndims, i.e. 3!"

      self._NVARS = <int>model_configs['nvars']

      self._NI = <int>sizes[0]
      self._NJ = <int>sizes[1]
      self._NK = <int>sizes[2]

      self._STATE_SIZE = self._NI * self._NJ * self._NK * self._NVARS

      self._NUM_PROC = 1
      for i in range(self._NDIMS):
        self._NUM_PROC *= iproc[i]

      if use_MPI:
        COMM = MPI.COMM_WORLD
        comm_size = COMM.Get_size()
        my_rank = COMM.Get_rank()
      else:
        comm_size = 1
        my_rank = 0

      # inspect/create proper directories
      fold_lim = 100  # maximum number of simultaneous instances on this node
      for i in range(fold_lim):  # Is it possible to run more than 50 instances on a single node!
        #
        rank_dir = "_HyPar_Instance_%d_Rank_%d" % (i, my_rank)
        if python_version >= (3, 0, 0):
          py_path = os.path.join(DL_ROOT_PATH, str.encode(_rel_model_path))
          rank_dir = str.encode(rank_dir)
        else:
          py_path = os.path.join(DL_ROOT_PATH, _rel_model_path)
        output_path = os.path.join(py_path, rank_dir)
        #
        if not os.path.isdir(output_path):
          break
        elif i == fold_lim-1:
          print("Can't  create more than %d instances of HyPar under DLiDA" % fold_lim)
          print("Consider removing directory: %s" % output_path)
          raise OSError
        elif i >= (fold_lim // 5):
          print("WARNING: More than %d instances of DLiDA-HyPar seems to be running simultaneously, previous runs were not cleaned up properly!" % (fold_lim//5))
          print("Consider removing directory: %s" % output_path)
          #
      model_path = <char*> output_path

      # create/cleanup model instance directory
      if os.path.isdir(output_path):
        print("This shouldn't happen! Check the new implementation!")
        raise OSError
        # shutil.rmtree(output_path)
      os.makedirs(output_path)

      # PETSc runtime options passed to HyPar
      PETSc_options = model_configs['PETSc_options']
      # Prepare run command:
      if python_version >= (3, 0, 0):
        py_cmd = b"%s -n %d %s %s" % (HYPAR_MPI_CMD, self._NUM_PROC, os.path.join(HYPAR_ROOT_PATH, HYPAR_REL_EXEC), PETSc_options.strip(b" "))
      else:
        py_cmd = "%s -n %d %s %s" % (HYPAR_MPI_CMD, self._NUM_PROC, os.path.join(HYPAR_ROOT_PATH, HYPAR_REL_EXEC), PETSc_options.strip(" "))
      model_run_cmd = <char*> py_cmd

      # Basic file-names
      init_sol_file_name = "initial.inp"
      config_file_name   = "solver.inp"
      out_sol_file_name  = "op.bin"
      physics_file_name  = 'physics.inp'
      boundary_file_name = 'boundary.inp'
      if python_version >= (3, 0, 0):
        init_sol_file_name = str.encode(init_sol_file_name)
        config_file_name   = str.encode(config_file_name)
        out_sol_file_name  = str.encode(out_sol_file_name)
        physics_file_name  = str.encode(physics_file_name)
        boundary_file_name = str.encode(boundary_file_name)

      # Input File Names (e.g. boundary, physics, etc.
      orig_physics_file_path = os.path.join(py_path, physics_file_name) # this is the original bounary file; will be ignored
      physics_file_path = os.path.join(output_path, physics_file_name)
      orig_boundary_file_path = os.path.join(py_path, boundary_file_name)  # this is the original bounary file; will be ignored
      boundary_file_path = os.path.join(output_path, boundary_file_name)
      #
      self._paths_dict = {'config_file': config_file_name,
                          'init_sol_file': init_sol_file_name,
                          'physics_file': physics_file_name,
                          'boundary_file':boundary_file_name,
                          'out_sol_file': out_sol_file_name,
                          'dl_root_path': DL_ROOT_PATH,
                          'hypar_root_path': HYPAR_ROOT_PATH,
                          'model_path': model_path,
                          'model_run_cmd':model_run_cmd
                          }
      # print("self._paths_dict: ")
      # for key in self._paths_dict:
      #   print(key, self._paths_dict[key])

      # TODO: Add step to backup existing files!
      pass

      # write/copy physics file
      physics_settings = model_configs['physics']
      str_out = "begin\n"
      for key in physics_settings:
        val = physics_settings[key]
        if np.isscalar(val):
          val = str(val)
        elif self.isiterable(val):
          val = "  ".join([str(v) for v in val])
        line = "  %s\t%s\n" % (key, val)
        str_out += line
      str_out += "end"
      # print("Here is what should be written to physics.inp:")
      # print(str_out)
      # write setting to the target file
      with open(physics_file_path, 'w') as f_id:
        f_id.write(str_out)

      # Aggregate, validate, and write input configurations
      # this verwrites existing model configurations (self._model_configs['config_file']='solver.inp')
      self._model_configs = self.write_model_configs(model_configs, validate=True)
      model_configs = self._model_configs
      # TODO: do the same for physics.inp, and boundary.inp

      # Write boundary.inp file; 
      boundary_settings = model_configs['boundary']
      print("*** Creating boundary stuff ***")
      # Create a boundary file from settings in model_configs
      num_faces = boundary_settings['num_faces']
      faces_types = boundary_settings['faces_types']
      faces_settings = boundary_settings['faces_settings']
      #
      thermal_ids = []
      if num_faces != 2*self._NDIMS:
        print("The number of faces is improperly set to %s; This should be 2 x NDIM, i.e. 6 here, instead!" % num_faces)
        raise ValueError
      else:
        print("Attempting to retrieve bounds")
        grid_l_bounds = model_configs['domain_lower_bounds']
        grid_u_bounds = model_configs['domain_upper_bounds']
        # get the settings of each face, and create a proper string
        str_out = "%d\n" % num_faces
        face_side = 1
        for face_id, face_settings in enumerate(zip(faces_types, faces_settings)):
          face_type = face_settings[0].lower().strip()
          if 'thermal' in face_type:
            thermal_ids.append(face_id)
          face_conf = face_settings[1]
          if (face_id // 2) == 0:  # X-dim
            bounds_string = "%f\t%f\t%f\t%f\t%f\t%f" % (0, 0, grid_l_bounds[1], grid_u_bounds[1], grid_l_bounds[2], grid_u_bounds[2])
          elif (face_id // 2) == 1:
            bounds_string = "%f\t%f\t%f\t%f\t%f\t%f" % (grid_l_bounds[0], grid_u_bounds[0], 0, 0, grid_l_bounds[2], grid_u_bounds[2])
          elif (face_id // 2) == 2:
            bounds_string = "%f\t%f\t%f\t%f\t%f\t%f" % (grid_l_bounds[0], grid_u_bounds[0], grid_l_bounds[1], grid_u_bounds[1], 0, 0)
          else:
            print("How is that even possible?")
            raise ValueError
          line = "%s\t%d\t%d\t%s\n" % (face_type, face_id//2, face_side, bounds_string)
          if face_conf is not None:
            line += "%s\n"   % ('\t'.join([str(v) for v in face_conf]) )
          
          face_side = - face_side
          str_out += line
        # print("Here is what should be written to boundary.inp:")
        # print(str_out)
        # write setting to the target file
        with open(boundary_file_path, 'w') as f_id:
          f_id.write(str_out)


      gamma = <double> physics_settings['gamma']
      R = <double> physics_settings['R']
      rho_ref = <double> physics_settings['rho_ref']
      p_ref = <double> physics_settings['p_ref']
      HB = <int> physics_settings['HB']
      grav_x = <double> physics_settings['gravity'][0]
      grav_y = <double> physics_settings['gravity'][1]
      grav_z = <double> physics_settings['gravity'][2]
      Lx_l = <double> grid_l_bounds[0]
      Lx_u = <double> grid_u_bounds[0]
      Ly_l = <double> grid_l_bounds[1]
      Ly_u = <double> grid_u_bounds[1]
      Lz_l = <double> grid_l_bounds[2]
      Lz_u = <double> grid_u_bounds[2]

      self._create_initial_condition(init_sol_file_name, self._NDIMS, self._NVARS, Lx_l, Ly_l, Lz_l, Lx_u, Ly_u, Lz_u, self._NI, self._NJ, self._NK, gamma, R, rho_ref, p_ref, grav_x, grav_y, grav_z, HB, "binary")
      print("Iinitial Solution CREATED...")

      #
      # Allocate memory for model grid and model state
      self._X = <double *>malloc(self._NI * sizeof(double))
      if not self._X:
        raise MemoryError("Failed to allocate memory for self._X; the x-direction of model grid")
      self._Y = <double *>malloc(self._NJ * sizeof(double))
      if not self._Y:
        raise MemoryError("Failed to allocate memory for self._Y; the y-direction of model grid")
      self._Z = <double *>malloc(self._NK * sizeof(double))
      if not self._Z:
        raise MemoryError("Failed to allocate memory for self._Z; the z-direction of model grid")

      self.MODEL_STATE = self.state_vector()
      self.MODEL_STATE.time = model_configs['init_time']
      # self.MODEL_STATE = <double *>malloc(self._STATE_SIZE * sizeof(double))
      # if not self.MODEL_STATE:
      #   raise MemoryError("Failed to allocate memory for self.MODEL_STATE; the model state vector")

      filename = self._paths_dict['init_sol_file']
      self._read_binary_initial_condition(filename, self._X, self._Y, self._Z, self.MODEL_STATE.data)
      print("Iinitial Solution Loaded...")
      
      if False:
        self._write_binary_initial_condition('test_initial_condition.inp', self._X, self._Y, self._Z, self.MODEL_STATE.data)
        print("Copy of initial solution created in:\n\t %s " % os.path.join(os.getcwd(), 'test_initial_condition.inp'))

        
      # Generate Unsteady Temperature File:
      print("*** Creating Thermal stuff ***")
      thermal_source_settings = model_configs['thermal_source']
      if thermal_source_settings is None:
        if len(thermal_ids):  # found_thermal
          # TODO: Rewrite based on face_conf, and thermal_ids
          shape='disc'
          xcent = grid_u_bounds[0]/2.0  # 500
          ycent = grid_u_bounds[1]/2.0  #500
          zcent = 0
          rcent = (grid_u_bounds[2] - grid_l_bounds[2])/10
          T_diff = 10
          print("Based on the boundary settings, there is a thermal source, yet no settings are passed!")
          print("Using default source:")
          print("shape: disc")
          print("xcent: %f" % xcent)
          print("ycent: %f" % ycent)
          print("zcent: %f" % zcent)
          print("rcent: %f" % rcent)
          shape_specs = {'shape':shape, 'xcent':xcent,'ycent':ycent, 'zcent':zcent, 'rcent':rcent}
        else:
          pass
      else:
        # read thermal settings from configureations dict:
        shape_specs = thermal_source_settings
        shape = thermal_source_settings['shape']
        T_diff = thermal_source_settings['T_diff']
        # print("Passed thermal settings used")
        # print(shape_specs)
      #
      if len(thermal_ids):
        # self.generate_temperature(2, 1, shape_specs={'r':200, 'x':xcent, 'y':ycent, 'z':zcent})  # TODO: the settings are to be ported to the config dictionary
        self.generate_temperature(T_diff=T_diff, shape=shape, shape_specs=shape_specs)
        #
      #
      self._INITIALIZED = 1
      #

  def show_model_configs(self):
    """
    """
    print("\n\n%s\n\tCurrent HyPar Model Configurations \n%s \n" % (self.__SEPP__, self.__SEPP__))
    for k in self._model_configs:
      print("\t%-18s  %s" % (k, str(self._model_configs)))
    print("%s\n" % self.__SEPP__)
    #

  def copy_model_configs(self):
    """
    return a copy of the model configurations dictionary
    """
    configs = copy.deepcopy(self._model_configs)
    path_configs = copy.deepcopy(self._paths_dict)
    configs.update({'model_paths': path_configs})
    return configs
  # Aliasing
  get_model_configs = model_configs = copy_model_configs

  cpdef write_model_configs(self, model_configs=None, validate=False):
    """
    Aggregate, validate, and write model configurations against default configurations returned by self._default_model_configs()
    """
    if model_configs is not None:
      assert isinstance(model_configs, dict), "** 'model_configs' must be either None or a Python dictionary **"
    else:
      model_configs = self.model_configs()
    # All the follwoing can be replaced with IO here, but I am playing with Cython/C :)
    if validate:
      model_configs = self.validate_model_configs(model_configs)

    cdef int ndims = <int> model_configs['ndims']
    cdef int nvars = <int> model_configs['nvars']
    cdef int ghost = <int> model_configs['ghost']
    cdef long n_iter = <long> model_configs['n_iter']
    cdef int n_io = <int> model_configs['n_io']
    cdef int rest_iter = <int> model_configs['rest_iter']
    cdef int screen_op_iter = <int> model_configs['screen_op_iter']
    cdef int file_op_iter = <int> model_configs['file_op_iter']

    cdef double dt = <double> model_configs['dt']

    cdef char* filename
    cdef char* ts
    cdef char* ts_type
    cdef char* hyp_scheme
    cdef char* hyp_flux_split
    cdef char* hyp_int_type
    cdef char* par_type
    cdef char* par_scheme
    cdef char* cons_check
    cdef char* op_format
    cdef char* ip_type
    cdef char* input_mode
    cdef char* output_mode
    cdef char* op_overwrite
    cdef char* model

    filename   = cython.cast('char*', self._paths_dict['config_file'])
    ts             = cython.cast('char*', model_configs['ts'])
    ts_type        = cython.cast('char*', model_configs['ts_type'])
    hyp_scheme     = cython.cast('char*', model_configs['hyp_scheme'])
    hyp_flux_split = cython.cast('char*', model_configs['hyp_flux_split'])
    hyp_int_type   = cython.cast('char*', model_configs['hyp_int_type'])
    par_type       = cython.cast('char*', model_configs['par_type'])
    par_scheme     = cython.cast('char*', model_configs['par_scheme'])
    cons_check     = cython.cast('char*', model_configs['cons_check'])
    op_format      = cython.cast('char*', model_configs['op_format'])
    ip_type        = cython.cast('char*', model_configs['ip_type'])
    input_mode     = cython.cast('char*', model_configs['input_mode'])
    output_mode    = cython.cast('char*', model_configs['output_mode'])
    op_overwrite   = cython.cast('char*', model_configs['op_overwrite'])
    model          = cython.cast('char*', model_configs['model'])


    cdef int* _size = <int*> malloc(ndims * sizeof(int))
    cdef int* _iproc = <int*> malloc(ndims * sizeof(int))
    cdef double* _domain_lower_bounds = <double*> malloc(ndims * sizeof(double))
    cdef double* _domain_upper_bounds = <double*> malloc(ndims * sizeof(double))

    l_sizes = [int(s) for s in list(model_configs['size'])]
    l_iproc = [int(p) for p in list(model_configs['iproc'])]
    l_domain_lower_bounds = [np.float64(p) for p in list(model_configs['domain_lower_bounds'])]
    l_domain_upper_bounds = [np.float64(p) for p in list(model_configs['domain_upper_bounds'])]

    cdef int i
    for i in range(ndims):
      _size[i] = <int> l_sizes[i]
      _iproc[i] = <int> l_iproc[i]
      _domain_lower_bounds[i] = l_domain_lower_bounds[i]
      _domain_upper_bounds[i] = l_domain_upper_bounds[i]


    # Navigate to model dir, and write settings file
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    out = _hypar_sol_handler.write_model_configs(filename, ndims, nvars, _size,
                                                 _iproc, ghost, _domain_lower_bounds, _domain_upper_bounds, n_iter, rest_iter,
                                                 ts, ts_type, hyp_scheme, hyp_flux_split,
                                                 hyp_int_type, par_type, par_scheme, dt,
                                                 cons_check, screen_op_iter, file_op_iter,
                                                 op_format, ip_type, input_mode, output_mode,
                                                 n_io, op_overwrite, model
                                                 )
    if out:
      print("Failed to write the model configurations!")
      raise IOError

    os.chdir(cwd)

    # free memory allocated here
    free(_size)
    free(_iproc)

    # Attach model_configs to self.
    return model_configs
    #

  def aggregate_configurations(self, configs=None, def_configs=None, copy_configs=False):
    """
    Blindly (and recursively) combine the two dictionaries. One-way copying: from def_configs to configs only.
    Add default configurations to the passed configs dictionary

    Args:
      configs:
      def_configs:
      copy_configs: if True, a new copy is returned, otherwise configs is updated

    Returns:
      aggr_configs: aggregate_configurations

    """
    if configs is not None:
      assert isinstance(configs, dict), "'configs' must be a dictionary!"
    if def_configs is not None:
      assert isinstance(def_configs, dict), "'def_configs' must be a dictionary!"

    if configs is None and def_configs is None:
      print("\n** Both inputs: (configs, def_configs) are None!\n\n")
      raise ValueError

    elif configs is None:
      aggr_configs = dict.copy(def_configs)

    elif def_configs is None:
      if copy_configs:
        aggr_configs = dict.copy(def_configs)
      else:
        aggr_configs = configs
    else:
      if copy_configs:
        aggr_configs = dict.copy(def_configs)
      else:
        aggr_configs = configs
      #
      for key in def_configs:
        if key not in aggr_configs:
          aggr_configs.update({key: def_configs[key]})
        elif aggr_configs[key] is None:
            aggr_configs[key] = def_configs[key]
        elif isinstance(aggr_configs[key], dict) and isinstance(def_configs[key], dict):
          # recursively aggregate the dictionary-valued keys
          sub_configs = self.aggregate_configurations(aggr_configs[key], def_configs[key])
          aggr_configs.update({key: sub_configs})
    #
    return aggr_configs

  @staticmethod
  def isiterable(x):
    try:
      iter(x)
      out = True
    except:
      out = False
    return out

  def validate_model_configs(self, model_configs=None, bytes_to_string=False):
    """
    Aggregate, and validate model configurations against default configurations returned by self._default_model_configs()
    """
    def_configs = self._default_model_configs()
    model_configs = self.aggregate_configurations(configs=model_configs, def_configs=def_configs, copy_configs=False)
    # TODO: Add sanity checks on the validity of arguments in the aggregated model configurations dict (model_configs)
    #
    # TODO: When checking bytes/string; we need to recursively call this function!
    if False:  # TODO: remove after debugging
      print("Validating mdoel configurations: ")
      print("\nPassed configurations: ")
      print(model_configs)
      print("\n\n Default configurations: ")
      print(def_configs)


    try:
      PETSc_options = model_configs['PETSc_options']
      if isinstance(PETSc_options, bytes):
        PETSc_options = PETSc_options.decode()
      PETSc_options = PETSc_options.strip(" ")
      PETSc_options = PETSc_options.split("-use-petscts")
      opts = PETSc_options[-1].strip(" ")
      if len(opts) > 0:
        PETSc_options = " -use-petscts %s " % opts
      else:
        PETSc_options = " "
    except KeyError:
      PETSc_options = " "
    if python_version >= (3, 0, 0) and isinstance(PETSc_options, str):
      PETSc_options = str.encode(PETSc_options)
    model_configs.update({'PETSc_options':PETSc_options})
    # Add more:
    # e.g. 1- assert isinstance(model_configs, str)
    #      2- remove whitespaces from strings,
    #      3- etc.
    for key in model_configs:
      value = model_configs[key]
      if isinstance(value, (bytes, str)):
        if python_version >= (3, 0, 0) and isinstance(value, str):
          if bytes_to_string:
            if isinstance(value, bytes):
              value = bytes.decode(value)
          else:
            value = str.encode(value)
          model_configs.update({key:value})
    #
    if False:  # TODO: remove after debugging
      print("Validated_configurations: ")
      print(model_configs)
      print("\n\n\n************VALIDATED**********\n\n\n")
    return model_configs


  def _default_model_configs(self):
    """
    return a dictionary with default model settings
    """
    settings = dict(ndims=3,
                    nvars=5,
                    size=[101,101,41],
                    iproc=[4,4,2],
                    ghost=3,
                    n_iter=50,
                    rest_iter=0,
                    ts='rk',
                    ts_type='ssprk3',
                    hyp_scheme='weno5',
                    hyp_flux_split='no',
                    hyp_int_type='components',
                    par_type='none',
                    par_scheme='none',
                    dt=0.25,
                    time_unit='s',
                    init_time=0.0,
                    cons_check='no',
                    domain_lower_bounds=[0, 0, 0],
                    domain_upper_bounds=[50000.0, 50000.0, 4000.0],
                    PETSc_options="",
                    screen_op_iter=1,
                    file_op_iter=20000,
                    op_format='binary',
                    ip_type='binary',
                    input_mode='serial',
                    output_mode='serial',
                    n_io=1,
                    op_overwrite='yes',
                    model='navierstokes3d',
                    cleanup_dir=True,
                    thermal_source=None,
                    boundary=dict(num_faces=6,  # X-low, X-high, Y-low, Y-high, Z-low, Z-high
                                  faces_types=['periodic', 'periodic', 'periodic', 'periodic', 'noslip-wall', 'slip-wall'],
                                  faces_settings=[None, None, None, None, (0.0, 0.0, 0.0), (0.0, 0.0, 0.0)]  # For (no)slip, (constant wall velocity) are given
                                 ),
                    physics=dict(gamma=1.4,
                                 upwinding="rusanov",
                                 gravity=[0.0, 0.0, 9.8],
                                 rho_ref=1.1612055171196529,
                                 p_ref=100000.0,
                                 R=287.058,
                                 HB=2,
                                ),
                  )
    return settings


  cdef int _get_model_params(self) except -1:

    cdef int* iproc
    cdef double* domain_lower_bounds
    cdef double* domain_upper_bounds
    cdef int* sizes
    cdef int nvars, ndims, rest_iter
    cdef long n_iter
    cdef char* filename
    cdef double dt
    filename = self._paths_dict['config_file']
    _hypar_sol_handler.read_model_configs(filename, &nvars, &ndims, &sizes, &iproc, &domain_lower_bounds, &domain_upper_bounds, &n_iter, &dt, &rest_iter)

    self._NVARS = nvars
    # self._DT = dt
    self._RESTART_ITER = rest_iter
    self._N_ITER = n_iter

    self._NDIMS = ndims
    self._NI = sizes[0]
    self._NJ = sizes[1]
    self._NK = sizes[2]

    self._NUM_PROC = 1
    for i in range(self._NDIMS):
      self._NUM_PROC *= iproc[i]
    self._STATE_SIZE = self._NI * self._NJ * self._NK * self._NVARS
    #
    try:
      if iproc != NULL:
        free(iproc)
    except:
      pass
    try:
      if domain_lower_bounds != NULL:
        free(domain_lower_bounds)
    except:
      pass
    try:
      if domain_upper_bounds != NULL:
        free(domain_upper_bounds)
    except:
      pass
    try:
      if sizes != NULL:
        free(sizes)
    except:
      pass

  cdef int _create_initial_condition(self, char *init_sol_file, int ndims, int nvars, double Lx_l, double Ly_l, double Lz_l, double Lx_u, double Ly_u, double Lz_u, int NI, int NJ, int NK, double gamma, double R, double rho_ref, double p_ref, double grav_x,
                                     double grav_y, double grav_z, int HB, char *ip_file_fomat):
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    out = _hypar_sol_handler.create_initial_condition(init_sol_file, ndims, nvars, Lx_l, Ly_l, Lz_l, Lx_u, Ly_u, Lz_u, NI, NJ, NK, gamma, R, rho_ref, p_ref, grav_x, grav_y, grav_z, HB, ip_file_fomat)
    os.chdir(cwd)
    return out


  cdef int _read_binary_solution(self, char* filename, double* x, double* y, double* z, double* U) except -1:
    cdef char* model_path
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    cdef int out, ni, nj, nk, ndims
    out = _hypar_sol_handler.read_binary_solution(filename, &ni, &nj, &nk, &ndims, x, y, z, U)
    os.chdir(cwd)
    if out != 0:
      print("Sorry; Failed to read the solution file %s !\n", filename)
      raise IOError()
    if ni != self._NI:
      print("NI retrieved from solution file is not correct!; Expected %d, Received %d\n", ni, self._NI)
    if nj != self._NJ:
      print("NJ retrieved from solution file is not correct!; Expected %d, Received %d\n", nj, self._NJ)
    if nk != self._NK:
      print("NK retrieved from solution file is not correct!; Expected %d, Received %d\n", nk, self._NK)
    if ndims != self._NDIMS:
      print("NDIMS retrieved from solution file is not correct!; Expected %d, Received %d\n", ndims, self._NDIMS)
    return out


  cdef int _read_binary_initial_condition(self, char* filename, double* x, double* y, double* z, double* U) except -1:
    cdef char* model_path
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    out = _hypar_sol_handler.read_initial_condition(filename, self._NI, self._NJ, self._NK, self._NDIMS, x, y, z, U)
    os.chdir(cwd)
    if out != 0:
      print("Sorry; Failed to read the solution file %s !\n" %  filename)
      raise IOError()
    return out


  cdef int _write_binary_array(self, char* filename, double* x, double* y, double* z, double* U) except -1:
    cdef char* model_path
    # model_path = self._paths_dict['model_path']
    # cwd = os.getcwd()
    # os.chdir(model_path)
    out = _hypar_sol_handler.write_binary_array(filename, self._NI, self._NJ, self._NK, self._NDIMS, self._NVARS, x, y, z, U)
    # os.chdir(cwd)
    if out != 0:
      print("Sorry; Failed to read the solution file %s !\n", filename)
      raise IOError()
    return out


  cdef int _write_binary_initial_condition(self, char* filename, double* x, double* y, double* z, double* U) except -1:
    cdef char* model_path
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    out = _hypar_sol_handler.write_initial_condition(filename, self._NI, self._NJ, self._NK, self._NDIMS, x, y, z, U)
    os.chdir(cwd)
    if out != 0:
      print("Sorry; Failed to read the solution file %s !\n", filename)
      raise IOError()
    return out

  cpdef int _update_state_from_sol_file(self, char* fname='__none__'):
    cdef char* filename
    if fname.lower() == '__none__':
      filename = self._paths_dict['init_sol_file']
    else:
      filename = fname

    self._read_binary_solution(filename, self._X, self._Y, self._Z, self.MODEL_STATE.data)

  cpdef update_state_from_vec(self, vec):
    cdef int i
    for i in range(self._STATE_SIZE):
      self.MODEL_STATE[i] = vec[i]

  cdef int _read_binary_sol_to_numpy(self, char* filename, np.ndarray state) except -1:
    cdef long i
    cdef double* x = <double *>malloc(self._NI * sizeof(double))
    cdef double* y = <double *>malloc(self._NJ * sizeof(double))
    cdef double* z = <double *>malloc(self._NK * sizeof(double))
    cdef double* U = <double *>malloc(self._STATE_SIZE * sizeof(double))
    #
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)

    self._read_binary_solution(filename, x, y, z, U)
    for i in range(self._STATE_SIZE):
      state[i] = U[i]
    #
    free(x)
    free(y)
    free(z)
    free(U)
    #
    os.chdir(cwd)

  cpdef read_hypar_initial_condition(self, file_name):
    """
    """
    X = <double *>malloc(self._NI * sizeof(double))
    Y = <double *>malloc(self._NJ * sizeof(double))
    Z = <double *>malloc(self._NK * sizeof(double))
    U = <double *>malloc(self._STATE_SIZE * sizeof(double))
    _hypar_sol_handler.read_initial_condition(file_name, self._NI, self._NJ, self._NK, self._NDIMS, X, Y, Z, U)
    state = self.state_vector()
    cdef int i
    for i in range(self._STATE_SIZE):
      state[i] = U[i]
    free(X)
    free(Y)
    free(Z)
    free(U)
    return state

  def read_hypar_binary_state_from_file(self, file_name, state=None):
    if state is None:
      state = np.zeros(self._STATE_SIZE, order='c', dtype=DTYPE_DOUBLE)
    else:
      if state.size != self._STATE_SIZE:
        print("State Size doesn't match model state size")
        raise ValueError
      else:
        pass
    #
    self._read_binary_sol_to_numpy(file_name, state)
    return state

  def read_solution_to_numpy(self, state=None):
    if state is None:
      state = np.zeros(self._STATE_SIZE, order='c', dtype=DTYPE_DOUBLE)
    else:
      if state.size != self._STATE_SIZE:
        print("State Size doesn't match model state size")
        raise ValueError
      else:
        pass
    #
    filename = self._paths_dict['init_sol_file']
    self._read_binary_sol_to_numpy(filename, state)
    return state

  cdef int _state_to_numpy(self, np.ndarray state) except -1:
    cdef int i
    for i in range(self._STATE_SIZE):
      state[i] = self.MODEL_STATE[i]

  def current_state_to_numpy(self, state=None):
    if state is None:
      state = np.zeros(self._STATE_SIZE, order='c', dtype=DTYPE_DOUBLE)
    else:
      if state.size != self._STATE_SIZE:
        print("State Size doesn't match model state size")
        raise ValueError
    self._state_to_numpy(state)
    return state

  cdef int _model_grid_to_numpy(self, np.ndarray x, np.ndarray y, np.ndarray z) except -1:
    cdef int i
    for i in range(self._NI):
      x[i] = self._X[i]
    for i in range(self._NJ):
      y[i] = self._Y[i]
    for i in range(self._NK):
      z[i] = self._Z[i]


  cdef int _current_grid_to_numpy(self, np.ndarray x, np.ndarray y, np.ndarray z) except -1:
    cdef int cntr = 0, out = 0
    cdef int i, j, k

    for i in range(self._NI):
      for j in range(self._NJ):
        for k in range(self._NK):

          # get coordinates:
          x[cntr] = self._X[i]
          y[cntr] = self._Y[j]
          z[cntr] = self._Z[k]
          cntr += 1
    return out


  cpdef gridpoint_to_index(self, double x, double y, double z, double tol=1):  # tol in meters
    """
    get the first index (out of 5) that holds information about the passed grid point
      tol is used for comparison betwwen points (using L2 norm of distance)
      -1 is returned if grid point is not in the model
    """
    # TODO: This could be more efficient by binary search
    cdef int p, cntr = 0, out = -1
    cdef int i, j, k
    cdef double gridpoint[3]
    cdef bint found = 0
    cdef double errnorm = 10000

    for i in range(self._NI):
      if abs(x-self._X[i]) > tol:
        continue
      for j in range(self._NJ):
        if abs(y-self._Y[j]) > tol:
          continue
        for k in range(self._NK):

          p = i + self._NI*j + self._NI*self._NJ*k;

          # get coordinates:
          gridpoint[0] = self._X[i]
          gridpoint[1] = self._Y[j]
          gridpoint[2] = self._Z[k]

          errnorm = sqrt( (gridpoint[0]-x)**2 + (gridpoint[1]-y)**2 + (gridpoint[2]-z)**2 )
          if errnorm <= tol:
            found = 1
            out = 5 * p
            # print("PINGOOOO", (x,y,z), "With", (gridpoint[0], gridpoint[1], gridpoint[2]), "Error: %f" %errnorm)
          else:
            pass
            # print("Couldn't be found!", (x,y,z), "With", (gridpoint[0], gridpoint[1], gridpoint[2]), "Error: %f" %errnorm)

          # print("cntr %d, relerr = %f " % (cntr, errnorm))
          cntr += 1

          if found:
            break
        if found:
          break
      if found:
        break
    # print("returning %d, relerr = %f " % (out, errnorm))
    return out
  
  def state_from_numpy(self, x, y, z, rho, u, v, w, E, t=0, validate_entries=False):
    """
    Wrapper around _state_from_numpy
    """
    if x.shape == y.shape == z.shape == u.shape == v.shape == w.shape == rho.shape == E.shape:
        pass
    else:
        print("Input shapes mismatch")
        print("X.shape: ", x.shape)
        print("Y.shape: ", y.shape)
        print("Z.shape: ", z.shape)
        print("Rho.shape: ", rho.shape)
        print("U.shape: ", u.shape)
        print("V.shape: ", v.shape)
        print("W.shape: ", w.shape)
        print("E.shape: ", E.shape)
        raise AssertionError

    assert u.ndim == 1, "input data must  be all 1d arrays"

    state = self._state_from_numpy(x=x, y=y, z=z, rho=rho, u=u, v=v, w=w, E=E, t=t, validate_entries=validate_entries)
    return state


  cdef _state_from_numpy(self, np.ndarray x, np.ndarray y, np.ndarray z, np.ndarray rho, np.ndarray u, np.ndarray v, np.ndarray w, np.ndarray E, int t=0, validate_entries=False):
    """
    Use the input field  data to create a model state (DlidarVec)
    x, y, z are the gridpoints, rho, u, v, w, E are the values given to be used in updating the state
    """
    
    cdef long int i
    cdef long int s

    state = self.state_vector(t=t)
    inds = [self.gridpoint_to_index(a, b, c) for a, b, c in zip(x, y, z)]
    
    if validate_entries:
      for i, s in enumerate(inds):
        rho_i      = rho[i]
        state[s]   = rho_i
        state[s+1] = u[i] * rho_i
        state[s+2] = v[i] * rho_i
        state[s+3] = w[i] * rho_i
        state[s+4] = E[i]
    else:
      for i, s in enumerate(inds):
        
        values = (rho[i], u[i], v[i], w[i], E[i])
        if np.any(np.isnan(values)):
            print("Nan values found at coordiantes: %s" % repr((x, y, z)))
            raise ValueError
        else:
            state[s]   = values[0]
            state[s+1] = values[1] * values[0]
            state[s+2] = values[2] * values[0]
            state[s+3] = values[3] * values[0]
            state[s+4] = values[4]

      return state

  def get_model_grid(self, x=None, y=None, z=None):
    dim = self._NI * self._NJ * self._NK
    #
    if x is None:
      x = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if x.size != dim:
        print("Grid: X-direction Size doesn't match model X-Grid size")
        raise ValueError
    if y is None:
      y = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if y.size != dim:
        print("Grid: Y-direction Size doesn't match model Y-Grid size")
        raise ValueError
    if z is None:
      z = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if z.size != dim:
        print("Grid: Z-direction Size doesn't match model Z-Grid size")
        raise ValueError

    #
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    self._current_grid_to_numpy(x, y, z)
    os.chdir(cwd)
    grid = np.concatenate((x,y,z)).reshape(3, dim).T
    return grid
  # alias
  model_grid=get_local_grid=get_model_grid



  cdef int _current_gridded_solution_to_numpy(self, np.ndarray x, np.ndarray y, np.ndarray z, np.ndarray rho, np.ndarray u, np.ndarray v, np.ndarray w, np.ndarray E) except -1:

    cdef int cntr = 0, out = 0
    cdef int p, s
    cdef int i, j, k
    cdef double _rho_val

    for i in range(self._NI):
      for j in range(self._NJ):
        for k in range(self._NK):

          # get coordinates:
          x[cntr] = self._X[i]
          y[cntr] = self._Y[j]
          z[cntr] = self._Z[k]

          p = i + self._NI*j + self._NI*self._NJ*k
          s = self._NVARS*p
          rho_val = self.MODEL_STATE[s]
          rho[cntr] = rho_val
          u[cntr] = self.MODEL_STATE[s+1] /rho
          v[cntr] = self.MODEL_STATE[s+2] /rho
          w[cntr] = self.MODEL_STATE[s+3] /rho
          E[cntr] = self.MODEL_STATE[s+4]

          cntr += 1

    return out


  def get_current_state(self):
    state = self.MODEL_STATE.copy()
    return state

  cpdef set_current_state(self, state):
    """
    """
    assert(len(state)==self._STATE_SIZE), "passed state must be an iterable of length == %d" %self._STATE_SIZE
    self.MODEL_STATE[:] = state[:]


  def gridded_solution_to_numpy(self, x=None, y=None, z=None, rho=None, u=None, v=None, w=None, E=None):
    dim = self._NI * self._NJ * self._NK
    #
    if x is None:
      x = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if x.size != dim:
        print("Grid: X-direction Size doesn't match model X-Grid size")
        raise ValueError
    if y is None:
      y = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if y.size != dim:
        print("Grid: Y-direction Size doesn't match model Y-Grid size")
        raise ValueError
    if z is None:
      z = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if z.size != dim:
        print("Grid: Z-direction Size doesn't match model Z-Grid size")
        raise ValueError
    #
    if rho is None:
      rho = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if rho.size != dim:
        print("RHO Size doesn't match model W-component size")
        raise ValueError
    if u is None:
      u = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if u.size != dim:
        print("Velocity: U-Component Size doesn't match model U-component size")
        raise ValueError
    if v is None:
      v = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if v.size != dim:
        print("Velocity: V-Component Size doesn't match model V-component size")
        raise ValueError
    if w is None:
      w = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if w.size != dim:
        print("Velocity: W-Component Size doesn't match model W-component size")
        raise ValueError
    if E is None:
      E = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if E.size != dim:
        print("E Size doesn't match model W-component size")
        raise ValueError
    #
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    self._current_gridded_solution_to_numpy(x, y, z, rho, u, v, w, E)
    os.chdir(cwd)
    return (x, y, z, rho, u, v, w, E)


  cdef int _current_velocity_to_numpy(self, np.ndarray u, np.ndarray v, np.ndarray w) except -1:
    cdef int i, j, k, p
    cdef int index = 0
    cdef double rho
    # cdef int dim = self._STATE_SIZE / self._NVARS
    for i in range(self._NI):
      for j in range(self._NJ):
        for k in range(self._NK):
          p = i + self._NI*j + self._NI*self._NJ*k
          rho = self.MODEL_STATE[5*p+0]
          u[index] = self.MODEL_STATE[5*p+1] / rho
          v[index] = self.MODEL_STATE[5*p+2] / rho
          w[index] = self.MODEL_STATE[5*p+3] / rho
          index += 1


  def velocity_field_to_numpy(self, u=None, v=None, w=None):
    dim = self._STATE_SIZE / self._NVARS
    if u is None:
      u = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if u.size != dim:
        print("Velocity: U-Component Size doesn't match model U-component size")
        raise ValueError
    if v is None:
      v = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if v.size != dim:
        print("Velocity: V-Component Size doesn't match model V-component size")
        raise ValueError
    if w is None:
      w = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if w.size != dim:
        print("Velocity: W-Component Size doesn't match model W-component size")
        raise ValueError
    #
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    self._current_velocity_to_numpy(u, v, w)
    os.chdir(cwd)
    return (u, v, w)


  cdef int _state_gridded_velocity_to_numpy(self, StateVector state, np.ndarray x, np.ndarray y, np.ndarray z, np.ndarray u, np.ndarray v, np.ndarray w, mask_size) except -1:
    """
    Extract, and return the wind velocity components of the current model state
    """
    cdef double rho
    cdef int out = -1
    # cdef double E

    if mask_size is not None:
      assert isinstance(mask_size, int), "mask_size must be either non or an integer!"
      if 1 < mask_size < min(self._NI, min(self._NJ, self._NK)):
        xsize = len(range(0, self._NI, mask_size))
        ysize = len(range(0, self._NJ, mask_size))
        zsize = len(range(0, self._NK, mask_size))
        dim = xsize * ysize * zsize
        m_s = mask_size
      else:
        dim = self._NI * self._NJ * self._NK
        m_s = 1
    else:
      dim = self._NI * self._NJ * self._NK
      m_s = 1

    cdef int cntr = 0
    cdef int p, s
    cdef int i, j, k

    for i in range(0, self._NI, m_s):
      for j in range(0, self._NJ, m_s):
        for k in range(0, self._NK, m_s):


          # get coordinates:
          x[cntr] = self._X[i]
          y[cntr] = self._Y[j]
          z[cntr] = self._Z[k]

          p = i + self._NI*j + self._NI*self._NJ*k
          s = self._NVARS*p

          rho = state[s]
          u[cntr] = state[s+1] / rho
          v[cntr] = state[s+2] / rho
          w[cntr] = state[s+3] / rho
          # E = self.MODEL_STATE[s+4]

          cntr += 1
    out = 0
    return out


  cdef _current_premitive_atmos_flow_variables_to_numpy(self, x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0, m_s):
    """
    """
    out = self._premitive_atmos_flow_variables_to_numpy(self.MODEL_STATE, x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0, m_s)
    return out


  cdef int _premitive_atmos_flow_variables_to_numpy(self, StateVector state, np.ndarray x, np.ndarray y, np.ndarray z, 
                                                    np.ndarray rho, np.ndarray u, np.ndarray v, np.ndarray w, 
                                                    np.ndarray P, np.ndarray theta, np.ndarray rho0, np.ndarray P0, 
                                                    np.ndarray Pexner, np.ndarray theta0, mask_size) except -1:
    """
    Extract, and return the wind velocity components of the current model state
    """
    cdef double grav_z = self._model_configs['physics']['gravity'][-1]
    cdef double R = self._model_configs['physics']['R']
    cdef double gamma = self._model_configs['physics']['gamma']
    cdef double P_ref = self._model_configs['physics']['p_ref']
    cdef double rho_ref = self._model_configs['physics']['rho_ref']
    cdef double T_ref = P_ref / ( R * rho_ref )
    cdef double inv_gamma_m1 = 1.0 / ( gamma - 1.0 )
    cdef double Cp = gamma * inv_gamma_m1 * R
    #

    cdef int p, s
    cdef int i, j, k

    cdef int out = -1

    if mask_size is not None:
      assert isinstance(mask_size, int), "mask_size must be either non or an integer!"
      if 1 < mask_size < min(self._NI, min(self._NJ, self._NK)):
        xsize = len(range(0, self._NI, mask_size))
        ysize = len(range(0, self._NJ, mask_size))
        zsize = len(range(0, self._NK, mask_size))
        dim = xsize * ysize * zsize
        m_s = mask_size
      else:
        dim = self._NI * self._NJ * self._NK
        m_s = 1
    else:
      dim = self._NI * self._NJ * self._NK
      m_s = 1

    cdef int cntr = 0
    cdef double Evel, Pvel, thetavel, rhovel, rho0vel, theta0vel, Pexnervel, P0vel
    cdef double uvel, vvel, wvel

    for i in range(0, self._NI, m_s):
      for j in range(0, self._NJ, m_s):
        for k in range(0, self._NK, m_s):

          # get coordinates:
          x[cntr] = self._X[i]
          y[cntr] = self._Y[j]
          z[cntr] = self._Z[k]

          # indexes:
          p = i + self._NI*j + self._NI*self._NJ*k
          s = self._NVARS*p

          # Extract values from passed state:
          rhovel   = state[s]
          uvel     = state[s+1] / rhovel
          vvel     = state[s+2] / rhovel
          wvel     = state[s+3] / rhovel
          Evel     = state[s+4]
          # Calc new vals:
          theta0vel  = T_ref
          Pexnervel  = 1.0 - (grav_z * self._Z[k]) / (Cp * T_ref)
          rho0vel    = (P_ref/(R * theta0vel)) * np.power(Pexnervel, inv_gamma_m1)
          P0vel      = P_ref   * np.power(Pexnervel, gamma*inv_gamma_m1)
          Pvel     = (gamma-1.0) * (Evel - 0.5 * rhovel * (uvel*uvel + vvel*vvel + wvel*wvel))
          thetavel = (Evel - 0.5 * rhovel * (uvel*uvel +vvel*vvel + wvel*wvel)) / (Pexnervel*rhovel) * ((gamma-1.0)/R)

          # update the passed numpy arrays:
          rho[cntr]     = rhovel
          u[cntr]       = uvel
          v[cntr]       = vvel
          w[cntr]       = wvel
          P[cntr]       = Pvel
          theta[cntr]   = thetavel
          rho0[cntr]    = rho0vel
          P0[cntr]      = P0vel
          Pexner[cntr]  = Pexnervel
          theta0[cntr]  = theta0vel

          # update counter
          cntr += 1
    out = 0
    return out

  cdef int _current_gridded_velocity_to_numpy(self, np.ndarray x, np.ndarray y, np.ndarray z, np.ndarray u, np.ndarray v, np.ndarray w, mask_size) except -1:
    """
    Naming sucks! This function returns the velocity field mapped to grid coordinates x, y, z
    """
    cdef double rho
    cdef int out = -1
    # cdef double E

    if mask_size is not None:
      assert isinstance(mask_size, int), "mask_size must be either non or an integer!"
      if 1 < mask_size < min(self._NI, min(self._NJ, self._NK)):
        xsize = len(range(0, self._NI, mask_size))
        ysize = len(range(0, self._NJ, mask_size))
        zsize = len(range(0, self._NK, mask_size))
        dim = xsize * ysize * zsize
        m_s = mask_size
      else:
        dim = self._NI * self._NJ * self._NK
        m_s = 1
    else:
      dim = self._NI * self._NJ * self._NK
      m_s = 1


    cdef int cntr = 0
    cdef int p, s
    cdef int i, j, k

    for i in range(0, self._NI, m_s):
      for j in range(0, self._NJ, m_s):
        for k in range(0, self._NK, m_s):

          # get coordinates:
          x[cntr] = self._X[i]
          y[cntr] = self._Y[j]
          z[cntr] = self._Z[k]

          p = i + self._NI*j + self._NI*self._NJ*k
          s = self._NVARS*p
          rho = self.MODEL_STATE[s]
          u[cntr] = self.MODEL_STATE[s+1] / rho
          v[cntr] = self.MODEL_STATE[s+2] / rho
          w[cntr] = self.MODEL_STATE[s+3] / rho
          # E = self.MODEL_STATE[s+4]

          cntr += 1
    out = 0
    return out

  cpdef get_prognostic_variables_indexes(self):
    """
    return the indexes in the model state corresponding to rho, u, v, w, E respectively

    """
    cdef int i, j, k, p, ind
    if False:  # TODO: for debugging only
      rho_inds = np.empty(self._STATE_SIZE/self._NVARS, dtype=np.int)
      ind = 0
      for i in range(self._NI):
        for j in range(self._NJ):
          for k in range(self._NK):
            rho_inds[ind] = self._NVARS*(i + self._NI*j + self._NI*self._NJ*k)
            ind += 1
    else:
      rho_inds = np.arange(0, self._STATE_SIZE, self._NVARS, dtype=np.int)
    #
    u_inds = rho_inds + 1
    v_inds = rho_inds + 2
    w_inds = rho_inds + 3
    E_inds = rho_inds + 4
    return rho_inds, u_inds, v_inds, w_inds, E_inds

  def premitive_atmos_flow_variables_to_numpy(self, state=None, x=None, y=None, z=None, rho=None, u=None, v=None, w=None,
                                              P=None, theta=None, rho0=None, P0=None, Pexner=None, theta0=None
                                              ,mask_size=None):
    """
    calculates the primitive atmospheric flow variables: rho, u, v, w, P, theta, pi, rho0, P0, theta0, pi0

    Args:
        state
        x
        y
        z
        rho
        u
        v
        w
        P
        theta
        rho0
        P0
        Pexner
        theta0
        mas_size
    """
    if mask_size is not None:
      assert isinstance(mask_size, int), "mask_size must be either non or an integer!"
      if 1 < mask_size < min(self._NI, min(self._NJ, self._NK)):
        xsize = len(range(0, self._NI, mask_size))
        ysize = len(range(0, self._NJ, mask_size))
        zsize = len(range(0, self._NK, mask_size))
        dim = xsize * ysize * zsize
        m_s = mask_size
      else:
        dim = self._NI * self._NJ * self._NK
        m_s = 1
    else:
      dim = self._NI * self._NJ * self._NK
      m_s = 1

    #
    if x is None:
      x = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if x.size != dim:
        print("Grid: X-direction Size doesn't match model X-Grid size")
        raise ValueError
    if y is None:
      y = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if y.size != dim:
        print("Grid: Y-direction Size doesn't match model Y-Grid size")
        raise ValueError
    if z is None:
      z = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if z.size != dim:
        print("Grid: Z-direction Size doesn't match model Z-Grid size")
        raise ValueError
    #
    if u is None:
      u = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if u.size != dim:
        print("Velocity: U-Component Size doesn't match model U-component size")
        raise ValueError
    if v is None:
      v = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if v.size != dim:
        print("Velocity: V-Component Size doesn't match model V-component size")
        raise ValueError
    if w is None:
      w = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if w.size != dim:
        print("Velocity: W-Component Size doesn't match model W-component size")
        raise ValueError

    if Pexner is None:
      Pexner = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if Pexner.size != dim:
        print("Penxer-Component Size doesn't match model Pexner-component size")
        raise ValueError
    #
    if P is None:
      P = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if P.size != dim:
        print("P-Component Size doesn't match model P-component size")
        raise ValueError
    #
    if P0 is None:
      P0 = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if P0.size != dim:
        print("P0-Component Size doesn't match model P0-component size")
        raise ValueError
    #
    if theta is None:
      theta = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if theta.size != dim:
        print("theta-Component Size doesn't match model theta-component size")
        raise ValueError
    #
    if theta0 is None:
      theta0 = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if theta0.size != dim:
        print("theta0-Component Size doesn't match model theta0-component size")
        raise ValueError
    #
    if rho is None:
      rho = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if rho.size != dim:
        print("rho-Component Size doesn't match model rho-component size")
        raise ValueError
    #
    if rho0 is None:
      rho0 = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if rho0.size != dim:
        print("rho0-Component Size doesn't match model rho0-component size")
        raise ValueError
    #
    #
    if state is None:
      # model_path = self._paths_dict['model_path']
      # cwd = os.getcwd()
      # os.chdir(model_path)
      out = self._current_premitive_atmos_flow_variables_to_numpy(x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0, m_s)

      # os.chdir(cwd)
    else:
      if isinstance(state, StateVector):
        _state = state
      else:
        try:
          _state = self.state_vector()
          _state[:] = state[:]
        except:
          print("Failed to broadcast the passed state to an instance of StateVector of size %d" % self.state_size())
          print("Please pass an instance of StateVector, e.g use function self.state_vector()!")
          raise ValueError
      out = self._premitive_atmos_flow_variables_to_numpy(_state, x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0, m_s)
      if out == -1:
        print("Failed to extract premitive flow variables from model state")
        raise ValueError
      else:
        pass
      #
    return x, y, z, rho, u, v, w, P, theta, rho0, P0, Pexner, theta0
  # add an alias
  get_premitive_atmos_flow_variable = premitive_atmos_flow_variables_to_numpy




  def velocity_field_gridded_to_numpy(self, state=None, x=None, y=None, z=None, u=None, v=None, w=None, mask_size=None):
    """
    mask_size: not None (or non-positive number,) every ('mask_size'-1) points in each direction are skipped
    """
    if mask_size is not None:
      assert isinstance(mask_size, int), "mask_size must be either non or an integer!"
      if 1 < mask_size < min(self._NI, min(self._NJ, self._NK)):
        xsize = len(range(0, self._NI, mask_size))
        ysize = len(range(0, self._NJ, mask_size))
        zsize = len(range(0, self._NK, mask_size))
        dim = xsize * ysize * zsize
        m_s = mask_size
      else:
        dim = self._NI * self._NJ * self._NK
        m_s = 1
    else:
      dim = self._NI * self._NJ * self._NK
      m_s = 1

    #
    if x is None:
      x = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if x.size != dim:
        print("Grid: X-direction Size doesn't match model X-Grid size")
        raise ValueError
    if y is None:
      y = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if y.size != dim:
        print("Grid: Y-direction Size doesn't match model Y-Grid size")
        raise ValueError
    if z is None:
      z = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if z.size != dim:
        print("Grid: Z-direction Size doesn't match model Z-Grid size")
        raise ValueError
    #
    if u is None:
      u = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if u.size != dim:
        print("Velocity: U-Component Size doesn't match model U-component size")
        raise ValueError
    if v is None:
      v = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if v.size != dim:
        print("Velocity: V-Component Size doesn't match model V-component size")
        raise ValueError
    if w is None:
      w = np.zeros(dim, order='c', dtype=DTYPE_DOUBLE)
    else:
      if w.size != dim:
        print("Velocity: W-Component Size doesn't match model W-component size")
        raise ValueError
    #
    if state is None:
      # model_path = self._paths_dict['model_path']
      # cwd = os.getcwd()
      # os.chdir(model_path)
      out = self._current_gridded_velocity_to_numpy(x, y, z, u, v, w, m_s)
      # os.chdir(cwd)
    else:
      if isinstance(state, StateVector):
        _state = state
      else:
        try:
          _state = self.state_vector()
          _state[:] = state[:]
        except:
          print("Failed to broadcast the passed state to an instance of StateVector of size %d" % self.state_size())
          print("Please pass an instance of StateVector, e.g use function self.state_vector()!")
          raise ValueError
      out = self._state_gridded_velocity_to_numpy(_state, x, y, z, u, v, w, m_s)
      if out == -1:
        print("Failed to convert to model state to velocity field!")
        raise ValueError
      else:
        pass
      #
    return (x, y, z, u, v, w)
  # add an alias
  get_gridded_velocity_field = velocity_field_gridded_to_numpy


  def grid_to_numpy(self, x=None, y=None, z=None):
    if x is None:
      x = np.zeros(self._NI, order='c', dtype=DTYPE_DOUBLE)
    else:
      if x.size != self._NI:
        print("Grid: X-direction Size doesn't match model X-Grid size")
        raise ValueError
    if y is None:
      y = np.zeros(self._NJ, order='c', dtype=DTYPE_DOUBLE)
    else:
      if y.size != self._NJ:
        print("Grid: Y-direction Size doesn't match model Y-Grid size")
        raise ValueError
    if z is None:
      z = np.zeros(self._NK, order='c', dtype=DTYPE_DOUBLE)
    else:
      if z.size != self._NK:
        print("Grid: Z-direction Size doesn't match model Z-Grid size")
        raise ValueError
    #
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    self._model_grid_to_numpy(x, y, z)
    os.chdir(cwd)
    return (x, y, z)


  def generate_temperature(self, time_levels=None, T_diff=10, shape='disc', shape_specs=None, temperature_field=None, output_filename=None):
    """
    Based on the shape, call the function that generates and writes the temperature field

    Args:
      boundar_dim: 0, 1, 2; specifies the dimension along which the heat is generated
      boundary_dir: 1 (min face; e.g. bottom) or -1 (max face; e.g. top)
      shape: define the shape of the region that contains hot area
      shape_specs: all the geometrical variables required to define the prespecified shape
        e.g. disc >> {'rcent', 'xcent', 'ycent', 'zcent'} are the radius, and the center coordinates
    """
    if time_levels is None:
      time_levels = np.zeros(1)
    else:
      time_levels = np.asarray(time_levels).flatten()

    if shape_specs is None:
      print("You must define shape specs!")
      raise ValueError
    _shape = shape.lower().strip()
    if not ( _shape in [] or re.match(r'\A(multiple)*(-|_| )*disc(s)*\Z', _shape, re.IGNORECASE)):  # Add more to the list upon new implementations
      print("The shape %s is not supported yet!" % _shape)
      raise ValueError

    # get boundary settings to see where to generate heat!
    boundary_settings = self._model_configs['boundary']
    num_faces = boundary_settings['num_faces']
    faces_types = boundary_settings['faces_types']
    faces_settings = boundary_settings['faces_settings']

    # Loop over all faces, and create temperature file.
    for face_id in range(num_faces):
      face_type = faces_types[face_id]
      face_settings = faces_settings[face_id]
      if 'thermal' in face_type.lower().strip():
        boundary_dim = face_id // 2
        if boundary_dim % 2 == 0:
          boundary_dir = 1
        else:
          boundary_dir = -1

        # output file name:
        if isinstance(face_settings[-1], str):
          output_filename = face_settings[-1]
        else:
          output_filename = "temperature_data.dat"
        if python_version >= (3, 0, 0):
          output_filename = str.encode(output_filename)

        #
        if _shape == 'disc':
          r = shape_specs['rcent']
          x = shape_specs['xcent']
          y = shape_specs['ycent']
          z = shape_specs['zcent']
          #
          if temperature_field is None:
            n_time_levels = time_levels.size
            dims_global = np.asarray([self._NI, self._NJ, self._NK])
            dims_global[boundary_dim] = n_time_levels
            data_size = dims_global[0]*dims_global[1]*dims_global[2]
            temperature_field = np.zeros(data_size)
          #
          self.generate_temperature_field_disc(boundary_dim, boundary_dir, r, x, y, z, time_levels, T_diff, output_filename=output_filename, temp_field=temperature_field)
          #
        elif re.match(r'\A(multiple)*(-|_)*discs\Z', _shape, re.IGNORECASE):
          r = shape_specs['rcent']
          x = shape_specs['xcent']
          y = shape_specs['ycent']
          z = shape_specs['zcent']
          #
          if temperature_field is None:
            n_time_levels = time_levels.size
            dims_global = np.asarray([self._NI, self._NJ, self._NK])
            dims_global[boundary_dim] = n_time_levels
            data_size = dims_global[0]*dims_global[1]*dims_global[2]
            temperature_field = np.zeros(data_size)
          # Multiple discs:
          # assert on the length of r, x, y, z, T_diff
          num_discs = 1
          for trgt in [r, x, y, z, T_diff]:
            try:
              if len(r) > num_discs:
                num_discs = len(r)
            except(TypeError):
              pass

          # Validate r, x, y, z, T_diff:
          if self.isiterable(r):
            if len(r) == num_discs:
              rcs = r
            elif len(r) == 1:
              rcs = [r[0]] * num_discs
            else:
              print("Received 'r' of size %d " % len(r))
              print("The number of discs is %d; You must specify 'r' to be scalar, an iterable of length 1 or %d" % (num_discs, num_discs))
              raise ValueError
          else:
            rcs = [r] * num_discs
          #
          if self.isiterable(x):
            if len(x) == num_discs:
              xcs = x
            elif len(x) == 1:
              xcs = [x[0]] * num_discs
            else:
              print("Received 'x' of size %d " % len(x))
              print("The number of discs is %d; You must specify 'x' to be scalar, an iterable of length 1 or %d" % (num_discs, num_discs))
              raise ValueError
          else:
            xcs = [x] * num_discs
          #
          if self.isiterable(y):
            if len(y) == num_discs:
              ycs = y
            elif len(y) == 1:
              ycs = [y[0]] * num_discs
            else:
              print("Received 'y' of size %d " % len(y))
              print("The number of discs is %d; You must specify 'y' to be scalar, an iterable of length 1 or %d" % (num_discs, num_discs))
              raise ValueError
          else:
            ycs = [y] * num_discs
          #
          if self.isiterable(z):
            if len(z) == num_discs:
              zcs = z
            elif len(z) == 1:
              zcs = [z[0]] * num_discs
            else:
              print("Received 'z' of size %d " % len(z))
              print("The number of discs is %d; You must specify 'z' to be scalar, an iterable of length 1 or %d" % (num_discs, num_discs))
              raise ValueError
          else:
            zcs = [z] * num_discs
          #
          if self.isiterable(T_diff):
            if len(T_diff) == num_discs:
              T_diffs = T_diff
            elif len(T_diff) == 1:
              T_diffs = [T_diff[0]] * num_discs
            else:
              print("Received 'T_diff' of size %d " % len(T_diff))
              print("The number of discs is %d; You must specify 'T_diffs' to be a scalar, iterable of length 1 or %d" % (num_discs, num_discs))
              raise ValueError
          else:
            T_diffs = [T_diff] * num_discs
          # Now, generate multiple discs!
          #
          self.generate_temperature_field_multiple_discs(boundary_dim, boundary_dir, rcs, xcs, ycs, zcs, time_levels, T_diffs, output_filename=output_filename, temp_field=temperature_field)
          #
        else:
          print("The shape %s is no supported!" % shape)
          raise ValueError
        #
    return temperature_field

  cdef int generate_temperature_field_disc(self, bc_dim, bc_dir, rc, xc, yc, zc, time_levels, T_diff, output_filename, temp_field=None):
    """
    This code generates the temperature field and writes it out
    in the format that HyPar wants.
    Specifically, a hot temperature is specified within a disk
    with radius rc (meters), and a cold temperature is specified outside.
    The temperature is applied to either the lower, or upper faces of the domain.
    The temperature is eather steady (fixed over all time instances), or unsteady (prespecified for several time instances).
    
    For parallel runs, the data is split in this code.

    Remarks:
      To compile this file, you need to include hypar/Extras
   
    Args:
      bc_dim: spatial dimension for this BC (0 - x, 1 - y, or 2 - z): ");
        the temperature (hot) is assumed to propagate along this dimension x/y/z for n_time_levels
      bc_dir: face at which the thermal BC is being applied: 1 -> min face, -1 -> max face
      rc: radius of the thermal disc (in meters)
      xc, yc, zc: coordinates of the center of the disk. 
      time_levels: time levels at which temerature is generated (steady vs. unsteady temp)
      T_diff: the increate of temperature (in K) of the reference temperature, used to generate hot regions.
      output_filename: name of the file used to write the temperature field

    Notes:
      init_file_format is 'ip_file_type' in solver.inp
      dims_global is stored in solver.inp as size: Nx Ny Nz

    Returns:
      int

    """
    cdef int ierr, i, j, k
    cdef double rho_ref, p_ref, gamma, R, HB
    cdef double* gravity = <double*> calloc(self._NDIMS, sizeof(double))
    cdef char upwinding[100]
    cdef char ib_surface_data[100]
    cdef double Pr, Re, Minf, N_bv
    
    cdef int data_size
    cdef double* T_global
    
    cdef double T_ref, T_cold, T_hot;

    cdef double r, rx, ry, rz
    cdef int p
   
    # TODO: Debugging starts: 
    # print("bc_dim: ", bc_dim)
    # print("bc_dir: ", bc_dir)
    # print("Time levels: ", time_levels)
    # TODO: Debugging ends: 

    # Read physics inputs:
    physics_filename = self._paths_dict['physics_file']
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    out = _hypar_sol_handler.read_physics_input(<char*>physics_filename, <int>self._NDIMS, &rho_ref, &p_ref, &gamma, &R, &HB, gravity, upwinding, &Pr, &Re, &Minf, &N_bv, ib_surface_data)

    #
    time_levels = np.array(time_levels).flatten()
    n_time_levels = time_levels.size
    
    dims_global = np.asarray([self._NI, self._NJ, self._NK])
    dims_global[bc_dim] = n_time_levels
    
    data_size = dims_global[0]*dims_global[1]*dims_global[2]
    T_global = <double*> calloc (data_size,sizeof(double))

    if temp_field is None:
      # temp_field = np.zeros(data_size, order='c', dtype=DTYPE_DOUBLE)
      pass
    else:
      if temp_field.size != data_size:
        print("Temperature field: temp_field Size doesn't match model state size")
        raise ValueError

    T_ref = (p_ref/rho_ref)/R
    T_cold = T_ref
    T_hot = T_ref + T_diff  # T_diff [K] hotter
    
    # TODO: Debugging starts: 
    # tmp_out_string = ""
    # TODO: Debugging ends: 
    #
    for i in range(dims_global[0]):
      for j in range(dims_global[1]):
        for k in range(dims_global[2]):

          rx = self._X[i] - xc
          ry = self._Y[j] - yc
          rz = self._Z[k] - zc

          if bc_dim == 0:
            r = sqrt(ry*ry+rz*rz)
          elif bc_dim == 1:
            r = sqrt(rx*rx+rz*rz)
          elif bc_dim == 2:
            r = sqrt(rx*rx+ry*ry)

          p = i + dims_global[0]*j + dims_global[0]*dims_global[1]*k

          if r < rc:
            T_global[p] = R*T_hot
            if temp_field is not None:
              temp_field[p] = R*T_hot
          else:
            T_global[p] = R*T_cold
            if temp_field is not None:
              temp_field[p] = R*T_cold
          # print(p, temp_field[p])
          
          # TODO: Debugging starts: 
          # _ln = "%d  %f  %f  %f  %f  %f %f %f %f  %f  %f" % (p, self._X[i], self._Y[j], self._Z[k], xc, yc, zc, rx, ry, rz, T_global[p])
          # tmp_out_string  += "%s\n" % _ln
          # TODO: Debugging ends: 
    
    # TODO: Debugging starts: 
    # fname = "My_temperature_lines.txt"
    # fname = os.path.abspath(fname)
    # print("Saving temperature output in %s" % fname)
    # with open(fname, 'w') as f_id:
    #   f_id.write(tmp_out_string)
    # TODO: Debugging ends: 

    cdef int* c_iproc = <int*>calloc(self._NDIMS, sizeof(int))
    for i in range(self._NDIMS):
      c_iproc[i] = <int>self._model_configs['iproc'][i]
      # This BUG here kept me awake a couple of nights!!! TODO: Remove this comment :D


    cdef int* c_dims_global = <int*>calloc(self._NDIMS, sizeof(int))
    cdef int d_i
    for d_i in range(self._NDIMS):
      c_dims_global[d_i] = dims_global[d_i]
    
    cdef double* c_time_levels = <double*>calloc(n_time_levels, sizeof(double))
    for i in range(n_time_levels):
      c_time_levels[i] = <double> time_levels[i]
    
    # TODO: Debugging starts: 
    # print("data_size", data_size)
    # print("n_time_levels", n_time_levels)
    # print("c_iproc", [self._model_configs['iproc'][i] for i in range(self._NDIMS)])
    # print("ndims: %d", self._NDIMS)
    # print("bc_dim: %d"% bc_dim)
    # print("c_dims_global: [%d, %d, %d]", c_dims_global[0],c_dims_global[1], c_dims_global[2])
    # TODO: Debugging ends: 

    ierr = _hypar_sol_handler.write_temperature_to_file(self._NDIMS, T_global, c_iproc, c_dims_global, bc_dim, bc_dir, output_filename, c_time_levels)
    
    free(T_global)
    free(c_iproc)
    free(c_dims_global)
    free(c_time_levels)
    free(gravity)
    
    # Return to original directory:
    os.chdir(cwd)

    if ierr:
      print("Failed to write Temperature to File!")
      return ierr
    else:
      # print("Temperature Field Written to File:")  # TODO: remove after debugging
      # print("%s" % os.path.join(model_path, output_filename))
      return 0

  cdef int generate_temperature_field_multiple_discs(self, bc_dim, bc_dir, rcs, xcs, ycs, zcs, time_levels, T_diffs, output_filename, temp_field=None):
    """
    This code generates the temperature field and writes it out
    in the format that HyPar wants.
    Specifically, a set of hot sources are specified within a set of disks
    with radii rcs (meters), and cold temperatures are specified outside.
    The temperature is applied to either the lower, or upper faces of the domain.
    The temperature is eather steady (fixed over all time instances), or unsteady (prespecified for several time instances).
    
    if two discs intersect, the temperature is added from both discs

    For parallel runs, the data is split in this code.

    Args:
      bc_dim: spatial dimension for this BC (0 - x, 1 - y, or 2 - z): ");
        the temperature (hot) is assumed to propagate along this dimension x/y/z for n_time_levels
      bc_dir: face at which the thermal BC is being applied: 1 -> min face, -1 -> max face
      rcs: iterable of radii of the thermal discs (in meters); must be an iterable of the same length as xcs, ycs, zcs
      xcs, ycs, zcs: iterable of coordinates of the centers of the disks which radii are specified in rcs
      time_levels: time levels at which temerature is generated (steady vs. unsteady temp)
      T_diff: the increate of temperature (in K) of the reference temperature, used to generate hot regions;
        must be of the same length as xcs, ycs, zcs
      output_filename: name of the file used to write the temperature field

    Notes:
      init_file_format is 'ip_file_type' in solver.inp
      dims_global is stored in solver.inp as size: Nx Ny Nz

    Returns:
      int

    """
    cdef int ierr, i, j, k
    cdef double rho_ref, p_ref, gamma, R, HB
    cdef double* gravity = <double*> calloc(self._NDIMS, sizeof(double))
    cdef char upwinding[100]
    cdef char ib_surface_data[100]
    cdef double Pr, Re, Minf, N_bv

    cdef int data_size
    cdef double* T_global

    cdef double T_ref, T_cold, T_hot, temp_val

    cdef double r, rx, ry, rz
    cdef int p

    # TODO: Debugging starts: 
    # print("bc_dim: ", bc_dim)
    # print("bc_dir: ", bc_dir)
    # print("Time levels: ", time_levels)
    # TODO: Debugging ends: 

    # Read physics inputs:
    physics_filename = self._paths_dict['physics_file']
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)
    out = _hypar_sol_handler.read_physics_input(<char*>physics_filename, <int>self._NDIMS, &rho_ref, &p_ref, &gamma, &R, &HB, gravity, upwinding, &Pr, &Re, &Minf, &N_bv, ib_surface_data)

    #
    time_levels = np.array(time_levels).flatten()
    n_time_levels = time_levels.size

    dims_global = np.asarray([self._NI, self._NJ, self._NK])
    dims_global[bc_dim] = n_time_levels

    data_size = dims_global[0]*dims_global[1]*dims_global[2]
    T_global = <double*> calloc (data_size,sizeof(double))

    if temp_field is None:
      # temp_field = np.zeros(data_size, order='c', dtype=DTYPE_DOUBLE)
      pass
    else:
      if temp_field.size != data_size:
        print("Temperature field: temp_field Size doesn't match model state size")
        raise ValueError

    T_ref = (p_ref/rho_ref)/R
    T_cold = T_ref
    # T_hot = T_ref + T_diff  # T_diff [K] hotter

    #
    for i in range(dims_global[0]):
      for j in range(dims_global[1]):
        for k in range(dims_global[2]):

          p = i + dims_global[0]*j + dims_global[0]*dims_global[1]*k
          T_hot = T_cold  # Reference/Cold temperature

          # loop over all heat souces, i.e., discs
          for t_src_ind, (xc, yc, zc, rc, tdiff) in enumerate(zip(xcs, ycs, zcs, rcs, T_diffs)):

            rx = self._X[i] - xc
            ry = self._Y[j] - yc
            rz = self._Z[k] - zc

            if bc_dim == 0:
              r = sqrt(ry*ry+rz*rz)
            elif bc_dim == 1:
              r = sqrt(rx*rx+rz*rz)
            elif bc_dim == 2:
              r = sqrt(rx*rx+ry*ry)

            if r < rc:
              T_hot += tdiff
            #
          T_global[p] = R * T_hot
          if temp_field is not None:
            temp_field[p] = R * T_hot
          # print(p, temp_field[p])
          
          # TODO: Debugging starts: 
          # _ln = "%d  %f  %f  %f  %f  %f %f %f %f  %f  %f" % (p, self._X[i], self._Y[j], self._Z[k], xc, yc, zc, rx, ry, rz, T_global[p])
          # tmp_out_string  += "%s\n" % _ln
          # TODO: Debugging ends: 
    
    # TODO: Debugging starts: 
    # fname = "My_temperature_lines.txt"
    # fname = os.path.abspath(fname)
    # print("Saving temperature output in %s" % fname)
    # with open(fname, 'w') as f_id:
    #   f_id.write(tmp_out_string)
    # TODO: Debugging ends: 

    cdef int* c_iproc = <int*>calloc(self._NDIMS, sizeof(int))
    for i in range(self._NDIMS):
      c_iproc[i] = <int>self._model_configs['iproc'][i]
      # This BUG here kept me awake a couple of nights!!! TODO: Remove this comment :D


    cdef int* c_dims_global = <int*>calloc(self._NDIMS, sizeof(int))
    cdef int d_i
    for d_i in range(self._NDIMS):
      c_dims_global[d_i] = dims_global[d_i]
    
    cdef double* c_time_levels = <double*>calloc(n_time_levels, sizeof(double))
    for i in range(n_time_levels):
      c_time_levels[i] = <double> time_levels[i]

    # TODO: Debugging starts: 
    # print("data_size", data_size)
    # print("n_time_levels", n_time_levels)
    # print("c_iproc", [self._model_configs['iproc'][i] for i in range(self._NDIMS)])
    # print("ndims: %d", self._NDIMS)
    # print("bc_dim: %d"% bc_dim)
    # print("c_dims_global: [%d, %d, %d]", c_dims_global[0],c_dims_global[1], c_dims_global[2])
    # TODO: Debugging ends: 

    ierr = _hypar_sol_handler.write_temperature_to_file(self._NDIMS, T_global, c_iproc, c_dims_global, bc_dim, bc_dir, output_filename, c_time_levels)
    
    free(T_global)
    free(c_iproc)
    free(c_dims_global)
    free(c_time_levels)
    free(gravity)
    
    # Return to original directory:
    os.chdir(cwd)

    if ierr:
      print("Failed to write Temperature to File!")
      return ierr
    else:
      # print("Temperature Field Written to File:")  # TODO: remove after debugging
      # print("%s" % os.path.join(model_path, output_filename))
      return 0

  def update_ground_temperature(self, ground_temperature, output_filename=None, show_stats=False):
    """
    Given a 2D array of size (Nx by Ny), update the temperature field, where
      - the zero height (zero-level of z) is taken from ground_temperature, and the rest of the grid points
        are initialized based on filling_strategy
        - None, or 'none' -> standard temperature (low) from model configs
        - TODO: more to be added after talking to Emil
    Args:
      ground_temperature: Nx * Ny array representing temperature at the ground level; if ground temperature is a one dimensional array,
      it is assumed to be ordered such that, for each x entries, all y entries are contiguous i.e., x0, x*, x1, x*, etc.
      In this case, it is reordered properly
    """
    # get configs
    Nx, Ny, Nz = self.get_grid_sizes()
    data_size = Nx* Ny  # Just Nx * Ny
    err_msg = "ground temperature must be 1d array of size %d, or 2d numpy array with shape (Nx, Ny)=(%d, %d); received array of shape %s " % (data_size, Nx, Ny, np.shape(ground_temperature))
    assert np.size(ground_temperature) == data_size , err_msg
     
    if np.ndim(ground_temperature) == 1:
      # pass
      ground_temperature = np.reshape(ground_temperature, (Ny, Nx), order='C').flatten(order='F')
    elif np.ndim(ground_temperature) == 2:
      ground_temperature = ground_temperature.flatten(order='F')
    else:
      raise TypeError
    
    if np.isnan(ground_temperature).any():
      print("SOME NANS FOUND IN GROUND TEMPERATURE!!!!")
      raise ValueError
    
    if show_stats:
      print("\nGround Temperature Stats (K):")
      print("  Min: %f" % ground_temperature.min())
      print("  Max: %f" % ground_temperature.max())
      print("  Mean: %f" % np.mean(ground_temperature))
      print("  STD: %f" % np.std(ground_temperature))
  
    # Scale by R
    R = self._model_configs['physics']['R']
    temperature_field = ground_temperature * R

    if output_filename is None:
      try:
        boundary_settings = self.get_model_configs()['boundary']
        output_filename = boundary_settings['faces_settings'][4][-1]
      except(IndexError):
        output_filename = "_temperature_data.dat"
        print("WARNING: It seems that z boundary is not set to recognize thermal sources!")
        print("The ground temperature is written to a default file named %s")
        print("This file is not useful for HyPar given the model configurations on the boundary:")
        print(boundary_settings)
    elif isinstance(output_filename, (str, bytes)):
      pass
    else:
      print("output_filename must be either a string or bytes; received %s " % type(output_filename))
      raise TypeError

    if python_version >= (3, 0, 0):
      if isinstance(output_filename, str):
        output_filename = str.encode(output_filename)
      elif isinstance(output_filename, bytes):
        pass
      else:
        print("output_filename must be either a string or bytes; received %s " % type(output_filename))
        raise TypeError

    out = self.c_update_ground_temperature(temperature_field, output_filename)
    if out:
      print("c_update_ground_temperatur FAILED, with return value %d" % out)
    else:
      # print("c_update_ground_temperatur succeeded")
      pass
  
  cdef int c_update_ground_temperature(self, temperature_field, output_filename):
    """
    """
    cdef int Nx, Ny, Nz, data_size
    cdef int i, ierr=0
    cdef double* T_global
    
    cdef int bc_dir = 1
    cdef int bc_dim = 2

    cdef n_time_levels = 1
    time_levels = [1]

    Nx, Ny, Nz = self.get_grid_sizes()

    dims_global = np.asarray([Nx, Ny, 1])
    data_size = dims_global[0]*dims_global[1]*dims_global[2]

    assert np.size(temperature_field) == data_size, "ground temperature has a wrong size! Received %d while expected %d"  % (data_size, np.size(temperature_field))
    
    # Read physics inputs:
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)

    # Good to proceed with memory allocatioin, and storing c-structs
    T_global = <double*> calloc (data_size, sizeof(double))
    # Copy data from temperature field to T_gloabl
    for i, t in zip(range(data_size), np.nditer(temperature_field)):
      T_global[i] = t


    cdef int* c_iproc = <int*>calloc(self._NDIMS, sizeof(int))
    for i in range(self._NDIMS):
      c_iproc[i] = <int>self._model_configs['iproc'][i]
      # This BUG here kept me awake a couple of nights!!! TODO: Remove this comment :D

    cdef int* c_dims_global = <int*>calloc(self._NDIMS, sizeof(int))
    cdef int d_i
    for d_i in range(self._NDIMS):
      c_dims_global[d_i] = dims_global[d_i]

    cdef double* c_time_levels = <double*>calloc(n_time_levels, sizeof(double))
    for i in range(n_time_levels):
      c_time_levels[i] = <double> time_levels[i]

    ierr = _hypar_sol_handler.write_temperature_to_file(self._NDIMS, T_global, c_iproc, c_dims_global, bc_dim, bc_dir, output_filename, c_time_levels)

    free(T_global)
    free(c_iproc)
    free(c_dims_global)
    free(c_time_levels)

    # Return to original directory:
    os.chdir(cwd)

    if ierr:
      print("Failed to write Temperature to File!")
      ierr = 1
    else:
      # print("Temperature Field Written to File:")  # TODO: remove after debugging
      # print("%s" % os.path.join(model_path, output_filename))
      ierr = 0

    return ierr


  def update_configs(self, configs):
    """
    given configurations in the dictionary 'configs', update each (key, value) pair in self.model_configs with passed ones
    """
    assert isinstance(configs, dict), "The passed configurations must be an instance of dict, not %s" % type(configs)
    permitted_configs = ['dt', 'n_iter', 'cleanup_dir', 'rest_iter', 'ts', 'ts_type', 'hyp_scheme', 'hyp_flux_split', 
                        'hyp_int_type', 'par_type', 'par_scheme', 'cons_check', 'screen_op_iter', 'file_op_iter', 'n_io']

    if False:  # TODO: remove after debugging
      print("\n\n Attempting to update HyPar model configs with passed configurations: ")
      print(configs)
      print("\n\n")

    _configs = {}
    #  validate all passed configuratiioins
    for key in configs:
      if key in permitted_configs:
        if key in _configs:
          print("*** WARNING: Duplicate configurations found '%s'; skipping ***" % key)
        else:
          _configs.update({key:configs[key]})
      else:
        # pass  # TODO: make a choice
        print("Configuratioin '%s' is either unrecognized, or not allowed to updated online; skipping...")
        raise ValueError

    if False:  # TODO: remove after debugging
      print("\nValidated Configurations: ")
      print(_configs)
      print("\n\n")

    # _configs contains all allowed; valid configurations
    for key in _configs:
      current_val = self._model_configs[key]
      new_val = _configs[key]
      if type(new_val) == type(current_val):
        pass
      else:
        # Deeper comparison (only for handling string/byte. User is responsible for passing a valid value)
        if isinstance(current_val, (str, bytes)):
          if isinstance(current_val, str):
            # bytes to str
            new_val = bytes.decode(new_val)
            _configs.update({key:new_val})
            pass
          else:
            # str to bytes
            new_val = new_val.encode()
            _configs.update({key:new_val})
        else:
          print("The passed configuration value type doesn't match the current type")
          print("Configuration key: %s" % key)
          print("Current value %s of type %s " % (current_val, type(current_val)))
          print("New value %s of type %s " % (new_val, type(new_val)))
          raise TypeError
        #
    if False:  # TODO: remove after debugging
      print("Attempting to update the model with the following configurations...")
      print(_configs)
    # Overwrite model configs on file
    self._model_configs.update(_configs)
    if False:  # TODO: remove after debugging
      print("UPDATE")
      print("Updated configurations:" )
      print(self._model_configs)
      print("\n\n Writing to File...")
    self.write_model_configs()  # write the udpated configurations to file
    if False:  # TODO: remove after debugging
      print("SUCCEEDED...........")
    # return self.get_model_configs()
  # Add an alias
  update_model_configs = update_configs

  # TODO: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  # TODO: Remove after debugging...
  # TODO: the following function is a WIP, and is UNFINISHED...
  # TODO: We need answers to the following questions
  #         1- ow is steady vs. unsteady temperature are defined and imported into Hypar?
  # 
  # TODO: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  def update_temperature_field(self, bc_dim, bc_dir, time_levels, temperature_field, output_filename):
    """
    Given the temperature field, and the optional time_levels, prepare hte temperature field to be written to file,
    for HyPar to load properly.
    This functin prepares the input to be passed to the c-version c_update_temperature_field.

    Args:
      bc_dim:
      bc_dir:
      time_levels:
      temperature_field:
      output_filename:

    Remarks:
      Single-time-instance temperature is the main goal, but more should be tested...

    """
    msg = "Current Implementation requires debugging. If needed, debugging will be carried out!"
    msg += "TODO: We need a bug-fix, because the temperature field should give T at one side only!!!"
    raise NotImplementedError(msg)
    np_time_levels = np.asarray(time_levels).flatten()
    n_time_levels = np_time_levels.size
    NI, NJ, NK = self.get_grid_sizes()
    grid_size = NI * NJ * NK
    data_size = n_time_levels * grid_size
    
    print("A"*20)
    print("np_time_levels", np_time_levels)
    print("n_time_levels", n_time_levels)
    print("NI, NJ, NK", NI, NJ, NK)
    print("grid_size", grid_size)
    print("data_size", data_size)
    print("A"*20)

    # Check type of temperature_field
    if isinstance(temperature_field, (list, tuple)):
      # Each entry must be either a 1d array, or an instance of StateVector; add a proper assertiion
      assert len(temperature_field) == n_time_levels, "The length of time_levels must be equal to the length of temperature_field..."
      if n_time_levels == 1:
        state = temperature_field[0]
        if isinstance(state, np.ndarray):
          np_temperature_field = state.flatten()  # .copy()
        elif isinstance(state, StateVector):
          np_temperature_field = state.get_np_array()
        else:
          print("Entries of temperature_field must be either 1D numpy arrays or instances of StateVector")
          print("Received entries of type: %s " % type(state))
          raise TypeError
        # 
        assert np.size(np.temperature_field) == grid_size, "The size of temperature filed doesn't match state size"
        #
      else:
        np_temperature_field = np.empty((grid_size, n_time_levels))
        for j, state in enumerate(temperature_field):
          if isinstance(state, np.ndarray):
            np_temperature_field[:, j] = state[...]
          elif isinstance(state, StateVector):
            np_temperature_field[:, j] = state.get_np_array()
          else:
            print("Entries of temperature_field must be either 1D numpy arrays or instances of StateVector")
            print("Received entries of type: %s " % type(state))
            raise TypeError
          #
    elif isinstance(temperature_field, np.ndarray):
      # Must be a 1D array with size wqual to self.grid_size; add a proper assertiion
      assert np.size(temperature_field) == data_size, "The total number of elements in temperature field must equal to the product of number of time levels by the mdoel grid size"
      if n_time_levels == 1:
        np_temperature_field = temperature_field.flatten()
      else:
        if np.shape(temperature_field) == (grid_size, n_time_levels):
          np_temperature_field = temperature_field  # .copy()   no need for copying
        else:
          print("Shape of Numpy array containing temperature field is nto correct.")
          raise TypeError
        #

    elif isinstance(temperature_field, StateVector):
      # size equal to self.state_size; add a proper assertiion
      if not n_time_levels == 1:
        print(" a state vector instance containing temperature field is allowed only for one time level")
        raise TypeError
      else:
        np_temperature_field = temperature_field.get_np_array()
        #
    else:
      print("The passed temperature field is of an unrecogniized type %s" % type(temperature_field))
      print("Only tuple or list of numpy arrays, instances of StateVector are supported")
      raise TypeError

    if python_version >= (3, 0, 0):
      if isinstance(output_filename, str):
        output_filename = str.encode(output_filename)
      elif isinstance(output_filename, bytes):
        pass
      else:
        print("output_filename must be either a string or bytes; received %s " % type(output_filename))
        raise TypeError

    ierr = self.c_update_temperature_field(bc_dim, bc_dir, np_time_levels, np_temperature_field, output_filename)
    if not ierr:
      # 
      print("*** Successfully updated the temperature field on disk ***")
      # pass
    else:
      print("Failed to update the temperature field on disk; an error [%d] returned from PyHypar.c_update_temperature_field" % ierr)
      raise TypeError  # consider initiating a new local class for DLiDA exceptions...


  cdef int c_update_temperature_field(self, int bc_dim, int bc_dir, np.ndarray time_levels, np.ndarray temperature_field, char* output_filename):
    """
    Given a temperature field 'temperature_field', evaluated at each model grid point, with identical order to the model state,
      over(write) HyPar temerature source file
      temperature_field must be a numpy array, and it can be either 1 or 2D.
        If it's 1D, it will be taken as steady temperature field,
        if it's 2D, the columns represent temperature fields corresponding to the passed time levels.

    Args:
      bc_dim: spatial dimension for this BC (0 - x, 1 - y, or 2 - z): ");
        the temperature (hot) is assumed to propagate along this dimension x/y/z for n_time_levels
      bc_dir: face at which the thermal BC is being applied: 1 -> min face, -1 -> max face
      time_levels: an array of time levels at which the temperature files(s) is/are given
      temperature_field: 
      output_filename: name of the file used to write the temperature field

    Returns:
      int

    Remarks:
      init_file_format is 'ip_file_type' in solver.inp
      dims_global is stored in solver.inp as size: Nx Ny Nz
      the length of time_levels must be equal to the 2nd dimension of temperature_field, for 1D

    
   WIP: Coding Steps:
     1- Assert input conformability
     2- Check the passed time levels, and the type/shape of temperature field
     3- Write things by calling _hypar_sol_handler.write_temperature_to_file
    
    
    """
    cdef int data_size, i
    cdef double* T_global
    cdef int ierr = 0
    cdef double t

    # Check input values and assert conformability
    if not(0<=bc_dim<=2):
      print("bc_dim must be 0, 1 or 2 :>>: 0 - x, 1 - y, or 2 - z")
      ierr = 1
    if not abs(bc_dir)==1:
      print("bc_dir must be either 1 or -1 :>>: 1 -> min face, -1 -> max face")
      ierr = 1
    
    if ierr:
      return ierr

    #
    # Number of time levels, and shape of temperature filed
    if not np.ndim(time_levels) == 1:
      print("time_levels must be a 1D numpy array; dimension received is %d " % np.ndim(time_levels))
      ierr = 1
    n_time_levels = time_levels.size
    #
    if n_time_levels > 1:
      err_msg = None
      if not np.ndim(temperature_field) == 2:
        err_msg = "\n\nThe number of time levels is %d " % n_time_levels
        err_msg += "However, the shape of temperature_field is not 2d "
        err_msg += "Shape of temperature_field is %s\n" % repr(np.shape(temperature_field))
      elif np.size(temperature_field, 1) != n_time_levels:
        err_msg = "\n\nThe number of time levels is %d " % n_time_levels
        err_msg += "However, the shape of temperature_field is not 2d with columns equal to n_time_levels."
        err_msg += "Shape of temperature_field is %s\n" % repr(np.shape(temperature_field))
      if err_msg is None:
        pass
      else:
        print(err_msg)
        ierr = 1
    else:
      if np.ndim(temperature_field) == 1:
        pass
      else:
        err_msg = "\n\nThe number of time levels is %d " % n_time_levels
        err_msg += "However, the shape of temperature_field is not 1d"
        err_msg += "Shape of temperature_field is %s\n" % repr(np.shape(temperature_field))
        print(err_msg)
        ierr = 1

    if ierr:
      return ierr


    # Read physics inputs:
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    os.chdir(model_path)

    if False:
      _np_time_levels = np.asarray(time_levels).flatten()
      _n_time_levels = _np_time_levels.size
      NI, NJ, NK = self.get_grid_sizes()
      _grid_size = NI * NJ * NK
      _data_size = _n_time_levels * _grid_size
      
      print("B"*20)
      print("np_time_levels", _np_time_levels)
      print("n_time_levels", _n_time_levels)
      print("NI, NJ, NK", NI, NJ, NK)
      print("grid_size", _grid_size)
      print("data_size", _data_size)
      print("B"*20)


    dims_global = np.asarray([self._NI, self._NJ, self._NK])
    dims_global[bc_dim] = n_time_levels
    print("dims_global[0]: ", dims_global[0])
    print("dims_global[1]: ", dims_global[1])
    print("dims_global[2]: ", dims_global[2])
    data_size = dims_global[0]*dims_global[1]*dims_global[2]
    
    if temperature_field.size != data_size:
      print("Temperature field: temp_field Size doesn't match model state size")
      print("temperature_field.size: ", temperature_field.size)
      print("data_size should be: ", data_size)
      raise ValueError

    # Good to proceed with memory allocatioin, and storing c-structs
    T_global = <double*> calloc (data_size,sizeof(double))
    # Copy data from temperature field to T_gloabl
    for i, t in zip(range(data_size), np.nditer(temperature_field)):
      T_global[i] = t


    cdef int* c_iproc = <int*>calloc(self._NDIMS, sizeof(int))
    for i in range(self._NDIMS):
      c_iproc[i] = <int>self._model_configs['iproc'][i]
      # This BUG here kept me awake a couple of nights!!! TODO: Remove this comment :D

    cdef int* c_dims_global = <int*>calloc(self._NDIMS, sizeof(int))
    cdef int d_i
    for d_i in range(self._NDIMS):
      c_dims_global[d_i] = dims_global[d_i]

    cdef double* c_time_levels = <double*>calloc(n_time_levels, sizeof(double))
    for i in range(n_time_levels):
      c_time_levels[i] = <double> time_levels[i]

    ierr = _hypar_sol_handler.write_temperature_to_file(self._NDIMS, T_global, c_iproc, c_dims_global, bc_dim, bc_dir, output_filename, c_time_levels)

    free(T_global)
    free(c_iproc)
    free(c_dims_global)
    free(c_time_levels)

    # Return to original directory:
    os.chdir(cwd)

    if ierr:
      print("Failed to write Temperature to File!")
      ierr = 1
    else:
      # print("Temperature Field Written to File:")  # TODO: remove after debugging
      # print("%s" % os.path.join(model_path, output_filename))
      ierr = 0

    return ierr


  cpdef write_hypar_state(self, StateVector state, file_name, format='binary'):
    """
    This is similar to HyPar writearray functionality, that spits out the HyPar state in binary format
    """
    assert isinstance(state, StateVector), "state must be an instance of StateVector"
    # cdef char *_fpath
    # _fpath = <char*> fpath
    #
    if format.lower().strip() == 'binary':
      # cwd = os.getcwd()
      # cdef char *_fpath
      fpath = os.path.abspath(file_name)
      # _fpath = <char*> fpath
      out = self._write_binary_array(fpath, self._X, self._Y, self._Z, state.data)
    elif format.lower().strip() in ['ascii', 'txt', 'text']:
      print("Ascii format is not yet supported. soon to com!")
      raise ValueError
    else:
      print("Unrecognized format; only binary and ascii are supported!")
      raise ValueError
    #
    if out:
      print("Failed to wrie the state!")
    else:
      print("State written to '%s'"% fpath)


  cpdef write_hypar_initial_state(self, StateVector state, file_name):
    """
    """
    assert isinstance(state, StateVector), "state must be an instance of StateVector"
    # cwd = os.getcwd()
    # cdef char *_fpath
    fpath = os.path.abspath(file_name)
    # _fpath = <char*> fpath
    self._write_binary_initial_condition(fpath, self._X, self._Y, self._Z, state.data)
    print("State written to '%s'"% fpath)
  # Add an alias
  write_hypar_IC = write_hypar_initial_state

  cdef int _integrate(self, char* log_filepath) except -1:
    """
    This function propagates the state self.MODEL_STATE forward (based on the configurations inside solver.inp)
    If you need to change any settings of the forward propagation, this has to be done before calling this function
    What happens here is as follows:
      1- U is writtien as initial condition ('initial.inp')
      2- HyPar is executed and 'op.inp' is produced,
      3- op.inp is read, and U is overwritten
    """
    model_path = self._paths_dict['model_path']
    cwd = os.getcwd()
    # print("Changing dir to : " , model_path)
    os.chdir(model_path)

    # Write self.MODEL_STATE to initial solution file:
    cdef char* filename

    filename = self._paths_dict['init_sol_file']
    curr_time = self.MODEL_STATE.time
    self._write_binary_initial_condition(filename, self._X, self._Y, self._Z, self.MODEL_STATE.data)

    cdef Py_ssize_t length = 0
    model_run_cmd = self._paths_dict['model_run_cmd']
    
    # convert to string if bytes
    if isinstance(model_run_cmd, bytes):
      _model_run_cmd = bytes.decode(model_run_cmd)
    else:
      _model_run_cmd = model_run_cmd

    if isinstance(log_filepath, bytes):
      _log_filepath = bytes.decode(log_filepath)
    else:
      _log_filepath = log_filepath
    
    log_free_model_run_cmd = self._paths_dict['model_run_cmd']

    if len(_log_filepath) >= 1:
      screen_output = False
      _model_run_cmd = "%s >> %s" % (_model_run_cmd, _log_filepath)
      #
      if python_version >= (3, 0, 0):
        _model_run_cmd = str.encode(_model_run_cmd)

    else:
      screen_output = True
      if python_version >= (3, 0, 0):
        _model_run_cmd = str.encode(_model_run_cmd)

    # Run Hypar  # TODO: Use processes instead of system
    # print("||||  Executing HyPar with command:" ,  _model_run_cmd)
    out = system(_model_run_cmd)
    if out and not screen_output:
      print("Hypar execution failed with a return error code [%d]; Retrying ONCE with screen output on!" % out)  # TODO: experimental -> update
      print("*** RE-Executing HyPar >> $%s ***" % log_free_model_run_cmd)
      out = system(log_free_model_run_cmd)

    if not screen_output:
        # print("*** \nModel propagation log saved in file:\n\t%s\n ***" % _log_filepath)
        pass

    # update state (and grid!)
    filename = self._paths_dict['out_sol_file']
    out = self._update_state_from_sol_file(filename)
    os.chdir(cwd)
    if out:
      print("\n\t***Updating HyPar state from solution file failed!***")
      raise IOError()
    new_time = curr_time + (self._model_configs['n_iter'] * self._model_configs['dt'])
    self.MODEL_STATE.time = new_time
    return out
    #

  cpdef get_grid_sizes(self):
    cdef ni, nj, nk
    ni = self._NI
    nj = self._NJ
    nk = self._NK
    return (ni, nj, nk)

  def integrate_state(self, state=None, checkpoints=None, reset_state=True, screen_output=True, check_state_entries=True, refine_timestep=False):
    """
    return a tuple containing timespan, and states integrated over the requested timespan
      if state is None current state is used;
      if tspan is None, n_iter*dt is the length of tspan with two states (current, new) returned

      if screen_output is False, Hypar output is directed to a file named '_Hypar_output.log' in the current working directory

    """
    # get time settings
    if checkpoints is None:
      curr_time = self.MODEL_STATE.time
      new_time = curr_time + (self._model_configs['n_iter'] * self._model_configs['dt'])
      timespan = [curr_time, new_time]
    else:
      timespan = list(set(np.asarray(checkpoints).flatten()))
      timespan.sort()
      if len(timespan) < 1:
        print("Checkpoints must contain at least one time instance!")
        raise ValueError

      if timespan[0] < 0.0:
        print("Unsupported Negative time instances found in the passsed checkpoints!")
        raise ValueError

    # Backup current model info (state, time, dt, n_iter):
    curr_dt = self._model_configs['dt']
    curr_n_iter = self._model_configs['n_iter']
    if reset_state:
      current_state = self.MODEL_STATE.copy()
      current_time = self.MODEL_STATE.time
    else:
      current_state = None

    # Update model
    if state is not None:
      if isinstance(state, StateVector):
        assert self._STATE_SIZE == state.size, "Passed state must be of size equal to self.STATE_SIZE = %d; passed state is of size %d " % (self._STATE_SIZE, state.size)
        self.MODEL_STATE = state.copy()  # TODO: check if slicing is needed!
      else:
        assert(len(state)==self._STATE_SIZE), "passed state must be an iterable of length == %d" %self._STATE_SIZE
        self.MODEL_STATE[:] = [i for i in state]  # this should be really slow and Memory_InEfficient!
    else:
      # The current state self.MODEL_STATE is propagated
      pass

    # Sanity checks for NaNs
    if check_state_entries:
      nan_locs = state.where_nan()
      if nan_locs.size > 0:
        print("%s\n***ERROR***\nInitial state entries has NaN values at locations:" % ("\n\n%s" % "*"*20))
        print(nan_locs)
        print("\n%s\n\n" % "*"*20)
        raise ValueError

    # Adjust state time, then initiate the trajectory
    self.MODEL_STATE.time = timespan[0]
    # Build the forward trajectory
    trajectory = [self.MODEL_STATE.copy()]
    #
    if use_MPI:
      COMM = MPI.COMM_WORLD
      my_rank = COMM.Get_rank()
    else:
      my_rank = 0
    #
    if screen_output:
      if python_version >= (3, 0, 0):
        model_log_file = b''
      else:
        model_log_file = ''
    else:
      # TODO: replace
      output_dir = self._paths_dict['model_path']
      if python_version >= (3, 0, 0):
        model_log_file = os.path.join(output_dir, b'_Hypar_output_%d.log'%my_rank)
      else:
        model_log_file = os.path.join(output_dir, '_Hypar_output_%d.log'%my_rank)
 
    if len(timespan) > 1:
      #
      for t0, t1 in zip(timespan[0:-1], timespan[1:]):
        # extract time settings
        dt = self._model_configs['dt']
        n_iter = int((t1-t0) / dt)

        if n_iter == 0:
          rem_time = float(t1-t0)
        else:
          rem_time = float(t1-t0) % (n_iter*dt)
        # Forward integration over a subinterval
        msg = "Forward integration of HyPar Model State over time interval:[%12.8g/%12.8g]" % (t0, t1)
        if screen_output:
          print(msg)

        self.update_model_configs({'dt': dt, 'n_iter': n_iter})
        self._integrate(model_log_file)
        if  rem_time > MODEL_TIME_EPS and refine_timestep:
          final_timespan = [t1-(n_iter*dt)+t0, t1]
          print("Forward integration over a timespan smaller than original timestep: [%12.8g/%12.8g] " % (final_timespan[0], final_timespan[1]))
          self.update_model_configs({'dt': rem_time, 'n_iter': 1})
          out = self._integrate(model_log_file)
        else:
          out = 0
        if out:
          print("Time integration failed!")
        if screen_output:
          print("...Done..."),
          sys.stdout.flush()

        # Update time, and append new state to trajectory
        self.MODEL_STATE.time = t1
        out_state = self.MODEL_STATE.copy()
        if out_state.time != t1:
          print("Failed to update state time to t1 while integration using HyPar!")
          raise ValueError

        # Sanity checks for NaNs
        if check_state_entries:
          nan_locs = out_state.where_nan()
          if nan_locs.size > 0:
            print("%s\n***VALUE ERROR***\nPropagated state entries has NaN values at locations:" % ("\n\n%s" % ("*"*20)))
            print(nan_locs)
            print("\n%s\n\n" % ("*"*20))
            raise ValueError

        # append state
        trajectory.append(out_state)

        if screen_output:
          print("%s %s" % (" "*len(msg),  " "*12))

      # Restore model configs:
      self.update_model_configs({'dt': curr_dt, 'n_iter': curr_n_iter})
      if current_state is not None and reset_state:
        self.MODEL_STATE = current_state
        self.MODEL_STATE.time = current_time

    # # Sanity checks for NaNs
    # if check_state_entries:
    #   nan_locs = self.MODEL_STATE.where_nan()
    #   if nan_locs.size > 0:
    #     print("%s\n***WARNING***\nPropagated state entries has NaN values at locations:" % ("\n\n%s" % ("*"*20)))
    #     print(nan_locs)
    #     print("\n%s\n\n" % ("*"*20))

    return timespan, trajectory
    #


  cdef _velocity_to_meshgrid_filler(self, np.ndarray xm, np.ndarray ym, np.ndarray zm, np.ndarray um, np.ndarray vm, np.ndarray wm):
    """
    Quickly fill up the xmesh, ymesh, zmesh with a meshgrid (similar to Python.meshgrid)
    The passed meshes are assumed to be empty with the same dimensions: (NJ, NI, NK)
    Remark:
      I am designing this only while I am LEARNING about CYTHON, but it's not wise to use of course as it requires allocating 6 arrays of size (NIxNJxNK)
      Alternatively, if you wish, you can use 'velocity_field_gridded_to_numpy' which can be equivalently mapped to quiver if you need 3d plot
    """
    cdef int ni, nj, nk
    ni = self._NI
    nj = self._NJ
    nk = self._NK

    for i in range(ni):
        xm[0, i, :] = self._X[i]
    for j in range(1, nj):
        xm[j, :, :] = xm[0, :, :]

    for j in range(nj):
        ym[j, :, :] = self._Y[j]

    for k in range(nk):
        zm[0, :, k] = self._Z[k]
    for j in range(1, nj):
        zm[j, :, :] = zm[0, :, :]

    cdef long dim = ni*nj*nk

    # allocate some memory for collected velocity field components
    cdef double *u
    cdef double *v
    cdef double *w
    _u = <double *>malloc(dim * sizeof(double))
    _v = <double *>malloc(dim * sizeof(double))
    _w = <double *>malloc(dim * sizeof(double))

    cdef double rho
    #
    cdef int cntr = 0
    #
    for i in range(self._NI):
      for j in range(self._NJ):
        for k in range(self._NK):

          p = i + self._NI*j + self._NI*self._NJ*k
          s = self._NVARS*p
          rho = self.MODEL_STATE[s]
          _u[cntr] = self.MODEL_STATE[s+1] / rho
          _v[cntr] = self.MODEL_STATE[s+2] / rho
          _w[cntr] = self.MODEL_STATE[s+3] / rho

          cntr += 1

    #
    # Fill the meshed velocity field matrices:
    cntr = 0
    #
    for j in range(self._NJ):
      for i in range(self._NI):
        for k in range(self._NK):
          um[i, j, k] = <double>_u[cntr]  # Do we need casting!
          vm[i, j, k] = <double>_v[cntr]
          wm[i, j, k] = <double>_w[cntr]
          cntr += 1

    # cleanup
    free(_u)
    free(_v)
    free(_w)


  cpdef velocity_field_to_meshgrid(self):
    """
    This may cause memory overflow! Be careful when model is really big!
    """
    xm = np.empty((self._NJ, self._NI, self._NK))
    ym = np.empty_like(xm)
    zm = np.empty_like(xm)
    um = np.empty_like(xm)
    vm = np.empty_like(xm)
    wm = np.empty_like(xm)

    print("\n>>> Started Meshing...")
    self._velocity_to_meshgrid_filler(xm, ym, zm, um, vm, wm)
    print("Done...")

    return (xm, ym, zm, um, vm, wm)

  def state_vector(self, t=0, init_val=None, sequential=True, data_file=None):
    """
    create and return a state vector object of size equal to self._STATE_SIZE.
    Whether this is serial or parallel should be decided internally; TODO after we decide how to interface PETSc operaotions, and how to do DA...

    """
    if sequential:
      if data_file is not None:
        if os.path.isfile(data_file):
          state = StateVector(data_file=data_file)
          if state.size != self._STATE_SIZE:
            print("\nThe state stored in the passed file is of size %d, \
                  \n\rwhile the model state size should be %d\n" % (state.size, self._STATE_SIZE))
            raise ValueError
        else:
          print("Can't find the passed data file: %s " % data_file)
          raise IOError
          #
      else:
        state = StateVector(self._STATE_SIZE)
        if init_val is None:
          state.create_sequential(0.0)
        if np.isscalar(init_val):
          state.create_sequential(init_val)
        elif self.isiterable(init_val):
          state.create_sequential(0.0)
          try:
            state[:] = init_val[:]
          except:
            print("Failed to set state entries to passed init_val; setting to zero!")
            # 
      if state is not None:
        state.time = t
      #
      return state
      #
    else:
      print("TODO; ")
      raise NotImplementedError


  def state_size(self):
    return self._STATE_SIZE

  # Properties' Setters, and Getters:
  # ------------------------------------
  #
  @property
  def size(self):
    """
    Get the size of the state vector.

    """
    return self._STATE_SIZE
    #
  @size.setter
  def size(self, value):
    """
    set the size of state vector. This should not be allowed to be updated by users.

    """
    print("Can't change model state size after initialization!")
    raise NotImplementedError
    # self._size = value
    #
  @property
  def dt(self):
    return self._model_configs['dt']
  @dt.setter
  def dt(self, value):
    assert np.isscalar(value), "time step size must be scalar; Not %s !" % type(value)
    self._model_configs['dt'] = value

  @property
  def step_size(self):
    return self._model_configs['dt']
  @step_size.setter
  def step_size(self, value):
    assert np.isscalar(value), "time step size must be scalar; Not %s !" % type(value)
    self._model_configs['dt'] = value

  @property
  def time_unit(self):
    """
    return model time unit: 's', 'm', 'h'
    """
    return self._model_configs['time_unit']
