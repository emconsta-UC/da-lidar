
import os
import sys
sys.path.append(os.path.abspath("../"))
sys.path.append(os.path.abspath("../../"))

from DL_constants import DL_ROOT_PATH
# ========================================================================================== #
#                                  HyPar information:
#                              -------------------------
# - This includes HYPAR_ROOT_PATH
#
# ------------------------------------------------------------------------------------------ #
#
filename = 'hypar_path.txt'
f_dir, _ = os.path.split(__file__)
filename = os.path.join(f_dir, filename)
if not os.path.isfile(filename):
    with open(filename, 'w') as f_id:
        f_id.write("Please put the path do the directory that contains Hypar bin, src, dirs. e.g. /Users/X/.../hypar")
    sep = "\n%s\n" % ("*"*100)
    print("\n%s Please add the Hypar root path only to the file I just created:\n %s %s \n" % (sep, filename, sep))
    raise ValueError
else:
    with open(filename, 'r') as f_id:
        h_path = f_id.readline().strip(' \n')
    h_path = os.path.abspath(h_path)
    if not os.path.isdir(h_path):
        print("The Hypar root path [%s] is not a valid DIRECTORY; Please put the path do the directory that contains Hypar bin, src, dirs!" % h_path )
        raise ValueError()
    else:
        HYPAR_ROOT_PATH = h_path

HYPAR_REL_EXEC = "bin/HyPar"  # I hate that this is hardcoded!? <-env?!
HYPAR_MPI_CMD = "mpiexec"



rel_model_path = "src/Forward_Operator/Py_HyPar"  # I hate that this is hardcoded!? <-env?!


# ========================================================================================== #


#
# ========================================================================================== #
#                                  VALIDATE constants                                        #
#                    (Don't touch these unless you add new constants):                       #
# ------------------------------------------------------------------------------------------ #

if DL_ROOT_PATH is not None:
    if not isinstance(DL_ROOT_PATH, str):
        print(" 'DL_ROOT_PATH' must be a string!")
        raise TypeError
    elif not DL_ROOT_PATH:
        DL_ROOT_PATH =  os.environ.get("DL_ROOT_PATH")

if not os.path.isdir(DL_ROOT_PATH):
    print("[DL_ROOT_PATH] %s is not a valid directory!" % DL_ROOT_PATH)
    raise ValueError
else:
    os.environ["DL_ROOT_PATH"] = os.path.abspath(DL_ROOT_PATH)


if HYPAR_ROOT_PATH is not None:
    if not isinstance(HYPAR_ROOT_PATH, str):
        print(" 'HYPAR_ROOT_PATH' must be a string!")
        raise TypeError
    elif not HYPAR_ROOT_PATH:
        HYPAR_ROOT_PATH =  os.environ.get("HYPAR_ROOT_PATH")

if not os.path.isdir(HYPAR_ROOT_PATH):
    print("[HYPAR_ROOT_PATH] %s is not a valid directory!" % HYPAR_ROOT_PATH)
    raise ValueError
else:
    os.environ["HYPAR_ROOT_PATH"] = os.path.abspath(HYPAR_ROOT_PATH)


if HYPAR_REL_EXEC is not None:
    if not isinstance(HYPAR_REL_EXEC, str):
        print(" 'HYPAR_REL_EXEC' must be a string!")
        raise TypeError
    elif not HYPAR_REL_EXEC:
        HYPAR_REL_EXEC =  os.environ.get("HYPAR_REL_EXEC")
if not HYPAR_REL_EXEC:
    print("[HYPAR_REL_EXEC] %s is not setup!" % HYPAR_REL_EXEC)
    raise ValueError
else:
    os.environ['HYPAR_REL_EXEC'] = HYPAR_REL_EXEC


if HYPAR_MPI_CMD is not None:
    if not isinstance(HYPAR_MPI_CMD, str):
        print(" 'HYPAR_MPI_CMD' is not a valid mpi executable identifier !")
        raise TypeError
    elif not HYPAR_MPI_CMD:
        HYPAR_MPI_CMD =  os.environ.get("HYPAR_MPI_CMD")
if not HYPAR_MPI_CMD:
    print("[HYPAR_MPI_CMD] %s is not setup!" % HYPAR_MPI_CMD)
    raise ValueError
else:
    os.environ['HYPAR_MPI_CMD'] = HYPAR_MPI_CMD


# ========================================================================================== #
#
