# create observations' and assimilation checkpoints:
import numpy as np
obs_checkpoints = np.arange(0, 1250.001, 12.5)
da_checkpoints = obs_checkpoints

# Create sequential filtering_process object;
from filtering_process import FilteringProcess
ref_IC = model._reference_initial_condition.copy()
experiment = FilteringProcess(assimilation_configs=dict(filter=denkf_filter,
                                            da_checkpoints=da_checkpoints,
                                            ref_initial_condition=ref_IC,
                                            obs_checkpoints=obs_checkpoints,
                                            ),
                      output_configs = dict(scr_output=True, 
                                            scr_output_iter=1,
                                            file_output=True,
                                            file_output_iter=1
                                            ))
