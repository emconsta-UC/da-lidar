#!/usr/bin/env python3

import sys
import numpy as np

# Define environment variables and update Python search path;
import dates_utility as utility

# Create a model object
from qg_1p5_model import QG1p5
model = QG1p5(model_configs = dict(MREFIN=7,
                                   observation_operator_type='linear',
                                   observation_vector_size=300,
                                   observation_error_variances=4.0
                                   )
           )

# Create DA pieces:
ensemble_size = 20
initial_ensemble = model.create_initial_ensemble(ensemble_size=ensemble_size, ensemble_from_repo=True)

# create filter object
from EnKF import DEnKF
denkf_filter_configs = dict(model=model,
                           analysis_ensemble=initial_ensemble,
                           ensemble_size=ensemble_size,
                           inflation_factor=1.06,
                           localize_covariances=True,
                           localization_method='covariance_filtering',
                           localization_radius=12,
                           localization_function='Gaspari-Cohn',
                           )
denkf_filter = DEnKF(filter_configs=denkf_filter_configs, output_configs=dict(file_output_moment_only=False))

# create observations' and assimilation checkpoints:
obs_checkpoints = np.arange(0, 1250.001, 12.5)
da_checkpoints = obs_checkpoints

# Create sequential filtering_process object;
from filtering_process import FilteringProcess
ref_IC = model._reference_initial_condition.copy()
experiment = FilteringProcess(assimilation_configs=dict(filter=denkf_filter,
                                                        da_checkpoints=da_checkpoints,
                                                        ref_initial_condition=ref_IC,
                                                        obs_checkpoints=obs_checkpoints,
                                                        ),
                              output_configs = dict(scr_output=True, scr_output_iter=1,
                                                    file_output=True,file_output_iter=1)
                              )
# run the sequential filtering over the timespan created by da_checkpoints
experiment.recursive_assimilation_process()

# Clean executables and temporary modules
utility.clean_executable_files()
