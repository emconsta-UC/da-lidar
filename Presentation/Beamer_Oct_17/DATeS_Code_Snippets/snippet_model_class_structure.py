import dates_utility as utility
from models_base import ModelsBase

from state_vector_numpy import StateVectorNumpy as StateVector
from state_matrix_numpy import StateMatrixNumpy as StateMatrix
from state_matrix_sp_scipy import StateMatrixSpSciPy as SparseStateMatrix
from observation_vector_numpy import ObservationVectorNumpy as ObservationVector
from observation_matrix_numpy import ObservationMatrixNumpy as ObservationMatrix
from observation_matrix_sp_scipy import ObservationMatrixSpSciPy as SparseObservationMatrix
from error_models_numpy import BackgroundErrorModelNumpy as BackgroundErrorModel
from error_models_numpy import ObservationErrorModelNumpy as ObservationErrorModel

class DummyModel(ModelsBase):
    """ DummyModel class implementation """
    _model_name = "DummyModel"
    _default_model_configs = dict(model_name=_model_name)

    def __init__(self, model_configs=None, output_configs=None):
        """ constructor of the DummyModel class """
        # Aggregate passed model configurations with default configurations
        model_configs = utility.aggregate_configurations(model_configs, 
							 DummyModel._default_model_configs)
        self.model_configs = utility.aggregate_configurations(model_configs, 
							      ModelsBase._default_model_configs)
        
        # Aggregate passed output configurations with default configurations
        self._output_configs = utility.aggregate_configurations(output_configs, 
							 	ModelsBase._default_output_configs)        

    def state_vector(self):
        """ initialize an empty state vector """
        initial_vec_ref = np.zeros(self.state_size(), dtype=np.float32)
        initial_vec = StateVector(initial_vec_ref)
        return initial_state
        
    def state_matrix(self, create_sparse=False):
        """ initialize an dense/sparse empty state matrix """
        state_size = self.state_size()
        if create_sparse:
            state_matrix_ref = sparse.lil_matrix((state_size, state_size), dtype=np.float32)
            state_matrix = SparseStateMatrix(state_matrix_ref)
        else:
            state_matrix_ref = np.zeros((state_size, state_size))
            state_matrix = StateMatrix(state_matrix_ref)
        return state_matrix        
