# create an initial ensemble:
ensemble_size = 20
initial_ensemble = model.create_initial_ensemble(ensemble_size=ensemble_size, ensemble_from_repo=True)

# create filter object
from EnKF import DEnKF
denkf_filter_configs = dict(model=model,
                            analysis_ensemble=initial_ensemble,
                            ensemble_size=ensemble_size,
                            inflation_factor=1.06,
                            localize_covariances=True,
                            localization_method='covariance_filtering',
                            localization_radius=12,
                            localization_function='Gaspari-Cohn'
                            )
denkf_filter = DEnKF(filter_configs=denkf_filter_configs,
                     output_configs=dict(file_output_moment_only=False))
