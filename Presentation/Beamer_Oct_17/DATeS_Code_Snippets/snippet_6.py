import dates_setup
import dates_utility as utility
import numpy as np

# Prepare DATeS for a run:
dates_setup.initialize_dates(random_seed=2345)

# Create a QG-1.5 model object:
from qg_1p5_model import QG1p5
conf = dict(MREFIN=5, model_name='QG-1.5',
            model_grid_type='cartesian',
            observation_operator_type='linear',
            observation_vector_size=50,  
            observation_error_variances=4.0,
            observation_errors_covariance_method='diagonal',
            background_error_variances=5.0
            background_errors_covariance_method='diagonal',
            background_errors_covariance_localization_method='Gaspari_Cohn',
            background_errors_covariance_localization_radius=12
            )
model = QG1p5(model_configs=conf)

# Create ClHMC filter object:
ensemble_size = 25
initial_ensemble = model.create_initial_ensemble(ensemble_size=ensemble_size)

obs_checkpoints = np.arange(0, 1250.001, 12.5)
da_checkpoints = obs_checkpoints

from multi_chain_mcmc_filter import MultiChainMCMC
gmm_settings = dict(clustering_model='gmm',
                    cov_type='diag',
                    localize_covariances=False,
                    inf_criteria='aic',
                    number_of_components=None,
                    min_number_of_components=None,
                    max_number_of_components=None,
                    min_number_of_points_per_component=2,
                    invert_uncertainty_param=False,
                    approximate_covariances_from_comp=False,
                    use_oringinal_hmc_for_one_comp=False,
                    initialize_chain_strategy='forecast_mean'
                    )
mc_chain_params = dict(Symplectic_integrator='3-stage',
                       Hamiltonian_num_steps=20,
                       Hamiltonian_step_size=0.075,
                       Mass_matrix_type='prior_precisions',
                       Burn_in_num_steps=80,
                       Mixing_steps=10,
                       )                    
hmc_filter_configs = dict(model=model,
                          analysis_ensemble=initial_ensemble,
                          chain_parameters=mc_chain_parms,
                          prior_distribution='gmm',
                          gmm_prior_settings=gmm_settings,
                          ensemble_size=ensemble_size,
                          localize_covariances=False,
                          forecast_inflation_factor=1.0,
                          analysis_inflation_factor=1.0,
                          hybrid_background_coeff=1.0
                          )
hmc_output_configs = dict(scr_output=True,
                          file_output=False,
                          file_output_moment_only=False,
                          file_output_moment_name='mean',
                          file_output_separate_files=False
                          )

mc_mcmc_filter_configs = dict.copy(hmc_filter_configs)
mc_mcmc_filter_configs.update(dict(proposal_density='hmc',
                                   proposal_covariances_domain='local',
                                   gaussian_proposal_covariance_shape='diagonal',
                                   fix_gaussian_proposal_covar_diagonal=True,
                                   gaussian_proposal_variance = None,
                                   ensemble_size_weighting_strategy = 'mean_likelihood',
                                   reduce_burn_in_stage=False,
                                   forced_num_chains = None,
                                   reset_random_seed_per_chain=True,
                                   random_seed_per_chain= 1,
                                   inter_comp_inflation_factor=1.0,
                                   intra_comp_inflation_factor=1.0 
                                   )
                               )

mc_mcmc_output_configs = hmc_output_configs
filter_obj = MultiChainMCMC(filter_configs=mc_mcmc_filter_configs, 
                            output_configs = mc_mcmc_output_configs
                            )

# Create filtering process object: 
from filtering_process import FilteringProcess
ref_IC = model._reference_initial_condition.copy()
experiment = FilteringProcess(dict(model=model,
                                   filter=filter_obj,
                                   obs_checkpoints=obs_checkpoints,
                                   da_checkpoints=da_checkpoints,
                                   forecast_first=True,
                                   ref_initial_condition=ref_IC,
                                   ref_initial_time=0,
                                   random_seed=0
                                   ),
                              dict(scr_output=True,
                                   scr_output_iter=1,
                                   file_output=True,
                                   file_output_iter=1
                                   )
                              )
# Run the sequential filtering experiement:
experiment.recursive_assimilation_process()

# Clean executables and temporary modules:
utility.clean_executable_files()
