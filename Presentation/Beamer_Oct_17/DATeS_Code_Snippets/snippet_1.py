from qg_1p5_model import QG1p5
model = QG1p5(model_configs = dict(MREFIN=7,
                                   observation_operator_type='linear',
                                   observation_vector_size=300,
                                   observation_error_variances=4.0
                                   ))
