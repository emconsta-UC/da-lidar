from lorenz_models import Lorenz96
model_configs = dict(force=9.0, time_integration_scheme='ERK')
output_configs = dict(file_output_output=False)
lorenz_model = Lorenz96(model_configs, output_configs)
