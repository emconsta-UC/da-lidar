\todo{ This chapter should contain/highlight the following
  \begin{enumerate}\setlength\itemsep{0.0em}
    \item Data Assimilation and the Filtering Problem
    \item Various formulation of EnKF; and the selected version
  \end{enumerate}
}

% --------------------------------------------|
\section{Sequential Data Assimilation}
\label{sec:sequentail_DA}
% --------------------------------------------|
  Sequential ensemble-based DA filtering algorithms work by repeating two steps: \textit{forecast} and \textit{analysis}.
  In the forecast step, an ensemble of model states is propagated forward by the model equations \eqref{eqn:forward_model} to the next time point where observations are available.
  The result of the forecast step is a forecast ensemble encapsulating the prior knowledge about the model state.
  In the analysis step, the forecast states are corrected, given the collected measurements, to best approximate the truth with lower uncertainty.

  % ------------------------------------------|
  \subsection{Ensemble Kalman Filtering}
  \label{subsec:EnKF}
  % ------------------------------------------|
    The EnKF (see, e.g.,~\citep{Burgers_1998_EnKF,Evensen_1994,Evensen_2003,Houtekamer_1998a}) follows a Monte Carlo approach to approximate the moments of the underlying probability distributions in the Kalman filter equations~\citep{REKalman_1960a,Kalman_1961}.
    At time instant $t_{k-1}$, an ensemble of $\Nens$ states $\{ \xa_{k-1}(e) \}_{e=1,\ldots,\Nens} $ is used to represent the analysis, that is, the posterior probability distribution.
    The model~\eqref{eqn:forward_model} propagates each ensemble state over the time interval $[t_{k-1},t_k]$ to generate a forecast ensemble at $t_k$:
    %
    \begin{subequations}\label{eqn:EnKF_equations}
    %
    \begin{equation}
      \xb_k(e) = \mathcal{M}_{t_{k-1}\rightarrow t_k}(\xa_{k-1}(e)) + \vec{\eta}_k(e),\ \ e=1,\ldots, \Nens . \label{eq:EnKF_forecast}
    \end{equation}
    %

    The model imperfection can be accounted for by adding model errors
    through perturbations to the forecast states. The model errors are generally assumed to be Gaussian random variables, $\vec{\eta}_k \in \GM{\vec{0}}{\mat{Q}_k}$.
    In this study we do not prescribe a $\mat{Q}_k$; however, we account for model errors via inflation, and we set $\mat{Q}_k = \mat{0}\,,~ \forall k$.
    %TODO: The following statement could be moved to the discussion/future work
    Accounting for model uncertainty by perturbing the forecast ensemble using additive model errors, see e.g.,~\citep{hamill2005accounting}, or stochastic parametrizatiion~\citep{palmer2009stochastic}, is beyond the scope of this manuscript, and will be considered in separate works.

    The prior, namely, the background state, and the prior covariance matrix are approximated by the mean of the forecast ensemble $\xbarb_k$ and the flow-dependent covariance matrix $\mat{B}_k$,
    respectively, at the next assimilation time instant $t_k$:
    %
    \begin{equation}\label{eqn:EnKF_analysis}
      \begin{aligned}
      \xbarb_k   &=  \frac{1}{\Nens} \sum_{e=1}^{\Nens}{\xb_k(e) } \,, \\
      \mat{X}^{\rm b}_k   &=  [\xb_k(1)- \xbarb_k, \ldots,  \xb_k(\Nens)- \xbarb_k] \,,  \\
      \mat{B}_k   &=  \frac{1}{{\Nens-1}}  \mat{X}^{\rm b}_k \left( \mat{X}^{\rm b}_k \right)\tran  \,. % \odot \rho. \label{eq:EnKF_localization}
      \end{aligned}
    \end{equation}
    %
    \end{subequations}
    %

    Each member of the forecast ensemble $\{ \xb_k(e) \}_{e=1,\ldots,\Nens}$ is analyzed separately by using the Kalman filter formulae~\citep{Burgers_1998_EnKF,Evensen_1994}:
    %
    %
    \begin{equation}
      \label{eqn:EnKF_Analysis_and_gain}
      \begin{aligned}
        \xa_k(e)   &=  \xb_k(e) + \mat{K}_k \left( \left[\yk + \vec{\zeta}_k(e)\right] - \mathcal{H}_k(\xb_k(e)) \right),\ \\
        \mat{K}_k  &=  \mat{B}_k \mat{H}\tran_k { \left(\mat{H}_k \mat{B}_k \mat{H}\tran_k + \mat{R}_k \right)}^{-1}.
      \end{aligned}
    \end{equation}
    %
    %

    The Kalman gain matrix $\mat{K}_k$ uses the linear (or a linearized, e.g., tangent linear) observation operator $\mat{H}_k$, and the same Kalman gain is used for all ensemble members.
    The mean of the posterior ensemble $\xbara_k =  \frac{1}{\Nens} \sum_{e=1}^{\Nens}{\xa_k(e) }$ approximates the true posterior mean and can be used as the initial condition for future single-state predictions.
    The posterior covariance matrix $\mat{A}_k$ approximates the true posterior covariance matrix $\mat{P}_k^a$ and is given by
    %
    \begin{eqnarray}\label{eqn:postior_covariance}
      \mat{A}_k = \left( \mat{I} - \mat{K}_k \mat{H} \right) \mat{B}_k
      \equiv \left( \mat{B}_k^{-1} + \mat{H}_k\tran \mat{R}^{-1} \mat{H}_k \right)^{-1}.
    \end{eqnarray}
    %

   
    The stochastic ``perturbed observations'' version~\citep{Burgers_1998_EnKF} of the ensemble Kalman filter adds a different realization
    of the observation noise $\vec{\zeta}_k \sim \GM{\vec{0}}{\mat{R}_k}$ to each individual assimilation.
    On the other hand, square root ``deterministic'' formulations of the EnKF~\citep{Tippett_2003_EnSRF} avoid adding random noise to observations and thus avoid additional sampling errors.
    For additional discussion of the EnKF variants, see, for example,~\citep{asch2016data,Evensen_2007_book}.
    We note that the discussion hereafter is independent of the specific flavor of the EnKF used in the analysis step of the filter.
    %
    In what follows, we drop the time subscript $k$, for brevity of notation, and assume that all calculations are carried out at time instant $t_k$  unless otherwise stated explicitly.
    %




\subsection{Observation operator $\mathcal{H}$}


\iffalse
  Let's call the DA observational operator $H\mathcal{H}$, the elevation $\alpha$
  and the azimuth $\beta$. The wind speed components are as usual $u$,
  $v$, and $w$. The observational operator takes the model $u(t,x,y,z)$,
  $v(t,x,y,z)$, and $w(t,x,y,z)$; the $\alpha(t)$ and $\beta(t)$, and
  provites the radial velocity $r$ at $(t,x,y,z;\alpha,\beta)$.

  Assume we have $u(t,x,y,z)$, $v(t,x,y,z)$, and $w(t,x,y,z)$ from the
  model, maybe trilinear interpolation; then
  \begin{align}
    \mathcal{H}(u,v,w;\alpha,\beta)=\sqrt{a \cos(\alpha')^2 + b \cos(\beta')^2}\,,
  \end{align}
  where
  \begin{subequations}
  \begin{align}
    a=&\left(v^2+w^2\right)\\
    b=&\left(u^2+v^2\right)\\
    \alpha'=& \alpha - \arctan\left(\frac{w}{v}\right)\\
    \beta'=& \beta - \arctan\left(\frac{v}{u}\right)\,.
  \end{align}
  \end{subequations}
  ..or something like this.
\fi

%
Let's call the DA observational operator $\mathcal{H}$.
Our objective is transform a model-based simulations of the velocity vector $\vec{v} = (u, v, w)$ at any point, to radial velocity along some line of sight with predefined orientation.
For now, we assume observations are made at the same point $(x,y,z)$. The generalization of the following discussion is made using trilinear interpolation.

Assume a coordinate system (perfectly oriented with the model grid coordinates) as shown in Figure~\ref{fig:coordinate_system}.
%
\begin{figure}[H]
  \centering
  \includegraphics[width=0.25\linewidth]{Plots/3D_Spherical}
  \caption{3D ISO Coordinate system.}
  \label{fig:coordinate_system}
\end{figure}
%
Here, $\phi$ is the azimuth angle, and $\theta$ is the inclination.
The vector $\vec{v} = (u, v, w)$ in the 3D cartesian coordinate system is equivalent to a vector in the spherical coordinates $\vec{v} = (r, \theta, \phi)$.
Note that the azimuth is the angle between the positive direction of the $x$-axis with the projection of the vector $\vec{v} =(r, \theta, \phi)$  onto the $x-y$ plane. 
The inclination is the angle between the vector $\vec{v} =(r, \theta, \phi)$ and the positive direction of the $z$-axis. This means that the elevation angle (between vector and $x-y$ plane) is $\pi/2-\theta$.
Using basic transformation between spherical and caresian coordinates, the following relations hold.

Given the spherical representation of the wind velocity vector, i.e., $\vec{v} =(r, \theta, \phi)$
%
\begin{align}
  u &= r \sin{\theta} \cos{\phi}  \\
  v &= r \sin{\theta} \sin{\phi}  \\
  w &= r \cos{\theta}
\end{align}
%

Given the cartesian representation of the wind velocity vector, i.e., $\vec{v} =(u, v, w)$
%
\begin{align}
  r      &= \sqrt{u^2+v^2+w^2}    \\
  \phi   &= \arctan{\frac{v}{u}}  \\
  \theta &= \arccos{\frac{w}{r}} = \arccos{\frac{w}{ \sqrt{u^2+v^2+w^2} }}
\end{align}
%

Now, assume a line of sight represented by a unit vector $\vec{\rho}$ having an azimuth angle $\beta$, and an elevation angle $\alpha$ where the elevation is restricted in the interval $[0, \pi/2]$. From elevation, the inclination is $\pi/2-\alpha$. The cartesian representation of this vector is 
%
\begin{equation}
  \vec{\rho}= (\sin{\pi/2-\alpha} \cos{\beta} , \sin{\pi/2-\alpha} \sin{\beta}, \cos{\pi/2-\alpha}) 
  = (\cos{\alpha}\cos{\beta}, \cos{\alpha}\sin{\beta}, \sin{\alpha}) \,,
\end{equation}
%
with $\norm{\vec{\rho}}=1$. 
The goal is to find the radial velocity $\eta$, i.e. velocity observed along the lign of sight aligned with $\vec{\rho}$, 
that is the projection of the vector $\vec{v}={u,v,w}$ onto the lign of sight along the vector $\vec{\rho}$. 
This is found by Euclidean dot product as
%
\begin{align}
  \eta 
    &= \frac{\vec{v} \cdot \vec{\rho}}{\norm{\vec{\rho}}}
    = \vec{v} \cdot \vec{\rho}
    = \left( r \sin{\theta} \cos{\phi},\, r \sin{\theta} \sin{\phi},\, r \cos{\theta} \right)
      \, \left( \cos{\alpha}\cos{\beta},\, \cos{\alpha}\sin{\beta},\, \sin{\alpha} \right) \tran  \\
    &=  r \sin{\theta} \cos{\phi} \cos{\alpha}\cos{\beta} 
      + r \sin{\theta} \sin{\phi} \cos{\alpha}\sin{\beta}
      + r \cos{\theta} \sin{\alpha}  \\
    &= r \sin{\theta} \cos{\alpha} \left( \cos{\phi} \cos{\beta} + \sin{\phi} \sin{\beta} \right) 
      + r \cos{\theta} \sin{\alpha}  \\
    % &= r \left( \sin{\theta} \cos{\alpha} \cos{(\phi-\beta)} + \cos{\theta} \sin{\alpha} \right)  \\
    % 
    &= r \left( \sin{\left(\arccos{\frac{w}{r}}\right)} \cos{\alpha} \right)
      \left( 
        \cos{\left(\arctan{\frac{v}{u}}\right)} \cos{\beta} + \sin{\left(\arctan{\frac{v}{u}}\right)} \sin{\beta} 
      \right) 
      + r \cos{\left( \arccos{\frac{w}{r}} \right)} \sin{\alpha}  \\
    %
    &= r \left( \sqrt{1-\left(\frac{w}{r}\right)^2} \cos{\alpha} \right)
      \left( 
        \frac{1}{\sqrt{1+\left(\frac{v}{u}\right)^2 }} \cos{\beta} + \frac{\frac{v}{u}}{\sqrt{1+\left(\frac{v}{u}\right)^2 }} \sin{\beta}
      \right) 
      + r \frac{w}{r}  \sin{\alpha}  \\
    &= r\, 
      \left( 
        \cos{\alpha} \sqrt{1-\left(\frac{w}{r}\right)^2} \, \frac{1}{\sqrt{1+\left(\frac{v}{u}\right)^2 }}
          \left( \cos{\beta} + \frac{v}{u} \sin{\beta} \right)
      \right) \,
      + w \sin{\alpha}  \\
    &= r\, \sqrt{\frac{1-\left(\frac{w}{r}\right)^2}{1+\left(\frac{v}{u}\right)^2} } \cos{\alpha}\, 
          \left( \cos{\beta} + \frac{v}{u} \sin{\beta} \right) \,
      + w \sin{\alpha}  \\
    &= \sqrt{\frac{r^2-w^2 }{1+\left(\frac{v}{u}\right)^2} } \cos{\alpha}\, 
          \left( \cos{\beta} + \frac{v}{u} \sin{\beta} \right) \,
      + w \sin{\alpha} \\
    &= \sqrt{\frac{u^2+v^2 }{1+\left(\frac{v}{u}\right)^2} } \cos{\alpha}\, 
          \left( \cos{\beta} + \frac{v}{u} \sin{\beta} \right) \,
      + w \sin{\alpha}  \\
    &= u \, \sqrt{\frac{u^2+v^2 }{u^2+v^2} } \cos{\alpha}\, 
          \left( \cos{\beta} + \frac{v}{u} \sin{\beta} \right) \,
      + w \sin{\alpha}  \\
    &= u \, \cos{\alpha}\, \cos{\beta} \, + v \, \cos{\alpha}\, \sin{\beta}  \, + w \sin{\alpha}
\end{align}
%
where the forward-inverse trignometric identities are utilized.  % Check http://mathworld.wolfram.com/InverseTrigonometricFunctions.html
The last equation gives the transformation from model-based velocity vector components $u,\,v,\,w$ at any point in space, to radial velocity at the same point along a line of sight having an elevation angle $\alpha$ and azimuth angle $\beta$.

The last equation now, seems intuitive.
I believe, we were overthinking this matter, and didn't see it from the beginning. 
$\vec{v} = (u,v,w)$ is a vector in the 3D cartesian system. The projection of this vector on another vector $\vec{a}(k,l,m)$ is 
$ \frac{\vec{v}i \cdot \vec{a}}{\|\vec{a}\|}$. 
Assuming $\vec{a}$ has a unit length, i.e., $\|\vec{a}\|=1$, 
then the projection of $\vec{v}$ onto $\vec{a}$ is $uk + vl + wm$. 
Now, if $\vec{a} $ is represented in the polar coordinate system as $(r, \pi/2 - \alpha, \beta)$, where $r=1$, and $\alpha$ is the elevation, and $\beta$ is the azimuth, 
then the cartesian representation of this vector $(k, l, m)$ reduces to $(\sin{(\pi/2-\alpha)} \cos{\beta} ,\, \sin{(\pi/2-\alpha)} \sin{\beta} ,\, \cos{\pi/2-\alpha} )$ which is equivalent to $( \cos{\alpha} \cos{\beta} ,\, \cos{\alpha}\sin{\beta}  ,\,\sin{\alpha} )$. 
Finally, the projection of the vector $\vec{v}$ onto $\vec{a}$ is given by $u\cos{\alpha} \cos{\beta} + v \cos{\alpha}\sin{\beta} + w \sin{\alpha}$, which matches the result obtained above.


\section{Using WRF Data}
The goal is to incorporate data from WRF simulations in the DLiDA, with following purposes
%
\begin{itemize}
  \item Use Tskin to represent sensible heat
  \item Use WRF U,V,W as validation data
  \item Use WRF information to feed HyPar at the boundary of the cartesian domain
\end{itemize}
%

To be able to do any of the above, we need to:
%
\begin{enumerate}
  \item port WRF data into DLiDA,
  \item Verify WRF projected radial velocity on either the observation grid, or the model grid, against DL data
\end{enumerate}
%
This requires proper mapping from WRF coordinates to DLiDA-HyPar coordinates.
WRF uses the geodetic coordinate system, with Latitude, Longitude, and height levels (altitude) to form it's grid, while HyPar uses a simplified Cartesian grid.
To map WRF data into HyPar, we do the following
%
\begin{enumerate}
  \item Map the geodetic grid points, into Cartesian grid points, under a specific assumption of earth's shape, or using a given projection map, e.g. UTM
  \item Do the same with the coordinates of the lidar instrument location (lat, lon, alt)
  \item Now, given XYZ coordinates of WRF gridpoints, and the DL instruments (taken as the origin of HyPar), we can shift XYZ representation to the DL-based origin
  \item The shifted coordinates most likely won't match HyPar model grid points, which requires additional interpolation of the model variables from WRF XYZ points to HyPar XYZ points
\end{enumerate}
%

%
\begin{figure}[H]
  \centering
  \includegraphics[width=0.60\linewidth]{Plots/WRF_TSkin_time_2017_08_12_18_10_00}
  \includegraphics[width=0.35\linewidth]{Plots/HyPar_Interpolated_TSkin_time_2017_08_12_18_10_00}
  \caption{Left panel shows WRF-based ground temperature at 2017/08/12-18:10:00 UTC. Right panel shows the interpolated temperature on HyPar Cartesian coordinate grid}
  \label{fig:coordinate_system}
\end{figure}
%

\subsection{Questions}
Some questions need answers to properly do the above items.
\begin{itemize}
  \item The ``HeightLevels'' in WRF data are given in meters, right. Also, what is the reference datum? Is it the ground, or the MSL (Mean Sea Level)?
  \item what is the temperature unit it WRF simulations. What is the equivalent of $R$ (from HyPar) in WRF? I'm multiplying WRF TSkin by Hypar-based $R$ to get the same units, but this is a hack!
  \item The altitude of the DL instrument is given in m MSL, i.e., meters above mean sea level. If this different from WRF height levels, how do we carry out the mapping.
  \item The following link shows that WRF height can be retrieved in both cases, given the terrain heights.
    \url{https://wrf-python.readthedocs.io/en/master/internal_api/generated/wrf.geoht.get_height.html}
\end{itemize}



\subsection{Concerns}

\begin{itemize}
  \item To compare WRF data to DL data, and later to combine the former with the latter, we need to know how the DL instrument is oritented. Specifically, which directiion is taken as the 0 zimuth, and which is taken as 90 elevation?
  \item with grid mapping, I get negative heights. We may want to discuss mapping or Topography!
  \item HyPar blows up very quickly. Decreasing time step stabilizes the time integration scheme (lower CFL), for a while, but even with lower timestep, the wind-speed components blow up, $10$ orders of magnitude more than WRF-based values. We hope observation assimilation will handle this to some extent!
\end{itemize}



\subsection{The resolution and the initial ensemble}
The main obstacle is the validity of the Gaussian assumption, and the error distribution parameters.
The initial ensemble should be carefully generated, along with the observation error model, to avoid severe physics vioilation and model divergence.
Here, I generated a long series of model forecasts (no-assimilatioin), and used it to calculate variance of entries, say $\vec{b}$. I hence, generated a balanced state, and added random noise to generate an initial ensemble.
The added noise, are sampled from $\GM{0}{\diag{\vec{b}}}$.


\subsection{Constructing an observation error model}
Assuming a Gaussin model, i.e. $y-\mathcal{H}(\xf)\sim \GM{0}{\mat{R}}$, and uncorrelated observation errors, we need the diagonal of $\mat{R}$.
Here, I generated, a series of model forecast, from reanalysis, and fitted an exponential (see Fig~\ref{fig:obs_err_std}) to the standard deviation of model-observation errors. I used this (squared), to construct the diagonal of $\mat{R}$.
%
\begin{figure}[H]
  \centering
  \includegraphics[width=0.60\linewidth]{Plots/R_Diagonal}
  \caption{Standard deviation of $y-\mathcal{H}(\xf)\sim \GM{0}{\mat{R}}$ for reanalysis data, with fitted exponential used as diagonal of $\mat{R}$. The $x-$ axis represents the entries of the observation vector. Here, we have $120$ range gates.}
  \label{fig:obs_err_std}
\end{figure}
%


    % ------------------------------------------|
  \subsection{Sampling errors, spurious correlations, and filter divergence}
  \label{subsec:EnKF_limitations}
  % ------------------------------------------|
    Spurious correlations over long spatial distances occur when using
    a small ensemble of model states to approximate the covariances~\citep{Evensen_2007_book}.
    Spurious correlations, model errors, and forcing the Gaussian framework on non-Gaussian settings are known to lead to underestimation of the true state variances~\citep{Furrer_2007_covEstimation}
    and consequently can lead to filter divergence.
   
    In practice, the problem of covariance underestimation in the EnKF is
    alleviated by applying some form of covariance inflation. For example, one can scale the forecast (or the analysis) ensemble around its mean~\citep{anderson1999monte}.
    On the other hand, spurious correlations are reduced by applying covariance localization~\citep{Houtekamer_2001}. In geoscientific DA applications, distance-based covariance localization is widely used.
    In order to apply covariance inflation, an \textit{inflation factor} must be provided. Also, in order to apply distance-based covariance localization, a radius of influence (e.g., \textit{localization radius}) is required. 
    Both the inflation factor and the localization radius could be
    fixed scalars, in other words, they can be made space and time independent.
    On the other hand, making each of these parameters space and/or time dependent can enhance the performance of the filter~\citep{anderson2007adaptive,Anderson_2009_adaptive_covariance,el2018enhanced,flowerdew2015towards,Herschel_1999a}.
    Adaptive tuning of the inflation factor~\citep{anderson2007adaptive,anderson1999monte} and localization radius~\citep{anderson2007adaptive,Hamill_2001,Houtekamer_2001,Houtekamer_2005} is
    a nontrivial problem, especially in space-time--dependent settings~\citep{Constantinescu_A2007b,Constantinescu_A2007c}.
    Inflation and covariance localization are discussed in detail in~\S\ref{subsec:inflation}, \S\ref{subsec:localization}.

  % ------------------------------------------|
  \subsection{Inflation}
    \label{subsec:inflation}
  % ------------------------------------------|
    As discussed in~\S\ref{subsec:EnKF_limitations}, the problem of filter divergence due to underestimated ensemble covariance is mitigated by applying covariance inflation.
    The effect of this procedure is that the ensemble background covariance matrix $\mat{B}$ is replaced with an inflated version $\widetilde{\mat{B}}$.
    The most popular forms of covariance inflation can be classified into two types: \textit{additive} and \textit{multiplicative} inflation.
    In additive covariance inflation, a diagonal matrix $\inflmat = \diag{\inflvec}$, where $\inflvec=\left(\inflfac_1, \inflfac_2,\ldots, \inflfac_{\Nstate} \right)\tran$, is added to the ensemble covariance matrix;
    that is, $\widetilde{\mat{B}} = \inflmat + \mat{B}$. Here, the elements on the diagonal of $\inflmat$ are slightly larger than zero, namely, $0 \leq \inflfac_i \leq \inflfac^{\rm u}$ for some upper limit $\inflfac^{\rm u}$.
    The inflation factor $\inflfac_i$ can be held constant for all grid points or can be varied, in other words, made space dependent~\citep{Anderson_2009_adaptive_covariance,Herschel_1999a}.
    %
    
    % \paragraph{Multiplicative inflation}
    % ----------------------------------
      Multiplicative inflation, on the other hand, works by pushing the ensemble members away from the ensemble mean by a given inflation factor.
      Assume that the inflation factors are held constant over the model space domain, namely, $\inflfac_i=\inflfac\,,~\forall\, i=1,2,\ldots, \Nstate$.
      The inflated covariance matrix $\widetilde{\mat{B}}$ is obtained by magnifying the ensemble of forecast anomalies as follows,
      %
      \begin{equation}\label{eqn:multiplicative_inflation}
        \begin{aligned}
          \widetilde{\mat{X}^{\rm b}} &= \left[\sqrt{\inflfac}\left(\xb(1)- \xbarb\right), \ldots,  
            \sqrt{\inflfac}\left(\xb(\Nens)- \xbarb\right) \right] \,,  \\
          \widetilde{\mat{B}}   &=   \frac{1}{\Nens-1}  \widetilde{\mat{X}^{\rm b}} 
            \left( \widetilde{\mat{X}^{\rm b}} \right)\tran  = \inflfac \, \mat{B}  \,,
        \end{aligned}
      \end{equation}
      %
      where $1\leq \inflfac^l \leq \inflfac \leq \inflfac^u$ for some lower $\inflfac^l$ and upper  $\inflfac^u$ bounds, respectively.
      %
      This formulation, however, restricts the inflation factor to be
      identical for all state vector entries. Arguably, the inflation should be made more flexible by allowing spatial variability~\citep{anderson2007adaptive}.
      This generalization is formulated as follows. Consider the inflation matrix $\inflmat :=\diag{\inflvec}= \sum_{i=1}^{\Nstate}{\inflfac_i \vec{e}_i \vec{e}_i\tran}$, with
      $ \inflvec = \left( \inflfac_1, \inflfac_2, \ldots, \inflfac_{\Nstate} \right) \tran $, where $\inflfac^l_i \leq \inflfac_i \leq \inflfac^u_i$ for some lower and upper bounds on the inflation factor 
      for each entry of the state vector.
      The matrix of the magnified ensemble anomalies and the inflated ensemble-based covariance matrix $\widetilde{\mat{B}}$ respectively are the following:
      %
      \begin{equation}\label{eqn:space_multiplicative_inflation}
        \begin{aligned}
          \widetilde{\mat{X}^{\rm b}} &= \mat{D}^{\frac{1}{2}} \mat{X}^{\rm b} \,,  \\
          %
          \widetilde{\mat{B}} &= \frac{1}{\Nens-1}  \widetilde{\mat{X}^{\rm b}} \left( \widetilde{\mat{X}^{\rm b}} \right)\tran 
          = \mat{D}^{\frac{1}{2}}  \left( \frac{1}{\Nens-1} \mat{X}^{\rm b} \left( \mat{X}^{\rm b} \right)\tran \right) \mat{D}^{\frac{1}{2}}
          = \mat{D}^{\frac{1}{2}}  \mat{B} \mat{D}^{\frac{1}{2}} \,.
        \end{aligned}
      \end{equation}
      %
     
      Whether additive or multiplicative inflation is applied, the
      inflated covariance matrix is used in the analysis step of the
      filter, and thus it changes the posterior ensemble covariance matrix.
      The inflated Kalman gain $\widetilde{\mat{K}}$ and inflated analysis error covariance matrix $\widetilde{\mat{A}}$, respectively, can be written as
      %
      \begin{subequations} \label{eqn:infl_post_gain_cov}
      \begin{align} 
        \widetilde{\mat{K}} &= \widetilde{\mat{B}} \mat{H}\tran { \left(\mat{H} \widetilde{\mat{B}} \mat{H}\tran + \mat{R} \right)}^{-1} \label{eqn:infl_post_gain}\,,  \\ 
        %
        \widetilde{\mat{A}} &= \left( \mat{I} - \widetilde{\mat{K}} \mat{H} \right) \widetilde{\mat{B}}
        \equiv \left( \widetilde{\mat{B}}^{-1} + \mat{H}\tran \mat{R}^{-1} \mat{H} \right)^{-1} \,. \label{eqn:infl_post_cov}  
      \end{align}
      \end{subequations}
      %
      %
      
      Without loss of generality, hereafter we assume multiplicative inflation is used.
      In order to apply covariance inflation, that is,  Equation~\eqref{eqn:multiplicative_inflation} or~\eqref{eqn:space_multiplicative_inflation},
      the inflation factor $\inflvec$ must be tuned for the
      application in hand in order to prevent the ensemble collapse. 
      In \S\ref{sec:OED_inflation_localization}, we propose an approach to adaptively tune the space-time covariance inflation parameter $\inflvec$.


  % ------------------------------------------|
  \subsection{Covariance localization}
  \label{subsec:localization}
  % ------------------------------------------|
    In order to alleviate spurious correlations developed due to small ensemble size, covariance localization ~\citep{Hamill_2001,Houtekamer_2001,Whitaker_2002a} is performed by applying a pointwise multiplication to the ensemble
    covariances~\citep{bernstein2005matrix,Horn_1990_Hadamard,million2007hadamard,schur1911bemerkungen} with a localization or localization  matrix $\decorrmat$.
    In its standard form~\citep{bishop2007flow,Hamill_2001}, covariance localization is carried out in the model state space. 
    Specifically, the ensemble covariance matrix $\mat{B}$ is replaced in the analysis step of the filter with a localized version $\widehat{\mat{B}} = \mat{B}\odot \decorrmat$, 
    where $\decorrmat \in \Rnum^{\Nstate \times \Nstate}$ is a spatial localization matrix and $\odot$ refers to the Hadamard (Shur) pointwise product of matrices~\citep{bernstein2005matrix,Horn_1990_Hadamard,million2007hadamard,schur1911bemerkungen}.
    The entries of the localization matrix $\decorrmat$ can be calculated, for example, by using  Gauss-like formulae, such as a Gaussian kernel~\eqref{eqn:Gauss_decorr},
    or the fifth-order piecewise-rational function~\eqref{eqn:Gaspari_Cohn} of Gaspari and Cohn~\citep{Gaspari_1999_correlation}, hereafter called the GC function:
    %
    \begin{subequations}\label{eqn:localization_functions}
      %
      \begin{equation}\label{eqn:Gauss_decorr}
        \decorrcoeff_{i,j} (L)  = \exp{\left( \frac{-d(i,j)^2}{2 L^2 } \right)} \,, % \quad; i,j=1,2,\ldots, \Nstate \,,
      \end{equation}
      %
      %
      \begin{equation}\label{eqn:Gaspari_Cohn}
      \resizebox{0.90\hsize}{!}{$
        \decorrcoeff_{i,j} (L) =
        \begin{cases}
          %
          - \frac{1}{4} \left( \frac{d(i,j)}{L} \right)^5 + \frac{1}{2} \left( \frac{d(i,j)}{L} \right)^4 
            + \frac{5}{8} \left( \frac{d(i,j)}{L} \right)^3 - \frac{5}{3} \left( \frac{d(i,j)}{L} \right)^2 + 1\,, \quad  & 0 \leq d(i,j) \leq L \\
          %
          \frac{1}{12} \left( \frac{d(i,j)}{L} \right)^5 - \frac{1}{2} \left( \frac{d(i,j)}{L} \right)^4 
            +  \frac{5}{8} \left( \frac{d(i,j)}{L} \right)^3 + \frac{5}{3} \left( \frac{d(i,j)}{L} \right)^2 
              - 5 \left( \frac{d(i,j)}{L} \right)+4-\frac{2}{3} \left(\frac{L}{d(i,j)} \right)\,,  \quad & L \leq d(i,j) \leq 2L \\
          %
          0\,, \quad & 2L \leq d(i,j)\,,
          %
        \end{cases}
      %
      $}
      \end{equation}
    \end{subequations}
    %
    where $L$ is a localization parameter (e.g., localization radius) and $d(i, j)$ is the distance between the $i$th and  $j$th grid points.
    Unlike the Gaussian formula~\eqref{eqn:Gauss_decorr}, the GC
    function~\eqref{eqn:Gaspari_Cohn} is designed to have compact support such that it is nonzero only
    for a small local region and zero everywhere else~\citep{petrie2008localization}.
    Specifically, this correlation function attenuates covariance
    entries with increasing distance and has a compact support of $2L$.
    %.
    Note that, we use  $L$ to denote the localization radius  in both (\ref{eqn:Gauss_decorr}, \ref{eqn:Gaspari_Cohn}), for simplicity, however, this doesn't
    imply equivalence between the value of this parameter in both functions.
    %
    Obviously, the localization coefficient $\decorrcoeff$ is a function of the localization radius $L$.
    The distance metric $d(i,j)$ depends on the problem and is
    application specific. It
    can be  the distance between two state grid points, between two observations,
    or  between a model grid point and an observation.
    
    The correlation length scale $L$ controls the correlation function~\citep{lorenc2003potential} and must be adjusted for the application at hand.
    As suggested in~\citep{kepert2009covariance,Mitchell_2002a}, the radius of influence $L$ should be allowed to be space-time dependent and consequently must be tuned for each entry of the
    state vector.
    For example, the localization coefficients for Gaussian localization can take the form
    %
    \begin{equation}\label{eqn:localization_matrix_Gauss}
      \decorrcoeff_{i,j} (\locrad_{i,j}) = \exp{\left(- \frac{d(i, j)^2}{2 \locrad_{i,j}^2 } \right)} \,,
    \end{equation}
    %
    where $l_{i,j}$ is the localization radius corresponding to the $i$th and $j$th grid points.
    %
    In general, one can parametrize the global space-dependent localization matrix $\mat{C}$ in various ways; however, to keep the presentation simple, 
    we will maintain a high-level description of its entries:
    %
    \begin{equation}\label{eqn:localization_matrix}
      \decorrmat = [\decorrcoeff_{i,j} (\locrad_{i,j})]_{i,j=1,2,\ldots, \Nstate} \,.
    \end{equation}
    %
    
    By localizing the ensemble covariance matrix, the posterior ensemble covariance matrix is altered.
    From~\eqref{eqn:EnKF_Analysis_and_gain}, the localized Kalman gain $\widehat{\mat{K}}$ and the posterior covariance matrix $\widehat{\mat{A}}$, respectively, become
      %
      \begin{subequations}
      \begin{eqnarray}
        \widehat{\mat{K}} &=& \widehat{\mat{B}} \mat{H}\tran { \left(\mat{H} \widehat{\mat{B}} \mat{H}\tran + \mat{R} \right)}^{-1}  \,, \label{eqn:localized_gain}  \\
          \widehat{\mat{A}} &=& \left( \mat{I} - \widehat{\mat{K}} \mat{H} \right) \widehat{\mat{B}}
      \equiv \left( \widehat{\mat{B}}^{-1} + \mat{H}\tran \mat{R}^{-1} \mat{H} \right)^{-1} \label{eqn:localized_postior_covariance} \,.
      \end{eqnarray}
      \end{subequations}
    %
    
    This form of localization will be referred to as \textit{space localization}.
    Applying localization directly to the background error covariance matrix $\mat{B} \in \Rnum^{\Nstate \times \Nstate}$
    can be computationally expensive because of the high dimensionality of the model state space.
    An alternative approach is to localize the effect of assimilated observations to neighboring grid points. 
    
    The latter approach will be referred to as~\textit{observation localization}.
    % -------------------------------
    % \paragraph{$\mat{R}$-Localization}
    % -------------------------------
      In this approach the covariance localization is carried out in the observation
      space~\citep{chen2010cross,bishop2007flow,bishop2009ensemble_a,bishop2009ensemble_b}.
      This is typically done by applying localization to $\mathbf{H} \mat{B}$, and possibly to $\mathbf{HB}\mathbf{H}\tran$.
      In this version of localization, $\mat{HB} $ is replaced with
      $\widehat{\mat{HB} } = \decorrmat^{\rm loc, 1} \odot \mat{HB} $, and $\mat{HB} \mat{H}\tran$
      is possibly replaced with $\reallywidehat{\mat{HB} \mat{H}\tran} = \decorrmat^{\rm loc, 2} \odot \mat{HB} \mat{H}\tran $.
      The matrices $\decorrmat^{\rm loc, 1} \in \Rnum^{\Nobs \times \Nstate}$ and $\decorrmat^{\rm loc, 2} \in \Rnum^{\Nobs \times \Nobs}$
      are two localization matrices.
      Here $\decorrmat^{\rm loc, 1}$ is constructed based on distances between observations and model grid points.
      On the other hand, $\decorrmat^{\rm loc, 2}$ relies on distances between pairs of observation grid points.
      %
      To apply the observation localization, one can localize $\mathbf{H} \mat{B}$ only or apply localization to
      both $\mathbf{H} \mat{B}$ and $\mathbf{H} \mat{B} \mathbf{H}\tran$.
      Based on the chosen approach, the localized posterior covariance matrix $\widehat{\mat{A}}$
      can be written in the following respective forms:
      %
      \begin{equation}
        \widehat{\mat{A}} = \mat{B}-\widehat{\mat{HB} }\tran { \left( \mat{R} + {\mat{HB} \mat{H}\tran} \right)}^{-1} \widehat{\mat{HB} }
        \quad \text{or} \quad
        \widehat{\mat{A}} = \mat{B}-\widehat{\mat{HB} }\tran { \left( \mat{R} + \reallywidehat{\mat{HB} \mat{H}\tran} \right)}^{-1} \widehat{\mat{HB} }
        \,.
      \end{equation}
      %

      The space-dependent localization radii must be tuned if either space or observation localization is carried out.
      In \S\ref{sec:OED_inflation_localization}, we propose an approach for automatically tuning this parameter.



