#! /usr/bin/env python


__DEGUG_MODE__ = True

import numpy as np


def cartesian_to_spherical(cart_vec, return_degrees=True, adjust_azimuth=True, verbose=False):
    """
    Convert a vector from cartesian to spherical coordinates

    Args:
        cart_vec: the vector in cartesian coordinate system.
            cart_vec can be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries

        return_degrees: if True, the returned vector is represented in degrees, otherwise, it is represented using radian units.
        adjust_azimuth; return azimuth in the interval [0, 2 pi], otherwise [-pi, pi]
        verbose: turn printing results on/off

    Return:
        sph_vec: The representation of the vector cart_vec, in the spherical coordinates with origin centered at the origin of the cartesian system.
            sph_vec = (r, theta, phi), where r is the length of the vector, i.e., it's norm, and theta is the inclination angle, and phi is the azimuth angle.

    Remarks:
        - The inclination is the angle made with the positive direction of the z axis,
        - The azimuth is the angle between the projection of the vector cart_vec onto the x-y plane, and the positive direction of the x-axis (anti-clock-wise).

    """
    try:
        u, v, w = cart_vec[:]
    except:
        print("Couldn't cast the input 'cart_vec' into a 1d-array of length 3. ")
        print("The input must be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries")
        raise ValueError

    # Calculate the length of the vector
    r = np.sqrt(u**2 + v**2 + w**2)
    if r == 0:
        # this is the zeor vector
        phi = theta = 0
    else:
        # the inclination
        theta = np.arccos(w/r)

        # calculate the azimuth
        if u == 0:
            # the vector is in the y-z plane (phi = pi/2 or 3pi/2 based on the sign of v
            if v >= 0:
                phi = np.pi/2
            else:
                phi = 3 * np.pi/2
        else:
            phi = np.arctan2(v, u)  # This is equivalent to arctan(v/u) with singularity issues handled properly

    if adjust_azimuth:
        phi = ((2*np.pi) + phi ) % (2*np.pi)

    if return_degrees:
        theta = np.degrees(theta)
        phi = np.degrees(phi)

    sph_vec = np.array([r, theta, phi])

    if verbose:
        sep = "*"*70
        print("\n%s\n\tTrnsform Cartesian vecotr into spherical coordiantes\n%s" % (sep, sep))
        print("Input:")
        print("Cartesian representation (X-Y-X) components:\n\t %12.8g, %12.8g, %12.8g" % (u, v, w))
        print("Spherical representation (length, inclination, azimuth):")
        if return_degrees:
            print("Degrees: %12.8g, %12.8g, %12.8g" % (r, theta, phi))
        else:
            print("Radians: %12.8g, %12.8g, %12.8g" % (r, theta, phi))
        print(sep)

    return sph_vec

def spherical_to_cartesian(sph_vec, use_degrees=True, verbose=False):
    """
    Convert a vector from spherical to cartesian coordinates. This is the iverse of cartesian_to_spherical

    Args:
        sph_vec: the vector in spherical  coordinate system.
            sph_vec=(r, theta, phi) can be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries

        use_degrees: if True, the input vector is assumed to be represented in degrees, otherwise, it is represented using radian units.

        verbose: turn printing results on/off

    Return:
        cart_vec: The representation of the vector sph_vec, in the cartesian coordinates with origin centered at the origin of the spherical system.
            cart_vec = (u, v, w)

    Remarks:
        - The inclination theta, is the angle made with the positive direction of the z axis,
        - The azimuth phi, is the angle between the projection of the vector cart_vec onto the x-y plane, 
            and the positive direction of the x-axis (anti-clock-wise).

    """
    try:
        r, theta, phi = sph_vec[:]
    except:
        print("Couldn't cast the input 'sph_vec' into a 1d-array of length 3. ")
        print("The input must be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries")
        raise ValueError
    assert r >= 0, "The vector magnitude must be non-negative!"

    # We can check if r = 0, and reture immediately, but I'll just proceed
    # Convert to radians, if the input is in degrees
    if use_degrees:
        theta = np.radians(theta)
        phi = np.radians(phi)

    # Calclate the projections on the cartesian coordinates
    u = r * np.sin(theta) * np.cos(phi)
    v = r * np.sin(theta) * np.sin(phi)
    w = r * np.cos(theta)
    cart_vec = np.array([u, v, w])

    if verbose:
        sep = "*"*70
        print("\n%s\n\tTrnsform Spherical vector into Cartesian coordiantes\n%s" % (sep, sep))
        print("Input:")
        if use_degrees:
            print("** In Degrees **")
        else:
            print("** In Radians **")
        print("Spherical representation (r, theta, phi) components:\n\t %12.8g, %12.8g, %12.8g" % (sph_vec[0], sph_vec[1], sph_vec[2]))
        print("Cartesian representation (X-Y-Z) components:\n\t %12.8g, %12.8g, %12.8g" % (u, v, w))
        print(sep)

    return cart_vec


def velocity_vector_to_radial_velocity(velocity_vec, elevation, azimuth, verbose=True):
    """
    Given the velocity vector (u, v, w) in the cartesian system, claculate the radial velocity alog the line of sight 
        having a predefined elevation and azimuth

    Args:
        velocity_vec: the velocity vector in cartesian coordinate system.
            velocity_vec can be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries

        elevation, azimuth are elevation angle, and azimuth angle (both in DEGREES) of the projection vector/direction.
            For DL , these are the elevation and azimuth of the line of sight as defined by the LIDAR instrument.

        verbose: turn printing results on/off

    Returns:

        radial_velocity: the velocity observed along the line of sight defined by the passed elevation and azimuth degrees

    """
    try:
        u, v, w = velocity_vec[:]
    except:
        print("Couldn't cast the input 'velocity_vec' into a 1d-array of length 3. ")
        print("The input must be an iterable of length 3; i,e, a list, tuple of Numpy array with 3 entries")
        raise ValueError

    # Check elevation and azimuth degrees
    assert 0 <= elevation <= 90, "The elevation degrees must fall in the interval [0, 90]"
    assert 0 <= azimuth<= 360, "The azimuth degrees must fall in the interval [0, 360]"

    alpha = np.radians(elevation)
    beta = np.radians(azimuth)

    # The initial formula
    r, theta, phi = cartesian_to_spherical(velocity_vec, adjust_azimuth=False, return_degrees=False)
    rho  = r * np.sin(theta) * np.cos(phi) * np.cos(alpha) * np.cos(beta)
    rho += r * np.sin(theta) * np.sin(phi) * np.cos(alpha) * np.sin(beta)
    rho += r * np.cos(theta) * np.sin(alpha)

    # The final formula
    radial_velocity  = u * np.cos(alpha) * np.cos(beta)
    radial_velocity += v * np.cos(alpha) * np.sin(beta)
    radial_velocity += w * np.sin(alpha)

    # store computations of radial velocity using the two formulae derived in the notes
    radial_velocity_results = [rho, radial_velocity]

    if verbose:
        sep = "*"*70
        print("\n%s\n\tCalculte Radial Velocity from Velocity vecor\n%s" % (sep, sep))
        print("Passed velocity vector (%f, %f, %f)" % (u, v, w))
        print("Elevation Degrees: %f " % elevation)
        print("Azimuth Degrees: %f " % azimuth)
        if np.isclose(rho, radial_velocity, rtol=1e-10, atol=1e-12):
            print("Radial velocity results of the radial velocity from the two formula match")
            print("\t Radial velocity is: %f " % radial_velocity)
        else:
            print(" ??? The results of the radial velocity from the two formula DO NOT match!")
            print("\t First formula: radial velocity = %f " % rho)
            print("\t Second formula: radial velocity = %f " % radial_velocity)
            raise ValueError
        print(sep)

    return radial_velocity


#
if __name__ == '__main__':

    # Turn on/off the functionality you wish to test
    test_cartesian_to_spherical = True
    test_spherical_to_cartesian = True
    test_radial_velocity = True


    # Test functions
    if test_cartesian_to_spherical:
        # Cartesian to Spherical:
        print("%sTesting Cartesian to Spherical Transformation" % ("\n"*4))
        for u in [-10, 0, 15]:
            for v in [-10, 0, 15]:
                for w in [-10, 0, 15]:
                    cartesian_to_spherical([u, v, w], return_degrees=True, verbose=True)

    if test_spherical_to_cartesian:
        # Spherical to Cartesian:
        print("%sTesting Spherical to Cartesian Transformation" % ("\n"*4))
        for r in [0, 5]:
            for theta in [0, 45, 90, 135, 180]:
                for phi in [45*i for i in range(8)]:
                    spherical_to_cartesian([r, theta, phi], use_degrees=True, verbose=True)

    if test_radial_velocity:
        # Velocity to radial velocity:
        print("%sTesting Radial Velocity Calculator" % ("\n"*4))
        for elevation in [0, 30, 45, 60, 90]:
            for azimuth in [0, 45, 60, 90, 180, 270, 360]:
                for u in [-10, 0, 15]:
                    for v in [-10, 0, 15]:
                        for w in [-10, 0, 15]:
                            velocity_vector_to_radial_velocity([u, v, w], elevation, azimuth, verbose=True)

